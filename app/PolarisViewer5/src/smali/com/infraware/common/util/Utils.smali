.class public abstract Lcom/infraware/common/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final PER_USER_RANGE:I = 0x186a0

.field public static alertMessageId:I

.field public static alertTitleId:I

.field private static mButtonEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 597
    sput v0, Lcom/infraware/common/util/Utils;->alertTitleId:I

    .line 598
    sput v0, Lcom/infraware/common/util/Utils;->alertMessageId:I

    .line 800
    const/4 v0, 0x1

    sput-boolean v0, Lcom/infraware/common/util/Utils;->mButtonEnable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 72
    sput-boolean p0, Lcom/infraware/common/util/Utils;->mButtonEnable:Z

    return p0
.end method

.method public static changeDownloadMarketName(Landroid/content/Context;Landroid/view/View;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v12, 0x7f0702b2

    const/4 v11, 0x1

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 1346
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "FT03"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1348
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 1398
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1351
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    const-string/jumbo v4, ""

    .line 1352
    .local v4, "strMsg":Ljava/lang/String;
    const-string/jumbo v3, "More Service"

    .line 1353
    .local v3, "strMoreServiceName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1354
    .local v2, "hasMoreService":Z
    const/4 v1, 0x0

    .line 1358
    .local v1, "hasApps":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string/jumbo v7, "com.sec.android.app.moreservices"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1361
    const/4 v2, 0x1

    .line 1370
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string/jumbo v6, "com.sec.android.app.samsungapps"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1372
    const/4 v1, 0x1

    .line 1379
    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    .line 1381
    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v3, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1382
    const-string/jumbo v5, "Samsung"

    const-string/jumbo v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1389
    :goto_3
    instance-of v5, p1, Landroid/widget/TextView;

    if-eqz v5, :cond_5

    .line 1390
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1363
    .restart local p1    # "view":Landroid/view/View;
    :catch_0
    move-exception v0

    .line 1365
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_1

    .line 1374
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 1376
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_2

    .line 1384
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    if-eqz v1, :cond_4

    .line 1385
    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v11, [Ljava/lang/Object;

    const-string/jumbo v7, "Apps"

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 1387
    :cond_4
    invoke-virtual {p1, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 1392
    :cond_5
    invoke-virtual {p1, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1395
    .end local v1    # "hasApps":Z
    .end local v2    # "hasMoreService":Z
    .end local v3    # "strMoreServiceName":Ljava/lang/String;
    .end local v4    # "strMsg":Ljava/lang/String;
    :cond_6
    if-eqz p1, :cond_0

    .line 1396
    invoke-virtual {p1, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static dipToPixel(Landroid/content/Context;F)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # F

    .prologue
    .line 744
    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static dipToPx(Landroid/content/Context;F)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # F

    .prologue
    .line 739
    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static fullScreenActivity(Landroid/app/Activity;)V
    .locals 3
    .param p0, "oActivity"    # Landroid/app/Activity;

    .prologue
    .line 1461
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 1468
    return-void
.end method

.method public static getANSICharSet(I)Ljava/lang/String;
    .locals 2
    .param p0, "locale"    # I

    .prologue
    .line 239
    const/4 v0, 0x0

    .line 240
    .local v0, "charSet":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 320
    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->DEFAULT_CHAR_SET()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->DEFAULT_CHAR_SET()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->DEFAULT_CHAR_SET()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->DEFAULT_CHAR_SET()Ljava/lang/String;
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 329
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 330
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->displayName()Ljava/lang/String;

    move-result-object v0

    .line 334
    :cond_1
    :goto_1
    return-object v0

    .line 243
    :pswitch_1
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-932"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 244
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 246
    :pswitch_2
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-936"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 247
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 249
    :pswitch_3
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-949"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 250
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 253
    :pswitch_4
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-950"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 254
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 263
    :pswitch_5
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-950"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 264
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 270
    :pswitch_6
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1251"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 271
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 291
    :pswitch_7
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1252"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 292
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 294
    :pswitch_8
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1253"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 295
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 297
    :pswitch_9
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1254"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 298
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 300
    :pswitch_a
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1255"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 301
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 303
    :pswitch_b
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1256"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 304
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 308
    :pswitch_c
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1257"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 311
    :pswitch_d
    new-instance v0, Ljava/lang/String;

    .end local v0    # "charSet":Ljava/lang/String;
    const-string/jumbo v1, "windows-1258"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 312
    .restart local v0    # "charSet":Ljava/lang/String;
    goto :goto_1

    .line 326
    :catch_0
    move-exception v1

    goto/16 :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_6
        :pswitch_b
        :pswitch_a
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_1
        :pswitch_6
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static getBitmapOfHeight(Ljava/lang/String;)I
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 869
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 870
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 871
    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 872
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 874
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return v2

    .line 873
    :catch_0
    move-exception v0

    .line 874
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getBitmapOfWidth(Ljava/lang/String;)I
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 857
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 858
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 859
    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 860
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 862
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return v2

    .line 861
    :catch_0
    move-exception v0

    .line 862
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getCurrentLocaleType(Landroid/content/res/Resources;)I
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 339
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    return v0
.end method

.method public static getDataCacheDir(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1614
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDataDir(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 1610
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    return-object v0
.end method

.method public static getDensityDpi(Landroid/app/Activity;)I
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 780
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 781
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 782
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v1
.end method

.method public static getExifOrientation(Ljava/lang/String;)I
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    .line 897
    const/4 v0, 0x0

    .line 898
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 901
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 906
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 907
    const-string/jumbo v5, "Orientation"

    invoke-virtual {v2, v5, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 908
    .local v4, "orientation":I
    if-eq v4, v7, :cond_0

    .line 910
    packed-switch v4, :pswitch_data_0

    .line 924
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 902
    :catch_0
    move-exception v1

    .line 903
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v5, "Utils"

    const-string/jumbo v6, "cannot read exif"

    invoke-static {v5, v6}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 912
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 913
    goto :goto_1

    .line 915
    :pswitch_2
    const/16 v0, 0xb4

    .line 916
    goto :goto_1

    .line 918
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 910
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getFileNameWithExtFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "a_strFullPath"    # Ljava/lang/String;

    .prologue
    .line 1028
    const-string/jumbo v0, ""

    .line 1029
    .local v0, "fileName":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 1030
    :cond_0
    const/4 v2, 0x0

    .line 1038
    :goto_0
    return-object v2

    .line 1032
    :cond_1
    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 1033
    .local v1, "nIndex":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 1034
    move-object v0, p0

    :goto_1
    move-object v2, v0

    .line 1038
    goto :goto_0

    .line 1036
    :cond_2
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static getFlagImmersive()I
    .locals 5

    .prologue
    .line 1494
    const/4 v0, 0x0

    .line 1496
    .local v0, "SYSTEM_UI_FLAG_IMMERSIVE":I
    :try_start_0
    const-string/jumbo v3, "android.view.View"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 1497
    .local v2, "klass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v3, "SYSTEM_UI_FLAG_IMMERSIVE"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v0

    .line 1510
    .end local v2    # "klass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    return v0

    .line 1498
    :catch_0
    move-exception v1

    .line 1499
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1500
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 1502
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1503
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 1505
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 1506
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 1508
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getFolderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "a_szFullPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1058
    move-object v1, p0

    .line 1059
    .local v1, "szTemp":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 1070
    :cond_0
    :goto_0
    return-object v2

    .line 1061
    :cond_1
    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1065
    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 1066
    .local v0, "nStartIndex":I
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1069
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 1070
    goto :goto_0
.end method

.method public static getIsConnectMobileNetwork(Landroid/app/Activity;)Z
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 1599
    const-string/jumbo v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1600
    .local v1, "netManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1602
    .local v0, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1603
    const/4 v2, 0x1

    .line 1605
    :cond_0
    return v2
.end method

.method public static getIsConnectWlan(Landroid/app/Activity;)Z
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    .line 1585
    const-string/jumbo v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1588
    .local v0, "netManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1591
    .local v1, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1594
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getLocaleType(Ljava/util/Locale;)I
    .locals 5
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 76
    const/16 v0, 0x22

    .line 77
    .local v0, "nLocale":I
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "strLocaleCode":Ljava/lang/String;
    const/4 v0, 0x0

    .line 82
    if-eqz v2, :cond_0

    .line 84
    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "strKey":Ljava/lang/String;
    const-string/jumbo v3, "ko"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 88
    const/4 v0, 0x1

    .line 235
    .end local v1    # "strKey":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 90
    .restart local v1    # "strKey":Ljava/lang/String;
    :cond_1
    const-string/jumbo v3, "zh"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 92
    const-string/jumbo v3, "zh_TW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 93
    const/16 v0, 0x1d

    goto :goto_0

    .line 94
    :cond_2
    const-string/jumbo v3, "zh_HK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 95
    const/16 v0, 0x1e

    goto :goto_0

    .line 97
    :cond_3
    const/16 v0, 0x1c

    goto :goto_0

    .line 99
    :cond_4
    const-string/jumbo v3, "pt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    .line 101
    const-string/jumbo v3, "pt_BR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    .line 102
    const/16 v0, 0x1f

    goto :goto_0

    .line 104
    :cond_5
    const/16 v0, 0x11

    goto :goto_0

    .line 106
    :cond_6
    const-string/jumbo v3, "es"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_9

    .line 108
    const-string/jumbo v3, "es_MX"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_7

    .line 109
    const/16 v0, 0x20

    goto :goto_0

    .line 110
    :cond_7
    const-string/jumbo v3, "es_SA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_8

    .line 111
    const/16 v0, 0x38

    goto :goto_0

    .line 113
    :cond_8
    const/16 v0, 0x16

    goto :goto_0

    .line 115
    :cond_9
    const-string/jumbo v3, "fr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_d

    .line 117
    const-string/jumbo v3, "fr_CA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_a

    .line 118
    const/16 v0, 0x21

    goto :goto_0

    .line 119
    :cond_a
    const-string/jumbo v3, "fr_CH"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_b

    .line 120
    const/16 v0, 0x27

    goto/16 :goto_0

    .line 121
    :cond_b
    const-string/jumbo v3, "fr_BE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_c

    .line 122
    const/16 v0, 0x28

    goto/16 :goto_0

    .line 124
    :cond_c
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 126
    :cond_d
    const-string/jumbo v3, "nl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_f

    .line 128
    const-string/jumbo v3, "nl_BE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_e

    .line 129
    const/16 v0, 0x23

    goto/16 :goto_0

    .line 131
    :cond_e
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 133
    :cond_f
    const-string/jumbo v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_11

    .line 135
    const-string/jumbo v3, "de_CH"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_10

    .line 136
    const/16 v0, 0x29

    goto/16 :goto_0

    .line 138
    :cond_10
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 140
    :cond_11
    const-string/jumbo v3, "it"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_13

    .line 142
    const-string/jumbo v3, "it_CH"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_12

    .line 143
    const/16 v0, 0x2a

    goto/16 :goto_0

    .line 145
    :cond_12
    const/16 v0, 0xd

    goto/16 :goto_0

    .line 147
    :cond_13
    const-string/jumbo v3, "ru"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_15

    .line 149
    const-string/jumbo v3, "ru_IL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_14

    .line 150
    const/16 v0, 0x2b

    goto/16 :goto_0

    .line 152
    :cond_14
    const/16 v0, 0x19

    goto/16 :goto_0

    .line 154
    :cond_15
    const-string/jumbo v3, "en"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_19

    .line 156
    const-string/jumbo v3, "en_US"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_16

    .line 157
    const/16 v0, 0x22

    goto/16 :goto_0

    .line 158
    :cond_16
    const-string/jumbo v3, "en_AU"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_17

    .line 159
    const/16 v0, 0x24

    goto/16 :goto_0

    .line 160
    :cond_17
    const-string/jumbo v3, "en_CA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_18

    .line 161
    const/16 v0, 0x25

    goto/16 :goto_0

    .line 162
    :cond_18
    const-string/jumbo v3, "en_IE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 163
    const/16 v0, 0x26

    goto/16 :goto_0

    .line 165
    :cond_19
    const-string/jumbo v3, "ja"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1a

    .line 166
    const/16 v0, 0x2c

    goto/16 :goto_0

    .line 167
    :cond_1a
    const-string/jumbo v3, "bg"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1b

    .line 168
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 169
    :cond_1b
    const-string/jumbo v3, "hr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1c

    .line 170
    const/4 v0, 0x3

    goto/16 :goto_0

    .line 171
    :cond_1c
    const-string/jumbo v3, "cs"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1d

    .line 172
    const/4 v0, 0x4

    goto/16 :goto_0

    .line 173
    :cond_1d
    const-string/jumbo v3, "da"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1e

    .line 174
    const/4 v0, 0x5

    goto/16 :goto_0

    .line 175
    :cond_1e
    const-string/jumbo v3, "fi"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1f

    .line 176
    const/4 v0, 0x7

    goto/16 :goto_0

    .line 177
    :cond_1f
    const-string/jumbo v3, "el_GR"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_20

    .line 178
    const/16 v0, 0xa

    goto/16 :goto_0

    .line 179
    :cond_20
    const-string/jumbo v3, "hu"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_21

    .line 180
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 181
    :cond_21
    const-string/jumbo v3, "is"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_22

    .line 182
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 183
    :cond_22
    const-string/jumbo v3, "mk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_23

    .line 184
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 185
    :cond_23
    const-string/jumbo v3, "no"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_24

    .line 186
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 187
    :cond_24
    const-string/jumbo v3, "pl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_25

    .line 188
    const/16 v0, 0x10

    goto/16 :goto_0

    .line 189
    :cond_25
    const-string/jumbo v3, "sr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_26

    .line 190
    const/16 v0, 0x13

    goto/16 :goto_0

    .line 191
    :cond_26
    const-string/jumbo v3, "sk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_27

    .line 192
    const/16 v0, 0x14

    goto/16 :goto_0

    .line 193
    :cond_27
    const-string/jumbo v3, "sl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_28

    .line 194
    const/16 v0, 0x15

    goto/16 :goto_0

    .line 195
    :cond_28
    const-string/jumbo v3, "sv"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_29

    .line 196
    const/16 v0, 0x17

    goto/16 :goto_0

    .line 197
    :cond_29
    const-string/jumbo v3, "tr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2a

    .line 198
    const/16 v0, 0x18

    goto/16 :goto_0

    .line 199
    :cond_2a
    const-string/jumbo v3, "ar"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2b

    .line 200
    const/16 v0, 0x1a

    goto/16 :goto_0

    .line 201
    :cond_2b
    const-string/jumbo v3, "iw"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2c

    .line 202
    const/16 v0, 0x1b

    goto/16 :goto_0

    .line 203
    :cond_2c
    const-string/jumbo v3, "kk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2d

    .line 204
    const/16 v0, 0x2d

    goto/16 :goto_0

    .line 205
    :cond_2d
    const-string/jumbo v3, "lt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2e

    .line 206
    const/16 v0, 0x2e

    goto/16 :goto_0

    .line 207
    :cond_2e
    const-string/jumbo v3, "lv"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2f

    .line 208
    const/16 v0, 0x2f

    goto/16 :goto_0

    .line 209
    :cond_2f
    const-string/jumbo v3, "et"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_30

    .line 210
    const/16 v0, 0x30

    goto/16 :goto_0

    .line 211
    :cond_30
    const-string/jumbo v3, "vi"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_31

    .line 212
    const/16 v0, 0x31

    goto/16 :goto_0

    .line 213
    :cond_31
    const-string/jumbo v3, "he"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_32

    .line 214
    const/16 v0, 0x32

    goto/16 :goto_0

    .line 215
    :cond_32
    const-string/jumbo v3, "th"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_33

    .line 216
    const/16 v0, 0x33

    goto/16 :goto_0

    .line 217
    :cond_33
    const-string/jumbo v3, "in"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_34

    const-string/jumbo v3, "id"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_35

    .line 218
    :cond_34
    const/16 v0, 0x34

    goto/16 :goto_0

    .line 219
    :cond_35
    const-string/jumbo v3, "ms"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_36

    .line 220
    const/16 v0, 0x35

    goto/16 :goto_0

    .line 221
    :cond_36
    const-string/jumbo v3, "fa"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_37

    .line 222
    const/16 v0, 0x36

    goto/16 :goto_0

    .line 223
    :cond_37
    const-string/jumbo v3, "uk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_38

    .line 224
    const/16 v0, 0x37

    goto/16 :goto_0

    .line 225
    :cond_38
    const-string/jumbo v3, "ro"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 226
    const/16 v0, 0x12

    goto/16 :goto_0
.end method

.method public static getMimeInfo(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/infraware/common/define/CMDefine$MimeInfo;
    .locals 12
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "mediaType"    # I

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 434
    const/4 v8, 0x0

    .line 435
    .local v8, "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    const/4 v6, 0x0

    .line 437
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, -0x1

    .line 440
    .local v7, "index":I
    packed-switch p2, :pswitch_data_0

    .line 534
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    move-object v9, v10

    .line 556
    :goto_1
    return-object v9

    .line 443
    :pswitch_0
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_data = \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 444
    if-eqz v6, :cond_0

    .line 446
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 447
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 449
    :cond_2
    if-eq v7, v11, :cond_0

    .line 451
    new-instance v9, Lcom/infraware/common/define/CMDefine$MimeInfo;

    invoke-direct {v9, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 453
    .end local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .local v9, "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :try_start_1
    iput v7, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->id:I

    .line 454
    const/4 v0, 0x1

    iput v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaType:I

    .line 455
    const-string/jumbo v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    .line 456
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaUri:Landroid/net/Uri;

    .line 457
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v1, v7

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->contentUri:Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 534
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v8, v9

    .end local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    goto :goto_1

    .line 464
    :pswitch_1
    :try_start_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_data = \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 465
    if-eqz v6, :cond_0

    .line 467
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 468
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 470
    :cond_4
    if-eq v7, v11, :cond_0

    .line 472
    new-instance v9, Lcom/infraware/common/define/CMDefine$MimeInfo;

    invoke-direct {v9, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 474
    .end local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :try_start_3
    iput v7, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->id:I

    .line 475
    const/4 v0, 0x2

    iput v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaType:I

    .line 476
    const-string/jumbo v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    .line 477
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaUri:Landroid/net/Uri;

    .line 478
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v1, v7

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->contentUri:Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 534
    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_5

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v8, v9

    .end local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    goto/16 :goto_1

    .line 485
    :pswitch_2
    :try_start_4
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_data = \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 486
    if-eqz v6, :cond_0

    .line 488
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 489
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 491
    :cond_6
    if-eq v7, v11, :cond_0

    .line 493
    new-instance v9, Lcom/infraware/common/define/CMDefine$MimeInfo;

    invoke-direct {v9, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 495
    .end local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :try_start_5
    iput v7, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->id:I

    .line 496
    const/4 v0, 0x3

    iput v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaType:I

    .line 497
    const-string/jumbo v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    .line 498
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaUri:Landroid/net/Uri;

    .line 499
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v1, v7

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->contentUri:Landroid/net/Uri;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 534
    if-eqz v6, :cond_7

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_7

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v8, v9

    .end local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    goto/16 :goto_1

    .line 506
    :pswitch_3
    :try_start_6
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->getDocumentURI()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 509
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->getDocumentURI()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_data = \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 510
    if-eqz v6, :cond_0

    .line 512
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 513
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 515
    :cond_8
    if-eq v7, v11, :cond_0

    .line 517
    new-instance v9, Lcom/infraware/common/define/CMDefine$MimeInfo;

    invoke-direct {v9, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 519
    .end local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :try_start_7
    iput v7, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->id:I

    .line 520
    const/4 v0, 0x4

    iput v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaType:I

    .line 521
    const-string/jumbo v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    .line 522
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->getDocumentURI()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaUri:Landroid/net/Uri;

    .line 523
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->getDocumentURI()Landroid/net/Uri;

    move-result-object v0

    int-to-long v1, v7

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v9, Lcom/infraware/common/define/CMDefine$MimeInfo;->contentUri:Landroid/net/Uri;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 534
    if-eqz v6, :cond_9

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_9

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_9
    move-object v8, v9

    .end local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    goto/16 :goto_1

    .line 531
    :catch_0
    move-exception v0

    .line 534
    :goto_2
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 534
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_a

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_a

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v0

    .line 534
    .end local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :catchall_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    goto :goto_3

    .line 531
    .end local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :catch_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .restart local v8    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    goto :goto_2

    .line 440
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getMimeInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/infraware/common/define/CMDefine$MimeInfo;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 344
    const/4 v2, 0x0

    .line 346
    .local v2, "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    const/16 v5, 0x2e

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 348
    .local v1, "idx_ext":I
    if-gez v1, :cond_0

    const-string/jumbo v0, ""

    .line 352
    .local v0, "ext":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "rm"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 353
    new-instance v2, Lcom/infraware/common/define/CMDefine$MimeInfo;

    .end local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    invoke-direct {v2, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V

    .line 354
    .restart local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    const-string/jumbo v5, "video/mp4"

    iput-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    move-object v3, v2

    .line 394
    .end local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    .local v3, "info":Ljava/lang/Object;
    :goto_1
    return-object v3

    .line 349
    .end local v0    # "ext":Ljava/lang/String;
    .end local v3    # "info":Ljava/lang/Object;
    .restart local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    :cond_0
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "ext":Ljava/lang/String;
    goto :goto_0

    .line 357
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "mkv"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 358
    new-instance v2, Lcom/infraware/common/define/CMDefine$MimeInfo;

    .end local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    invoke-direct {v2, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V

    .line 359
    .restart local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    const-string/jumbo v5, "video/mkv"

    iput-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    move-object v3, v2

    .line 360
    .restart local v3    # "info":Ljava/lang/Object;
    goto :goto_1

    .line 364
    .end local v3    # "info":Ljava/lang/Object;
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 366
    .local v4, "resolver":Landroid/content/ContentResolver;
    const/4 v5, 0x1

    invoke-static {v4, p1, v5}, Lcom/infraware/common/util/Utils;->getMimeInfo(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/infraware/common/define/CMDefine$MimeInfo;

    move-result-object v2

    .line 367
    if-eqz v2, :cond_3

    move-object v3, v2

    .line 368
    .restart local v3    # "info":Ljava/lang/Object;
    goto :goto_1

    .line 370
    .end local v3    # "info":Ljava/lang/Object;
    :cond_3
    const/4 v5, 0x2

    invoke-static {v4, p1, v5}, Lcom/infraware/common/util/Utils;->getMimeInfo(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/infraware/common/define/CMDefine$MimeInfo;

    move-result-object v2

    .line 371
    if-eqz v2, :cond_4

    move-object v3, v2

    .line 372
    .restart local v3    # "info":Ljava/lang/Object;
    goto :goto_1

    .line 374
    .end local v3    # "info":Ljava/lang/Object;
    :cond_4
    const/4 v5, 0x3

    invoke-static {v4, p1, v5}, Lcom/infraware/common/util/Utils;->getMimeInfo(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/infraware/common/define/CMDefine$MimeInfo;

    move-result-object v2

    .line 375
    if-eqz v2, :cond_5

    move-object v3, v2

    .line 376
    .restart local v3    # "info":Ljava/lang/Object;
    goto :goto_1

    .line 378
    .end local v3    # "info":Ljava/lang/Object;
    :cond_5
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine;->getDocumentURI()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 380
    const/4 v5, 0x4

    invoke-static {v4, p1, v5}, Lcom/infraware/common/util/Utils;->getMimeInfo(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/infraware/common/define/CMDefine$MimeInfo;

    move-result-object v2

    .line 381
    if-eqz v2, :cond_8

    .line 384
    iget-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_7

    .line 385
    :cond_6
    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->getMimeTypeInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    :cond_7
    move-object v3, v2

    .line 387
    .restart local v3    # "info":Ljava/lang/Object;
    goto :goto_1

    .line 391
    .end local v3    # "info":Ljava/lang/Object;
    :cond_8
    new-instance v2, Lcom/infraware/common/define/CMDefine$MimeInfo;

    .end local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    invoke-direct {v2, p1}, Lcom/infraware/common/define/CMDefine$MimeInfo;-><init>(Ljava/lang/String;)V

    .line 392
    .restart local v2    # "info":Lcom/infraware/common/define/CMDefine$MimeInfo;
    invoke-static {p0, v0}, Lcom/infraware/common/util/Utils;->getMimeTypeInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    move-object v3, v2

    .line 394
    .restart local v3    # "info":Ljava/lang/Object;
    goto :goto_1
.end method

.method public static getMimeTypeInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ext"    # Ljava/lang/String;

    .prologue
    .line 399
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "type":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "FT03"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401
    if-nez v0, :cond_9

    .line 402
    const-string/jumbo v1, "mp2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 403
    const-string/jumbo v0, "audio/mpeg"

    .line 429
    :cond_0
    :goto_0
    return-object v0

    .line 404
    :cond_1
    const-string/jumbo v1, "mkv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 405
    const-string/jumbo v0, "video/mkv"

    goto :goto_0

    .line 406
    :cond_2
    const-string/jumbo v1, "m4v"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 407
    const-string/jumbo v0, "video/x-m4v"

    goto :goto_0

    .line 408
    :cond_3
    const-string/jumbo v1, "flv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 409
    const-string/jumbo v0, "video/flv"

    goto :goto_0

    .line 410
    :cond_4
    const-string/jumbo v1, "vts"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 411
    const-string/jumbo v0, "text/x-vtodo"

    goto :goto_0

    .line 412
    :cond_5
    const-string/jumbo v1, "ics"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 413
    const-string/jumbo v0, "text/x-vCalendar"

    goto :goto_0

    .line 414
    :cond_6
    const-string/jumbo v1, "webm"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 415
    const-string/jumbo v0, "video/webm"

    goto :goto_0

    .line 416
    :cond_7
    const-string/jumbo v1, "vnt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 417
    const-string/jumbo v0, "text/x-vnote"

    goto :goto_0

    .line 418
    :cond_8
    const-string/jumbo v1, "snb"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    const-string/jumbo v0, "application/snb"

    goto :goto_0

    .line 420
    :cond_9
    const-string/jumbo v1, "text/richtext"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 421
    const-string/jumbo v1, "rtx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 422
    const-string/jumbo v0, "audio/midi"

    goto :goto_0

    .line 423
    :cond_a
    const-string/jumbo v1, "text/calendar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 424
    const-string/jumbo v1, "ics"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    const-string/jumbo v0, "text/x-vCalendar"

    goto/16 :goto_0

    .line 426
    :cond_b
    const-string/jumbo v1, "rm"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    const-string/jumbo v0, "video/mp4"

    goto/16 :goto_0
.end method

.method public static getPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "a_szFullPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1043
    move-object v0, p0

    .line 1044
    .local v0, "szTmp":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 1052
    :cond_0
    :goto_0
    return-object v1

    .line 1047
    :cond_1
    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1051
    const/4 v1, 0x0

    const-string/jumbo v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1052
    goto :goto_0
.end method

.method public static getPressure(Landroid/view/MotionEvent;I)I
    .locals 10
    .param p0, "event"    # Landroid/view/MotionEvent;
    .param p1, "pos"    # I

    .prologue
    .line 1001
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v6

    if-lt p1, v6, :cond_2

    .line 1002
    :cond_0
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPressure()F

    move-result v5

    .line 1006
    .local v5, "pressure":F
    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1008
    .local v1, "maxDevicePressure":F
    :try_start_0
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x9

    if-lt v6, v7, :cond_3

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "sec_e-pen"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1009
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/view/InputDevice;->getMotionRange(I)Landroid/view/InputDevice$MotionRange;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/InputDevice$MotionRange;->getMax()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1016
    :goto_1
    const/16 v2, 0xff

    .line 1017
    .local v2, "maxPressure":I
    int-to-float v6, v2

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    add-double/2addr v6, v8

    float-to-double v8, v1

    div-double/2addr v6, v8

    double-to-float v3, v6

    .line 1019
    .local v3, "multiplier":F
    mul-float v6, v5, v3

    float-to-int v4, v6

    .line 1020
    .local v4, "nPressure":I
    if-le v4, v2, :cond_1

    .line 1021
    move v4, v2

    .line 1023
    :cond_1
    return v4

    .line 1004
    .end local v1    # "maxDevicePressure":F
    .end local v2    # "maxPressure":I
    .end local v3    # "multiplier":F
    .end local v4    # "nPressure":I
    .end local v5    # "pressure":F
    :cond_2
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v5

    .restart local v5    # "pressure":F
    goto :goto_0

    .line 1011
    .restart local v1    # "maxDevicePressure":F
    :cond_3
    move v5, v1

    goto :goto_1

    .line 1012
    :catch_0
    move-exception v0

    .line 1013
    .local v0, "e":Ljava/lang/Exception;
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_1
.end method

.method public static getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 928
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 929
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 930
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 932
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 933
    .local v7, "b2":Landroid/graphics/Bitmap;
    if-eq p0, v7, :cond_0

    .line 934
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 935
    const/4 p0, 0x0

    .line 936
    move-object p0, v7

    .line 944
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 939
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getScreenHightPixels(Landroid/app/Activity;)I
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 792
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 793
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 794
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v1
.end method

.method public static getScreenWidthPixels(Landroid/app/Activity;)I
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 786
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 787
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 788
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v1
.end method

.method public static getStatusBarHeight(Landroid/content/Context;)I
    .locals 6
    .param p0, "a_oContext"    # Landroid/content/Context;

    .prologue
    .line 1644
    const/4 v1, 0x0

    .line 1645
    .local v1, "result":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "status_bar_height"

    const-string/jumbo v4, "dimen"

    const-string/jumbo v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1646
    .local v0, "resourceId":I
    if-lez v0, :cond_0

    .line 1647
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1649
    :cond_0
    return v1
.end method

.method public static getSystemEmail(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 749
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    .line 750
    .local v4, "mgr":Landroid/accounts/AccountManager;
    invoke-virtual {v4}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 751
    .local v1, "accts":[Landroid/accounts/Account;
    array-length v2, v1

    .line 752
    .local v2, "count":I
    const/4 v0, 0x0

    .line 754
    .local v0, "acct":Landroid/accounts/Account;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 755
    aget-object v0, v1, v3

    .line 757
    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string/jumbo v6, "com.google"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 754
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 760
    :cond_0
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 763
    :goto_1
    return-object v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static getUnknownIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "drmMime"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 561
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 562
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, p1}, Lcom/infraware/common/util/Utils;->getMimeInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/infraware/common/define/CMDefine$MimeInfo;

    move-result-object v2

    .line 563
    .local v2, "mimeInfo":Lcom/infraware/common/define/CMDefine$MimeInfo;
    if-nez v2, :cond_1

    move-object v1, v3

    .line 594
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-object v1

    .line 566
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 567
    iput-object p2, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    .line 569
    :cond_2
    iget v4, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaType:I

    packed-switch v4, :pswitch_data_0

    .line 586
    iget-object v4, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->fileUri:Landroid/net/Uri;

    iget-object v5, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/high16 v5, 0x10000

    invoke-virtual {v4, v1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 588
    .local v0, "Apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_0

    move-object v1, v3

    .line 589
    goto :goto_0

    .line 574
    .end local v0    # "Apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :pswitch_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_AUDIO_ONESHOT()Z

    move-result v3

    if-nez v3, :cond_3

    iget v3, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mediaType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 576
    iget-object v3, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->fileUri:Landroid/net/Uri;

    iget-object v4, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 577
    const-string/jumbo v3, "cmp"

    const-string/jumbo v4, "com.android.music/.AudioPreview"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 581
    :cond_3
    iget-object v3, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->contentUri:Landroid/net/Uri;

    iget-object v4, v2, Lcom/infraware/common/define/CMDefine$MimeInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 569
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getVideoUriFromPath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 11
    .param p0, "a_oContext"    # Landroid/content/Context;
    .param p1, "a_szFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v10, 0x0

    .line 1076
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1077
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v10

    const-string/jumbo v3, "_data = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v10

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1081
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 1094
    :goto_0
    return-object v5

    .line 1084
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1085
    const-wide/16 v7, -0x1

    .line 1086
    .local v7, "id":J
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 1087
    const-string/jumbo v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 1088
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1090
    const/4 v9, 0x0

    .line 1091
    .local v9, "oUri":Landroid/net/Uri;
    const-wide/16 v1, -0x1

    cmp-long v1, v7, v1

    if-eqz v1, :cond_2

    .line 1092
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "content://media/external/video/media/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    :cond_2
    move-object v5, v9

    .line 1094
    goto :goto_0
.end method

.method private static hideSystemUIOnKitKat(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 1476
    invoke-static {}, Lcom/infraware/common/util/Utils;->getFlagImmersive()I

    move-result v0

    or-int/lit16 v0, v0, 0x706

    invoke-virtual {p0, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1483
    return-void
.end method

.method public static isActiveTTS(Landroid/content/Context;)Z
    .locals 9
    .param p0, "a_context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 1402
    new-instance v4, Landroid/speech/tts/TextToSpeech;

    const/4 v7, 0x0

    invoke-direct {v4, p0, v7}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    .line 1403
    .local v4, "textToSpeech":Landroid/speech/tts/TextToSpeech;
    const-string/jumbo v1, "com.samsung.SMT"

    .line 1404
    .local v1, "SAMSUNG_TTS_ENGINE_NAME":Ljava/lang/String;
    const-string/jumbo v0, "com.google.android.tts"

    .line 1406
    .local v0, "GOOGLE_TTS_ENGINE_NAME":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/speech/tts/TextToSpeech;->getEngines()Ljava/util/List;

    move-result-object v3

    .line 1408
    .local v3, "listEngine":Ljava/util/List;, "Ljava/util/List<Landroid/speech/tts/TextToSpeech$EngineInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/speech/tts/TextToSpeech$EngineInfo;

    .line 1410
    .local v5, "ttsEngine":Landroid/speech/tts/TextToSpeech$EngineInfo;
    iget-object v7, v5, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    const-string/jumbo v8, "com.google.android.tts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1417
    .end local v5    # "ttsEngine":Landroid/speech/tts/TextToSpeech$EngineInfo;
    :goto_0
    return v6

    .line 1413
    .restart local v5    # "ttsEngine":Landroid/speech/tts/TextToSpeech$EngineInfo;
    :cond_1
    iget-object v7, v5, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    const-string/jumbo v8, "com.samsung.SMT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_0

    .line 1417
    .end local v5    # "ttsEngine":Landroid/speech/tts/TextToSpeech$EngineInfo;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static isButtonEnable()Z
    .locals 1

    .prologue
    .line 803
    sget-boolean v0, Lcom/infraware/common/util/Utils;->mButtonEnable:Z

    return v0
.end method

.method public static isHoneyComb()Z
    .locals 2

    .prologue
    .line 982
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 983
    const/4 v0, 0x1

    .line 985
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isICS()Z
    .locals 2

    .prologue
    .line 958
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 959
    const/4 v0, 0x0

    .line 961
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isInstalled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 949
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 950
    const/4 v1, 0x1

    .line 953
    :goto_0
    return v1

    .line 951
    :catch_0
    move-exception v0

    .line 953
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public static isInstalledPolarisOffice(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1635
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.infraware.polarisoffice5"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1636
    const/4 v1, 0x1

    .line 1639
    :goto_0
    return v1

    .line 1637
    :catch_0
    move-exception v0

    .line 1639
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public static isIntentSafe(Ljava/lang/String;Landroid/app/Activity;)Z
    .locals 5
    .param p0, "intentAction"    # Ljava/lang/String;
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 1550
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1551
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1552
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1553
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v2, 0x1

    .line 1554
    .local v2, "isIntentSafe":Z
    :cond_0
    return v2
.end method

.method public static isJB()Z
    .locals 2

    .prologue
    .line 966
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 967
    const/4 v0, 0x0

    .line 969
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isJBP()Z
    .locals 2

    .prologue
    .line 974
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 975
    const/4 v0, 0x0

    .line 977
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isKKOver()Z
    .locals 2

    .prologue
    .line 991
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKnoxMode(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1424
    if-nez p0, :cond_1

    .line 1435
    :cond_0
    :goto_0
    return v1

    .line 1427
    :cond_1
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->CURRNT_VENDOR()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "FT03"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1430
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    const v3, 0x186a0

    div-int v0, v2, v3

    .line 1432
    .local v0, "user_id":I
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sec_container_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x64

    if-lt v0, v2, :cond_0

    .line 1433
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isL()Z
    .locals 2

    .prologue
    .line 996
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNetworkConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 713
    const-string/jumbo v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 714
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 716
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 717
    .local v1, "ni":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 718
    const/4 v2, 0x1

    .line 721
    .end local v1    # "ni":Landroid/net/NetworkInfo;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isSupportSamsungApps(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1619
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.sec.android.app.samsungapps"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1621
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v3, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_0

    .line 1629
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 1624
    :catch_0
    move-exception v0

    .line 1626
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1629
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isSystemKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 767
    sparse-switch p0, :sswitch_data_0

    .line 776
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 773
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 767
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x4f -> :sswitch_0
        0x5b -> :sswitch_0
    .end sparse-switch
.end method

.method public static isWiFiConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 726
    const-string/jumbo v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 727
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 729
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 730
    .local v1, "networkinfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-ne v3, v2, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_0

    .line 734
    .end local v1    # "networkinfo":Landroid/net/NetworkInfo;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static nonFullScreenActivity(Landroid/app/Activity;)V
    .locals 2
    .param p0, "oActivity"    # Landroid/app/Activity;

    .prologue
    .line 1471
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1472
    return-void
.end method

.method public static onLocaleChanged(Landroid/content/Context;Landroid/app/AlertDialog;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialog"    # Landroid/app/AlertDialog;

    .prologue
    .line 602
    if-nez p1, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    instance-of v2, p1, Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    .line 608
    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 611
    sget v2, Lcom/infraware/common/util/Utils;->alertTitleId:I

    if-gtz v2, :cond_2

    sget v2, Lcom/infraware/common/util/Utils;->alertMessageId:I

    if-gtz v2, :cond_2

    .line 613
    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0

    .line 617
    :cond_2
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 618
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_3

    .line 619
    const v2, 0x7f070063

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 621
    :cond_3
    sget v2, Lcom/infraware/common/util/Utils;->alertTitleId:I

    if-lez v2, :cond_4

    .line 622
    sget v2, Lcom/infraware/common/util/Utils;->alertTitleId:I

    invoke-virtual {p1, v2}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 624
    :cond_4
    sget v2, Lcom/infraware/common/util/Utils;->alertMessageId:I

    if-lez v2, :cond_0

    .line 626
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/infraware/common/util/Utils;->alertMessageId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 627
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static parseEnginePrefFromList(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "list"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1098
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1111
    :cond_0
    :goto_0
    return-object v6

    .line 1101
    :cond_1
    const-string/jumbo v7, ","

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1102
    .local v4, "prefValues":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 1103
    .local v5, "value":Ljava/lang/String;
    const/16 v7, 0x3a

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1104
    .local v1, "delimiter":I
    if-lez v1, :cond_2

    .line 1105
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1106
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1102
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static setButtonDisable()V
    .locals 2

    .prologue
    .line 808
    const/4 v1, 0x0

    sput-boolean v1, Lcom/infraware/common/util/Utils;->mButtonEnable:Z

    .line 810
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/infraware/common/util/Utils$1;

    invoke-direct {v1}, Lcom/infraware/common/util/Utils$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 823
    .local v0, "timer":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 824
    return-void
.end method

.method public static setButtonEnable()V
    .locals 1

    .prologue
    .line 828
    const/4 v0, 0x1

    sput-boolean v0, Lcom/infraware/common/util/Utils;->mButtonEnable:Z

    .line 829
    return-void
.end method

.method public static setCheckBoxAccessibility(Landroid/view/View;)V
    .locals 8
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 1517
    instance-of v4, p0, Landroid/widget/LinearLayout;

    if-eqz v4, :cond_2

    .line 1519
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Landroid/view/View;->setImportantForAccessibility(I)V

    move-object v3, p0

    .line 1520
    check-cast v3, Landroid/widget/LinearLayout;

    .line 1523
    .local v3, "linearLayout":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    .line 1524
    .local v2, "j":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 1526
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v4, v4, Landroid/widget/CheckBox;

    if-eqz v4, :cond_0

    .line 1527
    move v2, v1

    .line 1524
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1530
    :cond_1
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v4, v4, Landroid/widget/CheckBox;

    if-eqz v4, :cond_3

    .line 1531
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1535
    .local v0, "checkBox":Landroid/widget/CheckBox;
    :goto_1
    if-eqz v0, :cond_4

    .line 1536
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/infraware/common/util/Utils$3;

    invoke-direct {v5, v0, v3}, Lcom/infraware/common/util/Utils$3;-><init>(Landroid/widget/CheckBox;Landroid/widget/LinearLayout;)V

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1547
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "linearLayout":Landroid/widget/LinearLayout;
    :cond_2
    :goto_2
    return-void

    .line 1533
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "linearLayout":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "checkBox":Landroid/widget/CheckBox;
    goto :goto_1

    .line 1544
    :cond_4
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setImportantForAccessibility(I)V

    goto :goto_2
.end method

.method public static setMemoLine(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .param p0, "aContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 888
    .local p1, "aDividerViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02011a

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 889
    .local v1, "bmp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 890
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 892
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 893
    .local v3, "v":Landroid/view/View;
    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 894
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public static setScreenMode(Landroid/view/Window;I)V
    .locals 3
    .param p0, "window"    # Landroid/view/Window;
    .param p1, "nType"    # I

    .prologue
    const/16 v2, 0x800

    const/16 v1, 0x400

    .line 700
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 702
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->SHOW_LAND_INDICATOR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {p0, v2, v1}, Landroid/view/Window;->setFlags(II)V

    .line 709
    :goto_0
    return-void

    .line 705
    :cond_0
    invoke-virtual {p0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0

    .line 708
    :cond_1
    invoke-virtual {p0, v2, v1}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0
.end method

.method public static setSeparateLine(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .param p0, "aContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 879
    .local p1, "aDividerViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020119

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 880
    .local v1, "bmp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 881
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 883
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 884
    .local v3, "v":Landroid/view/View;
    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 885
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public static showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgId"    # I

    .prologue
    .line 1116
    if-gtz p3, :cond_0

    .line 1120
    :goto_0
    return-void

    .line 1119
    :cond_0
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/infraware/common/util/Utils;->showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static showActionBarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 1123
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_1

    .line 1192
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v3, p0

    .line 1127
    check-cast v3, Landroid/app/Activity;

    .line 1128
    .local v3, "a_oActivity":Landroid/app/Activity;
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v18

    if-nez v18, :cond_7

    const/4 v7, 0x1

    .line 1130
    .local v7, "isMultiWindow":Z
    :goto_1
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v17

    .line 1132
    .local v17, "toast":Landroid/widget/Toast;
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [I

    .line 1133
    .local v11, "nIconPos":[I
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v9, v0, [I

    .line 1134
    .local v9, "nActionBarPos":[I
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1135
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1138
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 1139
    .local v13, "nStatusBar":Landroid/graphics/Rect;
    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1140
    iget v0, v13, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    .line 1143
    .local v16, "statusBarTop":I
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 1144
    .local v8, "nActionBar":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1145
    iget v4, v8, Landroid/graphics/Rect;->bottom:I

    .line 1148
    .local v4, "actionBarBottom":I
    new-instance v10, Landroid/graphics/Point;

    invoke-direct {v10}, Landroid/graphics/Point;-><init>()V

    .line 1149
    .local v10, "nDisplayPoint":Landroid/graphics/Point;
    const-string/jumbo v18, "window"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/WindowManager;

    invoke-interface/range {v18 .. v18}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    .line 1150
    .local v5, "display":Landroid/view/Display;
    invoke-virtual {v5, v10}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1153
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getAppRect(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v12

    .line 1155
    .local v12, "nMultiDisplay":Landroid/graphics/Rect;
    iget v0, v10, Landroid/graphics/Point;->x:I

    move/from16 v18, v0

    const/16 v19, 0x0

    aget v19, v11, v19

    sub-int v18, v18, v19

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    sub-int v14, v18, v19

    .line 1156
    .local v14, "offsetX":I
    const/16 v18, 0x1

    aget v18, v11, v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getHeight()I

    move-result v19

    add-int v15, v18, v19

    .line 1158
    .local v15, "offsetY":I
    if-nez v7, :cond_2

    const/16 v18, 0x1

    aget v18, v9, v18

    if-lez v18, :cond_2

    .line 1159
    const/16 v18, 0x1

    aget v18, v9, v18

    sub-int v15, v15, v18

    .line 1162
    :cond_2
    const/4 v6, 0x0

    .line 1165
    .local v6, "hasStatusBar":Z
    iget v0, v10, Landroid/graphics/Point;->x:I

    move/from16 v18, v0

    iget v0, v10, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_8

    .line 1166
    if-nez v16, :cond_3

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    if-nez v18, :cond_3

    .line 1167
    const/16 v18, 0x1

    aget v18, v9, v18

    if-eqz v18, :cond_3

    .line 1168
    const/4 v6, 0x1

    .line 1171
    :cond_3
    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    .line 1172
    const/4 v6, 0x1

    .line 1180
    :cond_4
    :goto_2
    if-nez v6, :cond_5

    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v18

    if-eqz v18, :cond_9

    .line 1181
    :cond_5
    const/16 v18, 0x1

    aget v18, v11, v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getHeight()I

    move-result v19

    add-int v18, v18, v19

    invoke-static {v3}, Lcom/infraware/common/util/Utils;->getStatusBarHeight(Landroid/content/Context;)I

    move-result v19

    sub-int v15, v18, v19

    .line 1185
    :goto_3
    const/16 v18, 0x35

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v14, v15}, Landroid/widget/Toast;->setGravity(III)V

    .line 1186
    const v18, 0x7f070131

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 1187
    const/16 v18, 0x0

    aget v18, v11, v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    add-int v14, v18, v19

    .line 1188
    const/16 v18, 0x33

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v14, v15}, Landroid/widget/Toast;->setGravity(III)V

    .line 1191
    :cond_6
    invoke-virtual/range {v17 .. v17}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1128
    .end local v4    # "actionBarBottom":I
    .end local v5    # "display":Landroid/view/Display;
    .end local v6    # "hasStatusBar":Z
    .end local v7    # "isMultiWindow":Z
    .end local v8    # "nActionBar":Landroid/graphics/Rect;
    .end local v9    # "nActionBarPos":[I
    .end local v10    # "nDisplayPoint":Landroid/graphics/Point;
    .end local v11    # "nIconPos":[I
    .end local v12    # "nMultiDisplay":Landroid/graphics/Rect;
    .end local v13    # "nStatusBar":Landroid/graphics/Rect;
    .end local v14    # "offsetX":I
    .end local v15    # "offsetY":I
    .end local v16    # "statusBarTop":I
    .end local v17    # "toast":Landroid/widget/Toast;
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 1175
    .restart local v4    # "actionBarBottom":I
    .restart local v5    # "display":Landroid/view/Display;
    .restart local v6    # "hasStatusBar":Z
    .restart local v7    # "isMultiWindow":Z
    .restart local v8    # "nActionBar":Landroid/graphics/Rect;
    .restart local v9    # "nActionBarPos":[I
    .restart local v10    # "nDisplayPoint":Landroid/graphics/Point;
    .restart local v11    # "nIconPos":[I
    .restart local v12    # "nMultiDisplay":Landroid/graphics/Rect;
    .restart local v13    # "nStatusBar":Landroid/graphics/Rect;
    .restart local v14    # "offsetX":I
    .restart local v15    # "offsetY":I
    .restart local v16    # "statusBarTop":I
    .restart local v17    # "toast":Landroid/widget/Toast;
    :cond_8
    const/16 v18, 0x1

    aget v18, v9, v18

    if-eqz v18, :cond_4

    .line 1176
    const/4 v6, 0x1

    goto :goto_2

    .line 1183
    :cond_9
    add-int v15, v16, v4

    goto :goto_3
.end method

.method public static showAlert(Landroid/content/Context;II)Landroid/app/AlertDialog;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleId"    # I
    .param p2, "messageId"    # I

    .prologue
    .line 634
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    .line 635
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v2

    invoke-direct {v0, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 639
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    :goto_0
    if-lez p1, :cond_0

    .line 640
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 642
    :cond_0
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 643
    const v2, 0x7f070063

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 645
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 648
    .local v1, "dialog":Landroid/app/AlertDialog;
    if-eqz v1, :cond_1

    .line 650
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 651
    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 652
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 655
    :cond_1
    sput p1, Lcom/infraware/common/util/Utils;->alertTitleId:I

    .line 656
    sput p2, Lcom/infraware/common/util/Utils;->alertMessageId:I

    .line 658
    return-object v1

    .line 637
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_0
.end method

.method public static showAlert(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 664
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_3

    .line 665
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$I;->DIALOG_THEME()I

    move-result v3

    invoke-direct {v0, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 669
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 670
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 672
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 673
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 675
    :cond_1
    const v3, 0x7f070063

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 677
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 680
    .local v1, "dialog":Landroid/app/AlertDialog;
    if-eqz v1, :cond_2

    .line 681
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 682
    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_2

    .line 684
    :try_start_0
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    :cond_2
    :goto_1
    sput v5, Lcom/infraware/common/util/Utils;->alertTitleId:I

    .line 693
    sput v5, Lcom/infraware/common/util/Utils;->alertMessageId:I

    .line 695
    return-object v1

    .line 667
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :cond_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    goto :goto_0

    .line 686
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    :catch_0
    move-exception v2

    .line 687
    .local v2, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private showSystemUI(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1486
    const/16 v0, 0x700

    invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1490
    return-void
.end method

.method public static showTTSbarToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgId"    # I

    .prologue
    const/4 v5, 0x0

    .line 1223
    if-gtz p3, :cond_0

    .line 1236
    :goto_0
    return-void

    .line 1226
    :cond_0
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 1228
    .local v3, "toast":Landroid/widget/Toast;
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 1229
    .local v2, "pos":[I
    invoke-virtual {p2, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1231
    aget v0, v2, v5

    .line 1232
    .local v0, "offsetX":I
    const/4 v4, 0x1

    aget v4, v2, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    const/high16 v5, 0x41b80000    # 23.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    sub-int v1, v4, v5

    .line 1234
    .local v1, "offsetY":I
    const/16 v4, 0x33

    invoke-virtual {v3, v4, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1235
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static showToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgId"    # I

    .prologue
    const/4 v5, 0x0

    .line 1196
    if-gtz p3, :cond_0

    .line 1209
    :goto_0
    return-void

    .line 1199
    :cond_0
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 1201
    .local v3, "toast":Landroid/widget/Toast;
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 1202
    .local v2, "pos":[I
    invoke-virtual {p2, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1204
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    aget v5, v2, v5

    sub-int/2addr v4, v5

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v0, v4, v5

    .line 1205
    .local v0, "offsetX":I
    const/4 v4, 0x1

    aget v4, v2, v4

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    const/high16 v5, 0x42480000    # 50.0f

    invoke-static {p0, v5}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v5

    sub-int v1, v4, v5

    .line 1207
    .local v1, "offsetY":I
    const/16 v4, 0x35

    invoke-virtual {v3, v4, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1208
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static showToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;II)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgId"    # I
    .param p4, "gravity"    # I

    .prologue
    const/4 v2, 0x0

    .line 1213
    if-gtz p3, :cond_0

    .line 1219
    :goto_0
    return-void

    .line 1216
    :cond_0
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1217
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0, p4, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1218
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static showToolTipColorBar(Landroid/content/Context;Landroid/view/View;Landroid/view/View;I)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgId"    # I

    .prologue
    const/4 v6, 0x0

    .line 1559
    if-gtz p3, :cond_0

    .line 1582
    :goto_0
    return-void

    .line 1562
    :cond_0
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 1564
    .local v4, "toast":Landroid/widget/Toast;
    const/4 v5, 0x2

    new-array v2, v5, [I

    .line 1565
    .local v2, "pos":[I
    invoke-virtual {p2, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    move-object v5, p0

    .line 1567
    check-cast v5, Landroid/app/Activity;

    invoke-static {v5}, Lcom/infraware/common/util/Utils;->getScreenWidthPixels(Landroid/app/Activity;)I

    move-result v3

    .line 1569
    .local v3, "screenWidth":I
    aget v0, v2, v6

    .line 1570
    .local v0, "offsetX":I
    const/4 v5, 0x1

    aget v5, v2, v5

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    const/high16 v6, 0x42480000    # 50.0f

    invoke-static {p0, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    sub-int v1, v5, v6

    .line 1572
    .local v1, "offsetY":I
    div-int/lit8 v5, v3, 0x2

    if-le v5, v0, :cond_1

    .line 1573
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v0, v5

    .line 1579
    :goto_1
    const/16 v5, 0x33

    invoke-virtual {v4, v5, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1581
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1576
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v0, v5

    goto :goto_1
.end method

.method public static slideManagerShowToolTip(Landroid/app/Activity;Landroid/view/View;I)V
    .locals 1
    .param p0, "a_oActivity"    # Landroid/app/Activity;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "msgId"    # I

    .prologue
    .line 1240
    if-gtz p2, :cond_0

    .line 1244
    :goto_0
    return-void

    .line 1243
    :cond_0
    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/infraware/common/util/Utils;->slideManagerShowToolTip(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static slideManagerShowToolTip(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)V
    .locals 13
    .param p0, "a_oActivity"    # Landroid/app/Activity;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "msgString"    # Ljava/lang/String;

    .prologue
    .line 1247
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    if-gtz v10, :cond_1

    .line 1308
    :cond_0
    :goto_0
    return-void

    .line 1251
    :cond_1
    const/4 v10, 0x0

    invoke-static {p0, p2, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 1253
    .local v7, "toast":Landroid/widget/Toast;
    const/4 v8, 0x0

    .line 1254
    .local v8, "toastPosX":I
    const/4 v9, 0x0

    .line 1257
    .local v9, "toastPosY":I
    const/4 v5, 0x0

    .line 1258
    .local v5, "offsetX":I
    const/4 v6, 0x0

    .line 1261
    .local v6, "offsetY":I
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v10

    invoke-virtual {v10, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v10

    if-nez v10, :cond_3

    const/4 v0, 0x1

    .line 1263
    .local v0, "isMultiWindow":Z
    :goto_1
    invoke-virtual {v7}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v4

    .line 1265
    .local v4, "oView":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 1267
    const/4 v10, 0x2

    new-array v2, v10, [I

    .line 1268
    .local v2, "nIconPos":[I
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1271
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1272
    .local v3, "nStatusBar":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1275
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 1276
    .local v1, "nDisplayPoint":Landroid/graphics/Point;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1278
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/view/View;->measure(II)V

    .line 1279
    if-eqz v0, :cond_4

    .line 1280
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v11

    sub-int v6, v10, v11

    .line 1283
    iget v10, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v10

    .line 1284
    iget v10, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v10

    .line 1287
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1288
    const/high16 v10, 0x41f00000    # 30.0f

    invoke-static {p0, v10}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v10

    add-int/2addr v6, v10

    .line 1291
    :cond_2
    const/16 v10, 0x33

    const/4 v11, 0x0

    aget v11, v2, v11

    add-int/2addr v11, v5

    const/4 v12, 0x1

    aget v12, v2, v12

    sub-int/2addr v12, v6

    invoke-virtual {v7, v10, v11, v12}, Landroid/widget/Toast;->setGravity(III)V

    .line 1292
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1261
    .end local v0    # "isMultiWindow":Z
    .end local v1    # "nDisplayPoint":Landroid/graphics/Point;
    .end local v2    # "nIconPos":[I
    .end local v3    # "nStatusBar":Landroid/graphics/Rect;
    .end local v4    # "oView":Landroid/view/View;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1295
    .restart local v0    # "isMultiWindow":Z
    .restart local v1    # "nDisplayPoint":Landroid/graphics/Point;
    .restart local v2    # "nIconPos":[I
    .restart local v3    # "nStatusBar":Landroid/graphics/Rect;
    .restart local v4    # "oView":Landroid/view/View;
    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 1297
    const/4 v10, 0x0

    aget v10, v2, v10

    add-int/2addr v10, v5

    iget v11, v3, Landroid/graphics/Rect;->left:I

    add-int v8, v10, v11

    .line 1298
    const/4 v10, 0x1

    aget v10, v2, v10

    sub-int/2addr v10, v6

    iget v11, v3, Landroid/graphics/Rect;->top:I

    sub-int v9, v10, v11

    .line 1300
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v9

    iget v11, v1, Landroid/graphics/Point;->y:I

    if-le v10, v11, :cond_5

    .line 1301
    const/4 v10, 0x1

    aget v10, v2, v10

    sub-int/2addr v10, v6

    iget v11, v3, Landroid/graphics/Rect;->top:I

    add-int v9, v10, v11

    .line 1304
    :cond_5
    const/16 v10, 0x33

    invoke-virtual {v7, v10, v8, v9}, Landroid/widget/Toast;->setGravity(III)V

    .line 1305
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method public static slideshowToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ILjava/lang/Boolean;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgId"    # I
    .param p4, "portait"    # Ljava/lang/Boolean;

    .prologue
    .line 1312
    if-gtz p3, :cond_0

    .line 1316
    :goto_0
    return-void

    .line 1315
    :cond_0
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0, p4}, Lcom/infraware/common/util/Utils;->slideshowToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public static slideshowToolTip(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "msgString"    # Ljava/lang/String;
    .param p4, "portait"    # Ljava/lang/Boolean;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1320
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_1

    .line 1342
    :cond_0
    :goto_0
    return-void

    .line 1323
    :cond_1
    invoke-static {p0, p3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 1325
    .local v4, "toast":Landroid/widget/Toast;
    const/4 v5, 0x2

    new-array v3, v5, [I

    .line 1326
    .local v3, "pos":[I
    const/4 v0, 0x0

    .line 1327
    .local v0, "nHeight":I
    invoke-virtual {p2, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1329
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-ne v5, v7, :cond_2

    .line 1331
    const/16 v0, 0x19

    .line 1337
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    aget v6, v3, v6

    sub-int/2addr v5, v6

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v1, v5, v6

    .line 1338
    .local v1, "offsetX":I
    aget v5, v3, v7

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v6, v0

    invoke-static {p0, v6}, Lcom/infraware/common/util/Utils;->dipToPixel(Landroid/content/Context;F)I

    move-result v6

    sub-int v2, v5, v6

    .line 1340
    .local v2, "offsetY":I
    const/16 v5, 0x35

    invoke-virtual {v4, v5, v1, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1341
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1335
    .end local v1    # "offsetX":I
    .end local v2    # "offsetY":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static startFocusTracker(Ljava/lang/String;Landroid/content/Context;JJ)V
    .locals 7
    .param p0, "logTag"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "count"    # J
    .param p4, "interval"    # J

    .prologue
    .line 1439
    new-instance v0, Lcom/infraware/common/util/Utils$2;

    move-wide v1, p2

    move-wide v3, p4

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/infraware/common/util/Utils$2;-><init>(JJLjava/lang/String;Landroid/content/Context;)V

    .line 1457
    .local v0, "cdt":Landroid/os/CountDownTimer;
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 1458
    return-void
.end method

.method public static unbindDrawables(Landroid/view/View;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 832
    if-nez p0, :cond_1

    .line 852
    .end local p0    # "view":Landroid/view/View;
    .local v0, "e":Ljava/lang/Exception;
    .local v1, "i":I
    :cond_0
    :goto_0
    return-void

    .line 835
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i":I
    .restart local p0    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 836
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 839
    :cond_2
    instance-of v2, p0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 840
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_3

    move-object v2, p0

    .line 841
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/Utils;->unbindDrawables(Landroid/view/View;)V

    .line 840
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 844
    :cond_3
    instance-of v2, p0, Landroid/widget/AdapterView;

    if-nez v2, :cond_0

    .line 846
    :try_start_0
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "view":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 847
    :catch_0
    move-exception v0

    .line 848
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
