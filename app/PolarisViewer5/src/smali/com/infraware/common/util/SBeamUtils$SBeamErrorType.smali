.class public final enum Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;
.super Ljava/lang/Enum;
.source "SBeamUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/util/SBeamUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SBeamErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

.field public static final enum ERROR_DRM_FILE:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

.field public static final enum ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

.field public static final enum ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    new-instance v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    const-string/jumbo v1, "ERROR_SBEAM_OFF"

    invoke-direct {v0, v1, v2}, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    .line 76
    new-instance v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    const-string/jumbo v1, "ERROR_FILE_NOT_SELECTED"

    invoke-direct {v0, v1, v3}, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    .line 77
    new-instance v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    const-string/jumbo v1, "ERROR_DRM_FILE"

    invoke-direct {v0, v1, v4}, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_DRM_FILE:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    sget-object v1, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_SBEAM_OFF:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_FILE_NOT_SELECTED:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ERROR_DRM_FILE:Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->$VALUES:[Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    return-object v0
.end method

.method public static values()[Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->$VALUES:[Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    invoke-virtual {v0}, [Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    return-object v0
.end method
