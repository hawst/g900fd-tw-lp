.class Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;
.super Ljava/lang/Object;
.source "SbeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/util/SbeamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SbeamPushCompleteCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/util/SbeamHelper;


# direct methods
.method constructor <init>(Lcom/infraware/common/util/SbeamHelper;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 5
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v4, 0x0

    .line 247
    const-string/jumbo v1, "[SBeam]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SbeamPushCompleteCallback ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v3}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v2}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 250
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v1, v4}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    .line 260
    :goto_0
    return-void

    .line 254
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 256
    .local v0, "i":Landroid/content/Intent;
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257
    const-string/jumbo v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    :cond_1
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 259
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v1, v4}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto :goto_0
.end method
