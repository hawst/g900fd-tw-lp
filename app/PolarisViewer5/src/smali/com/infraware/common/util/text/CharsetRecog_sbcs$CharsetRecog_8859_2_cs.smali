.class Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;
.super Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2;
.source "CharsetRecog_sbcs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/util/text/CharsetRecog_sbcs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CharsetRecog_8859_2_cs"
.end annotation


# static fields
.field private static ngrams:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 473
    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;->ngrams:[I

    return-void

    :array_0
    .array-data 4
        0x206120
        0x206279
        0x20646f
        0x206a65
        0x206e61
        0x206e65
        0x206f20
        0x206f64
        0x20706f
        0x207072
        0x2070f8
        0x20726f
        0x207365
        0x20736f
        0x207374
        0x20746f
        0x207620
        0x207679
        0x207a61
        0x612070
        0x636520
        0x636820
        0x652070
        0x652073
        0x652076
        0x656d20
        0x656eed
        0x686f20
        0x686f64
        0x697374
        0x6a6520
        0x6b7465
        0x6c6520
        0x6c6920
        0x6e6120
        0x6ee920
        0x6eec20
        0x6eed20
        0x6f2070
        0x6f646e
        0x6f6a69
        0x6f7374
        0x6f7520
        0x6f7661
        0x706f64
        0x706f6a
        0x70726f
        0x70f865
        0x736520
        0x736f75
        0x737461
        0x737469
        0x73746e
        0x746572
        0x746eed
        0x746f20
        0x752070
        0xbe6520
        0xe16eed
        0xe9686f
        0xed2070
        0xed2073
        0xed6d20
        0xf86564
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2;-><init>()V

    return-void
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 482
    const-string/jumbo v0, "cs"

    return-object v0
.end method

.method public match(Lcom/infraware/common/util/text/CharsetDetector;)I
    .locals 2
    .param p1, "det"    # Lcom/infraware/common/util/text/CharsetDetector;

    .prologue
    .line 487
    sget-object v0, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;->ngrams:[I

    sget-object v1, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;->byteMap:[B

    invoke-virtual {p0, p1, v0, v1}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;->match(Lcom/infraware/common/util/text/CharsetDetector;[I[B)I

    move-result v0

    return v0
.end method
