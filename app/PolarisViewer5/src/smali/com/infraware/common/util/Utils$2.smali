.class final Lcom/infraware/common/util/Utils$2;
.super Landroid/os/CountDownTimer;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/common/util/Utils;->startFocusTracker(Ljava/lang/String;Landroid/content/Context;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$logTag:Ljava/lang/String;


# direct methods
.method constructor <init>(JJLjava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # J
    .param p3, "x1"    # J

    .prologue
    .line 1439
    iput-object p5, p0, Lcom/infraware/common/util/Utils$2;->val$logTag:Ljava/lang/String;

    iput-object p6, p0, Lcom/infraware/common/util/Utils$2;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/infraware/common/util/Utils$2;->val$logTag:Ljava/lang/String;

    const-string/jumbo v1, "FocusTracker is terminated."

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    iget-object v0, p0, Lcom/infraware/common/util/Utils$2;->val$logTag:Ljava/lang/String;

    const-string/jumbo v1, "=============================="

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    return-void
.end method

.method public onTick(J)V
    .locals 4
    .param p1, "arg0"    # J

    .prologue
    .line 1448
    iget-object v1, p0, Lcom/infraware/common/util/Utils$2;->val$context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1449
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1450
    iget-object v1, p0, Lcom/infraware/common/util/Utils$2;->val$logTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms remain... current view is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454
    :goto_0
    return-void

    .line 1452
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/util/Utils$2;->val$logTag:Ljava/lang/String;

    const-string/jumbo v2, "View is null"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
