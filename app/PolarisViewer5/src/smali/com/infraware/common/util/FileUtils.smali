.class public abstract Lcom/infraware/common/util/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# static fields
.field public static m_oStorageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addPathDelemeter(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 80
    if-eqz p0, :cond_0

    const-string/jumbo v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 83
    :cond_0
    return-object p0
.end method

.method private static changePermissons(Ljava/io/File;I)I
    .locals 10
    .param p0, "path"    # Ljava/io/File;
    .param p1, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 887
    const-string/jumbo v2, "android.os.FileUtils"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 888
    .local v0, "fileUtils":Ljava/lang/Class;
    const-string/jumbo v2, "setPermissions"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v8

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v9

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 889
    .local v1, "setPermissions":Ljava/lang/reflect/Method;
    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    return v2
.end method

.method public static changePermissons(Ljava/io/File;IZ)V
    .locals 5
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "mode"    # I
    .param p2, "bRecursive"    # Z

    .prologue
    .line 861
    if-nez p0, :cond_1

    .line 884
    :cond_0
    :goto_0
    return-void

    .line 864
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 869
    :try_start_0
    invoke-static {p0, p1}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 873
    :goto_1
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 878
    :try_start_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 880
    .local v1, "file":Ljava/io/File;
    const/4 v4, 0x1

    invoke-static {v1, p1, v4}, Lcom/infraware/common/util/FileUtils;->changePermissons(Ljava/io/File;IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 878
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 871
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catch_0
    move-exception v4

    goto :goto_1

    .line 883
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public static convertFileNameToValid(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 517
    const-string/jumbo v1, ""

    .line 518
    .local v1, "newFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 520
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 535
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 518
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 523
    :sswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 524
    goto :goto_1

    .line 532
    :sswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 533
    goto :goto_1

    .line 540
    :cond_0
    return-object v1

    .line 520
    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_1
        0x2a -> :sswitch_1
        0x3a -> :sswitch_1
        0x3c -> :sswitch_1
        0x3e -> :sswitch_1
        0x3f -> :sswitch_1
        0x5c -> :sswitch_0
        0x7c -> :sswitch_1
    .end sparse-switch
.end method

.method public static createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "customPath"    # Ljava/lang/String;
    .param p1, "dirName"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 806
    const/4 v0, 0x0

    .line 807
    .local v0, "tempPath":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 808
    invoke-static {p0, v2, p3}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 810
    :cond_0
    if-nez v0, :cond_1

    .line 811
    const/4 v1, 0x0

    invoke-static {p1, v1, p3}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 812
    invoke-static {p2, v2, p3}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 813
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->SDROOT_PATH:Ljava/lang/String;

    .line 815
    :cond_1
    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->addPathDelemeter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 817
    return-object v0
.end method

.method public static decideFileName(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ext"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 435
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 437
    .local v2, "prefix":Ljava/lang/String;
    const-string/jumbo v3, ".doc"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, ".docx"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 438
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 448
    :cond_1
    :goto_0
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    .local v1, "newfile":Ljava/io/File;
    const/4 v0, 0x0

    .line 451
    .local v0, "index":I
    :goto_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 452
    add-int/lit8 v0, v0, 0x1

    .line 453
    new-instance v1, Ljava/io/File;

    .end local v1    # "newfile":Ljava/io/File;
    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v1    # "newfile":Ljava/io/File;
    goto :goto_1

    .line 439
    .end local v0    # "index":I
    .end local v1    # "newfile":Ljava/io/File;
    :cond_2
    const-string/jumbo v3, ".hwp"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 440
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 441
    :cond_3
    const-string/jumbo v3, ".xls"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, ".xlsx"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 442
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 443
    :cond_5
    const-string/jumbo v3, ".ppt"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, ".pptx"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 444
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 445
    :cond_7
    const-string/jumbo v3, ".txt"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 446
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 456
    .restart local v0    # "index":I
    .restart local v1    # "newfile":Ljava/io/File;
    :cond_8
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static deleteAllFilesInDirectory(Ljava/lang/String;)V
    .locals 6
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 298
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 300
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 311
    :cond_0
    return-void

    .line 303
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 304
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 306
    .local v2, "files":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 307
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-char v5, Ljava/io/File;->separatorChar:C

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v2, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 308
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 306
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static deleteDirectory(Ljava/io/File;Z)V
    .locals 6
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "bDeleteDir"    # Z

    .prologue
    .line 315
    if-nez p0, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 321
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 323
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v1, v0, v2

    .line 324
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 325
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/infraware/polarisoffice5/text/manager/TEConstant$Path;->PATH_BOOKMARKDATA:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 323
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 327
    :cond_2
    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 331
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catch_0
    move-exception v4

    .line 333
    :cond_3
    if-eqz p1, :cond_0

    .line 334
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 330
    .restart local v0    # "arr$":[Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_4
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public static deleteDirectory(Ljava/lang/String;Z)V
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "bDeleteDir"    # Z

    .prologue
    .line 340
    if-nez p0, :cond_0

    .line 345
    :goto_0
    return-void

    .line 343
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 344
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0, p1}, Lcom/infraware/common/util/FileUtils;->deleteDirectory(Ljava/io/File;Z)V

    goto :goto_0
.end method

.method public static getArrayChildFileName(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "parentPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 349
    .local v5, "parent":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 351
    .local v1, "arrFileName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v2, v0, v3

    .line 352
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 353
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 357
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "arrFileName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    const/4 v1, 0x0

    :cond_2
    return-object v1
.end method

.method public static getAvailableExternalStoragePath()Ljava/lang/String;
    .locals 7

    .prologue
    .line 822
    const/4 v4, 0x0

    .line 823
    .local v4, "sdCard":Ljava/io/File;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-static {}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getCount()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 825
    new-instance v4, Ljava/io/File;

    .end local v4    # "sdCard":Ljava/io/File;
    invoke-static {v3}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getFolderPath(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 826
    .restart local v4    # "sdCard":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 823
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 829
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 830
    .local v2, "fileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 833
    invoke-static {v3}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getFolderPath(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/FileUtils;->getFreeSize(Ljava/lang/String;)J

    move-result-wide v0

    .line 834
    .local v0, "byteAvailable":J
    const-wide/32 v5, 0x100000

    cmp-long v5, v0, v5

    if-ltz v5, :cond_0

    .line 837
    invoke-static {v3}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getFolderPath(I)Ljava/lang/String;

    move-result-object v5

    .line 840
    .end local v0    # "byteAvailable":J
    .end local v2    # "fileList":[Ljava/io/File;
    :goto_1
    return-object v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static getAvailableInternalStoragePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 844
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/FileUtils;->getFreeSize(Ljava/lang/String;)J

    move-result-wide v0

    .line 845
    .local v0, "byteAvailable":J
    const-wide/32 v2, 0x100000

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 846
    const/4 v2, 0x0

    .line 847
    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getDateString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # J

    .prologue
    .line 461
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 462
    .local v0, "dateFormat":Ljava/text/DateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 463
    .local v1, "dateTime":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getFileExt(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "a_strFullPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-static {p0}, Lcom/infraware/common/util/FileUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "strFileName":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-object v2

    .line 71
    :cond_1
    const/16 v3, 0x2e

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 72
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 75
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getFileExtension(IZ)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I
    .param p1, "isDot"    # Z

    .prologue
    .line 362
    const/4 v0, 0x0

    .line 363
    .local v0, "ext":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 378
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 381
    :cond_0
    return-object v0

    .line 365
    :pswitch_1
    const-string/jumbo v0, "doc"

    goto :goto_0

    .line 366
    :pswitch_2
    const-string/jumbo v0, "ppt"

    goto :goto_0

    .line 367
    :pswitch_3
    const-string/jumbo v0, "hwp"

    goto :goto_0

    .line 368
    :pswitch_4
    const-string/jumbo v0, "xls"

    goto :goto_0

    .line 369
    :pswitch_5
    const-string/jumbo v0, "docx"

    goto :goto_0

    .line 370
    :pswitch_6
    const-string/jumbo v0, "pptx"

    goto :goto_0

    .line 371
    :pswitch_7
    const-string/jumbo v0, "xlsx"

    goto :goto_0

    .line 372
    :pswitch_8
    const-string/jumbo v0, "pdf"

    goto :goto_0

    .line 373
    :pswitch_9
    const-string/jumbo v0, "txt"

    goto :goto_0

    .line 374
    :pswitch_a
    const-string/jumbo v0, "pps"

    goto :goto_0

    .line 375
    :pswitch_b
    const-string/jumbo v0, "ppsx"

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "fullPath"    # Ljava/lang/String;

    .prologue
    .line 41
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 48
    .end local p0    # "fullPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 44
    .restart local p0    # "fullPath":Ljava/lang/String;
    :cond_1
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 45
    .local v0, "nIndex":I
    if-ltz v0, :cond_0

    .line 46
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getFileNameWithoutExt(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "a_strFilename"    # Ljava/lang/String;

    .prologue
    .line 53
    move-object v1, p0

    .line 54
    .local v1, "strFileName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 61
    .end local v1    # "strFileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 57
    .restart local v1    # "strFileName":Ljava/lang/String;
    :cond_1
    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 58
    .local v0, "nIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 61
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFileSize(Ljava/lang/String;)J
    .locals 7
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 719
    const-wide/16 v3, 0x0

    .line 720
    .local v3, "result":J
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 722
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    .line 723
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    .line 735
    :goto_0
    return-wide v5

    .line 725
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 727
    .local v1, "fileList":[Ljava/io/File;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v1

    if-ge v2, v5, :cond_2

    .line 729
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 730
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/infraware/common/util/FileUtils;->getFileSize(Ljava/lang/String;)J

    move-result-wide v5

    add-long/2addr v3, v5

    .line 727
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 732
    :cond_1
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_2

    :cond_2
    move-wide v5, v3

    .line 735
    goto :goto_0
.end method

.method public static getFreeBlock(Ljava/lang/String;)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 695
    const/4 v0, 0x0

    .line 697
    .local v0, "freeBlock":I
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 698
    .local v1, "sf":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 702
    .end local v1    # "sf":Landroid/os/StatFs;
    :goto_0
    return v0

    .line 699
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static getFreeSize(Ljava/lang/String;)J
    .locals 7
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 707
    const-wide/16 v0, 0x0

    .line 709
    .local v0, "freeSize":J
    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 710
    .local v2, "sf":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v5, v5

    mul-long v0, v3, v5

    .line 714
    .end local v2    # "sf":Landroid/os/StatFs;
    :goto_0
    return-wide v0

    .line 711
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public static getMicroSDCardDirectory()Ljava/lang/String;
    .locals 8

    .prologue
    .line 122
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->readMountsFile()Ljava/util/List;

    move-result-object v2

    .line 123
    .local v2, "mMounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->readVoldFile()Ljava/util/List;

    move-result-object v3

    .line 125
    .local v3, "mVold":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_4

    .line 126
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 128
    .local v4, "mount":Ljava/lang/String;
    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 129
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 125
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 134
    .local v5, "root":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_3

    .line 135
    :cond_2
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 136
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 139
    :cond_3
    invoke-static {v4}, Lcom/infraware/common/util/FileUtils;->isAvailableFileSystem(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 140
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "i":I
    .restart local v1    # "i":I
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 141
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 145
    .end local v4    # "mount":Ljava/lang/String;
    .end local v5    # "root":Ljava/io/File;
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_5

    .line 146
    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 149
    :goto_2
    return-object v6

    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public static getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ext"    # Ljava/lang/String;

    .prologue
    .line 426
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 427
    .local v0, "type":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 428
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->getExtraMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    :cond_0
    return-object v0
.end method

.method public static getRealPathFromURI(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 10
    .param p0, "contentUri"    # Landroid/net/Uri;
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 926
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 927
    const/4 v7, 0x0

    .line 929
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "_data"

    aput-object v1, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 930
    const-string/jumbo v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 931
    .local v6, "column_index":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 932
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 933
    .local v9, "str":Ljava/lang/String;
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 934
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 935
    const/4 v7, 0x0

    .line 947
    .end local v6    # "column_index":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v9    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v9

    .line 939
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 940
    .local v8, "e":Ljava/lang/Exception;
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 941
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 947
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_1
    const-string/jumbo v9, ""

    goto :goto_0
.end method

.method public static getSizeString(J)Ljava/lang/String;
    .locals 15
    .param p0, "size"    # J

    .prologue
    .line 470
    const-string/jumbo v7, "GB"

    .local v7, "strGiga":Ljava/lang/String;
    const-string/jumbo v9, "MB"

    .local v9, "strMega":Ljava/lang/String;
    const-string/jumbo v8, "KB"

    .local v8, "strKilo":Ljava/lang/String;
    const-string/jumbo v6, "Bytes"

    .line 471
    .local v6, "strBytes":Ljava/lang/String;
    const/high16 v1, 0x4e800000

    .local v1, "nGiga":F
    const/high16 v4, 0x49800000    # 1048576.0f

    .local v4, "nMega":F
    const/high16 v2, 0x44800000    # 1024.0f

    .line 472
    .local v2, "nKilo":F
    const/4 v5, 0x0

    .line 474
    .local v5, "nUnitSize":F
    const-wide/16 v11, 0x0

    cmp-long v11, p0, v11

    if-gez v11, :cond_0

    .line 475
    const/4 v0, 0x0

    .line 512
    :goto_0
    return-object v0

    .line 477
    :cond_0
    long-to-float v11, p0

    cmpl-float v11, v11, v1

    if-ltz v11, :cond_1

    .line 479
    long-to-float v11, p0

    div-float v5, v11, v1

    .line 480
    move-object v10, v7

    .line 498
    .local v10, "strUnit":Ljava/lang/String;
    :goto_1
    long-to-float v11, p0

    cmpl-float v11, v11, v2

    if-ltz v11, :cond_4

    float-to-int v11, v5

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v3

    .line 499
    .local v3, "nLen":I
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 508
    const-string/jumbo v11, "%d%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    float-to-int v14, v5

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v10, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .local v0, "StrBuf":Ljava/lang/String;
    goto :goto_0

    .line 482
    .end local v0    # "StrBuf":Ljava/lang/String;
    .end local v3    # "nLen":I
    .end local v10    # "strUnit":Ljava/lang/String;
    :cond_1
    long-to-float v11, p0

    cmpl-float v11, v11, v4

    if-ltz v11, :cond_2

    .line 484
    long-to-float v11, p0

    div-float v5, v11, v4

    .line 485
    move-object v10, v9

    .restart local v10    # "strUnit":Ljava/lang/String;
    goto :goto_1

    .line 487
    .end local v10    # "strUnit":Ljava/lang/String;
    :cond_2
    long-to-float v11, p0

    cmpl-float v11, v11, v2

    if-ltz v11, :cond_3

    .line 489
    long-to-float v11, p0

    div-float v5, v11, v2

    .line 490
    move-object v10, v8

    .restart local v10    # "strUnit":Ljava/lang/String;
    goto :goto_1

    .line 494
    .end local v10    # "strUnit":Ljava/lang/String;
    :cond_3
    long-to-float v5, p0

    .line 495
    move-object v10, v6

    .restart local v10    # "strUnit":Ljava/lang/String;
    goto :goto_1

    .line 498
    :cond_4
    const/4 v3, -0x1

    goto :goto_2

    .line 502
    .restart local v3    # "nLen":I
    :pswitch_0
    const-string/jumbo v11, "%.2f%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v10, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 503
    .restart local v0    # "StrBuf":Ljava/lang/String;
    goto :goto_0

    .line 505
    .end local v0    # "StrBuf":Ljava/lang/String;
    :pswitch_1
    const-string/jumbo v11, "%.1f%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v10, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 506
    .restart local v0    # "StrBuf":Ljava/lang/String;
    goto :goto_0

    .line 499
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getTextFromFile(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "maxLength"    # I

    .prologue
    const/4 v11, 0x0

    .line 545
    const/4 v10, 0x0

    .line 547
    .local v10, "text":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 548
    .local v3, "file":Ljava/io/File;
    new-array v4, p2, [B

    .line 549
    .local v4, "fileBuffer":[B
    const/4 v9, 0x0

    .line 550
    .local v9, "size":I
    const/4 v5, 0x0

    .line 552
    .local v5, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 553
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .local v6, "inputStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v6, v4}, Ljava/io/InputStream;->read([B)I

    move-result v9

    .line 554
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 561
    const/16 v11, 0x50

    if-ge v9, v11, :cond_1

    .line 564
    :try_start_2
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->DEFAULT_CHAR_SET()Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_0

    .line 565
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget-object v11, v11, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v11}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v7

    .line 566
    .local v7, "locale":I
    invoke-static {v7}, Lcom/infraware/common/util/Utils;->getANSICharSet(I)Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "charSet":Ljava/lang/String;
    new-instance v10, Ljava/lang/String;

    .end local v10    # "text":Ljava/lang/String;
    invoke-direct {v10, v4, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .end local v0    # "charSet":Ljava/lang/String;
    .end local v7    # "locale":I
    .restart local v10    # "text":Ljava/lang/String;
    :goto_0
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    move-object v11, v10

    .line 587
    :goto_1
    return-object v11

    .line 555
    :catch_0
    move-exception v2

    .line 556
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_2
    goto :goto_1

    .line 557
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 558
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    goto :goto_1

    .line 570
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :cond_0
    new-instance v10, Ljava/lang/String;

    .end local v10    # "text":Ljava/lang/String;
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$S;->DEFAULT_CHAR_SET()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v4, v11}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .restart local v10    # "text":Ljava/lang/String;
    goto :goto_0

    .line 572
    .end local v10    # "text":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 573
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v10, 0x0

    .line 574
    .restart local v10    # "text":Ljava/lang/String;
    goto :goto_0

    .line 578
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_1
    new-instance v1, Lcom/infraware/common/util/text/CharsetDetector;

    invoke-direct {v1}, Lcom/infraware/common/util/text/CharsetDetector;-><init>()V

    .line 579
    .local v1, "detector":Lcom/infraware/common/util/text/CharsetDetector;
    invoke-virtual {v1, v4}, Lcom/infraware/common/util/text/CharsetDetector;->setText([B)Lcom/infraware/common/util/text/CharsetDetector;

    .line 581
    :try_start_3
    invoke-virtual {v1}, Lcom/infraware/common/util/text/CharsetDetector;->detect()Lcom/infraware/common/util/text/CharsetMatch;

    move-result-object v8

    .line 582
    .local v8, "match":Lcom/infraware/common/util/text/CharsetMatch;
    invoke-virtual {v8, p2}, Lcom/infraware/common/util/text/CharsetMatch;->getString(I)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v10

    goto :goto_0

    .line 583
    .end local v8    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :catch_3
    move-exception v2

    .line 584
    .local v2, "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    goto :goto_0

    .line 557
    .end local v1    # "detector":Lcom/infraware/common/util/text/CharsetDetector;
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .line 555
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :catch_5
    move-exception v2

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v5    # "inputStream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public static getTypeByExt(Ljava/lang/String;)I
    .locals 2
    .param p0, "ext"    # Ljava/lang/String;

    .prologue
    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "type":I
    const-string/jumbo v1, "doc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 388
    const/4 v0, 0x1

    .line 406
    :cond_0
    :goto_0
    return v0

    .line 389
    :cond_1
    const-string/jumbo v1, "ppt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 390
    const/4 v0, 0x2

    goto :goto_0

    .line 391
    :cond_2
    const-string/jumbo v1, "hwp"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 392
    const/16 v0, 0x9

    goto :goto_0

    .line 393
    :cond_3
    const-string/jumbo v1, "xls"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 394
    const/4 v0, 0x3

    goto :goto_0

    .line 395
    :cond_4
    const-string/jumbo v1, "docx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 396
    const/4 v0, 0x4

    goto :goto_0

    .line 397
    :cond_5
    const-string/jumbo v1, "pptx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 398
    const/4 v0, 0x5

    goto :goto_0

    .line 399
    :cond_6
    const-string/jumbo v1, "xlsx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 400
    const/4 v0, 0x6

    goto :goto_0

    .line 401
    :cond_7
    const-string/jumbo v1, "pdf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 402
    const/4 v0, 0x7

    goto :goto_0

    .line 403
    :cond_8
    const-string/jumbo v1, "txt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 404
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static getTypeByFileName(Ljava/lang/String;)I
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 411
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 412
    :cond_0
    const/4 v2, 0x0

    .line 421
    :goto_0
    return v2

    .line 414
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 416
    .local v1, "idx_ext":I
    if-gez v1, :cond_2

    .line 417
    const-string/jumbo v0, ""

    .line 421
    .local v0, "ext":Ljava/lang/String;
    :goto_1
    invoke-static {v0}, Lcom/infraware/common/util/FileUtils;->getTypeByExt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 419
    .end local v0    # "ext":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "ext":Ljava/lang/String;
    goto :goto_1
.end method

.method public static getTypeSelection(I)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 740
    const/4 v0, 0x0

    .line 741
    .local v0, "strSelection":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 801
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 744
    :pswitch_1
    const-string/jumbo v0, "_data LIKE \'%.doc\' or _data LIKE \'%.docx\'"

    .line 745
    const-string/jumbo v1, "dot"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 746
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.dot\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 747
    :cond_1
    const-string/jumbo v1, "dotx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 748
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.dotx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 751
    :pswitch_2
    const-string/jumbo v0, "_data LIKE \'%.xls\' or _data LIKE \'%.xlsx\'"

    .line 752
    const-string/jumbo v1, "xlt"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 753
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.xlt\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 754
    :cond_2
    const-string/jumbo v1, "xltx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 755
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.xltx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 758
    :pswitch_3
    const-string/jumbo v0, "_data LIKE \'%.ppt\' or _data LIKE \'%.pptx\'"

    .line 759
    const-string/jumbo v1, "pot"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 760
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.pot\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 761
    :cond_3
    const-string/jumbo v1, "potx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 762
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.potx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 763
    :cond_4
    const-string/jumbo v1, "pps"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 764
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.pps\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 765
    :cond_5
    const-string/jumbo v1, "ppsx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 766
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.ppsx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 769
    :pswitch_4
    const-string/jumbo v0, "_data LIKE \'%.pdf\'"

    .line 770
    goto/16 :goto_0

    .line 772
    :pswitch_5
    const-string/jumbo v0, "_data LIKE \'%.txt\'"

    .line 773
    goto/16 :goto_0

    .line 775
    :pswitch_6
    const-string/jumbo v0, "_data LIKE \'%.doc\' or _data LIKE \'%.docx\'"

    .line 776
    const-string/jumbo v1, "dot"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 777
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.dot\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 778
    :cond_6
    const-string/jumbo v1, "dotx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 779
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.dotx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 781
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.xls\' or _data LIKE \'%.xlsx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 782
    const-string/jumbo v1, "xlt"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 783
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.xlt\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 784
    :cond_8
    const-string/jumbo v1, "xltx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 785
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.xltx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 787
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.ppt\' or _data LIKE \'%.pptx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 788
    const-string/jumbo v1, "pot"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 789
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.pot\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 790
    :cond_a
    const-string/jumbo v1, "potx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 791
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.potx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 792
    :cond_b
    const-string/jumbo v1, "pps"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 793
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.pps\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794
    :cond_c
    const-string/jumbo v1, "ppsx"

    invoke-static {v1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 795
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.ppsx\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 797
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.pdf\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 798
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or _data LIKE \'%.txt\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 741
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public static hasChildDirectory(Ljava/io/File;Z)Z
    .locals 7
    .param p0, "file"    # Ljava/io/File;
    .param p1, "isShowHidden"    # Z

    .prologue
    const/4 v5, 0x0

    .line 88
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 89
    .local v2, "childList":[Ljava/io/File;
    if-nez v2, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v5

    .line 92
    :cond_1
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 94
    .local v1, "childFile":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 97
    if-nez p1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->isHidden()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 92
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 100
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 101
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public static initStorageList(Landroid/content/Context;)V
    .locals 2
    .param p0, "oContext"    # Landroid/content/Context;

    .prologue
    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    .line 110
    sget-object v0, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-static {}, Lcom/infraware/common/util/FileUtils;->getMicroSDCardDirectory()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    sget-object v0, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    invoke-static {}, Lcom/infraware/common/util/FileUtils;->getMicroSDCardDirectory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    if-eqz p0, :cond_1

    .line 117
    sget-object v0, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    invoke-static {p0}, Lcom/infraware/filemanager/porting/DeviceConfig$ExternalSD;->getUSBMountList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 119
    :cond_1
    return-void
.end method

.method private static isAvailableFileSystem(Ljava/lang/String;)Z
    .locals 9
    .param p0, "fileSystemName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 153
    const/16 v7, 0x8

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v7, "/dev"

    aput-object v7, v4, v5

    const-string/jumbo v7, "/mnt/asec"

    aput-object v7, v4, v6

    const/4 v7, 0x2

    const-string/jumbo v8, "/mnt/obb"

    aput-object v8, v4, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "/system"

    aput-object v8, v4, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "/data"

    aput-object v8, v4, v7

    const/4 v7, 0x5

    const-string/jumbo v8, "/cache"

    aput-object v8, v4, v7

    const/4 v7, 0x6

    const-string/jumbo v8, "/efs"

    aput-object v8, v4, v7

    const/4 v7, 0x7

    const-string/jumbo v8, "/firmware"

    aput-object v8, v4, v7

    .line 155
    .local v4, "unAvailableFileSystemList":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 156
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-ne v7, v6, :cond_1

    .line 165
    .end local v3    # "name":Ljava/lang/String;
    :cond_0
    :goto_1
    return v5

    .line 155
    .restart local v3    # "name":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    .end local v3    # "name":Ljava/lang/String;
    :cond_2
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eq v7, v6, :cond_0

    move v5, v6

    .line 165
    goto :goto_1
.end method

.method public static isDirectory(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 256
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 258
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 261
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDocumentType(ILjava/lang/String;)Z
    .locals 2
    .param p0, "type"    # I
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 644
    packed-switch p0, :pswitch_data_0

    .line 680
    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    .line 647
    :pswitch_1
    const-string/jumbo v1, "doc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "docx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 650
    const-string/jumbo v0, "dot"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "dotx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 651
    :cond_2
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 655
    :pswitch_2
    const-string/jumbo v0, "hwp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 656
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 661
    :pswitch_3
    const-string/jumbo v1, "xls"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "xlsx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 664
    const-string/jumbo v0, "xlt"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "xltx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 665
    :cond_3
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 669
    :pswitch_4
    const-string/jumbo v1, "ppt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "pptx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 672
    const-string/jumbo v0, "pps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "ppsx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 673
    :cond_4
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0

    .line 675
    :cond_5
    const-string/jumbo v0, "pot"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "potx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 676
    :cond_6
    invoke-static {p1}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0

    .line 644
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static isExist(Ljava/lang/String;)Z
    .locals 2
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 852
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 853
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 854
    const/4 v1, 0x1

    .line 856
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSavableDirectory(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "oContext"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 223
    invoke-static {p0}, Lcom/infraware/common/util/FileUtils;->initStorageList(Landroid/content/Context;)V

    .line 225
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v6, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 227
    sget-object v6, Lcom/infraware/common/util/FileUtils;->m_oStorageList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 229
    .local v4, "storagePath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 230
    .local v3, "storageFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 225
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 233
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 236
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 238
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .local v0, "file":Ljava/io/File;
    move-object v1, v0

    .line 239
    .end local v0    # "file":Ljava/io/File;
    .local v1, "file":Ljava/io/File;
    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 241
    invoke-virtual {v1}, Ljava/io/File;->isHidden()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 251
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "storageFile":Ljava/io/File;
    .end local v4    # "storagePath":Ljava/lang/String;
    :cond_2
    :goto_2
    return v5

    .line 244
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "storageFile":Ljava/io/File;
    .restart local v4    # "storagePath":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    move-object v1, v0

    .end local v0    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_1

    .line 247
    :cond_4
    const/4 v5, 0x1

    goto :goto_2
.end method

.method public static isSdcard()Z
    .locals 2

    .prologue
    .line 685
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "stateSd":Ljava/lang/String;
    const-string/jumbo v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 688
    const/4 v1, 0x1

    .line 690
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSupportEditType(I)Z
    .locals 2
    .param p0, "extension"    # I

    .prologue
    const/4 v0, 0x1

    .line 616
    if-eq p0, v0, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x6

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-ne p0, v1, :cond_1

    .line 622
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupported(Ljava/lang/String;)Z
    .locals 2
    .param p0, "extension"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 592
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 593
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    .line 595
    const-string/jumbo v1, "doc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "docx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "ppt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "pptx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "pps"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "ppsx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "xls"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "xlsx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "csv"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "rtf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "hwp"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "pdf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "txt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "asc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 610
    :cond_0
    :goto_0
    return v0

    .line 607
    :cond_1
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine;->isSupportDocument(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 610
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportedBroadcast(Ljava/lang/String;)Z
    .locals 2
    .param p0, "extension"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 627
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 628
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    .line 630
    const-string/jumbo v1, "ppt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "pptx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "pps"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "ppsx"

    invoke-virtual {p0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 639
    :cond_0
    :goto_0
    return v0

    .line 635
    :cond_1
    invoke-static {p0}, Lcom/infraware/common/define/CMModelDefine;->isSupportBroadcast(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 639
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeDirectories(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 285
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 288
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 289
    const/4 v1, 0x0

    .line 293
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "isAbsolutePath"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 266
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 267
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "dirName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 270
    .local v2, "new_dir":Ljava/io/File;
    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    .line 271
    const/4 v3, 0x3

    invoke-virtual {p2, v0, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    .line 277
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 278
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 280
    :goto_1
    return-object v3

    .line 273
    :cond_1
    new-instance v2, Ljava/io/File;

    .end local v2    # "new_dir":Ljava/io/File;
    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 274
    .restart local v2    # "new_dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 275
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    goto :goto_0

    .line 280
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static readMountsFile()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v4, "mMounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v5, Ljava/util/Scanner;

    new-instance v6, Ljava/io/File;

    const-string/jumbo v7, "/proc/mounts"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V

    .line 201
    .local v5, "scanner":Ljava/util/Scanner;
    :cond_0
    :goto_0
    invoke-virtual {v5}, Ljava/util/Scanner;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 202
    invoke-virtual {v5}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    .line 204
    .local v2, "line":Ljava/lang/String;
    const-string/jumbo v6, "/dev/block/vold/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 205
    const-string/jumbo v6, "[ \t]+"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 206
    .local v3, "lineElements":[Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v1, v3, v6

    .line 208
    .local v1, "element":Ljava/lang/String;
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    .end local v1    # "element":Ljava/lang/String;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "lineElements":[Ljava/lang/String;
    .end local v5    # "scanner":Ljava/util/Scanner;
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 215
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    return-object v4
.end method

.method private static readVoldFile()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v4, "mVold":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v5, Ljava/util/Scanner;

    new-instance v6, Ljava/io/File;

    const-string/jumbo v7, "/system/etc/vold.fstab"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V

    .line 174
    .local v5, "scanner":Ljava/util/Scanner;
    :cond_0
    :goto_0
    invoke-virtual {v5}, Ljava/util/Scanner;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 175
    invoke-virtual {v5}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    .line 177
    .local v2, "line":Ljava/lang/String;
    const-string/jumbo v6, "dev_mount"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 178
    const-string/jumbo v6, "[ \t]+"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "lineElements":[Ljava/lang/String;
    const/4 v6, 0x2

    aget-object v1, v3, v6

    .line 181
    .local v1, "element":Ljava/lang/String;
    const-string/jumbo v6, ":"

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 182
    const/4 v6, 0x0

    const-string/jumbo v7, ":"

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 185
    :cond_1
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    .end local v1    # "element":Ljava/lang/String;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "lineElements":[Ljava/lang/String;
    .end local v5    # "scanner":Ljava/util/Scanner;
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 192
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    return-object v4
.end method

.method public static showUnavailableStorageDialog(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 893
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07025b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070063

    new-instance v3, Lcom/infraware/common/util/FileUtils$2;

    invoke-direct {v3, p0}, Lcom/infraware/common/util/FileUtils$2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/infraware/common/util/FileUtils$1;

    invoke-direct {v2, p0}, Lcom/infraware/common/util/FileUtils$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 918
    .local v0, "dlg":Landroid/app/AlertDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 919
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 921
    return-void
.end method
