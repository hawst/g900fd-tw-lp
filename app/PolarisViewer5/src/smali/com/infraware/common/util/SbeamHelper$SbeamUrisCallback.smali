.class Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;
.super Ljava/lang/Object;
.source "SbeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/util/SbeamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SbeamUrisCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/util/SbeamHelper;


# direct methods
.method constructor <init>(Lcom/infraware/common/util/SbeamHelper;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createBeamUris(Landroid/nfc/NfcEvent;)[Landroid/net/Uri;
    .locals 8
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 270
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # invokes: Lcom/infraware/common/util/SbeamHelper;->getFiles()[Ljava/io/File;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$000(Lcom/infraware/common/util/SbeamHelper;)[Ljava/io/File;

    move-result-object v0

    .line 272
    .local v0, "files":[Ljava/io/File;
    const-string/jumbo v4, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "SbeamUrisCallback ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v6}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 275
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v4, "POPUP_MODE"

    const-string/jumbo v5, "no_file_selected"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 278
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v4, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    .line 301
    .end local v1    # "i":Landroid/content/Intent;
    :goto_0
    return-object v3

    .line 280
    :cond_0
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 281
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 282
    .restart local v1    # "i":Landroid/content/Intent;
    const-string/jumbo v4, "POPUP_MODE"

    const-string/jumbo v5, "from_cloud_file"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 284
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v4, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto :goto_0

    .line 286
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 287
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 288
    .restart local v1    # "i":Landroid/content/Intent;
    const-string/jumbo v4, "POPUP_MODE"

    const-string/jumbo v5, "from_drm_file"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 290
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v4, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto :goto_0

    .line 292
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/infraware/common/util/SbeamHelper;->access$100(Lcom/infraware/common/util/SbeamHelper;)I

    move-result v5

    if-ne v4, v5, :cond_3

    .line 293
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v4, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 294
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "POPUP_MODE"

    const-string/jumbo v5, "does_not_saved"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/infraware/common/util/SbeamHelper;->access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 296
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v4, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    goto :goto_0

    .line 300
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v3, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # setter for: Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I
    invoke-static {v3, v7}, Lcom/infraware/common/util/SbeamHelper;->access$102(Lcom/infraware/common/util/SbeamHelper;I)I

    .line 301
    iget-object v3, p0, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;->this$0:Lcom/infraware/common/util/SbeamHelper;

    # getter for: Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;
    invoke-static {v3}, Lcom/infraware/common/util/SbeamHelper;->access$400(Lcom/infraware/common/util/SbeamHelper;)[Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_0
.end method
