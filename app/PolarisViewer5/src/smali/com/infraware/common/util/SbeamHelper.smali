.class public Lcom/infraware/common/util/SbeamHelper;
.super Ljava/lang/Object;
.source "SbeamHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;,
        Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;,
        Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;
    }
.end annotation


# static fields
.field private static final ACTION_ABEAM_POPUP:Ljava/lang/String; = "com.android.nfc.AndroidBeamPopUp"

.field private static final ACTION_POPUP:Ljava/lang/String; = "com.sec.android.directshare.DirectSharePopUp"

.field private static final ACTION_START:Ljava/lang/String; = "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

.field private static final EXTRA_KEY_POPUP_MODE:Ljava/lang/String; = "POPUP_MODE"

.field private static final EXTRA_VAL_CLOUD_FILE:Ljava/lang/String; = "from_cloud_file"

.field private static final EXTRA_VAL_DRM_FILE:Ljava/lang/String; = "from_drm_file"

.field private static final EXTRA_VAL_NOT_SAVED:Ljava/lang/String; = "does_not_saved"

.field private static final EXTRA_VAL_NO_FILE:Ljava/lang/String; = "no_file_selected"

.field private static final KEY_SBEAM_ONOFF:Ljava/lang/String; = "SBeam_on_off"

.field private static final KEY_SBEAM_SUPPORT:Ljava/lang/String; = "SBeam_support"

.field private static final PACKAGE_NAME_SBEAM:Ljava/lang/String; = "com.sec.android.directshare"

.field private static final PACKAGE_NAME_SETTINGS:Ljava/lang/String; = "com.android.settings"

.field private static final PREP_NAME_SBEAM:Ljava/lang/String; = "pref_sbeam"

.field private static final TAG:Ljava/lang/String; = "[SBeam]"

.field private static final TAGClass:Ljava/lang/String; = "SbeamHelper."


# instance fields
.field private final STATUS_IS_CLOUD_FILE:I

.field private final STATUS_IS_DRM:I

.field private final STATUS_NEED_SAVED:I

.field private final STATUS_NONE:I

.field private final STATUS_NO_FILE_SELECTED:I

.field private final STATUS_PUSH:I

.field private mAppMime:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mNdefStatus:I

.field private mSbeamCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;

.field private mSbeamCompleteCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;

.field private mSbeamUrisCb:Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;

.field private mUris:[Landroid/net/Uri;

.field private mbSupportedSbeam:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appMime"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 352
    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mAppMime:Ljava/lang/String;

    .line 353
    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;

    .line 354
    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;

    .line 355
    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamCompleteCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;

    .line 356
    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamUrisCb:Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;

    .line 357
    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    .line 358
    iput-boolean v6, p0, Lcom/infraware/common/util/SbeamHelper;->mbSupportedSbeam:Z

    .line 361
    iput v6, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    .line 362
    iput v6, p0, Lcom/infraware/common/util/SbeamHelper;->STATUS_NONE:I

    .line 363
    const/4 v3, 0x1

    iput v3, p0, Lcom/infraware/common/util/SbeamHelper;->STATUS_PUSH:I

    .line 364
    const/4 v3, 0x2

    iput v3, p0, Lcom/infraware/common/util/SbeamHelper;->STATUS_NO_FILE_SELECTED:I

    .line 365
    const/4 v3, 0x3

    iput v3, p0, Lcom/infraware/common/util/SbeamHelper;->STATUS_IS_CLOUD_FILE:I

    .line 366
    const/4 v3, 0x4

    iput v3, p0, Lcom/infraware/common/util/SbeamHelper;->STATUS_IS_DRM:I

    .line 367
    iput v7, p0, Lcom/infraware/common/util/SbeamHelper;->STATUS_NEED_SAVED:I

    .line 34
    iput-object p1, p0, Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/infraware/common/util/SbeamHelper;->mAppMime:Ljava/lang/String;

    .line 37
    const/4 v2, 0x0

    .line 39
    .local v2, "settingContext":Landroid/content/Context;
    :try_start_0
    iget-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "com.android.settings"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 47
    :goto_0
    if-nez v2, :cond_0

    .line 62
    :goto_1
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SbeamHelper.SbeamHelper:NameNotFoundException > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/SecurityException;
    const-string/jumbo v3, "[SBeam]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SbeamHelper.SbeamHelper:SecurityException > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 50
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_0
    const-string/jumbo v3, "pref_sbeam"

    invoke-virtual {v2, v3, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 53
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string/jumbo v3, "SBeam_support"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/infraware/common/util/SbeamHelper;->mbSupportedSbeam:Z

    .line 54
    const-string/jumbo v4, "[SBeam]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SbeamHelper.SbeamHelper : SBeam is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v3, p0, Lcom/infraware/common/util/SbeamHelper;->mbSupportedSbeam:Z

    if-eqz v3, :cond_2

    const-string/jumbo v3, "supported"

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-boolean v3, p0, Lcom/infraware/common/util/SbeamHelper;->mbSupportedSbeam:Z

    if-eqz v3, :cond_1

    .line 57
    new-instance v3, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;

    invoke-direct {v3, p0}, Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;-><init>(Lcom/infraware/common/util/SbeamHelper;)V

    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;

    .line 58
    new-instance v3, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;

    invoke-direct {v3, p0}, Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;-><init>(Lcom/infraware/common/util/SbeamHelper;)V

    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamCompleteCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;

    .line 61
    :cond_1
    new-instance v3, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;

    invoke-direct {v3, p0}, Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;-><init>(Lcom/infraware/common/util/SbeamHelper;)V

    iput-object v3, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamUrisCb:Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;

    goto/16 :goto_1

    .line 54
    :cond_2
    const-string/jumbo v3, "not supported"

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/infraware/common/util/SbeamHelper;)[Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/SbeamHelper;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/infraware/common/util/SbeamHelper;->getFiles()[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/common/util/SbeamHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/SbeamHelper;

    .prologue
    .line 27
    iget v0, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    return v0
.end method

.method static synthetic access$102(Lcom/infraware/common/util/SbeamHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/util/SbeamHelper;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    return p1
.end method

.method static synthetic access$200(Lcom/infraware/common/util/SbeamHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/SbeamHelper;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/infraware/common/util/SbeamHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/SbeamHelper;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/common/util/SbeamHelper;->mAppMime:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/infraware/common/util/SbeamHelper;)[Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/util/SbeamHelper;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    return-object v0
.end method

.method private getFiles()[Ljava/io/File;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 311
    iget-object v8, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    if-nez v8, :cond_1

    .line 312
    const/4 v8, 0x2

    iput v8, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    move-object v1, v7

    .line 338
    :cond_0
    :goto_0
    return-object v1

    .line 316
    :cond_1
    iget-object v8, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    array-length v8, v8

    new-array v1, v8, [Ljava/io/File;

    .line 318
    .local v1, "files":[Ljava/io/File;
    const/4 v2, 0x0

    .line 319
    .local v2, "i":I
    iget-object v0, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v6, v0, v3

    .line 320
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 322
    .local v5, "scheme":Ljava/lang/String;
    const-string/jumbo v8, "file"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 323
    invoke-direct {p0, v6}, Lcom/infraware/common/util/SbeamHelper;->isDrmFile(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 324
    const/4 v8, 0x4

    iput v8, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    move-object v1, v7

    .line 325
    goto :goto_0

    .line 326
    :cond_2
    invoke-direct {p0, v6}, Lcom/infraware/common/util/SbeamHelper;->needSaved(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 327
    const/4 v8, 0x5

    iput v8, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    move-object v1, v7

    .line 328
    goto :goto_0

    .line 331
    :cond_3
    const/4 v8, 0x3

    iput v8, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    move-object v1, v7

    .line 332
    goto :goto_0

    .line 334
    :cond_4
    new-instance v8, Ljava/io/File;

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v8, v1, v2

    .line 335
    add-int/lit8 v2, v2, 0x1

    .line 319
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private isDrmFile(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method private needSaved(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 348
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method isSbeamOn(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 104
    iget-boolean v4, p0, Lcom/infraware/common/util/SbeamHelper;->mbSupportedSbeam:Z

    if-nez v4, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v3

    .line 108
    :cond_1
    const/4 v2, 0x0

    .line 110
    .local v2, "settingContext":Landroid/content/Context;
    :try_start_0
    iget-object v4, p0, Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "com.android.settings"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 118
    :goto_1
    if-eqz v2, :cond_0

    .line 121
    const-string/jumbo v4, "pref_sbeam"

    const/4 v5, 0x5

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 125
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string/jumbo v4, "SBeam_on_off"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_0

    .line 111
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v4, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "SbeamHelper.SbeamHelper:NameNotFoundException > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 113
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/SecurityException;
    const-string/jumbo v4, "[SBeam]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "SbeamHelper.SbeamHelper:SecurityException > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setBeamUris([Landroid/net/Uri;Landroid/app/Activity;Landroid/content/Context;)V
    .locals 5
    .param p1, "uris"    # [Landroid/net/Uri;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 66
    if-nez p2, :cond_0

    .line 67
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "Activity shouldn\'t be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 71
    .local v0, "nfcAdapter":Landroid/nfc/NfcAdapter;
    if-nez v0, :cond_1

    .line 72
    const-string/jumbo v1, "[SBeam]"

    const-string/jumbo v2, "SbeamHelper.setBeamUris > can\'t load nfcadpater"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :goto_0
    return-void

    .line 76
    :cond_1
    iput v3, p0, Lcom/infraware/common/util/SbeamHelper;->mNdefStatus:I

    .line 77
    iput-object v4, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    .line 79
    if-nez p1, :cond_2

    .line 80
    new-array v1, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v4, p2, v1}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 81
    new-array v1, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v4, p2, v1}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 82
    invoke-virtual {v0, v4, p2}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V

    goto :goto_0

    .line 86
    :cond_2
    iput-object p1, p0, Lcom/infraware/common/util/SbeamHelper;->mUris:[Landroid/net/Uri;

    .line 88
    invoke-virtual {p0, p3}, Lcom/infraware/common/util/SbeamHelper;->isSbeamOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 89
    const-string/jumbo v1, "[SBeam]"

    const-string/jumbo v2, "setSbeam"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p2, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 91
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamCompleteCb:Lcom/infraware/common/util/SbeamHelper$SbeamPushCompleteCallback;

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p2, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 92
    invoke-virtual {v0, v4, p2}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V

    goto :goto_0

    .line 95
    :cond_3
    const-string/jumbo v1, "[SBeam]"

    const-string/jumbo v2, "setAbeam"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-object v1, p0, Lcom/infraware/common/util/SbeamHelper;->mSbeamUrisCb:Lcom/infraware/common/util/SbeamHelper$SbeamUrisCallback;

    invoke-virtual {v0, v1, p2}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V

    .line 97
    new-array v1, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v4, p2, v1}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 98
    new-array v1, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v4, p2, v1}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method
