.class public Lcom/infraware/common/util/text/CharsetDetector;
.super Ljava/lang/Object;
.source "CharsetDetector.java"


# static fields
.field private static fCSRecognizers:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/common/util/text/CharsetRecognizer;",
            ">;"
        }
    .end annotation
.end field

.field private static fCharsetNames:[Ljava/lang/String; = null

.field private static final kBufSize:I = 0x1f40


# instance fields
.field fByteStats:[S

.field fC1Bytes:Z

.field fDeclaredEncoding:Ljava/lang/String;

.field fInputBytes:[B

.field fInputLen:I

.field fInputStream:Ljava/io/InputStream;

.field fRawInput:[B

.field fRawLength:I

.field fStripTags:Z

.field private m_context:Landroid/content/Context;

.field m_strDefaultLanguage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 528
    invoke-static {}, Lcom/infraware/common/util/text/CharsetDetector;->createRecognizers()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/infraware/common/util/text/CharsetDetector;->fCSRecognizers:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    const/16 v0, 0x1f40

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputBytes:[B

    .line 496
    const/16 v0, 0x100

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fByteStats:[S

    .line 500
    iput-boolean v1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fC1Bytes:Z

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fDeclaredEncoding:Ljava/lang/String;

    .line 519
    iput-boolean v1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fStripTags:Z

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    const/16 v0, 0x1f40

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputBytes:[B

    .line 496
    const/16 v0, 0x100

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fByteStats:[S

    .line 500
    iput-boolean v1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fC1Bytes:Z

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fDeclaredEncoding:Ljava/lang/String;

    .line 519
    iput-boolean v1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fStripTags:Z

    .line 64
    iput-object p1, p0, Lcom/infraware/common/util/text/CharsetDetector;->m_context:Landroid/content/Context;

    .line 65
    return-void
.end method

.method private MungeInput()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 412
    const/4 v8, 0x0

    .line 413
    .local v8, "srci":I
    const/4 v2, 0x0

    .line 415
    .local v2, "dsti":I
    const/4 v5, 0x0

    .line 416
    .local v5, "inMarkup":Z
    const/4 v7, 0x0

    .line 417
    .local v7, "openTags":I
    const/4 v1, 0x0

    .line 425
    .local v1, "badTags":I
    iget-boolean v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fStripTags:Z

    if-eqz v10, :cond_5

    .line 426
    const/4 v8, 0x0

    :goto_0
    iget v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    if-ge v8, v10, :cond_4

    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputBytes:[B

    array-length v10, v10

    if-ge v2, v10, :cond_4

    .line 427
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawInput:[B

    aget-byte v0, v10, v8

    .line 428
    .local v0, "b":B
    const/16 v10, 0x3c

    if-ne v0, v10, :cond_1

    .line 429
    if-eqz v5, :cond_0

    .line 430
    add-int/lit8 v1, v1, 0x1

    .line 432
    :cond_0
    const/4 v5, 0x1

    .line 433
    add-int/lit8 v7, v7, 0x1

    .line 436
    :cond_1
    if-nez v5, :cond_2

    .line 437
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputBytes:[B

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "dsti":I
    .local v3, "dsti":I
    aput-byte v0, v10, v2

    move v2, v3

    .line 440
    .end local v3    # "dsti":I
    .restart local v2    # "dsti":I
    :cond_2
    const/16 v10, 0x3e

    if-ne v0, v10, :cond_3

    .line 441
    const/4 v5, 0x0

    .line 426
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 445
    .end local v0    # "b":B
    :cond_4
    iput v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputLen:I

    .line 453
    :cond_5
    const/4 v10, 0x5

    if-lt v7, v10, :cond_6

    div-int/lit8 v10, v7, 0x5

    if-lt v10, v1, :cond_6

    iget v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputLen:I

    const/16 v11, 0x64

    if-ge v10, v11, :cond_9

    iget v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    const/16 v11, 0x258

    if-le v10, v11, :cond_9

    .line 455
    :cond_6
    iget v6, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    .line 457
    .local v6, "limit":I
    const/16 v10, 0x1f40

    if-le v6, v10, :cond_7

    .line 458
    const/16 v6, 0x1f40

    .line 461
    :cond_7
    const/4 v8, 0x0

    :goto_1
    if-ge v8, v6, :cond_8

    .line 462
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputBytes:[B

    iget-object v11, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawInput:[B

    aget-byte v11, v11, v8

    aput-byte v11, v10, v8

    .line 461
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 464
    :cond_8
    iput v8, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputLen:I

    .line 471
    .end local v6    # "limit":I
    :cond_9
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fByteStats:[S

    invoke-static {v10, v12}, Ljava/util/Arrays;->fill([SS)V

    .line 472
    const/4 v8, 0x0

    :goto_2
    iget v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputLen:I

    if-ge v8, v10, :cond_a

    .line 473
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputBytes:[B

    aget-byte v10, v10, v8

    and-int/lit16 v9, v10, 0xff

    .line 474
    .local v9, "val":I
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fByteStats:[S

    aget-short v11, v10, v9

    add-int/lit8 v11, v11, 0x1

    int-to-short v11, v11

    aput-short v11, v10, v9

    .line 472
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 477
    .end local v9    # "val":I
    :cond_a
    iput-boolean v12, p0, Lcom/infraware/common/util/text/CharsetDetector;->fC1Bytes:Z

    .line 478
    const/16 v4, 0x80

    .local v4, "i":I
    :goto_3
    const/16 v10, 0x9f

    if-gt v4, v10, :cond_b

    .line 479
    iget-object v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fByteStats:[S

    aget-short v10, v10, v4

    if-eqz v10, :cond_c

    .line 480
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/infraware/common/util/text/CharsetDetector;->fC1Bytes:Z

    .line 484
    :cond_b
    return-void

    .line 478
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method private static createRecognizers()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/infraware/common/util/text/CharsetRecognizer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 535
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 537
    .local v5, "recognizers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/common/util/text/CharsetRecognizer;>;"
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_UTF8;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_UTF8;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_16_BE;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_16_BE;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_16_LE;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_16_LE;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_32_BE;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_32_BE;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_32_LE;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_Unicode$CharsetRecog_UTF_32_LE;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_sjis;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_sjis;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_2022$CharsetRecog_2022JP;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_2022$CharsetRecog_2022JP;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_2022$CharsetRecog_2022CN;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_2022$CharsetRecog_2022CN;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_2022$CharsetRecog_2022KR;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_2022$CharsetRecog_2022KR;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_gb_18030;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_gb_18030;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_euc$CharsetRecog_euc_jp;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_euc$CharsetRecog_euc_jp;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_euc$CharsetRecog_euc_kr;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_euc$CharsetRecog_euc_kr;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_big5;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_mbcs$CharsetRecog_big5;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 553
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_da;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_da;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_de;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_de;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_en;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_en;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_es;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_es;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_fr;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_fr;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_it;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_it;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_nl;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_nl;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_no;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_no;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_pt;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_pt;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_sv;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_1_sv;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_cs;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_hu;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_hu;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_pl;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_pl;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_ro;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_2_ro;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_5_ru;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_5_ru;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_6_ar;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_6_ar;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_7_el;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_7_el;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_8_I_he;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_8_I_he;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_8_he;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_8_he;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_windows_1251;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_windows_1251;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_windows_1256;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_windows_1256;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_KOI8_R;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_KOI8_R;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_9_tr;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_8859_9_tr;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_IBM424_he_rtl;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_IBM424_he_rtl;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    new-instance v6, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_IBM424_he_ltr;

    invoke-direct {v6}, Lcom/infraware/common/util/text/CharsetRecog_sbcs$CharsetRecog_IBM424_he_ltr;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v0, v6, [Ljava/lang/String;

    .line 586
    .local v0, "charsetNames":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 588
    .local v3, "out":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 589
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/infraware/common/util/text/CharsetRecognizer;

    invoke-virtual {v6}, Lcom/infraware/common/util/text/CharsetRecognizer;->getName()Ljava/lang/String;

    move-result-object v2

    .line 591
    .local v2, "name":Ljava/lang/String;
    if-eqz v3, :cond_0

    add-int/lit8 v6, v3, -0x1

    aget-object v6, v0, v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 592
    :cond_0
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "out":I
    .local v4, "out":I
    aput-object v2, v0, v3

    move v3, v4

    .line 588
    .end local v4    # "out":I
    .restart local v3    # "out":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 596
    .end local v2    # "name":Ljava/lang/String;
    :cond_2
    new-array v6, v3, [Ljava/lang/String;

    sput-object v6, Lcom/infraware/common/util/text/CharsetDetector;->fCharsetNames:[Ljava/lang/String;

    .line 597
    sget-object v6, Lcom/infraware/common/util/text/CharsetDetector;->fCharsetNames:[Ljava/lang/String;

    invoke-static {v0, v7, v6, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 599
    return-object v5
.end method

.method public static getAllDetectableCharsets()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 370
    sget-object v0, Lcom/infraware/common/util/text/CharsetDetector;->fCharsetNames:[Ljava/lang/String;

    return-object v0
.end method

.method private getLocaleCharsetMatch([Lcom/infraware/common/util/text/CharsetMatch;)Lcom/infraware/common/util/text/CharsetMatch;
    .locals 8
    .param p1, "matches"    # [Lcom/infraware/common/util/text/CharsetMatch;

    .prologue
    const/4 v7, 0x0

    .line 188
    aget-object v5, p1, v7

    invoke-virtual {v5}, Lcom/infraware/common/util/text/CharsetMatch;->getConfidence()I

    move-result v0

    .line 190
    .local v0, "highConfidence":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/common/util/text/CharsetMatch;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p1

    if-ge v1, v5, :cond_0

    .line 194
    aget-object v5, p1, v1

    invoke-virtual {v5}, Lcom/infraware/common/util/text/CharsetMatch;->getConfidence()I

    move-result v5

    if-ne v0, v5, :cond_0

    .line 195
    aget-object v5, p1, v1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 201
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 202
    aget-object v5, p1, v7

    .line 221
    :goto_1
    return-object v5

    .line 205
    :cond_1
    iget-object v5, p0, Lcom/infraware/common/util/text/CharsetDetector;->m_context:Landroid/content/Context;

    if-nez v5, :cond_2

    .line 206
    aget-object v5, p1, v7

    goto :goto_1

    .line 209
    :cond_2
    iget-object v5, p0, Lcom/infraware/common/util/text/CharsetDetector;->m_context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v4, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 210
    .local v4, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 212
    .local v2, "language":Ljava/lang/String;
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 215
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-virtual {v5}, Lcom/infraware/common/util/text/CharsetMatch;->getLanguage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-virtual {v5}, Lcom/infraware/common/util/text/CharsetMatch;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 217
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/infraware/common/util/text/CharsetMatch;

    goto :goto_1

    .line 212
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 221
    :cond_4
    aget-object v5, p1, v7

    goto :goto_1
.end method


# virtual methods
.method public detect()Lcom/infraware/common/util/text/CharsetMatch;
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/infraware/common/util/text/CharsetDetector;->detectAll()[Lcom/infraware/common/util/text/CharsetMatch;

    move-result-object v0

    .line 176
    .local v0, "matches":[Lcom/infraware/common/util/text/CharsetMatch;
    if-eqz v0, :cond_0

    array-length v2, v0

    if-nez v2, :cond_1

    .line 177
    :cond_0
    const/4 v1, 0x0

    .line 183
    :goto_0
    return-object v1

    .line 181
    :cond_1
    invoke-direct {p0, v0}, Lcom/infraware/common/util/text/CharsetDetector;->getLocaleCharsetMatch([Lcom/infraware/common/util/text/CharsetMatch;)Lcom/infraware/common/util/text/CharsetMatch;

    move-result-object v1

    .line 183
    .local v1, "result":Lcom/infraware/common/util/text/CharsetMatch;
    goto :goto_0
.end method

.method public detectAll()[Lcom/infraware/common/util/text/CharsetMatch;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 245
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 247
    .local v5, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/infraware/common/util/text/CharsetMatch;>;"
    invoke-direct {p0}, Lcom/infraware/common/util/text/CharsetDetector;->MungeInput()V

    .line 251
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget-object v7, Lcom/infraware/common/util/text/CharsetDetector;->fCSRecognizers:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 252
    sget-object v7, Lcom/infraware/common/util/text/CharsetDetector;->fCSRecognizers:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/infraware/common/util/text/CharsetRecognizer;

    .line 253
    .local v1, "csr":Lcom/infraware/common/util/text/CharsetRecognizer;
    invoke-virtual {v1, p0}, Lcom/infraware/common/util/text/CharsetRecognizer;->match(Lcom/infraware/common/util/text/CharsetDetector;)I

    move-result v2

    .line 254
    .local v2, "detectResults":I
    and-int/lit16 v0, v2, 0xff

    .line 255
    .local v0, "confidence":I
    if-lez v0, :cond_0

    .line 256
    new-instance v4, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-direct {v4, p0, v1, v0}, Lcom/infraware/common/util/text/CharsetMatch;-><init>(Lcom/infraware/common/util/text/CharsetDetector;Lcom/infraware/common/util/text/CharsetRecognizer;I)V

    .line 257
    .local v4, "m":Lcom/infraware/common/util/text/CharsetMatch;
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    .end local v4    # "m":Lcom/infraware/common/util/text/CharsetMatch;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 260
    .end local v0    # "confidence":I
    .end local v1    # "csr":Lcom/infraware/common/util/text/CharsetRecognizer;
    .end local v2    # "detectResults":I
    :cond_1
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 261
    invoke-static {v5}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 263
    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_3

    .line 265
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-virtual {v7}, Lcom/infraware/common/util/text/CharsetMatch;->getConfidence()I

    move-result v8

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-virtual {v7}, Lcom/infraware/common/util/text/CharsetMatch;->getConfidence()I

    move-result v7

    if-ne v8, v7, :cond_3

    .line 267
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-virtual {v7}, Lcom/infraware/common/util/text/CharsetMatch;->getLanguage()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 269
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/infraware/common/util/text/CharsetMatch;

    invoke-virtual {v7}, Lcom/infraware/common/util/text/CharsetMatch;->getLanguage()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/infraware/common/util/text/CharsetDetector;->m_strDefaultLanguage:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 271
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v9, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 263
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 279
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v6, v7, [Lcom/infraware/common/util/text/CharsetMatch;

    .line 280
    .local v6, "resultArray":[Lcom/infraware/common/util/text/CharsetMatch;
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "resultArray":[Lcom/infraware/common/util/text/CharsetMatch;
    check-cast v6, [Lcom/infraware/common/util/text/CharsetMatch;

    .line 281
    .restart local v6    # "resultArray":[Lcom/infraware/common/util/text/CharsetMatch;
    return-object v6
.end method

.method public enableInputFilter(Z)Z
    .locals 1
    .param p1, "filter"    # Z

    .prologue
    .line 400
    iget-boolean v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fStripTags:Z

    .line 402
    .local v0, "previous":Z
    iput-boolean p1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fStripTags:Z

    .line 404
    return v0
.end method

.method public getReader(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/Reader;
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "declaredEncoding"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 308
    iput-object p2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fDeclaredEncoding:Ljava/lang/String;

    .line 311
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/infraware/common/util/text/CharsetDetector;->setText(Ljava/io/InputStream;)Lcom/infraware/common/util/text/CharsetDetector;

    .line 313
    invoke-virtual {p0}, Lcom/infraware/common/util/text/CharsetDetector;->detect()Lcom/infraware/common/util/text/CharsetMatch;

    move-result-object v1

    .line 315
    .local v1, "match":Lcom/infraware/common/util/text/CharsetMatch;
    if-nez v1, :cond_0

    .line 321
    .end local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :goto_0
    return-object v2

    .line 319
    .restart local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :cond_0
    invoke-virtual {v1}, Lcom/infraware/common/util/text/CharsetMatch;->getReader()Ljava/io/Reader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 320
    .end local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getString([BLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "in"    # [B
    .param p2, "declaredEncoding"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 343
    iput-object p2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fDeclaredEncoding:Ljava/lang/String;

    .line 346
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/infraware/common/util/text/CharsetDetector;->setText([B)Lcom/infraware/common/util/text/CharsetDetector;

    .line 348
    invoke-virtual {p0}, Lcom/infraware/common/util/text/CharsetDetector;->detect()Lcom/infraware/common/util/text/CharsetMatch;

    move-result-object v1

    .line 350
    .local v1, "match":Lcom/infraware/common/util/text/CharsetMatch;
    if-nez v1, :cond_0

    .line 356
    .end local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :goto_0
    return-object v2

    .line 354
    .restart local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :cond_0
    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Lcom/infraware/common/util/text/CharsetMatch;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 355
    .end local v1    # "match":Lcom/infraware/common/util/text/CharsetMatch;
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public inputFilterEnabled()Z
    .locals 1

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fStripTags:Z

    return v0
.end method

.method public setDeclaredEncoding(Ljava/lang/String;)Lcom/infraware/common/util/text/CharsetDetector;
    .locals 0
    .param p1, "encoding"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fDeclaredEncoding:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public setDefaultLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "lan"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/infraware/common/util/text/CharsetDetector;->m_strDefaultLanguage:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setText(Ljava/io/InputStream;)Lcom/infraware/common/util/text/CharsetDetector;
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x1f40

    .line 128
    iput-object p1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    .line 129
    iget-object v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->mark(I)V

    .line 130
    new-array v2, v3, [B

    iput-object v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawInput:[B

    .line 133
    const/4 v2, 0x0

    iput v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    .line 134
    const/16 v1, 0x1f40

    .line 135
    .local v1, "remainingLength":I
    :goto_0
    if-lez v1, :cond_0

    .line 137
    iget-object v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawInput:[B

    iget v4, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    invoke-virtual {v2, v3, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 138
    .local v0, "bytesRead":I
    if-gtz v0, :cond_1

    .line 144
    .end local v0    # "bytesRead":I
    :cond_0
    iget-object v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V

    .line 146
    return-object p0

    .line 141
    .restart local v0    # "bytesRead":I
    :cond_1
    iget v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    .line 142
    sub-int/2addr v1, v0

    .line 143
    goto :goto_0
.end method

.method public setText([B)Lcom/infraware/common/util/text/CharsetDetector;
    .locals 1
    .param p1, "in"    # [B

    .prologue
    .line 98
    iput-object p1, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawInput:[B

    .line 99
    array-length v0, p1

    iput v0, p0, Lcom/infraware/common/util/text/CharsetDetector;->fRawLength:I

    .line 101
    return-object p0
.end method
