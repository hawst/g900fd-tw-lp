.class public Lcom/infraware/common/util/SBeamUtils;
.super Ljava/lang/Object;
.source "SBeamUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/util/SBeamUtils$1;,
        Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;,
        Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;
    }
.end annotation


# static fields
.field private static final ACTION_DIRECT_SHARE_POPUP:Ljava/lang/String; = "com.sec.android.directshare.DirectSharePopUp"

.field private static final ACTION_DIRECT_SHARE_START:Ljava/lang/String; = "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

.field private static final KEY_FILE_LENGTH:Ljava/lang/String; = "fileLen"

.field private static final KEY_FILE_NAME:Ljava/lang/String; = "fileName"

.field private static final KEY_FILE_PATH:Ljava/lang/String; = "filepath"

.field private static final KEY_LIST:Ljava/lang/String; = "list"

.field private static final KEY_MAC:Ljava/lang/String; = "mac"

.field private static final KEY_MIME_TYPE:Ljava/lang/String; = "mimeType"

.field private static final KEY_SBEAM_ONOFF:Ljava/lang/String; = "SBeam_on_off"

.field private static final KEY_SBEAM_POPUP:Ljava/lang/String; = "POPUP_MODE"

.field private static final KEY_SBEAM_SUPPORT:Ljava/lang/String; = "SBeam_support"

.field private static final KEY_SUB_PATH:Ljava/lang/String; = "subPath"

.field private static final MIME_TYPE_SEC_DIRECT_SHARE_ERROR:Ljava/lang/String; = "text/DirectShareError"

.field private static final MIME_TYPE_SEC_DIRECT_SHARE_POLARIS:Ljava/lang/String; = "text/DirectSharePolarisViewer"

.field private static final SBEAM_APPLICATION_PACKAGE:Ljava/lang/String; = "com.sec.android.directshare"

.field private static final SBEAM_ERROR:Ljava/lang/String; = "Error"

.field private static final SBEAM_PREFERENCE_NAME:Ljava/lang/String; = "pref_sbeam"

.field private static final SBEAM_PREFERENCE_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field private static final VALUE_DRM_FILE:Ljava/lang/String; = "from_drm_file"

.field private static final VALUE_FILE_NOT_SAVED:Ljava/lang/String; = "does_not_saved"

.field private static final VALUE_FILE_NOT_SELECTED:Ljava/lang/String; = "no_file_selected"

.field private static final VALUE_SBEAM_OFF:Ljava/lang/String; = "s_beam_off"

.field private static mSBeamSupported:Z

.field private static mSBeamSupportedChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    sput-boolean v0, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupported:Z

    .line 61
    sput-boolean v0, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupportedChecked:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    return-void
.end method

.method public static createSBeamNdefErrorRecord(Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;)Landroid/nfc/NdefRecord;
    .locals 6
    .param p0, "errorType"    # Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;

    .prologue
    .line 210
    const-string/jumbo v0, "Error"

    .line 212
    .local v0, "error":Ljava/lang/String;
    sget-object v1, Lcom/infraware/common/util/SBeamUtils$1;->$SwitchMap$com$infraware$common$util$SBeamUtils$SBeamErrorType:[I

    invoke-virtual {p0}, Lcom/infraware/common/util/SBeamUtils$SBeamErrorType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 224
    :goto_0
    new-instance v1, Landroid/nfc/NdefRecord;

    const/4 v2, 0x2

    const-string/jumbo v3, "text/DirectShareError"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [B

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    return-object v1

    .line 214
    :pswitch_0
    const-string/jumbo v0, "s_beam_off"

    .line 215
    goto :goto_0

    .line 217
    :pswitch_1
    const-string/jumbo v0, "no_file_selected"

    .line 218
    goto :goto_0

    .line 220
    :pswitch_2
    const-string/jumbo v0, "from_drm_file"

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static createSBeamNdefMessage(Landroid/nfc/NdefRecord;)Landroid/nfc/NdefMessage;
    .locals 4
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 241
    new-instance v0, Landroid/nfc/NdefMessage;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/nfc/NdefRecord;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "com.sec.android.directshare"

    invoke-static {v3}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    return-object v0
.end method

.method public static createSBeamNdefRecord(Landroid/content/Context;[Ljava/lang/String;)Landroid/nfc/NdefRecord;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePaths"    # [Ljava/lang/String;

    .prologue
    .line 201
    new-instance v0, Landroid/nfc/NdefRecord;

    const/4 v1, 0x2

    const-string/jumbo v2, "text/DirectSharePolarisViewer"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [B

    const-string/jumbo v4, "text/DirectSharePolarisViewer"

    invoke-static {p0, v4, p1}, Lcom/infraware/common/util/SBeamUtils;->getJsonString(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    return-object v0
.end method

.method private static final getJsonString(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 25
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "filePathList"    # [Ljava/lang/String;

    .prologue
    .line 255
    const-string/jumbo v19, ""

    .line 258
    .local v19, "ret":Ljava/lang/String;
    :try_start_0
    const-string/jumbo v22, "wifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/net/wifi/WifiManager;

    .line 259
    .local v21, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual/range {v21 .. v21}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v20

    .line 260
    .local v20, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual/range {v20 .. v20}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x2

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 261
    .local v10, "firstMac":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x2

    invoke-virtual/range {v20 .. v20}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 263
    .local v14, "lastMac":Ljava/lang/String;
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 264
    .local v12, "jsonStringObj":Lorg/json/JSONObject;
    const/16 v22, 0x0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    const-string/jumbo v23, "0"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 265
    const-string/jumbo v22, "mac"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "0"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v24, 0x10

    move/from16 v0, v24

    invoke-static {v10, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v24

    or-int/lit8 v24, v24, 0x2

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 270
    :goto_0
    const-string/jumbo v22, "mimeType"

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 272
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v22, v0

    if-nez v22, :cond_1

    .line 273
    const-string/jumbo v19, ""

    .line 304
    .end local v10    # "firstMac":Ljava/lang/String;
    .end local v12    # "jsonStringObj":Lorg/json/JSONObject;
    .end local v14    # "lastMac":Ljava/lang/String;
    .end local v19    # "ret":Ljava/lang/String;
    .end local v20    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :goto_1
    return-object v19

    .line 268
    .restart local v10    # "firstMac":Ljava/lang/String;
    .restart local v12    # "jsonStringObj":Lorg/json/JSONObject;
    .restart local v14    # "lastMac":Ljava/lang/String;
    .restart local v19    # "ret":Ljava/lang/String;
    .restart local v20    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    const-string/jumbo v22, "mac"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v24, 0x10

    move/from16 v0, v24

    invoke-static {v10, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v24

    or-int/lit8 v24, v24, 0x2

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 294
    .end local v10    # "firstMac":Ljava/lang/String;
    .end local v12    # "jsonStringObj":Lorg/json/JSONObject;
    .end local v14    # "lastMac":Ljava/lang/String;
    .end local v20    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v17

    .line 295
    .local v17, "nfe":Ljava/lang/NumberFormatException;
    goto :goto_1

    .line 276
    .end local v17    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v10    # "firstMac":Ljava/lang/String;
    .restart local v12    # "jsonStringObj":Lorg/json/JSONObject;
    .restart local v14    # "lastMac":Ljava/lang/String;
    .restart local v20    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_1
    new-instance v16, Lorg/json/JSONArray;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONArray;-><init>()V

    .line 277
    .local v16, "list":Lorg/json/JSONArray;
    move-object/from16 v4, p2

    .local v4, "arr$":[Ljava/lang/String;
    array-length v15, v4

    .local v15, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_2
    if-ge v11, v15, :cond_2

    aget-object v9, v4, v11

    .line 278
    .local v9, "filePath":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v7, "f":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 280
    .local v3, "absolutePath":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    .line 282
    .local v6, "externalPath":Ljava/io/File;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 283
    .local v8, "fileInfoObj":Lorg/json/JSONObject;
    const-string/jumbo v22, "fileName"

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 284
    const-string/jumbo v22, "subPath"

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 285
    const-string/jumbo v22, "fileLen"

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v23

    move-object/from16 v0, v22

    move-wide/from16 v1, v23

    invoke-virtual {v8, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 286
    const-string/jumbo v22, "filepath"

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 288
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 277
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 290
    .end local v3    # "absolutePath":Ljava/lang/String;
    .end local v6    # "externalPath":Ljava/io/File;
    .end local v7    # "f":Ljava/io/File;
    .end local v8    # "fileInfoObj":Lorg/json/JSONObject;
    .end local v9    # "filePath":Ljava/lang/String;
    :cond_2
    const-string/jumbo v22, "list"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 292
    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v19

    goto/16 :goto_1

    .line 297
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v10    # "firstMac":Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v12    # "jsonStringObj":Lorg/json/JSONObject;
    .end local v14    # "lastMac":Ljava/lang/String;
    .end local v15    # "len$":I
    .end local v16    # "list":Lorg/json/JSONArray;
    .end local v20    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_1
    move-exception v13

    .line 298
    .local v13, "jsone":Lorg/json/JSONException;
    goto/16 :goto_1

    .line 300
    .end local v13    # "jsone":Lorg/json/JSONException;
    :catch_2
    move-exception v18

    .line 301
    .local v18, "npe":Ljava/lang/NullPointerException;
    goto/16 :goto_1

    .line 303
    .end local v18    # "npe":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v5

    .line 304
    .local v5, "e":Ljava/lang/Exception;
    goto/16 :goto_1
.end method

.method public static isSBeamEnabled(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "enabled":Z
    const/4 v3, 0x0

    .line 138
    .local v3, "sbeamSettingContext":Landroid/content/Context;
    const/4 v2, 0x0

    .line 141
    .local v2, "sbeamPreference":Landroid/content/SharedPreferences;
    :try_start_0
    const-string/jumbo v5, "com.android.settings"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v3

    .line 142
    const-string/jumbo v5, "pref_sbeam"

    const/4 v6, 0x5

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 144
    const-string/jumbo v5, "SBeam_on_off"

    const/4 v6, 0x0

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 155
    :goto_0
    return v0

    .line 146
    :catch_0
    move-exception v1

    .line 147
    .local v1, "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 148
    const/4 v0, 0x0

    .line 153
    goto :goto_0

    .line 150
    .end local v1    # "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v4

    .line 151
    .local v4, "se":Ljava/lang/SecurityException;
    invoke-virtual {v4}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 152
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSBeamSupportedDevice(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 167
    sget-boolean v4, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupportedChecked:Z

    if-nez v4, :cond_0

    .line 168
    const/4 v2, 0x0

    .line 169
    .local v2, "sbeamSettingContext":Landroid/content/Context;
    const/4 v1, 0x0

    .line 172
    .local v1, "sbeamPreference":Landroid/content/SharedPreferences;
    :try_start_0
    const-string/jumbo v4, "com.android.settings"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 173
    const-string/jumbo v4, "pref_sbeam"

    const/4 v5, 0x5

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 175
    const-string/jumbo v4, "SBeam_support"

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupported:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 187
    .end local v1    # "sbeamPreference":Landroid/content/SharedPreferences;
    .end local v2    # "sbeamSettingContext":Landroid/content/Context;
    :cond_0
    :goto_0
    sget-boolean v4, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupported:Z

    return v4

    .line 177
    .restart local v1    # "sbeamPreference":Landroid/content/SharedPreferences;
    .restart local v2    # "sbeamSettingContext":Landroid/content/Context;
    :catch_0
    move-exception v0

    .line 178
    .local v0, "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 179
    sput-boolean v6, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupported:Z

    goto :goto_0

    .line 181
    .end local v0    # "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v3

    .line 182
    .local v3, "se":Ljava/lang/SecurityException;
    invoke-virtual {v3}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 183
    sput-boolean v6, Lcom/infraware/common/util/SBeamUtils;->mSBeamSupported:Z

    goto :goto_0
.end method

.method public static startSBeamDirectShareService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 90
    .local v0, "i":Landroid/content/Intent;
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    const-string/jumbo v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 93
    return-void
.end method

.method public static startSBeamPopupActivity(Landroid/content/Context;Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 106
    .local v0, "i":Landroid/content/Intent;
    sget-object v1, Lcom/infraware/common/util/SBeamUtils$1;->$SwitchMap$com$infraware$common$util$SBeamUtils$SBeamPopupType:[I

    invoke-virtual {p1}, Lcom/infraware/common/util/SBeamUtils$SBeamPopupType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 120
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    const-string/jumbo v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_1
    return-void

    .line 108
    :pswitch_0
    const-string/jumbo v1, "POPUP_MODE"

    const-string/jumbo v2, "s_beam_off"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 111
    :pswitch_1
    const-string/jumbo v1, "POPUP_MODE"

    const-string/jumbo v2, "no_file_selected"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 114
    :pswitch_2
    const-string/jumbo v1, "POPUP_MODE"

    const-string/jumbo v2, "does_not_saved"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 124
    :catch_0
    move-exception v1

    goto :goto_1

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
