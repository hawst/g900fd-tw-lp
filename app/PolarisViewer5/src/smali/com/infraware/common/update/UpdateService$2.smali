.class Lcom/infraware/common/update/UpdateService$2;
.super Ljava/lang/Object;
.source "UpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/update/UpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/update/UpdateService;


# direct methods
.method constructor <init>(Lcom/infraware/common/update/UpdateService;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 159
    :try_start_0
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    # invokes: Lcom/infraware/common/update/UpdateService;->onDownloadApk()V
    invoke-static {v2}, Lcom/infraware/common/update/UpdateService;->access$000(Lcom/infraware/common/update/UpdateService;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 178
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/net/MalformedURLException;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 162
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2, v1}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 163
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v2, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 164
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    goto :goto_0

    .line 165
    .end local v0    # "e":Ljava/net/MalformedURLException;
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 166
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2, v1}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 168
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v2, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 169
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    .line 170
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 171
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 173
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2, v1}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 174
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v2, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 175
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$2;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    .line 176
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
