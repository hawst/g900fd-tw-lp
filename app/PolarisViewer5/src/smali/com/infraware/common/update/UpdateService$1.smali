.class Lcom/infraware/common/update/UpdateService$1;
.super Ljava/lang/Object;
.source "UpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/common/update/UpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/update/UpdateService;


# direct methods
.method constructor <init>(Lcom/infraware/common/update/UpdateService;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    :try_start_0
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->onUpdateApk()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 152
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/net/MalformedURLException;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 136
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2, v1}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 137
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v2, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 138
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    goto :goto_0

    .line 139
    .end local v0    # "e":Ljava/net/MalformedURLException;
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 141
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2, v1}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 142
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v2, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 143
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    .line 144
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 145
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2, v1}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 148
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    iput-boolean v3, v2, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 149
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService$1;->this$0:Lcom/infraware/common/update/UpdateService;

    invoke-virtual {v2}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    .line 150
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
