.class public Lcom/infraware/common/update/UpdateService;
.super Landroid/app/Service;
.source "UpdateService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/update/UpdateService$UpdateBinder;
    }
.end annotation


# static fields
.field public static final APK_FILE_NAME_FOR_INSTALL:Ljava/lang/String; = "APKFileNameForInstall"

.field public static final DOCMASTER_CONNECTION_ERROR:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_CONNECTION_ERROR"

.field public static final DOCMASTER_DISABLE_CONFIRM_VERSION:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_DISABLE_CONFIRM_VERSION"

.field public static final DOCMASTER_DOWNLOAD_COMPLETE:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_DOWNLOAD_COMPLETE"

.field public static final DOCMASTER_FILE_WRITE_ERROR:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_FILE_WRITE_ERROR"

.field public static final DOCMASTER_LATEST_VERSION:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_LATEST_VERSION"

.field public static final DOCMASTER_NETWORK_ERROR:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

.field public static final DOCMASTER_UPDATE_AVAILABE:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_UPDATE_AVAILABE"

.field public static final DOCMASTER_USER_CANCEL:Ljava/lang/String; = "com.infraware.polarisviewer5.ACTION_USER_CANCEL"

.field static final eXML_ANDROIDMAJORVERSION:I = 0x5

.field static final eXML_DEVICEINFO:I = 0x4

.field static final eXML_DOWNLOADURL:I = 0x3

.field static final eXML_FILENAME:I = 0x2

.field static final eXML_NONE:I = 0x0

.field static final eXML_VERSION:I = 0x1

.field private static parseVersion:Ljava/util/regex/Pattern;


# instance fields
.field private backgroundDownload:Ljava/lang/Runnable;

.field private backgroundUpdate:Ljava/lang/Runnable;

.field private downloadThread:Ljava/lang/Thread;

.field private final mBinder:Landroid/os/IBinder;

.field public mDownloadStop:Z

.field private mTempPath:Ljava/lang/String;

.field public mThreadStatus:Z

.field private m_dCurByte:D

.field private m_dTotalByte:D

.field private m_isCorrectAndroidVersion:Z

.field private m_needUpdate:Z

.field private mstrCheckVersionURL:Ljava/lang/String;

.field private mstrCurAndroidMajorVersion:Ljava/lang/String;

.field private mstrCurVersion:Ljava/lang/String;

.field private mstrDeviceInfo:Ljava/lang/String;

.field private mstrDownloadURL:Ljava/lang/String;

.field private mstrFileName:Ljava/lang/String;

.field private mstrNewAndroidMajorVersion:Ljava/lang/String;

.field private mstrNewVersion:Ljava/lang/String;

.field private updateThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-string/jumbo v0, "\\d{4}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/infraware/common/update/UpdateService;->parseVersion:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 38
    iput-wide v3, p0, Lcom/infraware/common/update/UpdateService;->m_dTotalByte:D

    .line 39
    iput-wide v3, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    .line 41
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->mTempPath:Ljava/lang/String;

    .line 43
    iput-boolean v2, p0, Lcom/infraware/common/update/UpdateService;->m_isCorrectAndroidVersion:Z

    .line 44
    iput-boolean v2, p0, Lcom/infraware/common/update/UpdateService;->m_needUpdate:Z

    .line 46
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    .line 47
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    .line 48
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrDownloadURL:Ljava/lang/String;

    .line 49
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrFileName:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrDeviceInfo:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrCurAndroidMajorVersion:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrNewAndroidMajorVersion:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->mstrCheckVersionURL:Ljava/lang/String;

    .line 68
    new-instance v0, Lcom/infraware/common/update/UpdateService$UpdateBinder;

    invoke-direct {v0, p0}, Lcom/infraware/common/update/UpdateService$UpdateBinder;-><init>(Lcom/infraware/common/update/UpdateService;)V

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->mBinder:Landroid/os/IBinder;

    .line 70
    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->updateThread:Ljava/lang/Thread;

    iput-object v1, p0, Lcom/infraware/common/update/UpdateService;->downloadThread:Ljava/lang/Thread;

    .line 71
    iput-boolean v2, p0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 72
    iput-boolean v2, p0, Lcom/infraware/common/update/UpdateService;->mDownloadStop:Z

    .line 130
    new-instance v0, Lcom/infraware/common/update/UpdateService$1;

    invoke-direct {v0, p0}, Lcom/infraware/common/update/UpdateService$1;-><init>(Lcom/infraware/common/update/UpdateService;)V

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->backgroundUpdate:Ljava/lang/Runnable;

    .line 156
    new-instance v0, Lcom/infraware/common/update/UpdateService$2;

    invoke-direct {v0, p0}, Lcom/infraware/common/update/UpdateService$2;-><init>(Lcom/infraware/common/update/UpdateService;)V

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->backgroundDownload:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/common/update/UpdateService;)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/update/UpdateService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/infraware/common/update/UpdateService;->onDownloadApk()V

    return-void
.end method

.method private getAndroidMajorVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 482
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 483
    .local v1, "osVer":Ljava/lang/String;
    const-string/jumbo v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 484
    .local v0, "MajorNumber":[Ljava/lang/String;
    array-length v2, v0

    if-eqz v2, :cond_0

    .line 485
    const/4 v2, 0x0

    aget-object v2, v0, v2

    .line 487
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onDownloadApk()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 183
    const/4 v4, 0x0

    .line 184
    .local v4, "intent":Landroid/content/Intent;
    new-instance v2, Ljava/net/URL;

    iget-object v8, p0, Lcom/infraware/common/update/UpdateService;->mstrDownloadURL:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 186
    .local v2, "downloadURL":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 189
    .local v7, "urlConnection":Ljava/net/HttpURLConnection;
    const/16 v8, 0x7530

    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 190
    const-string/jumbo v8, "GET"

    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->connect()V

    .line 192
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v8

    int-to-double v8, v8

    iput-wide v8, p0, Lcom/infraware/common/update/UpdateService;->m_dTotalByte:D

    .line 193
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    .line 194
    iget-object v8, p0, Lcom/infraware/common/update/UpdateService;->mstrFileName:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 195
    const-string/jumbo v8, "PolarisOffice.apk"

    iput-object v8, p0, Lcom/infraware/common/update/UpdateService;->mstrFileName:Ljava/lang/String;

    .line 197
    :cond_0
    invoke-virtual {p0}, Lcom/infraware/common/update/UpdateService;->makeOfficeDir()V

    .line 198
    new-instance v3, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/infraware/common/update/UpdateService;->mTempPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/infraware/common/update/UpdateService;->mstrFileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    .local v3, "file":Ljava/io/File;
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 201
    .local v6, "os":Ljava/io/FileOutputStream;
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 203
    .local v5, "is":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 205
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-double v8, v8

    iput-wide v8, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    .line 206
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/io/InputStream;->skip(J)J

    .line 209
    :cond_1
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v8

    int-to-double v8, v8

    iput-wide v8, p0, Lcom/infraware/common/update/UpdateService;->m_dTotalByte:D

    .line 211
    const/16 v8, 0x400

    new-array v0, v8, [B

    .line 212
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 214
    .local v1, "bufferLength":I
    :goto_0
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-lez v1, :cond_2

    .line 215
    iget-boolean v8, p0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    if-nez v8, :cond_3

    .line 216
    iput-boolean v12, p0, Lcom/infraware/common/update/UpdateService;->mDownloadStop:Z

    .line 222
    :cond_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 223
    iget-boolean v8, p0, Lcom/infraware/common/update/UpdateService;->mDownloadStop:Z

    if-ne v8, v12, :cond_4

    .line 224
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string/jumbo v8, "com.infraware.polarisviewer5.ACTION_USER_CANCEL"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 229
    .restart local v4    # "intent":Landroid/content/Intent;
    :goto_1
    invoke-virtual {p0, v4}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 230
    invoke-virtual {p0}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    .line 231
    return-void

    .line 219
    :cond_3
    const/4 v8, 0x0

    invoke-virtual {v6, v0, v8, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 220
    iget-wide v8, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    int-to-double v10, v1

    add-double/2addr v8, v10

    iput-wide v8, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    goto :goto_0

    .line 226
    :cond_4
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string/jumbo v8, "com.infraware.polarisviewer5.ACTION_DOWNLOAD_COMPLETE"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string/jumbo v8, "APKFileNameForInstall"

    iget-object v9, p0, Lcom/infraware/common/update/UpdateService;->mstrFileName:Ljava/lang/String;

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private updateProcess()V
    .locals 4

    .prologue
    .line 112
    const-string/jumbo v0, "UPDATE_SERVICE"

    const-string/jumbo v1, "updateProcess()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->backgroundUpdate:Ljava/lang/Runnable;

    const-string/jumbo v3, "update_docMaster"

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->updateThread:Ljava/lang/Thread;

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 115
    iget-object v0, p0, Lcom/infraware/common/update/UpdateService;->updateThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 117
    return-void
.end method


# virtual methods
.method public convertVersion(Ljava/lang/String;)I
    .locals 4
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 460
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 466
    :cond_0
    :goto_0
    return v0

    .line 464
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    const/4 v2, 0x2

    const/4 v3, 0x3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int v0, v1, v2

    .line 466
    .local v0, "result":I
    goto :goto_0
.end method

.method public downloadProcess()V
    .locals 4

    .prologue
    .line 121
    const-string/jumbo v0, "UPDATE_SERVICE"

    const-string/jumbo v1, "downloadProcess()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/infraware/common/update/UpdateService;->downloadThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/common/update/UpdateService;->downloadThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 124
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->backgroundDownload:Ljava/lang/Runnable;

    const-string/jumbo v3, "download_docMaster"

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->downloadThread:Ljava/lang/Thread;

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    .line 126
    iget-object v0, p0, Lcom/infraware/common/update/UpdateService;->downloadThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public getAPKCurMByte()D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 499
    iget-wide v0, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v0, v2

    mul-double/2addr v0, v4

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    int-to-double v0, v0

    div-double/2addr v0, v4

    return-wide v0
.end method

.method public getAPKTotalMByte()D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 493
    iget-wide v0, p0, Lcom/infraware/common/update/UpdateService;->m_dTotalByte:D

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v0, v2

    mul-double/2addr v0, v4

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    int-to-double v0, v0

    div-double/2addr v0, v4

    return-wide v0
.end method

.method public getOfficeBuildUiVersionNumber(Ljava/lang/String;)I
    .locals 2
    .param p1, "Version"    # Ljava/lang/String;

    .prologue
    .line 445
    if-nez p1, :cond_0

    .line 446
    const/4 v0, -0x1

    .line 447
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getOfficeBuildVersionNumber(Ljava/lang/String;)I
    .locals 2
    .param p1, "Version"    # Ljava/lang/String;

    .prologue
    .line 438
    if-nez p1, :cond_0

    .line 439
    const/4 v0, -0x1

    .line 440
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x5

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getOfficeBuildVersionType(Ljava/lang/String;)I
    .locals 1
    .param p1, "Version"    # Ljava/lang/String;

    .prologue
    .line 431
    if-nez p1, :cond_0

    .line 432
    const/4 v0, -0x1

    .line 433
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    goto :goto_0
.end method

.method public getOfficeMajorVersion(Ljava/lang/String;)I
    .locals 1
    .param p1, "Version"    # Ljava/lang/String;

    .prologue
    .line 417
    if-nez p1, :cond_0

    .line 418
    const/4 v0, -0x1

    .line 419
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    goto :goto_0
.end method

.method public getOfficeMinorVersion(Ljava/lang/String;)I
    .locals 1
    .param p1, "Version"    # Ljava/lang/String;

    .prologue
    .line 424
    if-nez p1, :cond_0

    .line 425
    const/4 v0, -0x1

    .line 426
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    goto :goto_0
.end method

.method public getVersionCheck(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 274
    :try_start_0
    invoke-virtual {p0}, Lcom/infraware/common/update/UpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/infraware/common/update/UpdateService;->httpToData(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 278
    :goto_0
    return-object v1

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 278
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public httpToData(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 282
    const/4 v2, 0x0

    .line 283
    .local v2, "c":Ljava/net/HttpURLConnection;
    const/4 v5, 0x0

    .line 285
    .local v5, "in":Ljava/io/InputStream;
    const/4 v7, 0x0

    .line 286
    .local v7, "isCorrectOfficeMajorVersion":Z
    const/4 v8, 0x0

    .line 287
    .local v8, "isCorrectOfficeMinorVersion":Z
    const/4 v6, 0x0

    .line 288
    .local v6, "isCorrectOfficeBuildType":Z
    const/4 v9, 0x0

    .line 290
    .local v9, "isNeedUpdate":Z
    const/4 v10, 0x0

    .line 292
    .local v10, "isOfficeVersionCheckOK":Z
    const/16 v21, 0x0

    .line 294
    .local v21, "strTempNewVersion":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeMajorVersion(Ljava/lang/String;)I

    move-result v15

    .line 295
    .local v15, "nCurMajorVersion":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeMinorVersion(Ljava/lang/String;)I

    move-result v16

    .line 296
    .local v16, "nCurMinorVersion":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildVersionType(Ljava/lang/String;)I

    move-result v13

    .line 297
    .local v13, "nCurBuildType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildVersionNumber(Ljava/lang/String;)I

    move-result v12

    .line 298
    .local v12, "nCurBuildNumber":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildUiVersionNumber(Ljava/lang/String;)I

    move-result v14

    .line 301
    .local v14, "nCurBuildUiNumber":I
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 302
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v18

    .line 304
    .local v18, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string/jumbo v25, "connectivity"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/net/ConnectivityManager;

    .line 305
    .local v17, "netManager":Landroid/net/ConnectivityManager;
    const/16 v25, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v11

    .line 307
    .local v11, "mobile":Landroid/net/NetworkInfo;
    const/16 v25, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v23

    .line 310
    .local v23, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v25

    if-nez v25, :cond_0

    invoke-virtual/range {v23 .. v23}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v25

    if-eqz v25, :cond_3

    .line 311
    :cond_0
    new-instance v22, Ljava/net/URL;

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 312
    .local v22, "url":Ljava/net/URL;
    invoke-virtual/range {v22 .. v22}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v25

    move-object/from16 v0, v25

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 313
    if-nez v2, :cond_1

    .line 314
    const/16 v25, 0x0

    .line 400
    .end local v22    # "url":Ljava/net/URL;
    :goto_0
    return-object v25

    .line 315
    .restart local v22    # "url":Ljava/net/URL;
    :cond_1
    const-string/jumbo v25, "GET"

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 316
    const/16 v25, 0x7530

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 317
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 318
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v20

    .line 319
    .local v20, "responseCode":I
    const/16 v25, 0xc8

    move/from16 v0, v20

    move/from16 v1, v25

    if-ne v0, v1, :cond_17

    .line 320
    invoke-virtual/range {v22 .. v22}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v5

    .line 321
    const/16 v25, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 323
    const/16 v24, 0x0

    .line 324
    .local v24, "xmlStatus":I
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v19

    .line 325
    .local v19, "parserEvent":I
    :goto_1
    const/16 v25, 0x1

    move/from16 v0, v19

    move/from16 v1, v25

    if-eq v0, v1, :cond_2

    .line 326
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    move/from16 v25, v0

    if-nez v25, :cond_4

    .line 327
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/common/update/UpdateService;->mDownloadStop:Z

    .line 393
    :cond_2
    if-eqz v2, :cond_3

    .line 394
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 400
    .end local v19    # "parserEvent":I
    .end local v20    # "responseCode":I
    .end local v22    # "url":Ljava/net/URL;
    .end local v24    # "xmlStatus":I
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    goto :goto_0

    .line 330
    .restart local v19    # "parserEvent":I
    .restart local v20    # "responseCode":I
    .restart local v22    # "url":Ljava/net/URL;
    .restart local v24    # "xmlStatus":I
    :cond_4
    packed-switch v19, :pswitch_data_0

    .line 391
    :cond_5
    :goto_3
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    goto :goto_1

    .line 332
    :pswitch_0
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "Device"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 333
    const/16 v24, 0x4

    .line 334
    const/16 v25, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrDeviceInfo:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 401
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "mobile":Landroid/net/NetworkInfo;
    .end local v17    # "netManager":Landroid/net/ConnectivityManager;
    .end local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v19    # "parserEvent":I
    .end local v20    # "responseCode":I
    .end local v22    # "url":Ljava/net/URL;
    .end local v23    # "wifi":Landroid/net/NetworkInfo;
    .end local v24    # "xmlStatus":I
    :catch_0
    move-exception v3

    .line 403
    .local v3, "e":Ljava/lang/Exception;
    if-eqz v2, :cond_6

    .line 404
    :try_start_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 405
    :cond_6
    if-eqz v5, :cond_7

    .line 406
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 407
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    if-nez v25, :cond_8

    .line 408
    const-string/jumbo v25, "1"

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 411
    :cond_8
    :goto_4
    throw v3

    .line 336
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v11    # "mobile":Landroid/net/NetworkInfo;
    .restart local v17    # "netManager":Landroid/net/ConnectivityManager;
    .restart local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v19    # "parserEvent":I
    .restart local v20    # "responseCode":I
    .restart local v22    # "url":Ljava/net/URL;
    .restart local v23    # "wifi":Landroid/net/NetworkInfo;
    .restart local v24    # "xmlStatus":I
    :cond_9
    :try_start_2
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "AndroidMajorVersion"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 337
    const/16 v24, 0x5

    goto :goto_3

    .line 338
    :cond_a
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "Version"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 339
    const/16 v24, 0x1

    goto :goto_3

    .line 340
    :cond_b
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "DownloadURL"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 341
    const/16 v24, 0x3

    goto :goto_3

    .line 342
    :cond_c
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "FileName"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 343
    const/16 v24, 0x2

    goto/16 :goto_3

    .line 349
    :pswitch_1
    const/16 v24, 0x0

    .line 350
    goto/16 :goto_3

    .line 352
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrDeviceInfo:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrDeviceInfo:Ljava/lang/String;

    move-object/from16 v25, v0

    sget-object v26, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 355
    const/16 v25, 0x5

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 357
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrNewAndroidMajorVersion:Ljava/lang/String;

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrNewAndroidMajorVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrCurAndroidMajorVersion:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/infraware/common/update/UpdateService;->m_isCorrectAndroidVersion:Z

    goto/16 :goto_3

    .line 360
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/common/update/UpdateService;->m_isCorrectAndroidVersion:Z

    move/from16 v25, v0

    if-eqz v25, :cond_14

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_14

    .line 362
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v21

    .line 364
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeMajorVersion(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    if-ne v15, v0, :cond_f

    const/4 v7, 0x1

    .line 365
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeMinorVersion(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v16

    move/from16 v1, v25

    if-ne v0, v1, :cond_10

    const/4 v8, 0x1

    .line 366
    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildVersionType(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    if-ne v13, v0, :cond_11

    const/4 v6, 0x1

    .line 367
    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildVersionNumber(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    if-lt v12, v0, :cond_e

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildVersionNumber(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    if-ne v12, v0, :cond_12

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/infraware/common/update/UpdateService;->getOfficeBuildUiVersionNumber(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    if-ge v14, v0, :cond_12

    :cond_e
    const/4 v9, 0x1

    .line 371
    :goto_8
    if-eqz v6, :cond_13

    if-eqz v7, :cond_13

    if-eqz v8, :cond_13

    if-eqz v9, :cond_13

    const/4 v10, 0x1

    .line 373
    :goto_9
    if-eqz v10, :cond_5

    .line 375
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    .line 376
    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/infraware/common/update/UpdateService;->m_needUpdate:Z

    goto/16 :goto_3

    .line 364
    :cond_f
    const/4 v7, 0x0

    goto :goto_5

    .line 365
    :cond_10
    const/4 v8, 0x0

    goto :goto_6

    .line 366
    :cond_11
    const/4 v6, 0x0

    goto :goto_7

    .line 367
    :cond_12
    const/4 v9, 0x0

    goto :goto_8

    .line 371
    :cond_13
    const/4 v10, 0x0

    goto :goto_9

    .line 381
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/common/update/UpdateService;->m_isCorrectAndroidVersion:Z

    move/from16 v25, v0

    if-eqz v25, :cond_15

    if-eqz v10, :cond_15

    const/16 v25, 0x3

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_15

    .line 382
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrDownloadURL:Ljava/lang/String;

    goto/16 :goto_3

    .line 383
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/infraware/common/update/UpdateService;->m_isCorrectAndroidVersion:Z

    move/from16 v25, v0

    if-eqz v25, :cond_5

    if-eqz v10, :cond_5

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    .line 384
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrFileName:Ljava/lang/String;

    goto/16 :goto_3

    .line 386
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    move-object/from16 v25, v0

    if-nez v25, :cond_5

    .line 387
    const-string/jumbo v25, "1"

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    goto/16 :goto_3

    .line 396
    .end local v19    # "parserEvent":I
    .end local v24    # "xmlStatus":I
    :cond_17
    if-eqz v2, :cond_3

    .line 397
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 409
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "mobile":Landroid/net/NetworkInfo;
    .end local v17    # "netManager":Landroid/net/ConnectivityManager;
    .end local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v20    # "responseCode":I
    .end local v22    # "url":Ljava/net/URL;
    .end local v23    # "wifi":Landroid/net/NetworkInfo;
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v25

    goto/16 :goto_4

    .line 330
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isDownloadFinished()Z
    .locals 4

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/infraware/common/update/UpdateService;->m_dTotalByte:D

    iget-wide v2, p0, Lcom/infraware/common/update/UpdateService;->m_dCurByte:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadCurrentVersion()Ljava/lang/String;
    .locals 4

    .prologue
    .line 451
    const/4 v1, 0x0

    .line 452
    .local v1, "returnVersion":Ljava/lang/String;
    sget-object v2, Lcom/infraware/common/update/UpdateService;->parseVersion:Ljava/util/regex/Pattern;

    iget-object v3, p0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 453
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 454
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 456
    :cond_0
    return-object v1
.end method

.method protected makeOfficeDir()V
    .locals 3

    .prologue
    .line 477
    invoke-virtual {p0}, Lcom/infraware/common/update/UpdateService;->makeRootDir()V

    .line 478
    sget-object v0, Lcom/infraware/common/define/CMModelDefine;->CUSTOM_TEMP_PATH:Ljava/lang/String;

    const-string/jumbo v1, "polarisTemp"

    sget-object v2, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->TEMP_PATH:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/infraware/common/util/FileUtils;->createCustomPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->mTempPath:Ljava/lang/String;

    .line 479
    return-void
.end method

.method protected makeRootDir()V
    .locals 2

    .prologue
    .line 472
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 473
    sget-object v0, Lcom/infraware/common/define/CMDefine$OfficeDefaultPath;->OFFICE_ROOT_PATH:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/infraware/common/util/FileUtils;->makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;

    .line 474
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 83
    const-string/jumbo v0, "DOWNLOAD_URL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->mstrCheckVersionURL:Ljava/lang/String;

    .line 84
    const-string/jumbo v0, "CURRENT_VER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->mstrCurVersion:Ljava/lang/String;

    .line 85
    invoke-direct {p0}, Lcom/infraware/common/update/UpdateService;->getAndroidMajorVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/update/UpdateService;->mstrCurAndroidMajorVersion:Ljava/lang/String;

    .line 87
    const-string/jumbo v0, "UPDATE_SERVICE"

    const-string/jumbo v1, "onBind()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->USE_LIVE_UPDATE()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/infraware/common/update/UpdateService;->mThreadStatus:Z

    if-nez v0, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/infraware/common/update/UpdateService;->updateProcess()V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/update/UpdateService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 80
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 96
    const-string/jumbo v0, "UPDATE_SERVICE"

    const-string/jumbo v1, "onUnbind()"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method public onUpdateApk()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 237
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->mstrCheckVersionURL:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/infraware/common/update/UpdateService;->getVersionCheck(Ljava/lang/String;)Ljava/lang/String;

    .line 239
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->mstrCheckVersionURL:Ljava/lang/String;

    const-string/jumbo v3, "https://doc2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->mstrDownloadURL:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 242
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->mstrDownloadURL:Ljava/lang/String;

    const-string/jumbo v3, "doc1"

    const-string/jumbo v4, "doc2"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/infraware/common/update/UpdateService;->mstrDownloadURL:Ljava/lang/String;

    .line 246
    :cond_0
    iget-object v2, p0, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/infraware/common/update/UpdateService;->getOfficeMajorVersion(Ljava/lang/String;)I

    move-result v1

    .line 250
    .local v1, "newVersion":I
    iget-boolean v2, p0, Lcom/infraware/common/update/UpdateService;->m_isCorrectAndroidVersion:Z

    if-eqz v2, :cond_3

    .line 251
    iget-boolean v2, p0, Lcom/infraware/common/update/UpdateService;->m_needUpdate:Z

    if-eqz v2, :cond_1

    .line 253
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_UPDATE_AVAILABE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 254
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "NEW_VERSION"

    iget-object v3, p0, Lcom/infraware/common/update/UpdateService;->mstrNewVersion:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 268
    :goto_0
    invoke-virtual {p0, v0}, Lcom/infraware/common/update/UpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 269
    invoke-virtual {p0}, Lcom/infraware/common/update/UpdateService;->stopSelf()V

    .line 270
    return-void

    .line 257
    :cond_1
    if-nez v1, :cond_2

    .line 259
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_NETWORK_ERROR"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 263
    :cond_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_LATEST_VERSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 266
    :cond_3
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.infraware.polarisviewer5.ACTION_LATEST_VERSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method
