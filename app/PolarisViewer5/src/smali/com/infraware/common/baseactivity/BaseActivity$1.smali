.class Lcom/infraware/common/baseactivity/BaseActivity$1;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/infraware/common/baseactivity/BaseActivity;->onCreateOnKKOver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/infraware/common/baseactivity/BaseActivity;


# direct methods
.method constructor <init>(Lcom/infraware/common/baseactivity/BaseActivity;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/infraware/common/baseactivity/BaseActivity$1;->this$0:Lcom/infraware/common/baseactivity/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 2
    .param p1, "toMulti"    # Z

    .prologue
    .line 182
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity$1;->this$0:Lcom/infraware/common/baseactivity/BaseActivity;

    # getter for: Lcom/infraware/common/baseactivity/BaseActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/infraware/common/baseactivity/BaseActivity;->access$000(Lcom/infraware/common/baseactivity/BaseActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 183
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity$1;->this$0:Lcom/infraware/common/baseactivity/BaseActivity;

    # invokes: Lcom/infraware/common/baseactivity/BaseActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/common/baseactivity/BaseActivity;->access$100(Lcom/infraware/common/baseactivity/BaseActivity;Z)V

    .line 184
    return-void

    .line 182
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "arg0"    # Landroid/graphics/Rect;

    .prologue
    .line 176
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity$1;->this$0:Lcom/infraware/common/baseactivity/BaseActivity;

    # getter for: Lcom/infraware/common/baseactivity/BaseActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/infraware/common/baseactivity/BaseActivity;->access$000(Lcom/infraware/common/baseactivity/BaseActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 177
    .local v0, "bSplit":Z
    :goto_0
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity$1;->this$0:Lcom/infraware/common/baseactivity/BaseActivity;

    # invokes: Lcom/infraware/common/baseactivity/BaseActivity;->onMultiWindowStatusChanged(Z)V
    invoke-static {v1, v0}, Lcom/infraware/common/baseactivity/BaseActivity;->access$100(Lcom/infraware/common/baseactivity/BaseActivity;Z)V

    .line 178
    return-void

    .line 176
    .end local v0    # "bSplit":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 172
    return-void
.end method
