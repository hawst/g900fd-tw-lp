.class public Lcom/infraware/common/baseactivity/BaseWebView;
.super Landroid/app/Activity;
.source "BaseWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/common/baseactivity/BaseWebView$1;,
        Lcom/infraware/common/baseactivity/BaseWebView$BaseWebViewClient;
    }
.end annotation


# instance fields
.field public m_oWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseWebView;->m_oWebView:Landroid/webkit/WebView;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseWebView;->setContentView(I)V

    .line 30
    const v0, 0x7f0b003d

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseWebView;->m_oWebView:Landroid/webkit/WebView;

    .line 31
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseWebView;->m_oWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 32
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseWebView;->m_oWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/infraware/common/baseactivity/BaseWebView$BaseWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/infraware/common/baseactivity/BaseWebView$BaseWebViewClient;-><init>(Lcom/infraware/common/baseactivity/BaseWebView;Lcom/infraware/common/baseactivity/BaseWebView$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 33
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 38
    packed-switch p1, :pswitch_data_0

    .line 47
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 41
    :pswitch_0
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseWebView;->m_oWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseWebView;->m_oWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 44
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
