.class public abstract Lcom/infraware/common/baseactivity/BaseTabActivity;
.super Landroid/app/TabActivity;
.source "BaseTabActivity.java"

# interfaces
.implements Lcom/infraware/common/event/SdCardListener;


# instance fields
.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    .line 16
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nLocaleType:I

    .line 17
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    return-void
.end method


# virtual methods
.method public getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 40
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 41
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 42
    return-object v0
.end method

.method public isSdCardAction(Ljava/lang/String;)V
    .locals 3
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 87
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "SdCardUnMounted"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 88
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->setResult(ILandroid/content/Intent;)V

    .line 89
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->finish()V

    .line 90
    return-void
.end method

.method public onChangeScreen(I)V
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 63
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 67
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 47
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 48
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nLocaleType:I

    if-eq v1, v0, :cond_0

    .line 49
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nLocaleType:I

    .line 50
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iget v2, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nLocaleType:I

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetLocale(I)V

    .line 51
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nLocaleType:I

    invoke-virtual {p0, v1}, Lcom/infraware/common/baseactivity/BaseTabActivity;->onLocaleChange(I)V

    .line 54
    :cond_0
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    .line 55
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    .line 56
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    invoke-virtual {p0, v1}, Lcom/infraware/common/baseactivity/BaseTabActivity;->onChangeScreen(I)V

    .line 59
    :cond_1
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 60
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nLocaleType:I

    .line 23
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    .line 24
    iget v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->onChangeScreen(I)V

    .line 26
    new-instance v0, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v0}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 27
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v0, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 28
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/infraware/common/baseactivity/BaseTabActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 30
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 36
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 76
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 78
    invoke-super {p0}, Landroid/app/TabActivity;->onDestroy()V

    .line 79
    return-void
.end method

.method public abstract onLocaleChange(I)V
.end method

.method public onPivotLock(I)V
    .locals 0
    .param p1, "nMode"    # I

    .prologue
    .line 81
    return-void
.end method

.method protected setResultFromSettingActivity()V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseTabActivity;->m_nOrientation:I

    .line 71
    return-void
.end method
