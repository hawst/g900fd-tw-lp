.class public Lcom/infraware/common/baseactivity/BaseSwitchableActivity;
.super Lcom/infraware/common/baseactivity/BaseActivity;
.source "BaseSwitchableActivity.java"


# instance fields
.field private mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

.field protected mEvInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

.field private mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

.field private mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

.field private mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

.field private mEvPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

.field private mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

.field private mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

.field protected mInterfaceHandle:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mInterfaceHandle:I

    .line 16
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .line 17
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    .line 18
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    .line 19
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    .line 20
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    .line 21
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    .line 22
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPrintListener:Lcom/infraware/office/evengine/EvListener$PrintListener;

    .line 23
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPdfAnnotListener:Lcom/infraware/office/evengine/EvListener$PdfAnnotListener;

    .line 27
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 28
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->getJNIInterfaceHandleValue()I

    move-result v0

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mInterfaceHandle:I

    .line 31
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->getEvLister()V

    goto :goto_0
.end method

.method private getEvLister()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetDvLister()Lcom/infraware/office/evengine/EvListener$ViewerListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .line 40
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEvLister()Lcom/infraware/office/evengine/EvListener$EditorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    .line 41
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEvWLister()Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    .line 42
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEvPLister()Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    .line 43
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEvSLister()Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    .line 44
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->IGetEvPDFLister()Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    goto :goto_0
.end method


# virtual methods
.method public onLocaleChange(I)V
    .locals 0
    .param p1, "nLocale"    # I

    .prologue
    .line 63
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    .line 50
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mInterfaceHandle:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvInterface;->SetInterfaceHandleAddress(I)V

    .line 52
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvInterface:Lcom/infraware/office/evengine/EvInterface;

    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    iget-object v2, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvListener:Lcom/infraware/office/evengine/EvListener$EditorListener;

    iget-object v3, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvWordListener:Lcom/infraware/office/evengine/EvListener$WordEditorListener;

    iget-object v4, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPptListener:Lcom/infraware/office/evengine/EvListener$PptEditorListener;

    iget-object v5, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvSheetListener:Lcom/infraware/office/evengine/EvListener$SheetEditorListener;

    iget-object v6, p0, Lcom/infraware/common/baseactivity/BaseSwitchableActivity;->mEvPdfViewerListener:Lcom/infraware/office/evengine/EvListener$PdfViewerListener;

    invoke-virtual/range {v0 .. v6}, Lcom/infraware/office/evengine/EvInterface;->ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;Lcom/infraware/office/evengine/EvListener$EditorListener;Lcom/infraware/office/evengine/EvListener$WordEditorListener;Lcom/infraware/office/evengine/EvListener$PptEditorListener;Lcom/infraware/office/evengine/EvListener$SheetEditorListener;Lcom/infraware/office/evengine/EvListener$PdfViewerListener;)V

    .line 57
    :cond_0
    invoke-super {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onResume()V

    .line 58
    return-void
.end method
