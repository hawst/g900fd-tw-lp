.class public abstract Lcom/infraware/common/baseactivity/BaseListActivity;
.super Landroid/app/ListActivity;
.source "BaseListActivity.java"

# interfaces
.implements Lcom/infraware/common/event/SdCardListener;


# instance fields
.field protected mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 17
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nLocaleType:I

    .line 18
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    .line 19
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 20
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    return-void
.end method


# virtual methods
.method public getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 45
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 46
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 47
    return-object v0
.end method

.method public isSdCardAction(Ljava/lang/String;)V
    .locals 3
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 111
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 112
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "SdCardUnMounted"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/infraware/common/baseactivity/BaseListActivity;->setResult(ILandroid/content/Intent;)V

    .line 114
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseListActivity;->finish()V

    .line 115
    return-void
.end method

.method public onChangeScreen(I)V
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 77
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 81
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 90
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 53
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 54
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nLocaleType:I

    if-eq v1, v0, :cond_0

    .line 55
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nLocaleType:I

    .line 56
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iget v2, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nLocaleType:I

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetLocale(I)V

    .line 57
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nLocaleType:I

    invoke-virtual {p0, v1}, Lcom/infraware/common/baseactivity/BaseListActivity;->onLocaleChange(I)V

    .line 59
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->onLocaleChanged()V

    .line 64
    :cond_0
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    .line 65
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    .line 66
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    invoke-virtual {p0, v1}, Lcom/infraware/common/baseactivity/BaseListActivity;->onChangeScreen(I)V

    .line 68
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-eqz v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    iget v2, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    invoke-virtual {v1, v2}, Lcom/infraware/office/actionbar/ActionTitleBar;->changeActionTitleBarHeight(I)V

    .line 72
    :cond_1
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nLocaleType:I

    .line 25
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    .line 26
    iget v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseListActivity;->onChangeScreen(I)V

    .line 28
    new-instance v0, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v0}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 29
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v0, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 30
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseListActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/infraware/common/baseactivity/BaseListActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 32
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 41
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseListActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 99
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 103
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 104
    return-void
.end method

.method public abstract onLocaleChange(I)V
.end method

.method public onPivotLock(I)V
    .locals 0
    .param p1, "nMode"    # I

    .prologue
    .line 106
    return-void
.end method

.method protected setResultFromSettingActivity()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseListActivity;->m_nOrientation:I

    .line 94
    return-void
.end method
