.class public abstract Lcom/infraware/common/baseactivity/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/infraware/common/event/SdCardListener;


# static fields
.field static final DOWNLOAD_CANCEL:I = 0x1

.field static final DOWNLOAD_CONFIRM_ERROR:I = 0x4

.field static final DOWNLOAD_LATEST_VERSION:I = 0x2

.field static final DOWNLOAD_NETWORK_ERROR:I = 0x3

.field static final DOWNLOAD_SUCCESS:I


# instance fields
.field public mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

.field public mIsMultiWindow:Z

.field private mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private m_nLocaleType:I

.field private m_nOrientation:I

.field private m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nLocaleType:I

    .line 26
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    .line 27
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 28
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    .line 36
    iput-boolean v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    .line 38
    iput-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-void
.end method

.method static synthetic access$000(Lcom/infraware/common/baseactivity/BaseActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/infraware/common/baseactivity/BaseActivity;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/infraware/common/baseactivity/BaseActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/infraware/common/baseactivity/BaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/infraware/common/baseactivity/BaseActivity;->onMultiWindowStatusChanged(Z)V

    return-void
.end method

.method private onCreateOnKKOver()V
    .locals 2

    .prologue
    .line 168
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 170
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    new-instance v1, Lcom/infraware/common/baseactivity/BaseActivity$1;

    invoke-direct {v1, p0}, Lcom/infraware/common/baseactivity/BaseActivity$1;-><init>(Lcom/infraware/common/baseactivity/BaseActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 186
    return-void
.end method

.method private onMultiWindowStatusChanged(Z)V
    .locals 4
    .param p1, "mIsMaximized"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 195
    .local v0, "isMW":Z
    :goto_0
    iget-boolean v3, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    if-ne v3, v0, :cond_2

    .line 196
    const-string/jumbo v1, "onMultiWindowStatusChanged"

    const-string/jumbo v2, "Even"

    invoke-static {v1, v2}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_0
    :goto_1
    return-void

    .end local v0    # "isMW":Z
    :cond_1
    move v0, v2

    .line 194
    goto :goto_0

    .line 199
    .restart local v0    # "isMW":Z
    :cond_2
    iget-boolean v3, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 200
    const-string/jumbo v2, "onMultiWindowStatusChanged"

    const-string/jumbo v3, "Normal -> Multi"

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onChangedFromNormalWindowToMultiWindow()V

    .line 202
    iput-boolean v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    goto :goto_1

    .line 203
    :cond_3
    iget-boolean v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 204
    const-string/jumbo v1, "c"

    const-string/jumbo v3, "Multi -> Normal"

    invoke-static {v1, v3}, Lcom/infraware/common/util/CMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onChangedFromMultiwindowToNormalWindow()V

    .line 206
    iput-boolean v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    goto :goto_1
.end method


# virtual methods
.method public getIcon(Ljava/lang/String;)I
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 154
    const v0, 0x7f020069

    .line 155
    .local v0, "icon":I
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    const v0, 0x7f020067

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 157
    :cond_1
    const-string/jumbo v1, "/storage/extSdCard"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 158
    const v0, 0x7f020069

    goto :goto_0

    .line 159
    :cond_2
    const-string/jumbo v1, "/storage/UsbDrive"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 160
    const v0, 0x7f02006a

    goto :goto_0

    .line 161
    :cond_3
    const-string/jumbo v1, "/storage/PersonalPage"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    const v0, 0x7f020068

    goto :goto_0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    return v0
.end method

.method public getSdcardIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 77
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 79
    return-object v0
.end method

.method public declared-synchronized isSdCardAction(Ljava/lang/String;)V
    .locals 3
    .param p1, "nowAction"    # Ljava/lang/String;

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 143
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "SdCardUnMounted"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 144
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/infraware/common/baseactivity/BaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 145
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    monitor-exit p0

    return-void

    .line 142
    .end local v0    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onChangeScreen(I)V
    .locals 1
    .param p1, "nType"    # I

    .prologue
    .line 106
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/infraware/common/util/Utils;->setScreenMode(Landroid/view/Window;I)V

    .line 111
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v0}, Lcom/infraware/office/actionbar/ActionTitleBar;->show()V

    .line 121
    :cond_1
    return-void
.end method

.method protected onChangedFromMultiwindowToNormalWindow()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method protected onChangedFromNormalWindowToMultiWindow()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->onLocaleChanged()V

    .line 87
    iget-object v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mActionTitleBar:Lcom/infraware/office/actionbar/ActionTitleBar;

    invoke-virtual {v1}, Lcom/infraware/office/actionbar/ActionTitleBar;->changeActionTitleBarHeight()V

    .line 90
    :cond_0
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Lcom/infraware/common/util/Utils;->getLocaleType(Ljava/util/Locale;)I

    move-result v0

    .line 91
    .local v0, "nlocale":I
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nLocaleType:I

    if-eq v1, v0, :cond_1

    .line 92
    iput v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nLocaleType:I

    .line 93
    invoke-static {}, Lcom/infraware/office/evengine/EvInterface;->getInterface()Lcom/infraware/office/evengine/EvInterface;

    move-result-object v1

    iget v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nLocaleType:I

    invoke-virtual {v1, v2}, Lcom/infraware/office/evengine/EvInterface;->ISetLocale(I)V

    .line 94
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nLocaleType:I

    invoke-virtual {p0, v1}, Lcom/infraware/common/baseactivity/BaseActivity;->onLocaleChange(I)V

    .line 97
    :cond_1
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_2

    .line 98
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    .line 99
    iget v1, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    invoke-virtual {p0, v1}, Lcom/infraware/common/baseactivity/BaseActivity;->onChangeScreen(I)V

    .line 102
    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 103
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/infraware/common/util/Utils;->getCurrentLocaleType(Landroid/content/res/Resources;)I

    move-result v2

    iput v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nLocaleType:I

    .line 43
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    .line 44
    iget v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    invoke-virtual {p0, v2}, Lcom/infraware/common/baseactivity/BaseActivity;->onChangeScreen(I)V

    .line 46
    new-instance v2, Lcom/infraware/common/event/SdCardEvent;

    invoke-direct {v2}, Lcom/infraware/common/event/SdCardEvent;-><init>()V

    iput-object v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    .line 47
    iget-object v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {v2, p0}, Lcom/infraware/common/event/SdCardEvent;->setSdListener(Lcom/infraware/common/event/SdCardListener;)V

    .line 48
    iget-object v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->getSdcardIntentFilter()Landroid/content/IntentFilter;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/infraware/common/baseactivity/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 55
    .local v0, "start":J
    const-string/jumbo v2, "***"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "BaseActivity start:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/infraware/common/util/CMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/infraware/common/util/Utils;->isICS()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/infraware/polarisoffice5/common/HoveringManager;->setHoveringButton(Landroid/app/Activity;)Z

    .line 59
    :cond_0
    invoke-static {}, Lcom/infraware/common/define/CMModelDefine$B;->HIDE_INDICATOR_BAR()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    invoke-static {p0}, Lcom/infraware/common/util/Utils;->fullScreenActivity(Landroid/app/Activity;)V

    .line 64
    :cond_1
    invoke-static {}, Lcom/infraware/common/util/Utils;->isKKOver()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    invoke-direct {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->onCreateOnKKOver()V

    .line 69
    :cond_2
    invoke-static {}, Lcom/infraware/common/util/Utils;->isL()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 70
    const v2, 0x103012c

    invoke-virtual {p0, v2}, Lcom/infraware/common/baseactivity/BaseActivity;->setTheme(I)V

    .line 72
    :cond_3
    invoke-static {}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->getInstance()Lcom/infraware/common/multiwindow/MultiWindowWrapper;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/infraware/common/multiwindow/MultiWindowWrapper;->isNormalWindow(Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/infraware/common/baseactivity/BaseActivity;->mIsMultiWindow:Z

    .line 73
    return-void

    .line 72
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_oSDReceiver:Lcom/infraware/common/event/SdCardEvent;

    invoke-virtual {p0, v0}, Lcom/infraware/common/baseactivity/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 130
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 134
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 135
    return-void
.end method

.method public abstract onLocaleChange(I)V
.end method

.method public onPivotLock(I)V
    .locals 0
    .param p1, "nMode"    # I

    .prologue
    .line 137
    return-void
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 3
    .param p1, "isMaximized"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    .line 189
    const-string/jumbo v0, "multiwindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onWindowStatusChanged(max["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]min["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]pinup["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/infraware/common/baseactivity/BaseActivity;->onMultiWindowStatusChanged(Z)V

    .line 191
    return-void

    .line 190
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setResultFromSettingActivity()V
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/infraware/common/baseactivity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/infraware/common/baseactivity/BaseActivity;->m_nOrientation:I

    .line 125
    return-void
.end method
