.class public Lcom/diotek/diodict/engine/DBTypes;
.super Ljava/lang/Object;
.source "DBTypes.java"


# static fields
.field public static final DEDT_BERLITZ_DATOENG:I = 0x80c

.field public static final DEDT_BERLITZ_ENGTODA:I = 0x80d

.field public static final DEDT_BERLITZ_ENGTOFI:I = 0x80f

.field public static final DEDT_BERLITZ_ENGTONL:I = 0x811

.field public static final DEDT_BERLITZ_ENGTONO:I = 0x813

.field public static final DEDT_BERLITZ_ENGTOSV:I = 0x815

.field public static final DEDT_BERLITZ_ENGTOTUR:I = 0x80b

.field public static final DEDT_BERLITZ_FITOENG:I = 0x80e

.field public static final DEDT_BERLITZ_NLTOENG:I = 0x810

.field public static final DEDT_BERLITZ_NOTOENG:I = 0x812

.field public static final DEDT_BERLITZ_STANDARD_ENGTUR:I = 0x817

.field public static final DEDT_BERLITZ_STANDARD_TURENG:I = 0x816

.field public static final DEDT_BERLITZ_SVTOENG:I = 0x814

.field public static final DEDT_BERLITZ_TURTOENG:I = 0x80a

.field public static final DEDT_BRITANNICA_CONCISE:I = 0xfe01

.field public static final DEDT_COLLINS_ARABTOENG:I = 0x128

.field public static final DEDT_COLLINS_ENGGRAMMAR:I = 0x103

.field public static final DEDT_COLLINS_ENGIDIOM:I = 0x105

.field public static final DEDT_COLLINS_ENGTHES:I = 0x101

.field public static final DEDT_COLLINS_ENGTOARAB:I = 0x127

.field public static final DEDT_COLLINS_ENGTOENG:I = 0x100

.field public static final DEDT_COLLINS_ENGTOENGCHNJPNKOR:I = 0x120

.field public static final DEDT_COLLINS_ENGTOFRA:I = 0x10c

.field public static final DEDT_COLLINS_ENGTOGER:I = 0x10a

.field public static final DEDT_COLLINS_ENGTOGRE:I = 0x116

.field public static final DEDT_COLLINS_ENGTOITA:I = 0x10e

.field public static final DEDT_COLLINS_ENGTOMAS:I = 0x110

.field public static final DEDT_COLLINS_ENGTOPIC:I = 0x104

.field public static final DEDT_COLLINS_ENGTOPOL:I = 0x118

.field public static final DEDT_COLLINS_ENGTOPOR:I = 0x108

.field public static final DEDT_COLLINS_ENGTOSPN:I = 0x106

.field public static final DEDT_COLLINS_ENGUSAGE:I = 0x102

.field public static final DEDT_COLLINS_FRATOENG:I = 0x10d

.field public static final DEDT_COLLINS_GERTOENG:I = 0x10b

.field public static final DEDT_COLLINS_GRETOENG:I = 0x117

.field public static final DEDT_COLLINS_ITATOENG:I = 0x10f

.field public static final DEDT_COLLINS_KORTOTHA:I = 0x112

.field public static final DEDT_COLLINS_KORTOVIE:I = 0x114

.field public static final DEDT_COLLINS_MASTOENG:I = 0x111

.field public static final DEDT_COLLINS_POLTOENG:I = 0x119

.field public static final DEDT_COLLINS_PORTOENG:I = 0x109

.field public static final DEDT_COLLINS_SMALL_ARABTOENG:I = 0x126

.field public static final DEDT_COLLINS_SMALL_ENGTOARAB:I = 0x125

.field public static final DEDT_COLLINS_SPNTOENG:I = 0x107

.field public static final DEDT_COLLINS_THATOKOR:I = 0x113

.field public static final DEDT_COLLINS_UNABRIDGED_ENGTOSPA:I = 0x121

.field public static final DEDT_COLLINS_UNABRIDGED_SPATOENG:I = 0x122

.field public static final DEDT_COLLINS_UNA_ENGTOGER:I = 0x123

.field public static final DEDT_COLLINS_UNA_GERTOENG:I = 0x124

.field public static final DEDT_COLLINS_VIETOKOR:I = 0x115

.field public static final DEDT_DD_ALZZA_BUSINESS_WORD:I = 0x60e

.field public static final DEDT_DD_CHNTOKOR:I = 0x607

.field public static final DEDT_DD_ENCYBER_HISTORY_CHN:I = 0x655

.field public static final DEDT_DD_ENCYBER_HISTORY_EASTERN:I = 0x658

.field public static final DEDT_DD_ENCYBER_HISTORY_FRA:I = 0x656

.field public static final DEDT_DD_ENCYBER_HISTORY_GER:I = 0x650

.field public static final DEDT_DD_ENCYBER_HISTORY_KOR:I = 0x657

.field public static final DEDT_DD_ENCYBER_HISTORY_RUS:I = 0x651

.field public static final DEDT_DD_ENCYBER_HISTORY_UK:I = 0x654

.field public static final DEDT_DD_ENCYBER_HISTORY_US:I = 0x652

.field public static final DEDT_DD_ENCYBER_HISTORY_WESTERN:I = 0x653

.field public static final DEDT_DD_ENCYBER_HISTORY_WORLD:I = 0x659

.field public static final DEDT_DD_ENGTOKOR:I = 0x600

.field public static final DEDT_DD_ENGTOKOR_EXAMPLE:I = 0x603

.field public static final DEDT_DD_ENGTOKOR_IDIOM:I = 0x602

.field public static final DEDT_DD_FRATOKOR:I = 0x612

.field public static final DEDT_DD_GENX_SUNUNG_ENGTOKOR:I = 0x60c

.field public static final DEDT_DD_GERTOKOR:I = 0x614

.field public static final DEDT_DD_JPNTOKOR:I = 0x60a

.field public static final DEDT_DD_KANJITOKOR:I = 0x60b

.field public static final DEDT_DD_KORTOCHN:I = 0x606

.field public static final DEDT_DD_KORTOENG:I = 0x601

.field public static final DEDT_DD_KORTOFRA:I = 0x611

.field public static final DEDT_DD_KORTOGER:I = 0x613

.field public static final DEDT_DD_KORTOJPN:I = 0x609

.field public static final DEDT_DD_KORTOKOR:I = 0x604

.field public static final DEDT_DD_OLDKOR:I = 0x605

.field public static final DEDT_DD_PINYINTOKOR:I = 0x608

.field public static final DEDT_DD_TOEIC_KONGNAMUL_WORD:I = 0x60d

.field public static final DEDT_DD_YONSEI_ENGTOKOR:I = 0x610

.field public static final DEDT_DD_YONSEI_KORTOKOR:I = 0x60f

.field public static final DEDT_DRWIT_ENGTOTHA:I = 0xa1c

.field public static final DEDT_DRWIT_POCKET_ENGTOTHA:I = 0xa1a

.field public static final DEDT_DRWIT_POCKET_THATOENG:I = 0xa1b

.field public static final DEDT_DRWIT_THATOENG:I = 0xa1d

.field public static final DEDT_FNAG_ENGTOGLE:I = 0xa13

.field public static final DEDT_FNAG_GLETOENG:I = 0xa14

.field public static final DEDT_GRAMEDIA_ENGTOIND:I = 0xa02

.field public static final DEDT_GRAMEDIA_INDTOENG:I = 0xa03

.field public static final DEDT_GUMMERUS_ENG_FIN:I = 0xa24

.field public static final DEDT_GUMMERUS_FIN_ENG:I = 0xa25

.field public static final DEDT_GUMMERUS_POCKET_ENG_FIN:I = 0xa22

.field public static final DEDT_GUMMERUS_POCKET_FIN_ENG:I = 0xa23

.field public static final DEDT_GYLDENDAL_DEN_ENG:I = 0xa2b

.field public static final DEDT_GYLDENDAL_ENG_DEN:I = 0xa2a

.field public static final DEDT_GYLDENDAL_MINI_DEN_ENG:I = 0xa2d

.field public static final DEDT_GYLDENDAL_MINI_ENG_DEN:I = 0xa2c

.field public static final DEDT_KSSP_ENGTOSIMP:I = 0x400

.field public static final DEDT_KSSP_ENGTOSIMPTRAD:I = 0x408

.field public static final DEDT_KSSP_ENGTOSIMPTRAD_SIMP:I = 0x408

.field public static final DEDT_KSSP_ENGTOSIMPTRAD_TRAD:I = 0x409

.field public static final DEDT_KSSP_ENGTOTRAD:I = 0x404

.field public static final DEDT_KSSP_INITIAL_SIMPTOENG:I = 0x403

.field public static final DEDT_KSSP_INITIAL_SIMPTRADTOENG_SIMP:I = 0x40d

.field public static final DEDT_KSSP_INITIAL_SIMPTRADTOENG_TRAD:I = 0x40f

.field public static final DEDT_KSSP_INITIAL_TRADTOENG:I = 0x407

.field public static final DEDT_KSSP_PINYIN_SIMPTOENG:I = 0x402

.field public static final DEDT_KSSP_PINYIN_SIMPTRADTOENG_SIMP:I = 0x40c

.field public static final DEDT_KSSP_PINYIN_SIMPTRADTOENG_TRAD:I = 0x40e

.field public static final DEDT_KSSP_PINYIN_TRADTOENG:I = 0x406

.field public static final DEDT_KSSP_SIMPTOENG:I = 0x401

.field public static final DEDT_KSSP_SIMPTRADTOENG:I = 0x40a

.field public static final DEDT_KSSP_SIMPTRADTOENG_SIMP:I = 0x40a

.field public static final DEDT_KSSP_SIMPTRADTOENG_TRAD:I = 0x40b

.field public static final DEDT_KSSP_TRADTOENG:I = 0x405

.field public static final DEDT_LACVIET_ENGTOVIE:I = 0xa04

.field public static final DEDT_LACVIET_KORTOVIE:I = 0xa15

.field public static final DEDT_LACVIET_VIETOENG:I = 0xa05

.field public static final DEDT_LACVIET_VIETOKOR:I = 0xa16

.field public static final DEDT_LANGEN_ENGTOGER:I = 0x800

.field public static final DEDT_LANGEN_FRATOGER:I = 0x802

.field public static final DEDT_LANGEN_GERTOENG:I = 0x801

.field public static final DEDT_LANGEN_GERTOFRA:I = 0x803

.field public static final DEDT_LANGEN_GERTOITA:I = 0x805

.field public static final DEDT_LANGEN_GERTOSPN:I = 0x807

.field public static final DEDT_LANGEN_GERTOTUR:I = 0x809

.field public static final DEDT_LANGEN_ITATOGER:I = 0x804

.field public static final DEDT_LANGEN_SPNTOGER:I = 0x806

.field public static final DEDT_LANGEN_TURTOGER:I = 0x808

.field public static final DEDT_LDSOFT_ENGTOSIMP:I = 0xfe04

.field public static final DEDT_LDSOFT_ENGTOSIMPTRAD:I = 0xfe08

.field public static final DEDT_LDSOFT_ENGTOTRAD:I = 0xfe06

.field public static final DEDT_LDSOFT_SIMPTOENG:I = 0xfe05

.field public static final DEDT_LDSOFT_SIMPTRADTOENG:I = 0xfe09

.field public static final DEDT_LDSOFT_TRADTOENG:I = 0xfe07

.field public static final DEDT_LINGVO_ENGTORUS:I = 0x200

.field public static final DEDT_LINGVO_ENGTOUKR:I = 0x204

.field public static final DEDT_LINGVO_GERTORUS:I = 0x207

.field public static final DEDT_LINGVO_RUSTOENG:I = 0x201

.field public static final DEDT_LINGVO_RUSTOGER:I = 0x206

.field public static final DEDT_LINGVO_RUSTOSPN:I = 0x208

.field public static final DEDT_LINGVO_RUSTOUKR:I = 0x202

.field public static final DEDT_LINGVO_SPNTORUS:I = 0x209

.field public static final DEDT_LINGVO_UKRTOENG:I = 0x205

.field public static final DEDT_LINGVO_UKRTORUS:I = 0x203

.field public static final DEDT_MACMILLAN_ENGTOENG:I = 0xfe00

.field public static final DEDT_MALAYIN_ARATOENG:I = 0xa01

.field public static final DEDT_MALAYIN_ENGTOARA:I = 0xa00

.field public static final DEDT_MANTOU_INITIAL_SIMPTOKOR:I = 0xd03

.field public static final DEDT_MANTOU_INITIAL_TRADTOKOR:I = 0xd07

.field public static final DEDT_MANTOU_KORTOSIMP:I = 0xd00

.field public static final DEDT_MANTOU_KORTOTRAD:I = 0xd04

.field public static final DEDT_MANTOU_PINYIN_SIMPTOKOR:I = 0xd02

.field public static final DEDT_MANTOU_PINYIN_TRADTOKOR:I = 0xd06

.field public static final DEDT_MANTOU_SIMPTOKOR:I = 0xd01

.field public static final DEDT_MANTOU_TRADTOKOR:I = 0xd05

.field public static final DEDT_MAX:I = 0xff02

.field public static final DEDT_MOTECH_CHNTOENG:I = 0xc01

.field public static final DEDT_MOTECH_ENGTOCHN:I = 0xc00

.field public static final DEDT_MOTECH_ENGTOSIMP:I = 0xc00

.field public static final DEDT_MOTECH_ENGTOSIMPTRAD:I = 0xc08

.field public static final DEDT_MOTECH_ENGTOSIMPTRAD_SIMP:I = 0xc08

.field public static final DEDT_MOTECH_ENGTOSIMPTRAD_TRAD:I = 0xc09

.field public static final DEDT_MOTECH_ENGTOTRAD:I = 0xc04

.field public static final DEDT_MOTECH_INITIAL_SIMPTOENG:I = 0xc03

.field public static final DEDT_MOTECH_INITIAL_SIMPTRADTOENG_SIMP:I = 0xc0d

.field public static final DEDT_MOTECH_INITIAL_SIMPTRADTOENG_TRAD:I = 0xc0f

.field public static final DEDT_MOTECH_INITIAL_TRADTOENG:I = 0xc07

.field public static final DEDT_MOTECH_PINYINTOENG:I = 0xc02

.field public static final DEDT_MOTECH_PINYIN_SIMPTOENG:I = 0xc02

.field public static final DEDT_MOTECH_PINYIN_SIMPTRADTOENG_SIMP:I = 0xc0c

.field public static final DEDT_MOTECH_PINYIN_SIMPTRADTOENG_TRAD:I = 0xc0e

.field public static final DEDT_MOTECH_PINYIN_TRADTOENG:I = 0xc06

.field public static final DEDT_MOTECH_SIMPTOENG:I = 0xc01

.field public static final DEDT_MOTECH_SIMPTRADTOENG:I = 0xc0a

.field public static final DEDT_MOTECH_SIMPTRADTOENG_SIMP:I = 0xc0a

.field public static final DEDT_MOTECH_SIMPTRADTOENG_TRAD:I = 0xc0b

.field public static final DEDT_MOTECH_TRADTOENG:I = 0xc05

.field public static final DEDT_MYDIC_KSC:I = 0xff00

.field public static final DEDT_MYDIC_UCS2:I = 0xff01

.field public static final DEDT_NATKOREAN_KORTOKOR:I = 0xa17

.field public static final DEDT_NATKOREAN_OLDKOR:I = 0xa18

.field public static final DEDT_NEWACE_ENGTOKOR:I = 0xb04

.field public static final DEDT_NEWACE_ENGTOKOR_EXAMPLE:I = 0xb08

.field public static final DEDT_NEWACE_ENGTOKOR_IDIOM:I = 0xb07

.field public static final DEDT_NEWACE_JPNTOKOR:I = 0xb01

.field public static final DEDT_NEWACE_KANJITOKOR:I = 0xb02

.field public static final DEDT_NEWACE_KORTOENG:I = 0xb03

.field public static final DEDT_NEWACE_KORTOJPN:I = 0xb00

.field public static final DEDT_NEWACE_KORTOKOR:I = 0xb05

.field public static final DEDT_NEWACE_OLDKOR:I = 0xb06

.field public static final DEDT_NORSTEDTS_ENG_SWE:I = 0xa20

.field public static final DEDT_NORSTEDTS_POCKET_ENG_SWE:I = 0xa1e

.field public static final DEDT_NORSTEDTS_POCKET_SWE_ENG:I = 0xa1f

.field public static final DEDT_NORSTEDTS_SWE_ENG:I = 0xa21

.field public static final DEDT_OBUNSHA_ENGTOJPN:I = 0x702

.field public static final DEDT_OBUNSHA_JPNTOENG:I = 0x703

.field public static final DEDT_OBUNSHA_JPNTOJPN:I = 0x700

.field public static final DEDT_OBUNSHA_KANJITOENG:I = 0x704

.field public static final DEDT_OBUNSHA_KANJITOJPN:I = 0x701

.field public static final DEDT_OXFORDNEWACE_ENGTOKOR:I = 0x5f7

.field public static final DEDT_OXFORDNEWACE_ENGTOKOR_EXAMPLE:I = 0x5ff

.field public static final DEDT_OXFORDNEWACE_ENGTOKOR_IDIOM:I = 0x5fe

.field public static final DEDT_OXFORDNEWACE_KORTOENG:I = 0xbf7

.field public static final DEDT_OXFORD_AMERICAN_COLLEGE_DICTIONARY:I = 0x509

.field public static final DEDT_OXFORD_CHN_ENG:I = 0x514

.field public static final DEDT_OXFORD_CHN_ENG_MINI:I = 0x510

.field public static final DEDT_OXFORD_COLLOCATIONS:I = 0x502

.field public static final DEDT_OXFORD_CONCISE:I = 0x504

.field public static final DEDT_OXFORD_ENGTOKOR:I = 0x503

.field public static final DEDT_OXFORD_ENGTOKOR_EXAMPLE:I = 0x50b

.field public static final DEDT_OXFORD_ENGTOKOR_IDIOM:I = 0x50a

.field public static final DEDT_OXFORD_ENG_CHN:I = 0x513

.field public static final DEDT_OXFORD_ENG_CHN_MINI:I = 0x507

.field public static final DEDT_OXFORD_ENG_JPN_MINI:I = 0x506

.field public static final DEDT_OXFORD_ENG_MAL:I = 0x51b

.field public static final DEDT_OXFORD_ENG_MINI:I = 0x505

.field public static final DEDT_OXFORD_FLTRP_CHN_ENG:I = 0x517

.field public static final DEDT_OXFORD_FLTRP_ENG_CHN:I = 0x516

.field public static final DEDT_OXFORD_FLTRP_PINYIN_CHN_ENG:I = 0x518

.field public static final DEDT_OXFORD_INITIAL_CHN_ENG_MINI:I = 0x512

.field public static final DEDT_OXFORD_JPN_ENG_MINI:I = 0x50e

.field public static final DEDT_OXFORD_KANJITOENG_MINI:I = 0x50f

.field public static final DEDT_OXFORD_MINI_ENG_MAL:I = 0x519

.field public static final DEDT_OXFORD_MINI_MAL_ENG:I = 0x51a

.field public static final DEDT_OXFORD_NEW_AMERICAN_DICTIONARY:I = 0x508

.field public static final DEDT_OXFORD_OALD:I = 0x500

.field public static final DEDT_OXFORD_OALD_EXAMPLE:I = 0x50d

.field public static final DEDT_OXFORD_OALD_IDIOM:I = 0x50c

.field public static final DEDT_OXFORD_PINYIN_CHN_ENG:I = 0x515

.field public static final DEDT_OXFORD_PINYIN_CHN_ENG_MINI:I = 0x511

.field public static final DEDT_OXFORD_THESAURUS:I = 0x501

.field public static final DEDT_PAIBOON_ENGTOTHA:I = 0xa06

.field public static final DEDT_PAIBOON_THAPHONTOENG:I = 0xa08

.field public static final DEDT_PAIBOON_THATOENG:I = 0xa07

.field public static final DEDT_STARPUBLICATIONS_BENTOENG:I = 0xa0c

.field public static final DEDT_STARPUBLICATIONS_ENGTOBEN:I = 0xa0b

.field public static final DEDT_STARPUBLICATIONS_ENGTOHIN:I = 0xa09

.field public static final DEDT_STARPUBLICATIONS_ENGTOPER:I = 0xa0f

.field public static final DEDT_STARPUBLICATIONS_ENGTOTGL:I = 0xa11

.field public static final DEDT_STARPUBLICATIONS_ENGTOURD:I = 0xa0d

.field public static final DEDT_STARPUBLICATIONS_HINTOENG:I = 0xa0a

.field public static final DEDT_STARPUBLICATIONS_PERTOENG:I = 0xa10

.field public static final DEDT_STARPUBLICATIONS_TGLTOENG:I = 0xa12

.field public static final DEDT_STARPUBLICATIONS_URDARABICTOENG:I = 0xa19

.field public static final DEDT_STARPUBLICATIONS_URDTOENG:I = 0xa0e

.field public static final DEDT_TOTAL_SEARCH:I = 0xfff0

.field public static final DEDT_VANDALE_ENG_NETH:I = 0xa30

.field public static final DEDT_VANDALE_MINI_ENG_NETH:I = 0xa2e

.field public static final DEDT_VANDALE_MINI_NETH_ENG:I = 0xa2f

.field public static final DEDT_VANDALE_NETH_ENG:I = 0xa31

.field public static final DEDT_VEGA_FORLAG_ENGTONOR:I = 0xa28

.field public static final DEDT_VEGA_FORLAG_NORTOENG:I = 0xa29

.field public static final DEDT_VEGA_FORLAG_POCKET_ENGTONOR:I = 0xa26

.field public static final DEDT_VEGA_FORLAG_POCKET_NORTOENG:I = 0xa27

.field public static final DEDT_VOX_ENGTOSPN:I = 0x902

.field public static final DEDT_VOX_FRATOSPN:I = 0x900

.field public static final DEDT_VOX_SPNTOENG:I = 0x903

.field public static final DEDT_VOX_SPNTOFRA:I = 0x901

.field public static final DEDT_WORDBANK:I = -0x3e8

.field public static final DEDT_WYS_ENGTOSIMP:I = 0x300

.field public static final DEDT_WYS_ENGTOSIMPTRAD:I = 0x308

.field public static final DEDT_WYS_ENGTOSIMPTRAD_SIMP:I = 0x308

.field public static final DEDT_WYS_ENGTOSIMPTRAD_TRAD:I = 0x309

.field public static final DEDT_WYS_ENGTOTRAD:I = 0x304

.field public static final DEDT_WYS_INITIAL_SIMPTOENG:I = 0x303

.field public static final DEDT_WYS_INITIAL_SIMPTRADTOENG_SIMP:I = 0x30d

.field public static final DEDT_WYS_INITIAL_SIMPTRADTOENG_TRAD:I = 0x30f

.field public static final DEDT_WYS_INITIAL_TRADTOENG:I = 0x307

.field public static final DEDT_WYS_PINYIN_SIMPTOENG:I = 0x302

.field public static final DEDT_WYS_PINYIN_SIMPTRADTOENG_SIMP:I = 0x30c

.field public static final DEDT_WYS_PINYIN_SIMPTRADTOENG_TRAD:I = 0x30e

.field public static final DEDT_WYS_PINYIN_TRADTOENG:I = 0x306

.field public static final DEDT_WYS_SIMPTOENG:I = 0x301

.field public static final DEDT_WYS_SIMPTRADTOENG:I = 0x30a

.field public static final DEDT_WYS_SIMPTRADTOENG_SIMP:I = 0x30a

.field public static final DEDT_WYS_SIMPTRADTOENG_TRAD:I = 0x30b

.field public static final DEDT_WYS_TRADTOENG:I = 0x305

.field public static final DEDT_YBM_ALLINALL_ENGTOKOR:I = 0x4

.field public static final DEDT_YBM_ALLINALL_ENGTOKOR_EXAMPLE:I = 0x7

.field public static final DEDT_YBM_ALLINALL_ENGTOKOR_IDIOM:I = 0x6

.field public static final DEDT_YBM_ALLINALL_JPNTOKOR:I = 0x18

.field public static final DEDT_YBM_ALLINALL_KANJITOKOR:I = 0x19

.field public static final DEDT_YBM_ALLINALL_KORTOENG:I = 0x5

.field public static final DEDT_YBM_ALLINALL_KORTOJPN:I = 0x17

.field public static final DEDT_YBM_BUSINESS:I = 0x12

.field public static final DEDT_YBM_CHNTOKOR:I = 0xb

.field public static final DEDT_YBM_E4U_ENGTOKOR:I = 0x0

.field public static final DEDT_YBM_E4U_ENGTOKOR_EXAMPLE:I = 0x3

.field public static final DEDT_YBM_E4U_ENGTOKOR_IDIOM:I = 0x2

.field public static final DEDT_YBM_E4U_KORTOENG:I = 0x1

.field public static final DEDT_YBM_ENGTOENG:I = 0x14

.field public static final DEDT_YBM_JPNTOKOR:I = 0xe

.field public static final DEDT_YBM_JUNIOR:I = 0x15

.field public static final DEDT_YBM_KANJITOKOR:I = 0xf

.field public static final DEDT_YBM_KORTOCHN:I = 0xa

.field public static final DEDT_YBM_KORTOJPN:I = 0xd

.field public static final DEDT_YBM_KORTOKOR:I = 0x8

.field public static final DEDT_YBM_MIDDLE_WORD:I = 0x16

.field public static final DEDT_YBM_OLDKOR:I = 0x9

.field public static final DEDT_YBM_PINYINTOKOR:I = 0xc

.field public static final DEDT_YBM_SUNUNG_WORD:I = 0x10

.field public static final DEDT_YBM_TOEIC_IDIOM:I = 0x13

.field public static final DEDT_YBM_TOEIC_WORD:I = 0x11


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
