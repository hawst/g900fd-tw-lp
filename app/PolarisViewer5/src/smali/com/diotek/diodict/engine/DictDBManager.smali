.class public Lcom/diotek/diodict/engine/DictDBManager;
.super Ljava/lang/Object;
.source "DictDBManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isArabicDictOnly(I)Z
    .locals 1
    .param p0, "dicType"    # I

    .prologue
    .line 7
    const/16 v0, 0xa00

    if-eq v0, p0, :cond_0

    const/16 v0, 0xa01

    if-eq v0, p0, :cond_0

    const/16 v0, 0x126

    if-eq v0, p0, :cond_0

    const/16 v0, 0x127

    if-eq v0, p0, :cond_0

    const/16 v0, 0x128

    if-ne v0, p0, :cond_1

    .line 12
    :cond_0
    const/4 v0, 0x1

    .line 14
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOxfordDict(I)Z
    .locals 1
    .param p0, "dicType"    # I

    .prologue
    .line 19
    const/16 v0, 0x500

    if-gt v0, p0, :cond_0

    const/16 v0, 0x5ff

    if-lt v0, p0, :cond_0

    .line 21
    const/4 v0, 0x1

    .line 23
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVanDaleDict(I)Z
    .locals 1
    .param p0, "dicType"    # I

    .prologue
    .line 28
    const/16 v0, 0xa2f

    if-eq v0, p0, :cond_0

    const/16 v0, 0xa31

    if-eq v0, p0, :cond_0

    const/16 v0, 0xa30

    if-eq v0, p0, :cond_0

    const/16 v0, 0xa2e

    if-ne v0, p0, :cond_1

    .line 32
    :cond_0
    const/4 v0, 0x1

    .line 34
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
