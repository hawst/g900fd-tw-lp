.class public Lcom/diotek/diodict/core/engine/EntryId;
.super Ljava/lang/Object;
.source "EntryId.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/diotek/diodict/core/engine/EntryId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEntryNum:I

.field private mHeadSn:I

.field private mHeadType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId$1;

    invoke-direct {v0}, Lcom/diotek/diodict/core/engine/EntryId$1;-><init>()V

    sput-object v0, Lcom/diotek/diodict/core/engine/EntryId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mEntryNum:I

    .line 31
    iput v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadType:I

    .line 32
    iput v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadSn:I

    .line 33
    return-void
.end method

.method public constructor <init>([I)V
    .locals 1
    .param p1, "entryId"    # [I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mEntryNum:I

    .line 40
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadType:I

    .line 41
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadSn:I

    .line 42
    return-void
.end method

.method static synthetic access$002(Lcom/diotek/diodict/core/engine/EntryId;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p1, "x1"    # I

    .prologue
    .line 10
    iput p1, p0, Lcom/diotek/diodict/core/engine/EntryId;->mEntryNum:I

    return p1
.end method

.method static synthetic access$102(Lcom/diotek/diodict/core/engine/EntryId;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p1, "x1"    # I

    .prologue
    .line 10
    iput p1, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadType:I

    return p1
.end method

.method static synthetic access$202(Lcom/diotek/diodict/core/engine/EntryId;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p1, "x1"    # I

    .prologue
    .line 10
    iput p1, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadSn:I

    return p1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public getEntryNum()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mEntryNum:I

    return v0
.end method

.method public getHeadSn()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadSn:I

    return v0
.end method

.method public getHeadType()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadType:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 93
    iget v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mEntryNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget v0, p0, Lcom/diotek/diodict/core/engine/EntryId;->mHeadSn:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    return-void
.end method
