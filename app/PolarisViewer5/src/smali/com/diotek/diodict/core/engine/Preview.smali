.class public Lcom/diotek/diodict/core/engine/Preview;
.super Ljava/lang/Object;
.source "Preview.java"


# static fields
.field private static final KEY_FREQ:Ljava/lang/String; = "freq"

.field private static final KEY_HANJA:Ljava/lang/String; = "hanja"

.field private static final KEY_KEYWORD:Ljava/lang/String; = "keyword"

.field private static final KEY_PREVIEW:Ljava/lang/String; = "preview"

.field private static final KEY_SUB:Ljava/lang/String; = "sub"

.field private static final KEY_SUB_EQUIV:Ljava/lang/String; = "equiv"

.field private static final KEY_SUB_PART:Ljava/lang/String; = "part"


# instance fields
.field private mFreq:Ljava/lang/String;

.field private mHanja:Ljava/lang/String;

.field private mKeyword:Ljava/lang/String;

.field private mParsedEquivString:Ljava/lang/StringBuilder;

.field private mSubList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "preview"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object v4, p0, Lcom/diotek/diodict/core/engine/Preview;->mSubList:Ljava/util/ArrayList;

    .line 73
    iput-object v4, p0, Lcom/diotek/diodict/core/engine/Preview;->mParsedEquivString:Ljava/lang/StringBuilder;

    .line 81
    const/4 v1, 0x0

    .line 82
    .local v1, "object":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 84
    .local v3, "previewObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v1    # "object":Lorg/json/JSONObject;
    .local v2, "object":Lorg/json/JSONObject;
    invoke-direct {p0, v2}, Lcom/diotek/diodict/core/engine/Preview;->getPreview(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v3

    .line 93
    const-string/jumbo v4, "freq"

    invoke-direct {p0, v4, v3}, Lcom/diotek/diodict/core/engine/Preview;->getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/diotek/diodict/core/engine/Preview;->mFreq:Ljava/lang/String;

    .line 96
    const-string/jumbo v4, "keyword"

    invoke-direct {p0, v4, v3}, Lcom/diotek/diodict/core/engine/Preview;->getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/diotek/diodict/core/engine/Preview;->mKeyword:Ljava/lang/String;

    .line 99
    const-string/jumbo v4, "hanja"

    invoke-direct {p0, v4, v3}, Lcom/diotek/diodict/core/engine/Preview;->getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 100
    const-string/jumbo v4, "hanja"

    invoke-direct {p0, v4, v3}, Lcom/diotek/diodict/core/engine/Preview;->getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/diotek/diodict/core/engine/Preview;->mHanja:Ljava/lang/String;

    .line 104
    :cond_0
    invoke-direct {p0, v3}, Lcom/diotek/diodict/core/engine/Preview;->setSubList(Lorg/json/JSONObject;)V

    move-object v1, v2

    .line 105
    .end local v2    # "object":Lorg/json/JSONObject;
    .restart local v1    # "object":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private getPreview(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 3
    .param p1, "root"    # Lorg/json/JSONObject;

    .prologue
    .line 108
    const/4 v1, 0x0

    .line 110
    .local v1, "previewObject":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v2, "preview"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 114
    :goto_0
    return-object v1

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "object"    # Lorg/json/JSONObject;

    .prologue
    .line 188
    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 189
    :cond_0
    const-string/jumbo v1, ""

    .line 197
    :goto_0
    return-object v1

    .line 191
    :cond_1
    const-string/jumbo v1, ""

    .line 193
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private setSubList(Lorg/json/JSONObject;)V
    .locals 10
    .param p1, "root"    # Lorg/json/JSONObject;

    .prologue
    .line 118
    if-eqz p1, :cond_0

    const-string/jumbo v7, "sub"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    const/4 v6, 0x0

    .line 122
    .local v6, "subArray":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .line 125
    .local v4, "size":I
    :try_start_0
    const-string/jumbo v7, "sub"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 126
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 128
    if-lez v4, :cond_0

    .line 129
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/diotek/diodict/core/engine/Preview;->mSubList:Ljava/util/ArrayList;

    .line 130
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v7, p0, Lcom/diotek/diodict/core/engine/Preview;->mParsedEquivString:Ljava/lang/StringBuilder;

    .line 131
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_0

    .line 132
    invoke-virtual {v6, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 133
    .local v5, "sub":Lorg/json/JSONObject;
    if-eqz v5, :cond_3

    .line 135
    const-string/jumbo v7, "part"

    invoke-direct {p0, v7, v5}, Lcom/diotek/diodict/core/engine/Preview;->getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 136
    .local v3, "part":Ljava/lang/String;
    const-string/jumbo v7, "equiv"

    invoke-direct {p0, v7, v5}, Lcom/diotek/diodict/core/engine/Preview;->getValue(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "equiv":Ljava/lang/String;
    iget-object v7, p0, Lcom/diotek/diodict/core/engine/Preview;->mSubList:Ljava/util/ArrayList;

    new-instance v8, Landroid/util/Pair;

    invoke-direct {v8, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 141
    iget-object v7, p0, Lcom/diotek/diodict/core/engine/Preview;->mParsedEquivString:Ljava/lang/StringBuilder;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 144
    iget-object v7, p0, Lcom/diotek/diodict/core/engine/Preview;->mParsedEquivString:Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v1    # "equiv":Ljava/lang/String;
    .end local v3    # "part":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 149
    .end local v2    # "i":I
    .end local v5    # "sub":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private trimQuotation(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 159
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object v1, p1

    .line 178
    :cond_1
    :goto_0
    return-object v1

    .line 163
    :cond_2
    const/4 v1, 0x0

    .line 166
    .local v1, "tempBuf":Ljava/lang/String;
    const-string/jumbo v2, "\'"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 167
    .local v0, "index":I
    if-le v0, v3, :cond_3

    .line 168
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 173
    :goto_1
    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 174
    if-le v0, v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_1

    .line 175
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 170
    :cond_3
    move-object v1, p1

    goto :goto_1
.end method


# virtual methods
.method public getEquivString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mParsedEquivString:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mParsedEquivString:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 251
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getFrequency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mFreq:Ljava/lang/String;

    return-object v0
.end method

.method public getHanja()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mHanja:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSubList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mSubList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public hasHanja()Z
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mHanja:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict/core/engine/Preview;->mHanja:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 222
    const/4 v0, 0x1

    .line 224
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
