.class Lcom/diotek/diodict/mean/TagConverter$17;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1552
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 0
    .param p1, "tag"    # I

    .prologue
    .line 1570
    return-void
.end method

.method public handleTag_Start(I)V
    .locals 5
    .param p1, "tag"    # I

    .prologue
    .line 1554
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 1555
    .local v0, "end_point":I
    const/4 v1, 0x0

    .line 1556
    .local v1, "index":I
    :goto_0
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 1557
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    const/16 v3, 0x3f

    if-ne v2, v3, :cond_1

    .line 1558
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$808(Lcom/diotek/diodict/mean/TagConverter;)I

    .line 1565
    :cond_0
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSymbolIndex:[C
    invoke-static {}, Lcom/diotek/diodict/mean/TagConverter;->access$2300()[C

    move-result-object v3

    aget-char v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 1566
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$908(Lcom/diotek/diodict/mean/TagConverter;)I

    .line 1567
    return-void

    .line 1561
    :cond_1
    mul-int/lit8 v2, v1, 0xa

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v2, -0x30

    .line 1563
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$17;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$808(Lcom/diotek/diodict/mean/TagConverter;)I

    goto :goto_0
.end method
