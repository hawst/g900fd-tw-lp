.class Lcom/diotek/diodict/mean/TagConverter$2;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 989
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$2;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 0
    .param p1, "tag"    # I

    .prologue
    .line 1001
    return-void
.end method

.method public handleTag_Start(I)V
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 991
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$2;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 992
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$2;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v0, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    .line 993
    .local v0, "checkingTag":I
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$2;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->isEmptyTag(I)Z
    invoke-static {v1, v0}, Lcom/diotek/diodict/mean/TagConverter;->access$400(Lcom/diotek/diodict/mean/TagConverter;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 994
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$2;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$500(Lcom/diotek/diodict/mean/TagConverter;)[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-interface {v1, v0}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_End(I)V

    .line 995
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$2;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 998
    .end local v0    # "checkingTag":I
    :cond_0
    return-void
.end method
