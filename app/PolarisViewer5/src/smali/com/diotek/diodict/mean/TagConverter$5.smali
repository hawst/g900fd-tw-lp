.class Lcom/diotek/diodict/mean/TagConverter$5;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1050
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 6
    .param p1, "tag"    # I

    .prologue
    .line 1062
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1063
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v1, p1, :cond_1

    .line 1064
    const-string/jumbo v1, "TagConverter"

    const-string/jumbo v2, "Tag Missing B"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1080
    :cond_0
    :goto_0
    return-void

    .line 1067
    :cond_1
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    if-ltz v1, :cond_0

    .line 1070
    and-int/lit16 v1, p1, 0xff

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    and-int/lit16 v1, p1, 0xff

    const/4 v2, 0x4

    if-gt v1, v2, :cond_0

    .line 1071
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v3, v3, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    and-int/lit16 v4, p1, 0xff

    aget v3, v3, v4

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget v3, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1076
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1077
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    iput v2, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    goto :goto_0
.end method

.method public handleTag_Start(I)V
    .locals 4
    .param p1, "tag"    # I

    .prologue
    const/16 v3, 0xf

    const/4 v2, 0x1

    .line 1052
    and-int/lit16 v0, p1, 0xff

    if-ne v0, v2, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v1, v1, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    aget v1, v1, v2

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v0, p1, v1, v3}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    .line 1059
    :goto_0
    return-void

    .line 1057
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$5;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v1, v1, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    and-int/lit16 v2, p1, 0xff

    aget v1, v1, v2

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v0, p1, v1, v3}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    goto :goto_0
.end method
