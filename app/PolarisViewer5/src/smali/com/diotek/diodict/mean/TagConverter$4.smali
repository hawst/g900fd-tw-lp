.class Lcom/diotek/diodict/mean/TagConverter$4;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1024
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$4;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 6
    .param p1, "tag"    # I

    .prologue
    .line 1030
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$4;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1031
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v1, p1, :cond_1

    .line 1032
    const-string/jumbo v1, "TagConverter"

    const-string/jumbo v2, "Tag Missing B"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    :cond_0
    :goto_0
    return-void

    .line 1035
    :cond_1
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$4;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1038
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$4;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$4$1;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$4$1;-><init>(Lcom/diotek/diodict/mean/TagConverter$4;)V

    iget v3, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$4;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public handleTag_Start(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter$4;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v1, -0x1

    const/16 v2, 0xf

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v0, p1, v1, v2}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    .line 1027
    return-void
.end method
