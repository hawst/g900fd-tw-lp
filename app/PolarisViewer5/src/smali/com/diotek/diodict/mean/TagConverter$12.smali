.class Lcom/diotek/diodict/mean/TagConverter$12;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1390
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 1420
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1421
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v1, p1, :cond_1

    .line 1422
    const-string/jumbo v1, "TagConverter"

    const-string/jumbo v2, "Tag Missing M"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    :cond_0
    :goto_0
    return-void

    .line 1425
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$1300(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2100(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1426
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 1427
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 1430
    :cond_2
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    goto :goto_0
.end method

.method public handleTag_Start(I)V
    .locals 6
    .param p1, "tag"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1392
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_Empty:Lcom/diotek/diodict/mean/TagConverter$TagHandler;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1100(Lcom/diotek/diodict/mean/TagConverter;)Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_Start(I)V

    .line 1393
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1000(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    and-int/lit8 v2, v2, 0xf

    if-nez v2, :cond_1

    .line 1394
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2, v3}, Lcom/diotek/diodict/mean/TagConverter;->access$802(Lcom/diotek/diodict/mean/TagConverter;I)I

    .line 1417
    :cond_0
    :goto_0
    return-void

    .line 1397
    :cond_1
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v3, v3, Lcom/diotek/diodict/mean/TagConverter;->mContentSpannableBuilder:Landroid/text/SpannableStringBuilder;

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v2, v3}, Lcom/diotek/diodict/mean/TagConverter;->access$602(Lcom/diotek/diodict/mean/TagConverter;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    .line 1398
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1300(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1399
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 1400
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1401
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2, v4}, Lcom/diotek/diodict/mean/TagConverter;->access$902(Lcom/diotek/diodict/mean/TagConverter;I)I

    .line 1403
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 1404
    .local v0, "end_point":I
    :goto_1
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-ge v2, v0, :cond_2

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_2

    .line 1405
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$808(Lcom/diotek/diodict/mean/TagConverter;)I

    goto :goto_1

    .line 1409
    .end local v0    # "end_point":I
    :cond_2
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v3, -0x1

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v2, p1, v3, v5}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    .line 1411
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1300(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1412
    new-instance v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {v1, v2}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 1413
    .local v1, "item":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iput v5, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    .line 1414
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    iput v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 1415
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$12;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method
