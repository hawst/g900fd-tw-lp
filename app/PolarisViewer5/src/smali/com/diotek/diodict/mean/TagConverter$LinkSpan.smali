.class public Lcom/diotek/diodict/mean/TagConverter$LinkSpan;
.super Landroid/text/style/ClickableSpan;
.source "TagConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LinkSpan"
.end annotation


# instance fields
.field private mURL:Ljava/lang/String;

.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;Ljava/lang/String;)V
    .locals 0
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 122
    iput-object p2, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    .line 123
    return-void
.end method

.method private convertHexStr2Int(Ljava/lang/String;)I
    .locals 5
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 128
    .local v1, "end_point":I
    const/4 v0, 0x0

    .line 129
    .local v0, "cur_idx":I
    const/4 v2, 0x0

    .line 130
    .local v2, "result":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 131
    shl-int/lit8 v2, v2, 0x4

    .line 132
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    const/16 v4, 0x39

    if-le v3, v4, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x61

    add-int/lit8 v3, v3, 0xa

    :goto_1
    add-int/2addr v2, v3

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x30

    goto :goto_1

    .line 137
    :cond_1
    return v2
.end method

.method private convertString2Integer(Ljava/lang/String;)I
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 140
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 141
    .local v0, "result":I
    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "widget"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x7c

    const/4 v7, 0x0

    .line 148
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mClientFileLinkListener:Lcom/diotek/diodict/mean/TagConverter$LoadListener;
    invoke-static {v6}, Lcom/diotek/diodict/mean/TagConverter;->access$000(Lcom/diotek/diodict/mean/TagConverter;)Lcom/diotek/diodict/mean/TagConverter$LoadListener;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 155
    .local v3, "sep_index":I
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "sep_index":I
    .local v4, "sep_index":I
    invoke-virtual {v6, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "keyword":Ljava/lang/String;
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    invoke-virtual {v6, v8, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 157
    .local v1, "next_sep_index":I
    const/4 v6, -0x1

    if-ne v1, v6, :cond_3

    .line 158
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->convertString2Integer(Ljava/lang/String;)I

    move-result v5

    .line 164
    .local v5, "suid":I
    :goto_1
    const/4 v2, 0x0

    .line 165
    .local v2, "ref_id":Ljava/lang/String;
    if-lez v1, :cond_2

    .line 166
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    add-int/lit8 v7, v1, 0x1

    iget-object v8, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 168
    :cond_2
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mClientFileLinkListener:Lcom/diotek/diodict/mean/TagConverter$LoadListener;
    invoke-static {v6}, Lcom/diotek/diodict/mean/TagConverter;->access$000(Lcom/diotek/diodict/mean/TagConverter;)Lcom/diotek/diodict/mean/TagConverter$LoadListener;

    move-result-object v6

    invoke-interface {v6, v0, v5, v2}, Lcom/diotek/diodict/mean/TagConverter$LoadListener;->OnLoad(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 161
    .end local v2    # "ref_id":Ljava/lang/String;
    .end local v5    # "suid":I
    :cond_3
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->mURL:Ljava/lang/String;

    invoke-virtual {v6, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;->convertString2Integer(Ljava/lang/String;)I

    move-result v5

    .restart local v5    # "suid":I
    goto :goto_1
.end method
