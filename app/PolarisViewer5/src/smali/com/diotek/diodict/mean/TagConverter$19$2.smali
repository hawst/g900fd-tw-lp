.class Lcom/diotek/diodict/mean/TagConverter$19$2;
.super Landroid/text/style/MetricAffectingSpan;
.source "TagConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/diotek/diodict/mean/TagConverter$19;->handleTag_End(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/diotek/diodict/mean/TagConverter$19;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter$19;)V
    .locals 0

    .prologue
    .line 1760
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$19$2;->this$1:Lcom/diotek/diodict/mean/TagConverter$19;

    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 2
    .param p1, "tp"    # Landroid/text/TextPaint;

    .prologue
    .line 1763
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    const v1, 0x3fa66666    # 1.3f

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1764
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .locals 2
    .param p1, "p"    # Landroid/text/TextPaint;

    .prologue
    .line 1768
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    const v1, 0x3fa66666    # 1.3f

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1769
    return-void
.end method
