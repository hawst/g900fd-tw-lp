.class Lcom/diotek/diodict/mean/TagConverter$7;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1107
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 7
    .param p1, "tag"    # I

    .prologue
    .line 1114
    const/4 v1, 0x1

    .line 1115
    .local v1, "linefeed":Z
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    if-le v2, v3, :cond_2

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$1000(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x29

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2c

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "%L05"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    if-ltz v2, :cond_2

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "%W3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1120
    :cond_1
    const/4 v1, 0x0

    .line 1122
    :cond_2
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-virtual {v2, p1, v1}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(IZ)V

    .line 1124
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1125
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v2, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v2, p1, :cond_4

    .line 1126
    const-string/jumbo v2, "TagConverter"

    const-string/jumbo v3, "Tag Missing E"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    :cond_3
    :goto_0
    return-void

    .line 1129
    :cond_4
    iget v2, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v3}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 1132
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v4, v4, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    const/16 v5, 0x9

    aget v4, v4, v5

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget v4, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v5, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v5}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public handleTag_Start(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$7;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v1, v1, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    const/16 v2, 0x9

    aget v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(IIZ)V

    .line 1111
    return-void
.end method
