.class public Lcom/diotek/diodict/mean/TagConverter;
.super Ljava/lang/Object;
.source "TagConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict/mean/TagConverter$TagHandler;,
        Lcom/diotek/diodict/mean/TagConverter$LinkSpan;,
        Lcom/diotek/diodict/mean/TagConverter$LoadListener;,
        Lcom/diotek/diodict/mean/TagConverter$DictPos;,
        Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    }
.end annotation


# static fields
.field private static BUFFERSIZE_MEAN:I = 0x0

.field public static final DSP_All_Mask:I = 0x7f

.field public static final DSP_Etc_Mask:I = 0x8

.field public static final DSP_Example_Mask:I = 0x4

.field public static final DSP_Frequency_Mask:I = 0x40

.field public static final DSP_Idiom_Mask:I = 0x2

.field public static final DSP_KeyField_ALL_Mask:I = 0x70

.field public static final DSP_Keyword_Mask:I = 0x10

.field public static final DSP_MeanField_All_Mask:I = 0xf

.field public static final DSP_Meaning_Mask:I = 0x1

.field public static final DSP_None_Mask:I = 0x0

.field public static final DSP_Pronounce_Mask:I = 0x20

.field public static final DSP_Quiz_Mask:I = 0x100

.field public static final DSP_Short_Meaning_Mask:I = 0x200

.field private static final HashMap_ID:I = 0x0

.field private static final HashMap_StartIndex:I = 0x1

.field public static final LOADMEANING_FOR_QUIZ:I = 0x1

.field public static final LOADMEANING_FOR_SETTING:I = 0x2

.field public static final LOADMEANING_NORMAL:I = 0x0

.field public static final advAddInfo:I = 0xc

.field public static final advExample:I = 0x9

.field public static final advFrequency:I = 0x0

.field public static final advIdiom:I = 0xb

.field public static final advNumbering1:I = 0x1

.field public static final advNumbering2:I = 0x2

.field public static final advNumbering3:I = 0x3

.field public static final advNumbering4:I = 0x4

.field public static final advPractical:I = 0xa

.field public static final advSelectBg:I = 0x8

.field public static final advSelectFg:I = 0x7

.field public static final advSquareSpc:I = 0x6

.field public static final advUsage:I = 0x5

.field public static final baseChnInfo:I = 0x7

.field public static final baseKeySearched:I = 0x2

.field public static final baseKeyword:I = 0x0

.field public static final baseKeywordBg:I = 0x1

.field public static final baseMean:I = 0x4

.field public static final baseMeanBg:I = 0x5

.field public static final basePart:I = 0x8

.field public static final basePronoun:I = 0x6

.field public static final baseSeperateLine:I = 0x3

.field private static mSymbolIndex:[C

.field private static mTagDspMask:[I


# instance fields
.field mAdvTheme:[I

.field private mAvailableMode:I

.field mBaseTheme:[I

.field private mClientFileLinkListener:Lcom/diotek/diodict/mean/TagConverter$LoadListener;

.field protected mContentSpannableBuilder:Landroid/text/SpannableStringBuilder;

.field private mCurDspMode:I

.field private mCurrentDicType:I

.field private mCurrentTheme:I

.field private mDspIdxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/mean/TagConverter$DictPos;",
            ">;"
        }
    .end annotation
.end field

.field private mDstIndex:I

.field private mFoundCategoryOne:Z

.field private mIDList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mIs2DepthStyle:Z

.field private mIsBuildCtrlData:Z

.field private mIsLinkable:Z

.field private mIsShortMeanMode:Z

.field private mIsStyleClose:Z

.field private mKeywordSpannableBuilder:Landroid/text/SpannableStringBuilder;

.field private mLinkIdxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/mean/TagConverter$DictPos;",
            ">;"
        }
    .end annotation
.end field

.field private mNumTableData:I

.field private mSource:Ljava/lang/String;

.field private mSpannableBuilder:Landroid/text/SpannableStringBuilder;

.field private mSrcIndex:I

.field private mStringBuilder:Ljava/lang/StringBuilder;

.field private mStyleStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;",
            ">;"
        }
    .end annotation
.end field

.field private mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_B:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_C:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_D:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_DspMode:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_E:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_Empty:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_F:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_I:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_K:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_L:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_M:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_O:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_P:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_S:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_T:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_U:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_W:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private mTagHandler_Z:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private final mTagLength:[B

.field private mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

.field private final mTagSubIndex:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const v0, 0x1a000

    sput v0, Lcom/diotek/diodict/mean/TagConverter;->BUFFERSIZE_MEAN:I

    .line 1800
    const/16 v0, 0x1a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/diotek/diodict/mean/TagConverter;->mTagDspMask:[I

    .line 1842
    const/16 v0, 0x392

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/diotek/diodict/mean/TagConverter;->mSymbolIndex:[C

    return-void

    .line 1800
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x2
        0x4
        0x40
        0x0
        0x0
        0x0
        0x0
        0x10
        0x0
        0x1
        0x0
        0x8
        0x20
        0x8
        0x0
        0x0
        0x0
        0x0
        0x0
        0x8
        0x8
        0x8
        0x0
    .end array-data

    .line 1842
    :array_1
    .array-data 2
        0xc1s
        0xc0s
        -0x1ffes
        -0x1ffds
        -0x1ffcs
        -0x1ffbs
        0xc5s
        0xc6s
        0x28cs
        -0x1ff9s
        -0x1ff8s
        0x101s
        0xe1s
        0xe0s
        0xe2s
        0x103s
        0xe3s
        0xe4s
        -0x1ff2s
        -0x1ff1s
        -0x1ff0s
        -0x1fefs
        -0x1fees
        -0x1feds
        -0x1fecs
        0xe6s
        0x1fds
        -0x1fe9s
        0x259s
        -0x1fe7s
        -0x1fe6s
        -0x1fe5s
        -0x1fe4s
        -0x1fe3s
        -0x1fe2s
        -0x1fe1s
        0x106s
        0x10cs
        0x107s
        -0x1fe0s
        -0x1fdfs
        -0x1fdes
        0xe7s
        -0x1fdcs
        0xc9s
        0xc8s
        -0x1fd9s
        -0x1fd8s
        -0x1fd7s
        0x113s
        0xe9s
        0xe8s
        0xeas
        0x115s
        0xebs
        -0x1fd2s
        0x25bs
        -0x1fd0s
        -0x1fcfs
        -0x1fces
        0x283s
        -0x1fccs
        -0x1fcbs
        -0x1fcas
        -0x1fc9s
        0x124s
        -0x1fc8s
        -0x1fc7s
        0x272s
        -0x1fc5s
        0xcds
        0xccs
        -0x1fc2s
        -0x1fc1s
        -0x1fc0s
        0x12bs
        0xeds
        0xecs
        0xees
        0x1d0s
        0xefs
        -0x1fbas
        -0x1fb9s
        -0x1fb8s
        0x139s
        -0x1fb7s
        -0x1fb6s
        -0x1fb5s
        0x144s
        0xf1s
        -0x1fb3s
        0xd3s
        0xd2s
        -0x1fb0s
        -0x1fafs
        -0x1faes
        -0x1fads
        0x14ds
        0xf3s
        0xf2s
        0xf4s
        0x1d2s
        -0x1fa9s
        0xf6s
        -0x1fa7s
        0xf8s
        -0x1fa5s
        0x153s
        -0x1fa3s
        -0x1fa2s
        -0x1fa1s
        0x254s
        -0x1f9fs
        -0x1f9es
        -0x1f9ds
        -0x1f9cs
        0xf0s
        0x3b8s
        0x14bs
        0x154s
        0x159s
        -0x1f98s
        0x15as
        0x161s
        0xdas
        0xd9s
        -0x1f95s
        0xdcs
        -0x1f93s
        0x16bs
        0xfas
        0xf9s
        0xfbs
        0x1d4s
        0xfcs
        0x1d8s
        0x1dcs
        0x1das
        -0x1f8ds
        -0x1f8cs
        0x28as
        0x1e82s
        0x1e83s
        -0x1f8as
        0xdds
        0x1ef2s
        0xfds
        0x1ef3s
        0x179s
        0x292s
        -0x1f86s
        0x3b1s
        0x3b2s
        0x3b3s
        0x3b8s
        0x3bcs
        0x3c0s
        0x3a3s
        0x3bbs
        0x3c3s
        0x3c8s
        0x2070s
        0xb9s
        0xb2s
        0xb3s
        0x2074s
        0x2075s
        0x2076s
        0x2077s
        0x2078s
        0x2079s
        -0x1f59s
        -0x1f58s
        -0x1f57s
        -0x1f56s
        -0x1f55s
        -0x1f75s
        -0x1f74s
        -0x1f73s
        -0x1f72s
        -0x1f71s
        -0x1f70s
        -0x1f6fs
        -0x1f6es
        -0x1f6ds
        -0x1f6cs
        -0x1f6bs
        -0x1f6as
        -0x1f69s
        -0x1f68s
        -0x1f67s
        -0x1f66s
        -0x1f65s
        -0x1f64s
        -0x1f63s
        -0x1f62s
        -0x1f61s
        -0x1f60s
        -0x1f5fs
        -0x1f5es
        -0x1f5ds
        -0x1f5cs
        -0x1f5bs
        -0x1f5as
        -0x1f51s
        -0x1f54s
        -0x1f53s
        -0x1f52s
        0x2d0s
        -0x1f7fs
        -0x1f7es
        -0x1f7ds
        -0x1f7cs
        -0x1f7bs
        -0x1f7as
        -0x1f79s
        -0x1f78s
        -0x1f77s
        -0x1f76s
        0x2020s
        0x5es
        0x2c7s
        0x2d8s
        0x2das
        0x2dcs
        0x2cas
        0x2cfs
        0x32cs
        0xa7s
        0xb7s
        0x2026s
        0x33ds
        0x2228s
        0x2014s
        0x203bs
        0x3010s
        0x3011s
        0x25cbs
        0x25a0s
        0x252s
        0x25cs
        0x2b3s
        0x2b0s
        0x1d4as
        -0x1f4cs
        -0x1f4bs
        0x1e0ds
        0x1e25s
        0x1e63s
        0x1e6ds
        0x1e93s
        0x10ds
        0x142s
        0x16fs
        0x2082s
        0xe5s
        0x2080s
        0x2081s
        0x2083s
        0x2084s
        0x2085s
        0x2086s
        0x2087s
        0x2088s
        0x2089s
        0x148s
        0x15fs
        0x397s
        0x2013s
        0x207bs
        0x2207s
        0x25abs
        0x2610s
        0x2666s
        0x266es
        0x266fs
        0x221as
        0x2714s
        0x2717s
        0x301as
        0x301bs
        -0x1f4as
        -0x1f49s
        -0x1f48s
        -0x5faes
        -0x5f7cs
        -0x7bcs
        -0x1f46s
        -0x1f45s
        -0x1f44s
        -0x1f43s
        -0x1f3as
        0x1d00s
        0x299s
        0x1d04s
        0x1d05s
        0x1d07s
        -0x1f42s
        0x262s
        0x29cs
        0x26as
        0x1d0as
        0x1d0bs
        0x29fs
        0x1d0ds
        0x274s
        0x1d0fs
        0x1d18s
        -0x1f41s
        0x280s
        -0x1f40s
        0x1d1bs
        0x1d1cs
        0x1d20s
        0x1d21s
        -0x1f3fs
        0x28fs
        0x1d22s
        -0x1f3es
        -0x1f3ds
        -0x1f3cs
        0x2021s
        0x2248s
        0x24c7s
        0x2726s
        0x207fs
        0x2c8s
        0x2ccs
        0x28es
        0x2a4s
        0x2a7s
        -0x1f3bs
        0x20a9s
        0x28as
        0x21ds
        0x2260s
        0x2116s
        0x2727s
        0x25b6s
        0x25b7s
        0x2bcs
        -0x1fces
        0x1ebds
        0x129s
        0x169s
        0x2dcs
        -0x1fa1s
        -0x1f39s
        -0x1f38s
        -0x1f37s
        -0xfe0s
        -0xfdfs
        -0xfdes
        -0xfdds
        -0xfdcs
        -0xfdbs
        -0xfdas
        -0xfd9s
        -0xfd8s
        -0xfd7s
        -0xfd6s
        -0xfd5s
        -0xfd4s
        -0xfd3s
        -0xfd1s
        0x215cs
        -0x1f36s
        -0x1f35s
        0x20acs
        0xbds
        0x14cs
        0x207as
        0x2153s
        0x24d0s
        0x24d1s
        0x24d2s
        0x2ecas
        0x3b74s
        0x3e74s
        0x415fs
        0x44dds
        0x45e9s
        0x4d18s
        0x4d19s
        0x4d88s
        0x100s
        -0x1f34s
        -0x1f33s
        0x27a1s
        -0x1f30s
        0x25c8s
        0x2605s
        0x2663s
        0x2660s
        0x2665s
        0x266ds
        0x25c7s
        0x261es
        0x394s
        0x393s
        0x39bs
        0x3a9s
        0x3a6s
        0x3a0s
        0x3a8s
        0x2032s
        0x2033s
        0x201cs
        0x201ds
        0x2018s
        0x2019s
        0x3c7s
        0x3b7s
        0x3b9s
        0x3c9s
        0x3c6s
        0x3c1s
        0x3c4s
        0x3bes
        0x3b6s
        0x2192s
        0x2194s
        0x251s
        0x221bs
        0x2122s
        0x2261s
        0x398s
        0x39es
        0x3a5s
        0x3bds
        0x215ds
        0x215es
        -0x1f32s
        -0x1f31s
        -0x200s
        -0x1ffs
        -0x1fes
        -0x1fds
        -0x1fcs
        -0x1fbs
        -0x1fas
        -0x1f9s
        0x3003s
        0x3b4s
        0x3bas
        -0x578s
        0x105s
        0x109s
        0x117s
        0x119s
        0x11bs
        0x11ds
        0x125s
        0x135s
        0x151s
        0x15bs
        0x15ds
        0x15es
        0x160s
        0x16as
        0x173s
        0x17as
        0x17cs
        0x17ds
        0x17es
        0x1b1s
        0x1e7s
        0x21bs
        0x227s
        0x229s
        0x395s
        0x3a1s
        0x3bfs
        0x4abs
        0x4f1s
        0x11e2s
        0x1e24s
        0x1e2bs
        0x1e37s
        0x1e43s
        0x1e45s
        0x1e47s
        0x1e49s
        0x1e5as
        0x1e5bs
        0x1e60s
        0x1e62s
        0x1e6cs
        0x1e92s
        0x1e94s
        0x1ea1s
        0x1fd1s
        0x1fd9s
        0x2022s
        0x207ds
        0x207es
        0x208bs
        0x2090s
        0x2093s
        0x216as
        0x216bs
        0x21c4s
        0x2214s
        0x2202s
        0x220bs
        0x2262s
        0x2264s
        0x2265s
        0x2267s
        0x24dds
        0x258ds
        0x2627s
        0x2630s
        0x2631s
        0x2632s
        0x2633s
        0x2634s
        0x2635s
        0x2636s
        0x2637s
        0x2640s
        0x2667s
        0x321cs
        0x3388s
        0x3389s
        0x338es
        0x3390s
        0x3391s
        0x3392s
        0x3393s
        0x3398s
        0x339as
        0x339fs
        0x33a3s
        0x33a8s
        0x33a9s
        0x33b2s
        0x33b3s
        0x33b7s
        0x33b8s
        0x33bds
        0x33bes
        0x33bfs
        0x33c4s
        0x33c5s
        0x33c8s
        0x33cas
        0x33cfs
        0x33d6s
        -0x5fa3s
        -0x1e59s
        -0x1d7ds
        -0x1d7bs
        -0x1d78s
        -0x1d67s
        -0x1c76s
        -0x1c58s
        -0x1b94s
        -0x1b90s
        -0x1a9as
        -0x19b6s
        -0x19b1s
        -0x19ads
        -0x17a9s
        -0x175fs
        -0x1759s
        -0x1757s
        -0x1756s
        -0x174es
        -0x1683s
        -0x1675s
        -0x166fs
        -0x1641s
        -0x158fs
        -0x156fs
        -0x1519s
        -0x12f4s
        -0x1271s
        -0x1178s
        -0x1174s
        -0x116bs
        -0xeacs
        -0xe04s
        -0xe00s
        -0xcbfs
        -0xcacs
        -0xbb8s
        -0xac9s
        -0xac6s
        -0xac4s
        -0xab0s
        -0x819s
        -0x7b7s
        -0x7b6s
        -0x7b4s
        -0x286s
        -0x252s
        -0x251s
        -0x227s
        -0x226s
        -0x225s
        -0x224s
        -0x222s
        -0x221s
        -0x220s
        -0x21fs
        -0x21es
        -0x21ds
        -0x21cs
        -0x21bs
        -0x21as
        -0x219s
        -0x218s
        -0x217s
        -0x216s
        -0x215s
        -0x214s
        -0x213s
        -0x212s
        -0x211s
        -0x210s
        -0x20fs
        -0x20es
        -0x20ds
        -0x20cs
        -0x20bs
        -0x20as
        -0x209s
        -0x208s
        -0x207s
        -0x206s
        -0x205s
        -0x204s
        -0x203s
        -0x202s
        -0x201s
        -0x1cas
        -0x5fs
        -0x5es
        -0x5ds
        -0x5cs
        -0x5bs
        -0x5as
        -0x59s
        -0x58s
        -0x57s
        -0x56s
        -0x55s
        -0x54s
        -0x53s
        -0x52s
        -0x51s
        -0x50s
        -0x4fs
        -0x4es
        -0x4ds
        -0x4cs
        -0x4bs
        -0x4as
        -0x49s
        -0x48s
        -0x47s
        -0x46s
        -0x45s
        -0x44s
        -0x43s
        -0x42s
        -0x41s
        -0x40s
        -0x3fs
        -0x3es
        -0x3ds
        -0x3cs
        0x12ds
        0x143s
        0x155s
        0x16ds
        0x1b7s
        0x1dds
        0x1f4s
        0x25as
        0x261s
        0x278s
        0x311s
        0x3ads
        0x3d2s
        0x3dcs
        0x1e30s
        0x1e3es
        0x1e43s
        0x1e54s
        0x1e55s
        0x1e82s
        0x1ef3s
        0x1f72s
        0x201bs
        0x2042s
        0x2135s
        0x2322s
        0x2323s
        0x2628s
        0x262es
        0x2636s
        0x266es
        0x2720s
        0x2721s
        -0x5faes
        -0x5f7cs
        -0x7b7s
        -0x4ffs
        -0x29fs
        -0x29es
        -0x29ds
        -0x298s
        -0x297s
        -0x296s
        -0x295s
        -0x294s
        -0x293s
        -0x292s
        -0x291s
        -0x290s
        -0x28fs
        -0x190s
        -0x18cs
        -0x18bs
        -0x18as
        -0x189s
        -0x188s
        -0x187s
        -0x186s
        -0x185s
        -0x184s
        -0x183s
        -0x182s
        -0x181s
        -0x180s
        -0x17fs
        -0x17es
        -0x17ds
        -0x17cs
        -0x17bs
        -0x17as
        -0x179s
        -0x178s
        -0x177s
        -0x176s
        -0x175s
        -0x173s
        -0x172s
        -0x171s
        -0x170s
        -0x16fs
        -0x16es
        -0x16ds
        -0x16cs
        -0x16bs
        -0x16as
        -0x169s
        -0x168s
        -0x167s
        -0x166s
        -0x165s
        -0x164s
        -0x163s
        -0x158s
        -0x157s
        -0x156s
        -0x155s
        -0x154s
        -0x153s
        -0x152s
        -0x10as
        -0x107s
        -0x106s
        -0x105s
        -0x104s
        -0x103s
        -0x101s
        -0x1f2s
        -0x1f1s
        -0x1f0s
        -0x1efs
        -0x1ees
        -0x1eds
        -0x1ecs
        -0x1ebs
        -0x1eas
        -0x1e9s
        -0x1e8s
        -0x1e7s
        -0x1e6s
        -0x1e5s
        0x272s
        0x303s
        0x30bs
        0x30cs
        0x30ds
        0x30es
        0x30fs
        0x310s
        0x312s
        0x313s
        0x314s
        0x315s
        0x316s
        0x317s
        0x318s
        0x327s
        0x337s
        0x338s
        0x203fs
        0x20a4s
        0x2191s
        0x163s
        -0x56as
        -0x569s
        -0x568s
        -0x567s
        -0x566s
        -0x565s
        -0x564s
        -0x563s
        -0x562s
        -0x561s
        0x275s
        0x2154s
        -0x1e4s
        -0x7ds
        -0x7bs
        -0x7as
        -0x79s
        -0x78s
        -0x77s
        -0x76s
        -0x75s
        -0x74s
        -0x73s
        -0x72s
        -0x71s
        -0x70s
        -0x6es
        -0x6ds
        -0x6cs
        -0x6bs
        0x18fs
        0x190s
        0x191s
        0x1f8s
        0x250s
        0x2b9s
        0x2bas
        0x308s
        0x30as
        0x341s
        0x3f0s
        0x475s
        0x5c3s
        0x1fb6s
        0x20a6s
        0x2502s
        0x2503s
        0x27f5s
        0x27f6s
        0x2919s
        0x291as
        0x46a1s
        -0x61s
        -0x165ds
        -0xb8bs
        0x1c7as
        0x1c7bs
        0x1cb7s
        -0x1f17s
        -0x1f16s
        -0x1f13s
        -0x1f12s
        -0x1f0fs
        -0x1f0es
        -0x1f0ds
        -0x1f0cs
        -0x1f0bs
        -0x1f0as
        0x1e05s
        0x1e41s
        0x1e61s
        0x840s
        0x841s
        0x842s
        0x843s
        0x844s
        0x845s
        0x846s
        0x847s
        0x848s
        0x849s
        0x84as
        0x84bs
        0x84cs
        0x84ds
        0x84es
        -0x2671s
        -0x2670s
        -0x266fs
        -0x266es
        -0x266ds
        -0x266cs
        -0x266bs
        -0x266as
        -0x2669s
        -0x2668s
        -0x2667s
        -0x2666s
        -0x2665s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/diotek/diodict/mean/TagConverter$LoadListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nTheme"    # I
    .param p3, "loadListener"    # Lcom/diotek/diodict/mean/TagConverter$LoadListener;

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/16 v3, 0x1a

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    .line 53
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mKeywordSpannableBuilder:Landroid/text/SpannableStringBuilder;

    .line 54
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mContentSpannableBuilder:Landroid/text/SpannableStringBuilder;

    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 56
    iput-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    .line 61
    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIs2DepthStyle:Z

    .line 62
    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsShortMeanMode:Z

    .line 63
    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mFoundCategoryOne:Z

    .line 64
    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I

    .line 66
    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I

    .line 86
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    .line 94
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    .line 107
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;

    .line 110
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;

    .line 197
    sget-object v2, Lcom/diotek/diodict/engine/ThemeColor;->baseTheme:[I

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mBaseTheme:[I

    .line 198
    sget-object v2, Lcom/diotek/diodict/engine/ThemeColor;->advTheme:[I

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    .line 199
    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentTheme:I

    .line 665
    new-array v2, v3, [B

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagLength:[B

    .line 673
    new-array v2, v3, [B

    fill-array-data v2, :array_1

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagSubIndex:[B

    .line 976
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$1;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$1;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 989
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$2;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$2;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_Empty:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1004
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$3;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$3;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_DspMode:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1024
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$4;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$4;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_B:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1050
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$5;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$5;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_C:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1084
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$6;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$6;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_D:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1107
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$7;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$7;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_E:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1140
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$8;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$8;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_F:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1166
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$9;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$9;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_I:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1197
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$10;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$10;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_K:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1234
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$11;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$11;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_L:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1390
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$12;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$12;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_M:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1437
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$13;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$13;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_O:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1461
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$14;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$14;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_P:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1486
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$15;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$15;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_S:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1521
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$16;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$16;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_T:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1552
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$17;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$17;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_U:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1574
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$18;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$18;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_W:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1699
    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$19;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$19;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_Z:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 1791
    new-array v2, v3, [Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v3, v2, v1

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_B:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v3, v2, v0

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_C:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_D:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_E:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_F:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_I:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_K:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_L:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_M:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_O:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_P:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_DspMode:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_S:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_T:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x14

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_U:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagRemover:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_W:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_DspMode:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_DspMode:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_Z:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aput-object v4, v2, v3

    iput-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    .line 70
    iput-object p3, p0, Lcom/diotek/diodict/mean/TagConverter;->mClientFileLinkListener:Lcom/diotek/diodict/mean/TagConverter$LoadListener;

    .line 71
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mClientFileLinkListener:Lcom/diotek/diodict/mean/TagConverter$LoadListener;

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsLinkable:Z

    .line 72
    invoke-virtual {p0, p2}, Lcom/diotek/diodict/mean/TagConverter;->setTheme(I)I

    .line 73
    return-void

    :cond_0
    move v0, v1

    .line 71
    goto :goto_0

    .line 665
    :array_0
    .array-data 1
        0x1t
        0x1t
        0x2t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x3t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x2t
        0x1t
        0x1t
        0x2t
    .end array-data

    .line 673
    nop

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x4t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x9t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x9t
        0x0t
        0x0t
        0x8t
    .end array-data
.end method

.method private Convert_EngineContent_Spanned()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/16 v10, 0xa

    const/4 v9, 0x0

    .line 363
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mKeywordSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iput-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    .line 364
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 365
    :cond_0
    invoke-virtual {p0}, Lcom/diotek/diodict/mean/TagConverter;->clearSpannableBuilder()V

    .line 501
    :cond_1
    :goto_0
    return-void

    .line 369
    :cond_2
    iput v9, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    iput v9, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    .line 371
    iget-boolean v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    if-eqz v6, :cond_3

    .line 372
    iput v9, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    .line 373
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 374
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 377
    :cond_3
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    .line 380
    .local v0, "end_point":I
    iput-boolean v9, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    .line 382
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    and-int/lit8 v6, v6, 0x70

    if-nez v6, :cond_4

    .line 383
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    const-string/jumbo v7, "%M"

    iget v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    .line 384
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mContentSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iput-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    .line 388
    :cond_4
    :goto_1
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    if-ge v6, v0, :cond_5

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    if-ne v6, v10, :cond_5

    .line 389
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    goto :goto_1

    .line 392
    :cond_5
    invoke-virtual {p0}, Lcom/diotek/diodict/mean/TagConverter;->clearSpannableBuilder()V

    .line 393
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 394
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 395
    :cond_6
    :goto_2
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    if-ge v6, v0, :cond_8

    .line 396
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    const/16 v7, 0x25

    if-ne v6, v7, :cond_e

    .line 397
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v7, v7, 0x1

    invoke-direct {p0, v6, v7}, Lcom/diotek/diodict/mean/TagConverter;->getTagValue(Ljava/lang/String;I)I

    move-result v4

    .line 398
    .local v4, "tag":I
    const/4 v6, -0x1

    if-le v4, v6, :cond_e

    .line 399
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    .line 401
    .local v5, "value":I
    const/16 v6, 0x40

    if-le v5, v6, :cond_c

    const/16 v6, 0x5b

    if-ge v5, v6, :cond_c

    .line 402
    add-int/lit8 v5, v5, -0x41

    .line 403
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    .line 404
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagLength:[B

    aget-byte v7, v7, v5

    add-int/2addr v6, v7

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    .line 406
    sget-object v6, Lcom/diotek/diodict/mean/TagConverter;->mTagDspMask:[I

    aget v6, v6, v5

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v6, v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    and-int/2addr v6, v7

    if-eqz v6, :cond_6

    .line 410
    :cond_7
    iget-boolean v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsShortMeanMode:Z

    if-eqz v6, :cond_b

    .line 411
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aget-object v6, v6, v5

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_C:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    if-ne v6, v7, :cond_b

    .line 412
    iget-boolean v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mFoundCategoryOne:Z

    if-eqz v6, :cond_a

    .line 413
    iput-boolean v9, p0, Lcom/diotek/diodict/mean/TagConverter;->mFoundCategoryOne:Z

    .line 471
    .end local v4    # "tag":I
    .end local v5    # "value":I
    :cond_8
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_9

    .line 472
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 473
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 477
    :cond_9
    :goto_3
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_12

    .line 478
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v4, v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    .line 479
    .restart local v4    # "tag":I
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    invoke-direct {p0, v4}, Lcom/diotek/diodict/mean/TagConverter;->getTagIndex(I)I

    move-result v7

    aget-object v6, v6, v7

    invoke-interface {v6, v4}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_End(I)V

    .line 480
    invoke-direct {p0, v4}, Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Pop(I)V

    .line 481
    const/16 v6, 0xc

    if-eq v4, v6, :cond_9

    .line 482
    const-string/jumbo v6, "TagConverter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Stack Sync Error"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 417
    .restart local v5    # "value":I
    :cond_a
    iput-boolean v11, p0, Lcom/diotek/diodict/mean/TagConverter;->mFoundCategoryOne:Z

    .line 421
    :cond_b
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 422
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 424
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aget-object v6, v6, v5

    invoke-interface {v6, v4}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_Start(I)V

    goto/16 :goto_2

    .line 426
    :cond_c
    const/16 v6, 0x60

    if-le v5, v6, :cond_6

    const/16 v6, 0x7b

    if-ge v5, v6, :cond_6

    .line 427
    add-int/lit8 v5, v5, -0x61

    .line 428
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    .line 429
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagLength:[B

    aget-byte v7, v7, v5

    add-int/2addr v6, v7

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    .line 431
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v6, v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    and-int/2addr v6, v7

    if-nez v6, :cond_d

    sget-object v6, Lcom/diotek/diodict/mean/TagConverter;->mTagDspMask:[I

    aget v6, v6, v5

    if-eqz v6, :cond_6

    .line 436
    :cond_d
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 437
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 439
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aget-object v6, v6, v5

    invoke-interface {v6, v4}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_End(I)V

    .line 440
    invoke-direct {p0, v4}, Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Pop(I)V

    goto/16 :goto_2

    .line 447
    .end local v4    # "tag":I
    .end local v5    # "value":I
    :cond_e
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    if-ne v6, v10, :cond_10

    .line 448
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_f

    .line 449
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_10

    .line 450
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    goto/16 :goto_2

    .line 455
    :cond_f
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    if-lez v6, :cond_10

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-ne v6, v10, :cond_10

    .line 456
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    goto/16 :goto_2

    .line 464
    :cond_10
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->empty()Z

    move-result v6

    if-nez v6, :cond_11

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v6, v6, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    and-int/2addr v6, v7

    if-lez v6, :cond_11

    .line 465
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/String;->codePointAt(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 466
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    .line 468
    :cond_11
    iget v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    goto/16 :goto_2

    .line 486
    :cond_12
    iget-boolean v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    if-eqz v6, :cond_1

    .line 487
    const/4 v1, 0x0

    .line 488
    .local v1, "idx":I
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 490
    .local v3, "size":I
    iget-boolean v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsShortMeanMode:Z

    if-eqz v6, :cond_13

    .line 491
    iput v11, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    goto/16 :goto_0

    .line 494
    .end local v1    # "idx":I
    .local v2, "idx":I
    :goto_4
    if-ge v2, v3, :cond_1

    .line 495
    iget v7, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v6, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    or-int/2addr v6, v7

    iput v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    move v2, v1

    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_4

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    :cond_13
    move v2, v1

    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_4
.end method

.method private StyleEnd(Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;I)V
    .locals 4
    .param p1, "topItem"    # Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    .param p2, "f_Color"    # I

    .prologue
    const/4 v3, -0x1

    .line 781
    if-eq p2, v3, :cond_0

    iget v1, p1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    if-eq v1, p2, :cond_0

    .line 782
    iget v1, p1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    invoke-direct {p0, v1}, Lcom/diotek/diodict/mean/TagConverter;->getTagIndex(I)I

    move-result v0

    .line 783
    .local v0, "tag_index":I
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    .line 784
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    aget-object v1, v1, v0

    iget v2, p1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    invoke-interface {v1, v2}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_End(I)V

    .line 785
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    .line 786
    iput v3, p1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    .line 788
    .end local v0    # "tag_index":I
    :cond_0
    return-void
.end method

.method private StyleStack_Pop(I)V
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/4 v4, -0x1

    .line 791
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 792
    const-string/jumbo v2, "TagConverter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "StyleStack_Pop error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    :cond_0
    return-void

    .line 795
    :cond_1
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 797
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v2, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    if-ne v2, v4, :cond_0

    .line 798
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v3, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v3, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    .line 800
    iget-boolean v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mIs2DepthStyle:Z

    if-eqz v2, :cond_3

    .line 801
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mIs2DepthStyle:Z

    .line 802
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 803
    .local v1, "tempItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v2, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    if-ne v2, v4, :cond_2

    .line 804
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v3, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v3, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    .line 805
    :cond_2
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    .end local v1    # "tempItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    :cond_3
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    .line 809
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 810
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v2, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    if-ne v2, v4, :cond_4

    .line 811
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v3, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v3, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    .line 809
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private StyleStack_Push(III)V
    .locals 9
    .param p1, "tag"    # I
    .param p2, "f_Color"    # I
    .param p3, "dspMode"    # I

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0xf

    const/4 v6, -0x1

    .line 740
    new-instance v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;-><init>(Lcom/diotek/diodict/mean/TagConverter;Lcom/diotek/diodict/mean/TagConverter$1;)V

    .line 741
    .local v1, "tItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iput p1, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    .line 743
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 744
    iput p3, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    .line 745
    iput p2, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    .line 776
    .end local p2    # "f_Color":I
    .end local p3    # "dspMode":I
    :cond_0
    :goto_0
    iget v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v4, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    .line 777
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v4, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 778
    return-void

    .line 748
    .restart local p2    # "f_Color":I
    .restart local p3    # "dspMode":I
    :cond_1
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 749
    .local v3, "topItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    iget v5, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    and-int/2addr v4, v5

    if-lez v4, :cond_8

    .line 750
    iget v4, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    invoke-direct {p0, v4}, Lcom/diotek/diodict/mean/TagConverter;->getTagIndex(I)I

    move-result v2

    .line 752
    .local v2, "tag_index":I
    invoke-direct {p0, v3, p2}, Lcom/diotek/diodict/mean/TagConverter;->StyleEnd(Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;I)V

    .line 753
    if-eq p3, v7, :cond_4

    move v4, p3

    :goto_1
    iput v4, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    .line 754
    if-eq p2, v6, :cond_5

    move v4, p2

    :goto_2
    iput v4, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    .line 756
    const/16 v4, 0x8

    if-eq v2, v4, :cond_2

    if-ne v2, v8, :cond_0

    .line 757
    :cond_2
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "topItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    check-cast v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 758
    .restart local v3    # "topItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 759
    .local v0, "parentTopItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    if-eqz v0, :cond_3

    .line 760
    iget v4, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    invoke-direct {p0, v4}, Lcom/diotek/diodict/mean/TagConverter;->getTagIndex(I)I

    move-result v2

    .line 761
    const/16 v4, 0x13

    if-ne v2, v4, :cond_3

    .line 762
    iput-boolean v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mIs2DepthStyle:Z

    .line 763
    invoke-direct {p0, v0, p2}, Lcom/diotek/diodict/mean/TagConverter;->StyleEnd(Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;I)V

    .line 764
    if-eq p3, v7, :cond_6

    .end local p3    # "dspMode":I
    :goto_3
    iput p3, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    .line 765
    if-eq p2, v6, :cond_7

    .end local p2    # "f_Color":I
    :goto_4
    iput p2, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    .line 768
    :cond_3
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v4, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 753
    .end local v0    # "parentTopItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    .restart local p2    # "f_Color":I
    .restart local p3    # "dspMode":I
    :cond_4
    iget v4, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    goto :goto_1

    .line 754
    :cond_5
    iget v4, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    goto :goto_2

    .line 764
    .restart local v0    # "parentTopItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    :cond_6
    iget p3, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    goto :goto_3

    .line 765
    .end local p3    # "dspMode":I
    :cond_7
    iget p2, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    goto :goto_4

    .line 771
    .end local v0    # "parentTopItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    .end local v2    # "tag_index":I
    .restart local p3    # "dspMode":I
    :cond_8
    if-eq p3, v7, :cond_9

    .end local p3    # "dspMode":I
    :goto_5
    iput p3, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    .line 772
    if-eq p2, v6, :cond_a

    .end local p2    # "f_Color":I
    :goto_6
    iput p2, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    goto :goto_0

    .line 771
    .restart local p2    # "f_Color":I
    .restart local p3    # "dspMode":I
    :cond_9
    iget p3, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    goto :goto_5

    .line 772
    .end local p3    # "dspMode":I
    :cond_a
    iget p2, v3, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    goto :goto_6
.end method

.method static synthetic access$000(Lcom/diotek/diodict/mean/TagConverter;)Lcom/diotek/diodict/mean/TagConverter$LoadListener;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mClientFileLinkListener:Lcom/diotek/diodict/mean/TagConverter$LoadListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    return v0
.end method

.method static synthetic access$1100(Lcom/diotek/diodict/mean/TagConverter;)Lcom/diotek/diodict/mean/TagConverter$TagHandler;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler_Empty:Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I

    return v0
.end method

.method static synthetic access$1300(Lcom/diotek/diodict/mean/TagConverter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/diotek/diodict/mean/TagConverter;Ljava/lang/String;I)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/diotek/diodict/mean/TagConverter;->getTagValue(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/diotek/diodict/mean/TagConverter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsLinkable:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/diotek/diodict/mean/TagConverter;)[B
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagLength:[B

    return-object v0
.end method

.method static synthetic access$200(Lcom/diotek/diodict/mean/TagConverter;III)V
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V

    return-void
.end method

.method static synthetic access$2000(Lcom/diotek/diodict/mean/TagConverter;I)V
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Pop(I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/diotek/diodict/mean/TagConverter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2300()[C
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/diotek/diodict/mean/TagConverter;->mSymbolIndex:[C

    return-object v0
.end method

.method static synthetic access$2400(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I

    return v0
.end method

.method static synthetic access$2402(Lcom/diotek/diodict/mean/TagConverter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I

    return p1
.end method

.method static synthetic access$2408(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 2
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I

    return v0
.end method

.method static synthetic access$2500(Lcom/diotek/diodict/mean/TagConverter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/diotek/diodict/mean/TagConverter;->getTagIndex(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->assertLineFeed()V

    return-void
.end method

.method static synthetic access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    return-object v0
.end method

.method static synthetic access$400(Lcom/diotek/diodict/mean/TagConverter;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/diotek/diodict/mean/TagConverter;->isEmptyTag(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/diotek/diodict/mean/TagConverter;)[Lcom/diotek/diodict/mean/TagConverter$TagHandler;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method static synthetic access$602(Lcom/diotek/diodict/mean/TagConverter;Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    return-object p1
.end method

.method static synthetic access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    return v0
.end method

.method static synthetic access$802(Lcom/diotek/diodict/mean/TagConverter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    return p1
.end method

.method static synthetic access$808(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 2
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    return v0
.end method

.method static synthetic access$812(Lcom/diotek/diodict/mean/TagConverter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    return v0
.end method

.method static synthetic access$900(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    return v0
.end method

.method static synthetic access$902(Lcom/diotek/diodict/mean/TagConverter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    return p1
.end method

.method static synthetic access$908(Lcom/diotek/diodict/mean/TagConverter;)I
    .locals 2
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    return v0
.end method

.method static synthetic access$912(Lcom/diotek/diodict/mean/TagConverter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict/mean/TagConverter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    return v0
.end method

.method private assertLineFeed()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 819
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v1

    if-eq v1, v3, :cond_0

    .line 820
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 821
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    .line 825
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 827
    .local v0, "len":I
    :goto_0
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    if-ge v1, v0, :cond_2

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    .line 828
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    goto :goto_0

    .line 830
    :cond_2
    return-void
.end method

.method private assertWideLineFeed()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 837
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_2

    .line 838
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 839
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    .line 847
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 849
    .local v0, "len":I
    :goto_1
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    if-ge v1, v0, :cond_3

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_3

    .line 850
    :cond_1
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I

    goto :goto_1

    .line 840
    .end local v0    # "len":I
    :cond_2
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v1

    if-eq v1, v3, :cond_0

    .line 841
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 842
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 843
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    goto :goto_0

    .line 852
    .restart local v0    # "len":I
    :cond_3
    return-void
.end method

.method private getTagIndex(I)I
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 1810
    move v0, p1

    .line 1811
    .local v0, "tempTag":I
    const/16 v1, 0xff

    if-le v0, v1, :cond_0

    shr-int/lit8 v0, v0, 0x8

    .end local v0    # "tempTag":I
    :cond_0
    return v0
.end method

.method private getTagValue(Ljava/lang/String;I)I
    .locals 10
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    const/4 v7, -0x1

    .line 695
    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 697
    .local v4, "tag":I
    const/16 v8, 0x40

    if-le v4, v8, :cond_1

    const/16 v8, 0x5b

    if-ge v4, v8, :cond_1

    .line 698
    add-int/lit8 v4, v4, -0x41

    .line 710
    :goto_0
    iget-object v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagSubIndex:[B

    aget-byte v5, v8, v4

    .line 711
    .local v5, "validIndex":I
    if-nez v5, :cond_2

    move v7, v4

    .line 734
    .end local v5    # "validIndex":I
    :cond_0
    :goto_1
    return v7

    .line 700
    :cond_1
    const/16 v8, 0x60

    if-le v4, v8, :cond_0

    const/16 v8, 0x7b

    if-ge v4, v8, :cond_0

    .line 701
    iget-object v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v8}, Ljava/util/Stack;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 704
    add-int/lit8 v4, v4, -0x61

    goto :goto_0

    .line 715
    .restart local v5    # "validIndex":I
    :cond_2
    iget-object v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mTagLength:[B

    aget-byte v8, v8, v4

    add-int/lit8 v1, v8, -0x1

    .line 716
    .local v1, "len":I
    const/4 v3, 0x0

    .line 718
    .local v3, "subIndex":I
    add-int/lit8 p2, p2, 0x1

    move v2, v1

    .end local v1    # "len":I
    .local v2, "len":I
    move v0, p2

    .line 719
    .end local p2    # "index":I
    .local v0, "index":I
    :goto_2
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "len":I
    .restart local v1    # "len":I
    if-lez v2, :cond_5

    .line 720
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    .line 721
    .local v6, "value":I
    const/16 v8, 0x30

    if-lt v6, v8, :cond_3

    const/16 v8, 0x39

    if-le v6, v8, :cond_4

    :cond_3
    move p2, v0

    .line 722
    .end local v0    # "index":I
    .restart local p2    # "index":I
    goto :goto_1

    .line 724
    .end local p2    # "index":I
    .restart local v0    # "index":I
    :cond_4
    mul-int/lit8 v8, v3, 0xa

    add-int/lit8 p2, v0, 0x1

    .end local v0    # "index":I
    .restart local p2    # "index":I
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v9

    add-int/2addr v8, v9

    add-int/lit8 v3, v8, -0x30

    move v2, v1

    .end local v1    # "len":I
    .restart local v2    # "len":I
    move v0, p2

    .end local p2    # "index":I
    .restart local v0    # "index":I
    goto :goto_2

    .line 727
    .end local v2    # "len":I
    .end local v6    # "value":I
    .restart local v1    # "len":I
    :cond_5
    shl-int/lit8 v8, v4, 0x8

    or-int v4, v8, v3

    .line 729
    if-gt v3, v5, :cond_6

    move p2, v0

    .end local v0    # "index":I
    .restart local p2    # "index":I
    move v7, v4

    .line 730
    goto :goto_1

    .end local p2    # "index":I
    .restart local v0    # "index":I
    :cond_6
    move p2, v0

    .line 734
    .end local v0    # "index":I
    .restart local p2    # "index":I
    goto :goto_1
.end method

.method private isEmptyTag(I)Z
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 319
    sparse-switch p1, :sswitch_data_0

    .line 327
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 324
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 319
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xc -> :sswitch_0
        0xf -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public BlockTag_End(I)V
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 902
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(IZ)V

    .line 903
    return-void
.end method

.method public BlockTag_End(IZ)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "assertLinefeed"    # Z

    .prologue
    const/4 v1, 0x0

    .line 911
    iget-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    if-eqz v0, :cond_1

    .line 918
    :cond_0
    :goto_0
    return-void

    .line 913
    :cond_1
    if-eqz p2, :cond_2

    .line 914
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->assertLineFeed()V

    .line 915
    :cond_2
    iget-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    if-nez v0, :cond_0

    .line 916
    invoke-virtual {p0, v1, v1}, Lcom/diotek/diodict/mean/TagConverter;->DspIdxList_Add(IZ)V

    goto :goto_0
.end method

.method public BlockTag_Start(II)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "f_Color"    # I

    .prologue
    .line 855
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(IIZ)V

    .line 856
    return-void
.end method

.method public BlockTag_Start(IIZ)V
    .locals 3
    .param p1, "tag"    # I
    .param p2, "f_Color"    # I
    .param p3, "assertLinefeed"    # Z

    .prologue
    .line 864
    iget-boolean v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z

    if-eqz v2, :cond_0

    .line 898
    :goto_0
    return-void

    .line 867
    :cond_0
    if-eqz p3, :cond_1

    .line 868
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->assertLineFeed()V

    .line 870
    :cond_1
    const/16 v2, 0xff

    if-le p1, v2, :cond_5

    shr-int/lit8 v1, p1, 0x8

    .line 871
    .local v1, "value":I
    :goto_1
    sget-object v2, Lcom/diotek/diodict/mean/TagConverter;->mTagDspMask:[I

    aget v0, v2, v1

    .line 873
    .local v0, "curDspMask":I
    const/16 v2, 0xb08

    if-eq p1, v2, :cond_2

    const/16 v2, 0xb03

    if-ne p1, v2, :cond_3

    .line 875
    :cond_2
    const/4 v0, 0x1

    .line 878
    :cond_3
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 879
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v2, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    packed-switch v2, :pswitch_data_0

    .line 890
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v0, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    .line 896
    :cond_4
    :goto_2
    :pswitch_0
    invoke-direct {p0, p1, p2, v0}, Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V

    .line 897
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/diotek/diodict/mean/TagConverter;->DspIdxList_Add(IZ)V

    goto :goto_0

    .end local v0    # "curDspMask":I
    .end local v1    # "value":I
    :cond_5
    move v1, p1

    .line 870
    goto :goto_1

    .line 881
    .restart local v0    # "curDspMask":I
    .restart local v1    # "value":I
    :pswitch_1
    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    .line 882
    const/4 v0, 0x2

    goto :goto_2

    .line 879
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public DspIdxList_Add(IZ)V
    .locals 4
    .param p1, "dsp_mode"    # I
    .param p2, "is_start"    # Z

    .prologue
    .line 921
    iget-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    if-nez v1, :cond_1

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 925
    :cond_1
    if-eqz p2, :cond_5

    .line 926
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    if-ne v1, v2, :cond_3

    .line 927
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    if-ne v1, p1, :cond_2

    .line 929
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 932
    :cond_2
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iput p1, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    goto :goto_0

    .line 936
    :cond_3
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    if-eq v1, p1, :cond_0

    .line 937
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_4

    .line 938
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 942
    :goto_1
    new-instance v0, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    invoke-direct {v0, p0}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 943
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iput p1, v0, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    .line 944
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v1, v0, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 945
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 940
    .end local v0    # "item":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :cond_4
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    goto :goto_1

    .line 949
    :cond_5
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    iget-object v3, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v1, v3}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    if-eq v2, v1, :cond_0

    .line 951
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 952
    new-instance v0, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    invoke-direct {v0, p0}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 953
    .restart local v0    # "item":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    iget v1, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->dspMode:I

    iput v1, v0, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    .line 954
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I

    iput v1, v0, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 955
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public WideBlockTag_End(I)V
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 906
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->assertWideLineFeed()V

    .line 907
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(IZ)V

    .line 908
    return-void
.end method

.method public WideBlockTag_Start(II)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "f_Color"    # I

    .prologue
    .line 859
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->assertWideLineFeed()V

    .line 860
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(IIZ)V

    .line 861
    return-void
.end method

.method public clearSpannableBuilder()V
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    and-int/lit8 v0, v0, 0x70

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mKeywordSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 357
    :cond_0
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    and-int/lit8 v0, v0, 0xf

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mContentSpannableBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 360
    :cond_1
    return-void
.end method

.method public convert_Partial_Index(III)Ljava/util/ArrayList;
    .locals 11
    .param p1, "dspMode"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/mean/TagConverter$DictPos;",
            ">;"
        }
    .end annotation

    .prologue
    .line 520
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 522
    .local v8, "tPosList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/diotek/diodict/mean/TagConverter$DictPos;>;"
    const/4 v2, 0x0

    .line 523
    .local v2, "idx":I
    const/4 v1, 0x0

    .line 527
    .local v1, "current_start":I
    iget v10, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    and-int/2addr v10, p1

    if-nez v10, :cond_1

    .line 576
    :cond_0
    :goto_0
    return-object v8

    .line 531
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 540
    :goto_1
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v2, v10, :cond_0

    .line 541
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idx":I
    .local v3, "idx":I
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    .line 542
    .local v4, "pItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iget v10, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    if-ne v10, p1, :cond_6

    .line 543
    iget v9, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 544
    .local v9, "tStart":I
    if-le v9, p3, :cond_2

    move v2, v3

    .line 545
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0

    .line 533
    .end local v4    # "pItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .end local v9    # "tStart":I
    :pswitch_0
    new-instance v7, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    invoke-direct {v7, p0}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 534
    .local v7, "tPosItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iput p2, v7, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 535
    iput p3, v7, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 536
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 547
    .end local v2    # "idx":I
    .end local v7    # "tPosItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .restart local v3    # "idx":I
    .restart local v4    # "pItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .restart local v9    # "tStart":I
    :cond_2
    iget v5, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 548
    .local v5, "tEnd":I
    add-int v10, v1, v5

    sub-int/2addr v10, v9

    add-int/lit8 v0, v10, 0x1

    .line 549
    .local v0, "current_end":I
    if-ge v5, p2, :cond_3

    .line 550
    move v1, v0

    move v2, v3

    .line 551
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .line 554
    .end local v2    # "idx":I
    .restart local v3    # "idx":I
    :cond_3
    new-instance v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    invoke-direct {v6, p0}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 555
    .local v6, "tPos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    if-ge p2, v9, :cond_4

    .line 556
    iput v1, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 562
    :goto_2
    if-le p3, v5, :cond_5

    .line 563
    iput v0, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 564
    add-int/lit8 p2, v5, 0x1

    .line 569
    :goto_3
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    move v1, v0

    move v2, v3

    .line 571
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .line 559
    .end local v2    # "idx":I
    .restart local v3    # "idx":I
    :cond_4
    add-int v10, v1, p2

    sub-int/2addr v10, v9

    iput v10, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    goto :goto_2

    .line 567
    :cond_5
    sub-int v10, p3, v9

    add-int/2addr v10, v1

    iput v10, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    goto :goto_3

    .end local v0    # "current_end":I
    .end local v5    # "tEnd":I
    .end local v6    # "tPos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .end local v9    # "tStart":I
    :cond_6
    move v2, v3

    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .line 531
    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
    .end packed-switch
.end method

.method public convert_Total_Index(III)Ljava/util/ArrayList;
    .locals 10
    .param p1, "dspMode"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/mean/TagConverter$DictPos;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 609
    .local v7, "tPosList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/diotek/diodict/mean/TagConverter$DictPos;>;"
    const/4 v2, 0x0

    .line 610
    .local v2, "idx":I
    const/4 v1, 0x0

    .line 613
    .local v1, "current_start":I
    iget v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    and-int/2addr v8, p1

    if-nez v8, :cond_1

    .line 614
    const-string/jumbo v8, "TagConverter"

    const-string/jumbo v9, "convert_Total_Index() Wrong display mode"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_0
    :goto_0
    return-object v7

    .line 618
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 627
    :goto_1
    iget-object v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 628
    iget-object v8, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idx":I
    .local v3, "idx":I
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    .line 629
    .local v4, "pItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iget v8, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->display_mask:I

    if-ne v8, p1, :cond_5

    .line 630
    iget v8, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    add-int/2addr v8, v1

    iget v9, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    sub-int/2addr v8, v9

    add-int/lit8 v0, v8, 0x1

    .line 631
    .local v0, "current_end":I
    if-le p2, v0, :cond_2

    .line 632
    move v1, v0

    move v2, v3

    .line 633
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .line 620
    .end local v0    # "current_end":I
    .end local v4    # "pItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :pswitch_0
    new-instance v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    invoke-direct {v6, p0}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 621
    .local v6, "tPosItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iput p2, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 622
    iput p3, v6, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 623
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 636
    .end local v2    # "idx":I
    .end local v6    # "tPosItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .restart local v0    # "current_end":I
    .restart local v3    # "idx":I
    .restart local v4    # "pItem":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :cond_2
    new-instance v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    invoke-direct {v5, p0}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 637
    .local v5, "tPos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iget v8, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    add-int/2addr v8, p2

    sub-int/2addr v8, v1

    iput v8, v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 638
    if-ge p3, v0, :cond_3

    .line 639
    iget v8, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    add-int/2addr v8, p3

    sub-int/2addr v8, v1

    iput v8, v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 645
    :goto_2
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    if-ge p3, v0, :cond_4

    move v2, v3

    .line 648
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0

    .line 642
    .end local v2    # "idx":I
    .restart local v3    # "idx":I
    :cond_3
    iget v8, v4, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    add-int/2addr v8, v0

    sub-int/2addr v8, v1

    iput v8, v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 643
    move p2, v0

    goto :goto_2

    .line 651
    :cond_4
    move v1, v0

    move v2, v3

    .line 652
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .end local v0    # "current_end":I
    .end local v2    # "idx":I
    .end local v5    # "tPos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .restart local v3    # "idx":I
    :cond_5
    move v2, v3

    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .line 618
    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
    .end packed-switch
.end method

.method public getAvailableDispMode()I
    .locals 1

    .prologue
    .line 503
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    return v0
.end method

.method public getFragmentOffset(Ljava/lang/String;)I
    .locals 7
    .param p1, "ref_id"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 299
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    move v0, v5

    .line 315
    :goto_0
    return v0

    .line 303
    :cond_1
    const/4 v0, 0x0

    .line 304
    .local v0, "goFragmentOffset":I
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 305
    .local v4, "size":I
    const/4 v1, 0x0

    .local v1, "idx":I
    move v2, v1

    .line 307
    .end local v1    # "idx":I
    .local v2, "idx":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 308
    iget-object v6, p0, Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 309
    .local v3, "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 310
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 311
    goto :goto_0

    :cond_2
    move v2, v1

    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    .end local v3    # "item":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    :cond_3
    move v1, v2

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_0
.end method

.method public getKeyFieldSpan()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mKeywordSpannableBuilder:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method public getLinkIndexList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/mean/TagConverter$DictPos;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMeanFieldSpan()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mContentSpannableBuilder:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method public getTheme()I
    .locals 2

    .prologue
    .line 1837
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentTheme:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentTheme:I

    if-lez v0, :cond_0

    .line 1838
    iget v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentTheme:I

    .line 1839
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLinkOffset(I)Lcom/diotek/diodict/mean/TagConverter$DictPos;
    .locals 3
    .param p1, "off"    # I

    .prologue
    .line 584
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 585
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    .line 587
    .local v1, "pos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iget v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    if-gt v2, p1, :cond_0

    iget v2, v1, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    if-gt p1, v2, :cond_0

    .line 591
    .end local v1    # "pos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :goto_1
    return-object v1

    .line 584
    .restart local v1    # "pos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 591
    .end local v1    # "pos":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public loadMeaning(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "dicType"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    .line 205
    iput p2, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I

    .line 206
    const/16 v2, 0x7f

    iput v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    .line 207
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 215
    :goto_0
    return v0

    .line 211
    :cond_1
    iput-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    .line 212
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->Convert_EngineContent_Spanned()V

    .line 213
    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    goto :goto_0
.end method

.method public loadMeaningBySource(ILjava/lang/String;)Z
    .locals 3
    .param p1, "dicType"    # I
    .param p2, "source"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 261
    iput-object p2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    .line 262
    iput p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I

    .line 263
    const/16 v2, 0x7f

    iput v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    .line 264
    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 272
    :goto_0
    return v0

    .line 268
    :cond_0
    iput-boolean v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    .line 269
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->Convert_EngineContent_Spanned()V

    .line 270
    iput-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z

    move v0, v1

    .line 272
    goto :goto_0
.end method

.method public loadMeaningWithMode(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "dicType"    # I

    .prologue
    .line 232
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    .line 233
    iput p2, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I

    .line 234
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 235
    const/4 v0, 0x0

    .line 242
    :goto_0
    return v0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mDspIdxList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 239
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 240
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->Convert_EngineContent_Spanned()V

    .line 242
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setTheme(I)I
    .locals 3
    .param p1, "theme"    # I

    .prologue
    .line 1815
    const/4 v0, 0x0

    .line 1816
    .local v0, "nMeanColor":I
    iput p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurrentTheme:I

    .line 1817
    packed-switch p1, :pswitch_data_0

    .line 1828
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->baseTheme:[I

    iput-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mBaseTheme:[I

    .line 1829
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->advTheme:[I

    iput-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    .line 1832
    :goto_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mBaseTheme:[I

    const/4 v2, 0x4

    aget v0, v1, v2

    .line 1833
    return v0

    .line 1819
    :pswitch_0
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->baseTheme2:[I

    iput-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mBaseTheme:[I

    .line 1820
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->advTheme2:[I

    iput-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    goto :goto_0

    .line 1823
    :pswitch_1
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->baseTheme3:[I

    iput-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mBaseTheme:[I

    .line 1824
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->advTheme3:[I

    iput-object v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mAdvTheme:[I

    goto :goto_0

    .line 1817
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateDispalyMode(I)Z
    .locals 3
    .param p1, "displayMode"    # I

    .prologue
    const/4 v0, 0x1

    .line 285
    const/16 v1, 0x200

    and-int/lit16 v2, p1, 0xf00

    if-ne v1, v2, :cond_0

    .line 286
    and-int/lit16 p1, p1, 0xff

    .line 287
    iput-boolean v0, p0, Lcom/diotek/diodict/mean/TagConverter;->mIsShortMeanMode:Z

    .line 290
    :cond_0
    iget v1, p0, Lcom/diotek/diodict/mean/TagConverter;->mAvailableMode:I

    and-int/2addr v1, p1

    if-nez v1, :cond_1

    .line 291
    const/4 v0, 0x0

    .line 295
    :goto_0
    return v0

    .line 293
    :cond_1
    iput p1, p0, Lcom/diotek/diodict/mean/TagConverter;->mCurDspMode:I

    .line 294
    invoke-direct {p0}, Lcom/diotek/diodict/mean/TagConverter;->Convert_EngineContent_Spanned()V

    goto :goto_0
.end method
