.class Lcom/diotek/diodict/mean/TagConverter$3;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1004
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 1016
    const/4 v0, 0x1

    .line 1017
    .local v0, "linefeed":Z
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x29

    if-ne v1, v2, :cond_0

    .line 1018
    const/4 v0, 0x0

    .line 1020
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-virtual {v1, p1, v0}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(IZ)V

    .line 1021
    return-void
.end method

.method public handleTag_Start(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 1006
    const/4 v0, 0x1

    .line 1007
    .local v0, "linefeed":Z
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x28

    if-ne v1, v2, :cond_0

    .line 1009
    const/4 v0, 0x0

    .line 1010
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$3;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v2, -0x1

    invoke-virtual {v1, p1, v2, v0}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(IIZ)V

    .line 1011
    return-void
.end method
