.class Lcom/diotek/diodict/mean/TagConverter$11;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 6
    .param p1, "tag"    # I

    .prologue
    .line 1350
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1351
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v1, p1, :cond_1

    .line 1352
    const-string/jumbo v1, "TagConverter"

    const-string/jumbo v2, "Tag Missing L"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1386
    :cond_0
    :goto_0
    return-void

    .line 1355
    :cond_1
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v2, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v2}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1358
    and-int/lit16 v1, p1, 0xff

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 1360
    :sswitch_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/diotek/diodict/mean/TagConverter$11$1;

    invoke-direct {v2, p0}, Lcom/diotek/diodict/mean/TagConverter$11$1;-><init>(Lcom/diotek/diodict/mean/TagConverter$11;)V

    iget v3, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 1377
    :sswitch_1
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsStyleClose:Z
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2100(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1378
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(IZ)V

    goto :goto_0

    .line 1380
    :cond_2
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-virtual {v1, p1}, Lcom/diotek/diodict/mean/TagConverter;->WideBlockTag_End(I)V

    goto :goto_0

    .line 1383
    :sswitch_2
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-virtual {v1, p1}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(I)V

    goto :goto_0

    .line 1358
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method public handleTag_Start(I)V
    .locals 14
    .param p1, "tag"    # I

    .prologue
    .line 1236
    and-int/lit16 v10, p1, 0xff

    packed-switch v10, :pswitch_data_0

    .line 1344
    :pswitch_0
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v11, -0x1

    const/16 v12, 0xf

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v10, p1, v11, v12}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    .line 1347
    :cond_0
    :goto_0
    return-void

    .line 1238
    :pswitch_1
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1200(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v10

    invoke-static {v10}, Lcom/diotek/diodict/engine/DictDBManager;->isOxfordDict(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1239
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v11, -0x1

    invoke-virtual {v10, p1, v11}, Lcom/diotek/diodict/mean/TagConverter;->WideBlockTag_Start(II)V

    goto :goto_0

    .line 1241
    :cond_1
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v11, -0x1

    invoke-virtual {v10, p1, v11}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(II)V

    goto :goto_0

    .line 1244
    :pswitch_2
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "%l04"

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 1246
    .local v1, "end_index":I
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsBuildCtrlData:Z
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1300(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1249
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v0

    .line 1250
    .local v0, "dstIndex":I
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v11, v1, 0x4

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0xa

    if-ne v10, v11, :cond_2

    .line 1251
    add-int/lit8 v0, v0, 0x1

    .line 1252
    :cond_2
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1253
    .local v7, "tItem":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    invoke-virtual {v11, v12, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1254
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1255
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIDList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1400(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1257
    .end local v0    # "dstIndex":I
    .end local v7    # "tItem":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    :cond_3
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    add-int/lit8 v11, v1, 0x4

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10, v11}, Lcom/diotek/diodict/mean/TagConverter;->access$802(Lcom/diotek/diodict/mean/TagConverter;I)I

    goto/16 :goto_0

    .line 1261
    .end local v1    # "end_index":I
    :pswitch_3
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1500(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_4

    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1500(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$1500(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget v10, v10, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-ne v10, v11, :cond_4

    .line 1263
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1264
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$908(Lcom/diotek/diodict/mean/TagConverter;)I

    .line 1267
    :cond_4
    const/4 v8, 0x0

    .line 1268
    .local v8, "url":Ljava/lang/String;
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v6

    .line 1269
    .local v6, "start":I
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    .line 1270
    .local v2, "end_point":I
    :goto_1
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v10

    if-ge v10, v2, :cond_0

    .line 1271
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->codePointAt(I)I

    move-result v10

    const/16 v11, 0x25

    if-ne v10, v11, :cond_b

    .line 1272
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->getTagValue(Ljava/lang/String;I)I
    invoke-static {v10, v11, v12}, Lcom/diotek/diodict/mean/TagConverter;->access$1600(Lcom/diotek/diodict/mean/TagConverter;Ljava/lang/String;I)I

    move-result v3

    .line 1274
    .local v3, "inner_tag":I
    const/16 v10, 0xb06

    if-ne v3, v10, :cond_5

    .line 1275
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v11, 0x4

    # += operator for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10, v11}, Lcom/diotek/diodict/mean/TagConverter;->access$812(Lcom/diotek/diodict/mean/TagConverter;I)I

    .line 1276
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "%l06"

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 1277
    .restart local v1    # "end_index":I
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v11

    invoke-virtual {v10, v11, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 1278
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    add-int/lit8 v11, v1, 0x4

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10, v11}, Lcom/diotek/diodict/mean/TagConverter;->access$802(Lcom/diotek/diodict/mean/TagConverter;I)I

    goto :goto_1

    .line 1281
    .end local v1    # "end_index":I
    :cond_5
    const/16 v10, 0xb05

    if-ne v3, v10, :cond_7

    .line 1282
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v11, 0x4

    # += operator for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10, v11}, Lcom/diotek/diodict/mean/TagConverter;->access$812(Lcom/diotek/diodict/mean/TagConverter;I)I

    .line 1283
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1284
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1286
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mIsLinkable:Z
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1800(Lcom/diotek/diodict/mean/TagConverter;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1289
    :goto_2
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v6, v10, :cond_6

    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v10

    const/16 v11, 0xa

    if-ne v10, v11, :cond_6

    .line 1291
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1294
    :cond_6
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    new-instance v11, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {v11, v12, v8}, Lcom/diotek/diodict/mean/TagConverter$LinkSpan;-><init>(Lcom/diotek/diodict/mean/TagConverter;Ljava/lang/String;)V

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    const/16 v13, 0x21

    invoke-virtual {v10, v11, v6, v12, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1298
    new-instance v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;

    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {v5, v10}, Lcom/diotek/diodict/mean/TagConverter$DictPos;-><init>(Lcom/diotek/diodict/mean/TagConverter;)V

    .line 1299
    .local v5, "item":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    iput v6, v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;->start:I

    .line 1300
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    iput v10, v5, Lcom/diotek/diodict/mean/TagConverter$DictPos;->end:I

    .line 1301
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mLinkIdxList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1500(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1305
    .end local v5    # "item":Lcom/diotek/diodict/mean/TagConverter$DictPos;
    :cond_7
    if-lez v3, :cond_b

    .line 1306
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->codePointAt(I)I

    move-result v10

    const/16 v11, 0x60

    if-ge v10, v11, :cond_8

    const/4 v4, 0x1

    .line 1307
    .local v4, "isStart":Z
    :goto_3
    const v10, 0xff00

    and-int/2addr v10, v3

    if-lez v10, :cond_9

    shr-int/lit8 v9, v3, 0x8

    .line 1308
    .local v9, "value":I
    :goto_4
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mTagLength:[B
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$1900(Lcom/diotek/diodict/mean/TagConverter;)[B

    move-result-object v11

    aget-byte v11, v11, v9

    add-int/lit8 v11, v11, 0x1

    # += operator for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10, v11}, Lcom/diotek/diodict/mean/TagConverter;->access$812(Lcom/diotek/diodict/mean/TagConverter;I)I

    .line 1310
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1311
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1312
    if-eqz v4, :cond_a

    .line 1313
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$500(Lcom/diotek/diodict/mean/TagConverter;)[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    move-result-object v10

    aget-object v10, v10, v9

    invoke-interface {v10, v3}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_Start(I)V

    goto/16 :goto_1

    .line 1306
    .end local v4    # "isStart":Z
    .end local v9    # "value":I
    :cond_8
    const/4 v4, 0x0

    goto :goto_3

    .restart local v4    # "isStart":Z
    :cond_9
    move v9, v3

    .line 1307
    goto :goto_4

    .line 1316
    .restart local v9    # "value":I
    :cond_a
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mTagHandler:[Lcom/diotek/diodict/mean/TagConverter$TagHandler;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$500(Lcom/diotek/diodict/mean/TagConverter;)[Lcom/diotek/diodict/mean/TagConverter$TagHandler;

    move-result-object v10

    aget-object v10, v10, v9

    invoke-interface {v10, v3}, Lcom/diotek/diodict/mean/TagConverter$TagHandler;->handleTag_End(I)V

    .line 1317
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Pop(I)V
    invoke-static {v10, p1}, Lcom/diotek/diodict/mean/TagConverter;->access$2000(Lcom/diotek/diodict/mean/TagConverter;I)V

    goto/16 :goto_1

    .line 1323
    .end local v3    # "inner_tag":I
    .end local v4    # "isStart":Z
    .end local v9    # "value":I
    :cond_b
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v11}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$808(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->codePointAt(I)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 1324
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$908(Lcom/diotek/diodict/mean/TagConverter;)I

    goto/16 :goto_1

    .line 1331
    .end local v2    # "end_point":I
    .end local v6    # "start":I
    .end local v8    # "url":Ljava/lang/String;
    :pswitch_4
    const-string/jumbo v10, "TagConverter"

    const-string/jumbo v11, "File Link parsing error"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1335
    :pswitch_5
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v11, -0x1

    invoke-virtual {v10, p1, v11}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(II)V

    goto/16 :goto_0

    .line 1339
    :pswitch_6
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSource:Ljava/lang/String;
    invoke-static {v10}, Lcom/diotek/diodict/mean/TagConverter;->access$700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "%l09"

    iget-object v12, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v12}, Lcom/diotek/diodict/mean/TagConverter;->access$800(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 1340
    .restart local v1    # "end_index":I
    iget-object v10, p0, Lcom/diotek/diodict/mean/TagConverter$11;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    add-int/lit8 v11, v1, 0x4

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mSrcIndex:I
    invoke-static {v10, v11}, Lcom/diotek/diodict/mean/TagConverter;->access$802(Lcom/diotek/diodict/mean/TagConverter;I)I

    goto/16 :goto_0

    .line 1236
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
