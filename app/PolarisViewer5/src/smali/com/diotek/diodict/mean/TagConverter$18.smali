.class Lcom/diotek/diodict/mean/TagConverter$18;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 1574
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 9
    .param p1, "tag"    # I

    .prologue
    const/16 v8, 0x21

    .line 1609
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1610
    .local v1, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v4, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v4, p1, :cond_1

    .line 1611
    const-string/jumbo v4, "TagConverter"

    const-string/jumbo v5, "Tag Missing W"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1696
    :cond_0
    :goto_0
    return-void

    .line 1614
    :cond_1
    iget v4, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v5, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v5}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v5

    if-eq v4, v5, :cond_0

    .line 1617
    and-int/lit16 v4, p1, 0xff

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 1620
    :sswitch_0
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-virtual {v4, p1}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_End(I)V

    goto :goto_0

    .line 1629
    :sswitch_1
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget v6, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->f_Color:I

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget v6, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v7}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v7

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1633
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1634
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1635
    .local v0, "curItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 1636
    .local v2, "parentItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    if-eqz v2, :cond_2

    .line 1637
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    iget v5, v2, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->getTagIndex(I)I
    invoke-static {v4, v5}, Lcom/diotek/diodict/mean/TagConverter;->access$2500(Lcom/diotek/diodict/mean/TagConverter;I)I

    move-result v3

    .line 1638
    .local v3, "tag_index":I
    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 1639
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    new-instance v5, Lcom/diotek/diodict/mean/TagConverter$18$1;

    invoke-direct {v5, p0}, Lcom/diotek/diodict/mean/TagConverter$18$1;-><init>(Lcom/diotek/diodict/mean/TagConverter$18;)V

    iget v6, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v7}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v7

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1659
    .end local v3    # "tag_index":I
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1648
    .restart local v3    # "tag_index":I
    :cond_3
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 1649
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    new-instance v5, Lcom/diotek/diodict/mean/TagConverter$18$2;

    invoke-direct {v5, p0}, Lcom/diotek/diodict/mean/TagConverter$18$2;-><init>(Lcom/diotek/diodict/mean/TagConverter$18;)V

    iget v6, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v7}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v7

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 1681
    .end local v0    # "curItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    .end local v2    # "parentItem":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    .end local v3    # "tag_index":I
    :sswitch_2
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mSpannableBuilder:Landroid/text/SpannableStringBuilder;
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$600(Lcom/diotek/diodict/mean/TagConverter;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    new-instance v5, Lcom/diotek/diodict/mean/TagConverter$18$3;

    invoke-direct {v5, p0}, Lcom/diotek/diodict/mean/TagConverter$18$3;-><init>(Lcom/diotek/diodict/mean/TagConverter$18;)V

    iget v6, v1, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->start:I

    iget-object v7, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v7}, Lcom/diotek/diodict/mean/TagConverter;->access$900(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v7

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1690
    iget-object v4, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->assertLineFeed()V
    invoke-static {v4}, Lcom/diotek/diodict/mean/TagConverter;->access$2600(Lcom/diotek/diodict/mean/TagConverter;)V

    goto/16 :goto_0

    .line 1617
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method public handleTag_Start(I)V
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1576
    and-int/lit16 v1, p1, 0xff

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1577
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v2, -0x1

    invoke-virtual {v1, p1, v2}, Lcom/diotek/diodict/mean/TagConverter;->BlockTag_Start(II)V

    .line 1589
    :goto_0
    and-int/lit16 v1, p1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 1606
    :goto_1
    return-void

    .line 1579
    :cond_0
    const/4 v0, -0x1

    .line 1580
    .local v0, "Color":I
    and-int/lit16 v1, p1, 0xff

    if-ne v1, v4, :cond_1

    .line 1581
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mCurrentDicType:I
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$1200(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v1

    invoke-static {v1}, Lcom/diotek/diodict/engine/DictDBManager;->isVanDaleDict(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1582
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->w3color_grey:[I

    aget v0, v1, v3

    .line 1586
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/16 v2, 0xf

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v1, p1, v0, v2}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    goto :goto_0

    .line 1584
    :cond_2
    sget-object v1, Lcom/diotek/diodict/engine/ThemeColor;->w3color:[I

    aget v0, v1, v3

    goto :goto_2

    .line 1592
    .end local v0    # "Color":I
    :pswitch_0
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # setter for: Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I
    invoke-static {v1, v3}, Lcom/diotek/diodict/mean/TagConverter;->access$2402(Lcom/diotek/diodict/mean/TagConverter;I)I

    goto :goto_1

    .line 1599
    :pswitch_1
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2400(Lcom/diotek/diodict/mean/TagConverter;)I

    move-result v1

    if-lez v1, :cond_3

    .line 1600
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStringBuilder:Ljava/lang/StringBuilder;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$1700(Lcom/diotek/diodict/mean/TagConverter;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1601
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # += operator for: Lcom/diotek/diodict/mean/TagConverter;->mDstIndex:I
    invoke-static {v1, v4}, Lcom/diotek/diodict/mean/TagConverter;->access$912(Lcom/diotek/diodict/mean/TagConverter;I)I

    .line 1603
    :cond_3
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$18;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # operator++ for: Lcom/diotek/diodict/mean/TagConverter;->mNumTableData:I
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$2408(Lcom/diotek/diodict/mean/TagConverter;)I

    goto :goto_1

    .line 1589
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
