.class Lcom/diotek/diodict/mean/TagConverter$1;
.super Ljava/lang/Object;
.source "TagConverter.java"

# interfaces
.implements Lcom/diotek/diodict/mean/TagConverter$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict/mean/TagConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict/mean/TagConverter;


# direct methods
.method constructor <init>(Lcom/diotek/diodict/mean/TagConverter;)V
    .locals 0

    .prologue
    .line 976
    iput-object p1, p0, Lcom/diotek/diodict/mean/TagConverter$1;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleTag_End(I)V
    .locals 4
    .param p1, "tag"    # I

    .prologue
    .line 982
    iget-object v1, p0, Lcom/diotek/diodict/mean/TagConverter$1;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    # getter for: Lcom/diotek/diodict/mean/TagConverter;->mStyleStack:Ljava/util/Stack;
    invoke-static {v1}, Lcom/diotek/diodict/mean/TagConverter;->access$300(Lcom/diotek/diodict/mean/TagConverter;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;

    .line 983
    .local v0, "item":Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;
    iget v1, v0, Lcom/diotek/diodict/mean/TagConverter$StyleStackItem;->tag:I

    if-eq v1, p1, :cond_0

    .line 984
    const-string/jumbo v1, "TagConverter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Tag Missing remover"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    :cond_0
    return-void
.end method

.method public handleTag_Start(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    .line 978
    iget-object v0, p0, Lcom/diotek/diodict/mean/TagConverter$1;->this$0:Lcom/diotek/diodict/mean/TagConverter;

    const/4 v1, -0x1

    const/16 v2, 0xf

    # invokes: Lcom/diotek/diodict/mean/TagConverter;->StyleStack_Push(III)V
    invoke-static {v0, p1, v1, v2}, Lcom/diotek/diodict/mean/TagConverter;->access$200(Lcom/diotek/diodict/mean/TagConverter;III)V

    .line 979
    return-void
.end method
