.class public Lcom/diotek/diodict3/phone/service/ExSupportLanguage;
.super Ljava/lang/Object;
.source "ExSupportLanguage.java"


# static fields
.field public static final Arabic:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Chinese_Simplified:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Croatian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Czech:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Danish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Dutch:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final English_UK:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final English_US:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Finnish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final French:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final German:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Greek:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Italian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Japanese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Korean:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Norwegian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Polish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Portuguese_Brazil:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Portuguese_Portugal:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Russian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Spanish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Spanish_Latin_America:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Swedish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Thai:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Turkish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

.field public static final Vietnamese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;


# instance fields
.field private languageCode:I

.field private languageDescription:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x0

    const-string/jumbo v2, "Arabic"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Arabic:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 17
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x1

    const-string/jumbo v2, "Chinese (Simplified)"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Chinese_Simplified:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 22
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x2

    const-string/jumbo v2, "Croatian"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Croatian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 27
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x3

    const-string/jumbo v2, "Czech"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Czech:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 32
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x4

    const-string/jumbo v2, "Danish"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Danish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 37
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x5

    const-string/jumbo v2, "Dutch"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Dutch:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 42
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x6

    const-string/jumbo v2, "English (UK)"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->English_UK:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 47
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/4 v1, 0x7

    const-string/jumbo v2, "English (US)"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->English_US:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 52
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x8

    const-string/jumbo v2, "Finnish"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Finnish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 57
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x9

    const-string/jumbo v2, "French"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->French:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 62
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0xa

    const-string/jumbo v2, "German"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->German:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 67
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0xb

    const-string/jumbo v2, "Greek"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Greek:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 72
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0xc

    const-string/jumbo v2, "Italian"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Italian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 77
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0xd

    const-string/jumbo v2, "Japanese"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Japanese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 82
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0xe

    const-string/jumbo v2, "Korean"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Korean:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 87
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0xf

    const-string/jumbo v2, "Norwegian"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Norwegian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 92
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x10

    const-string/jumbo v2, "Polish"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Polish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 97
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x11

    const-string/jumbo v2, "Portuguese (Brazil)"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Portuguese_Brazil:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 102
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x12

    const-string/jumbo v2, "Portuguese (Portugal)"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Portuguese_Portugal:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 107
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x13

    const-string/jumbo v2, "Russian"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Russian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 112
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x14

    const-string/jumbo v2, "Spanish"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Spanish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 117
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x15

    const-string/jumbo v2, "Spanish (Latin America)"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Spanish_Latin_America:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 122
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x16

    const-string/jumbo v2, "Swedish"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Swedish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 127
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x17

    const-string/jumbo v2, "Thai"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Thai:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 132
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x18

    const-string/jumbo v2, "Turkish"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Turkish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 137
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    const/16 v1, 0x19

    const-string/jumbo v2, "Vietnamese"

    invoke-direct {v0, v1, v2}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;-><init>(ILjava/lang/String;)V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Vietnamese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "languageCode"    # I
    .param p2, "languageDescription"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->languageCode:I

    .line 153
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->languageDescription:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public static getSupportLanguage(I)Lcom/diotek/diodict3/phone/service/ExSupportLanguage;
    .locals 2
    .param p0, "code"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "support":Lcom/diotek/diodict3/phone/service/ExSupportLanguage;
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Arabic:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_1

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Arabic:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .line 201
    .end local v0    # "support":Lcom/diotek/diodict3/phone/service/ExSupportLanguage;
    :cond_0
    :goto_0
    return-object v0

    .line 175
    .restart local v0    # "support":Lcom/diotek/diodict3/phone/service/ExSupportLanguage;
    :cond_1
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Chinese_Simplified:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_2

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Chinese_Simplified:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 176
    :cond_2
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Croatian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_3

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Croatian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 177
    :cond_3
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Czech:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_4

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Czech:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 178
    :cond_4
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Danish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_5

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Danish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 179
    :cond_5
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Dutch:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_6

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Dutch:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 180
    :cond_6
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->English_UK:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_7

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->English_UK:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 181
    :cond_7
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->English_US:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_8

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->English_US:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 182
    :cond_8
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Finnish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_9

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Finnish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 183
    :cond_9
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->French:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_a

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->French:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 184
    :cond_a
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->German:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_b

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->German:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 185
    :cond_b
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Greek:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_c

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Greek:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto :goto_0

    .line 186
    :cond_c
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Italian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_d

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Italian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 187
    :cond_d
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Japanese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_e

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Japanese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 188
    :cond_e
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Korean:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_f

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Korean:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 189
    :cond_f
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Norwegian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_10

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Norwegian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 190
    :cond_10
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Polish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_11

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Polish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 191
    :cond_11
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Portuguese_Brazil:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_12

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Portuguese_Brazil:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 192
    :cond_12
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Portuguese_Portugal:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_13

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Portuguese_Portugal:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 193
    :cond_13
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Russian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_14

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Russian:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 194
    :cond_14
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Spanish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_15

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Spanish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 195
    :cond_15
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Spanish_Latin_America:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_16

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Spanish_Latin_America:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 196
    :cond_16
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Swedish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_17

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Swedish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 197
    :cond_17
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Thai:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_18

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Thai:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 198
    :cond_18
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Turkish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_19

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Turkish:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0

    .line 199
    :cond_19
    sget-object v1, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Vietnamese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    if-ne v1, p0, :cond_0

    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->Vietnamese:Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    goto/16 :goto_0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->languageCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 162
    const-string/jumbo v0, "Language : %d (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->languageCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->languageDescription:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
