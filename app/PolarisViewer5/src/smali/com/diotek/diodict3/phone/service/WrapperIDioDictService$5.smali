.class Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$5;
.super Landroid/os/Handler;
.source "WrapperIDioDictService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;


# direct methods
.method constructor <init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V
    .locals 0

    .prologue
    .line 1571
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$5;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1575
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 1576
    .local v6, "bundle":Landroid/os/Bundle;
    const-string/jumbo v0, "webview_navigate_message_key"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1577
    const-string/jumbo v0, "webview_navigate_message_key"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1578
    .local v2, "data":Ljava/lang/String;
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "Success get meaning data length : %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1579
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$5;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$700(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1580
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "Success get Webview"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1581
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$5;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$700(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Landroid/webkit/WebView;

    move-result-object v0

    const-string/jumbo v1, "file:///data/data/com.diotek.diodict3.service/files/"

    const-string/jumbo v3, "text/html"

    const-string/jumbo v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "load meaning"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1593
    .end local v2    # "data":Ljava/lang/String;
    :goto_0
    return-void

    .line 1585
    .restart local v2    # "data":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "Webview is null!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1589
    .end local v2    # "data":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "meaning data is null!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1592
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
