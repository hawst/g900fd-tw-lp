.class public final Lcom/diotek/diodict3/phone/service/ExSearchInfo;
.super Ljava/lang/Object;
.source "ExSearchInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/diotek/diodict3/phone/service/ExSearchInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAIN_SEARCH_NORMAL:I = 0x0

.field public static final MAIN_SEARCH_ORIGIN:I = 0x2

.field public static final MAIN_SEARCH_SAMEWORD:I = 0x1


# instance fields
.field private mDicType:I

.field private mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

.field private mKeyword:Ljava/lang/String;

.field private mSearchType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo$1;

    invoke-direct {v0}, Lcom/diotek/diodict3/phone/service/ExSearchInfo$1;-><init>()V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    .line 53
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mSearchType:I

    .line 56
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .locals 4
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "suid"    # I

    .prologue
    const/4 v3, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    .line 44
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    .line 45
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v1, 0x3

    new-array v1, v1, [I

    aput p3, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    const/4 v2, 0x2

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 48
    iput v3, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mSearchType:I

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/diotek/diodict3/phone/service/ExSearchInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/diotek/diodict3/phone/service/ExSearchInfo$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    .line 65
    const-class v0, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mSearchType:I

    .line 67
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public getDicType()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    return v0
.end method

.method public getEntryId()Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    return-object v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchType()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mSearchType:I

    return v0
.end method

.method public getSuid()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    .line 177
    return-void
.end method

.method public setSearchInfo(ILjava/lang/String;I)V
    .locals 4
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "suid"    # I

    .prologue
    const/4 v3, 0x0

    .line 85
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    .line 86
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    .line 87
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v1, 0x3

    new-array v1, v1, [I

    aput p3, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    const/4 v2, 0x2

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 90
    return-void
.end method

.method public setSearchInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V
    .locals 0
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 100
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    .line 101
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    .line 102
    iput-object p3, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 103
    return-void
.end method

.method public setSearchType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mSearchType:I

    .line 115
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 71
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mDicType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mKeyword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 74
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->mSearchType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 75
    return-void
.end method
