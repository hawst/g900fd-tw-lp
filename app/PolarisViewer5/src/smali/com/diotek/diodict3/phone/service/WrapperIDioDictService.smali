.class public Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
.super Ljava/lang/Object;
.source "WrapperIDioDictService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$DbInfo;,
        Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field private static final MEANTYPE_ALL:I = 0x0

.field private static final MEANTYPE_EXAMPLE:I = 0x3

.field private static final MEANTYPE_IDIOM:I = 0x2

.field private static final MEANTYPE_PURE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WrapperIDioDictService"

.field private static final WEBVIEW_NAVIGATE_MESSAGE_KEY:Ljava/lang/String; = "webview_navigate_message_key"


# instance fields
.field private final SAMSUNG_OCR_PACKAGE_NAME:Ljava/lang/String;

.field private mAvailableDBList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

.field private mContentsBuilder:Landroid/text/SpannableStringBuilder;

.field private mContext:Landroid/content/Context;

.field private mDBListMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$DbInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDicType:I

.field private mIsImportantMeaning:Z

.field private mIsServiceBinded:Z

.field private mKeywordBuilder:Landroid/text/SpannableStringBuilder;

.field private mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

.field private mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

.field private mMeanWebView:Landroid/webkit/WebView;

.field private mProviderTableItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

.field private mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

.field private mSerial:Ljava/lang/String;

.field private mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

.field private mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

.field private mStartCallbackHandler:Landroid/os/Handler;

.field private mWebviewLoadDataHandler:Landroid/os/Handler;

.field private mWebviewLoadUrlHandler:Landroid/os/Handler;

.field private m_connection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serial"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    .line 55
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    .line 57
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    .line 59
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    .line 60
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    .line 62
    const/16 v0, 0xb04

    iput v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 64
    iput-boolean v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z

    .line 72
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    .line 75
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 76
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    .line 77
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    .line 78
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    .line 79
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mKeywordBuilder:Landroid/text/SpannableStringBuilder;

    .line 80
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContentsBuilder:Landroid/text/SpannableStringBuilder;

    .line 82
    iput-boolean v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsImportantMeaning:Z

    .line 83
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    .line 85
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mStartCallbackHandler:Landroid/os/Handler;

    .line 89
    const-string/jumbo v0, "com.sec.android.app.ocr"

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->SAMSUNG_OCR_PACKAGE_NAME:Ljava/lang/String;

    .line 341
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;

    invoke-direct {v0, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->m_connection:Landroid/content/ServiceConnection;

    .line 1571
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$5;

    invoke-direct {v0, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$5;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mWebviewLoadDataHandler:Landroid/os/Handler;

    .line 1597
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$6;

    invoke-direct {v0, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$6;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mWebviewLoadUrlHandler:Landroid/os/Handler;

    .line 98
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    .line 99
    iput-object p3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    .line 100
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    .line 101
    sget-boolean v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 102
    const-string/jumbo v0, "WrapperIDioDictService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mSerial = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$1;

    invoke-direct {v0, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$1;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 50
    sget-boolean v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    return v0
.end method

.method static synthetic access$200(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictService;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    return-object v0
.end method

.method static synthetic access$202(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Lcom/diotek/diodict3/phone/service/IDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictService;
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
    .param p1, "x1"    # Lcom/diotek/diodict3/phone/service/IDioDictService;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    return-object p1
.end method

.method static synthetic access$302(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z

    return p1
.end method

.method static synthetic access$400(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getApiVersion()V

    return-void
.end method

.method static synthetic access$500(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    return-object v0
.end method

.method static synthetic access$502(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;)Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    .locals 0
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
    .param p1, "x1"    # Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    return-object p1
.end method

.method static synthetic access$600(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mStartCallbackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method private addBoldKeywordForSamsungOCR()V
    .locals 6

    .prologue
    .line 739
    const-string/jumbo v1, "com.sec.android.app.ocr"

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 740
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v2}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 741
    .local v0, "keywordStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 742
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContentsBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 743
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContentsBuilder:Landroid/text/SpannableStringBuilder;

    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 749
    .end local v0    # "keywordStr":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private changeDicType(ILjava/lang/String;)I
    .locals 1
    .param p1, "dicType"    # I
    .param p2, "word"    # Ljava/lang/String;

    .prologue
    .line 1470
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1471
    invoke-virtual {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->reqDBList()Z

    .line 1474
    :cond_0
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getAvailableDicType(I)I

    move-result v0

    return v0
.end method

.method private checkDBTypeAvailable(I)Z
    .locals 3
    .param p1, "dictType"    # I

    .prologue
    .line 938
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 940
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqIsDBEnable(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 941
    const/4 v1, 0x1

    .line 947
    :goto_0
    return v1

    .line 943
    :catch_0
    move-exception v0

    .line 944
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 947
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private chkPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 433
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 435
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 436
    const/4 v2, 0x1

    .line 440
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v2

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "ex":Ljava/lang/Exception;
    goto :goto_0
.end method

.method private getApiVersion()V
    .locals 7

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 472
    :try_start_0
    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v4}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqApiVersion()Ljava/lang/String;

    move-result-object v3

    .line 474
    .local v3, "result":Ljava/lang/String;
    sget-boolean v4, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 475
    const-string/jumbo v2, ""

    .line 476
    .local v2, "log":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 477
    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v4}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v1

    .line 478
    .local v1, "err":I
    if-nez v1, :cond_3

    .line 479
    const-string/jumbo v2, "API Not-supported"

    .line 488
    .end local v1    # "err":I
    :cond_0
    :goto_0
    const-string/jumbo v4, "WrapperIDioDictService"

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    .end local v2    # "log":Ljava/lang/String;
    :cond_1
    if-eqz v3, :cond_2

    .line 491
    const-string/jumbo v4, "WrapperIDioDictService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Service Version : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    .end local v3    # "result":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 481
    .restart local v1    # "err":I
    .restart local v2    # "log":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    :cond_3
    if-gez v1, :cond_0

    .line 482
    const-string/jumbo v2, "Error Return"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 494
    .end local v1    # "err":I
    .end local v2    # "log":Ljava/lang/String;
    .end local v3    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1

    .line 498
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    sget-boolean v4, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 499
    const-string/jumbo v4, "WrapperIDioDictService"

    const-string/jumbo v5, "Service not active"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getAvailableDicType(I)I
    .locals 2
    .param p1, "dicType"    # I

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1478
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    .line 1481
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1488
    .end local p1    # "dicType":I
    :goto_0
    return p1

    .line 1484
    .restart local p1    # "dicType":I
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1485
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    .line 1488
    :cond_2
    const/4 p1, -0x1

    goto :goto_0
.end method

.method private getDisplayMode(I)I
    .locals 2
    .param p1, "meanKinds"    # I

    .prologue
    .line 1102
    const/16 v0, 0xf

    .line 1103
    .local v0, "displayMode":I
    if-nez p1, :cond_1

    .line 1112
    :cond_0
    :goto_0
    return v0

    .line 1105
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 1106
    const/4 v0, 0x1

    goto :goto_0

    .line 1107
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 1108
    const/4 v0, 0x2

    goto :goto_0

    .line 1109
    :cond_3
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 1110
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private getMeaning(Ljava/lang/String;II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "dicType"    # I
    .param p3, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 769
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 770
    invoke-direct {p0, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->checkDBTypeAvailable(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 777
    :cond_0
    :goto_0
    return-object v0

    .line 772
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->requestSearchWord(Ljava/lang/String;I)Lcom/diotek/diodict3/phone/service/ExSearchResult;

    move-result-object v1

    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    .line 773
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v1, :cond_0

    .line 774
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getKeyword()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getSuid()I

    move-result v1

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v2}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getDicType()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getMeaning(Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "dicType"    # I
    .param p4, "type"    # I

    .prologue
    .line 685
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 686
    invoke-direct {p0, p3, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setMeanInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V

    .line 689
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-interface {v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqMeaning(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExMeanInfo;)Lcom/diotek/diodict3/phone/service/ExMeanResult;

    move-result-object v1

    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    .line 690
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 691
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->getMeaning()Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 697
    :goto_0
    return-object v1

    .line 693
    :catch_0
    move-exception v0

    .line 694
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 697
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIIIZ)Z
    .locals 7
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "dicType"    # I
    .param p4, "resultType"    # I
    .param p5, "contentsType"    # I
    .param p6, "meanKinds"    # I
    .param p7, "bShortMean"    # Z

    .prologue
    .line 1032
    invoke-direct {p0, p6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getDisplayMode(I)I

    move-result v5

    .line 1034
    .local v5, "displayMode":I
    if-eqz p7, :cond_0

    .line 1035
    or-int/lit16 v5, v5, 0x200

    .line 1037
    :cond_0
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p5

    move v6, p4

    .line 1038
    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->reqMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIII)Z

    move-result v0

    .line 1040
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSuidList()[I
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getSuidList()[I

    move-result-object v0

    .line 654
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getWordList(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "dbtype"    # I

    .prologue
    const/4 v1, 0x0

    .line 798
    invoke-direct {p0, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->checkDBTypeAvailable(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 813
    :cond_0
    :goto_0
    return v1

    .line 801
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->requestSearchWord(Ljava/lang/String;I)Lcom/diotek/diodict3/phone/service/ExSearchResult;

    move-result-object v2

    iput-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    .line 802
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 804
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v2}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 805
    const/4 v1, 0x1

    goto :goto_0

    .line 809
    :catch_0
    move-exception v0

    .line 810
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private invokeJavaScript(Ljava/lang/String;)V
    .locals 6
    .param p1, "functionName"    # Ljava/lang/String;

    .prologue
    .line 231
    if-eqz p1, :cond_0

    .line 232
    const-string/jumbo v3, "javascript:%s();"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "query":Ljava/lang/String;
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 234
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 235
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "webview_navigate_message_key"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 237
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mWebviewLoadUrlHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 239
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "query":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private isActiveService()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 323
    iget-boolean v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    if-eqz v2, :cond_0

    .line 324
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v2}, Lcom/diotek/diodict3/phone/service/IDioDictService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 325
    .local v0, "binder":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    const/4 v1, 0x1

    .line 331
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_0
    return v1
.end method

.method private isEnglish(Ljava/lang/String;)Z
    .locals 4
    .param p1, "word"    # Ljava/lang/String;

    .prologue
    .line 1493
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1495
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_5

    .line 1496
    const/16 v2, 0x61

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x7a

    if-le v2, v3, :cond_3

    :cond_0
    const/16 v2, 0x41

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5a

    if-le v2, v3, :cond_3

    :cond_1
    const/16 v2, 0xc0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xf6

    if-le v2, v3, :cond_3

    :cond_2
    const/16 v2, 0xf8

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_4

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xff

    if-gt v2, v3, :cond_4

    .line 1500
    :cond_3
    const/4 v2, 0x1

    .line 1503
    :goto_1
    return v2

    .line 1495
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1503
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isKorean(Ljava/lang/String;)Z
    .locals 4
    .param p1, "word"    # Ljava/lang/String;

    .prologue
    .line 1519
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1521
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 1522
    const v2, 0xac00

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const v3, 0xd7a3

    if-le v2, v3, :cond_1

    :cond_0
    const/16 v2, 0x3131

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x318e

    if-gt v2, v3, :cond_2

    .line 1524
    :cond_1
    const/4 v2, 0x1

    .line 1527
    :goto_1
    return v2

    .line 1521
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1527
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isLatin(Ljava/lang/String;)Z
    .locals 4
    .param p1, "word"    # Ljava/lang/String;

    .prologue
    .line 1507
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1509
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 1510
    const/16 v2, 0x61

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x7a

    if-le v2, v3, :cond_1

    :cond_0
    const/16 v2, 0x41

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v2, v3, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5a

    if-gt v2, v3, :cond_2

    .line 1512
    :cond_1
    const/4 v2, 0x1

    .line 1515
    :goto_1
    return v2

    .line 1509
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1515
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private loadMeanInWebView(Landroid/webkit/WebView;ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;II)Z
    .locals 11
    .param p1, "webview"    # Landroid/webkit/WebView;
    .param p2, "dicType"    # I
    .param p3, "keyword"    # Ljava/lang/String;
    .param p4, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p5, "contentsType"    # I
    .param p6, "meanKinds"    # I

    .prologue
    .line 177
    const/4 v4, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move v3, p2

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getMeaning(I)Ljava/lang/CharSequence;

    move-result-object v9

    .line 179
    .local v9, "meaning":Ljava/lang/CharSequence;
    if-nez v9, :cond_0

    .line 180
    const/4 v0, 0x0

    .line 192
    .end local v9    # "meaning":Ljava/lang/CharSequence;
    :goto_0
    return v0

    .line 183
    .restart local v9    # "meaning":Ljava/lang/CharSequence;
    :cond_0
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "Success get meaning"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    new-instance v10, Landroid/os/Message;

    invoke-direct {v10}, Landroid/os/Message;-><init>()V

    .line 185
    .local v10, "msg":Landroid/os/Message;
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 186
    .local v8, "bundle":Landroid/os/Bundle;
    const-string/jumbo v0, "webview_navigate_message_key"

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-virtual {v10, v8}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 188
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mWebviewLoadDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 189
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 190
    const/4 v0, 0x1

    goto :goto_0

    .line 192
    .end local v8    # "bundle":Landroid/os/Bundle;
    .end local v9    # "meaning":Ljava/lang/CharSequence;
    .end local v10    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeDBHashMap()V
    .locals 9

    .prologue
    .line 1181
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 1182
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1185
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 1186
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    .line 1187
    .local v8, "size":I
    const-string/jumbo v1, "WrapperIDioDictService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v8, :cond_1

    .line 1189
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    .line 1190
    .local v7, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$DbInfo;

    const-string/jumbo v1, "language_code_source"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string/jumbo v1, "language_code_target"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string/jumbo v1, "db_name"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v1, "language_name"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$DbInfo;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;IILjava/lang/String;Ljava/lang/String;)V

    .line 1196
    .local v0, "info":Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$DbInfo;
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDBListMap:Ljava/util/HashMap;

    const-string/jumbo v1, "db_type"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1188
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1201
    .end local v0    # "info":Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$DbInfo;
    .end local v6    # "i":I
    .end local v7    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8    # "size":I
    :cond_1
    return-void
.end method

.method private printDBList()V
    .locals 11

    .prologue
    .line 1230
    iget-object v8, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v8, :cond_0

    .line 1231
    iget-object v8, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    .line 1232
    .local v5, "size":I
    const-string/jumbo v8, "WrapperIDioDictService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "size = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_0

    .line 1234
    iget-object v8, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 1235
    .local v4, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v8, "db_name"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1236
    .local v0, "dbName":Ljava/lang/String;
    const-string/jumbo v8, "language_name"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1237
    .local v3, "langName":Ljava/lang/String;
    const-string/jumbo v8, "db_type"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1238
    .local v1, "dbType":I
    const-string/jumbo v8, "language_code_source"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1239
    .local v6, "srcLang":I
    const-string/jumbo v8, "language_code_target"

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1241
    .local v7, "trgLang":I
    const-string/jumbo v8, "WrapperIDioDictService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "dbName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\nlangName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\ndbType="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\nsrcLang="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "\ntrgLang="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1246
    .end local v0    # "dbName":Ljava/lang/String;
    .end local v1    # "dbType":I
    .end local v2    # "i":I
    .end local v3    # "langName":Ljava/lang/String;
    .end local v4    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5    # "size":I
    .end local v6    # "srcLang":I
    .end local v7    # "trgLang":I
    :cond_0
    return-void
.end method

.method private printError(I)V
    .locals 3
    .param p1, "reqLastError"    # I

    .prologue
    .line 915
    sget-boolean v1, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 916
    const-string/jumbo v0, ""

    .line 917
    .local v0, "log":Ljava/lang/String;
    const/4 v1, -0x2

    if-ne p1, v1, :cond_1

    .line 918
    const-string/jumbo v0, "Service not initialization"

    .line 931
    :goto_0
    const-string/jumbo v1, "WrapperIDioDictService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    .end local v0    # "log":Ljava/lang/String;
    :cond_0
    return-void

    .line 919
    .restart local v0    # "log":Ljava/lang/String;
    :cond_1
    if-nez p1, :cond_2

    .line 920
    const-string/jumbo v0, "API Not-supported"

    goto :goto_0

    .line 921
    :cond_2
    const/4 v1, -0x1

    if-ne p1, v1, :cond_3

    .line 922
    const-string/jumbo v0, "INVALID_PARAMETER"

    goto :goto_0

    .line 923
    :cond_3
    const/4 v1, 0x1

    if-ne p1, v1, :cond_4

    .line 924
    const-string/jumbo v0, "Request FAILED"

    goto :goto_0

    .line 925
    :cond_4
    const/4 v1, 0x2

    if-ne p1, v1, :cond_5

    .line 926
    const-string/jumbo v0, "Request Succeed"

    goto :goto_0

    .line 928
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private printExceptionLog(Ljava/lang/Exception;)V
    .locals 4
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v3, 0x0

    .line 1551
    sget-boolean v0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1552
    const-string/jumbo v0, "WrapperIDioDictService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": Exception: e = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1554
    :cond_0
    invoke-virtual {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->unbindService()V

    .line 1555
    return-void
.end method

.method private recycleObject()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->recycle()V

    .line 293
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->recycle()V

    .line 297
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_2

    .line 300
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->recycle()V

    .line 301
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    if-eqz v0, :cond_3

    .line 304
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->recycle()V

    .line 305
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    .line 307
    :cond_3
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 308
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 309
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    .line 311
    :cond_4
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 312
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 313
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    .line 316
    :cond_5
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mStartCallbackHandler:Landroid/os/Handler;

    .line 317
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    .line 318
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    .line 319
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    .line 320
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    .line 321
    return-void
.end method

.method private recyleBitmap()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 338
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    .line 340
    :cond_0
    return-void
.end method

.method private reqMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIII)Z
    .locals 6
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "dicType"    # I
    .param p4, "type"    # I
    .param p5, "kinds"    # I
    .param p6, "resultType"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 700
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 701
    invoke-direct {p0, p3, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setMeanInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V

    .line 702
    if-eq p4, v4, :cond_0

    .line 703
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-virtual {v3, p4}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->setContainContent(I)V

    .line 704
    :cond_0
    if-eq p5, v4, :cond_1

    .line 705
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-virtual {v3, p5}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->setDisplayMode(I)V

    .line 707
    :cond_1
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-virtual {v3, p6}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->setResultType(I)V

    .line 709
    :try_start_0
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v5, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-interface {v3, v4, v5}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqMeaning(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExMeanInfo;)Lcom/diotek/diodict3/phone/service/ExMeanResult;

    move-result-object v3

    iput-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    .line 711
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    invoke-virtual {v3}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->getMeaning()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v3, :cond_3

    .line 735
    :cond_2
    :goto_0
    return v2

    .line 715
    :cond_3
    if-nez p6, :cond_4

    .line 716
    new-instance v0, Lcom/diotek/diodict/mean/TagConverter;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v0, v3, v4, v5}, Lcom/diotek/diodict/mean/TagConverter;-><init>(Landroid/content/Context;ILcom/diotek/diodict/mean/TagConverter$LoadListener;)V

    .line 717
    .local v0, "converter":Lcom/diotek/diodict/mean/TagConverter;
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    invoke-virtual {v3}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->getMeaning()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p3}, Lcom/diotek/diodict/mean/TagConverter;->loadMeaning(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 718
    invoke-virtual {v0}, Lcom/diotek/diodict/mean/TagConverter;->getMeanFieldSpan()Landroid/text/Spanned;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Spanned;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 719
    invoke-virtual {v0, p5}, Lcom/diotek/diodict/mean/TagConverter;->updateDispalyMode(I)Z

    .line 720
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mKeywordBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 721
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mKeywordBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/diotek/diodict/mean/TagConverter;->getKeyFieldSpan()Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 722
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mKeywordBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3, v4}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->setKeyword(Ljava/lang/CharSequence;)V

    .line 723
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContentsBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 724
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->addBoldKeywordForSamsungOCR()V

    .line 725
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContentsBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/diotek/diodict/mean/TagConverter;->getMeanFieldSpan()Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 726
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContentsBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3, v4}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->setMeaning(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    .end local v0    # "converter":Lcom/diotek/diodict/mean/TagConverter;
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 731
    :catch_0
    move-exception v1

    .line 732
    .local v1, "e":Ljava/lang/Exception;
    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private requestOpenApiLevel()V
    .locals 5

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 508
    :try_start_0
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqOpenApiLevel(Ljava/lang/String;)I

    move-result v2

    .line 509
    .local v2, "result":I
    sget-boolean v3, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 510
    const-string/jumbo v1, ""

    .line 511
    .local v1, "log":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 512
    const-string/jumbo v1, "API Not-supported"

    .line 519
    :goto_0
    const-string/jumbo v3, "WrapperIDioDictService"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    .end local v1    # "log":Ljava/lang/String;
    .end local v2    # "result":I
    :cond_0
    :goto_1
    return-void

    .line 514
    .restart local v1    # "log":Ljava/lang/String;
    .restart local v2    # "result":I
    :cond_1
    if-gez v2, :cond_2

    .line 515
    const-string/jumbo v1, "Error Return"

    goto :goto_0

    .line 517
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Open API Level :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 521
    .end local v1    # "log":Ljava/lang/String;
    .end local v2    # "result":I
    :catch_0
    move-exception v0

    .line 522
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1

    .line 525
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-boolean v3, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 526
    const-string/jumbo v3, "WrapperIDioDictService"

    const-string/jumbo v4, "Service not active"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private requestSearchWord(Ljava/lang/String;I)Lcom/diotek/diodict3/phone/service/ExSearchResult;
    .locals 4
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "dbtype"    # I

    .prologue
    .line 782
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 783
    invoke-direct {p0, p2, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->changeDicType(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 784
    iget v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    const/4 v2, -0x1

    invoke-direct {p0, v1, p1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setSearchInfo(ILjava/lang/String;I)V

    .line 786
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-interface {v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqSearchWord(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;)Lcom/diotek/diodict3/phone/service/ExSearchResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 792
    :goto_0
    return-object v1

    .line 788
    :catch_0
    move-exception v0

    .line 789
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 792
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setMeanInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V
    .locals 2
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 560
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    if-nez v0, :cond_0

    .line 561
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;-><init>(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    .line 565
    :goto_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    iget-boolean v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsImportantMeaning:Z

    invoke-virtual {v0, v1}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->setSummary(Z)V

    .line 566
    return-void

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanInfo:Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    invoke-virtual {v0, p1, p2, p3}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->setMeanInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V

    goto :goto_0
.end method

.method private setSearchInfo(ILjava/lang/String;I)V
    .locals 1
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "SUID"    # I

    .prologue
    .line 553
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    if-nez v0, :cond_0

    .line 554
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;-><init>(ILjava/lang/String;I)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 558
    :goto_0
    return-void

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-virtual {v0, p1, p2, p3}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->setSearchInfo(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private settingWebView()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 242
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    if-nez v2, :cond_1

    .line 243
    const-string/jumbo v1, "WrapperIDioDictService"

    const-string/jumbo v2, "webview is null!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v1, 0x0

    .line 287
    :cond_0
    :goto_0
    return v1

    .line 247
    :cond_1
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 248
    .local v0, "setting":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 249
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 250
    sget-object v2, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 252
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$2;

    invoke-direct {v3, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$2;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 264
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$3;

    invoke-direct {v3, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$3;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 283
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method


# virtual methods
.method public bindservice()Z
    .locals 5

    .prologue
    .line 408
    const/4 v1, 0x0

    .line 409
    .local v1, "isResult":Z
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.diotek.diodict3.service"

    invoke-direct {p0, v2, v3}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->chkPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 410
    const-string/jumbo v2, "WrapperIDioDictService"

    const-string/jumbo v3, "DioDict3 Service package not installed!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    const/4 v2, 0x0

    .line 429
    :goto_0
    return v2

    .line 413
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 414
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.diotek.diodict3.service"

    const-string/jumbo v3, "com.diotek.diodict3.phone.service.DioDictService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_1

    .line 418
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->m_connection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 421
    sget-boolean v2, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 422
    if-eqz v1, :cond_2

    .line 423
    const-string/jumbo v2, "WrapperIDioDictService"

    const-string/jumbo v3, "bind request success!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    move v2, v1

    .line 429
    goto :goto_0

    .line 425
    :cond_2
    const-string/jumbo v2, "WrapperIDioDictService"

    const-string/jumbo v3, "bind request failed!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public changeMeanType(Landroid/webkit/WebView;I)Z
    .locals 2
    .param p1, "webview"    # Landroid/webkit/WebView;
    .param p2, "meanKinds"    # I

    .prologue
    .line 204
    if-eqz p1, :cond_0

    if-ltz p2, :cond_0

    const/4 v1, 0x3

    if-ge v1, p2, :cond_1

    .line 205
    :cond_0
    const/4 v1, 0x0

    .line 227
    :goto_0
    return v1

    .line 208
    :cond_1
    const/4 v0, 0x0

    .line 210
    .local v0, "functionName":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 225
    :goto_1
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->invokeJavaScript(Ljava/lang/String;)V

    .line 227
    const/4 v1, 0x1

    goto :goto_0

    .line 212
    :pswitch_0
    const-string/jumbo v0, "addStyleForFullMeaning"

    .line 213
    goto :goto_1

    .line 215
    :pswitch_1
    const-string/jumbo v0, "addStyleForPureMeaning"

    .line 216
    goto :goto_1

    .line 218
    :pswitch_2
    const-string/jumbo v0, "addStyleForIdiom"

    .line 219
    goto :goto_1

    .line 221
    :pswitch_3
    const-string/jumbo v0, "addStyleForExample"

    goto :goto_1

    .line 210
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public enableDebugMessage(Z)V
    .locals 0
    .param p1, "isEnable"    # Z

    .prologue
    .line 467
    sput-boolean p1, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z

    .line 468
    return-void
.end method

.method public getAvailableDBLanguageName(I)Ljava/lang/String;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1330
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1331
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1333
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "language_name"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1339
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-object v1

    .line 1335
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 1339
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAvailableDBName(I)Ljava/lang/String;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1255
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1256
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1258
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "db_name"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1264
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-object v1

    .line 1260
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 1264
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAvailableDBSourceLanguage(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1272
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1273
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1275
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v1, "language_code_source"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1280
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return v1

    .line 1276
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v1

    .line 1280
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/16 v1, 0x2d

    goto :goto_0
.end method

.method public getAvailableDBTargetLanguage(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1288
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1289
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1291
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v1, "language_code_target"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1296
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return v1

    .line 1292
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v1

    .line 1296
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/16 v1, 0x2d

    goto :goto_0
.end method

.method public getAvailableDBType(II)I
    .locals 6
    .param p1, "source"    # I
    .param p2, "target"    # I

    .prologue
    .line 1305
    iget-object v5, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v5, :cond_1

    .line 1306
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_0
    iget-object v5, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 1307
    iget-object v5, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 1309
    .local v1, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v5, "language_code_source"

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1310
    .local v3, "source_lang":I
    const-string/jumbo v5, "language_code_target"

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1311
    .local v4, "target_lang":I
    const-string/jumbo v5, "db_type"

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1313
    .local v0, "dicType":I
    if-ne p1, v3, :cond_0

    if-ne p2, v4, :cond_0

    .line 1321
    .end local v0    # "dicType":I
    .end local v1    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "position":I
    .end local v3    # "source_lang":I
    .end local v4    # "target_lang":I
    :goto_1
    return v0

    .line 1316
    .restart local v1    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v2    # "position":I
    :catch_0
    move-exception v5

    .line 1306
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1321
    .end local v1    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "position":I
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getAvaliableDBType(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 1348
    const/4 v0, -0x1

    .line 1349
    .local v0, "dicType":I
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 1350
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 1352
    .local v2, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v3, "db_type"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    .line 1358
    .end local v0    # "dicType":I
    .end local v2    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .local v1, "dicType":I
    :goto_0
    return v1

    .line 1354
    .end local v1    # "dicType":I
    .restart local v0    # "dicType":I
    .restart local v2    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v3

    .end local v2    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    move v1, v0

    .line 1358
    .end local v0    # "dicType":I
    .restart local v1    # "dicType":I
    goto :goto_0
.end method

.method public getDBListName(Z)[Ljava/lang/CharSequence;
    .locals 7
    .param p1, "isContainLanguage"    # Z

    .prologue
    .line 1211
    iget-object v5, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v5, :cond_1

    .line 1212
    invoke-virtual {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getDBListSize()I

    move-result v4

    .line 1213
    .local v4, "size":I
    new-array v0, v4, [Ljava/lang/CharSequence;

    .line 1214
    .local v0, "DBName":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_2

    .line 1215
    iget-object v5, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 1216
    .local v3, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "db_name"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1217
    .local v1, "DbName":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    .line 1218
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v5, "language_name"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    .line 1214
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1225
    .end local v0    # "DBName":[Ljava/lang/CharSequence;
    .end local v1    # "DbName":Ljava/lang/StringBuilder;
    .end local v2    # "i":I
    .end local v3    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "size":I
    :cond_1
    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method

.method public getDBListSize()I
    .locals 1

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1205
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1207
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeaultDicType()I
    .locals 1

    .prologue
    .line 960
    iget v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    return v0
.end method

.method public getDicType()I
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getDicType()I

    move-result v0

    .line 612
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    goto :goto_0
.end method

.method public getDictionaryActivityName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1138
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1140
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqApplicationActivityName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1142
    .local v1, "result":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1143
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v2}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150
    .end local v1    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 1146
    :catch_0
    move-exception v0

    .line 1147
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 1150
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDictionaryPackageName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1119
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1121
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqApplicationPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1123
    .local v1, "result":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1124
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v2}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1131
    .end local v1    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 1127
    :catch_0
    move-exception v0

    .line 1128
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 1131
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEntryId(I)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 623
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getEntryIdList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    .line 626
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFontPath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 664
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 666
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    invoke-interface {v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqFontPath(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 671
    :goto_0
    return-object v1

    .line 667
    :catch_0
    move-exception v0

    .line 668
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 671
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFontPath(I)Ljava/lang/String;
    .locals 3
    .param p1, "dicType"    # I

    .prologue
    .line 674
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqFontPath(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 681
    :goto_0
    return-object v1

    .line 677
    :catch_0
    move-exception v0

    .line 678
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 681
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIntroduceDownloadMsg()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 1051
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1053
    :try_start_0
    invoke-virtual {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isPadDevice()Z

    move-result v1

    .line 1054
    .local v1, "isPad":Z
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqIntroduceDownloadMsg(Ljava/lang/String;Z)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1060
    .end local v1    # "isPad":Z
    :goto_0
    return-object v2

    .line 1055
    :catch_0
    move-exception v0

    .line 1056
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 1060
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getKeyword()Ljava/lang/String;

    move-result-object v0

    .line 598
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getKeyword(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 616
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getSearchWordList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 619
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getKeywordPosition()I
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getKeywordPos()I

    move-result v0

    .line 660
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getMeaning(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 754
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 755
    if-nez p1, :cond_0

    .line 756
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->getKeyword()Ljava/lang/CharSequence;

    move-result-object v1

    .line 764
    :goto_0
    return-object v1

    .line 758
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanResult:Lcom/diotek/diodict3/phone/service/ExMeanResult;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->getMeaning()Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 761
    :catch_0
    move-exception v0

    .line 762
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .line 764
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMeaning(Ljava/lang/String;IIIIIZ)Z
    .locals 8
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "dicType"    # I
    .param p4, "resultType"    # I
    .param p5, "contentsType"    # I
    .param p6, "meanKinds"    # I
    .param p7, "bShortMean"    # Z

    .prologue
    const/4 v7, 0x0

    .line 1027
    new-instance v2, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v0, 0x3

    new-array v0, v0, [I

    aput p2, v0, v7

    const/4 v1, 0x1

    aput v7, v0, v1

    const/4 v1, 0x2

    aput v7, v0, v1

    invoke-direct {v2, v0}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIIIZ)Z

    move-result v0

    return v0
.end method

.method public getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIII)Z
    .locals 8
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p3, "dicType"    # I
    .param p4, "mMeanType"    # I
    .param p5, "mContentsType"    # I
    .param p6, "meanKinds"    # I

    .prologue
    .line 1010
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getMeaning(Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;IIIIZ)Z

    move-result v0

    return v0
.end method

.method public getProviderItemBookmark(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 903
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 904
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 906
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "bookmark"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 912
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return v1

    .line 908
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 912
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProviderItemDicType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 852
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 853
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 855
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "DictType"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 861
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return v1

    .line 857
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 861
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getProviderItemEntryId(I)Lcom/diotek/diodict/core/engine/EntryId;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 878
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 879
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 881
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "suid"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 882
    .local v1, "result":I
    new-instance v2, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v3, 0x3

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v1, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput v5, v3, v4

    invoke-direct {v2, v3}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 887
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v1    # "result":I
    :goto_0
    return-object v2

    .line 883
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 887
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    new-instance v2, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-direct {v2}, Lcom/diotek/diodict/core/engine/EntryId;-><init>()V

    goto :goto_0
.end method

.method public getProviderItemKeyword(I)Ljava/lang/String;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 865
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 866
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 868
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "keyword"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 874
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-object v1

    .line 870
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 874
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProviderItemSuid(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 890
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 891
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 893
    .local v0, "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    const-string/jumbo v2, "suid"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 899
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return v1

    .line 895
    .restart local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 899
    .end local v0    # "nItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProviderTableItem()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    return-object v0
.end method

.method public getResultBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSuid()I
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getSuid()I

    move-result v0

    .line 605
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSuid(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 629
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getEntryIdList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    .line 632
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getWordList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 645
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getSearchWordList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 647
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExactMatch()Z
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->isExactMatch()Z

    move-result v0

    .line 641
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExistProviderItem()Z
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 846
    const/4 v0, 0x1

    .line 848
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isPadDevice()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1559
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xa

    if-gt v4, v5, :cond_1

    .line 1568
    :cond_0
    :goto_0
    return v3

    .line 1562
    :cond_1
    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    .line 1565
    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v4, v4, 0xf

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    move v1, v2

    .line 1566
    .local v1, "xlarge":Z
    :goto_1
    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v4, v4, 0xf

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    move v0, v2

    .line 1568
    .local v0, "large":Z
    :goto_2
    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    :cond_2
    move v3, v2

    goto :goto_0

    .end local v0    # "large":Z
    .end local v1    # "xlarge":Z
    :cond_3
    move v1, v3

    .line 1565
    goto :goto_1

    .restart local v1    # "xlarge":Z
    :cond_4
    move v0, v3

    .line 1566
    goto :goto_2
.end method

.method public isSameWord(III)Z
    .locals 10
    .param p1, "dicType"    # I
    .param p2, "pos1"    # I
    .param p3, "pos2"    # I

    .prologue
    const/4 v9, 0x0

    .line 1071
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v9

    .line 1098
    :goto_0
    return v0

    .line 1075
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-nez v0, :cond_1

    move v0, v9

    .line 1076
    goto :goto_0

    .line 1079
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getSearchWordList()Ljava/util/List;

    move-result-object v8

    .line 1080
    .local v8, "keywordList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getEntryIdList()Ljava/util/List;

    move-result-object v7

    .line 1082
    .local v7, "entryIdList":Ljava/util/List;, "Ljava/util/List<Lcom/diotek/diodict/core/engine/EntryId;>;"
    if-eqz v8, :cond_2

    if-nez v7, :cond_3

    :cond_2
    move v0, v9

    .line 1083
    goto :goto_0

    .line 1086
    :cond_3
    if-ltz p2, :cond_4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_4

    if-ltz p3, :cond_4

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_4

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_4

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-lt p3, v0, :cond_5

    :cond_4
    move v0, v9

    .line 1088
    goto :goto_0

    .line 1092
    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v8, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v7, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-interface {v8, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v7, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/diotek/diodict/core/engine/EntryId;

    move v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/diotek/diodict3/phone/service/IDioDictService;->isSameWord(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1094
    :catch_0
    move-exception v6

    .line 1095
    .local v6, "e":Ljava/lang/Exception;
    invoke-direct {p0, v6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    move v0, v9

    .line 1098
    goto :goto_0
.end method

.method public isServiceBinded()Z
    .locals 1

    .prologue
    .line 400
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->recyleBitmap()V

    .line 121
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->recycleObject()V

    .line 122
    return-void
.end method

.method public reqDBList()Z
    .locals 3

    .prologue
    .line 1159
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1161
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqAvailableDBList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    .line 1163
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mAvailableDBList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1166
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->makeDBHashMap()V

    .line 1167
    const/4 v1, 0x1

    .line 1177
    :goto_0
    return v1

    .line 1169
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1177
    :cond_1
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1172
    :catch_0
    move-exception v0

    .line 1173
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public requestMeanDialog(Ljava/lang/String;II)Z
    .locals 5
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "dicType"    # I

    .prologue
    .line 1451
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1452
    invoke-direct {p0, p3, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->changeDicType(ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 1453
    iget v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    invoke-direct {p0, v2, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setSearchInfo(ILjava/lang/String;I)V

    .line 1456
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-interface {v2, v3, v4}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqMeanDialog(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;)I

    move-result v1

    .line 1457
    .local v1, "error":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1458
    const/4 v2, 0x1

    .line 1467
    .end local v1    # "error":I
    :goto_0
    return v2

    .line 1460
    .restart local v1    # "error":I
    :cond_0
    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1467
    .end local v1    # "error":I
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1462
    :catch_0
    move-exception v0

    .line 1463
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public requestProvideDataOperation(I)Z
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 1425
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1428
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqProviderDataOperation(Ljava/lang/String;I)I

    move-result v1

    .line 1429
    .local v1, "error":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1430
    const/4 v2, 0x1

    .line 1439
    .end local v1    # "error":I
    :goto_0
    return v2

    .line 1432
    .restart local v1    # "error":I
    :cond_0
    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1439
    .end local v1    # "error":I
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1434
    :catch_0
    move-exception v0

    .line 1435
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public requestProvideItemOperation(Ljava/lang/String;IIII)Z
    .locals 5
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "dicType"    # I
    .param p4, "type"    # I
    .param p5, "kinds"    # I

    .prologue
    .line 1399
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1400
    invoke-direct {p0, p3, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->changeDicType(ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 1401
    iget v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    invoke-direct {p0, v2, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setSearchInfo(ILjava/lang/String;I)V

    .line 1404
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-interface {v2, v3, p4, p5, v4}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqProviderItemOperation(Ljava/lang/String;IILcom/diotek/diodict3/phone/service/ExSearchInfo;)I

    move-result v1

    .line 1405
    .local v1, "error":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1406
    const/4 v2, 0x1

    .line 1415
    .end local v1    # "error":I
    :goto_0
    return v2

    .line 1408
    .restart local v1    # "error":I
    :cond_0
    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1415
    .end local v1    # "error":I
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1410
    :catch_0
    move-exception v0

    .line 1411
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public requestProviderItem(ILcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;)Z
    .locals 6
    .param p1, "type"    # I
    .param p2, "callback"    # Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 823
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 825
    :try_start_0
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    const/4 v5, 0x3

    invoke-interface {v3, v4, p1, v5}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqProviderItem(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    .line 827
    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mProviderTableItem:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 828
    if-eqz p2, :cond_0

    .line 829
    const/4 v3, 0x1

    invoke-interface {p2, v3}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;->run(Z)V

    .line 841
    :cond_0
    :goto_0
    return v1

    .line 833
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 834
    goto :goto_0

    .line 836
    :catch_0
    move-exception v0

    .line 837
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    move v1, v2

    .line 841
    goto :goto_0
.end method

.method public requestSpeech(Ljava/lang/String;III)Z
    .locals 5
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "suid"    # I
    .param p3, "dicType"    # I
    .param p4, "type"    # I

    .prologue
    .line 1370
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1371
    invoke-direct {p0, p3, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->changeDicType(ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 1372
    iget v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    invoke-direct {p0, v2, p1, p2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setSearchInfo(ILjava/lang/String;I)V

    .line 1375
    :try_start_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v4, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-interface {v2, v3, v4, p4}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqSpeech(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;I)I

    move-result v1

    .line 1376
    .local v1, "error":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1377
    const/4 v2, 0x1

    .line 1386
    .end local v1    # "error":I
    :goto_0
    return v2

    .line 1379
    .restart local v1    # "error":I
    :cond_0
    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1386
    .end local v1    # "error":I
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1381
    :catch_0
    move-exception v0

    .line 1382
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public searchWord(Ljava/lang/String;II)Z
    .locals 4
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "dicType"    # I
    .param p3, "searchType"    # I

    .prologue
    .line 971
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->isActiveService()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 972
    invoke-direct {p0, p2, p1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->changeDicType(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 973
    iget v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    const/4 v2, -0x1

    invoke-direct {p0, v1, p1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->setSearchInfo(ILjava/lang/String;I)V

    .line 974
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->getSearchType()I

    move-result v1

    if-eq v1, p3, :cond_0

    .line 975
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-virtual {v1, p3}, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->setSearchType(I)V

    .line 978
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSerial:Ljava/lang/String;

    iget-object v3, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchInfo:Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    invoke-interface {v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqSearchWord(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;)Lcom/diotek/diodict3/phone/service/ExSearchResult;

    move-result-object v1

    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    .line 980
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    if-eqz v1, :cond_1

    .line 981
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mSearchResult:Lcom/diotek/diodict3/phone/service/ExSearchResult;

    invoke-virtual {v1}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->getDicType()I

    move-result v1

    iput v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mDicType:I

    .line 982
    const/4 v1, 0x1

    .line 993
    :goto_0
    return v1

    .line 985
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;

    invoke-interface {v1}, Lcom/diotek/diodict3/phone/service/IDioDictService;->reqLastError()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printError(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 993
    :cond_2
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 987
    :catch_0
    move-exception v0

    .line 988
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->printExceptionLog(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public setBitmapConfiguration(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 539
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 540
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 545
    :cond_2
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->recyleBitmap()V

    .line 546
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setImportantMeaning(Z)V
    .locals 0
    .param p1, "isImportant"    # Z

    .prologue
    .line 572
    iput-boolean p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsImportantMeaning:Z

    .line 573
    return-void
.end method

.method public unbindService()V
    .locals 3

    .prologue
    .line 446
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 447
    iget-boolean v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z

    if-eqz v1, :cond_0

    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->m_connection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 458
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z

    .line 461
    :cond_1
    return-void

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 454
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public updateMeaing(Landroid/webkit/WebView;ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;II)Z
    .locals 1
    .param p1, "webview"    # Landroid/webkit/WebView;
    .param p2, "dicType"    # I
    .param p3, "keyword"    # Ljava/lang/String;
    .param p4, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;
    .param p5, "contentsType"    # I
    .param p6, "meanKinds"    # I

    .prologue
    .line 138
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    .line 140
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->settingWebView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 144
    :goto_0
    return v0

    :cond_0
    invoke-direct/range {p0 .. p6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->loadMeanInWebView(Landroid/webkit/WebView;ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;II)Z

    move-result v0

    goto :goto_0
.end method

.method public updateMeaningForWordbank(Landroid/webkit/WebView;Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSupportLanguage;Lcom/diotek/diodict3/phone/service/ExSupportLanguage;)Z
    .locals 7
    .param p1, "webview"    # Landroid/webkit/WebView;
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "sourceLang"    # Lcom/diotek/diodict3/phone/service/ExSupportLanguage;
    .param p4, "targetLang"    # Lcom/diotek/diodict3/phone/service/ExSupportLanguage;

    .prologue
    const/4 v5, 0x0

    .line 159
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;

    .line 161
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->settingWebView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    :goto_0
    return v5

    .line 165
    :cond_0
    if-eqz p3, :cond_1

    if-nez p4, :cond_2

    .line 166
    :cond_1
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "Argument is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 170
    :cond_2
    new-instance v4, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v0, 0x3

    new-array v0, v0, [I

    invoke-virtual {p3}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v1

    aput v1, v0, v5

    const/4 v1, 0x1

    invoke-virtual {p4}, Lcom/diotek/diodict3/phone/service/ExSupportLanguage;->getCode()I

    move-result v3

    aput v3, v0, v1

    const/4 v1, 0x2

    aput v5, v0, v1

    invoke-direct {v4, v0}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    .line 171
    .local v4, "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    const/16 v2, -0x3e8

    .line 173
    .local v2, "dicType":I
    const/16 v6, 0xf

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->loadMeanInWebView(Landroid/webkit/WebView;ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;II)Z

    move-result v5

    goto :goto_0
.end method
