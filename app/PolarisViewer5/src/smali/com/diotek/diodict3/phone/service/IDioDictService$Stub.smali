.class public abstract Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;
.super Landroid/os/Binder;
.source "IDioDictService.java"

# interfaces
.implements Lcom/diotek/diodict3/phone/service/IDioDictService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict3/phone/service/IDioDictService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict3/phone/service/IDioDictService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.diotek.diodict3.phone.service.IDioDictService"

.field static final TRANSACTION_isSameWord:I = 0x11

.field static final TRANSACTION_reqApiVersion:I = 0x1

.field static final TRANSACTION_reqApplicationActivityName:I = 0xd

.field static final TRANSACTION_reqApplicationPackageName:I = 0xc

.field static final TRANSACTION_reqAvailableDBList:I = 0x9

.field static final TRANSACTION_reqChangeDicType:I = 0x3

.field static final TRANSACTION_reqDBType:I = 0xa

.field static final TRANSACTION_reqFontPath:I = 0x7

.field static final TRANSACTION_reqIntroduceDownloadMsg:I = 0xf

.field static final TRANSACTION_reqIsDBEnable:I = 0x6

.field static final TRANSACTION_reqLastError:I = 0x8

.field static final TRANSACTION_reqLaunchDictionary:I = 0xe

.field static final TRANSACTION_reqMeanDialog:I = 0x15

.field static final TRANSACTION_reqMeaning:I = 0x5

.field static final TRANSACTION_reqOpenApiLevel:I = 0x2

.field static final TRANSACTION_reqProviderDataOperation:I = 0x14

.field static final TRANSACTION_reqProviderItem:I = 0xb

.field static final TRANSACTION_reqProviderItemOperation:I = 0x13

.field static final TRANSACTION_reqSearchWord:I = 0x4

.field static final TRANSACTION_reqSpeech:I = 0x12

.field static final TRANSACTION_setScanFinishedCallback:I = 0x10


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 48
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p0, p0, v0}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/diotek/diodict3/phone/service/IDioDictService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 56
    if-nez p0, :cond_0

    .line 57
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0

    .line 59
    :cond_0
    const-string/jumbo v1, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 60
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/diotek/diodict3/phone/service/IDioDictService;

    if-eqz v1, :cond_1

    .line 61
    check-cast v0, Lcom/diotek/diodict3/phone/service/IDioDictService;

    goto :goto_0

    .line 63
    :cond_1
    new-instance v0, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 67
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 71
    sparse-switch p1, :sswitch_data_0

    .line 386
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 75
    :sswitch_0
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :sswitch_1
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqApiVersion()Ljava/lang/String;

    move-result-object v6

    .line 82
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 83
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 88
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_2
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqOpenApiLevel(Ljava/lang/String;)I

    move-result v6

    .line 92
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 93
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 98
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":I
    :sswitch_3
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 102
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 103
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqChangeDicType(Ljava/lang/String;I)I

    move-result v6

    .line 104
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 105
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 110
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v6    # "_result":I
    :sswitch_4
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 114
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 120
    .local v2, "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqSearchWord(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;)Lcom/diotek/diodict3/phone/service/ExSearchResult;

    move-result-object v6

    .line 121
    .local v6, "_result":Lcom/diotek/diodict3/phone/service/ExSearchResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    if-eqz v6, :cond_1

    .line 123
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    invoke-virtual {v6, p3, v7}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 118
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    .end local v6    # "_result":Lcom/diotek/diodict3/phone/service/ExSearchResult;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    goto :goto_1

    .line 127
    .restart local v6    # "_result":Lcom/diotek/diodict3/phone/service/ExSearchResult;
    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 133
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    .end local v6    # "_result":Lcom/diotek/diodict3/phone/service/ExSearchResult;
    :sswitch_5
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 137
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    sget-object v0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict3/phone/service/ExMeanInfo;

    .line 143
    .local v2, "_arg1":Lcom/diotek/diodict3/phone/service/ExMeanInfo;
    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqMeaning(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExMeanInfo;)Lcom/diotek/diodict3/phone/service/ExMeanResult;

    move-result-object v6

    .line 144
    .local v6, "_result":Lcom/diotek/diodict3/phone/service/ExMeanResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 145
    if-eqz v6, :cond_3

    .line 146
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 147
    invoke-virtual {v6, p3, v7}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 141
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExMeanInfo;
    .end local v6    # "_result":Lcom/diotek/diodict3/phone/service/ExMeanResult;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExMeanInfo;
    goto :goto_2

    .line 150
    .restart local v6    # "_result":Lcom/diotek/diodict3/phone/service/ExMeanResult;
    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 156
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExMeanInfo;
    .end local v6    # "_result":Lcom/diotek/diodict3/phone/service/ExMeanResult;
    :sswitch_6
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 160
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 161
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqIsDBEnable(Ljava/lang/String;I)I

    move-result v6

    .line 162
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 163
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 168
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v6    # "_result":I
    :sswitch_7
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 172
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 173
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqFontPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 175
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 180
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_8
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqLastError()I

    move-result v6

    .line 182
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 183
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 188
    .end local v6    # "_result":I
    :sswitch_9
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 191
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqAvailableDBList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 192
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 193
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 198
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Ljava/util/List;
    :sswitch_a
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 202
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 204
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 205
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqDBType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 206
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 207
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 212
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":I
    :sswitch_b
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 216
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 218
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 219
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqProviderItem(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v6

    .line 220
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 221
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 226
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Ljava/util/List;
    :sswitch_c
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 229
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqApplicationPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 230
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 236
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_d
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 239
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqApplicationActivityName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 240
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 241
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 246
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_e
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 250
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 251
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 256
    .local v2, "_arg1":Landroid/os/Bundle;
    :goto_3
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqLaunchDictionary(Ljava/lang/String;Landroid/os/Bundle;)I

    move-result v6

    .line 257
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 258
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 254
    .end local v2    # "_arg1":Landroid/os/Bundle;
    .end local v6    # "_result":I
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/os/Bundle;
    goto :goto_3

    .line 263
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Landroid/os/Bundle;
    :sswitch_f
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 267
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v2, v7

    .line 268
    .local v2, "_arg1":Z
    :goto_4
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqIntroduceDownloadMsg(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v6

    .line 269
    .local v6, "_result":Ljava/lang/CharSequence;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 270
    if-eqz v6, :cond_6

    .line 271
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 272
    invoke-static {v6, p3, v7}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Ljava/lang/CharSequence;
    :cond_5
    move v2, v8

    .line 267
    goto :goto_4

    .line 275
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Ljava/lang/CharSequence;
    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 281
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Ljava/lang/CharSequence;
    :sswitch_10
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 283
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    move-result-object v1

    .line 284
    .local v1, "_arg0":Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    invoke-virtual {p0, v1}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->setScanFinishedCallback(Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;)V

    .line 285
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 290
    .end local v1    # "_arg0":Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    :sswitch_11
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 292
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 294
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 297
    sget-object v0, Lcom/diotek/diodict/core/engine/EntryId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/diotek/diodict/core/engine/EntryId;

    .line 303
    .local v3, "_arg2":Lcom/diotek/diodict/core/engine/EntryId;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 305
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 306
    sget-object v0, Lcom/diotek/diodict/core/engine/EntryId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/diotek/diodict/core/engine/EntryId;

    .local v5, "_arg4":Lcom/diotek/diodict/core/engine/EntryId;
    :goto_6
    move-object v0, p0

    .line 311
    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->isSameWord(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;Ljava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)Z

    move-result v6

    .line 312
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313
    if-eqz v6, :cond_7

    move v8, v7

    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 300
    .end local v3    # "_arg2":Lcom/diotek/diodict/core/engine/EntryId;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Lcom/diotek/diodict/core/engine/EntryId;
    .end local v6    # "_result":Z
    :cond_8
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/diotek/diodict/core/engine/EntryId;
    goto :goto_5

    .line 309
    .restart local v4    # "_arg3":Ljava/lang/String;
    :cond_9
    const/4 v5, 0x0

    .restart local v5    # "_arg4":Lcom/diotek/diodict/core/engine/EntryId;
    goto :goto_6

    .line 318
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/diotek/diodict/core/engine/EntryId;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Lcom/diotek/diodict/core/engine/EntryId;
    :sswitch_12
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 322
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 323
    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 329
    .local v2, "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 330
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqSpeech(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;I)I

    move-result v6

    .line 331
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 332
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 326
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    .end local v3    # "_arg2":I
    .end local v6    # "_result":I
    :cond_a
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    goto :goto_7

    .line 337
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    :sswitch_13
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 341
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 343
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 345
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 346
    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 351
    .local v4, "_arg3":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    :goto_8
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqProviderItemOperation(Ljava/lang/String;IILcom/diotek/diodict3/phone/service/ExSearchInfo;)I

    move-result v6

    .line 352
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 353
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 349
    .end local v4    # "_arg3":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    .end local v6    # "_result":I
    :cond_b
    const/4 v4, 0x0

    .restart local v4    # "_arg3":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    goto :goto_8

    .line 358
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    :sswitch_14
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 362
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 363
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqProviderDataOperation(Ljava/lang/String;I)I

    move-result v6

    .line 364
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 365
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 370
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v6    # "_result":I
    :sswitch_15
    const-string/jumbo v0, "com.diotek.diodict3.phone.service.IDioDictService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 374
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 375
    sget-object v0, Lcom/diotek/diodict3/phone/service/ExSearchInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict3/phone/service/ExSearchInfo;

    .line 380
    .local v2, "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    :goto_9
    invoke-virtual {p0, v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->reqMeanDialog(Ljava/lang/String;Lcom/diotek/diodict3/phone/service/ExSearchInfo;)I

    move-result v6

    .line 381
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 382
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 378
    .end local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    .end local v6    # "_result":I
    :cond_c
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/diotek/diodict3/phone/service/ExSearchInfo;
    goto :goto_9

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
