.class public Lcom/diotek/diodict3/phone/service/ExMeanInfo;
.super Ljava/lang/Object;
.source "ExMeanInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CONTAIN_KEYWORD_MEANING:I = 0x0

.field public static final CONTAIN_KEYWORD_ONLY:I = 0x1

.field public static final CONTAIN_MEANING_ONLY:I = 0x2

.field public static final CONTAIN_SHORT_MEANING:I = 0x3

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/diotek/diodict3/phone/service/ExMeanInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DSP_ETC:I = 0x8

.field public static final DSP_EXAMPLE:I = 0x4

.field public static final DSP_FREQUENCY:I = 0x40

.field public static final DSP_IDIOM:I = 0x2

.field public static final DSP_KEYWORD:I = 0x10

.field public static final DSP_KEYWORD_ALL:I = 0x70

.field public static final DSP_MEANING:I = 0x1

.field public static final DSP_MEANING_ALL:I = 0xf

.field public static final DSP_PRONOUNCE:I = 0x20

.field public static final RET_TYPE_STRING:I = 0x0

.field public static final RET_TYPE_WEBVIEW:I = 0x1


# instance fields
.field private mContainContent:I

.field private mDicType:I

.field private mDispalyMode:I

.field private mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

.field private mKeyword:Ljava/lang/String;

.field private mResultType:I

.field private mSummaryMeaning:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExMeanInfo$1;

    invoke-direct {v0}, Lcom/diotek/diodict3/phone/service/ExMeanInfo$1;-><init>()V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    .line 134
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 137
    const/4 v0, 0x1

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mResultType:I

    .line 138
    const/16 v0, 0xf

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDispalyMode:I

    .line 139
    iput v1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mContainContent:I

    .line 140
    iput v1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mSummaryMeaning:I

    .line 142
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .locals 4
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "suid"    # I

    .prologue
    const/4 v3, 0x0

    .line 144
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;-><init>()V

    .line 145
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    .line 146
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 147
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v1, 0x3

    new-array v1, v1, [I

    aput p3, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    const/4 v2, 0x2

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 148
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V
    .locals 0
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;-><init>()V

    .line 152
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    .line 153
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 154
    iput-object p3, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 155
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 279
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/diotek/diodict3/phone/service/ExMeanInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/diotek/diodict3/phone/service/ExMeanInfo$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExMeanInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 109
    const-class v0, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mResultType:I

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDispalyMode:I

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mContainContent:I

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mSummaryMeaning:I

    .line 115
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public getContainContent()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mContainContent:I

    return v0
.end method

.method public getDicType()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    return v0
.end method

.method public getDisplayMode()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDispalyMode:I

    return v0
.end method

.method public getEntryId()Lcom/diotek/diodict/core/engine/EntryId;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    return-object v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getResultType()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mResultType:I

    return v0
.end method

.method public getSuid()I
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    .line 239
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSummary()Z
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mSummaryMeaning:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 282
    return-void
.end method

.method public setContainContent(I)V
    .locals 0
    .param p1, "content"    # I

    .prologue
    .line 203
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mContainContent:I

    .line 204
    return-void
.end method

.method public setDisplayMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDispalyMode:I

    .line 194
    return-void
.end method

.method public setMeanInfo(ILjava/lang/String;I)V
    .locals 4
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "suid"    # I

    .prologue
    const/4 v3, 0x0

    .line 164
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    .line 165
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 166
    new-instance v0, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v1, 0x3

    new-array v1, v1, [I

    aput p3, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    const/4 v2, 0x2

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 167
    return-void
.end method

.method public setMeanInfo(ILjava/lang/String;Lcom/diotek/diodict/core/engine/EntryId;)V
    .locals 0
    .param p1, "dicType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "entryId"    # Lcom/diotek/diodict/core/engine/EntryId;

    .prologue
    .line 175
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    .line 176
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    .line 177
    iput-object p3, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    .line 178
    return-void
.end method

.method public setResultType(I)V
    .locals 0
    .param p1, "resultType"    # I

    .prologue
    .line 215
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mResultType:I

    .line 216
    return-void
.end method

.method public setSummary(Z)V
    .locals 1
    .param p1, "isSummary"    # Z

    .prologue
    .line 211
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mSummaryMeaning:I

    .line 212
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 94
    if-eqz p1, :cond_0

    .line 95
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDicType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mKeyword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mEntryId:Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 98
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mResultType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mDispalyMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mContainContent:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanInfo;->mSummaryMeaning:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    :cond_0
    return-void
.end method
