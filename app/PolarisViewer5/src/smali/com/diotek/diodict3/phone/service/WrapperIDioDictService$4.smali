.class Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;
.super Ljava/lang/Object;
.source "WrapperIDioDictService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;


# direct methods
.method constructor <init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 343
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-static {p2}, Lcom/diotek/diodict3/phone/service/IDioDictService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/diotek/diodict3/phone/service/IDioDictService;

    move-result-object v2

    # setter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;
    invoke-static {v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$202(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Lcom/diotek/diodict3/phone/service/IDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictService;

    .line 344
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;
    invoke-static {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$200(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictService;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 345
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    const/4 v2, 0x1

    # setter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z
    invoke-static {v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$302(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Z)Z

    .line 346
    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z
    invoke-static {}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 347
    const-string/jumbo v1, "WrapperIDioDictService"

    const-string/jumbo v2, "Service Connected!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # invokes: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->getApiVersion()V
    invoke-static {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$400(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V

    .line 352
    :try_start_0
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    invoke-static {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$500(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    move-result-object v1

    if-nez v1, :cond_1

    .line 353
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    new-instance v2, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4$1;

    invoke-direct {v2, p0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4$1;-><init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;)V

    # setter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    invoke-static {v1, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$502(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;)Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mService:Lcom/diotek/diodict3/phone/service/IDioDictService;
    invoke-static {v1}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$200(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictService;

    move-result-object v1

    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mServiceCallback:Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;
    invoke-static {v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$500(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/diotek/diodict3/phone/service/IDioDictService;->setScanFinishedCallback(Lcom/diotek/diodict3/phone/service/IDioDictServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :cond_2
    :goto_0
    return-void

    .line 378
    :catch_0
    move-exception v0

    .line 380
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v2, 0x0

    .line 386
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # setter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mIsServiceBinded:Z
    invoke-static {v0, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$302(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;Z)Z

    .line 387
    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->DEBUG:Z
    invoke-static {}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$100()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    const-string/jumbo v0, "WrapperIDioDictService"

    const-string/jumbo v1, "Service disconnected!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;
    invoke-static {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$000(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$4;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mCallback:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;
    invoke-static {v0}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$000(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$RunnableCallback;->run(Z)V

    .line 393
    :cond_1
    return-void
.end method
