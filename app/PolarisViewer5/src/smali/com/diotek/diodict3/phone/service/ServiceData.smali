.class public Lcom/diotek/diodict3/phone/service/ServiceData;
.super Ljava/lang/Object;
.source "ServiceData.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final ACTIVITY_CLASS:Ljava/lang/String; = "com.diotek.diodict3.phone.DioAuthActivity"

.field public static final ACTIVITY_PACKAGE:Ljava/lang/String; = "com.diotek.diodict3.phone.chungdam"

.field public static final BITMAP_MODE_KEYWORD_MEANING:I = 0x0

.field public static final BITMAP_MODE_KEYWORD_ONLY:I = 0x1

.field public static final BITMAP_MODE_MEANING_ONLY:I = 0x2

.field public static final DICTYPE_ENGKOR:I = 0xb04

.field public static final DICTYPE_KORENG:I = 0xb03

.field public static final DIOTEK_INTENT_ACTION_HISTORY_UPDATE:Ljava/lang/String; = "com.diotek.intent.action.HISTORY_UPDATE"

.field public static final DIOTEK_INTENT_ACTION_WIDGET_UPDATE:Ljava/lang/String; = "com.diotek.intent.action.WIDGET_UPDATE"

.field public static final INTENT_EXTRA_DICTIONARY_NAME:Ljava/lang/String; = "wordinfo_dict_type"

.field public static final INTENT_EXTRA_SEARCHTYPE:Ljava/lang/String; = "wordinfo_searchtype"

.field public static final INTENT_EXTRA_WORD_NAME:Ljava/lang/String; = "wordinfo_keyword"

.field public static final INTENT_EXTRA_WORD_SUID:Ljava/lang/String; = "wordinfo_suid"

.field public static final INTENT_MODE_NAME:Ljava/lang/String; = "display_mode"

.field public static final INTENT_MODE_VALUE:Ljava/lang/String; = "display_mode_view"

.field public static final ListItem_DictType:Ljava/lang/String; = "DictType"

.field public static final ListItem_Keyword:Ljava/lang/String; = "keyword"

.field public static final ListItem_bookmark:Ljava/lang/String; = "bookmark"

.field public static final ListItem_suid:Ljava/lang/String; = "suid"

.field public static final REQUESTMODE_BITMAP:I = 0x1

.field public static final REQUESTMODE_STRING:I = 0x0

.field public static final REQUESTMODE_WORDLIST:I = 0x2

.field public static final SEARCHTYPE_EXAM:I = 0x4

.field public static final SEARCHTYPE_HANGULRO:I = 0x8

.field public static final SEARCHTYPE_IDIOM:I = 0x2

.field public static final SEARCHTYPE_VOICE:I = 0x111

.field public static final SEARCHTYPE_WORD:I = 0x1

.field public static final SERVICE_CLASS:Ljava/lang/String; = "com.diotek.diodict3.phone.service.DioDictService"

.field public static final SERVICE_PACKAGE:Ljava/lang/String; = "com.diotek.diodict3.service"

.field public static final SORT_ORDER_TIME_ASC:I = 0x2

.field public static final SORT_ORDER_TIME_DSC:I = 0x3

.field public static final SORT_ORDER_WORD_ASC:I = 0x0

.field public static final SORT_ORDER_WORD_DSC:I = 0x1

.field public static final advTheme:[I

.field public static final baseTheme:[I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/16 v8, 0xd7

    const/16 v7, 0xa3

    const/16 v6, 0xff

    const/4 v5, 0x0

    .line 52
    const/16 v0, 0x9

    new-array v0, v0, [I

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/4 v1, 0x2

    const/16 v2, 0xb7

    const/16 v3, 0xd5

    const/16 v4, 0x30

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x7b

    invoke-static {v6, v2, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/diotek/diodict3/phone/service/ServiceData;->baseTheme:[I

    .line 63
    const/16 v0, 0xd

    new-array v0, v0, [I

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/4 v1, 0x2

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    invoke-static {v6, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xb7

    const/16 v3, 0xd5

    const/16 v4, 0x30

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/diotek/diodict3/phone/service/ServiceData;->advTheme:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
