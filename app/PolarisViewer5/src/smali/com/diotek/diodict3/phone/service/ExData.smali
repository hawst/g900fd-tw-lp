.class public Lcom/diotek/diodict3/phone/service/ExData;
.super Ljava/lang/Object;
.source "ExData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict3/phone/service/ExData$LANGUAGE;,
        Lcom/diotek/diodict3/phone/service/ExData$DictInfo;,
        Lcom/diotek/diodict3/phone/service/ExData$MeaningType;,
        Lcom/diotek/diodict3/phone/service/ExData$AppLaunchIntent;,
        Lcom/diotek/diodict3/phone/service/ExData$AppLaunchMode;,
        Lcom/diotek/diodict3/phone/service/ExData$ServiceInfo;,
        Lcom/diotek/diodict3/phone/service/ExData$BroadCast;,
        Lcom/diotek/diodict3/phone/service/ExData$Err;,
        Lcom/diotek/diodict3/phone/service/ExData$SpeechType;,
        Lcom/diotek/diodict3/phone/service/ExData$ProviderDataOp;,
        Lcom/diotek/diodict3/phone/service/ExData$ProviderItemOp;,
        Lcom/diotek/diodict3/phone/service/ExData$ProviderItem;,
        Lcom/diotek/diodict3/phone/service/ExData$ProviderSort;,
        Lcom/diotek/diodict3/phone/service/ExData$ProviderType;
    }
.end annotation


# static fields
.field public static final DICTYPE_ENGKOR:I = 0xb04

.field public static final DICTYPE_KORENG:I = 0xb03

.field public static final advTheme:[I

.field public static final baseTheme:[I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/16 v8, 0xd7

    const/16 v7, 0xa3

    const/16 v6, 0xff

    const/4 v5, 0x0

    .line 230
    const/16 v0, 0x9

    new-array v0, v0, [I

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/4 v1, 0x2

    const/16 v2, 0xb7

    const/16 v3, 0xd5

    const/16 v4, 0x30

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x7b

    invoke-static {v6, v2, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExData;->baseTheme:[I

    .line 244
    const/16 v0, 0xd

    new-array v0, v0, [I

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/4 v1, 0x2

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    invoke-static {v8, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    invoke-static {v6, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xb7

    const/16 v3, 0xd5

    const/16 v4, 0x30

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExData;->advTheme:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
