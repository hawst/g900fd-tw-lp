.class public final Lcom/diotek/diodict3/phone/service/ExSearchResult;
.super Ljava/lang/Object;
.source "ExSearchResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/diotek/diodict3/phone/service/ExSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final DIODICT_DB_VERSION_3:I = 0x3

.field public static final DIODICT_DB_VERSION_4:I = 0x4


# instance fields
.field private mDBVersion:I

.field private mDicType:I

.field private mEntryIdList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/diotek/diodict/core/engine/EntryId;",
            ">;"
        }
    .end annotation
.end field

.field private mEquivList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKeywordList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKeywordPos:I

.field private mbExactMatch:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExSearchResult$1;

    invoke-direct {v0}, Lcom/diotek/diodict3/phone/service/ExSearchResult$1;-><init>()V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    .line 65
    iput v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDicType:I

    .line 66
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    .line 67
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    .line 68
    iput-object v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    .line 69
    iput v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mbExactMatch:I

    .line 71
    const/4 v0, 0x3

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDBVersion:I

    .line 72
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "dictype"    # I

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    .line 74
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDicType:I

    .line 75
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    .line 77
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExSearchResult;->readFromParcel(Landroid/os/Parcel;)V

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/diotek/diodict3/phone/service/ExSearchResult$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/diotek/diodict3/phone/service/ExSearchResult$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExSearchResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDicType:I

    .line 235
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 236
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 237
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    sget-object v1, Lcom/diotek/diodict/core/engine/EntryId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mbExactMatch:I

    .line 240
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public getDBVersion()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDBVersion:I

    return v0
.end method

.method public getDicType()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDicType:I

    return v0
.end method

.method public getEntryId()Lcom/diotek/diodict/core/engine/EntryId;
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    iget v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    return-object v0
.end method

.method public getEntryIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/diotek/diodict/core/engine/EntryId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    return-object v0
.end method

.method public getEquivList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    return-object v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    iget v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getKeywordPos()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    return v0
.end method

.method public getSearchWordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    return-object v0
.end method

.method public getSuid()I
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    iget v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v0}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v0

    return v0
.end method

.method public getSuidList()[I
    .locals 3

    .prologue
    .line 204
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [I

    .line 206
    .local v1, "suidList":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 207
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/diotek/diodict/core/engine/EntryId;

    invoke-virtual {v2}, Lcom/diotek/diodict/core/engine/EntryId;->getEntryNum()I

    move-result v2

    aput v2, v1, v0

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_0
    return-object v1
.end method

.method public isExactMatch()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 183
    iget v1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mbExactMatch:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 243
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 244
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 245
    return-void
.end method

.method public setEntryIdList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/diotek/diodict/core/engine/EntryId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "entryIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/diotek/diodict/core/engine/EntryId;>;"
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    if-nez v2, :cond_0

    .line 119
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    .line 122
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 123
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/diotek/diodict/core/engine/EntryId;

    .line 124
    .local v0, "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    .end local v0    # "entryId":Lcom/diotek/diodict/core/engine/EntryId;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public setEquivList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "equivList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    .line 94
    return-void
.end method

.method public setExactMatch(Z)V
    .locals 1
    .param p1, "isMatch"    # Z

    .prologue
    .line 148
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mbExactMatch:I

    .line 149
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setKeywordPos(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    .line 142
    return-void
.end method

.method public setSearchInfo(II)V
    .locals 0
    .param p1, "dicType"    # I
    .param p2, "dbVersion"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDicType:I

    .line 86
    iput p2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDBVersion:I

    .line 87
    return-void
.end method

.method public setSearchWordList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "wordList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    .line 134
    return-void
.end method

.method public setSuidList(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "suidList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 100
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    if-nez v2, :cond_0

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    .line 104
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 105
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 106
    .local v1, "suid":I
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    new-instance v3, Lcom/diotek/diodict/core/engine/EntryId;

    const/4 v4, 0x3

    new-array v4, v4, [I

    aput v1, v4, v6

    const/4 v5, 0x1

    aput v6, v4, v5

    const/4 v5, 0x2

    aput v6, v4, v5

    invoke-direct {v3, v4}, Lcom/diotek/diodict/core/engine/EntryId;-><init>([I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "suid":I
    :cond_1
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 40
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDicType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 41
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 42
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEquivList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 43
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mEntryIdList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 44
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mKeywordPos:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mbExactMatch:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 46
    iget v0, p0, Lcom/diotek/diodict3/phone/service/ExSearchResult;->mDBVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    return-void
.end method
