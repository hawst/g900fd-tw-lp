.class public final Lcom/diotek/diodict3/phone/service/ExMeanResult;
.super Ljava/lang/Object;
.source "ExMeanResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/diotek/diodict3/phone/service/ExMeanResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mKeyword:Ljava/lang/CharSequence;

.field private mMeaning:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/diotek/diodict3/phone/service/ExMeanResult$1;

    invoke-direct {v0}, Lcom/diotek/diodict3/phone/service/ExMeanResult$1;-><init>()V

    sput-object v0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    .line 46
    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    .line 47
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExMeanResult;->readFromParcel(Landroid/os/Parcel;)V

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/diotek/diodict3/phone/service/ExMeanResult$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/diotek/diodict3/phone/service/ExMeanResult$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/diotek/diodict3/phone/service/ExMeanResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "csKeyword"    # Ljava/lang/CharSequence;
    .param p2, "csMeaning"    # Ljava/lang/CharSequence;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    .line 50
    iput-object p2, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    .line 51
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 86
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    .line 87
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    .line 88
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    return v0
.end method

.method public getKeyword()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getMeaning()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 91
    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    .line 92
    iput-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    .line 93
    return-void
.end method

.method public setKeyword(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "csKeyword"    # Ljava/lang/CharSequence;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    .line 58
    return-void
.end method

.method public setMeaning(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "csMeaning"    # Ljava/lang/CharSequence;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    .line 65
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x0

    .line 24
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mKeyword:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 25
    iget-object v0, p0, Lcom/diotek/diodict3/phone/service/ExMeanResult;->mMeaning:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 26
    return-void
.end method
