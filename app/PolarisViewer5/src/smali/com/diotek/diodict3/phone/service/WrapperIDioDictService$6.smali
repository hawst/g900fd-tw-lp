.class Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$6;
.super Landroid/os/Handler;
.source "WrapperIDioDictService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;


# direct methods
.method constructor <init>(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)V
    .locals 0

    .prologue
    .line 1597
    iput-object p1, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$6;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1601
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1602
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "webview_navigate_message_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1603
    const-string/jumbo v2, "webview_navigate_message_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1604
    .local v1, "data":Ljava/lang/String;
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$6;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$700(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Landroid/webkit/WebView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1605
    iget-object v2, p0, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService$6;->this$0:Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;

    # getter for: Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->mMeanWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;->access$700(Lcom/diotek/diodict3/phone/service/WrapperIDioDictService;)Landroid/webkit/WebView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 1611
    .end local v1    # "data":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1610
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
