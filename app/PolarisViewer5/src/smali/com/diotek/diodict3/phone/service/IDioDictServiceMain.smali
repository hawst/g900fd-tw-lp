.class public interface abstract Lcom/diotek/diodict3/phone/service/IDioDictServiceMain;
.super Ljava/lang/Object;
.source "IDioDictServiceMain.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/diotek/diodict3/phone/service/IDioDictServiceMain$Stub;
    }
.end annotation


# virtual methods
.method public abstract reqAvailableDBTypeList()[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract reqDBPath()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract reqFontFileName(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
