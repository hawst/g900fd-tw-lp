.class final Lcom/android/nfc/NfcService$OffHostServiceService;
.super Lcom/gsma/services/nfc/IOffHostService$Stub;
.source "NfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/NfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "OffHostServiceService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcService;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcService;)V
    .locals 0

    .prologue
    .line 5275
    iput-object p1, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {p0}, Lcom/gsma/services/nfc/IOffHostService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lcom/gsma/services/nfc/OffHostService;)I
    .locals 4
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    const/4 v1, -0x5

    .line 5278
    monitor-enter p0

    .line 5279
    :try_start_0
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 5280
    const/4 v1, -0x4

    monitor-exit p0

    .line 5295
    :goto_0
    return v1

    .line 5282
    :cond_0
    const/16 v0, 0xc8

    .line 5283
    .local v0, "max":I
    iget-object v2, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v2}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/android/nfc/cardemulation/DynamicService;->checkTableFull(Lcom/gsma/services/nfc/OffHostService;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5284
    monitor-exit p0

    goto :goto_0

    .line 5296
    .end local v0    # "max":I
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 5286
    .restart local v0    # "max":I
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v2}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/nfc/cardemulation/DynamicService;->getOffHostServiceSize()I

    move-result v2

    const/16 v3, 0x14

    if-lt v2, v3, :cond_2

    .line 5287
    monitor-exit p0

    goto :goto_0

    .line 5289
    :cond_2
    iget-object v1, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/nfc/cardemulation/DynamicService;->addOffHostService(Lcom/gsma/services/nfc/OffHostService;)Z

    .line 5290
    iget-object v1, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/nfc/cardemulation/DynamicService;->commit()V

    .line 5291
    iget-object v1, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    iget-boolean v1, v1, Lcom/android/nfc/NfcService;->mIsHceCapable:Z

    if-eqz v1, :cond_3

    .line 5292
    iget-object v1, p0, Lcom/android/nfc/NfcService$OffHostServiceService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v1

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/nfc/cardemulation/DynamicService;->invalidateCache(I)V

    .line 5295
    :cond_3
    const/4 v1, 0x0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
