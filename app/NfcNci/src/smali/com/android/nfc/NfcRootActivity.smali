.class public Lcom/android/nfc/NfcRootActivity;
.super Landroid/app/Activity;
.source "NfcRootActivity.java"


# static fields
.field private static final BROWSER_DIALOG:I = 0x1

.field public static final EXTRA_INVALID_TAG:Ljava/lang/String; = "invalidTag"

.field static final EXTRA_LAUNCH_INTENT:Ljava/lang/String; = "launchIntent"

.field static final EXTRA_UNSUPPORTED_TAG:Ljava/lang/String; = "unSupportedTag"

.field static final EXTRA_UNSUPPORTED_TECH:Ljava/lang/String; = "mTechnology"

.field private static final INVALID_DIALOG:I = 0x3

.field public static final PREF:Ljava/lang/String; = "NfcServicePrefs"

.field static final PREF_BROWSER_NEVER_ASK:Ljava/lang/String; = "browser_never_ask"

.field static final PREF_CONTACT_NEVER_ASK:Ljava/lang/String; = "contact_never_ask"

.field private static final TAG:Ljava/lang/String; = "NfcService"

.field private static final UNSUPPORTED_DIALOG:I = 0x4

.field private static final VCARD_DIALOG:I = 0x2


# instance fields
.field private browserDialog:Landroid/app/AlertDialog;

.field private contactDialog:Landroid/app/AlertDialog;

.field private invalidTagDialog:Landroid/app/AlertDialog;

.field mBrowerSchemeList:[Ljava/lang/String;

.field mFName:Ljava/lang/String;

.field mIntent:Landroid/content/Intent;

.field mLaunchIntent:Landroid/content/Intent;

.field mMessage:Ljava/lang/String;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mPrefsEditor:Landroid/content/SharedPreferences$Editor;

.field mTagTechnology:I

.field mVcardMimeList:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "http"

    aput-object v1, v0, v2

    const-string v1, "https"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/android/nfc/NfcRootActivity;->mBrowerSchemeList:[Ljava/lang/String;

    .line 69
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "text/vcard"

    aput-object v1, v0, v2

    const-string v1, "text/x-vcard"

    aput-object v1, v0, v3

    const-string v1, "text/x-vCard"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/android/nfc/NfcRootActivity;->mVcardMimeList:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/android/nfc/NfcRootActivity;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/android/nfc/NfcRootActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/nfc/NfcRootActivity;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method


# virtual methods
.method checkSchemList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 301
    if-nez p1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v1

    .line 303
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/android/nfc/NfcRootActivity;->mBrowerSchemeList:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/android/nfc/NfcRootActivity;->mBrowerSchemeList:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 305
    const/4 v1, 0x1

    goto :goto_0

    .line 303
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method checkVCardList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 312
    if-nez p1, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v1

    .line 314
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/android/nfc/NfcRootActivity;->mVcardMimeList:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 315
    iget-object v2, p0, Lcom/android/nfc/NfcRootActivity;->mVcardMimeList:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 316
    const/4 v1, 0x1

    goto :goto_0

    .line 314
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method isValidPopFeature(Ljava/lang/String;)Z
    .locals 2
    .param p1, "popup"    # Ljava/lang/String;

    .prologue
    .line 292
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "browser"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "contact"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    :cond_0
    const/4 v0, 0x1

    .line 297
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v6, -0x1

    const/4 v7, 0x0

    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/android/nfc/NfcRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    .line 90
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "invalidTag"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    const-string v3, "NfcService"

    const-string v4, "Display invalid tag popup"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v3, "NfcService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "technology: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/nfc/NfcRootActivity;->mTagTechnology:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "mTechnology"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/android/nfc/NfcRootActivity;->mTagTechnology:I

    .line 97
    iget v3, p0, Lcom/android/nfc/NfcRootActivity;->mTagTechnology:I

    if-eq v3, v6, :cond_0

    .line 98
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lcom/android/nfc/NfcRootActivity;->showDialog(I)V

    .line 159
    :goto_0
    return-void

    .line 101
    :cond_0
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/android/nfc/NfcRootActivity;->showDialog(I)V

    goto :goto_0

    .line 108
    :cond_1
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "launchIntent"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 109
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "launchIntent"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    iput-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    .line 111
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    if-eqz v3, :cond_6

    .line 112
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_NFC_EnableSecurityPromptPopup"

    const-string v5, "none"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "popup":Ljava/lang/String;
    const-string v3, "NfcService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tag value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 118
    .local v0, "intentData":Landroid/net/Uri;
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "intentType":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/nfc/NfcRootActivity;->isValidPopFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 125
    const-string v3, "NfcServicePrefs"

    invoke-virtual {p0, v3, v7}, Lcom/android/nfc/NfcRootActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mPrefs:Landroid/content/SharedPreferences;

    .line 126
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 127
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/nfc/NfcRootActivity;->checkSchemList(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "browser_never_ask"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    .line 129
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "all"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "browser"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 130
    :cond_2
    invoke-virtual {p0}, Lcom/android/nfc/NfcRootActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07001d

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "%"

    const-string v5, "%%"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mMessage:Ljava/lang/String;

    .line 131
    invoke-virtual {p0, v8}, Lcom/android/nfc/NfcRootActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 135
    :cond_3
    if-eqz v1, :cond_5

    invoke-virtual {p0, v1}, Lcom/android/nfc/NfcRootActivity;->checkVCardList(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "contact_never_ask"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_5

    .line 137
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "all"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "contact"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 138
    :cond_4
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    const-string v4, "mFNname"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mFName:Ljava/lang/String;

    .line 139
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/android/nfc/NfcRootActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 146
    :cond_5
    :try_start_0
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v3, v4}, Lcom/android/nfc/NfcRootActivity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_1
    invoke-virtual {p0}, Lcom/android/nfc/NfcRootActivity;->finish()V

    goto/16 :goto_0

    .line 153
    .end local v0    # "intentData":Landroid/net/Uri;
    .end local v1    # "intentType":Ljava/lang/String;
    .end local v2    # "popup":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/android/nfc/NfcRootActivity;->finish()V

    goto/16 :goto_0

    .line 157
    :cond_7
    invoke-virtual {p0}, Lcom/android/nfc/NfcRootActivity;->finish()V

    goto/16 :goto_0

    .line 147
    .restart local v0    # "intentData":Landroid/net/Uri;
    .restart local v1    # "intentType":Ljava/lang/String;
    .restart local v2    # "popup":Ljava/lang/String;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 11
    .param p1, "id"    # I

    .prologue
    const v5, 0x7f070022

    const/high16 v10, 0x1040000

    const/4 v3, 0x0

    const/4 v9, 0x0

    const v8, 0x104000a

    .line 169
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const/high16 v4, 0x7f080000

    invoke-direct {v1, p0, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 170
    .local v1, "c":Landroid/view/ContextThemeWrapper;
    packed-switch p1, :pswitch_data_0

    .line 277
    :goto_0
    return-object v3

    .line 172
    :pswitch_0
    const/high16 v4, 0x7f030000

    invoke-static {p0, v4, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 173
    .local v2, "linear":Landroid/widget/LinearLayout;
    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 174
    .local v0, "browserDialogMessage":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/android/nfc/NfcRootActivity;->mMessage:Ljava/lang/String;

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f07001c

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$3;

    invoke-direct {v4, p0, v2}, Lcom/android/nfc/NfcRootActivity$3;-><init>(Lcom/android/nfc/NfcRootActivity;Landroid/widget/LinearLayout;)V

    invoke-virtual {v3, v8, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$2;

    invoke-direct {v4, p0}, Lcom/android/nfc/NfcRootActivity$2;-><init>(Lcom/android/nfc/NfcRootActivity;)V

    invoke-virtual {v3, v10, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$1;

    invoke-direct {v4, p0}, Lcom/android/nfc/NfcRootActivity$1;-><init>(Lcom/android/nfc/NfcRootActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 213
    .end local v0    # "browserDialogMessage":Landroid/widget/TextView;
    .end local v2    # "linear":Landroid/widget/LinearLayout;
    :pswitch_1
    const v4, 0x7f030004

    invoke-static {p0, v4, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 214
    .restart local v2    # "linear":Landroid/widget/LinearLayout;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f07001e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/nfc/NfcRootActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07001f

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/nfc/NfcRootActivity;->mFName:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$6;

    invoke-direct {v4, p0, v2}, Lcom/android/nfc/NfcRootActivity$6;-><init>(Lcom/android/nfc/NfcRootActivity;Landroid/widget/LinearLayout;)V

    invoke-virtual {v3, v8, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$5;

    invoke-direct {v4, p0}, Lcom/android/nfc/NfcRootActivity$5;-><init>(Lcom/android/nfc/NfcRootActivity;)V

    invoke-virtual {v3, v10, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$4;

    invoke-direct {v4, p0}, Lcom/android/nfc/NfcRootActivity$4;-><init>(Lcom/android/nfc/NfcRootActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 251
    .end local v2    # "linear":Landroid/widget/LinearLayout;
    :pswitch_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f070020

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$7;

    invoke-direct {v4, p0}, Lcom/android/nfc/NfcRootActivity$7;-><init>(Lcom/android/nfc/NfcRootActivity;)V

    invoke-virtual {v3, v8, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 263
    :pswitch_3
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f070021

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/android/nfc/NfcRootActivity$8;

    invoke-direct {v4, p0}, Lcom/android/nfc/NfcRootActivity$8;-><init>(Lcom/android/nfc/NfcRootActivity;)V

    invoke-virtual {v3, v8, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto/16 :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 164
    return-void
.end method
