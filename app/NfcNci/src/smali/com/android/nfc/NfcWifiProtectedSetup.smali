.class public final Lcom/android/nfc/NfcWifiProtectedSetup;
.super Ljava/lang/Object;
.source "NfcWifiProtectedSetup.java"


# static fields
.field private static final AUTHENTICATION_TYPE_ID:Ljava/lang/String; = "1003"

.field public static final EXTRA_WIFI_CONFIG:Ljava/lang/String; = "com.android.nfc.WIFI_CONFIG_EXTRA"

.field private static final HEX_CHARS_PER_BYTE:I = 0x2

.field private static final MAX_AUTHENTICATION_SIZE_BYTES:I = 0x4

.field private static final MAX_NETWORK_KEY_SIZE_BYTES:I = 0x40

.field private static final MAX_SSID_SIZE_BYTES:I = 0x20

.field private static final NETWORK_KEY_ID:Ljava/lang/String; = "1027"

.field public static final NFC_TOKEN_MIME_TYPE:Ljava/lang/String; = "application/vnd.wfa.wsc"

.field static final SECURITY_PSK:Ljava/lang/String; = "0020"

.field private static final SIZE_FIELD_WIDTH:I = 0x4

.field private static final SSID_ID:Ljava/lang/String; = "1045"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static bytesToHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    const/16 v5, 0x10

    .line 208
    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    new-array v0, v3, [C

    .line 209
    .local v0, "hexChars":[C
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_0

    .line 210
    aget-byte v3, p0, v1

    and-int/lit16 v2, v3, 0xff

    .line 211
    .local v2, "value":I
    mul-int/lit8 v3, v1, 0x2

    ushr-int/lit8 v4, v2, 0x4

    invoke-static {v4, v5}, Ljava/lang/Character;->forDigit(II)C

    move-result v4

    aput-char v4, v0, v3

    .line 212
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    and-int/lit8 v4, v2, 0xf

    invoke-static {v4, v5}, Ljava/lang/Character;->forDigit(II)C

    move-result v4

    aput-char v4, v0, v3

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 214
    .end local v2    # "value":I
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method private static hexStringToInt(Ljava/lang/String;)I
    .locals 4
    .param p0, "bigEndianHexString"    # Ljava/lang/String;

    .prologue
    .line 194
    const/4 v1, 0x0

    .line 196
    .local v1, "val":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 197
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    move-result v2

    or-int/2addr v1, v2

    .line 199
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 200
    shl-int/lit8 v1, v1, 0x4

    .line 196
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_1
    return v1
.end method

.method private static parse(Landroid/nfc/NdefMessage;)Landroid/net/wifi/WifiConfiguration;
    .locals 27
    .param p0, "message"    # Landroid/nfc/NdefMessage;

    .prologue
    .line 83
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v19

    .line 85
    .local v19, "records":[Landroid/nfc/NdefRecord;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v11, v0, :cond_6

    .line 86
    aget-object v18, v19, v11

    .line 87
    .local v18, "record":Landroid/nfc/NdefRecord;
    new-instance v25, Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/lang/String;-><init>([B)V

    const-string v26, "application/vnd.wfa.wsc"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 88
    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/android/nfc/NfcWifiProtectedSetup;->bytesToHex([B)Ljava/lang/String;

    move-result-object v10

    .line 90
    .local v10, "hexStringPayload":Ljava/lang/String;
    const-string v25, "1045"

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v24

    .line 92
    .local v24, "ssidStringIndex":I
    if-lez v24, :cond_5

    .line 93
    const-string v25, "1027"

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v16

    .line 94
    .local v16, "networkKeyStringIndex":I
    if-lez v16, :cond_5

    .line 96
    const-string v25, "1045"

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    add-int v24, v24, v25

    .line 97
    const-string v25, "1027"

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    add-int v16, v16, v25

    .line 101
    add-int/lit8 v25, v24, 0x4

    :try_start_0
    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 107
    .local v22, "ssidSize":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/android/nfc/NfcWifiProtectedSetup;->hexStringToInt(Ljava/lang/String;)I

    move-result v23

    .line 108
    .local v23, "ssidSizeBytes":I
    const/16 v25, 0x20

    move/from16 v0, v23

    move/from16 v1, v25

    if-le v0, v1, :cond_1

    .line 109
    const/4 v8, 0x0

    .line 190
    .end local v10    # "hexStringPayload":Ljava/lang/String;
    .end local v16    # "networkKeyStringIndex":I
    .end local v18    # "record":Landroid/nfc/NdefRecord;
    .end local v22    # "ssidSize":Ljava/lang/String;
    .end local v23    # "ssidSizeBytes":I
    .end local v24    # "ssidStringIndex":I
    :cond_0
    :goto_1
    return-object v8

    .line 103
    .restart local v10    # "hexStringPayload":Ljava/lang/String;
    .restart local v16    # "networkKeyStringIndex":I
    .restart local v18    # "record":Landroid/nfc/NdefRecord;
    .restart local v24    # "ssidStringIndex":I
    :catch_0
    move-exception v9

    .line 104
    .local v9, "ex":Ljava/lang/IndexOutOfBoundsException;
    const/4 v8, 0x0

    goto :goto_1

    .line 114
    .end local v9    # "ex":Ljava/lang/IndexOutOfBoundsException;
    .restart local v22    # "ssidSize":Ljava/lang/String;
    .restart local v23    # "ssidSizeBytes":I
    :cond_1
    add-int/lit8 v25, v16, 0x4

    :try_start_1
    move/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .line 120
    .local v14, "networkKeySize":Ljava/lang/String;
    invoke-static {v14}, Lcom/android/nfc/NfcWifiProtectedSetup;->hexStringToInt(Ljava/lang/String;)I

    move-result v15

    .line 121
    .local v15, "networkKeySizeBytes":I
    const/16 v25, 0x40

    move/from16 v0, v25

    if-le v15, v0, :cond_2

    .line 122
    const/4 v8, 0x0

    goto :goto_1

    .line 116
    .end local v14    # "networkKeySize":Ljava/lang/String;
    .end local v15    # "networkKeySizeBytes":I
    :catch_1
    move-exception v9

    .line 117
    .restart local v9    # "ex":Ljava/lang/IndexOutOfBoundsException;
    const/4 v8, 0x0

    goto :goto_1

    .line 125
    .end local v9    # "ex":Ljava/lang/IndexOutOfBoundsException;
    .restart local v14    # "networkKeySize":Ljava/lang/String;
    .restart local v15    # "networkKeySizeBytes":I
    :cond_2
    add-int/lit8 v24, v24, 0x4

    .line 126
    add-int/lit8 v16, v16, 0x4

    .line 131
    :try_start_2
    div-int/lit8 v21, v24, 0x2

    .line 132
    .local v21, "ssidByteIndex":I
    new-instance v20, Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v25

    add-int v26, v21, v23

    move-object/from16 v0, v25

    move/from16 v1, v21

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v25

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 135
    .local v20, "ssid":Ljava/lang/String;
    div-int/lit8 v13, v16, 0x2

    .line 136
    .local v13, "networkKeyByteIndex":I
    new-instance v12, Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v25

    add-int v26, v13, v15

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v0, v13, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    .line 143
    .local v12, "networkKey":Ljava/lang/String;
    new-instance v8, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v8}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 144
    .local v8, "configuration":Landroid/net/wifi/WifiConfiguration;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v26, 0x22

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x22

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    iput-object v0, v8, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 145
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v26, 0x22

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x22

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    iput-object v0, v8, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 148
    const-string v25, "1003"

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 149
    .local v7, "authenticationTypeStringIndex":I
    if-lez v7, :cond_0

    .line 150
    const-string v25, "1003"

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    add-int v7, v7, v25

    .line 153
    add-int/lit8 v25, v7, 0x4

    :try_start_3
    move/from16 v0, v25

    invoke-virtual {v10, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v5

    .line 159
    .local v5, "authenticationTypeSize":Ljava/lang/String;
    invoke-static {v5}, Lcom/android/nfc/NfcWifiProtectedSetup;->hexStringToInt(Ljava/lang/String;)I

    move-result v6

    .line 160
    .local v6, "authenticationTypeSizeBytes":I
    const/16 v25, 0x4

    move/from16 v0, v25

    if-le v6, v0, :cond_3

    .line 161
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 139
    .end local v5    # "authenticationTypeSize":Ljava/lang/String;
    .end local v6    # "authenticationTypeSizeBytes":I
    .end local v7    # "authenticationTypeStringIndex":I
    .end local v8    # "configuration":Landroid/net/wifi/WifiConfiguration;
    .end local v12    # "networkKey":Ljava/lang/String;
    .end local v13    # "networkKeyByteIndex":I
    .end local v20    # "ssid":Ljava/lang/String;
    .end local v21    # "ssidByteIndex":I
    :catch_2
    move-exception v9

    .line 140
    .local v9, "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 155
    .end local v9    # "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v7    # "authenticationTypeStringIndex":I
    .restart local v8    # "configuration":Landroid/net/wifi/WifiConfiguration;
    .restart local v12    # "networkKey":Ljava/lang/String;
    .restart local v13    # "networkKeyByteIndex":I
    .restart local v20    # "ssid":Ljava/lang/String;
    .restart local v21    # "ssidByteIndex":I
    :catch_3
    move-exception v9

    .line 156
    .local v9, "ex":Ljava/lang/IndexOutOfBoundsException;
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 163
    .end local v9    # "ex":Ljava/lang/IndexOutOfBoundsException;
    .restart local v5    # "authenticationTypeSize":Ljava/lang/String;
    .restart local v6    # "authenticationTypeSizeBytes":I
    :cond_3
    add-int/lit8 v7, v7, 0x4

    .line 166
    :try_start_4
    div-int/lit8 v4, v7, 0x2

    .line 167
    .local v4, "authenticationTypeByteIndex":I
    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v17

    .line 168
    .local v17, "payload":[B
    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v25

    add-int v26, v4, v6

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-static {v0, v4, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/android/nfc/NfcWifiProtectedSetup;->bytesToHex([B)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v3

    .line 174
    .local v3, "authenticationType":Ljava/lang/String;
    const-string v25, "0020"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 175
    iget-object v0, v8, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-virtual/range {v25 .. v26}, Ljava/util/BitSet;->set(I)V

    .line 176
    const-string v25, "[0-9A-Fa-f]{64}"

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 177
    iput-object v12, v8, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_1

    .line 171
    .end local v3    # "authenticationType":Ljava/lang/String;
    .end local v4    # "authenticationTypeByteIndex":I
    .end local v17    # "payload":[B
    :catch_4
    move-exception v9

    .line 172
    .local v9, "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 179
    .end local v9    # "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v3    # "authenticationType":Ljava/lang/String;
    .restart local v4    # "authenticationTypeByteIndex":I
    .restart local v17    # "payload":[B
    :cond_4
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v26, 0x22

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const/16 v26, 0x22

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    iput-object v0, v8, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_1

    .line 85
    .end local v3    # "authenticationType":Ljava/lang/String;
    .end local v4    # "authenticationTypeByteIndex":I
    .end local v5    # "authenticationTypeSize":Ljava/lang/String;
    .end local v6    # "authenticationTypeSizeBytes":I
    .end local v7    # "authenticationTypeStringIndex":I
    .end local v8    # "configuration":Landroid/net/wifi/WifiConfiguration;
    .end local v10    # "hexStringPayload":Ljava/lang/String;
    .end local v12    # "networkKey":Ljava/lang/String;
    .end local v13    # "networkKeyByteIndex":I
    .end local v14    # "networkKeySize":Ljava/lang/String;
    .end local v15    # "networkKeySizeBytes":I
    .end local v16    # "networkKeyStringIndex":I
    .end local v17    # "payload":[B
    .end local v20    # "ssid":Ljava/lang/String;
    .end local v21    # "ssidByteIndex":I
    .end local v22    # "ssidSize":Ljava/lang/String;
    .end local v23    # "ssidSizeBytes":I
    .end local v24    # "ssidStringIndex":I
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 190
    .end local v18    # "record":Landroid/nfc/NdefRecord;
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method public static tryNfcWifiSetup(Landroid/nfc/tech/Ndef;Landroid/content/Context;)Z
    .locals 7
    .param p0, "ndef"    # Landroid/nfc/tech/Ndef;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 57
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v3

    .line 61
    :cond_1
    invoke-virtual {p0}, Landroid/nfc/tech/Ndef;->getCachedNdefMessage()Landroid/nfc/NdefMessage;

    move-result-object v0

    .line 62
    .local v0, "cachedNdefMessage":Landroid/nfc/NdefMessage;
    if-eqz v0, :cond_0

    .line 66
    invoke-static {v0}, Lcom/android/nfc/NfcWifiProtectedSetup;->parse(Landroid/nfc/NdefMessage;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    .line 68
    .local v2, "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
    if-eqz v2, :cond_0

    invoke-static {p1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v4

    const-string v5, "no_config_wifi"

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, v5, v6}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 70
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.android.nfc.WIFI_CONFIG_EXTRA"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    const-class v4, Lcom/android/nfc/ConfirmConnectToWifiNetworkActivity;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 74
    .local v1, "configureNetworkIntent":Landroid/content/Intent;
    const/high16 v3, 0x18000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 75
    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 76
    const/4 v3, 0x1

    goto :goto_0
.end method
