.class Lcom/android/nfc/NfcKorTagConnectionPopup$1;
.super Ljava/lang/Object;
.source "NfcKorTagConnectionPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/nfc/NfcKorTagConnectionPopup;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcKorTagConnectionPopup;

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcKorTagConnectionPopup;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->this$0:Lcom/android/nfc/NfcKorTagConnectionPopup;

    iput-object p2, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->val$v:Landroid/view/View;

    const v2, 0x7f09000c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 70
    .local v0, "askOption":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->this$0:Lcom/android/nfc/NfcKorTagConnectionPopup;

    # getter for: Lcom/android/nfc/NfcKorTagConnectionPopup;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/android/nfc/NfcKorTagConnectionPopup;->access$000(Lcom/android/nfc/NfcKorTagConnectionPopup;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "disable_dialog"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 72
    iget-object v1, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->this$0:Lcom/android/nfc/NfcKorTagConnectionPopup;

    # getter for: Lcom/android/nfc/NfcKorTagConnectionPopup;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/android/nfc/NfcKorTagConnectionPopup;->access$000(Lcom/android/nfc/NfcKorTagConnectionPopup;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .end local v0    # "askOption":Landroid/widget/CheckBox;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->this$0:Lcom/android/nfc/NfcKorTagConnectionPopup;

    # getter for: Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/android/nfc/NfcKorTagConnectionPopup;->access$100(Lcom/android/nfc/NfcKorTagConnectionPopup;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 77
    iget-object v1, p0, Lcom/android/nfc/NfcKorTagConnectionPopup$1;->this$0:Lcom/android/nfc/NfcKorTagConnectionPopup;

    invoke-virtual {v1}, Lcom/android/nfc/NfcKorTagConnectionPopup;->finish()V

    .line 78
    return-void

    .line 74
    :catch_0
    move-exception v1

    goto :goto_0
.end method
