.class Lcom/android/nfc/HciEventControl$1;
.super Ljava/lang/Object;
.source "HciEventControl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/HciEventControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/HciEventControl;


# direct methods
.method constructor <init>(Lcom/android/nfc/HciEventControl;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/nfc/HciEventControl$1;->this$0:Lcom/android/nfc/HciEventControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 101
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcServiceHciEventControl"

    const-string v1, "SmartcardService onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/HciEventControl$1;->this$0:Lcom/android/nfc/HciEventControl;

    invoke-static {p2}, Lorg/simalliance/openmobileapi/service/ISmartcardService$Stub;->asInterface(Landroid/os/IBinder;)Lorg/simalliance/openmobileapi/service/ISmartcardService;

    move-result-object v1

    # setter for: Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;
    invoke-static {v0, v1}, Lcom/android/nfc/HciEventControl;->access$002(Lcom/android/nfc/HciEventControl;Lorg/simalliance/openmobileapi/service/ISmartcardService;)Lorg/simalliance/openmobileapi/service/ISmartcardService;

    .line 103
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 107
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcServiceHciEventControl"

    const-string v1, "SmartcardService onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    return-void
.end method
