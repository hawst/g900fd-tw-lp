.class public Lcom/android/nfc/dhimpl/NativeNfcManager;
.super Lcom/android/nfc/DeviceHostImplement;
.source "NativeNfcManager.java"


# static fields
.field static final DEFAULT_LLCP_MIU:I = 0x7bc

.field static final DEFAULT_LLCP_RWSIZE:I = 0x2

.field static final DRIVER_NAME:Ljava/lang/String; = "android-nci"

.field public static final INTERNAL_TARGET_DESELECTED_ACTION:Ljava/lang/String; = "com.android.nfc.action.INTERNAL_TARGET_DESELECTED"

.field static final PREF:Ljava/lang/String; = "NciDeviceHost"

.field private static final TAG:Ljava/lang/String; = "NativeNfcManager"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

.field private mNative:J

.field private mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "nfc_nci_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/nfc/DeviceHost$DeviceHostListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/android/nfc/DeviceHost$DeviceHostListener;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/android/nfc/DeviceHostImplement;-><init>()V

    .line 62
    iput-object p2, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    .line 63
    invoke-virtual {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->initializeNativeStructure()Z

    .line 64
    iput-object p1, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mContext:Landroid/content/Context;

    .line 65
    return-void
.end method

.method private native doCreateLlcpConnectionlessSocket(ILjava/lang/String;)Lcom/android/nfc/dhimpl/NativeLlcpConnectionlessSocket;
.end method

.method private native doCreateLlcpServiceSocket(ILjava/lang/String;III)Lcom/android/nfc/dhimpl/NativeLlcpServiceSocket;
.end method

.method private native doCreateLlcpSocket(IIII)Lcom/android/nfc/dhimpl/NativeLlcpSocket;
.end method

.method private native doDeinitialize()Z
.end method

.method private native doDisableScreenOffSuspend()V
.end method

.method private native doDownload()Z
.end method

.method private native doDump()Ljava/lang/String;
.end method

.method private native doEnableDiscovery(IZZZZ)V
.end method

.method private native doEnableScreenOffSuspend()V
.end method

.method private native doGetTimeout(I)I
.end method

.method private native doInitialize()Z
.end method

.method private native doResetTimeouts()V
.end method

.method private native doSetP2pInitiatorModes(I)V
.end method

.method private native doSetP2pTargetModes(I)V
.end method

.method private native doSetTimeout(II)Z
.end method

.method private notifyAidRoutingTableFull()V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onAidRoutingTableFull()V

    .line 473
    return-void
.end method

.method private notifyCardEmulationAidSelected([B)V
    .locals 1
    .param p1, "aid"    # [B

    .prologue
    .line 492
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onCardEmulationAidSelected4Google([B)V

    .line 493
    return-void
.end method

.method private notifyConnectivityListeners(I)V
    .locals 1
    .param p1, "evtSrc"    # I

    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onConnectivityEvent(I)V

    .line 501
    return-void
.end method

.method private notifyHostEmuActivated()V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onHostCardEmulationActivated()V

    .line 461
    return-void
.end method

.method private notifyHostEmuData([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 464
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onHostCardEmulationData([B)V

    .line 465
    return-void
.end method

.method private notifyHostEmuDeactivated()V
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onHostCardEmulationDeactivated()V

    .line 469
    return-void
.end method

.method private notifyLlcpLinkActivation(Lcom/android/nfc/dhimpl/NativeP2pDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/android/nfc/dhimpl/NativeP2pDevice;

    .prologue
    .line 426
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onLlcpLinkActivated(Lcom/android/nfc/DeviceHost$NfcDepEndpoint;)V

    .line 427
    return-void
.end method

.method private notifyLlcpLinkDeactivated(Lcom/android/nfc/dhimpl/NativeP2pDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/android/nfc/dhimpl/NativeP2pDevice;

    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onLlcpLinkDeactivated(Lcom/android/nfc/DeviceHost$NfcDepEndpoint;)V

    .line 434
    return-void
.end method

.method private notifyLlcpLinkFirstPacketReceived(Lcom/android/nfc/dhimpl/NativeP2pDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/android/nfc/dhimpl/NativeP2pDevice;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onLlcpFirstPacketReceived(Lcom/android/nfc/DeviceHost$NfcDepEndpoint;)V

    .line 441
    return-void
.end method

.method private notifyNdefMessageListeners(Lcom/android/nfc/dhimpl/NativeNfcTag;)V
    .locals 1
    .param p1, "tag"    # Lcom/android/nfc/dhimpl/NativeNfcTag;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onRemoteEndpointDiscovered(Lcom/android/nfc/DeviceHost$TagEndpoint;)V

    .line 412
    return-void
.end method

.method private notifyRfFieldActivated()V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onRemoteFieldActivated()V

    .line 477
    return-void
.end method

.method private notifyRfFieldDeactivated()V
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onRemoteFieldDeactivated()V

    .line 481
    return-void
.end method

.method private notifySeApduReceived([B)V
    .locals 1
    .param p1, "apdu"    # [B

    .prologue
    .line 504
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onSeApduReceived([B)V

    .line 505
    return-void
.end method

.method private notifySeEmvCardRemoval()V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onSeEmvCardRemoval()V

    .line 509
    return-void
.end method

.method private notifySeFieldActivated()V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onRemoteFieldActivated()V

    .line 445
    return-void
.end method

.method private notifySeFieldDeactivated()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mListener:Lcom/android/nfc/DeviceHost$DeviceHostListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$DeviceHostListener;->onRemoteFieldDeactivated()V

    .line 449
    return-void
.end method

.method private notifySeListenActivated()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onSeListenActivated()V

    .line 453
    return-void
.end method

.method private notifySeListenDeactivated()V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onSeListenDeactivated()V

    .line 457
    return-void
.end method

.method private notifySeMifareAccess([B)V
    .locals 1
    .param p1, "block"    # [B

    .prologue
    .line 512
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0, p1}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onSeMifareAccess([B)V

    .line 513
    return-void
.end method

.method private notifyTargetDeselected()V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onCardEmulationDeselected()V

    .line 419
    return-void
.end method

.method private notifyTransactionListeners([B[BI)V
    .locals 1
    .param p1, "aid"    # [B
    .param p2, "data"    # [B
    .param p3, "evtSrc"    # I

    .prologue
    .line 496
    iget-object v0, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/nfc/DeviceHost$SecureElementListener;->onCardEmulationAidSelected([B[BI)V

    .line 497
    return-void
.end method


# virtual methods
.method public native GetDefaultSE()I
.end method

.method public native SWPSelfTest(I)I
.end method

.method public canMakeReadOnly(I)Z
    .locals 2
    .param p1, "ndefType"    # I

    .prologue
    const/4 v0, 0x1

    .line 322
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/16 v1, 0x65

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkFirmware()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public clearRoutingEntry(I)Z
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doClearRoutingEntry(I)Z

    move-result v0

    return v0
.end method

.method public native commitRouting()Z
.end method

.method public createLlcpConnectionlessSocket(ILjava/lang/String;)Lcom/android/nfc/DeviceHost$LlcpConnectionlessSocket;
    .locals 5
    .param p1, "nSap"    # I
    .param p2, "sn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/nfc/LlcpException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-direct {p0, p1, p2}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doCreateLlcpConnectionlessSocket(ILjava/lang/String;)Lcom/android/nfc/dhimpl/NativeLlcpConnectionlessSocket;

    move-result-object v1

    .line 225
    .local v1, "socket":Lcom/android/nfc/DeviceHost$LlcpConnectionlessSocket;
    if-eqz v1, :cond_0

    .line 226
    return-object v1

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doGetLastError()I

    move-result v0

    .line 231
    .local v0, "error":I
    const-string v2, "NativeNfcManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to create llcp socket: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/nfc/ErrorCodes;->asString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    packed-switch v0, :pswitch_data_0

    .line 238
    :pswitch_0
    new-instance v2, Lcom/android/nfc/LlcpException;

    const/16 v3, -0xa

    invoke-direct {v2, v3}, Lcom/android/nfc/LlcpException;-><init>(I)V

    throw v2

    .line 236
    :pswitch_1
    new-instance v2, Lcom/android/nfc/LlcpException;

    invoke-direct {v2, v0}, Lcom/android/nfc/LlcpException;-><init>(I)V

    throw v2

    .line 233
    :pswitch_data_0
    .packed-switch -0xc
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public createLlcpServerSocket(ILjava/lang/String;III)Lcom/android/nfc/DeviceHost$LlcpServerSocket;
    .locals 5
    .param p1, "nSap"    # I
    .param p2, "sn"    # Ljava/lang/String;
    .param p3, "miu"    # I
    .param p4, "rw"    # I
    .param p5, "linearBufferLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/nfc/LlcpException;
        }
    .end annotation

    .prologue
    .line 248
    invoke-direct/range {p0 .. p5}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doCreateLlcpServiceSocket(ILjava/lang/String;III)Lcom/android/nfc/dhimpl/NativeLlcpServiceSocket;

    move-result-object v1

    .line 249
    .local v1, "socket":Lcom/android/nfc/DeviceHost$LlcpServerSocket;
    if-eqz v1, :cond_0

    .line 250
    return-object v1

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doGetLastError()I

    move-result v0

    .line 255
    .local v0, "error":I
    const-string v2, "NativeNfcManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to create llcp socket: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/nfc/ErrorCodes;->asString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    packed-switch v0, :pswitch_data_0

    .line 262
    :pswitch_0
    new-instance v2, Lcom/android/nfc/LlcpException;

    const/16 v3, -0xa

    invoke-direct {v2, v3}, Lcom/android/nfc/LlcpException;-><init>(I)V

    throw v2

    .line 260
    :pswitch_1
    new-instance v2, Lcom/android/nfc/LlcpException;

    invoke-direct {v2, v0}, Lcom/android/nfc/LlcpException;-><init>(I)V

    throw v2

    .line 257
    :pswitch_data_0
    .packed-switch -0xc
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public createLlcpSocket(IIII)Lcom/android/nfc/DeviceHost$LlcpSocket;
    .locals 5
    .param p1, "sap"    # I
    .param p2, "miu"    # I
    .param p3, "rw"    # I
    .param p4, "linearBufferLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/nfc/LlcpException;
        }
    .end annotation

    .prologue
    .line 272
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doCreateLlcpSocket(IIII)Lcom/android/nfc/dhimpl/NativeLlcpSocket;

    move-result-object v1

    .line 273
    .local v1, "socket":Lcom/android/nfc/DeviceHost$LlcpSocket;
    if-eqz v1, :cond_0

    .line 274
    return-object v1

    .line 277
    :cond_0
    invoke-virtual {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doGetLastError()I

    move-result v0

    .line 279
    .local v0, "error":I
    const-string v2, "NativeNfcManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to create llcp socket: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/nfc/ErrorCodes;->asString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    packed-switch v0, :pswitch_data_0

    .line 286
    :pswitch_0
    new-instance v2, Lcom/android/nfc/LlcpException;

    const/16 v3, -0xa

    invoke-direct {v2, v3}, Lcom/android/nfc/LlcpException;-><init>(I)V

    throw v2

    .line 284
    :pswitch_1
    new-instance v2, Lcom/android/nfc/LlcpException;

    invoke-direct {v2, v0}, Lcom/android/nfc/LlcpException;-><init>(I)V

    throw v2

    .line 281
    :pswitch_data_0
    .packed-switch -0xc
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public deinitialize()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 98
    iget-object v2, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mContext:Landroid/content/Context;

    const-string v3, "NciDeviceHost"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 99
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 101
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "se_wired"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 102
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 103
    invoke-direct {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doDeinitialize()Z

    move-result v2

    return v2
.end method

.method public native disableDiscovery()V
.end method

.method public disableScreenOffSuspend()Z
    .locals 1

    .prologue
    .line 402
    invoke-direct {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doDisableScreenOffSuspend()V

    .line 403
    const/4 v0, 0x1

    return v0
.end method

.method public native doAbort()V
.end method

.method public native doActivateLlcp()Z
.end method

.method public native doCheckLlcp()Z
.end method

.method public native doClearRoutingEntry(I)Z
.end method

.method public native doDeselectSecureElement(I)V
.end method

.method public native doGetLastError()I
.end method

.method public native doGetSecureElementList()[I
.end method

.method public native doGetSecureElementTechList()I
.end method

.method public native doPrbsOff()V
.end method

.method public native doPrbsOn(II)V
.end method

.method public native doRouteAid([BII)Z
.end method

.method public native doSelectSecureElement(I)V
.end method

.method public native doSetRoutingEntry(IIII)Z
.end method

.method public native doSetSEPowerOffState(IZ)V
.end method

.method public native doSetScreenOrPowerState(I)V
.end method

.method public native doSetSecureElementListenTechMask(I)V
.end method

.method public dump()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doDump()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public enableDiscovery(Lcom/android/nfc/NfcDiscoveryParameters;Z)V
    .locals 6
    .param p1, "params"    # Lcom/android/nfc/NfcDiscoveryParameters;
    .param p2, "restart"    # Z

    .prologue
    .line 168
    invoke-virtual {p1}, Lcom/android/nfc/NfcDiscoveryParameters;->getTechMask()I

    move-result v1

    invoke-virtual {p1}, Lcom/android/nfc/NfcDiscoveryParameters;->shouldEnableLowPowerDiscovery()Z

    move-result v2

    invoke-virtual {p1}, Lcom/android/nfc/NfcDiscoveryParameters;->shouldEnableReaderMode()Z

    move-result v3

    invoke-virtual {p1}, Lcom/android/nfc/NfcDiscoveryParameters;->shouldEnableHostRouting()Z

    move-result v4

    move-object v0, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doEnableDiscovery(IZZZZ)V

    .line 170
    return-void
.end method

.method public enableScreenOffSuspend()Z
    .locals 1

    .prologue
    .line 395
    invoke-direct {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doEnableScreenOffSuspend()V

    .line 396
    const/4 v0, 0x1

    return v0
.end method

.method public native getAidTableSize()I
.end method

.method public native getChipVer()I
.end method

.method public getDefaultLlcpMiu()I
    .locals 1

    .prologue
    .line 378
    const/16 v0, 0x7bc

    return v0
.end method

.method public getDefaultLlcpRwSize()I
    .locals 1

    .prologue
    .line 383
    const/4 v0, 0x2

    return v0
.end method

.method public getExtendedLengthApdusSupported()Z
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x1

    return v0
.end method

.method public native getFWVersion()I
.end method

.method public getMaxTransceiveLength(I)I
    .locals 1
    .param p1, "technology"    # I

    .prologue
    const/16 v0, 0xfd

    .line 328
    packed-switch p1, :pswitch_data_0

    .line 354
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    :pswitch_1
    return v0

    .line 341
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->getExtendedLengthApdusSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    const v0, 0xfeff

    goto :goto_0

    .line 349
    :cond_0
    const/16 v0, 0x105

    goto :goto_0

    .line 352
    :pswitch_3
    const/16 v0, 0xfc

    goto :goto_0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "android-nci"

    return-object v0
.end method

.method public getTimeout(I)I
    .locals 1
    .param p1, "tech"    # I

    .prologue
    .line 316
    invoke-direct {p0, p1}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doGetTimeout(I)I

    move-result v0

    return v0
.end method

.method public initialize()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 81
    iget-object v2, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mContext:Landroid/content/Context;

    const-string v3, "NciDeviceHost"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 82
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 84
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "se_wired"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    const-wide/16 v2, 0x2ee0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 87
    const-string v2, "se_wired"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doInitialize()Z

    move-result v2

    return v2

    .line 89
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public native initializeNativeStructure()Z
.end method

.method public resetTimeouts()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doResetTimeouts()V

    .line 302
    return-void
.end method

.method public routeAid([BII)Z
    .locals 2
    .param p1, "aid"    # [B
    .param p2, "route"    # I
    .param p3, "powerState"    # I

    .prologue
    .line 118
    const/4 v0, 0x1

    .line 120
    .local v0, "status":Z
    if-nez p2, :cond_0

    .line 121
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doRouteAid([BII)Z

    move-result v0

    .line 126
    :goto_0
    return v0

    .line 123
    :cond_0
    or-int/lit16 v1, p3, 0x80

    invoke-virtual {p0, p1, p2, v1}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doRouteAid([BII)Z

    move-result v0

    goto :goto_0
.end method

.method public native sendRawFrame([B)Z
.end method

.method public native setDefaultProtoRoute(III)V
.end method

.method public native setDefaultTechRoute(III)V
.end method

.method public setP2pInitiatorModes(I)V
    .locals 0
    .param p1, "modes"    # I

    .prologue
    .line 362
    invoke-direct {p0, p1}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doSetP2pInitiatorModes(I)V

    .line 363
    return-void
.end method

.method public setP2pTargetModes(I)V
    .locals 0
    .param p1, "modes"    # I

    .prologue
    .line 368
    invoke-direct {p0, p1}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doSetP2pTargetModes(I)V

    .line 369
    return-void
.end method

.method public setRoutingEntry(IIII)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "value"    # I
    .param p3, "route"    # I
    .param p4, "power"    # I

    .prologue
    .line 141
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doSetRoutingEntry(IIII)Z

    move-result v0

    return v0
.end method

.method public setSeListener(Lcom/android/nfc/DeviceHost$SecureElementListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/nfc/DeviceHost$SecureElementListener;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/android/nfc/dhimpl/NativeNfcManager;->mSeListener:Lcom/android/nfc/DeviceHost$SecureElementListener;

    .line 489
    return-void
.end method

.method public setTimeout(II)Z
    .locals 1
    .param p1, "tech"    # I
    .param p2, "timeout"    # I

    .prologue
    .line 310
    invoke-direct {p0, p1, p2}, Lcom/android/nfc/dhimpl/NativeNfcManager;->doSetTimeout(II)Z

    move-result v0

    return v0
.end method

.method public native unrouteAid([B)Z
.end method
