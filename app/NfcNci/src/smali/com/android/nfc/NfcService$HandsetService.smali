.class final Lcom/android/nfc/NfcService$HandsetService;
.super Lcom/gsma/services/utils/IHandset$Stub;
.source "NfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/NfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "HandsetService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcService;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcService;)V
    .locals 0

    .prologue
    .line 4879
    iput-object p1, p0, Lcom/android/nfc/NfcService$HandsetService;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {p0}, Lcom/gsma/services/utils/IHandset$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public enableMultiEvt_transactionReception()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v4, -0x3

    .line 4882
    iget-object v5, p0, Lcom/android/nfc/NfcService$HandsetService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v5}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 4883
    sget-boolean v4, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v4, :cond_0

    const-string v4, "NfcService"

    const-string v5, "Gsma Apis are not Supported in this project"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4884
    :cond_0
    const/4 v4, -0x1

    .line 4910
    :cond_1
    :goto_0
    return v4

    .line 4886
    :cond_2
    sget-boolean v5, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v5, :cond_3

    const-string v5, "NfcService"

    const-string v6, "enableMultiEvt_transactionReception"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4888
    :cond_3
    :try_start_0
    iget-object v5, p0, Lcom/android/nfc/NfcService$HandsetService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v5, v5, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    const-string v6, "sim"

    invoke-virtual {v5, v6}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v5

    if-eqz v5, :cond_1

    .line 4908
    :cond_4
    iget-object v4, p0, Lcom/android/nfc/NfcService$HandsetService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v4, v4, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    const-string v5, "sim"

    invoke-virtual {v4, v5, v7}, Lcom/android/nfc/HciEventControl;->enableMultiEvt_transactionReception(Ljava/lang/String;Z)V

    .line 4909
    iget-object v4, p0, Lcom/android/nfc/NfcService$HandsetService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v4, v4, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    const-string v5, "ese"

    invoke-virtual {v4, v5, v7}, Lcom/android/nfc/HciEventControl;->enableMultiEvt_transactionReception(Ljava/lang/String;Z)V

    .line 4910
    const/4 v4, 0x0

    goto :goto_0

    .line 4891
    :catch_0
    move-exception v0

    .line 4892
    .local v0, "e":Landroid/os/RemoteException;
    sget-boolean v5, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v5, :cond_5

    const-string v5, "NfcService"

    const-string v6, "Checking CDF failed."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4894
    :cond_5
    :try_start_1
    iget-object v5, p0, Lcom/android/nfc/NfcService$HandsetService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v5, v5, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    const-string v6, "ese"

    invoke-virtual {v5, v6}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma(Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v5

    if-nez v5, :cond_4

    goto :goto_0

    .line 4897
    :catch_1
    move-exception v2

    .line 4898
    .local v2, "e3":Landroid/os/RemoteException;
    sget-boolean v5, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v5, :cond_1

    const-string v5, "NfcService"

    const-string v6, "Checking CDF failed."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4900
    .end local v2    # "e3":Landroid/os/RemoteException;
    :catch_2
    move-exception v3

    .line 4901
    .local v3, "e4":Ljava/lang/NullPointerException;
    sget-boolean v5, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v5, :cond_1

    const-string v5, "NfcService"

    const-string v6, "mHciEventControl is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4904
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v3    # "e4":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v1

    .line 4905
    .local v1, "e2":Ljava/lang/NullPointerException;
    sget-boolean v5, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v5, :cond_1

    const-string v5, "NfcService"

    const-string v6, "mHciEventControl is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getProperty(I)Z
    .locals 3
    .param p1, "feature"    # I

    .prologue
    .line 4915
    const/4 v0, 0x0

    .line 4916
    .local v0, "result":Z
    sparse-switch p1, :sswitch_data_0

    .line 4954
    :cond_0
    :goto_0
    return v0

    .line 4918
    :sswitch_0
    const/4 v0, 0x1

    .line 4919
    goto :goto_0

    .line 4921
    :sswitch_1
    const/4 v0, 0x1

    .line 4922
    goto :goto_0

    .line 4924
    :sswitch_2
    const/4 v0, 0x1

    .line 4925
    goto :goto_0

    .line 4927
    :sswitch_3
    const/4 v0, 0x0

    .line 4928
    const-string v1, "NXP_PN544C3"

    const-string v2, "NXP_PN547C2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4929
    const/4 v0, 0x1

    goto :goto_0

    .line 4930
    :cond_1
    const-string v1, "NXP_PN547C2"

    const-string v2, "NXP_PN547C2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4931
    const/4 v0, 0x1

    goto :goto_0

    .line 4932
    :cond_2
    const-string v1, "NXP_PN547C1"

    const-string v2, "NXP_PN547C2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4933
    const/4 v0, 0x1

    goto :goto_0

    .line 4934
    :cond_3
    const-string v1, "NXP_PN548C2"

    const-string v2, "NXP_PN547C2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4935
    const/4 v0, 0x1

    goto :goto_0

    .line 4938
    :sswitch_4
    const/4 v0, 0x1

    .line 4939
    goto :goto_0

    .line 4941
    :sswitch_5
    const/4 v0, 0x1

    .line 4942
    goto :goto_0

    .line 4944
    :sswitch_6
    const/4 v0, 0x1

    .line 4945
    goto :goto_0

    .line 4947
    :sswitch_7
    const/4 v0, 0x1

    .line 4948
    goto :goto_0

    .line 4950
    :sswitch_8
    const/4 v0, 0x0

    .line 4951
    goto :goto_0

    .line 4916
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x20 -> :sswitch_2
        0x21 -> :sswitch_3
        0x22 -> :sswitch_4
        0x23 -> :sswitch_5
        0x50 -> :sswitch_6
        0x90 -> :sswitch_7
        0x91 -> :sswitch_8
    .end sparse-switch
.end method
