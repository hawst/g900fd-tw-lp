.class public Lcom/android/nfc/handover/BluetoothInputDeviceHandover;
.super Ljava/lang/Object;
.source "BluetoothInputDeviceHandover.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;
    }
.end annotation


# static fields
.field static final ACTION_ALLOW_CONNECT:Ljava/lang/String; = "com.android.nfc.handover.action.ALLOW_CONNECT"

.field static final ACTION_CONNECT:I = 0x2

.field static final ACTION_DENY_CONNECT:Ljava/lang/String; = "com.android.nfc.handover.action.DENY_CONNECT"

.field static final ACTION_DISCONNECT:I = 0x1

.field static final ACTION_INIT:I = 0x0

.field static final DBG:Z

.field static final MSG_NEXT_STEP:I = 0x2

.field static final MSG_TIMEOUT:I = 0x1

.field static final RESULT_CONNECTED:I = 0x1

.field static final RESULT_DISCONNECTED:I = 0x2

.field static final RESULT_PENDING:I = 0x0

.field static final STATE_BONDING:I = 0x4

.field static final STATE_COMPLETE:I = 0x7

.field static final STATE_CONNECTING:I = 0x5

.field static final STATE_DISCONNECTING:I = 0x6

.field static final STATE_INIT:I = 0x0

.field static final STATE_INIT_COMPLETE:I = 0x2

.field static final STATE_WAITING_FOR_BOND_CONFIRMATION:I = 0x3

.field static final STATE_WAITING_FOR_PROXIES:I = 0x1

.field static final TAG:Ljava/lang/String; = "BluetoothInputDeviceHandover"

.field static final TIMEOUT_MS:I = 0x4e20


# instance fields
.field mAction:I

.field final mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field final mBtClass:I

.field final mCallback:Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;

.field final mContext:Landroid/content/Context;

.field final mDevice:Landroid/bluetooth/BluetoothDevice;

.field final mHandler:Landroid/os/Handler;

.field mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

.field mInputDeviceResult:I

.field final mLock:Ljava/lang/Object;

.field final mName:Ljava/lang/String;

.field final mReceiver:Landroid/content/BroadcastReceiver;

.field mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget-boolean v0, Lcom/android/nfc/handover/HandoverManager;->DBG:Z

    sput-boolean v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;ILcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "cod"    # I
    .param p5, "callback"    # Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    .line 367
    new-instance v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$1;

    invoke-direct {v0, p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$1;-><init>(Lcom/android/nfc/handover/BluetoothInputDeviceHandover;)V

    iput-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    .line 383
    new-instance v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$2;

    invoke-direct {v0, p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$2;-><init>(Lcom/android/nfc/handover/BluetoothInputDeviceHandover;)V

    iput-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 107
    invoke-static {}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->checkMainThread()V

    .line 108
    iput-object p1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    .line 109
    iput-object p2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 110
    iput-object p3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    .line 111
    iput p4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBtClass:I

    .line 113
    iput-object p5, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mCallback:Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;

    .line 114
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 117
    return-void
.end method

.method static checkMainThread()V
    .locals 2

    .prologue
    .line 391
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 392
    new-instance v0, Ljava/lang/IllegalThreadStateException;

    const-string v1, "must be called on main thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 394
    :cond_0
    return-void
.end method


# virtual methods
.method complete(Z)V
    .locals 6
    .param p1, "connected"    # Z

    .prologue
    const/4 v3, 0x1

    .line 329
    sget-boolean v1, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "BluetoothInputDeviceHandover"

    const-string v2, "complete()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :cond_0
    const/4 v1, 0x7

    iput v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 333
    :try_start_0
    sget-boolean v1, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "BluetoothInputDeviceHandover"

    const-string v2, "unregisterReceiver()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_1
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 339
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 340
    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 341
    :try_start_1
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    if-eqz v1, :cond_2

    .line 342
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    invoke-virtual {v1, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 344
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    .line 345
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mCallback:Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;

    invoke-interface {v1, p1}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;->onBluetoothInputDeviceHandoverComplete(Z)V

    .line 349
    :goto_0
    return-void

    .line 345
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v1, "BluetoothInputDeviceHandover"

    const-string v2, "catch IllegalArgumentException and ignore it"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 339
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 340
    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 341
    :try_start_4
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    if-eqz v1, :cond_3

    .line 342
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    invoke-virtual {v1, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 344
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    .line 345
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 346
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mCallback:Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;

    invoke-interface {v1, p1}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;->onBluetoothInputDeviceHandoverComplete(Z)V

    goto :goto_0

    .line 345
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1

    .line 339
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_2
    move-exception v1

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 340
    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 341
    :try_start_6
    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    if-eqz v3, :cond_4

    .line 342
    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    invoke-virtual {v3, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 344
    :cond_4
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    .line 345
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 346
    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mCallback:Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;

    invoke-interface {v2, p1}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;->onBluetoothInputDeviceHandoverComplete(Z)V

    throw v1

    .line 345
    :catchall_3
    move-exception v1

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v1
.end method

.method getProfileProxys()Z
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, p0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    const/4 v0, 0x0

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method handleIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x2

    const/4 v7, 0x1

    const/high16 v6, -0x80000000

    const/4 v8, 0x0

    .line 297
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 299
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 300
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    iget-object v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4, v2}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    const-string v4, "com.android.nfc.handover.action.ALLOW_CONNECT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 303
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStepConnect()V

    goto :goto_0

    .line 304
    :cond_2
    const-string v4, "com.android.nfc.handover.action.DENY_CONNECT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 305
    invoke-virtual {p0, v8}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    goto :goto_0

    .line 306
    :cond_3
    const-string v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 307
    const-string v4, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 309
    .local v1, "bond":I
    const/16 v4, 0xc

    if-ne v1, v4, :cond_4

    .line 310
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStepConnect()V

    goto :goto_0

    .line 311
    :cond_4
    const/16 v4, 0xa

    if-ne v1, v4, :cond_0

    .line 312
    iget-object v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v5, 0x7f070017

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 313
    invoke-virtual {p0, v8}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    goto :goto_0

    .line 315
    .end local v1    # "bond":I
    :cond_5
    const-string v4, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_6

    iget v4, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 317
    :cond_6
    const-string v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 318
    .local v3, "state":I
    if-ne v3, v9, :cond_7

    .line 319
    iput v7, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    .line 320
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStep()V

    goto :goto_0

    .line 321
    :cond_7
    if-nez v3, :cond_0

    .line 322
    iput v9, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    .line 323
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStep()V

    goto :goto_0
.end method

.method public hasStarted()Z
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method nextStep()V
    .locals 2

    .prologue
    .line 151
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mAction:I

    if-nez v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStepInit()V

    .line 158
    :goto_0
    return-void

    .line 153
    :cond_0
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 154
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStepConnect()V

    goto :goto_0

    .line 156
    :cond_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStepDisconnect()V

    goto :goto_0
.end method

.method nextStepConnect()V
    .locals 6

    .prologue
    const/16 v3, 0xc

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 233
    sget-boolean v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "BluetoothInputDeviceHandover"

    const-string v1, "nextStepConnect()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_0
    sget-boolean v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v0, :cond_1

    const-string v0, "BluetoothInputDeviceHandover"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_1
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 285
    :cond_2
    :goto_0
    return-void

    .line 238
    :pswitch_0
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 239
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->requestPairConfirmation()V

    .line 240
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    goto :goto_0

    .line 246
    :cond_3
    :pswitch_1
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 247
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->startBonding()V

    goto :goto_0

    .line 254
    :cond_4
    :pswitch_2
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 255
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 256
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothInputDevice;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    .line 257
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    .line 258
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBtClass:I

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothDevice;->setBluetoothClass(I)Z

    .line 260
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothInputDevice;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    .line 264
    :goto_1
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    if-nez v0, :cond_6

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v3, 0x7f070011

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "..."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 266
    monitor-exit v1

    goto :goto_0

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 262
    :cond_5
    const/4 v0, 0x1

    :try_start_1
    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    goto :goto_1

    .line 268
    :cond_6
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    :pswitch_3
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    if-eqz v0, :cond_2

    .line 275
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    if-ne v0, v4, :cond_7

    .line 277
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v1, 0x7f070012

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 278
    invoke-virtual {p0, v4}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    goto/16 :goto_0

    .line 280
    :cond_7
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v1, 0x7f070013

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 281
    invoke-virtual {p0, v5}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    goto/16 :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method nextStepDisconnect()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 192
    sget-boolean v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "BluetoothInputDeviceHandover"

    const-string v1, "nextStepDisconnect()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_0
    sget-boolean v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v0, :cond_1

    const-string v0, "BluetoothInputDeviceHandover"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_1
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 223
    :cond_2
    :goto_0
    return-void

    .line 196
    :sswitch_0
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 197
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothInputDevice;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 199
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    .line 200
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothInputDevice;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    .line 204
    :goto_1
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    if-nez v0, :cond_4

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v3, 0x7f070014

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "..."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 207
    monitor-exit v1

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 202
    :cond_3
    const/4 v0, 0x2

    :try_start_1
    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    goto :goto_1

    .line 209
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :sswitch_1
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    if-eqz v0, :cond_2

    .line 216
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDeviceResult:I

    if-ne v0, v3, :cond_5

    .line 217
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v1, 0x7f070015

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 219
    :cond_5
    invoke-virtual {p0, v4}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    goto :goto_0

    .line 194
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method nextStepInit()V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 164
    iget v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 166
    :pswitch_0
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    if-nez v0, :cond_1

    .line 167
    iput v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 168
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->getProfileProxys()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    goto :goto_0

    .line 175
    :cond_1
    :pswitch_1
    iput v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 177
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 178
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothInputDevice;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179
    const-string v0, "BluetoothInputDeviceHandover"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_DISCONNECT addr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mAction:I

    .line 185
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStep()V

    goto :goto_0

    .line 182
    :cond_2
    :try_start_1
    const-string v0, "BluetoothInputDeviceHandover"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_CONNECT addr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mAction:I

    goto :goto_1

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 398
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 399
    packed-switch p1, :pswitch_data_0

    .line 405
    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    :goto_0
    :try_start_0
    monitor-exit v1

    .line 406
    return-void

    .line 401
    .restart local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    :pswitch_0
    check-cast p2, Landroid/bluetooth/BluetoothInputDevice;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    iput-object p2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mInputDevice:Landroid/bluetooth/BluetoothInputDevice;

    .line 402
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 399
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onServiceDisconnected(I)V
    .locals 0
    .param p1, "profile"    # I

    .prologue
    .line 411
    return-void
.end method

.method requestPairConfirmation()V
    .locals 3

    .prologue
    .line 357
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/nfc/handover/ConfirmConnectActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 359
    .local v0, "dialogIntent":Landroid/content/Intent;
    const/high16 v1, 0x18000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 360
    sget-boolean v1, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "BluetoothInputDeviceHandover"

    const-string v2, "ConfirmConnectActivity also need FLAG_ACTIVITY_MULTIPLE_TASK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_0
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 364
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 365
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    .line 128
    invoke-static {}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->checkMainThread()V

    .line 129
    iget v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    if-eqz v1, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    .line 132
    sget-boolean v1, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->DBG:Z

    if-eqz v1, :cond_2

    const-string v1, "BluetoothInputDeviceHandover"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start : state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 134
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    const-string v1, "com.android.nfc.handover.action.ALLOW_CONNECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    const-string v1, "com.android.nfc.handover.action.DENY_CONNECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x4e20

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 143
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mAction:I

    .line 144
    invoke-virtual {p0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->nextStep()V

    goto :goto_0
.end method

.method startBonding()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 288
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mState:I

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v2, 0x7f070016

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const v1, 0x7f070017

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->toast(Ljava/lang/CharSequence;)V

    .line 292
    invoke-virtual {p0, v4}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->complete(Z)V

    .line 294
    :cond_0
    return-void
.end method

.method toast(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 352
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->mContext:Landroid/content/Context;

    const/high16 v2, 0x7f080000

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 353
    .local v0, "c":Landroid/view/ContextThemeWrapper;
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 354
    return-void
.end method
