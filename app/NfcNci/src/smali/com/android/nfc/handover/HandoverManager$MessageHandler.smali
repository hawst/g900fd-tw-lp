.class Lcom/android/nfc/handover/HandoverManager$MessageHandler;
.super Landroid/os/Handler;
.source "HandoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/handover/HandoverManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MessageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/handover/HandoverManager;


# direct methods
.method constructor <init>(Lcom/android/nfc/handover/HandoverManager;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 137
    iget-object v4, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    # getter for: Lcom/android/nfc/handover/HandoverManager;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/nfc/handover/HandoverManager;->access$000(Lcom/android/nfc/handover/HandoverManager;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 138
    :try_start_0
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 172
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/nfc/handover/HandoverManager;->unbindServiceIfNeededLocked(Z)V

    .line 173
    monitor-exit v4

    .line 174
    return-void

    .line 140
    :pswitch_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 141
    .local v0, "pre_msg":Landroid/os/Message;
    iget v2, p1, Landroid/os/Message;->arg1:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 142
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 173
    .end local v0    # "pre_msg":Landroid/os/Message;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 145
    :pswitch_2
    :try_start_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 146
    .local v1, "transferId":I
    const-string v2, "NfcHandover"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Completed transfer id: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iget-object v2, v2, Lcom/android/nfc/handover/HandoverManager;->mPendingTransfers:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iget-object v2, v2, Lcom/android/nfc/handover/HandoverManager;->mPendingTransfers:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 150
    :cond_0
    const-string v2, "NfcHandover"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not find completed transfer id: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    .end local v1    # "transferId":I
    :pswitch_3
    iget-object v5, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iget v6, p1, Landroid/os/Message;->arg1:I

    if-eqz v6, :cond_1

    :goto_1
    iput-boolean v2, v5, Lcom/android/nfc/handover/HandoverManager;->mBluetoothEnabledByNfc:Z

    .line 156
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/android/nfc/handover/HandoverManager;->mBluetoothHeadsetConnected:Z

    .line 157
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/android/nfc/handover/HandoverManager;->mBluetoothHeadsetPending:Z

    goto/16 :goto_0

    :cond_1
    move v2, v3

    .line 155
    goto :goto_1

    .line 162
    :pswitch_4
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/android/nfc/handover/HandoverManager;->mBluetoothHeadsetConnected:Z

    .line 163
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/android/nfc/handover/HandoverManager;->mBluetoothHeadsetPending:Z

    goto/16 :goto_0

    .line 166
    :pswitch_5
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/android/nfc/handover/HandoverManager;->mBluetoothEnabledByNfc:Z

    .line 167
    const-string v2, "NfcHandover"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MSG_BT_DISABLED_BY_NFC : set mBluetoothEnabledByNfc to "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/nfc/handover/HandoverManager$MessageHandler;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iget-boolean v5, v5, Lcom/android/nfc/handover/HandoverManager;->mBluetoothEnabledByNfc:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method
