.class public Lcom/android/nfc/handover/HandoverService;
.super Landroid/app/Service;
.source "HandoverService.java"

# interfaces
.implements Lcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;
.implements Lcom/android/nfc/handover/BluetoothPeripheralHandover$Callback;
.implements Lcom/android/nfc/handover/HandoverTransfer$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc/handover/HandoverService$MessageHandler;,
        Lcom/android/nfc/handover/HandoverService$Device;
    }
.end annotation


# static fields
.field static final ACTION_CANCEL_HANDOVER_TRANSFER:Ljava/lang/String; = "com.android.nfc.handover.action.CANCEL_HANDOVER_TRANSFER"

.field public static final ACTION_HANDOVER_STARTED:Ljava/lang/String; = "android.nfc.handover.intent.action.HANDOVER_STARTED"

.field public static final ACTION_TRANSFER_DONE:Ljava/lang/String; = "android.nfc.handover.intent.action.TRANSFER_DONE"

.field public static final ACTION_TRANSFER_PROGRESS:Ljava/lang/String; = "android.nfc.handover.intent.action.TRANSFER_PROGRESS"

.field static final BUNDLE_TRANSFER:Ljava/lang/String; = "transfer"

.field static final DBG:Z

.field public static final DIRECTION_INCOMING:I = 0x0

.field public static final DIRECTION_OUTGOING:I = 0x1

.field public static final EXTRA_ADDRESS:Ljava/lang/String; = "android.nfc.handover.intent.extra.ADDRESS"

.field static final EXTRA_COD:Ljava/lang/String; = "cod"

.field static final EXTRA_DEVICE:Ljava/lang/String; = "device"

.field public static final EXTRA_HANDOVER_DEVICE_TYPE:Ljava/lang/String; = "android.nfc.handover.intent.extra.HANDOVER_DEVICE_TYPE"

.field public static final EXTRA_INCOMING:Ljava/lang/String; = "com.android.nfc.handover.extra.INCOMING"

.field static final EXTRA_NAME:Ljava/lang/String; = "name"

.field public static final EXTRA_OBJECT_COUNT:Ljava/lang/String; = "android.nfc.handover.intent.extra.OBJECT_COUNT"

.field static final EXTRA_PERIPHERAL_TRANSPORT:Ljava/lang/String; = "transporttype"

.field public static final EXTRA_TRANSFER_DIRECTION:Ljava/lang/String; = "android.nfc.handover.intent.extra.TRANSFER_DIRECTION"

.field public static final EXTRA_TRANSFER_ID:Ljava/lang/String; = "android.nfc.handover.intent.extra.TRANSFER_ID"

.field public static final EXTRA_TRANSFER_MIMETYPE:Ljava/lang/String; = "android.nfc.handover.intent.extra.TRANSFER_MIME_TYPE"

.field public static final EXTRA_TRANSFER_PROGRESS:Ljava/lang/String; = "android.nfc.handover.intent.extra.TRANSFER_PROGRESS"

.field public static final EXTRA_TRANSFER_STATUS:Ljava/lang/String; = "android.nfc.handover.intent.extra.TRANSFER_STATUS"

.field public static final EXTRA_TRANSFER_URI:Ljava/lang/String; = "android.nfc.handover.intent.extra.TRANSFER_URI"

.field public static final HANDOVER_STATUS_PERMISSION:Ljava/lang/String; = "android.permission.NFC_HANDOVER_STATUS"

.field public static final HANDOVER_TRANSFER_STATUS_FAILURE:I = 0x1

.field static final HANDOVER_TRANSFER_STATUS_FAILURE_SDCARD_FULL:I = 0x2

.field public static final HANDOVER_TRANSFER_STATUS_SUCCESS:I = 0x0

.field static final MSG_DEREGISTER_CLIENT:I = 0x1

.field static final MSG_DEVICE_HANDOVER:I = 0x4

.field static final MSG_PAUSE_POLLING:I = 0x5

.field static final MSG_REGISTER_CLIENT:I = 0x0

.field static final MSG_START_INCOMING_TRANSFER:I = 0x2

.field static final MSG_START_OUTGOING_TRANSFER:I = 0x3

.field public static final PAUSE_DELAY_MILLIS:I = 0x12c

.field private static final PAUSE_POLLING_TIMEOUT_MS:I = 0x88b8

.field public static final PROFILE_A2DP:I = 0x1

.field public static final PROFILE_HEADSET:I = 0x0

.field public static final PROFILE_HID:I = 0x3

.field public static final PROFILE_NAP:I = 0x5

.field public static final PROFILE_OPP:I = 0x2

.field public static final PROFILE_PANU:I = 0x4

.field static final TAG:Ljava/lang/String; = "HandoverService"


# instance fields
.field isCancelled:I

.field mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field mBluetoothEnabledByNfc:Z

.field mBluetoothHandoverOngoing:Z

.field mBluetoothHeadsetConnected:Z

.field mBluetoothInputDeviceConnected:Z

.field mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

.field mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

.field final mBluetoothTransfers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/android/nfc/handover/HandoverTransfer;",
            ">;"
        }
    .end annotation
.end field

.field mBtClass:I

.field mClient:Landroid/os/Messenger;

.field mDevice:Landroid/bluetooth/BluetoothDevice;

.field mHandler:Landroid/os/Handler;

.field final mHandoverStatusReceiver:Landroid/content/BroadcastReceiver;

.field final mMessenger:Landroid/os/Messenger;

.field mName:Ljava/lang/String;

.field mNfcAdapter:Landroid/nfc/NfcAdapter;

.field final mPendingOutTransfers:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/android/nfc/handover/BluetoothOppHandover;",
            ">;"
        }
    .end annotation
.end field

.field mSoundPool:Landroid/media/SoundPool;

.field mSuccessSound:I

.field mTransport:I

.field private mWifiTransfer:Lcom/android/nfc/handover/HandoverTransfer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget-boolean v0, Lcom/android/nfc/handover/HandoverManager;->DBG:Z

    sput-boolean v0, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 328
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 275
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/nfc/handover/HandoverService;->isCancelled:I

    .line 309
    new-instance v0, Lcom/android/nfc/handover/HandoverService$1;

    invoke-direct {v0, p0}, Lcom/android/nfc/handover/HandoverService$1;-><init>(Lcom/android/nfc/handover/HandoverService;)V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mHandoverStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 329
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 330
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mPendingOutTransfers:Ljava/util/Queue;

    .line 331
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    .line 332
    new-instance v0, Lcom/android/nfc/handover/HandoverService$MessageHandler;

    invoke-direct {v0, p0}, Lcom/android/nfc/handover/HandoverService$MessageHandler;-><init>(Lcom/android/nfc/handover/HandoverService;)V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    .line 333
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mMessenger:Landroid/os/Messenger;

    .line 334
    iput-boolean v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHeadsetConnected:Z

    .line 335
    iput-boolean v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceConnected:Z

    .line 336
    iput-boolean v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothEnabledByNfc:Z

    .line 337
    return-void
.end method

.method static synthetic access$000(Lcom/android/nfc/handover/HandoverService;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/nfc/handover/HandoverService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/nfc/handover/HandoverService;->handleBluetoothStateChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/nfc/handover/HandoverService;Landroid/content/Intent;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/nfc/handover/HandoverService;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/android/nfc/handover/HandoverService;->handleCancelTransfer(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/nfc/handover/HandoverService;Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/nfc/handover/HandoverService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/Intent;
    .param p3, "x3"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/android/nfc/handover/HandoverService;->handleTransferEvent(Landroid/content/Context;Landroid/content/Intent;I)V

    return-void
.end method

.method private doesClassMatch(II)Z
    .locals 5
    .param p1, "cod"    # I
    .param p2, "profile"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 822
    and-int/lit16 p1, p1, 0x1ffc

    .line 823
    sget-boolean v2, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "HandoverService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doesClassMatch : cod is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    :cond_0
    if-ne p2, v0, :cond_2

    .line 829
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 872
    :cond_1
    :goto_0
    :sswitch_0
    return v0

    .line 838
    :cond_2
    if-nez p2, :cond_3

    .line 840
    sparse-switch p1, :sswitch_data_1

    move v0, v1

    .line 846
    goto :goto_0

    .line 848
    :cond_3
    const/4 v2, 0x2

    if-ne p2, v2, :cond_4

    .line 849
    sparse-switch p1, :sswitch_data_2

    move v0, v1

    .line 865
    goto :goto_0

    .line 867
    :cond_4
    const/4 v2, 0x3

    if-ne p2, v2, :cond_5

    .line 868
    and-int/lit16 v2, p1, 0x500

    const/16 v3, 0x500

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0

    .line 869
    :cond_5
    const/4 v2, 0x4

    if-eq p2, v2, :cond_6

    const/4 v2, 0x5

    if-ne p2, v2, :cond_7

    .line 870
    :cond_6
    and-int/lit16 v2, p1, 0x300

    const/16 v3, 0x300

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v1

    .line 872
    goto :goto_0

    .line 829
    nop

    :sswitch_data_0
    .sparse-switch
        0x414 -> :sswitch_0
        0x418 -> :sswitch_0
        0x420 -> :sswitch_0
        0x428 -> :sswitch_0
    .end sparse-switch

    .line 840
    :sswitch_data_1
    .sparse-switch
        0x404 -> :sswitch_0
        0x408 -> :sswitch_0
        0x420 -> :sswitch_0
    .end sparse-switch

    .line 849
    :sswitch_data_2
    .sparse-switch
        0x100 -> :sswitch_0
        0x104 -> :sswitch_0
        0x108 -> :sswitch_0
        0x10c -> :sswitch_0
        0x110 -> :sswitch_0
        0x114 -> :sswitch_0
        0x118 -> :sswitch_0
        0x200 -> :sswitch_0
        0x204 -> :sswitch_0
        0x208 -> :sswitch_0
        0x20c -> :sswitch_0
        0x210 -> :sswitch_0
        0x214 -> :sswitch_0
    .end sparse-switch
.end method

.method private handleBluetoothStateChanged(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 683
    const-string v1, "android.bluetooth.adapter.extra.STATE"

    const/high16 v2, -0x80000000

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 685
    .local v0, "state":I
    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    .line 687
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    invoke-virtual {v1}, Lcom/android/nfc/handover/BluetoothPeripheralHandover;->hasStarted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 689
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    invoke-virtual {v1}, Lcom/android/nfc/handover/BluetoothPeripheralHandover;->start()Z

    move-result v1

    if-nez v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->resumePolling()V

    .line 694
    :cond_0
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    invoke-virtual {v1}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->hasStarted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 696
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    invoke-virtual {v1}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->start()V

    .line 700
    :cond_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->startPendingTransfers()V

    .line 707
    :cond_2
    :goto_0
    return-void

    .line 701
    :cond_3
    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    .line 704
    iput-boolean v3, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHeadsetConnected:Z

    .line 705
    iput-boolean v3, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceConnected:Z

    goto :goto_0
.end method

.method private handleCancelTransfer(Landroid/content/Intent;I)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "deviceType"    # I

    .prologue
    const/4 v5, -0x1

    .line 665
    const-string v4, "android.nfc.handover.intent.extra.ADDRESS"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 666
    .local v2, "sourceAddress":Ljava/lang/String;
    const-string v4, "com.android.nfc.handover.extra.INCOMING"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 668
    .local v0, "direction":I
    if-ne v0, v5, :cond_1

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 672
    :cond_1
    if-nez v0, :cond_3

    const/4 v1, 0x1

    .line 673
    .local v1, "incoming":Z
    :goto_1
    invoke-virtual {p0, v2, v1}, Lcom/android/nfc/handover/HandoverService;->findHandoverTransfer(Ljava/lang/String;Z)Lcom/android/nfc/handover/HandoverTransfer;

    move-result-object v3

    .line 675
    .local v3, "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    if-eqz v3, :cond_0

    .line 676
    sget-boolean v4, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v4, :cond_2

    const-string v4, "HandoverService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cancelling transfer "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/android/nfc/handover/HandoverTransfer;->mTransferId:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    :cond_2
    invoke-virtual {v3}, Lcom/android/nfc/handover/HandoverTransfer;->getTransferId()I

    move-result v4

    iput v4, p0, Lcom/android/nfc/handover/HandoverService;->isCancelled:I

    .line 678
    invoke-virtual {v3}, Lcom/android/nfc/handover/HandoverTransfer;->cancel()V

    goto :goto_0

    .line 672
    .end local v1    # "incoming":Z
    .end local v3    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private handleTransferEvent(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "deviceType"    # I

    .prologue
    .line 585
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 586
    .local v3, "action":Ljava/lang/String;
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_DIRECTION"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 587
    .local v6, "direction":I
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_ID"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 588
    .local v8, "id":I
    const-string v17, "android.nfc.handover.intent.action.HANDOVER_STARTED"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 590
    const/4 v6, 0x0

    .line 592
    :cond_0
    const-string v17, "android.nfc.handover.intent.extra.ADDRESS"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 594
    .local v13, "sourceAddress":Ljava/lang/String;
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v6, v0, :cond_1

    if-nez v13, :cond_2

    .line 662
    :cond_1
    :goto_0
    return-void

    .line 595
    :cond_2
    if-nez v6, :cond_4

    const/4 v9, 0x1

    .line 597
    .local v9, "incoming":Z
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v9}, Lcom/android/nfc/handover/HandoverService;->findHandoverTransfer(Ljava/lang/String;Z)Lcom/android/nfc/handover/HandoverTransfer;

    move-result-object v14

    .line 599
    .local v14, "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    if-nez v14, :cond_5

    .line 602
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_1

    .line 603
    const/16 v17, 0x1

    move/from16 v0, p3

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 604
    sget-boolean v17, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v17, :cond_3

    const-string v17, "HandoverService"

    const-string v18, "Didn\'t find transfer, stopping"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :cond_3
    new-instance v4, Landroid/content/Intent;

    const-string v17, "android.btopp.intent.action.STOP_HANDOVER_TRANSFER"

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 607
    .local v4, "cancelIntent":Landroid/content/Intent;
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_ID"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 608
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/nfc/handover/HandoverService;->sendBroadcast(Landroid/content/Intent;)V

    .line 609
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/nfc/handover/HandoverService;->isCancelled:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 610
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/nfc/handover/HandoverService;->isCancelled:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/nfc/handover/HandoverService;->notifyClientTransferComplete(I)V

    .line 611
    const/16 v17, -0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/nfc/handover/HandoverService;->isCancelled:I

    goto :goto_0

    .line 595
    .end local v4    # "cancelIntent":Landroid/content/Intent;
    .end local v9    # "incoming":Z
    .end local v14    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    :cond_4
    const/4 v9, 0x0

    goto :goto_1

    .line 618
    .restart local v9    # "incoming":Z
    .restart local v14    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    :cond_5
    invoke-virtual {v14, v8}, Lcom/android/nfc/handover/HandoverTransfer;->setBluetoothTransferId(I)V

    .line 620
    const-string v17, "android.nfc.handover.intent.action.TRANSFER_DONE"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 621
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_STATUS"

    const/16 v18, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 623
    .local v7, "handoverStatus":I
    if-nez v7, :cond_8

    .line 624
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_URI"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 625
    .local v16, "uriString":Ljava/lang/String;
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_MIME_TYPE"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 626
    .local v11, "mimeType":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 627
    .local v15, "uri":Landroid/net/Uri;
    sget-boolean v17, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v17, :cond_6

    .line 628
    const-string v17, "HandoverService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "uriSring: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " MimeType : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    const-string v17, "HandoverService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "uri.toString() : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "uri.getPath() : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    :cond_6
    if-eqz v15, :cond_7

    invoke-virtual {v15}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v17

    if-nez v17, :cond_7

    .line 637
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v15

    .line 640
    :cond_7
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0, v15, v11}, Lcom/android/nfc/handover/HandoverTransfer;->finishTransfer(ZLandroid/net/Uri;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 641
    .end local v11    # "mimeType":Ljava/lang/String;
    .end local v15    # "uri":Landroid/net/Uri;
    .end local v16    # "uriString":Ljava/lang/String;
    :cond_8
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v7, v0, :cond_a

    .line 643
    sget-boolean v17, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v17, :cond_9

    const-string v17, "HandoverService"

    const-string v18, "Displays popup because there\'re not enough memory in phone."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_9
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v14, v0, v1, v2}, Lcom/android/nfc/handover/HandoverTransfer;->finishTransfer(ZLandroid/net/Uri;Ljava/lang/String;)V

    .line 647
    new-instance v10, Landroid/content/Intent;

    const-string v17, "com.android.nfc.AndroidBeamPopUp"

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 648
    .local v10, "memoryPopupIntent":Landroid/content/Intent;
    const-string v17, "POPUP_MODE"

    const-string v18, "disk_full"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 649
    sget-object v17, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v10, v1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/16 :goto_0

    .line 651
    .end local v10    # "memoryPopupIntent":Landroid/content/Intent;
    :cond_a
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v14, v0, v1, v2}, Lcom/android/nfc/handover/HandoverTransfer;->finishTransfer(ZLandroid/net/Uri;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 653
    .end local v7    # "handoverStatus":I
    :cond_b
    const-string v17, "android.nfc.handover.intent.action.TRANSFER_PROGRESS"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 654
    const-string v17, "android.nfc.handover.intent.extra.TRANSFER_PROGRESS"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v12

    .line 655
    .local v12, "progress":F
    invoke-virtual {v14, v12}, Lcom/android/nfc/handover/HandoverTransfer;->updateFileProgress(F)V

    goto/16 :goto_0

    .line 656
    .end local v12    # "progress":F
    :cond_c
    const-string v17, "android.nfc.handover.intent.action.HANDOVER_STARTED"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 657
    const-string v17, "android.nfc.handover.intent.extra.OBJECT_COUNT"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 658
    .local v5, "count":I
    if-lez v5, :cond_1

    .line 659
    invoke-virtual {v14, v5}, Lcom/android/nfc/handover/HandoverTransfer;->setObjectCount(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method createHandoverTransfer(Lcom/android/nfc/handover/PendingHandoverTransfer;)V
    .locals 5
    .param p1, "pendingTransfer"    # Lcom/android/nfc/handover/PendingHandoverTransfer;

    .prologue
    .line 528
    iget v2, p1, Lcom/android/nfc/handover/PendingHandoverTransfer;->deviceType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 529
    iget-object v2, p1, Lcom/android/nfc/handover/PendingHandoverTransfer;->remoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "macAddress":Ljava/lang/String;
    iget-boolean v2, p1, Lcom/android/nfc/handover/PendingHandoverTransfer;->incoming:Z

    invoke-virtual {p0, v0, v2, p1}, Lcom/android/nfc/handover/HandoverService;->maybeCreateHandoverTransfer(Ljava/lang/String;ZLcom/android/nfc/handover/PendingHandoverTransfer;)Lcom/android/nfc/handover/HandoverTransfer;

    move-result-object v1

    .line 537
    .local v1, "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    if-eqz v1, :cond_0

    .line 538
    invoke-virtual {v1}, Lcom/android/nfc/handover/HandoverTransfer;->updateNotification()V

    .line 540
    .end local v0    # "macAddress":Ljava/lang/String;
    .end local v1    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    const-string v2, "HandoverService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid device type ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/android/nfc/handover/PendingHandoverTransfer;->deviceType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] received."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method disableBluetoothIfNeeded()V
    .locals 7

    .prologue
    .line 497
    iget-boolean v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothEnabledByNfc:Z

    if-nez v4, :cond_1

    .line 522
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    const-string v4, "HandoverService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mBluetoothTransfers.size(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mBluetoothHeadsetConnected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHeadsetConnected:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mBluetoothInputDeviceConnected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceConnected:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    iget-object v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHeadsetConnected:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceConnected:Z

    if-nez v4, :cond_0

    .line 502
    iget-object v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    .line 503
    .local v1, "devices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 504
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 505
    const-string v4, "HandoverService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "device "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is connected !!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 509
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_3
    iget-object v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 510
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothEnabledByNfc:Z

    .line 511
    iget-object v4, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    if-eqz v4, :cond_0

    .line 512
    const/4 v4, 0x0

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 514
    .local v3, "msg":Landroid/os/Message;
    :try_start_0
    const-string v4, "HandoverService"

    const-string v5, " disableBluetoothIfNeeded : send message"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v4, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    invoke-virtual {v4, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 516
    :catch_0
    move-exception v4

    goto/16 :goto_0
.end method

.method doCheckDevice(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 411
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 412
    .local v0, "msgData":Landroid/os/Bundle;
    const-string v1, "device"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    iput-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 413
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mName:Ljava/lang/String;

    .line 414
    const-string v1, "cod"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    .line 415
    const-string v1, "transporttype"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/nfc/handover/HandoverService;->mTransport:I

    .line 417
    iget-boolean v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    if-eqz v1, :cond_0

    .line 419
    const-string v1, "HandoverService"

    const-string v2, "Ignoring pairing request, existing handover in progress."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :goto_0
    return-void

    .line 422
    :cond_0
    iput-boolean v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    .line 424
    sget-boolean v1, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v1, :cond_1

    .line 425
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_3

    .line 426
    const-string v1, "HandoverService"

    const-string v2, "device is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_1
    :goto_1
    iget v1, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/android/nfc/handover/HandoverService;->doesClassMatch(II)Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    invoke-direct {p0, v1, v4}, Lcom/android/nfc/handover/HandoverService;->doesClassMatch(II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 432
    :cond_2
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mName:Ljava/lang/String;

    iget v3, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    iget v4, p0, Lcom/android/nfc/handover/HandoverService;->mTransport:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/nfc/handover/HandoverService;->doHeadsetHandover(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;II)V

    goto :goto_0

    .line 428
    :cond_3
    const-string v1, "HandoverService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/nfc/handover/HandoverService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/nfc/handover/HandoverService;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cod : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", transport : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/nfc/handover/HandoverService;->mTransport:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 433
    :cond_4
    iget v1, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2}, Lcom/android/nfc/handover/HandoverService;->doesClassMatch(II)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 434
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mName:Ljava/lang/String;

    iget v3, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/nfc/handover/HandoverService;->doInputDeviceHandover(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;I)V

    goto :goto_0

    .line 436
    :cond_5
    const-string v1, "HandoverService"

    const-string v2, "The device is not a headset or a hid, but try to pair"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mName:Ljava/lang/String;

    iget v3, p0, Lcom/android/nfc/handover/HandoverService;->mBtClass:I

    iget v4, p0, Lcom/android/nfc/handover/HandoverService;->mTransport:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/nfc/handover/HandoverService;->doHeadsetHandover(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method doHeadsetHandover(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;II)V
    .locals 7
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "cod"    # I
    .param p4, "transport"    # I

    .prologue
    .line 443
    new-instance v0, Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/nfc/handover/BluetoothPeripheralHandover;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;IILcom/android/nfc/handover/BluetoothPeripheralHandover$Callback;)V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    .line 446
    const/4 v0, 0x2

    if-ne p4, v0, :cond_1

    .line 447
    sget-boolean v0, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "HandoverService"

    const-string v1, "The transport of this device is TRANSPORT_LE. So pause polling"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 452
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    invoke-virtual {v0}, Lcom/android/nfc/handover/BluetoothPeripheralHandover;->start()Z

    move-result v0

    if-nez v0, :cond_2

    .line 453
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->resumePolling()V

    .line 464
    :cond_2
    :goto_0
    return-void

    .line 458
    :cond_3
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->enableBluetooth()Z

    move-result v0

    if-nez v0, :cond_2

    .line 459
    const-string v0, "HandoverService"

    const-string v1, "Error enabling Bluetooth."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    .line 461
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    goto :goto_0
.end method

.method doIncomingTransfer(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 396
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 398
    .local v0, "msgData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 399
    const-string v2, "transfer"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/nfc/handover/PendingHandoverTransfer;

    .line 400
    .local v1, "pendingTransfer":Lcom/android/nfc/handover/PendingHandoverTransfer;
    iget v2, v1, Lcom/android/nfc/handover/PendingHandoverTransfer;->deviceType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->enableBluetooth()Z

    move-result v2

    if-nez v2, :cond_0

    .line 402
    const-string v2, "HandoverService"

    const-string v3, "Error enabling Bluetooth."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget v2, v1, Lcom/android/nfc/handover/PendingHandoverTransfer;->id:I

    invoke-virtual {p0, v2}, Lcom/android/nfc/handover/HandoverService;->notifyClientTransferComplete(I)V

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/nfc/handover/HandoverService;->createHandoverTransfer(Lcom/android/nfc/handover/PendingHandoverTransfer;)V

    goto :goto_0
.end method

.method doInputDeviceHandover(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;I)V
    .locals 6
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "cod"    # I

    .prologue
    .line 467
    new-instance v0, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;ILcom/android/nfc/handover/BluetoothInputDeviceHandover$Callback;)V

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    .line 469
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    invoke-virtual {v0}, Lcom/android/nfc/handover/BluetoothInputDeviceHandover;->start()V

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->enableBluetooth()Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    const-string v0, "HandoverService"

    const-string v1, "Error enabling Bluetooth."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    goto :goto_0
.end method

.method doOutgoingTransfer(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 366
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 368
    .local v1, "msgData":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 369
    const-string v3, "transfer"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/android/nfc/handover/PendingHandoverTransfer;

    .line 370
    .local v2, "pendingTransfer":Lcom/android/nfc/handover/PendingHandoverTransfer;
    invoke-virtual {p0, v2}, Lcom/android/nfc/handover/HandoverService;->createHandoverTransfer(Lcom/android/nfc/handover/PendingHandoverTransfer;)V

    .line 372
    iget v3, v2, Lcom/android/nfc/handover/PendingHandoverTransfer;->deviceType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 375
    new-instance v0, Lcom/android/nfc/handover/BluetoothOppHandover;

    iget-object v3, v2, Lcom/android/nfc/handover/PendingHandoverTransfer;->remoteDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v4, v2, Lcom/android/nfc/handover/PendingHandoverTransfer;->uris:[Landroid/net/Uri;

    iget-boolean v5, v2, Lcom/android/nfc/handover/PendingHandoverTransfer;->remoteActivating:Z

    invoke-direct {v0, p0, v3, v4, v5}, Lcom/android/nfc/handover/BluetoothOppHandover;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;[Landroid/net/Uri;Z)V

    .line 378
    .local v0, "handover":Lcom/android/nfc/handover/BluetoothOppHandover;
    iget-object v3, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 380
    invoke-virtual {v0}, Lcom/android/nfc/handover/BluetoothOppHandover;->start()V

    .line 393
    .end local v0    # "handover":Lcom/android/nfc/handover/BluetoothOppHandover;
    :cond_0
    :goto_0
    return-void

    .line 382
    .restart local v0    # "handover":Lcom/android/nfc/handover/BluetoothOppHandover;
    :cond_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->enableBluetooth()Z

    move-result v3

    if-nez v3, :cond_2

    .line 383
    const-string v3, "HandoverService"

    const-string v4, "Error enabling Bluetooth."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget v3, v2, Lcom/android/nfc/handover/PendingHandoverTransfer;->id:I

    invoke-virtual {p0, v3}, Lcom/android/nfc/handover/HandoverService;->notifyClientTransferComplete(I)V

    goto :goto_0

    .line 387
    :cond_2
    sget-boolean v3, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v3, :cond_3

    const-string v3, "HandoverService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Queueing out transfer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/android/nfc/handover/PendingHandoverTransfer;->id:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_3
    iget-object v3, p0, Lcom/android/nfc/handover/HandoverService;->mPendingOutTransfers:Ljava/util/Queue;

    invoke-interface {v3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method enableBluetooth()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 489
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 490
    iput-boolean v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothEnabledByNfc:Z

    .line 491
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enableNoAutoConnect()Z

    move-result v0

    .line 493
    :cond_0
    return v0
.end method

.method findHandoverTransfer(Ljava/lang/String;Z)Lcom/android/nfc/handover/HandoverTransfer;
    .locals 3
    .param p1, "macAddress"    # Ljava/lang/String;
    .param p2, "incoming"    # Z

    .prologue
    .line 568
    new-instance v0, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 569
    .local v0, "key":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 570
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/nfc/handover/HandoverTransfer;

    .line 571
    .local v1, "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    invoke-virtual {v1}, Lcom/android/nfc/handover/HandoverTransfer;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 576
    .end local v1    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method maybeCreateHandoverTransfer(Ljava/lang/String;ZLcom/android/nfc/handover/PendingHandoverTransfer;)Lcom/android/nfc/handover/HandoverTransfer;
    .locals 3
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "incoming"    # Z
    .param p3, "pendingTransfer"    # Lcom/android/nfc/handover/PendingHandoverTransfer;

    .prologue
    .line 545
    new-instance v0, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 547
    .local v0, "key":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 548
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/nfc/handover/HandoverTransfer;

    .line 549
    .local v1, "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    invoke-virtual {v1}, Lcom/android/nfc/handover/HandoverTransfer;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 550
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    :goto_0
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 563
    :goto_1
    return-object v2

    .line 555
    :cond_0
    iget v2, p3, Lcom/android/nfc/handover/PendingHandoverTransfer;->id:I

    invoke-virtual {p0, v2}, Lcom/android/nfc/handover/HandoverService;->notifyClientTransferComplete(I)V

    .line 556
    const/4 v2, 0x0

    goto :goto_1

    .line 559
    .end local v1    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    :cond_1
    new-instance v1, Lcom/android/nfc/handover/HandoverTransfer;

    invoke-direct {v1, p0, p0, p3}, Lcom/android/nfc/handover/HandoverTransfer;-><init>(Landroid/content/Context;Lcom/android/nfc/handover/HandoverTransfer$Callback;Lcom/android/nfc/handover/PendingHandoverTransfer;)V

    .restart local v1    # "transfer":Lcom/android/nfc/handover/HandoverTransfer;
    goto :goto_0
.end method

.method notifyClientTransferComplete(I)V
    .locals 3
    .param p1, "transferId"    # I

    .prologue
    .line 710
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 711
    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 712
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 714
    :try_start_0
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 715
    .restart local v0    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onBluetoothInputDeviceHandoverComplete(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    const/4 v2, 0x0

    .line 804
    iput-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceHandover:Lcom/android/nfc/handover/BluetoothInputDeviceHandover;

    .line 805
    iput-boolean p1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothInputDeviceConnected:Z

    .line 806
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 807
    if-eqz p1, :cond_1

    const/4 v1, 0x3

    :goto_0
    invoke-static {v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 811
    .local v0, "msg":Landroid/os/Message;
    :try_start_0
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 816
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->disableBluetoothIfNeeded()V

    .line 817
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    .line 818
    return-void

    .line 807
    :cond_1
    const/4 v1, 0x4

    goto :goto_0

    .line 812
    .restart local v0    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public onBluetoothPeripheralHandoverComplete(Z)V
    .locals 8
    .param p1, "connected"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x5

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 767
    iget-object v5, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    iget v1, v5, Lcom/android/nfc/handover/BluetoothPeripheralHandover;->mTransport:I

    .line 768
    .local v1, "transport":I
    iput-object v7, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothPeripheralHandover:Lcom/android/nfc/handover/BluetoothPeripheralHandover;

    .line 769
    iput-boolean p1, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHeadsetConnected:Z

    .line 776
    if-ne v1, v2, :cond_1

    if-nez p1, :cond_1

    .line 777
    iget-object v5, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 778
    iget-object v5, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 783
    :cond_0
    iget-object v5, p0, Lcom/android/nfc/handover/HandoverService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v5}, Landroid/nfc/NfcAdapter;->resumePolling()V

    .line 786
    :cond_1
    iget-object v5, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    if-eqz v5, :cond_3

    .line 787
    if-eqz p1, :cond_2

    move v2, v3

    :cond_2
    invoke-static {v7, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 790
    .local v0, "msg":Landroid/os/Message;
    iget-boolean v2, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothEnabledByNfc:Z

    if-eqz v2, :cond_4

    :goto_0
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 792
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    invoke-virtual {v2, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    .end local v0    # "msg":Landroid/os/Message;
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->disableBluetoothIfNeeded()V

    .line 798
    iput-boolean v4, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    .line 799
    return-void

    .restart local v0    # "msg":Landroid/os/Message;
    :cond_4
    move v3, v4

    .line 790
    goto :goto_0

    .line 793
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 341
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 343
    new-instance v1, Landroid/media/SoundPool;

    const/4 v2, 0x5

    invoke-direct {v1, v4, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mSoundPool:Landroid/media/SoundPool;

    .line 344
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mSoundPool:Landroid/media/SoundPool;

    const/high16 v2, 0x7f040000

    invoke-virtual {v1, p0, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/android/nfc/handover/HandoverService;->mSuccessSound:I

    .line 345
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 346
    iput-boolean v3, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothHandoverOngoing:Z

    .line 348
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.nfc.handover.intent.action.TRANSFER_DONE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 349
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.nfc.handover.intent.action.TRANSFER_PROGRESS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 350
    const-string v1, "com.android.nfc.handover.action.CANCEL_HANDOVER_TRANSFER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 351
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 352
    const-string v1, "android.nfc.handover.intent.action.HANDOVER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mHandoverStatusReceiver:Landroid/content/BroadcastReceiver;

    const-string v2, "android.permission.NFC_HANDOVER_STATUS"

    iget-object v3, p0, Lcom/android/nfc/handover/HandoverService;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/android/nfc/handover/HandoverService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 354
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 358
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 359
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mHandoverStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/HandoverService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 363
    return-void
.end method

.method public onTransferComplete(Lcom/android/nfc/handover/HandoverTransfer;Z)V
    .locals 10
    .param p1, "transfer"    # Lcom/android/nfc/handover/HandoverTransfer;
    .param p2, "success"    # Z

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 733
    monitor-enter p0

    .line 734
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mWifiTransfer:Lcom/android/nfc/handover/HandoverTransfer;

    if-ne v0, p1, :cond_0

    .line 735
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mWifiTransfer:Lcom/android/nfc/handover/HandoverTransfer;

    .line 737
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 739
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mWifiTransfer:Lcom/android/nfc/handover/HandoverTransfer;

    if-nez v0, :cond_2

    .line 740
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mBluetoothTransfers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 741
    .local v8, "it":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 743
    .local v7, "hashPair":Ljava/util/Map$Entry;
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/nfc/handover/HandoverTransfer;

    .line 744
    .local v9, "transferEntry":Lcom/android/nfc/handover/HandoverTransfer;
    if-ne v9, p1, :cond_1

    .line 745
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 737
    .end local v7    # "hashPair":Ljava/util/Map$Entry;
    .end local v8    # "it":Ljava/util/Iterator;
    .end local v9    # "transferEntry":Lcom/android/nfc/handover/HandoverTransfer;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 751
    :cond_2
    iget v0, p0, Lcom/android/nfc/handover/HandoverService;->isCancelled:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 752
    invoke-virtual {p1}, Lcom/android/nfc/handover/HandoverTransfer;->getTransferId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/nfc/handover/HandoverService;->notifyClientTransferComplete(I)V

    .line 755
    :cond_3
    if-eqz p2, :cond_5

    .line 756
    iget-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/android/nfc/handover/HandoverService;->mSuccessSound:I

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 761
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/android/nfc/handover/HandoverService;->disableBluetoothIfNeeded()V

    .line 762
    return-void

    .line 758
    :cond_5
    sget-boolean v0, Lcom/android/nfc/handover/HandoverService;->DBG:Z

    if-eqz v0, :cond_4

    const-string v0, "HandoverService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transfer failed, final state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/android/nfc/handover/HandoverTransfer;->mState:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 724
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/handover/HandoverService;->mClient:Landroid/os/Messenger;

    .line 725
    const/4 v0, 0x0

    return v0
.end method

.method startPendingTransfers()V
    .locals 2

    .prologue
    .line 482
    :goto_0
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mPendingOutTransfers:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 483
    iget-object v1, p0, Lcom/android/nfc/handover/HandoverService;->mPendingOutTransfers:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/nfc/handover/BluetoothOppHandover;

    .line 484
    .local v0, "handover":Lcom/android/nfc/handover/BluetoothOppHandover;
    invoke-virtual {v0}, Lcom/android/nfc/handover/BluetoothOppHandover;->start()V

    goto :goto_0

    .line 486
    .end local v0    # "handover":Lcom/android/nfc/handover/BluetoothOppHandover;
    :cond_0
    return-void
.end method
