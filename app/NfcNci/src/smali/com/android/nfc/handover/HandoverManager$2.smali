.class Lcom/android/nfc/handover/HandoverManager$2;
.super Landroid/content/BroadcastReceiver;
.source "HandoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/handover/HandoverManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/handover/HandoverManager;


# direct methods
.method constructor <init>(Lcom/android/nfc/handover/HandoverManager;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/android/nfc/handover/HandoverManager$2;->this$0:Lcom/android/nfc/handover/HandoverManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v4, 0x64

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 251
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 253
    const-string v3, "android.intent.extra.user_handle"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 254
    .local v2, "userId":I
    if-lt v2, v4, :cond_1

    .line 255
    sget-boolean v3, Lcom/android/nfc/handover/HandoverManager;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "NfcHandover"

    const-string v4, "It\'s KNOX Mode. So still binding the service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    .end local v2    # "userId":I
    :cond_0
    :goto_0
    return-void

    .line 256
    .restart local v2    # "userId":I
    :cond_1
    if-nez v2, :cond_2

    const-string v3, "old_user_id"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-lt v3, v4, :cond_2

    .line 257
    sget-boolean v3, Lcom/android/nfc/handover/HandoverManager;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "NfcHandover"

    const-string v4, "It\'s From KNOX to OWNER Mode. So still binding the service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 259
    :cond_2
    sget-boolean v3, Lcom/android/nfc/handover/HandoverManager;->DBG:Z

    if-eqz v3, :cond_3

    const-string v3, "NfcHandover"

    const-string v4, "It\'s AOSP Mode. So unbinding the service"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_3
    iget-object v3, p0, Lcom/android/nfc/handover/HandoverManager$2;->this$0:Lcom/android/nfc/handover/HandoverManager;

    invoke-virtual {v3, v6}, Lcom/android/nfc/handover/HandoverManager;->unbindServiceIfNeededLocked(Z)V

    goto :goto_0

    .line 263
    .end local v2    # "userId":I
    :cond_4
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 264
    const-string v3, "android.bluetooth.adapter.extra.STATE"

    const/high16 v4, -0x80000000

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 265
    .local v1, "state":I
    const/16 v3, 0xa

    if-ne v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/nfc/handover/HandoverManager$2;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iget-boolean v3, v3, Lcom/android/nfc/handover/HandoverManager;->mBluetoothEnabledByNfc:Z

    if-ne v3, v6, :cond_0

    .line 266
    iget-object v3, p0, Lcom/android/nfc/handover/HandoverManager$2;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iput-boolean v5, v3, Lcom/android/nfc/handover/HandoverManager;->mBluetoothEnabledByNfc:Z

    .line 267
    const-string v3, "NfcHandover"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "set mBluetoothEnabledByNfc to : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/nfc/handover/HandoverManager$2;->this$0:Lcom/android/nfc/handover/HandoverManager;

    iget-boolean v5, v5, Lcom/android/nfc/handover/HandoverManager;->mBluetoothEnabledByNfc:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
