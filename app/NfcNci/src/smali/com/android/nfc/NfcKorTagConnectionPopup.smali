.class public Lcom/android/nfc/NfcKorTagConnectionPopup;
.super Landroid/app/Activity;
.source "NfcKorTagConnectionPopup.java"


# static fields
.field public static final PREF:Ljava/lang/String; = "NfcServicePrefs"

.field private static final PREF_DISABLE_DIALOG:Ljava/lang/String; = "disable_dialog"

.field private static final TAG:Ljava/lang/String; = "NfcKorTagConnectionPopup"


# instance fields
.field private mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

.field private mPopup:Landroid/app/AlertDialog$Builder;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mPrefsEditor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    .line 41
    iput-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method static synthetic access$000(Lcom/android/nfc/NfcKorTagConnectionPopup;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/android/nfc/NfcKorTagConnectionPopup;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/nfc/NfcKorTagConnectionPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/android/nfc/NfcKorTagConnectionPopup;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const-string v3, "NfcKorTagConnectionPopup"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const-string v3, "NfcServicePrefs"

    invoke-virtual {p0, v3, v7}, Lcom/android/nfc/NfcKorTagConnectionPopup;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPrefs:Landroid/content/SharedPreferences;

    .line 54
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 56
    invoke-virtual {p0}, Lcom/android/nfc/NfcKorTagConnectionPopup;->getApplicationContext()Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Lcom/android/nfc/NfcKorTagConnectionPopup;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 57
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 59
    .local v2, "v":Landroid/view/View;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    .line 60
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 61
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f070001

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 63
    const v3, 0x7f09000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 64
    .local v1, "textview":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/nfc/NfcKorTagConnectionPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070046

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/16 v6, 0x3c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    const v4, 0x7f07002c

    new-instance v5, Lcom/android/nfc/NfcKorTagConnectionPopup$1;

    invoke-direct {v5, p0, v2}, Lcom/android/nfc/NfcKorTagConnectionPopup$1;-><init>(Lcom/android/nfc/NfcKorTagConnectionPopup;Landroid/view/View;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/android/nfc/NfcKorTagConnectionPopup$2;

    invoke-direct {v4, p0, v2}, Lcom/android/nfc/NfcKorTagConnectionPopup$2;-><init>(Lcom/android/nfc/NfcKorTagConnectionPopup;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 100
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mPopup:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    .line 101
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 102
    iget-object v3, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 103
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 107
    const-string v0, "NfcKorTagConnectionPopup"

    const-string v1, " onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    .line 113
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 114
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 118
    const-string v0, "NfcKorTagConnectionPopup"

    const-string v1, " onPause"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/NfcKorTagConnectionPopup;->mNfckorTagConnectionPopup:Landroid/app/AlertDialog;

    .line 123
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 124
    invoke-virtual {p0}, Lcom/android/nfc/NfcKorTagConnectionPopup;->finish()V

    .line 125
    return-void
.end method
