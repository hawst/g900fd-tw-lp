.class public final Lcom/android/nfc/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Tag_Connection_Popup:I = 0x7f070046

.field public static final accessibility_nfc_enabled:I = 0x7f070006

.field public static final android_beam:I = 0x7f070050

.field public static final app_name:I = 0x7f070000

.field public static final askOption:I = 0x7f070047

.field public static final ask_nfc_tap:I = 0x7f070049

.field public static final beam_canceled:I = 0x7f07000c

.field public static final beam_complete:I = 0x7f07000a

.field public static final beam_failed:I = 0x7f07000b

.field public static final beam_handover_not_supported:I = 0x7f07000f

.field public static final beam_outgoing:I = 0x7f070009

.field public static final beam_progress:I = 0x7f070008

.field public static final beam_requires_nfc_enabled:I = 0x7f07004f

.field public static final beam_touch_to_view:I = 0x7f07000e

.field public static final beam_try_again:I = 0x7f070010

.field public static final browser_title:I = 0x7f07001c

.field public static final browser_warning_message:I = 0x7f07001d

.field public static final btn_ok:I = 0x7f07002c

.field public static final cancel:I = 0x7f07000d

.field public static final chn_enable_popup_contents:I = 0x7f07002f

.field public static final complete_with:I = 0x7f070035

.field public static final confirm_pairing:I = 0x7f070019

.field public static final connect_headset_failed:I = 0x7f070013

.field public static final connect_peripheral_failed:I = 0x7f070053

.field public static final connected_headset:I = 0x7f070012

.field public static final connected_peripheral:I = 0x7f070052

.field public static final connecting_headset:I = 0x7f070011

.field public static final connecting_peripheral:I = 0x7f070051

.field public static final contact_title:I = 0x7f07001e

.field public static final contact_warning_message:I = 0x7f07001f

.field public static final could_not_use_app:I = 0x7f070033

.field public static final default_pay_app_removed:I = 0x7f070036

.field public static final default_pay_app_removed_title:I = 0x7f070037

.field public static final device_storage_full_popup_label:I = 0x7f070027

.field public static final disconnected_headset:I = 0x7f070015

.field public static final disconnected_peripheral:I = 0x7f070055

.field public static final disconnecting_headset:I = 0x7f070014

.field public static final disconnecting_peripheral:I = 0x7f070054

.field public static final disk_full:I = 0x7f07002e

.field public static final error:I = 0x7f070022

.field public static final failed_to_enable_bt:I = 0x7f070018

.field public static final filepath_null_popup:I = 0x7f070028

.field public static final frem_drm:I = 0x7f07002a

.field public static final from_cloud:I = 0x7f070029

.field public static final full_dialig_title_dt:I = 0x7f070040

.field public static final full_dialog_button_dt:I = 0x7f070042

.field public static final full_dialog_contents_dt:I = 0x7f070041

.field public static final gsma_enable_popup:I = 0x7f070045

.field public static final inbound_me_profile_text:I = 0x7f070003

.field public static final inbound_me_profile_title:I = 0x7f070002

.field public static final invalid_popup:I = 0x7f070020

.field public static final never_ask_again:I = 0x7f070023

.field public static final nfcUserLabel:I = 0x7f070001

.field public static final no_files_selected_popup_label:I = 0x7f070026

.field public static final not_save:I = 0x7f07002b

.field public static final outbound_me_profile_text:I = 0x7f070005

.field public static final outbound_me_profile_title:I = 0x7f070004

.field public static final pair_no:I = 0x7f07001b

.field public static final pair_yes:I = 0x7f07001a

.field public static final pairing_headset:I = 0x7f070016

.field public static final pairing_headset_failed:I = 0x7f070017

.field public static final pairing_peripheral:I = 0x7f070056

.field public static final pairing_peripheral_failed:I = 0x7f070057

.field public static final pay_with:I = 0x7f070034

.field public static final popup_label:I = 0x7f070024

.field public static final prompt_connect_to_network:I = 0x7f07004e

.field public static final route_table_advanced_settings:I = 0x7f070039

.field public static final route_table_advanced_settings_vzw:I = 0x7f07003d

.field public static final route_table_alert_explain:I = 0x7f07003a

.field public static final route_table_alert_explain_nfc_and_sharing:I = 0x7f07003b

.field public static final route_table_alert_explain_vzw:I = 0x7f07003f

.field public static final route_table_ignore:I = 0x7f070038

.field public static final route_table_title:I = 0x7f07003c

.field public static final route_table_title_vzw:I = 0x7f07003e

.field public static final sbeam_not_support:I = 0x7f070043

.field public static final status_unable_to_connect:I = 0x7f07004b

.field public static final status_wifi_connected:I = 0x7f07004c

.field public static final stms_appgroup:I = 0x7f070048

.field public static final stms_version:I = 0x7f07002d

.field public static final tap_again_to_complete:I = 0x7f070031

.field public static final tap_again_to_pay:I = 0x7f070030

.field public static final title_connect_to_network:I = 0x7f07004d

.field public static final touch:I = 0x7f070007

.field public static final transaction_failure:I = 0x7f070032

.field public static final unable_to_receive_file:I = 0x7f070044

.field public static final unable_to_share_file_popup_label:I = 0x7f070025

.field public static final unsupport_mifare_msg:I = 0x7f070021

.field public static final wifi_connect:I = 0x7f07004a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
