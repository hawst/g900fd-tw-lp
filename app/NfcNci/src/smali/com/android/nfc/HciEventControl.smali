.class public Lcom/android/nfc/HciEventControl;
.super Ljava/lang/Object;
.source "HciEventControl.java"


# static fields
.field private static final ADMIN_PERM:Ljava/lang/String; = "android.permission.WRITE_SECURE_SETTINGS"

.field private static final ADMIN_PERM_ERROR:Ljava/lang/String; = "WRITE_SECURE_SETTINGS permission required"

.field static final DBG:Z

.field private static final GSMA_ACTION_TRANSACTION_EVENT:Ljava/lang/String; = "com.gsma.services.nfc.action.TRANSACTION_EVENT"

.field private static final GSMA_NFC_CONTROLLER_PERMISSION:Ljava/lang/String; = "android.permission.NFC"

.field private static final GSMA_TRANSACTION_EXTRA_AID:Ljava/lang/String; = "com.gsma.services.nfc.extra.AID"

.field private static final GSMA_TRANSACTION_EXTRA_DATA:Ljava/lang/String; = "com.gsma.services.nfc.extra.DATA"

.field private static final GSMA_TRANSACTION_PERMISSION:Ljava/lang/String; = "com.gsma.services.nfc.permission.TRANSACTION_EVENT"

.field public static final READER_ESE:I = 0x1

.field public static final READER_SIM:I = 0x0

.field static final TAG:Ljava/lang/String; = "NfcServiceHciEventControl"


# instance fields
.field private ACTION_TRANSACTION_EVENT:Ljava/lang/String;

.field READER_ESE_S:Ljava/lang/String;

.field READER_SIM_S:Ljava/lang/String;

.field private TRANSACTION_EXTRA_AID:Ljava/lang/String;

.field private TRANSACTION_EXTRA_DATA:Ljava/lang/String;

.field private TRANSACTION_PERMISSION:Ljava/lang/String;

.field private isGsma_v40_HciSupported:Z

.field private isIsisSupported:Z

.field private mBindSmartcardServiceSuccess:Z

.field private mContext:Landroid/content/Context;

.field private final mIActivityManager:Landroid/app/IActivityManager;

.field private mOverrideFilters:[Landroid/content/IntentFilter;

.field private mOverrideIntent:Landroid/app/PendingIntent;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private multEvt:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final smartcardConnection:Landroid/content/ServiceConnection;

.field private volatile smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    sget-boolean v0, Lcom/android/nfc/NfcService;->DBG:Z

    sput-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-string v1, "android.permission.NFC_TRANSACTION"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    .line 72
    const-string v1, "android.nfc.action.TRANSACTION_DETECTED"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->ACTION_TRANSACTION_EVENT:Ljava/lang/String;

    .line 73
    const-string v1, "android.nfc.extra.AID"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_AID:Ljava/lang/String;

    .line 74
    const-string v1, "android.nfc.extra.DATA"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_DATA:Ljava/lang/String;

    .line 84
    const-string v1, "SIM: UICC"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    .line 85
    const-string v1, "eSE: SmartMX"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    .line 88
    iput-boolean v2, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    .line 94
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->multEvt:Ljava/util/HashMap;

    .line 95
    iput-boolean v2, p0, Lcom/android/nfc/HciEventControl;->isGsma_v40_HciSupported:Z

    .line 96
    iput-boolean v2, p0, Lcom/android/nfc/HciEventControl;->isIsisSupported:Z

    .line 98
    new-instance v1, Lcom/android/nfc/HciEventControl$1;

    invoke-direct {v1, p0}, Lcom/android/nfc/HciEventControl$1;-><init>(Lcom/android/nfc/HciEventControl;)V

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->smartcardConnection:Landroid/content/ServiceConnection;

    .line 115
    iput-object p1, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    .line 116
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->bindSmartcardService()V

    .line 117
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->mIActivityManager:Landroid/app/IActivityManager;

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 120
    const-string v0, "3.1.0"

    .line 121
    .local v0, "version":Ljava/lang/String;
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 122
    const-string v1, "SIM - UICC"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    .line 123
    const-string v1, "eSE - SmartMX"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    .line 125
    :cond_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_NFC_SetSecureEventType"

    const-string v3, "GSMA_v40"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GSMA_v40"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/nfc/HciEventControl;->isGsma_v40_HciSupported:Z

    .line 127
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_NFC_SetSecureEventType"

    const-string v3, "GSMA_v40"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ISIS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/nfc/HciEventControl;->isIsisSupported:Z

    .line 129
    iget-boolean v1, p0, Lcom/android/nfc/HciEventControl;->isGsma_v40_HciSupported:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/nfc/HciEventControl;->isIsisSupported:Z

    if-eqz v1, :cond_2

    .line 130
    :cond_1
    const-string v1, "com.gsma.services.nfc.permission.TRANSACTION_EVENT"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    .line 131
    const-string v1, "com.gsma.services.nfc.action.TRANSACTION_EVENT"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->ACTION_TRANSACTION_EVENT:Ljava/lang/String;

    .line 132
    const-string v1, "com.gsma.services.nfc.extra.AID"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_AID:Ljava/lang/String;

    .line 133
    const-string v1, "com.gsma.services.nfc.extra.DATA"

    iput-object v1, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_DATA:Ljava/lang/String;

    .line 135
    :cond_2
    return-void
.end method

.method static synthetic access$002(Lcom/android/nfc/HciEventControl;Lorg/simalliance/openmobileapi/service/ISmartcardService;)Lorg/simalliance/openmobileapi/service/ISmartcardService;
    .locals 0
    .param p0, "x0"    # Lcom/android/nfc/HciEventControl;
    .param p1, "x1"    # Lorg/simalliance/openmobileapi/service/ISmartcardService;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/nfc/HciEventControl;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/nfc/HciEventControl;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method private bindSmartcardService()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 266
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 267
    .local v0, "intent":Landroid/content/Intent;
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "bindSmartcardService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    const-string v1, "org.simalliance.openmobileapi.service"

    const-string v2, "org.simalliance.openmobileapi.service.SmartcardService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    const-string v1, "org.simalliance.openmobileapi.service.ISmartcardService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    iget-object v1, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->smartcardConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 273
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "bindService success!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_1
    iput-boolean v3, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    .line 279
    :goto_0
    return-void

    .line 276
    :cond_2
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_3

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "bindService failed!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    goto :goto_0
.end method

.method private buildPartSeIntent(Ljava/lang/String;[B[B)Landroid/content/Intent;
    .locals 3
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "aid"    # [B
    .param p3, "param"    # [B

    .prologue
    .line 611
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 612
    .local v0, "querryIntent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 613
    .local v1, "uri":Landroid/net/Uri;
    iget-boolean v2, p0, Lcom/android/nfc/HciEventControl;->isGsma_v40_HciSupported:Z

    if-eqz v2, :cond_3

    .line 614
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->ACTION_TRANSACTION_EVENT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_AID:Ljava/lang/String;

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 616
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_DATA:Ljava/lang/String;

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 617
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 618
    const-string v2, "nfc://secure:0/SIM1"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 624
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 633
    .end local v0    # "querryIntent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-object v0

    .line 619
    .restart local v0    # "querryIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 620
    const-string v2, "nfc://secure:0/eSE1"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 622
    :cond_2
    const-string v2, "nfc://secure:0/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 626
    :cond_3
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 627
    const-string v2, "nfc://secure:0/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 628
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1

    .line 629
    :cond_4
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 630
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private buildSeIntent(Ljava/lang/String;[B[B)Landroid/content/Intent;
    .locals 6
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "aid"    # [B
    .param p3, "param"    # [B

    .prologue
    const/4 v2, 0x0

    .line 584
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 585
    .local v0, "transactionIntent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 586
    .local v1, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->ACTION_TRANSACTION_EVENT:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_AID:Ljava/lang/String;

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 588
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_DATA:Ljava/lang/String;

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 590
    iget-boolean v3, p0, Lcom/android/nfc/HciEventControl;->isGsma_v40_HciSupported:Z

    if-eqz v3, :cond_3

    .line 591
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 592
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nfc://secure:0/SIM1/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/android/nfc/HciEventControl;->bytesToString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 598
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 607
    .end local v0    # "transactionIntent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-object v0

    .line 593
    .restart local v0    # "transactionIntent":Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 594
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nfc://secure:0/eSE1/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/android/nfc/HciEventControl;->bytesToString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 596
    goto :goto_1

    .line 600
    :cond_3
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 601
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nfc://secure:0/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    const-string v5, ""

    invoke-direct {p0, p2, v3, v4, v5}, Lcom/android/nfc/HciEventControl;->byteArray2String([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 602
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1

    .line 603
    :cond_4
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v0, v2

    .line 604
    goto :goto_1
.end method

.method private byteArray2String([BIILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "data"    # [B
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "prefix"    # Ljava/lang/String;

    .prologue
    .line 295
    if-nez p1, :cond_0

    .line 296
    new-instance v2, Ljava/lang/String;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 305
    :goto_0
    return-object v2

    .line 298
    :cond_0
    const/4 v2, -0x1

    if-ne p3, v2, :cond_1

    .line 299
    array-length v2, p1

    sub-int p3, v2, p2

    .line 301
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 302
    .local v0, "buffer":Ljava/lang/StringBuffer;
    move v1, p2

    .local v1, "ind":I
    :goto_1
    add-int v2, p2, p3

    if-ge v1, v2, :cond_2

    .line 303
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    add-int/lit16 v3, v3, 0x100

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 305
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static bytesToString([B)Ljava/lang/String;
    .locals 9
    .param p0, "bytes"    # [B

    .prologue
    .line 806
    if-nez p0, :cond_0

    .line 807
    new-instance v5, Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 813
    :goto_0
    return-object v5

    .line 809
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 810
    .local v4, "sb":Ljava/lang/StringBuffer;
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_1

    aget-byte v1, v0, v2

    .line 811
    .local v1, "b":B
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    and-int/lit16 v8, v1, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 810
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 813
    .end local v1    # "b":B
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private checkCdfApproved(Ljava/lang/String;)V
    .locals 7
    .param p1, "SEName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 817
    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-nez v5, :cond_2

    .line 818
    :cond_0
    sget-boolean v5, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v5, :cond_1

    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "SmartCardSrv not binded - cdf not approved"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    :cond_1
    new-instance v5, Ljava/lang/SecurityException;

    const-string v6, "PKCS#15 CDF certificate authorization failed"

    invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 821
    :cond_2
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    .line 822
    .local v4, "pid":I
    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 823
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 824
    .local v2, "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 825
    .local v1, "appInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, v4, :cond_3

    .line 826
    sget-boolean v5, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v5, :cond_4

    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "found PID"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    :cond_4
    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    iget-object v6, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-interface {v5, p1, v6}, Lorg/simalliance/openmobileapi/service/ISmartcardService;->isCdfAllowed(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 828
    sget-boolean v5, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v5, :cond_5

    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "PKCS#15 CDF authorization passed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    :cond_5
    return-void

    .line 831
    :cond_6
    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "PKCS#15 CDF authorization failed"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    new-instance v5, Ljava/lang/SecurityException;

    const-string v6, "PKCS#15 CDF certificate authorization failed"

    invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 836
    .end local v1    # "appInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_7
    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "Caller package name cannot be determined"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    new-instance v5, Ljava/lang/SecurityException;

    const-string v6, "PKCS#15 CDF certificate authorization failed"

    invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)Ljava/lang/String;
    .locals 2
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    .line 285
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardError;->createException()Ljava/lang/Exception;

    move-result-object v0

    .line 286
    .local v0, "exp":Ljava/lang/Exception;
    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 288
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method private dispatchSecureEvent(Ljava/lang/String;[B[B)V
    .locals 10
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "aid"    # [B
    .param p3, "param"    # [B

    .prologue
    const v9, 0x10040

    .line 336
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_0

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "Dispatching SE"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/nfc/HciEventControl;->buildSeIntent(Ljava/lang/String;[B[B)Landroid/content/Intent;

    move-result-object v2

    .line 341
    .local v2, "intentTransaction":Landroid/content/Intent;
    invoke-direct {p0, p1, p2, p3}, Lcom/android/nfc/HciEventControl;->buildPartSeIntent(Ljava/lang/String;[B[B)Landroid/content/Intent;

    move-result-object v5

    .line 343
    .local v5, "partIntentTransaction":Landroid/content/Intent;
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 350
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intentTransaction":Landroid/content/Intent;
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 351
    .restart local v2    # "intentTransaction":Landroid/content/Intent;
    const-string v7, "com.android.nfc_extras.action.AID_SELECTED"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const-string v7, "com.android.nfc_extras.extra.AID"

    invoke-virtual {v2, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 353
    const-string v7, "com.android.nfc_extras.extra.DATA"

    invoke-virtual {v2, v7, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 354
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 456
    :cond_1
    :goto_0
    return-void

    .line 363
    :cond_2
    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->multEvt:Ljava/util/HashMap;

    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 365
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v2, v9}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 371
    .local v6, "ri":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v5, v9}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 373
    invoke-direct {p0, v6}, Lcom/android/nfc/HciEventControl;->removeDuplicateService(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 376
    :try_start_0
    invoke-direct {p0, p1, v2, p2, v6}, Lcom/android/nfc/HciEventControl;->sendSeBroadcast(Ljava/lang/String;Landroid/content/Intent;[BLjava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 377
    :catch_0
    move-exception v1

    .line 378
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_1

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "exception occured"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 383
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v6    # "ri":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_3
    monitor-enter p0

    .line 384
    :try_start_1
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->mOverrideFilters:[Landroid/content/IntentFilter;

    .line 385
    .local v3, "overrideFilters":[Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/android/nfc/HciEventControl;->mOverrideIntent:Landroid/app/PendingIntent;

    .line 386
    .local v4, "overrideIntent":Landroid/app/PendingIntent;
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    invoke-direct {p0, p1, v2, v4, v3}, Lcom/android/nfc/HciEventControl;->isEnabledForegroundSeDispatch(Ljava/lang/String;Landroid/content/Intent;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 391
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->resumeAppSwitches()V

    .line 413
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 419
    .restart local v6    # "ri":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v5, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 427
    new-instance v0, Lcom/android/nfc/HciEventControl$2;

    invoke-direct {v0, p0}, Lcom/android/nfc/HciEventControl$2;-><init>(Lcom/android/nfc/HciEventControl;)V

    .line 442
    .local v0, "comp":Ljava/util/Comparator;
    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 445
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_4

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "Looking for an activity for a SE."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :cond_4
    :try_start_2
    invoke-direct {p0, p1, v2, p2, v6}, Lcom/android/nfc/HciEventControl;->findAndStartActivity(Ljava/lang/String;Landroid/content/Intent;[BLjava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 448
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_1

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "Started activity for a SE."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 451
    :catch_1
    move-exception v1

    .line 452
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_5

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "exception occured"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_1

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "There\'s no activity for a SE."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 386
    .end local v0    # "comp":Ljava/util/Comparator;
    .end local v3    # "overrideFilters":[Landroid/content/IntentFilter;
    .end local v4    # "overrideIntent":Landroid/app/PendingIntent;
    .end local v6    # "ri":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7
.end method

.method private findAndStartActivity(Ljava/lang/String;Landroid/content/Intent;[BLjava/util/List;)Z
    .locals 10
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "intentTransaction"    # Landroid/content/Intent;
    .param p3, "aid"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "[B",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 535
    .local p4, "ri":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz p4, :cond_7

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_7

    .line 536
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_0

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "Trying to start a normal activity."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    :cond_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    new-array v6, v7, [Ljava/lang/String;

    .line 538
    .local v6, "packageNames":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 539
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    aput-object v7, v6, v3

    .line 538
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 541
    :cond_1
    invoke-direct {p0, p1, p2, v6}, Lcom/android/nfc/HciEventControl;->isConnectionAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z

    move-result-object v0

    .line 543
    .local v0, "access":[Z
    const/4 v3, 0x0

    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_6

    .line 544
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 545
    .local v5, "packageName":Ljava/lang/String;
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 547
    .local v1, "activityName":Ljava/lang/String;
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_2

    .line 549
    :try_start_0
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/16 v8, 0x1000

    invoke-virtual {v7, v5, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 552
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    const-string v7, "NfcServiceHciEventControl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "package="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const-string v7, "NfcServiceHciEventControl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "activity="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    const-string v7, "NfcServiceHciEventControl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "requestedPermissions="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_2
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    invoke-virtual {v7, v8, v5}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_2

    .line 563
    const-string v7, "NfcServiceHciEventControl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " doesn\'t exist"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    :cond_2
    iget-object v7, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    invoke-virtual {v7, v8, v5}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_3

    aget-boolean v7, v0, v3

    if-nez v7, :cond_5

    .line 572
    :cond_3
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_4

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "Permission denied"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 558
    :catch_0
    move-exception v2

    .line 559
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "NfcServiceHciEventControl"

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 576
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    invoke-direct {p0, p2, v5, v1}, Lcom/android/nfc/HciEventControl;->startActivity(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    .line 580
    .end local v0    # "access":[Z
    .end local v1    # "activityName":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v6    # "packageNames":[Ljava/lang/String;
    :goto_3
    return v7

    .line 578
    .restart local v0    # "access":[Z
    .restart local v3    # "i":I
    .restart local v6    # "packageNames":[Ljava/lang/String;
    :cond_6
    sget-boolean v7, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v7, :cond_7

    const-string v7, "NfcServiceHciEventControl"

    const-string v8, "There\'s no activity that is allowed to handle SE."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    .end local v0    # "access":[Z
    .end local v3    # "i":I
    .end local v6    # "packageNames":[Ljava/lang/String;
    :cond_7
    const/4 v7, 0x0

    goto :goto_3
.end method

.method private static hexCharToInt(C)I
    .locals 3
    .param p0, "c"    # C

    .prologue
    .line 190
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 191
    add-int/lit8 v0, p0, -0x30

    .line 195
    :goto_0
    return v0

    .line 192
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 193
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 194
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 195
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 196
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid hex char \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static hexStringToBytes(Ljava/lang/String;)[B
    .locals 6
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 178
    if-nez p0, :cond_1

    .line 179
    const/4 v2, 0x0

    .line 186
    :cond_0
    return-object v2

    .line 181
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 182
    .local v1, "l":I
    div-int/lit8 v3, v1, 0x2

    new-array v2, v3, [B

    .line 183
    .local v2, "ret":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 184
    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/android/nfc/HciEventControl;->hexCharToInt(C)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/android/nfc/HciEventControl;->hexCharToInt(C)I

    move-result v5

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 183
    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private isConnectionAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z
    .locals 8
    .param p1, "SEName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "packageNames"    # [Ljava/lang/String;

    .prologue
    .line 740
    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    .line 741
    :cond_0
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_1

    const-string v0, "NfcServiceHciEventControl"

    const-string v1, "aid and/or packageName == null , returning false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    :cond_1
    array-length v0, p3

    new-array v7, v0, [Z

    .line 763
    :cond_2
    :goto_0
    return-object v7

    .line 745
    :cond_3
    iget-object v0, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_EXTRA_AID:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 747
    .local v2, "aid":[B
    iget-boolean v0, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-nez v0, :cond_5

    .line 748
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_4

    const-string v0, "NfcServiceHciEventControl"

    const-string v1, "mBindSmartcardServiceSuccess == false , returning false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    :cond_4
    array-length v0, p3

    new-array v7, v0, [Z

    goto :goto_0

    .line 752
    :cond_5
    :try_start_0
    new-instance v4, Lcom/android/nfc/HciEventControl$3;

    invoke-direct {v4, p0}, Lcom/android/nfc/HciEventControl$3;-><init>(Lcom/android/nfc/HciEventControl;)V

    .line 753
    .local v4, "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    new-instance v5, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v5}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V

    .line 755
    .local v5, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    iget-object v0, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    move-object v1, p1

    move-object v3, p3

    invoke-interface/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/ISmartcardService;->isNFCEventAllowed(Ljava/lang/String;[B[Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)[Z

    move-result-object v7

    .line 756
    .local v7, "result":[Z
    if-nez v7, :cond_2

    .line 757
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_6

    const-string v0, "NfcServiceHciEventControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isNFCEventAllowed returned null: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v5}, Lcom/android/nfc/HciEventControl;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    :cond_6
    array-length v0, p3

    new-array v7, v0, [Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 760
    .end local v4    # "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .end local v5    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    .end local v7    # "result":[Z
    :catch_0
    move-exception v6

    .line 761
    .local v6, "e":Ljava/lang/Exception;
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_7

    const-string v0, "NfcServiceHciEventControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "There was problem during executing isNFCEventAllowed. Returning false. Exception : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :cond_7
    array-length v0, p3

    new-array v7, v0, [Z

    goto :goto_0
.end method

.method private isEnabledForegroundSeDispatch(Ljava/lang/String;Landroid/content/Intent;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;)Z
    .locals 10
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p4, "filters"    # [Landroid/content/IntentFilter;

    .prologue
    .line 492
    if-eqz p3, :cond_8

    .line 493
    invoke-virtual {p3}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v7

    .line 494
    .local v7, "packageName":Ljava/lang/String;
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v7, v8, v9

    invoke-direct {p0, p1, p2, v8}, Lcom/android/nfc/HciEventControl;->isConnectionAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z

    move-result-object v0

    .line 495
    .local v0, "access":[Z
    sget-boolean v8, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v8, :cond_0

    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Attempting to dispatch tag with override intent: "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_0
    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v9, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    invoke-virtual {v8, v9, v7}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v9, "android.permission.NFC"

    invoke-virtual {v8, v9, v7}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x0

    aget-boolean v8, v0, v8

    if-nez v8, :cond_3

    .line 499
    :cond_1
    sget-boolean v8, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v8, :cond_2

    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Permission denied"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_2
    const/4 v8, 0x0

    .line 531
    .end local v0    # "access":[Z
    .end local v7    # "packageName":Ljava/lang/String;
    :goto_0
    return v8

    .line 502
    .restart local v0    # "access":[Z
    .restart local v7    # "packageName":Ljava/lang/String;
    :cond_3
    if-nez p4, :cond_6

    .line 504
    :try_start_0
    sget-boolean v8, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v8, :cond_4

    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "IntentFilter[] == null - not checking filter matching."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    :cond_4
    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    const/4 v9, -0x1

    invoke-virtual {p3, v8, v9, p2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 510
    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Transaction intent was sent through pending intent."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    const/4 v8, 0x1

    goto :goto_0

    .line 506
    :catch_0
    move-exception v2

    .line 507
    .local v2, "e":Landroid/app/PendingIntent$CanceledException;
    sget-boolean v8, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v8, :cond_5

    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Cant\'t send an intent through pending intent."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_5
    const/4 v8, 0x0

    goto :goto_0

    .line 513
    .end local v2    # "e":Landroid/app/PendingIntent$CanceledException;
    :cond_6
    move-object v1, p4

    .local v1, "arr$":[Landroid/content/IntentFilter;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_8

    aget-object v3, v1, v4

    .line 514
    .local v3, "filter":Landroid/content/IntentFilter;
    sget-boolean v8, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v8, :cond_7

    .line 515
    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Found intent filter: "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    new-instance v6, Landroid/util/LogPrinter;

    const/4 v8, 0x3

    const-string v9, "NfcServiceHciEventControl"

    invoke-direct {v6, v8, v9}, Landroid/util/LogPrinter;-><init>(ILjava/lang/String;)V

    .line 517
    .local v6, "lp":Landroid/util/LogPrinter;
    const-string v8, ""

    invoke-virtual {v3, v6, v8}, Landroid/content/IntentFilter;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    .line 519
    .end local v6    # "lp":Landroid/util/LogPrinter;
    :cond_7
    invoke-direct {p0, v3, p2}, Lcom/android/nfc/HciEventControl;->isIntentFilterMatch(Landroid/content/IntentFilter;Landroid/content/Intent;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 521
    :try_start_1
    iget-object v8, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    const/4 v9, -0x1

    invoke-virtual {p3, v8, v9, p2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_1

    .line 526
    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Transaction intent was sent through pending intent."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    const/4 v8, 0x1

    goto :goto_0

    .line 522
    :catch_1
    move-exception v2

    .line 523
    .restart local v2    # "e":Landroid/app/PendingIntent$CanceledException;
    sget-boolean v8, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v8, :cond_8

    const-string v8, "NfcServiceHciEventControl"

    const-string v9, "Cant\'t send an intent through pending intent."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    .end local v0    # "access":[Z
    .end local v1    # "arr$":[Landroid/content/IntentFilter;
    .end local v2    # "e":Landroid/app/PendingIntent$CanceledException;
    .end local v3    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "packageName":Ljava/lang/String;
    :cond_8
    const/4 v8, 0x0

    goto :goto_0

    .line 513
    .restart local v0    # "access":[Z
    .restart local v1    # "arr$":[Landroid/content/IntentFilter;
    .restart local v3    # "filter":Landroid/content/IntentFilter;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v7    # "packageName":Ljava/lang/String;
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private isIntentFilterMatch(Landroid/content/IntentFilter;Landroid/content/Intent;)Z
    .locals 13
    .param p1, "intentFilter"    # Landroid/content/IntentFilter;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 637
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v10, v11

    .line 684
    :goto_0
    return v10

    .line 638
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 639
    .local v9, "scheme":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 640
    .local v1, "host":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPort()I

    move-result v8

    .line 642
    .local v8, "port":I
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 644
    .local v0, "fullPath":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "/"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 645
    .local v6, "partPath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 648
    .local v3, "match":Z
    invoke-virtual {p1, v9}, Landroid/content/IntentFilter;->hasDataScheme(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 649
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_2

    const-string v10, "NfcServiceHciEventControl"

    const-string v12, "Intent filter does not match: no matching scheme in the intent filter."

    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v10, v11

    .line 650
    goto :goto_0

    .line 654
    :cond_3
    invoke-virtual {p1}, Landroid/content/IntentFilter;->countDataAuthorities()I

    move-result v4

    .line 655
    .local v4, "numDataAuthorities":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_4

    .line 656
    invoke-virtual {p1, v2}, Landroid/content/IntentFilter;->getDataAuthority(I)Landroid/content/IntentFilter$AuthorityEntry;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/IntentFilter$AuthorityEntry;->getPort()I

    move-result v10

    if-ne v10, v8, :cond_6

    invoke-virtual {p1, v2}, Landroid/content/IntentFilter;->getDataAuthority(I)Landroid/content/IntentFilter$AuthorityEntry;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/IntentFilter$AuthorityEntry;->getHost()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_6

    .line 658
    const/4 v3, 0x1

    .line 662
    :cond_4
    if-nez v3, :cond_7

    .line 663
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_5

    const-string v10, "NfcServiceHciEventControl"

    const-string v12, "Intent filter does not match: no matching host and port in the intent filter."

    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move v10, v11

    .line 664
    goto :goto_0

    .line 655
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 669
    :cond_7
    invoke-virtual {p1}, Landroid/content/IntentFilter;->countDataPaths()I

    move-result v5

    .line 670
    .local v5, "numDataPaths":I
    if-lez v5, :cond_c

    .line 671
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_c

    .line 672
    invoke-virtual {p1, v2}, Landroid/content/IntentFilter;->getDataPath(I)Landroid/os/PatternMatcher;

    move-result-object v7

    .line 673
    .local v7, "pm":Landroid/os/PatternMatcher;
    invoke-virtual {v7}, Landroid/os/PatternMatcher;->getType()I

    move-result v10

    if-nez v10, :cond_a

    .line 674
    invoke-virtual {v7, v0}, Landroid/os/PatternMatcher;->match(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    invoke-virtual {v7, v6}, Landroid/os/PatternMatcher;->match(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 675
    :cond_8
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_9

    const-string v10, "NfcServiceHciEventControl"

    const-string v11, "Found matching path."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_9
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 679
    :cond_a
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_b

    const-string v10, "NfcServiceHciEventControl"

    const-string v12, "Only PATTERN_LITERAL paths are allowed."

    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 683
    .end local v7    # "pm":Landroid/os/PatternMatcher;
    :cond_c
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_d

    const-string v10, "NfcServiceHciEventControl"

    const-string v12, "Intent filter does not match: no matching path in the intent filter."

    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    move v10, v11

    .line 684
    goto/16 :goto_0
.end method

.method private removeDuplicateService(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 460
    .local v7, "newList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_1

    .line 487
    :cond_0
    :goto_0
    return-object v7

    .line 463
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    if-ge v4, v11, :cond_0

    .line 464
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 465
    .local v5, "info":Landroid/content/pm/ResolveInfo;
    iget-object v11, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 466
    .local v9, "packageName":Ljava/lang/String;
    iget-object v11, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 467
    .local v1, "className":Ljava/lang/String;
    const/4 v3, 0x0

    .line 468
    .local v3, "exist":Z
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_2

    .line 469
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-ge v6, v11, :cond_2

    .line 470
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    .line 471
    .local v10, "r":Landroid/content/pm/ResolveInfo;
    iget-object v11, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 472
    .local v8, "p":Ljava/lang/String;
    iget-object v11, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 473
    .local v0, "c":Ljava/lang/String;
    if-eqz v8, :cond_4

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 475
    const/4 v3, 0x1

    .line 480
    .end local v0    # "c":Ljava/lang/String;
    .end local v6    # "j":I
    .end local v8    # "p":Ljava/lang/String;
    .end local v10    # "r":Landroid/content/pm/ResolveInfo;
    :cond_2
    if-nez v3, :cond_3

    .line 481
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 469
    .restart local v0    # "c":Ljava/lang/String;
    .restart local v6    # "j":I
    .restart local v8    # "p":Ljava/lang/String;
    .restart local v10    # "r":Landroid/content/pm/ResolveInfo;
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 484
    .end local v0    # "c":Ljava/lang/String;
    .end local v1    # "className":Ljava/lang/String;
    .end local v3    # "exist":Z
    .end local v5    # "info":Landroid/content/pm/ResolveInfo;
    .end local v6    # "j":I
    .end local v8    # "p":Ljava/lang/String;
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "r":Landroid/content/pm/ResolveInfo;
    :catch_0
    move-exception v2

    .line 485
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private resumeAppSwitches()V
    .locals 1

    .prologue
    .line 768
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/HciEventControl;->mIActivityManager:Landroid/app/IActivityManager;

    invoke-interface {v0}, Landroid/app/IActivityManager;->resumeAppSwitches()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 771
    :goto_0
    return-void

    .line 769
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private sendSeBroadcast(Ljava/lang/String;Landroid/content/Intent;[BLjava/util/List;)V
    .locals 8
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "aid"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "[B",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 701
    .local p4, "ri":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sget-boolean v5, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v5, :cond_0

    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "Secure event broadcast."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    :cond_0
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->resumeAppSwitches()V

    .line 703
    if-eqz p4, :cond_6

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 704
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Ljava/lang/String;

    .line 705
    .local v4, "packageNames":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 706
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    aput-object v5, v4, v1

    .line 705
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 708
    :cond_1
    invoke-direct {p0, p1, p2, v4}, Lcom/android/nfc/HciEventControl;->isConnectionAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z

    move-result-object v0

    .line 709
    .local v0, "access":[Z
    const/4 v1, 0x0

    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_7

    .line 710
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 711
    .local v3, "packageName":Ljava/lang/String;
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 717
    .local v2, "itemName":Ljava/lang/String;
    sget-boolean v5, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v5, :cond_2

    .line 718
    const-string v5, "NfcServiceHciEventControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "package = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    const-string v5, "NfcServiceHciEventControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "class   = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    const-string v5, "NfcServiceHciEventControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "access  = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-boolean v7, v0, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    :cond_2
    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v6, p0, Lcom/android/nfc/HciEventControl;->TRANSACTION_PERMISSION:Ljava/lang/String;

    invoke-virtual {v5, v6, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.permission.NFC"

    invoke-virtual {v5, v6, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    aget-boolean v5, v0, v1

    if-nez v5, :cond_5

    .line 726
    :cond_3
    sget-boolean v5, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v5, :cond_4

    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "Permission denied"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 730
    :cond_5
    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 731
    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    invoke-virtual {v5, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 732
    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "sent broadcast intent"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 735
    .end local v0    # "access":[Z
    .end local v1    # "i":I
    .end local v2    # "itemName":Ljava/lang/String;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "packageNames":[Ljava/lang/String;
    :cond_6
    const-string v5, "NfcServiceHciEventControl"

    const-string v6, "list is null or size is 0"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    :cond_7
    return-void
.end method

.method private startActivity(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "intentTransaction"    # Landroid/content/Intent;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "activityName"    # Ljava/lang/String;

    .prologue
    .line 688
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 689
    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 690
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "Start Activity for Card Emulation event for package"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 693
    const/4 v1, 0x1

    .line 696
    :goto_0
    return v1

    .line 694
    :catch_0
    move-exception v0

    .line 695
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "There were problems starting activity"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 696
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private waitForSmartcardService()V
    .locals 6

    .prologue
    .line 312
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->smartcardConnection:Landroid/content/ServiceConnection;

    monitor-enter v3

    .line 314
    :try_start_0
    sget-boolean v2, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "NfcServiceHciEventControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "waitForSmartcardService : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_5

    .line 316
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    if-nez v2, :cond_4

    .line 317
    iget-boolean v2, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-nez v2, :cond_2

    .line 318
    sget-boolean v2, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v2, :cond_1

    const-string v2, "NfcServiceHciEventControl"

    const-string v4, "binding to access control service did not succeed"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 329
    .end local v1    # "i":I
    :goto_1
    return-void

    .line 321
    .restart local v1    # "i":I
    :cond_2
    :try_start_2
    sget-boolean v2, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v2, :cond_3

    const-string v2, "NfcServiceHciEventControl"

    const-string v4, "waiting for connection to SmartcardService"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_3
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->smartcardConnection:Landroid/content/ServiceConnection;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 315
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 325
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 326
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    sget-boolean v2, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v2, :cond_5

    const-string v2, "NfcServiceHciEventControl"

    const-string v4, "InterruptedException"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 328
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method


# virtual methods
.method public checkAndSendIntent(I[B[B)V
    .locals 3
    .param p1, "reader"    # I
    .param p2, "aid"    # [B
    .param p3, "param"    # [B

    .prologue
    .line 161
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "checkAndSendIntent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_0
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->waitForSmartcardService()V

    .line 163
    iget-boolean v1, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-eqz v1, :cond_1

    .line 165
    if-nez p1, :cond_2

    .line 166
    :try_start_0
    iget-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-direct {p0, v1, p2, p3}, Lcom/android/nfc/HciEventControl;->dispatchSecureEvent(Ljava/lang/String;[B[B)V

    .line 173
    :cond_1
    :goto_0
    return-void

    .line 167
    :cond_2
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 168
    iget-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-direct {p0, v1, p2, p3}, Lcom/android/nfc/HciEventControl;->dispatchSecureEvent(Ljava/lang/String;[B[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v1, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcServiceHciEventControl"

    const-string v2, "exception occured"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public enableMultiEvt_transactionReception(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "SEName"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 147
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcServiceHciEventControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableMultiEvt_transactionReception for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->waitForSmartcardService()V

    .line 149
    iget-boolean v0, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-eqz v0, :cond_1

    .line 150
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sim"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/android/nfc/HciEventControl;->multEvt:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    :cond_1
    :goto_0
    return-void

    .line 152
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ese"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/android/nfc/HciEventControl;->multEvt:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/nfc/HciEventControl;->READER_ESE_S:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public isAidAllowed(Ljava/lang/String;)Z
    .locals 6
    .param p1, "aid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 201
    :try_start_0
    iget-object v4, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    invoke-static {p1}, Lcom/android/nfc/HciEventControl;->hexStringToBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/simalliance/openmobileapi/service/ISmartcardService;->isAidAllowed([B)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    .line 207
    :goto_0
    return v3

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e1":Ljava/lang/NoSuchMethodError;
    goto :goto_0

    .line 204
    .end local v0    # "e1":Ljava/lang/NoSuchMethodError;
    :catch_1
    move-exception v1

    .line 205
    .local v1, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 206
    .end local v1    # "e2":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v2

    .line 207
    .local v2, "e3":Landroid/os/RemoteException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isAllowedForGsma()Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 236
    const/4 v10, 0x2

    new-array v7, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "SIM"

    aput-object v11, v7, v10

    const/4 v10, 0x1

    const-string v11, "eSE"

    aput-object v11, v7, v10

    .line 237
    .local v7, "list":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 238
    .local v8, "result":Z
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->waitForSmartcardService()V

    .line 239
    iget-boolean v10, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-eqz v10, :cond_0

    .line 241
    :try_start_0
    iget-object v10, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    const-string v11, "android.permission.WRITE_SECURE_SETTINGS"

    const-string v12, "WRITE_SECURE_SETTINGS permission required"

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    or-int/lit8 v8, v8, 0x1

    .line 259
    .end local v8    # "result":Z
    :cond_0
    return v8

    .line 243
    .restart local v8    # "result":Z
    :catch_0
    move-exception v1

    .line 244
    .local v1, "e":Ljava/lang/SecurityException;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v9, v0, v5

    .line 246
    .local v9, "se":Ljava/lang/String;
    :try_start_1
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_1

    const-string v10, "NfcServiceHciEventControl"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "check the permission on "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_1
    invoke-direct {p0, v9}, Lcom/android/nfc/HciEventControl;->checkCdfApproved(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    .line 248
    or-int/lit8 v8, v8, 0x1

    .line 244
    :cond_2
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 249
    :catch_1
    move-exception v2

    .line 250
    .local v2, "e1":Ljava/lang/SecurityException;
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_2

    const-string v10, "NfcServiceHciEventControl"

    const-string v11, "SecurityException"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 251
    .end local v2    # "e1":Ljava/lang/SecurityException;
    :catch_2
    move-exception v3

    .line 252
    .local v3, "e2":Landroid/os/RemoteException;
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_2

    const-string v10, "NfcServiceHciEventControl"

    const-string v11, "RemoteException"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 253
    .end local v3    # "e2":Landroid/os/RemoteException;
    :catch_3
    move-exception v4

    .line 254
    .local v4, "e3":Ljava/lang/NullPointerException;
    sget-boolean v10, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v10, :cond_2

    const-string v10, "NfcServiceHciEventControl"

    const-string v11, "NullPointerException"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isAllowedForGsma(Ljava/lang/String;)Z
    .locals 6
    .param p1, "SEName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 219
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->waitForSmartcardService()V

    .line 220
    iget-boolean v3, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-eqz v3, :cond_0

    .line 222
    :try_start_0
    iget-object v3, p0, Lcom/android/nfc/HciEventControl;->mContext:Landroid/content/Context;

    const-string v4, "android.permission.WRITE_SECURE_SETTINGS"

    const-string v5, "WRITE_SECURE_SETTINGS permission required"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_0
    const/4 v2, 0x1

    .line 232
    :cond_0
    :goto_1
    return v2

    .line 223
    :catch_0
    move-exception v0

    .line 225
    .local v0, "e":Ljava/lang/SecurityException;
    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/nfc/HciEventControl;->checkCdfApproved(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 226
    :catch_1
    move-exception v1

    .line 227
    .local v1, "e2":Ljava/lang/SecurityException;
    goto :goto_1
.end method

.method public isEvtAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z
    .locals 1
    .param p1, "SEName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "packageNames"    # [Ljava/lang/String;

    .prologue
    .line 214
    invoke-direct {p0, p1, p2, p3}, Lcom/android/nfc/HciEventControl;->isConnectionAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z

    move-result-object v0

    return-object v0
.end method

.method public isTrustedPkg(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "SEName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 842
    const/4 v0, 0x0

    .line 843
    .local v0, "bTrusted":Z
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-nez v2, :cond_2

    .line 844
    :cond_0
    sget-boolean v2, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v2, :cond_1

    const-string v2, "NfcServiceHciEventControl"

    const-string v3, "SmartCardSrv not binded - cdf not approved"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :cond_1
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "PKCS#15 CDF certificate authorization failed"

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 848
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc/HciEventControl;->smartcardSvc:Lorg/simalliance/openmobileapi/service/ISmartcardService;

    invoke-interface {v2, p1, p2}, Lorg/simalliance/openmobileapi/service/ISmartcardService;->isCdfAllowed(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 852
    :cond_3
    :goto_0
    return v0

    .line 849
    :catch_0
    move-exception v1

    .line 850
    .local v1, "e":Landroid/os/RemoteException;
    sget-boolean v2, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v2, :cond_3

    const-string v2, "NfcServiceHciEventControl"

    const-string v3, "RemoteException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;)V
    .locals 2
    .param p1, "intent"    # Landroid/app/PendingIntent;
    .param p2, "filters"    # [Landroid/content/IntentFilter;

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/nfc/HciEventControl;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcServiceHciEventControl"

    const-string v1, "Set SE Foreground Dispatch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    invoke-direct {p0}, Lcom/android/nfc/HciEventControl;->waitForSmartcardService()V

    .line 140
    iget-boolean v0, p0, Lcom/android/nfc/HciEventControl;->mBindSmartcardServiceSuccess:Z

    if-eqz v0, :cond_1

    .line 141
    iput-object p1, p0, Lcom/android/nfc/HciEventControl;->mOverrideIntent:Landroid/app/PendingIntent;

    .line 142
    iput-object p2, p0, Lcom/android/nfc/HciEventControl;->mOverrideFilters:[Landroid/content/IntentFilter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_1
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
