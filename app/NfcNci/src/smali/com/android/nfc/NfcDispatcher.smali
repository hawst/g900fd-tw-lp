.class Lcom/android/nfc/NfcDispatcher;
.super Ljava/lang/Object;
.source "NfcDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    }
.end annotation


# static fields
.field private static final DBG:Z

.field static final DISPATCH_FAIL:I = 0x2

.field static final DISPATCH_SUCCESS:I = 0x1

.field static final DISPATCH_UNLOCK:I = 0x3

.field static final HOMESYNC_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.homesync"

.field static final REG_EXPRESS_PHONE_NUMBER:Ljava/lang/String; = "^([0-9]|[*#(/)N,.;+-])+$"

.field private static final TAG:Ljava/lang/String; = "NfcDispatcher"

.field static final TYPE_NAME_SEC_OOB:Ljava/lang/String; = "application/vnd.sec.oob."

.field static final VALID_PHONE_NUMBER:Ljava/lang/String; = "0123456789*#(/)N,.;+-"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private final mHandoverManager:Lcom/android/nfc/handover/HandoverManager;

.field private final mIActivityManager:Landroid/app/IActivityManager;

.field private final mNfcUnlockManager:Lcom/android/nfc/NfcUnlockManager;

.field private mOverrideFilters:[Landroid/content/IntentFilter;

.field private mOverrideIntent:Landroid/app/PendingIntent;

.field private mOverrideTechLists:[[Ljava/lang/String;

.field private final mProvisioningMimes:[Ljava/lang/String;

.field private mProvisioningOnly:Z

.field private final mScreenStateHelper:Lcom/android/nfc/ScreenStateHelper;

.field private final mTechListFilters:Lcom/android/nfc/RegisteredComponentCache;

.field mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    sget-boolean v0, Lcom/android/nfc/NfcService;->DBG:Z

    sput-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/nfc/handover/HandoverManager;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handoverManager"    # Lcom/android/nfc/handover/HandoverManager;
    .param p3, "provisionOnly"    # Z

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mToast:Landroid/widget/Toast;

    .line 125
    iput-object p1, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    .line 126
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mIActivityManager:Landroid/app/IActivityManager;

    .line 127
    new-instance v2, Lcom/android/nfc/RegisteredComponentCache;

    iget-object v3, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const-string v4, "android.nfc.action.TECH_DISCOVERED"

    const-string v5, "android.nfc.action.TECH_DISCOVERED"

    invoke-direct {v2, v3, v4, v5}, Lcom/android/nfc/RegisteredComponentCache;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mTechListFilters:Lcom/android/nfc/RegisteredComponentCache;

    .line 129
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mContentResolver:Landroid/content/ContentResolver;

    .line 130
    iput-object p2, p0, Lcom/android/nfc/NfcDispatcher;->mHandoverManager:Lcom/android/nfc/handover/HandoverManager;

    .line 131
    new-instance v2, Lcom/android/nfc/ScreenStateHelper;

    invoke-direct {v2, p1}, Lcom/android/nfc/ScreenStateHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mScreenStateHelper:Lcom/android/nfc/ScreenStateHelper;

    .line 132
    invoke-static {}, Lcom/android/nfc/NfcUnlockManager;->getInstance()Lcom/android/nfc/NfcUnlockManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mNfcUnlockManager:Lcom/android/nfc/NfcUnlockManager;

    .line 134
    monitor-enter p0

    .line 135
    :try_start_0
    iput-boolean p3, p0, Lcom/android/nfc/NfcDispatcher;->mProvisioningOnly:Z

    .line 136
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    const/4 v1, 0x0

    .line 138
    .local v1, "provisionMimes":[Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 141
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f060000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 147
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/android/nfc/NfcDispatcher;->mProvisioningMimes:[Ljava/lang/String;

    .line 148
    return-void

    .line 136
    .end local v1    # "provisionMimes":[Ljava/lang/String;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 143
    .restart local v1    # "provisionMimes":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static checkForAar(Landroid/nfc/NdefRecord;)Ljava/lang/String;
    .locals 3
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 972
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v0

    sget-object v1, Landroid/nfc/NdefRecord;->RTD_ANDROID_APP:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 976
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static checkForSamsungPackage(Landroid/nfc/NdefRecord;)Ljava/lang/String;
    .locals 8
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    const/4 v7, 0x1

    .line 980
    const-string v1, "samsungapps://ProductDetail/"

    .line 981
    .local v1, "SamsungPrefix":Ljava/lang/String;
    const/4 v2, 0x0

    .line 982
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v4

    if-ne v4, v7, :cond_1

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v4

    sget-object v5, Landroid/nfc/NdefRecord;->RTD_URI:[B

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 984
    new-instance v3, Ljava/lang/String;

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 985
    .local v3, "payload":Ljava/lang/String;
    const-string v4, "samsungapps://ProductDetail/"

    const/4 v5, 0x0

    const-string v6, "samsungapps://ProductDetail/"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3, v7, v4, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 986
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 987
    .local v0, "ArStr":[Ljava/lang/String;
    array-length v4, v0

    if-lez v4, :cond_0

    .line 988
    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v2, v0, v4

    .line 989
    const-string v4, "NfcDispatcher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SamsungApp find - name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "ArStr":[Ljava/lang/String;
    :cond_0
    move-object v4, v2

    .line 994
    .end local v3    # "payload":Ljava/lang/String;
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method static extractAarPackages(Landroid/nfc/NdefMessage;)Ljava/util/List;
    .locals 6
    .param p0, "message"    # Landroid/nfc/NdefMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/nfc/NdefMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 853
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 854
    .local v0, "aarPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v1

    .local v1, "arr$":[Landroid/nfc/NdefRecord;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v5, v1, v2

    .line 855
    .local v5, "record":Landroid/nfc/NdefRecord;
    invoke-static {v5}, Lcom/android/nfc/NfcDispatcher;->checkForAar(Landroid/nfc/NdefRecord;)Ljava/lang/String;

    move-result-object v4

    .line 856
    .local v4, "pkg":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 857
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 854
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 860
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "record":Landroid/nfc/NdefRecord;
    :cond_1
    return-object v0
.end method

.method static extractSamsungPackages(Landroid/nfc/NdefMessage;)Ljava/util/List;
    .locals 6
    .param p0, "message"    # Landroid/nfc/NdefMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/nfc/NdefMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 864
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 865
    .local v0, "SamsungPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v1

    .local v1, "arr$":[Landroid/nfc/NdefRecord;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v5, v1, v2

    .line 866
    .local v5, "record":Landroid/nfc/NdefRecord;
    invoke-static {v5}, Lcom/android/nfc/NfcDispatcher;->checkForSamsungPackage(Landroid/nfc/NdefRecord;)Ljava/lang/String;

    move-result-object v4

    .line 867
    .local v4, "pkg":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 868
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 865
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 871
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "record":Landroid/nfc/NdefRecord;
    :cond_1
    return-object v0
.end method

.method static getAppSearchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    .line 1007
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1008
    .local v0, "market":Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "market://details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1009
    return-object v0
.end method

.method static getSamsungAppSearchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    .line 997
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 998
    .local v0, "market":Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "samsungapps://ProductDetail/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 999
    return-object v0
.end method

.method private handleNfcUnlock(Landroid/nfc/Tag;)Z
    .locals 1
    .param p1, "tag"    # Landroid/nfc/Tag;

    .prologue
    .line 561
    iget-object v0, p0, Lcom/android/nfc/NfcDispatcher;->mNfcUnlockManager:Lcom/android/nfc/NfcUnlockManager;

    invoke-virtual {v0, p1}, Lcom/android/nfc/NfcUnlockManager;->tryUnlock(Landroid/nfc/Tag;)Z

    move-result v0

    return v0
.end method

.method static isComponentEnabled(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)Z
    .locals 6
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 1013
    const/4 v2, 0x0

    .line 1014
    .local v2, "enabled":Z
    new-instance v0, Landroid/content/ComponentName;

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    .local v0, "compname":Landroid/content/ComponentName;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1021
    const/4 v2, 0x1

    .line 1026
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    .line 1027
    const-string v3, "NfcDispatcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Component not enabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1029
    :cond_1
    return v2

    .line 1023
    :catch_0
    move-exception v1

    .line 1024
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method HaveHomesyncAPK(Ljava/lang/String;Landroid/nfc/NdefMessage;)Ljava/lang/String;
    .locals 7
    .param p1, "firstPackage"    # Ljava/lang/String;
    .param p2, "message"    # Landroid/nfc/NdefMessage;

    .prologue
    .line 874
    const-string v3, ""

    .line 875
    .local v3, "strAddr":Ljava/lang/String;
    const-string v4, "com.sec.android.homesync"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 876
    const/16 v0, 0x11

    .line 877
    .local v0, "BTADDR_LENGTH":I
    invoke-virtual {p2}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    .line 878
    .local v2, "records":[Landroid/nfc/NdefRecord;
    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v4}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v1

    .line 879
    .local v1, "Payload":[B
    new-instance v3, Ljava/lang/String;

    .end local v3    # "strAddr":Ljava/lang/String;
    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    .line 880
    .restart local v3    # "strAddr":Ljava/lang/String;
    const-string v4, "NfcDispatcher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "com.sec.android.homesyncBluetooth addr : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    .end local v0    # "BTADDR_LENGTH":I
    .end local v1    # "Payload":[B
    .end local v2    # "records":[Landroid/nfc/NdefRecord;
    :cond_0
    return-object v3
.end method

.method public declared-synchronized disableProvisioningMode()V
    .locals 1

    .prologue
    .line 159
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/nfc/NfcDispatcher;->mProvisioningOnly:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dispatchTag(Landroid/nfc/Tag;)I
    .locals 14
    .param p1, "tag"    # Landroid/nfc/Tag;

    .prologue
    const/4 v10, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    .line 475
    monitor-enter p0

    .line 476
    :try_start_0
    iget-object v5, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideFilters:[Landroid/content/IntentFilter;

    .line 477
    .local v5, "overrideFilters":[Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideIntent:Landroid/app/PendingIntent;

    .line 478
    .local v4, "overrideIntent":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideTechLists:[[Ljava/lang/String;

    .line 479
    .local v6, "overrideTechLists":[[Ljava/lang/String;
    iget-boolean v8, p0, Lcom/android/nfc/NfcDispatcher;->mProvisioningOnly:Z

    .line 480
    .local v8, "provisioningOnly":Z
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    const/4 v9, 0x0

    .line 483
    .local v9, "screenUnlocked":Z
    if-nez v8, :cond_1

    iget-object v0, p0, Lcom/android/nfc/NfcDispatcher;->mScreenStateHelper:Lcom/android/nfc/ScreenStateHelper;

    invoke-virtual {v0}, Lcom/android/nfc/ScreenStateHelper;->checkScreenState()I

    move-result v0

    if-ne v0, v12, :cond_1

    .line 485
    invoke-direct {p0, p1}, Lcom/android/nfc/NfcDispatcher;->handleNfcUnlock(Landroid/nfc/Tag;)Z

    move-result v9

    .line 486
    if-nez v9, :cond_1

    move v10, v12

    .line 557
    :cond_0
    :goto_0
    return v10

    .line 480
    .end local v4    # "overrideIntent":Landroid/app/PendingIntent;
    .end local v5    # "overrideFilters":[Landroid/content/IntentFilter;
    .end local v6    # "overrideTechLists":[[Ljava/lang/String;
    .end local v8    # "provisioningOnly":Z
    .end local v9    # "screenUnlocked":Z
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 491
    .restart local v4    # "overrideIntent":Landroid/app/PendingIntent;
    .restart local v5    # "overrideFilters":[Landroid/content/IntentFilter;
    .restart local v6    # "overrideTechLists":[[Ljava/lang/String;
    .restart local v8    # "provisioningOnly":Z
    .restart local v9    # "screenUnlocked":Z
    :cond_1
    const/4 v3, 0x0

    .line 492
    .local v3, "message":Landroid/nfc/NdefMessage;
    invoke-static {p1}, Landroid/nfc/tech/Ndef;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/Ndef;

    move-result-object v7

    .line 493
    .local v7, "ndef":Landroid/nfc/tech/Ndef;
    if-eqz v7, :cond_2

    .line 494
    invoke-virtual {v7}, Landroid/nfc/tech/Ndef;->getCachedNdefMessage()Landroid/nfc/NdefMessage;

    move-result-object v3

    .line 497
    :cond_2
    sget-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v0, :cond_3

    const-string v0, "NfcDispatcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "dispatch tag: "

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/nfc/Tag;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v13, " message: "

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_3
    new-instance v1, Lcom/android/nfc/NfcDispatcher$DispatchInfo;

    iget-object v0, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0, p1, v3}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;-><init>(Landroid/content/Context;Landroid/nfc/Tag;Landroid/nfc/NdefMessage;)V

    .line 501
    .local v1, "dispatch":Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    invoke-virtual {p0}, Lcom/android/nfc/NfcDispatcher;->resumeAppSwitches()V

    .line 504
    invoke-virtual {p0, v1, p1}, Lcom/android/nfc/NfcDispatcher;->isUnSupportedTag(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/Tag;)Z

    .line 507
    invoke-virtual {p0, v1, v3}, Lcom/android/nfc/NfcDispatcher;->handleInputValidatioin(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/NdefMessage;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v10, v12

    .line 508
    goto :goto_0

    .line 511
    :cond_4
    invoke-virtual {p0, v1, v3}, Lcom/android/nfc/NfcDispatcher;->handleSecurityPopup(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/NdefMessage;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v10, v12

    .line 512
    goto :goto_0

    :cond_5
    move-object v0, p0

    move-object v2, p1

    .line 516
    invoke-virtual/range {v0 .. v6}, Lcom/android/nfc/NfcDispatcher;->tryOverrides(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/Tag;Landroid/nfc/NdefMessage;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 518
    if-eqz v9, :cond_6

    move v0, v10

    :goto_1
    move v10, v0

    goto :goto_0

    :cond_6
    move v0, v11

    goto :goto_1

    .line 521
    :cond_7
    iget-object v0, p0, Lcom/android/nfc/NfcDispatcher;->mHandoverManager:Lcom/android/nfc/handover/HandoverManager;

    invoke-virtual {v0, v3}, Lcom/android/nfc/handover/HandoverManager;->tryHandover(Landroid/nfc/NdefMessage;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 522
    sget-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v0, :cond_8

    const-string v0, "NfcDispatcher"

    const-string v2, "matched BT HANDOVER"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :cond_8
    if-nez v9, :cond_0

    move v10, v11

    goto :goto_0

    .line 526
    :cond_9
    iget-object v0, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    invoke-static {v7, v0}, Lcom/android/nfc/NfcWifiProtectedSetup;->tryNfcWifiSetup(Landroid/nfc/tech/Ndef;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 527
    sget-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v0, :cond_a

    const-string v0, "NfcDispatcher"

    const-string v2, "matched NFC WPS TOKEN"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_a
    if-nez v9, :cond_0

    move v10, v11

    goto/16 :goto_0

    .line 531
    :cond_b
    invoke-virtual {p0, v1, v3, v8}, Lcom/android/nfc/NfcDispatcher;->tryNdef(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/NdefMessage;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 532
    if-nez v9, :cond_0

    move v10, v11

    goto/16 :goto_0

    .line 535
    :cond_c
    if-nez v9, :cond_0

    .line 540
    if-eqz v8, :cond_d

    move v10, v12

    .line 542
    goto/16 :goto_0

    .line 546
    :cond_d
    invoke-virtual {p0, v1, p1}, Lcom/android/nfc/NfcDispatcher;->tryTech(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/Tag;)Z

    move-result v0

    if-eqz v0, :cond_e

    move v10, v11

    .line 547
    goto/16 :goto_0

    .line 550
    :cond_e
    invoke-virtual {v1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->setTagIntent()Landroid/content/Intent;

    .line 551
    invoke-virtual {v1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 552
    sget-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v0, :cond_f

    const-string v0, "NfcDispatcher"

    const-string v2, "matched TAG"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    move v10, v11

    .line 553
    goto/16 :goto_0

    .line 556
    :cond_10
    sget-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v0, :cond_11

    const-string v0, "NfcDispatcher"

    const-string v2, "no match"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    move v10, v12

    .line 557
    goto/16 :goto_0
.end method

.method dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 1033
    monitor-enter p0

    .line 1034
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mOverrideIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1035
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mOverrideFilters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideFilters:[Landroid/content/IntentFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1036
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mOverrideTechLists="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideTechLists:[[Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1037
    monitor-exit p0

    .line 1038
    return-void

    .line 1037
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method extractBtAddr(Landroid/nfc/NdefMessage;)Ljava/lang/String;
    .locals 8
    .param p1, "message"    # Landroid/nfc/NdefMessage;

    .prologue
    const/4 v5, 0x0

    .line 824
    const-string v2, ""

    .line 825
    .local v2, "btAddr":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v3, v6, v7

    .line 826
    .local v3, "firstRecord":Landroid/nfc/NdefRecord;
    new-instance v4, Ljava/lang/String;

    invoke-virtual {v3}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v6

    sget-object v7, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v4, v6, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 828
    .local v4, "rtn_cmp":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    const-string v6, "application/vnd.sec.oob."

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 829
    const/16 v0, 0x11

    .line 830
    .local v0, "BTADDR_LENGTH":I
    invoke-virtual {v3}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v1

    .line 831
    .local v1, "Payload":[B
    new-instance v2, Ljava/lang/String;

    .end local v2    # "btAddr":Ljava/lang/String;
    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    .line 832
    .restart local v2    # "btAddr":Ljava/lang/String;
    const-string v6, "NfcDispatcher"

    const-string v7, "extractBtAddr : Bluetooth addr : "

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    const/16 v6, 0x11

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v6, v7, :cond_1

    move-object v5, v2

    .line 841
    .end local v0    # "BTADDR_LENGTH":I
    .end local v1    # "Payload":[B
    :cond_0
    :goto_0
    return-object v5

    .line 836
    .restart local v0    # "BTADDR_LENGTH":I
    .restart local v1    # "Payload":[B
    :cond_1
    const-string v6, "NfcDispatcher"

    const-string v7, "extractBtAddr : invalid btAddr : "

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 840
    .end local v0    # "BTADDR_LENGTH":I
    .end local v1    # "Payload":[B
    :cond_2
    sget-boolean v6, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v6, :cond_0

    const-string v6, "NfcDispatcher"

    const-string v7, "extractBtAddr : failed "

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method filterMatch([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 6
    .param p1, "tagTechs"    # [Ljava/lang/String;
    .param p2, "filterTechs"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 961
    if-eqz p2, :cond_0

    array-length v5, p2

    if-nez v5, :cond_1

    .line 968
    :cond_0
    :goto_0
    return v4

    .line 963
    :cond_1
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 964
    .local v3, "tech":Ljava/lang/String;
    invoke-static {p1, v3}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-ltz v5, :cond_0

    .line 963
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 968
    .end local v3    # "tech":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public handleInputValidatioin(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/NdefMessage;)Z
    .locals 17
    .param p1, "dispatch"    # Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    .param p2, "msg"    # Landroid/nfc/NdefMessage;

    .prologue
    .line 278
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v14

    const-string v15, "CscFeature_NFC_EnableInvalidTagPopup"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 279
    const/4 v14, 0x0

    .line 337
    :goto_0
    return v14

    .line 281
    :cond_0
    if-nez p2, :cond_1

    .line 282
    const/4 v14, 0x0

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    invoke-virtual {v14}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v12

    .line 285
    .local v12, "tnf":S
    invoke-virtual/range {p2 .. p2}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    invoke-virtual {v14}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v13

    .line 287
    .local v13, "type":[B
    const/4 v2, 0x0

    .line 288
    .local v2, "checkValidation":Z
    const/4 v10, -0x1

    .line 290
    .local v10, "prefix":I
    const/4 v14, 0x1

    if-ne v12, v14, :cond_2

    .line 291
    sget-object v14, Landroid/nfc/NdefRecord;->RTD_URI:[B

    invoke-static {v13, v14}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 292
    invoke-virtual/range {p2 .. p2}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    invoke-virtual {v14}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v14

    const/4 v15, 0x0

    aget-byte v14, v14, v15

    and-int/lit8 v10, v14, -0x1

    .line 293
    const/4 v2, 0x1

    .line 316
    :cond_2
    :goto_1
    if-eqz v2, :cond_8

    .line 317
    if-ltz v10, :cond_3

    const/16 v14, 0x23

    if-le v10, v14, :cond_6

    .line 318
    :cond_3
    const-string v14, "NfcDispatcher"

    const-string v15, "Invalid Uri Tag"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    const-string v15, "invalidTag"

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 320
    const/4 v14, 0x1

    goto :goto_0

    .line 295
    :cond_4
    sget-object v14, Landroid/nfc/NdefRecord;->RTD_SMART_POSTER:[B

    invoke-static {v13, v14}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 297
    :try_start_0
    new-instance v7, Landroid/nfc/NdefMessage;

    invoke-virtual/range {p2 .. p2}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    invoke-virtual {v14}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v14

    invoke-direct {v7, v14}, Landroid/nfc/NdefMessage;-><init>([B)V

    .line 298
    .local v7, "nestedMessage":Landroid/nfc/NdefMessage;
    invoke-virtual {v7}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v1

    .local v1, "arr$":[Landroid/nfc/NdefRecord;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_2
    if-ge v5, v6, :cond_2

    aget-object v8, v1, v5

    .line 299
    .local v8, "nestedRecord":Landroid/nfc/NdefRecord;
    invoke-virtual {v8}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_5

    invoke-virtual {v8}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v14

    sget-object v15, Landroid/nfc/NdefRecord;->RTD_URI:[B

    invoke-static {v14, v15}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 301
    invoke-virtual {v8}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v14

    const/4 v15, 0x0

    aget-byte v14, v14, v15
    :try_end_0
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    and-int/lit8 v10, v14, -0x1

    .line 302
    const/4 v2, 0x1

    .line 303
    goto :goto_1

    .line 298
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 307
    .end local v1    # "arr$":[Landroid/nfc/NdefRecord;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "nestedMessage":Landroid/nfc/NdefMessage;
    .end local v8    # "nestedRecord":Landroid/nfc/NdefRecord;
    :catch_0
    move-exception v3

    .line 308
    .local v3, "e":Landroid/nfc/FormatException;
    const/4 v2, 0x0

    .line 312
    goto :goto_1

    .line 310
    .end local v3    # "e":Landroid/nfc/FormatException;
    :catch_1
    move-exception v3

    .line 311
    .local v3, "e":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_1

    .line 322
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :cond_6
    const/4 v14, 0x5

    if-ne v10, v14, :cond_8

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->ndefUri:Landroid/net/Uri;

    if-eqz v14, :cond_8

    .line 323
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->ndefUri:Landroid/net/Uri;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 324
    .local v11, "telUri":Ljava/lang/String;
    const/4 v14, 0x4

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v11, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 326
    .local v9, "phoneNumber":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v14

    if-ge v4, v14, :cond_8

    .line 327
    const-string v14, "0123456789*#(/)N,.;+-"

    add-int/lit8 v15, v4, 0x1

    invoke-virtual {v9, v4, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_7

    .line 328
    const-string v14, "NfcDispatcher"

    const-string v15, "Invalid phone number tag"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    const-string v15, "invalidTag"

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 330
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    sget-object v16, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 331
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 326
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 337
    .end local v4    # "i":I
    .end local v9    # "phoneNumber":Ljava/lang/String;
    .end local v11    # "telUri":Ljava/lang/String;
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method public handleSecurityPopup(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/NdefMessage;)Z
    .locals 16
    .param p1, "dispatch"    # Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    .param p2, "msg"    # Landroid/nfc/NdefMessage;

    .prologue
    .line 341
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v13

    const-string v14, "CscFeature_NFC_EnableSecurityPromptPopup"

    invoke-virtual {v13, v14}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 344
    .local v7, "popup":Ljava/lang/String;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_1

    .line 345
    :cond_0
    const/4 v13, 0x0

    .line 409
    :goto_0
    return v13

    .line 348
    :cond_1
    if-nez p2, :cond_2

    .line 349
    const/4 v13, 0x0

    goto :goto_0

    .line 351
    :cond_2
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    const-string v14, "all"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_3

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    const-string v14, "contact"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 352
    :cond_3
    move-object/from16 v5, p2

    .line 353
    .local v5, "message":Landroid/nfc/NdefMessage;
    invoke-virtual {v5}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v9

    .line 354
    .local v9, "records":[Landroid/nfc/NdefRecord;
    const/4 v13, 0x0

    aget-object v8, v9, v13

    .line 355
    .local v8, "record":Landroid/nfc/NdefRecord;
    invoke-virtual {v8}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v6

    .line 356
    .local v6, "payload":[B
    new-instance v12, Ljava/lang/String;

    invoke-virtual {v8}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v13

    const-string v14, "UTF8"

    invoke-static {v14}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 358
    .local v12, "type":Ljava/lang/String;
    const-string v13, "text/x-vcard"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_4

    const-string v13, "text/vcard"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 361
    :cond_4
    const-string v1, ""

    .line 364
    .local v1, "endLine":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 365
    .local v2, "is":Ljava/io/ByteArrayInputStream;
    new-instance v11, Ljava/io/InputStreamReader;

    const-string v13, "UTF8"

    invoke-static {v13}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v13

    invoke-direct {v11, v2, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 366
    .local v11, "tmpReader":Ljava/io/InputStreamReader;
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 369
    .local v4, "mReader":Ljava/io/BufferedReader;
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 370
    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string v14, "BEGIN:VCARD"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 371
    :cond_5
    sget-boolean v13, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v13, :cond_6

    const-string v13, "NfcDispatcher"

    const-string v14, "Not found BEGIN:VCARD at first line"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :cond_6
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    const-string v14, "invalidTag"

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 373
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    sget-object v15, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 374
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 379
    :cond_7
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    const-string v14, "mFNname"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 382
    :goto_1
    if-eqz v3, :cond_9

    .line 383
    const-string v13, ":"

    const/4 v14, 0x2

    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v10

    .line 384
    .local v10, "strArray":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, v10, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string v14, "TEL"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 385
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    const-string v14, "mFNname"

    const/4 v15, 0x1

    aget-object v15, v10, v15

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 386
    sget-boolean v13, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v13, :cond_8

    const-string v13, "NfcDispatcher"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "phoneNumber : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    aget-object v15, v10, v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_8
    move-object v1, v3

    .line 393
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 394
    goto :goto_1

    .line 397
    .end local v10    # "strArray":[Ljava/lang/String;
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    const-string v14, "END:VCARD"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_b

    .line 398
    sget-boolean v13, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v13, :cond_a

    const-string v13, "NfcDispatcher"

    const-string v14, "Not found END:VCARD at last line"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_a
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    const-string v14, "invalidTag"

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 400
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->rootIntent:Landroid/content/Intent;

    sget-object v15, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 404
    .end local v2    # "is":Ljava/io/ByteArrayInputStream;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "mReader":Ljava/io/BufferedReader;
    .end local v11    # "tmpReader":Ljava/io/InputStreamReader;
    :catch_0
    move-exception v13

    .line 409
    .end local v1    # "endLine":Ljava/lang/String;
    .end local v5    # "message":Landroid/nfc/NdefMessage;
    .end local v6    # "payload":[B
    .end local v8    # "record":Landroid/nfc/NdefRecord;
    .end local v9    # "records":[Landroid/nfc/NdefRecord;
    .end local v12    # "type":Ljava/lang/String;
    :cond_b
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method isFilterMatch(Landroid/content/Intent;[Landroid/content/IntentFilter;Z)Z
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "filters"    # [Landroid/content/IntentFilter;
    .param p3, "hasTechFilter"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 613
    if-eqz p2, :cond_2

    .line 614
    move-object v0, p2

    .local v0, "arr$":[Landroid/content/IntentFilter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v1, v0, v2

    .line 615
    .local v1, "filter":Landroid/content/IntentFilter;
    iget-object v6, p0, Lcom/android/nfc/NfcDispatcher;->mContentResolver:Landroid/content/ContentResolver;

    const-string v7, "NfcDispatcher"

    invoke-virtual {v1, v6, p1, v5, v7}, Landroid/content/IntentFilter;->match(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_1

    .line 622
    .end local v0    # "arr$":[Landroid/content/IntentFilter;
    .end local v1    # "filter":Landroid/content/IntentFilter;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    :goto_1
    return v4

    .line 614
    .restart local v0    # "arr$":[Landroid/content/IntentFilter;
    .restart local v1    # "filter":Landroid/content/IntentFilter;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 619
    .end local v0    # "arr$":[Landroid/content/IntentFilter;
    .end local v1    # "filter":Landroid/content/IntentFilter;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_2
    if-eqz p3, :cond_0

    :cond_3
    move v4, v5

    .line 622
    goto :goto_1
.end method

.method isTechMatch(Landroid/nfc/Tag;[[Ljava/lang/String;)Z
    .locals 7
    .param p1, "tag"    # Landroid/nfc/Tag;
    .param p2, "techLists"    # [[Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 626
    if-nez p2, :cond_1

    .line 637
    :cond_0
    :goto_0
    return v5

    .line 630
    :cond_1
    invoke-virtual {p1}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    move-result-object v4

    .line 631
    .local v4, "tagTechs":[Ljava/lang/String;
    invoke-static {v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 632
    move-object v0, p2

    .local v0, "arr$":[[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 633
    .local v1, "filterTechs":[Ljava/lang/String;
    invoke-virtual {p0, v4, v1}, Lcom/android/nfc/NfcDispatcher;->filterMatch([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 634
    const/4 v5, 0x1

    goto :goto_0

    .line 632
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method isUnSupportedTag(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/Tag;)Z
    .locals 10
    .param p1, "dispatch"    # Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    .param p2, "tag"    # Landroid/nfc/Tag;

    .prologue
    const/4 v6, 0x0

    .line 416
    iget-object v7, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.nxp.mifare"

    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 458
    :cond_0
    :goto_0
    return v6

    .line 420
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_NFC_EnableSecurityPromptPopup"

    const-string v9, "mifareclassic"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 422
    .local v3, "popup":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "all"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "mifareclassic"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 423
    sget-boolean v7, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v7, :cond_0

    const-string v7, "NfcDispatcher"

    const-string v8, "isUnsupportedTag : This feaure is not set"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 427
    :cond_2
    const/4 v7, 0x3

    invoke-virtual {p2, v7}, Landroid/nfc/Tag;->hasTech(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 428
    sget-boolean v7, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v7, :cond_0

    const-string v7, "NfcDispatcher"

    const-string v8, "isUnSupportedTag : This tag also supports ISO_DEP Tech"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 433
    :cond_3
    const/16 v7, 0x9

    new-array v5, v7, [S

    fill-array-data v5, :array_0

    .line 435
    .local v5, "unSupportedTagTechnology":[S
    invoke-static {p2}, Landroid/nfc/tech/NfcA;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/NfcA;

    move-result-object v0

    .line 436
    .local v0, "a":Landroid/nfc/tech/NfcA;
    if-eqz v0, :cond_0

    .line 438
    invoke-virtual {v0}, Landroid/nfc/tech/NfcA;->getSak()S

    move-result v4

    .line 440
    .local v4, "sak":I
    :try_start_0
    invoke-virtual {v0}, Landroid/nfc/tech/NfcA;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v7, v5

    if-ge v2, v7, :cond_0

    .line 448
    aget-short v7, v5, v2

    if-ne v4, v7, :cond_5

    .line 449
    iget-object v7, p0, Lcom/android/nfc/NfcDispatcher;->mToast:Landroid/widget/Toast;

    if-eqz v7, :cond_4

    .line 450
    iget-object v7, p0, Lcom/android/nfc/NfcDispatcher;->mToast:Landroid/widget/Toast;

    invoke-virtual {v7}, Landroid/widget/Toast;->cancel()V

    .line 451
    const-string v7, "NfcDispatcher"

    const-string v8, "isUnSupportedTag : mToast cancel !!"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :cond_4
    iget-object v7, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const v8, 0x7f070021

    invoke-static {v7, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/android/nfc/NfcDispatcher;->mToast:Landroid/widget/Toast;

    .line 454
    iget-object v6, p0, Lcom/android/nfc/NfcDispatcher;->mToast:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 455
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 442
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 443
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "NfcDispatcher"

    const-string v8, "Error closing"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 447
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "i":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 433
    :array_0
    .array-data 2
        0x1s
        0x8s
        0x9s
        0x10s
        0x11s
        0x18s
        0x28s
        0x38s
        0x88s
    .end array-data
.end method

.method resumeAppSwitches()V
    .locals 1

    .prologue
    .line 955
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/NfcDispatcher;->mIActivityManager:Landroid/app/IActivityManager;

    invoke-interface {v0}, Landroid/app/IActivityManager;->resumeAppSwitches()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 957
    :goto_0
    return-void

    .line 956
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method saveBtAddr(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "pkg_name"    # Ljava/lang/String;
    .param p2, "btAddr"    # Ljava/lang/String;

    .prologue
    .line 845
    iget-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const-string v3, "bt_addr_list"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 846
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 847
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 848
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 849
    return-void
.end method

.method saveHomeSyncData(Ljava/lang/String;)V
    .locals 5
    .param p1, "strAddr"    # Ljava/lang/String;

    .prologue
    .line 885
    iget-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const-string v3, "homesync"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 886
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 887
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "bt_addr"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 888
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 889
    return-void
.end method

.method public declared-synchronized setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V
    .locals 2
    .param p1, "intent"    # Landroid/app/PendingIntent;
    .param p2, "filters"    # [Landroid/content/IntentFilter;
    .param p3, "techLists"    # [[Ljava/lang/String;

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcDispatcher"

    const-string v1, "Set Foreground Dispatch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    iput-object p1, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideIntent:Landroid/app/PendingIntent;

    .line 154
    iput-object p2, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideFilters:[Landroid/content/IntentFilter;

    .line 155
    iput-object p3, p0, Lcom/android/nfc/NfcDispatcher;->mOverrideTechLists:[[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    monitor-exit p0

    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method tryNdef(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/NdefMessage;Z)Z
    .locals 24
    .param p1, "dispatch"    # Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    .param p2, "message"    # Landroid/nfc/NdefMessage;
    .param p3, "provisioningOnly"    # Z

    .prologue
    .line 641
    if-nez p2, :cond_0

    .line 642
    const/16 v21, 0x0

    .line 819
    :goto_0
    return v21

    .line 644
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->setNdefIntent()Landroid/content/Intent;

    move-result-object v12

    .line 647
    .local v12, "intent":Landroid/content/Intent;
    if-nez v12, :cond_1

    const/16 v21, 0x0

    goto :goto_0

    .line 649
    :cond_1
    if-eqz p3, :cond_3

    .line 650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher;->mProvisioningMimes:[Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher;->mProvisioningMimes:[Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    invoke-virtual {v12}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_3

    .line 652
    :cond_2
    const-string v21, "NfcDispatcher"

    const-string v22, "Dropping NFC intent in provisioning mode."

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    const/16 v21, 0x0

    goto :goto_0

    .line 658
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v6

    .local v6, "arr$":[Landroid/nfc/NdefRecord;
    array-length v13, v6

    .local v13, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v13, :cond_7

    aget-object v18, v6, v11

    .line 659
    .local v18, "record":Landroid/nfc/NdefRecord;
    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v21

    const/16 v22, 0x4

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_5

    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v21

    const-string v22, "samsung.com:facebook"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    invoke-static/range {v21 .. v22}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 661
    invoke-virtual/range {p1 .. p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 662
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_4

    const-string v21, "NfcDispatcher"

    const-string v22, "found facebook record"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    :cond_4
    const/16 v21, 0x1

    goto :goto_0

    .line 665
    :cond_5
    invoke-virtual/range {v18 .. v18}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v21

    if-nez v21, :cond_6

    .line 666
    const/16 v21, 0x0

    goto :goto_0

    .line 658
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 672
    .end local v18    # "record":Landroid/nfc/NdefRecord;
    :cond_7
    invoke-static/range {p2 .. p2}, Lcom/android/nfc/NfcDispatcher;->extractAarPackages(Landroid/nfc/NdefMessage;)Ljava/util/List;

    move-result-object v4

    .line 673
    .local v4, "aarPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_8
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 674
    .local v15, "pkg":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 675
    invoke-virtual/range {p1 .. p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity()Z

    move-result v21

    if-eqz v21, :cond_8

    .line 676
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_9

    const-string v21, "NfcDispatcher"

    const-string v22, "matched AAR to NDEF"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    :cond_9
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 682
    .end local v15    # "pkg":Ljava/lang/String;
    :cond_a
    invoke-static/range {p2 .. p2}, Lcom/android/nfc/NfcDispatcher;->extractSamsungPackages(Landroid/nfc/NdefMessage;)Ljava/util/List;

    move-result-object v3

    .line 683
    .local v3, "SamsungPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_b
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 684
    .restart local v15    # "pkg":Ljava/lang/String;
    const-string v21, "HOMESYNC_PACKAGE_NAME"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_b

    .line 685
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    invoke-virtual/range {p1 .. p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity()Z

    move-result v21

    if-eqz v21, :cond_c

    .line 687
    const-string v21, "NfcDispatcher"

    const-string v22, "String matched SAR to NDEF"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 689
    :cond_c
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "stub"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 690
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher;->extractBtAddr(Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object v7

    .line 691
    .local v7, "btAddr":Ljava/lang/String;
    if-eqz v7, :cond_d

    .line 692
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v7}, Lcom/android/nfc/NfcDispatcher;->saveBtAddr(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :cond_d
    const-string v21, "NfcDispatcher"

    const-string v22, "String matched SAR with stub to NDEF"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 700
    .end local v7    # "btAddr":Ljava/lang/String;
    .end local v15    # "pkg":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher;->extractBtAddr(Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object v7

    .line 701
    .restart local v7    # "btAddr":Ljava/lang/String;
    if-eqz v7, :cond_f

    .line 702
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_13

    .line 703
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 704
    .local v16, "pkg_name":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v7}, Lcom/android/nfc/NfcDispatcher;->saveBtAddr(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    .end local v16    # "pkg_name":Ljava/lang/String;
    :cond_f
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_18

    .line 715
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 716
    .local v10, "firstPackage":Ljava/lang/String;
    const-string v20, ""

    .line 719
    .local v20, "strAddr":Ljava/lang/String;
    :try_start_0
    new-instance v8, Landroid/os/UserHandle;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v21

    move/from16 v0, v21

    invoke-direct {v8, v0}, Landroid/os/UserHandle;-><init>(I)V

    .line 720
    .local v8, "currentUser":Landroid/os/UserHandle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "android"

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v8}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 726
    .local v17, "pm":Landroid/content/pm/PackageManager;
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_10

    const-string v21, "NfcDispatcher"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "firstPackage Name : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v10, v1}, Lcom/android/nfc/NfcDispatcher;->HaveHomesyncAPK(Ljava/lang/String;Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object v20

    .line 728
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 729
    .local v5, "appLaunchIntent":Landroid/content/Intent;
    if-eqz v5, :cond_15

    .line 730
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_11

    .line 731
    const-string v21, "NfcDispatcher"

    const-string v22, "appLaunchIntent.putExtra(bt_addr, strAddr);"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    const-string v21, "bt_addr"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 734
    :cond_11
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Landroid/content/Intent;)Z

    move-result v21

    if-eqz v21, :cond_15

    .line 735
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_12

    const-string v21, "NfcDispatcher"

    const-string v22, "matched SamsungPackages to application launch"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    :cond_12
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 705
    .end local v5    # "appLaunchIntent":Landroid/content/Intent;
    .end local v8    # "currentUser":Landroid/os/UserHandle;
    .end local v10    # "firstPackage":Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v20    # "strAddr":Ljava/lang/String;
    :cond_13
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_14

    .line 706
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 707
    .restart local v16    # "pkg_name":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v7}, Lcom/android/nfc/NfcDispatcher;->saveBtAddr(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 709
    .end local v16    # "pkg_name":Ljava/lang/String;
    :cond_14
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_f

    const-string v21, "NfcDispatcher"

    const-string v22, "no matched AAR or SAR"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 722
    .restart local v10    # "firstPackage":Ljava/lang/String;
    .restart local v20    # "strAddr":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 723
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v21, "NfcDispatcher"

    const-string v22, "Could not create user package context"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 740
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5    # "appLaunchIntent":Landroid/content/Intent;
    .restart local v8    # "currentUser":Landroid/os/UserHandle;
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_15
    invoke-static {v10}, Lcom/android/nfc/NfcDispatcher;->getSamsungAppSearchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    .line 741
    .local v19, "samsungmarketIntent":Landroid/content/Intent;
    if-eqz v19, :cond_18

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Landroid/content/Intent;)Z

    move-result v21

    if-eqz v21, :cond_18

    .line 742
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_16

    const-string v21, "NfcDispatcher"

    const-string v22, "matched SamsungApps to market launch"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    :cond_16
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_17

    .line 744
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher;->saveHomeSyncData(Ljava/lang/String;)V

    .line 746
    :cond_17
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 751
    .end local v5    # "appLaunchIntent":Landroid/content/Intent;
    .end local v8    # "currentUser":Landroid/os/UserHandle;
    .end local v10    # "firstPackage":Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v19    # "samsungmarketIntent":Landroid/content/Intent;
    .end local v20    # "strAddr":Ljava/lang/String;
    :cond_18
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_1f

    .line 752
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 754
    .restart local v10    # "firstPackage":Ljava/lang/String;
    const-string v20, ""

    .line 758
    .restart local v20    # "strAddr":Ljava/lang/String;
    :try_start_1
    new-instance v8, Landroid/os/UserHandle;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v21

    move/from16 v0, v21

    invoke-direct {v8, v0}, Landroid/os/UserHandle;-><init>(I)V

    .line 759
    .restart local v8    # "currentUser":Landroid/os/UserHandle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "android"

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2, v8}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v17

    .line 766
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_19

    const-string v21, "NfcDispatcher"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "firstPackage Name : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v10, v1}, Lcom/android/nfc/NfcDispatcher;->HaveHomesyncAPK(Ljava/lang/String;Landroid/nfc/NdefMessage;)Ljava/lang/String;

    move-result-object v20

    .line 769
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 771
    .restart local v5    # "appLaunchIntent":Landroid/content/Intent;
    if-eqz v5, :cond_1c

    .line 772
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1a

    .line 773
    const-string v21, "bt_addr"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 775
    :cond_1a
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Landroid/content/Intent;)Z

    move-result v21

    if-eqz v21, :cond_1c

    .line 777
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_1b

    const-string v21, "NfcDispatcher"

    const-string v22, "matched AAR to application launch"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :cond_1b
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 761
    .end local v5    # "appLaunchIntent":Landroid/content/Intent;
    .end local v8    # "currentUser":Landroid/os/UserHandle;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :catch_1
    move-exception v9

    .line 762
    .restart local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v21, "NfcDispatcher"

    const-string v22, "Could not create user package context"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 782
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5    # "appLaunchIntent":Landroid/content/Intent;
    .restart local v8    # "currentUser":Landroid/os/UserHandle;
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_1c
    invoke-static {v10}, Lcom/android/nfc/NfcDispatcher;->getAppSearchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v14

    .line 783
    .local v14, "marketIntent":Landroid/content/Intent;
    if-eqz v14, :cond_1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Landroid/content/Intent;)Z

    move-result v21

    if-eqz v21, :cond_1f

    .line 784
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_1d

    const-string v21, "NfcDispatcher"

    const-string v22, "matched AAR to market launch"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    :cond_1d
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1e

    .line 787
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher;->saveHomeSyncData(Ljava/lang/String;)V

    .line 790
    :cond_1e
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 795
    .end local v5    # "appLaunchIntent":Landroid/content/Intent;
    .end local v8    # "currentUser":Landroid/os/UserHandle;
    .end local v10    # "firstPackage":Ljava/lang/String;
    .end local v14    # "marketIntent":Landroid/content/Intent;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v20    # "strAddr":Ljava/lang/String;
    :cond_1f
    const-string v21, "CHINA"

    const-string v22, "ro.csc.country_code"

    invoke-static/range {v22 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_21

    .line 796
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->ndefUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    if-eqz v21, :cond_21

    .line 797
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->ndefUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    const-string v22, "http://play.google.com/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_21

    .line 798
    if-eqz v4, :cond_21

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_21

    .line 799
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 801
    .restart local v10    # "firstPackage":Ljava/lang/String;
    invoke-static {v10}, Lcom/android/nfc/NfcDispatcher;->getSamsungAppSearchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    .line 802
    .restart local v19    # "samsungmarketIntent":Landroid/content/Intent;
    if-eqz v19, :cond_21

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Landroid/content/Intent;)Z

    move-result v21

    if-eqz v21, :cond_21

    .line 803
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_20

    const-string v21, "NfcDispatcher"

    const-string v22, "CHINA : matched SamsungApps to market launch instead of Google URL"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    :cond_20
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 813
    .end local v10    # "firstPackage":Ljava/lang/String;
    .end local v19    # "samsungmarketIntent":Landroid/content/Intent;
    :cond_21
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 814
    invoke-virtual/range {p1 .. p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity()Z

    move-result v21

    if-eqz v21, :cond_23

    .line 815
    sget-boolean v21, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v21, :cond_22

    const-string v21, "NfcDispatcher"

    const-string v22, "matched NDEF"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    :cond_22
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 819
    :cond_23
    const/16 v21, 0x0

    goto/16 :goto_0
.end method

.method tryOverrides(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/Tag;Landroid/nfc/NdefMessage;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)Z
    .locals 6
    .param p1, "dispatch"    # Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    .param p2, "tag"    # Landroid/nfc/Tag;
    .param p3, "message"    # Landroid/nfc/NdefMessage;
    .param p4, "overrideIntent"    # Landroid/app/PendingIntent;
    .param p5, "overrideFilters"    # [Landroid/content/IntentFilter;
    .param p6, "overrideTechLists"    # [[Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 566
    if-nez p4, :cond_1

    .line 609
    :cond_0
    :goto_0
    return v4

    .line 572
    :cond_1
    if-eqz p3, :cond_4

    .line 573
    invoke-virtual {p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->setNdefIntent()Landroid/content/Intent;

    move-result-object v1

    .line 574
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_4

    if-eqz p6, :cond_3

    move v2, v3

    :goto_1
    invoke-virtual {p0, v1, p5, v2}, Lcom/android/nfc/NfcDispatcher;->isFilterMatch(Landroid/content/Intent;[Landroid/content/IntentFilter;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 577
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const/4 v5, -0x1

    invoke-virtual {p4, v2, v5, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 578
    sget-boolean v2, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v2, :cond_2

    const-string v2, "NfcDispatcher"

    const-string v5, "matched NDEF override"

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move v4, v3

    .line 579
    goto :goto_0

    :cond_3
    move v2, v4

    .line 574
    goto :goto_1

    .line 580
    :catch_0
    move-exception v0

    .line 581
    .local v0, "e":Landroid/app/PendingIntent$CanceledException;
    goto :goto_0

    .line 587
    .end local v0    # "e":Landroid/app/PendingIntent$CanceledException;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-virtual {p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->setTechIntent()Landroid/content/Intent;

    move-result-object v1

    .line 588
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, p2, p6}, Lcom/android/nfc/NfcDispatcher;->isTechMatch(Landroid/nfc/Tag;[[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 590
    :try_start_1
    iget-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const/4 v5, -0x1

    invoke-virtual {p4, v2, v5, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 591
    sget-boolean v2, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v2, :cond_5

    const-string v2, "NfcDispatcher"

    const-string v5, "matched TECH override"

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    move v4, v3

    .line 592
    goto :goto_0

    .line 593
    :catch_1
    move-exception v0

    .line 594
    .restart local v0    # "e":Landroid/app/PendingIntent$CanceledException;
    goto :goto_0

    .line 599
    .end local v0    # "e":Landroid/app/PendingIntent$CanceledException;
    :cond_6
    invoke-virtual {p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->setTagIntent()Landroid/content/Intent;

    move-result-object v1

    .line 600
    if-eqz p6, :cond_8

    move v2, v3

    :goto_2
    invoke-virtual {p0, v1, p5, v2}, Lcom/android/nfc/NfcDispatcher;->isFilterMatch(Landroid/content/Intent;[Landroid/content/IntentFilter;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 602
    :try_start_2
    iget-object v2, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const/4 v5, -0x1

    invoke-virtual {p4, v2, v5, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 603
    sget-boolean v2, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v2, :cond_7

    const-string v2, "NfcDispatcher"

    const-string v5, "matched TAG override"

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_7
    move v4, v3

    .line 604
    goto :goto_0

    :cond_8
    move v2, v4

    .line 600
    goto :goto_2

    .line 605
    :catch_2
    move-exception v0

    .line 606
    .restart local v0    # "e":Landroid/app/PendingIntent$CanceledException;
    goto :goto_0
.end method

.method tryTech(Lcom/android/nfc/NfcDispatcher$DispatchInfo;Landroid/nfc/Tag;)Z
    .locals 12
    .param p1, "dispatch"    # Lcom/android/nfc/NfcDispatcher$DispatchInfo;
    .param p2, "tag"    # Landroid/nfc/Tag;

    .prologue
    .line 892
    invoke-virtual {p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->setTechIntent()Landroid/content/Intent;

    .line 894
    invoke-virtual {p2}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    move-result-object v8

    .line 895
    .local v8, "tagTechs":[Ljava/lang/String;
    invoke-static {v8}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 898
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 899
    .local v5, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iget-object v9, p0, Lcom/android/nfc/NfcDispatcher;->mTechListFilters:Lcom/android/nfc/RegisteredComponentCache;

    invoke-virtual {v9}, Lcom/android/nfc/RegisteredComponentCache;->getComponents()Ljava/util/ArrayList;

    move-result-object v7

    .line 903
    .local v7, "registered":Ljava/util/List;, "Ljava/util/List<Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;>;"
    :try_start_0
    new-instance v0, Landroid/os/UserHandle;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v9

    invoke-direct {v0, v9}, Landroid/os/UserHandle;-><init>(I)V

    .line 904
    .local v0, "currentUser":Landroid/os/UserHandle;
    iget-object v9, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const-string v10, "android"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11, v0}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 911
    .local v6, "pm":Landroid/content/pm/PackageManager;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;

    .line 913
    .local v3, "info":Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;
    iget-object v9, v3, Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;->techs:[Ljava/lang/String;

    invoke-virtual {p0, v8, v9}, Lcom/android/nfc/NfcDispatcher;->filterMatch([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, v3, Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    invoke-static {v6, v9}, Lcom/android/nfc/NfcDispatcher;->isComponentEnabled(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 916
    iget-object v9, v3, Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 917
    iget-object v9, v3, Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 906
    .end local v0    # "currentUser":Landroid/os/UserHandle;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Lcom/android/nfc/RegisteredComponentCache$ComponentInfo;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 907
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "NfcDispatcher"

    const-string v10, "Could not create user package context"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    const/4 v9, 0x0

    .line 942
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_1
    return v9

    .line 922
    .restart local v0    # "currentUser":Landroid/os/UserHandle;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v6    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_5

    .line 924
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 925
    .local v3, "info":Landroid/content/pm/ResolveInfo;
    iget-object v9, p1, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    iget-object v10, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 926
    invoke-virtual {p1}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 927
    sget-boolean v9, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v9, :cond_2

    const-string v9, "NfcDispatcher"

    const-string v10, "matched single TECH"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    :cond_2
    const/4 v9, 0x1

    goto :goto_1

    .line 930
    :cond_3
    iget-object v9, p1, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 942
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    :cond_4
    const/4 v9, 0x0

    goto :goto_1

    .line 931
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_4

    .line 933
    new-instance v4, Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/nfc/NfcDispatcher;->mContext:Landroid/content/Context;

    const-class v10, Lcom/android/nfc/TechListChooserActivity;

    invoke-direct {v4, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 934
    .local v4, "intent":Landroid/content/Intent;
    const-string v9, "android.intent.extra.INTENT"

    iget-object v10, p1, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 935
    const-string v9, "rlist"

    invoke-virtual {v4, v9, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 937
    invoke-virtual {p1, v4}, Lcom/android/nfc/NfcDispatcher$DispatchInfo;->tryStartActivity(Landroid/content/Intent;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 938
    sget-boolean v9, Lcom/android/nfc/NfcDispatcher;->DBG:Z

    if-eqz v9, :cond_6

    const-string v9, "NfcDispatcher"

    const-string v10, "matched multiple TECH"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    :cond_6
    const/4 v9, 0x1

    goto :goto_1
.end method
