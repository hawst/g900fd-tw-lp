.class Lcom/android/nfc/NfcRootActivity$6;
.super Ljava/lang/Object;
.source "NfcRootActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/nfc/NfcRootActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcRootActivity;

.field final synthetic val$linear:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcRootActivity;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/android/nfc/NfcRootActivity$6;->this$0:Lcom/android/nfc/NfcRootActivity;

    iput-object p2, p0, Lcom/android/nfc/NfcRootActivity$6;->val$linear:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 221
    :try_start_0
    iget-object v1, p0, Lcom/android/nfc/NfcRootActivity$6;->val$linear:Landroid/widget/LinearLayout;

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 222
    .local v0, "askOption":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/android/nfc/NfcRootActivity$6;->this$0:Lcom/android/nfc/NfcRootActivity;

    # getter for: Lcom/android/nfc/NfcRootActivity;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/android/nfc/NfcRootActivity;->access$000(Lcom/android/nfc/NfcRootActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "contact_never_ask"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 224
    iget-object v1, p0, Lcom/android/nfc/NfcRootActivity$6;->this$0:Lcom/android/nfc/NfcRootActivity;

    # getter for: Lcom/android/nfc/NfcRootActivity;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/android/nfc/NfcRootActivity;->access$000(Lcom/android/nfc/NfcRootActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/android/nfc/NfcRootActivity$6;->this$0:Lcom/android/nfc/NfcRootActivity;

    iget-object v2, p0, Lcom/android/nfc/NfcRootActivity$6;->this$0:Lcom/android/nfc/NfcRootActivity;

    iget-object v2, v2, Lcom/android/nfc/NfcRootActivity;->mLaunchIntent:Landroid/content/Intent;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v2, v3}, Lcom/android/nfc/NfcRootActivity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .end local v0    # "askOption":Landroid/widget/CheckBox;
    :goto_0
    iget-object v1, p0, Lcom/android/nfc/NfcRootActivity$6;->this$0:Lcom/android/nfc/NfcRootActivity;

    invoke-virtual {v1}, Lcom/android/nfc/NfcRootActivity;->finish()V

    .line 230
    return-void

    .line 227
    :catch_0
    move-exception v1

    goto :goto_0
.end method
