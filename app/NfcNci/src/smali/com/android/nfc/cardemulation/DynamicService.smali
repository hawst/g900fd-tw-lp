.class public Lcom/android/nfc/cardemulation/DynamicService;
.super Ljava/lang/Object;
.source "DynamicService.java"


# static fields
.field static final CATEGORY_OTHER:Ljava/lang/String; = "other"

.field static final CATEGORY_PAYMENT:Ljava/lang/String; = "payment"

.field static final TAG:Ljava/lang/String; = "DynamicService"


# instance fields
.field private final RETURN_STATIC_AND_DYNAMIC_SERVICES:Z

.field dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

.field final mContext:Landroid/content/Context;

.field private final mOffHostService:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/gsma/services/nfc/OffHostService;",
            ">;>;"
        }
    .end annotation
.end field

.field final mReceiver:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Landroid/content/BroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field final mServiceCache:Lcom/android/nfc/cardemulation/RegisteredServicesCache;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/nfc/cardemulation/RegisteredServicesCache;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceCache"    # Lcom/android/nfc/cardemulation/RegisteredServicesCache;

    .prologue
    const/4 v4, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->mOffHostService:Ljava/util/LinkedHashMap;

    .line 29
    iput-object v4, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->RETURN_STATIC_AND_DYNAMIC_SERVICES:Z

    .line 39
    iput-object p1, p0, Lcom/android/nfc/cardemulation/DynamicService;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/android/nfc/cardemulation/DynamicService;->mServiceCache:Lcom/android/nfc/cardemulation/RegisteredServicesCache;

    .line 42
    new-instance v11, Lcom/android/nfc/cardemulation/DynamicService$1;

    invoke-direct {v11, p0}, Lcom/android/nfc/cardemulation/DynamicService$1;-><init>(Lcom/android/nfc/cardemulation/DynamicService;)V

    .line 69
    .local v11, "receiver":Landroid/content/BroadcastReceiver;
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v11}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->mReceiver:Ljava/util/concurrent/atomic/AtomicReference;

    .line 71
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 72
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 73
    const-string v0, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    const-string v0, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    const-string v0, "android.intent.action.PACKAGE_FIRST_LAUNCH"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    const-string v0, "android.intent.action.PACKAGE_RESTARTED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string v0, "package"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/nfc/cardemulation/DynamicService;->mReceiver:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 82
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 83
    .local v8, "sdFilter":Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    const-string v0, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    iget-object v5, p0, Lcom/android/nfc/cardemulation/DynamicService;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->mReceiver:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/BroadcastReceiver;

    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v9, v4

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 87
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    .line 90
    :cond_0
    return-void
.end method

.method static SetDefaultOffHostService(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "virtualClass"    # Ljava/lang/String;

    .prologue
    .line 346
    if-nez p0, :cond_0

    .line 347
    const-string v1, "DynamicService"

    const-string v2, "context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const/4 v1, 0x0

    .line 355
    :goto_0
    return v1

    .line 350
    :cond_0
    invoke-static {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-result-object v0

    .line 351
    .local v0, "dbHandler":Lcom/android/nfc/cardemulation/DynamicDBManager;
    if-eqz v0, :cond_1

    .line 352
    invoke-virtual {v0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->setDefaultOffHostService(Ljava/lang/String;)Z

    .line 353
    invoke-virtual {v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->close()V

    .line 355
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/nfc/cardemulation/DynamicService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/nfc/cardemulation/DynamicService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/android/nfc/cardemulation/DynamicService;->removeDynamicServices(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static buildApduServiceInfo(Lcom/gsma/services/nfc/OffHostService;)Landroid/nfc/cardemulation/ApduServiceInfo;
    .locals 23
    .param p0, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 93
    invoke-virtual/range {p0 .. p0}, Lcom/gsma/services/nfc/OffHostService;->getAidGroups()[Lcom/gsma/services/nfc/AidGroup;

    move-result-object v18

    .line 94
    .local v18, "groups":[Lcom/gsma/services/nfc/AidGroup;
    if-eqz v18, :cond_0

    move-object/from16 v0, v18

    array-length v3, v0

    if-nez v3, :cond_1

    .line 95
    :cond_0
    const-string v3, "DynamicService"

    const-string v5, "empty group"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v2, 0x0

    .line 115
    :goto_0
    return-object v2

    .line 98
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v4, "aidGroups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/AidGroup;>;"
    move-object/from16 v14, v18

    .local v14, "arr$":[Lcom/gsma/services/nfc/AidGroup;
    array-length v0, v14

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v19, 0x0

    .local v19, "i$":I
    move/from16 v20, v19

    .end local v14    # "arr$":[Lcom/gsma/services/nfc/AidGroup;
    .end local v19    # "i$":I
    .end local v21    # "len$":I
    .local v20, "i$":I
    :goto_1
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    aget-object v17, v14, v20

    .line 100
    .local v17, "g":Lcom/gsma/services/nfc/AidGroup;
    invoke-virtual/range {v17 .. v17}, Lcom/gsma/services/nfc/AidGroup;->getAids()[Ljava/lang/String;

    move-result-object v12

    .line 101
    .local v12, "aidList":[Ljava/lang/String;
    if-eqz v12, :cond_2

    array-length v3, v12

    if-nez v3, :cond_3

    .line 99
    .end local v20    # "i$":I
    :cond_2
    :goto_2
    add-int/lit8 v19, v20, 0x1

    .restart local v19    # "i$":I
    move/from16 v20, v19

    .end local v19    # "i$":I
    .restart local v20    # "i$":I
    goto :goto_1

    .line 104
    :cond_3
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v13, "aids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v15, v12

    .local v15, "arr$":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v22, v0

    .local v22, "len$":I
    const/16 v19, 0x0

    .end local v20    # "i$":I
    .restart local v19    # "i$":I
    :goto_3
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    aget-object v10, v15, v19

    .line 106
    .local v10, "a":Ljava/lang/String;
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    .line 108
    .end local v10    # "a":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v17 .. v17}, Lcom/gsma/services/nfc/AidGroup;->getCategory()Ljava/lang/String;

    move-result-object v16

    .line 109
    .local v16, "category":Ljava/lang/String;
    new-instance v11, Landroid/nfc/cardemulation/AidGroup;

    invoke-virtual/range {v17 .. v17}, Lcom/gsma/services/nfc/AidGroup;->getDescription()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v11, v13, v0, v3}, Landroid/nfc/cardemulation/AidGroup;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .local v11, "aidGroup":Landroid/nfc/cardemulation/AidGroup;
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 112
    .end local v11    # "aidGroup":Landroid/nfc/cardemulation/AidGroup;
    .end local v12    # "aidList":[Ljava/lang/String;
    .end local v13    # "aids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v15    # "arr$":[Ljava/lang/String;
    .end local v16    # "category":Ljava/lang/String;
    .end local v17    # "g":Lcom/gsma/services/nfc/AidGroup;
    .end local v19    # "i$":I
    .end local v22    # "len$":I
    .restart local v20    # "i$":I
    :cond_5
    new-instance v2, Landroid/nfc/cardemulation/ApduServiceInfo;

    invoke-virtual/range {p0 .. p0}, Lcom/gsma/services/nfc/OffHostService;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/gsma/services/nfc/OffHostService;->getBanner()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/gsma/services/nfc/OffHostService;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getVirtualClassName()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Landroid/nfc/cardemulation/ApduServiceInfo;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Landroid/graphics/drawable/Drawable;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .local v2, "info":Landroid/nfc/cardemulation/ApduServiceInfo;
    goto :goto_0
.end method

.method private getStaticService(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 30
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/gsma/services/nfc/OffHostService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicService;->mServiceCache:Lcom/android/nfc/cardemulation/RegisteredServicesCache;

    move-object/from16 v27, v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Lcom/android/nfc/cardemulation/RegisteredServicesCache;->getServices(I)Ljava/util/List;

    move-result-object v26

    .line 273
    .local v26, "staticServices":Ljava/util/List;, "Ljava/util/List<Landroid/nfc/cardemulation/ApduServiceInfo;>;"
    if-eqz v26, :cond_0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v27

    if-nez v27, :cond_2

    .line 274
    :cond_0
    const/16 v22, 0x0

    .line 332
    :cond_1
    :goto_0
    return-object v22

    .line 276
    :cond_2
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v20, "myServices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/ApduServiceInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v10

    .line 279
    .local v10, "dynamicServices":[Lcom/gsma/services/nfc/OffHostService;
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_3
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_8

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/nfc/cardemulation/ApduServiceInfo;

    .line 280
    .local v25, "ss":Landroid/nfc/cardemulation/ApduServiceInfo;
    const/16 v17, 0x0

    .line 281
    .local v17, "isDynamicService":Z
    invoke-virtual/range {v25 .. v25}, Landroid/nfc/cardemulation/ApduServiceInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 282
    if-eqz v10, :cond_4

    array-length v0, v10

    move/from16 v27, v0

    if-nez v27, :cond_5

    .line 283
    :cond_4
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 287
    :cond_5
    move-object v7, v10

    .local v7, "arr$":[Lcom/gsma/services/nfc/OffHostService;
    array-length v0, v7

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_2
    move/from16 v0, v18

    if-ge v15, v0, :cond_6

    aget-object v9, v7, v15

    .line 288
    .local v9, "ds":Lcom/gsma/services/nfc/OffHostService;
    invoke-virtual/range {v25 .. v25}, Landroid/nfc/cardemulation/ApduServiceInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v27

    invoke-virtual {v9}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getVirtualClassName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 289
    const/16 v17, 0x1

    .line 293
    .end local v9    # "ds":Lcom/gsma/services/nfc/OffHostService;
    :cond_6
    if-nez v17, :cond_3

    .line 294
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 287
    .restart local v9    # "ds":Lcom/gsma/services/nfc/OffHostService;
    :cond_7
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 299
    .end local v7    # "arr$":[Lcom/gsma/services/nfc/OffHostService;
    .end local v9    # "ds":Lcom/gsma/services/nfc/OffHostService;
    .end local v15    # "i$":I
    .end local v17    # "isDynamicService":Z
    .end local v18    # "len$":I
    .end local v25    # "ss":Landroid/nfc/cardemulation/ApduServiceInfo;
    :cond_8
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_9

    .line 300
    const/16 v22, 0x0

    goto :goto_0

    .line 303
    :cond_9
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 304
    .local v22, "myStaticServices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/gsma/services/nfc/OffHostService;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/nfc/cardemulation/ApduServiceInfo;

    .line 305
    .local v19, "myService":Landroid/nfc/cardemulation/ApduServiceInfo;
    invoke-virtual/range {v19 .. v19}, Landroid/nfc/cardemulation/ApduServiceInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    .line 306
    .local v8, "componentName":Landroid/content/ComponentName;
    invoke-virtual/range {v19 .. v19}, Landroid/nfc/cardemulation/ApduServiceInfo;->getSEInfo()Landroid/nfc/cardemulation/ApduServiceInfo$SecureElementInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/nfc/cardemulation/ApduServiceInfo$SecureElementInfo;->getSeId()I

    move-result v23

    .line 307
    .local v23, "seId":I
    const-string v24, "DH"

    .line 308
    .local v24, "seName":Ljava/lang/String;
    const/16 v27, 0x1

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_c

    .line 309
    const-string v24, "eSE"

    .line 313
    :cond_a
    :goto_4
    new-instance v21, Lcom/gsma/services/nfc/OffHostService;

    invoke-virtual/range {v19 .. v19}, Landroid/nfc/cardemulation/ApduServiceInfo;->getDescription()Ljava/lang/String;

    move-result-object v27

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move-object/from16 v2, v24

    move-object/from16 v3, v28

    move-object/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/gsma/services/nfc/OffHostService;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    .local v21, "myStaticService":Lcom/gsma/services/nfc/OffHostService;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicService;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v27

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/nfc/cardemulation/ApduServiceInfo;->loadBanner(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/gsma/services/nfc/OffHostService;->setBanner(Landroid/graphics/drawable/Drawable;)V

    .line 315
    invoke-virtual/range {v19 .. v19}, Landroid/nfc/cardemulation/ApduServiceInfo;->getAidGroups()Ljava/util/ArrayList;

    move-result-object v13

    .line 316
    .local v13, "groups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/AidGroup;>;"
    if-eqz v13, :cond_b

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v27

    if-nez v27, :cond_d

    .line 317
    :cond_b
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 310
    .end local v13    # "groups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/AidGroup;>;"
    .end local v21    # "myStaticService":Lcom/gsma/services/nfc/OffHostService;
    :cond_c
    const/16 v27, 0x2

    move/from16 v0, v23

    move/from16 v1, v27

    if-ne v0, v1, :cond_a

    .line 311
    const-string v24, "SIM"

    goto :goto_4

    .line 321
    .restart local v13    # "groups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/AidGroup;>;"
    .restart local v21    # "myStaticService":Lcom/gsma/services/nfc/OffHostService;
    :cond_d
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_e
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_f

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/nfc/cardemulation/AidGroup;

    .line 322
    .local v11, "g":Landroid/nfc/cardemulation/AidGroup;
    const-string v27, "static service"

    invoke-virtual {v11}, Landroid/nfc/cardemulation/AidGroup;->getCategory()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/gsma/services/nfc/OffHostService;->defineAidGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/gsma/services/nfc/AidGroup;

    move-result-object v12

    .line 323
    .local v12, "group":Lcom/gsma/services/nfc/AidGroup;
    invoke-virtual {v11}, Landroid/nfc/cardemulation/AidGroup;->getAids()Ljava/util/List;

    move-result-object v6

    .line 324
    .local v6, "aids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_e

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v27

    if-eqz v27, :cond_e

    .line 326
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_e

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 327
    .local v5, "aid":Ljava/lang/String;
    invoke-virtual {v12, v5}, Lcom/gsma/services/nfc/AidGroup;->addNewAid(Ljava/lang/String;)V

    goto :goto_5

    .line 330
    .end local v5    # "aid":Ljava/lang/String;
    .end local v6    # "aids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v11    # "g":Landroid/nfc/cardemulation/AidGroup;
    .end local v12    # "group":Lcom/gsma/services/nfc/AidGroup;
    .end local v16    # "i$":Ljava/util/Iterator;
    :cond_f
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3
.end method

.method public static readDefaultOffHostServices(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/nfc/cardemulation/ApduServiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    invoke-static {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-result-object v1

    .line 144
    .local v1, "dbHandler":Lcom/android/nfc/cardemulation/DynamicDBManager;
    if-nez v1, :cond_0

    .line 145
    invoke-static {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-result-object v1

    .line 147
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/ApduServiceInfo;>;"
    invoke-virtual {v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getPackageList()[Ljava/lang/String;

    move-result-object v6

    .line 149
    .local v6, "pkgList":[Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 150
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    .line 151
    .local v5, "pkg":Ljava/lang/String;
    invoke-virtual {v1, v5}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getDefaultOffHostService(Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;

    move-result-object v7

    .line 152
    .local v7, "service":Lcom/gsma/services/nfc/OffHostService;
    if-eqz v7, :cond_1

    .line 153
    invoke-static {v7}, Lcom/android/nfc/cardemulation/DynamicService;->buildApduServiceInfo(Lcom/gsma/services/nfc/OffHostService;)Landroid/nfc/cardemulation/ApduServiceInfo;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 157
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v7    # "service":Lcom/gsma/services/nfc/OffHostService;
    :cond_2
    invoke-virtual {v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->close()V

    .line 158
    return-object v4
.end method

.method public static readOffHostServices(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/nfc/cardemulation/ApduServiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-result-object v2

    .line 120
    .local v2, "dbHandler":Lcom/android/nfc/cardemulation/DynamicDBManager;
    if-nez v2, :cond_0

    .line 121
    invoke-static {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;

    move-result-object v2

    .line 123
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/ApduServiceInfo;>;"
    invoke-virtual {v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getPackageList()[Ljava/lang/String;

    move-result-object v9

    .line 125
    .local v9, "pkgList":[Ljava/lang/String;
    if-eqz v9, :cond_2

    .line 126
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v8, v0, v4

    .line 128
    .local v8, "pkg":Ljava/lang/String;
    invoke-virtual {v2, v8}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v11

    .line 129
    .local v11, "services":[Lcom/gsma/services/nfc/OffHostService;
    if-eqz v11, :cond_1

    array-length v12, v11

    if-lez v12, :cond_1

    .line 130
    move-object v1, v11

    .local v1, "arr$":[Lcom/gsma/services/nfc/OffHostService;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .end local v4    # "i$":I
    .restart local v3    # "i$":I
    :goto_1
    if-ge v3, v6, :cond_1

    aget-object v10, v1, v3

    .line 131
    .local v10, "s":Lcom/gsma/services/nfc/OffHostService;
    invoke-static {v10}, Lcom/android/nfc/cardemulation/DynamicService;->buildApduServiceInfo(Lcom/gsma/services/nfc/OffHostService;)Landroid/nfc/cardemulation/ApduServiceInfo;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 126
    .end local v1    # "arr$":[Lcom/gsma/services/nfc/OffHostService;
    .end local v3    # "i$":I
    .end local v6    # "len$":I
    .end local v10    # "s":Lcom/gsma/services/nfc/OffHostService;
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .restart local v3    # "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto :goto_0

    .line 138
    .end local v4    # "i$":I
    .end local v8    # "pkg":Ljava/lang/String;
    .end local v11    # "services":[Lcom/gsma/services/nfc/OffHostService;
    :cond_2
    invoke-virtual {v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->close()V

    .line 139
    return-object v7
.end method

.method private removeDynamicServices(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 234
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 235
    :cond_0
    const-string v0, "DynamicService"

    const-string v1, "package is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const/4 v0, 0x0

    .line 239
    :goto_0
    return v0

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->removeOffHostServices(Ljava/lang/String;)I

    .line 239
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addOffHostService(Lcom/gsma/services/nfc/OffHostService;)Z
    .locals 14
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 182
    if-nez p1, :cond_0

    .line 183
    const/4 v12, 0x0

    .line 222
    :goto_0
    return v12

    .line 185
    :cond_0
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 186
    .local v11, "pkg":Ljava/lang/String;
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_2

    .line 187
    :cond_1
    const-string v12, "DynamicService"

    const-string v13, "package is null"

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    const/4 v12, 0x0

    goto :goto_0

    .line 190
    :cond_2
    const/4 v6, 0x1

    .line 191
    .local v6, "insert":Z
    iget-object v12, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v12}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getAllOffHostServicesId()[I

    move-result-object v8

    .line 192
    .local v8, "list":[I
    if-eqz v8, :cond_3

    array-length v12, v8

    if-nez v12, :cond_5

    .line 193
    :cond_3
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setId(I)V

    .line 215
    :cond_4
    :goto_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getClassName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 216
    .local v10, "newName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v12

    invoke-virtual {v12, v10}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setVirtualClassName(Ljava/lang/String;)V

    .line 217
    if-eqz v6, :cond_a

    .line 218
    iget-object v12, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v12, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->insertOffHostService(Lcom/gsma/services/nfc/OffHostService;)I

    .line 222
    :goto_2
    const/4 v12, 0x1

    goto :goto_0

    .line 195
    .end local v10    # "newName":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v9

    .line 196
    .local v9, "newId":I
    if-nez v9, :cond_9

    .line 197
    const/16 v0, 0x3e8

    .line 198
    .local v0, "MAX":I
    const/16 v12, 0x3e8

    new-array v5, v12, [I

    .line 199
    .local v5, "ids":[I
    move-object v1, v8

    .local v1, "arr$":[I
    array-length v7, v1

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_3
    if-ge v3, v7, :cond_7

    aget v12, v1, v3

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 200
    .local v2, "i":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 201
    .local v4, "id":I
    if-lez v4, :cond_6

    const/16 v12, 0x3e8

    if-ge v4, v12, :cond_6

    .line 202
    const/4 v12, 0x1

    aput v12, v5, v4

    .line 199
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 205
    .end local v2    # "i":Ljava/lang/Integer;
    .end local v4    # "id":I
    :cond_7
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_4
    const/16 v12, 0x3e8

    if-ge v2, v12, :cond_4

    .line 206
    aget v12, v5, v2

    if-nez v12, :cond_8

    .line 207
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v12

    invoke-virtual {v12, v2}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setId(I)V

    goto :goto_1

    .line 205
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 212
    .end local v0    # "MAX":I
    .end local v1    # "arr$":[I
    .end local v2    # "i":I
    .end local v3    # "i$":I
    .end local v5    # "ids":[I
    .end local v7    # "len$":I
    :cond_9
    const/4 v6, 0x0

    goto :goto_1

    .line 220
    .end local v9    # "newId":I
    .restart local v10    # "newName":Ljava/lang/String;
    :cond_a
    iget-object v12, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v12, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->updateOffHostService(Lcom/gsma/services/nfc/OffHostService;)I

    goto :goto_2
.end method

.method public checkTableFull(Lcom/gsma/services/nfc/OffHostService;I)Z
    .locals 17
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;
    .param p2, "max"    # I

    .prologue
    .line 369
    const/4 v7, 0x0

    .line 370
    .local v7, "count":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .local v3, "aidList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getAidGroups()[Lcom/gsma/services/nfc/AidGroup;

    move-result-object v2

    .line 372
    .local v2, "aidGroup":[Lcom/gsma/services/nfc/AidGroup;
    if-eqz v2, :cond_0

    array-length v14, v2

    if-nez v14, :cond_1

    .line 373
    :cond_0
    const-string v14, "DynamicService"

    const-string v15, "aidGroup is null"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const/4 v14, 0x0

    .line 398
    :goto_0
    return v14

    .line 376
    :cond_1
    move-object v5, v2

    .local v5, "arr$":[Lcom/gsma/services/nfc/AidGroup;
    array-length v12, v5

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move v11, v10

    .end local v5    # "arr$":[Lcom/gsma/services/nfc/AidGroup;
    .end local v10    # "i$":I
    .end local v12    # "len$":I
    .local v11, "i$":I
    :goto_1
    if-ge v11, v12, :cond_5

    aget-object v8, v5, v11

    .line 377
    .local v8, "group":Lcom/gsma/services/nfc/AidGroup;
    invoke-virtual {v8}, Lcom/gsma/services/nfc/AidGroup;->getAids()[Ljava/lang/String;

    move-result-object v4

    .line 378
    .local v4, "aids":[Ljava/lang/String;
    if-eqz v4, :cond_2

    array-length v14, v4

    if-nez v14, :cond_4

    .line 379
    :cond_2
    const-string v14, "DynamicService"

    const-string v15, "aids is null"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    .end local v11    # "i$":I
    :cond_3
    add-int/lit8 v10, v11, 0x1

    .restart local v10    # "i$":I
    move v11, v10

    .end local v10    # "i$":I
    .restart local v11    # "i$":I
    goto :goto_1

    .line 382
    :cond_4
    move-object v6, v4

    .local v6, "arr$":[Ljava/lang/String;
    array-length v13, v6

    .local v13, "len$":I
    const/4 v10, 0x0

    .end local v11    # "i$":I
    .restart local v10    # "i$":I
    :goto_2
    if-ge v10, v13, :cond_3

    aget-object v1, v6, v10

    .line 383
    .local v1, "aid":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 386
    .end local v1    # "aid":Ljava/lang/String;
    .end local v4    # "aids":[Ljava/lang/String;
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v8    # "group":Lcom/gsma/services/nfc/AidGroup;
    .end local v10    # "i$":I
    .end local v13    # "len$":I
    .restart local v11    # "i$":I
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 387
    const-string v14, "DynamicService"

    const-string v15, "empty aid"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const/4 v14, 0x0

    goto :goto_0

    .line 390
    :cond_6
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v9, v14, :cond_7

    .line 391
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 392
    .restart local v1    # "aid":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    add-int/lit8 v14, v14, 0x4

    add-int/2addr v7, v14

    .line 390
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 394
    .end local v1    # "aid":Ljava/lang/String;
    :cond_7
    const-string v14, "DynamicService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "max = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", size = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    move/from16 v0, p2

    if-le v7, v0, :cond_8

    .line 396
    const/4 v14, 0x1

    goto :goto_0

    .line 398
    :cond_8
    const/4 v14, 0x0

    goto :goto_0
.end method

.method public commit()V
    .locals 13

    .prologue
    .line 162
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/nfc/cardemulation/ApduServiceInfo;>;"
    iget-object v11, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v11}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getPackageList()[Ljava/lang/String;

    move-result-object v8

    .line 164
    .local v8, "pkgList":[Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 165
    move-object v0, v8

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v7, v0, v3

    .line 169
    .local v7, "pkg":Ljava/lang/String;
    iget-object v11, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v11, v7}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v10

    .line 170
    .local v10, "services":[Lcom/gsma/services/nfc/OffHostService;
    if-eqz v10, :cond_0

    array-length v11, v10

    if-lez v11, :cond_0

    .line 171
    move-object v1, v10

    .local v1, "arr$":[Lcom/gsma/services/nfc/OffHostService;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .end local v3    # "i$":I
    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v9, v1, v2

    .line 172
    .local v9, "s":Lcom/gsma/services/nfc/OffHostService;
    invoke-static {v9}, Lcom/android/nfc/cardemulation/DynamicService;->buildApduServiceInfo(Lcom/gsma/services/nfc/OffHostService;)Landroid/nfc/cardemulation/ApduServiceInfo;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 165
    .end local v1    # "arr$":[Lcom/gsma/services/nfc/OffHostService;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v9    # "s":Lcom/gsma/services/nfc/OffHostService;
    :cond_0
    add-int/lit8 v2, v3, 0x1

    .restart local v2    # "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_0

    .line 177
    .end local v3    # "i$":I
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v10    # "services":[Lcom/gsma/services/nfc/OffHostService;
    :cond_1
    iget-object v11, p0, Lcom/android/nfc/cardemulation/DynamicService;->mServiceCache:Lcom/android/nfc/cardemulation/RegisteredServicesCache;

    invoke-virtual {v11}, Lcom/android/nfc/cardemulation/RegisteredServicesCache;->clearDynamicService()V

    .line 178
    iget-object v11, p0, Lcom/android/nfc/cardemulation/DynamicService;->mServiceCache:Lcom/android/nfc/cardemulation/RegisteredServicesCache;

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v12

    invoke-virtual {v11, v12, v6}, Lcom/android/nfc/cardemulation/RegisteredServicesCache;->updateDynamicServices(ILjava/util/ArrayList;)Z

    .line 179
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->close()V

    .line 35
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 36
    return-void
.end method

.method public getDefaultOffHostService(Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 359
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 360
    :cond_0
    const-string v1, "DynamicService"

    const-string v2, "package is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v0, 0x0

    .line 365
    :goto_0
    return-object v0

    .line 363
    :cond_1
    const/4 v0, 0x0

    .line 364
    .local v0, "service":Lcom/gsma/services/nfc/OffHostService;
    iget-object v1, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getDefaultOffHostService()Lcom/gsma/services/nfc/OffHostService;

    move-result-object v0

    .line 365
    goto :goto_0
.end method

.method public getOffHostServiceSize()I
    .locals 4

    .prologue
    .line 336
    iget-object v1, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getAllOffHostServicesId()[I

    move-result-object v0

    .line 337
    .local v0, "services":[I
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 338
    :cond_0
    const-string v1, "DynamicService"

    const-string v2, "size is 0"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    const/4 v1, 0x0

    .line 342
    :goto_0
    return v1

    .line 341
    :cond_1
    const-string v1, "DynamicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    array-length v1, v0

    goto :goto_0
.end method

.method public getOffHostServices(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/gsma/services/nfc/OffHostService;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 243
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 244
    :cond_0
    const-string v8, "DynamicService"

    const-string v9, "package is null"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v7

    .line 268
    :cond_1
    :goto_0
    return-object v3

    .line 247
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/gsma/services/nfc/OffHostService;>;"
    const/4 v6, 0x0

    .line 253
    .local v6, "staticServices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/gsma/services/nfc/OffHostService;>;"
    if-eqz v6, :cond_3

    .line 254
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 256
    :cond_3
    iget-object v8, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v8, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v5

    .line 257
    .local v5, "services":[Lcom/gsma/services/nfc/OffHostService;
    if-eqz v5, :cond_4

    array-length v8, v5

    if-nez v8, :cond_5

    .line 258
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_1

    move-object v3, v7

    .line 259
    goto :goto_0

    .line 264
    :cond_5
    move-object v0, v5

    .local v0, "arr$":[Lcom/gsma/services/nfc/OffHostService;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 265
    .local v4, "s":Lcom/gsma/services/nfc/OffHostService;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public invalidateCache(I)V
    .locals 1
    .param p1, "userId"    # I

    .prologue
    .line 402
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->mServiceCache:Lcom/android/nfc/cardemulation/RegisteredServicesCache;

    invoke-virtual {v0, p1}, Lcom/android/nfc/cardemulation/RegisteredServicesCache;->invalidateCache(I)V

    .line 403
    return-void
.end method

.method public removeOffHostService(Lcom/gsma/services/nfc/OffHostService;)Z
    .locals 1
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 226
    if-nez p1, :cond_0

    .line 227
    const/4 v0, 0x0

    .line 230
    :goto_0
    return v0

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicService;->dbHandler:Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-virtual {v0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->removeOffHostService(Lcom/gsma/services/nfc/OffHostService;)I

    .line 230
    const/4 v0, 0x1

    goto :goto_0
.end method
