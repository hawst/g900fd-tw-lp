.class public Lcom/android/nfc/cardemulation/RoutingEntryCache;
.super Ljava/lang/Object;
.source "RoutingEntryCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;
    }
.end annotation


# static fields
.field static final DBG:Z

.field public static final PREFS_DEFAULT_ROUTE_BACKUP:Ljava/lang/String; = "defaultRouteBackUp"

.field public static final PREFS_DEFAULT_ROUTE_SETTING:Ljava/lang/String; = "defaultRouteDest"

.field public static final PREFS_DEFAULT_TECH_ROUTE_SETTING:Ljava/lang/String; = "defaultTechRouteDest"

.field public static final PRIOIRTY_OFFHOST_ROUTE:Ljava/lang/String; = "ROUTE_TO_PRIOIRTY_OFFHOST_ROUTE"

.field public static final ROUTE_TO_LISTEN_PROTO_ISO_DEP:Ljava/lang/String; = "ROUTE_TO_LISTEN_PROTO_ISO_DEP"

.field public static final ROUTE_TO_LISTEN_PROTO_NFC_DEP:Ljava/lang/String; = "ROUTE_TO_LISTEN_PROTO_NFC_DEP"

.field public static final ROUTE_TO_LISTEN_TECH:Ljava/lang/String; = "ROUTE_TO_LISTEN_TECH"

.field static final TAG:Ljava/lang/String; = "NfcroutingManager"


# instance fields
.field private final FIRST_SETTING:Ljava/lang/String;

.field private final SAMSUNG_NFC_PREF:Ljava/lang/String;

.field mCallback:Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;

.field mChanged:Z

.field mContext:Landroid/content/Context;

.field mDefaultRouteList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mDirty:Z

.field mListenMode:I

.field mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

.field mNfcEnabled:Z

.field mPrefs:Landroid/content/SharedPreferences;

.field mSamsungPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/android/nfc/cardemulation/AidRoutingManager;->DBG:Z

    sput-boolean v0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEnabled:Z

    .line 88
    const-string v0, "First"

    iput-object v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->FIRST_SETTING:Ljava/lang/String;

    .line 89
    const-string v0, "SamsungNfcPrefs"

    iput-object v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->SAMSUNG_NFC_PREF:Ljava/lang/String;

    .line 108
    iput-object p1, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mContext:Landroid/content/Context;

    .line 109
    invoke-static {}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getInstance()Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    .line 110
    iput-object p2, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mCallback:Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;

    .line 113
    return-void
.end method


# virtual methods
.method public enableDisableTestMode(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p1, "nfcEeName"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 329
    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    invoke-virtual {v4, p1}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v4

    if-nez v4, :cond_2

    .line 330
    :cond_0
    sget-boolean v4, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    if-eqz v4, :cond_1

    const-string v4, "NfcroutingManager"

    const-string v5, "Warnig - This values cannot be NULL"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_1
    const/4 v3, 0x0

    .line 361
    :goto_0
    return-object v3

    .line 335
    :cond_2
    move-object v3, p1

    .line 337
    .local v3, "routeValue":Ljava/lang/String;
    sget-boolean v4, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    if-eqz v4, :cond_3

    const-string v4, "NfcroutingManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "testMode - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " route - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_3
    if-eqz p2, :cond_5

    .line 341
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    const-string v5, "defaultRouteBackUp"

    const-string v6, "ENABLE_TEST"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344
    .local v1, "enableTestMode":Ljava/lang/String;
    const-string v4, "ENABLE_TEST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 346
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    const-string v5, "defaultRouteDest"

    invoke-interface {v4, v5, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 347
    .local v0, "currenDefault":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "defaultRouteBackUp"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 348
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 351
    .end local v0    # "currenDefault":Ljava/lang/String;
    :cond_4
    move-object v3, p1

    .line 352
    goto :goto_0

    .line 354
    .end local v1    # "enableTestMode":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    const-string v5, "defaultRouteBackUp"

    invoke-interface {v4, v5, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 355
    .local v2, "restoreDefault":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "defaultRouteDest"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 356
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "defaultRouteBackUp"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 357
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 358
    move-object v3, v2

    goto/16 :goto_0
.end method

.method public getRoutingDestination(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "routeType"    # Ljava/lang/String;

    .prologue
    .line 318
    iget-object v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public initialize()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 117
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mContext:Landroid/content/Context;

    const-string v9, "NfcServicePrefs"

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mPrefs:Landroid/content/SharedPreferences;

    .line 118
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mContext:Landroid/content/Context;

    const-string v9, "SamsungNfcPrefs"

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    .line 129
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v8

    const-string v9, "CscFeature_NFC_DefaultCardModeConfig"

    const-string v10, "UICC"

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "defaultCardModeConfig":Ljava/lang/String;
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v8, ":"

    invoke-direct {v5, v0, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .local v5, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 136
    .local v3, "isoDepRouteFromCsc":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 140
    .local v6, "techRouteFromCsc":Ljava/lang/String;
    :goto_0
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    const-string v9, "defaultRouteDest"

    const-string v10, "First"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 141
    .local v4, "isoRouteFromPref":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    const-string v9, "defaultTechRouteDest"

    const-string v10, "First"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 154
    .local v7, "techRouteFromPref":Ljava/lang/String;
    const-string v8, "First"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 156
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mPrefs:Landroid/content/SharedPreferences;

    const-string v9, "defaultRouteDest"

    const-string v10, "First"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 158
    const-string v8, "First"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 159
    move-object v1, v3

    .line 166
    .local v1, "defaultIsoDepRouteValues":Ljava/lang/String;
    :goto_1
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    invoke-virtual {v8, v1}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v8

    if-nez v8, :cond_0

    .line 167
    const-string v1, "DH"

    .line 171
    :cond_0
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "defaultRouteDest"

    invoke-interface {v8, v9, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 178
    :goto_2
    const-string v8, "First"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 181
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mPrefs:Landroid/content/SharedPreferences;

    const-string v9, "defaultTechRouteDest"

    const-string v10, "First"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 184
    const-string v8, "First"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 185
    move-object v2, v6

    .line 192
    .local v2, "defaultTechRouteVaues":Ljava/lang/String;
    :goto_3
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    invoke-virtual {v8, v2}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v8

    if-nez v8, :cond_1

    .line 193
    const-string v2, "DH"

    .line 197
    :cond_1
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "defaultTechRouteDest"

    invoke-interface {v8, v9, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 204
    :goto_4
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "defaultRouteDest"

    invoke-interface {v8, v9}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 205
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "defaultTechRouteDest"

    invoke-interface {v8, v9}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 208
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    const-string v9, "ROUTE_TO_LISTEN_PROTO_ISO_DEP"

    invoke-virtual {v8, v9, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    const-string v9, "ROUTE_TO_LISTEN_TECH"

    invoke-virtual {v8, v9, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v8, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    const-string v9, "ROUTE_TO_LISTEN_PROTO_NFC_DEP"

    const-string v10, "DH"

    invoke-virtual {v8, v9, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-void

    .end local v1    # "defaultIsoDepRouteValues":Ljava/lang/String;
    .end local v2    # "defaultTechRouteVaues":Ljava/lang/String;
    .end local v4    # "isoRouteFromPref":Ljava/lang/String;
    .end local v6    # "techRouteFromCsc":Ljava/lang/String;
    .end local v7    # "techRouteFromPref":Ljava/lang/String;
    :cond_2
    move-object v6, v3

    .line 136
    goto/16 :goto_0

    .line 162
    .restart local v4    # "isoRouteFromPref":Ljava/lang/String;
    .restart local v6    # "techRouteFromCsc":Ljava/lang/String;
    .restart local v7    # "techRouteFromPref":Ljava/lang/String;
    :cond_3
    move-object v1, v4

    .restart local v1    # "defaultIsoDepRouteValues":Ljava/lang/String;
    goto/16 :goto_1

    .line 174
    .end local v1    # "defaultIsoDepRouteValues":Ljava/lang/String;
    :cond_4
    move-object v1, v4

    .restart local v1    # "defaultIsoDepRouteValues":Ljava/lang/String;
    goto :goto_2

    .line 188
    :cond_5
    move-object v2, v7

    .restart local v2    # "defaultTechRouteVaues":Ljava/lang/String;
    goto :goto_3

    .line 200
    .end local v2    # "defaultTechRouteVaues":Ljava/lang/String;
    :cond_6
    move-object v2, v7

    .restart local v2    # "defaultTechRouteVaues":Ljava/lang/String;
    goto :goto_4
.end method

.method public invalidate()V
    .locals 8

    .prologue
    .line 235
    const/4 v1, 0x0

    .line 238
    .local v1, "defaultRouteId":I
    invoke-static {}, Lcom/android/nfc/NfcService;->getInstance()Lcom/android/nfc/NfcService;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/android/nfc/NfcService;->clearTable(I)V

    .line 239
    invoke-static {}, Lcom/android/nfc/NfcService;->getInstance()Lcom/android/nfc/NfcService;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/nfc/NfcService;->clearTable(I)V

    .line 242
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    const-string v5, "ROUTE_TO_LISTEN_PROTO_ISO_DEP"

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 243
    .local v0, "defaultIsoDepRouteValues":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    const-string v5, "ROUTE_TO_LISTEN_TECH"

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 247
    .local v2, "defaultTechRouteVaues":Ljava/lang/String;
    sget-boolean v4, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    if-eqz v4, :cond_0

    .line 248
    const-string v4, "NfcroutingManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ISO_DEP : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "TECH : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :cond_0
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    invoke-virtual {v4, v0}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v3

    .line 254
    .local v3, "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    if-nez v3, :cond_2

    .line 255
    sget-boolean v4, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    if-eqz v4, :cond_1

    const-string v4, "NfcroutingManager"

    const-string v5, "ISO_DEP object is null, so use HCE"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :cond_1
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    const-string v5, "DH"

    invoke-virtual {v4, v5}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v3

    .line 259
    :cond_2
    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getID()I

    move-result v1

    .line 261
    invoke-static {}, Lcom/android/nfc/NfcService;->getInstance()Lcom/android/nfc/NfcService;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getListenProtocol()I

    move-result v5

    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getID()I

    move-result v6

    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getSupportedExtendedPowerState()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/nfc/NfcService;->routeProtocol(III)V

    .line 266
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    invoke-virtual {v4, v2}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v3

    .line 267
    if-nez v3, :cond_4

    .line 268
    sget-boolean v4, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    if-eqz v4, :cond_3

    const-string v4, "NfcroutingManager"

    const-string v5, "TECH object is null, so use HCE"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_3
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    const-string v5, "DH"

    invoke-virtual {v4, v5}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v3

    .line 272
    :cond_4
    invoke-static {}, Lcom/android/nfc/NfcService;->getInstance()Lcom/android/nfc/NfcService;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getListenTechnology()I

    move-result v5

    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getID()I

    move-result v6

    invoke-virtual {v3}, Lcom/android/nfc/cardemulation/NfcEeObject;->getSupportedExtendedPowerState()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/nfc/NfcService;->routeTechnology(III)V

    .line 280
    iget-object v4, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mCallback:Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;

    invoke-interface {v4, v1}, Lcom/android/nfc/cardemulation/RoutingEntryCache$Callback;->onDefaultRouteUpdated(I)V

    .line 281
    return-void
.end method

.method public isChanged()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mChanged:Z

    return v0
.end method

.method public onNfcDisabled()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEnabled:Z

    .line 221
    return-void
.end method

.method public onNfcEnabled()V
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEnabled:Z

    .line 216
    invoke-virtual {p0}, Lcom/android/nfc/cardemulation/RoutingEntryCache;->invalidate()V

    .line 217
    return-void
.end method

.method public setEnableRouting(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 323
    iput-boolean p1, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mChanged:Z

    .line 324
    return-void
.end method

.method public setRoutingDestination(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "routeType"    # Ljava/lang/String;
    .param p2, "nfcEeName"    # Ljava/lang/String;

    .prologue
    .line 285
    iget-object v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 286
    .local v1, "curNfcEeName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEeCache:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    invoke-virtual {v3, v1}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;

    move-result-object v2

    .line 287
    .local v2, "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    const/4 v0, 0x0

    .line 289
    .local v0, "changed":Z
    if-nez v2, :cond_1

    .line 290
    sget-boolean v3, Lcom/android/nfc/cardemulation/RoutingEntryCache;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "NfcroutingManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " It does not support "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :cond_0
    iget-boolean v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mChanged:Z

    .line 314
    :goto_0
    return v3

    .line 294
    :cond_1
    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 296
    iget-object v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mDefaultRouteList:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    const/4 v0, 0x1

    .line 300
    const-string v3, "ROUTE_TO_LISTEN_PROTO_ISO_DEP"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 301
    iget-object v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "defaultRouteDest"

    invoke-interface {v3, v4, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 302
    iget-boolean v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mNfcEnabled:Z

    if-eqz v3, :cond_2

    .line 303
    invoke-virtual {p0}, Lcom/android/nfc/cardemulation/RoutingEntryCache;->invalidate()V

    :cond_2
    :goto_1
    move v3, v0

    .line 314
    goto :goto_0

    .line 305
    :cond_3
    const-string v3, "ROUTE_TO_PRIOIRTY_OFFHOST_ROUTE"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 309
    const-string v3, "ROUTE_TO_LISTEN_TECH"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 310
    iget-object v3, p0, Lcom/android/nfc/cardemulation/RoutingEntryCache;->mSamsungPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "defaultTechRouteDest"

    invoke-interface {v3, v4, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method
