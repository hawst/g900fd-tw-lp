.class Lcom/android/nfc/cardemulation/DynamicService$1;
.super Landroid/content/BroadcastReceiver;
.source "DynamicService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/nfc/cardemulation/DynamicService;-><init>(Landroid/content/Context;Lcom/android/nfc/cardemulation/RegisteredServicesCache;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/cardemulation/DynamicService;


# direct methods
.method constructor <init>(Lcom/android/nfc/cardemulation/DynamicService;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/android/nfc/cardemulation/DynamicService$1;->this$0:Lcom/android/nfc/cardemulation/DynamicService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v6, -0x1

    .line 45
    const-string v5, "android.intent.extra.UID"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 46
    .local v4, "uid":I
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "action":Ljava/lang/String;
    if-eq v4, v6, :cond_2

    .line 48
    const-string v5, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 52
    .local v3, "replaced":Z
    :cond_1
    if-nez v3, :cond_3

    .line 53
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 54
    .local v1, "currentUser":I
    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    if-ne v1, v5, :cond_2

    .line 55
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 56
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "pkg":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/nfc/cardemulation/DynamicService$1;->this$0:Lcom/android/nfc/cardemulation/DynamicService;

    # invokes: Lcom/android/nfc/cardemulation/DynamicService;->removeDynamicServices(Ljava/lang/String;)Z
    invoke-static {v5, v2}, Lcom/android/nfc/cardemulation/DynamicService;->access$000(Lcom/android/nfc/cardemulation/DynamicService;Ljava/lang/String;)Z

    .line 67
    .end local v1    # "currentUser":I
    .end local v2    # "pkg":Ljava/lang/String;
    .end local v3    # "replaced":Z
    :cond_2
    :goto_0
    return-void

    .line 64
    .restart local v3    # "replaced":Z
    :cond_3
    const-string v5, "DynamicService"

    const-string v6, "Ignoring package intent due to package being replaced."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
