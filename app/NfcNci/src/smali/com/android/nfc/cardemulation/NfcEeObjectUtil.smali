.class public Lcom/android/nfc/cardemulation/NfcEeObjectUtil;
.super Ljava/lang/Object;
.source "NfcEeObjectUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc/cardemulation/NfcEeObjectUtil$Singleton;
    }
.end annotation


# static fields
.field static final DBG:Z

.field public static final MAX_NFCEE:I = 0x3

.field public static final NFCEE_DEVICE_HOST_ID:I = 0x0

.field public static final NFCEE_DEVICE_HOST_NAME:Ljava/lang/String; = "DH"

.field public static final NFCEE_SE_ESE_ID:I = 0x1

.field public static final NFCEE_SE_ESE_NAME:Ljava/lang/String; = "ESE"

.field public static final NFCEE_SE_UICC_ID:I = 0x2

.field public static final NFCEE_SE_UICC_NAME:Ljava/lang/String; = "UICC"

.field static final TAG:Ljava/lang/String; = "NfcroutingManager"


# instance fields
.field private mMinimumListenMode:I

.field mNfcEeObjectDB:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/nfc/cardemulation/NfcEeObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget-boolean v0, Lcom/android/nfc/cardemulation/AidRoutingManager;->DBG:Z

    sput-boolean v0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    .line 79
    return-void
.end method

.method private computePowerState(Ljava/lang/String;)I
    .locals 4
    .param p1, "routingMode"    # Ljava/lang/String;

    .prologue
    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, "powerState":I
    const-string v1, "ROUTE_OFF"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcroutingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "powerState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_1
    return v0

    .line 218
    :cond_2
    const/4 v0, 0x1

    .line 219
    const-string v1, "ROUTE_ON_ALWAYS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    or-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method private computeScreenState(Ljava/lang/String;)I
    .locals 4
    .param p1, "routingMode"    # Ljava/lang/String;

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 232
    .local v0, "screenState":I
    const-string v1, "ROUTE_OFF"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcroutingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "screenState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_1
    return v0

    .line 234
    :cond_2
    const-string v1, "ROUTE_ON_WHEN_SCREEN_UNLOCK"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 235
    const/16 v0, 0x20

    goto :goto_0

    .line 237
    :cond_3
    const-string v1, "ROUTE_ON_WHEN_SCREEN_ON"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 238
    const/16 v0, 0x30

    goto :goto_0

    .line 241
    :cond_4
    const-string v1, "ROUTE_ON_WHEN_POWER_ON"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 242
    const/16 v0, 0x38

    goto :goto_0

    .line 246
    :cond_5
    const-string v1, "ROUTE_ON_ALWAYS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    const/16 v0, 0x38

    goto :goto_0
.end method

.method public static getInstance()Lcom/android/nfc/cardemulation/NfcEeObjectUtil;
    .locals 1

    .prologue
    .line 86
    # getter for: Lcom/android/nfc/cardemulation/NfcEeObjectUtil$Singleton;->INSTANCE:Lcom/android/nfc/cardemulation/NfcEeObjectUtil;
    invoke-static {}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil$Singleton;->access$000()Lcom/android/nfc/cardemulation/NfcEeObjectUtil;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method computeRoutingMode(Ljava/lang/String;)I
    .locals 4
    .param p1, "routingMode"    # Ljava/lang/String;

    .prologue
    .line 258
    const/4 v0, 0x1

    .line 260
    .local v0, "mode":I
    const-string v1, "ROUTE_OFF"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 261
    const/4 v0, 0x1

    .line 276
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcroutingManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Routing Mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_1
    return v0

    .line 263
    :cond_2
    const-string v1, "ROUTE_ON_WHEN_SCREEN_UNLOCK"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 264
    const/4 v0, 0x3

    goto :goto_0

    .line 266
    :cond_3
    const-string v1, "ROUTE_ON_WHEN_SCREEN_ON"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 267
    const/4 v0, 0x2

    goto :goto_0

    .line 269
    :cond_4
    const-string v1, "ROUTE_ON_WHEN_POWER_ON"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 270
    const/4 v0, 0x4

    goto :goto_0

    .line 272
    :cond_5
    const-string v1, "ROUTE_ON_ALWAYS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public getObject(I)Lcom/android/nfc/cardemulation/NfcEeObject;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 179
    const/4 v2, 0x0

    .line 180
    .local v2, "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    iget-object v3, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 182
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 184
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 185
    .local v1, "nfcEeName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    check-cast v2, Lcom/android/nfc/cardemulation/NfcEeObject;

    .line 186
    .restart local v2    # "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    invoke-virtual {v2}, Lcom/android/nfc/cardemulation/NfcEeObject;->getID()I

    move-result v3

    if-ne v3, p1, :cond_0

    move-object v3, v2

    .line 189
    .end local v1    # "nfcEeName":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getObject(Ljava/lang/String;)Lcom/android/nfc/cardemulation/NfcEeObject;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/nfc/cardemulation/NfcEeObject;

    return-object v0
.end method

.method public getObjectAll()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/nfc/cardemulation/NfcEeObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v2, "nfcEeObjList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/nfc/cardemulation/NfcEeObject;>;"
    iget-object v3, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 197
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 199
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 200
    .local v1, "nfcEeName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 203
    .end local v1    # "nfcEeName":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method public insertObject(Lcom/android/nfc/cardemulation/NfcEeObject;)V
    .locals 2
    .param p1, "nfcEeObj"    # Lcom/android/nfc/cardemulation/NfcEeObject;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/android/nfc/cardemulation/NfcEeObject;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    return-void
.end method

.method public isSupportCardEmulation(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 207
    iget-object v1, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/nfc/cardemulation/NfcEeObject;

    .line 208
    .local v0, "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public makeNewObject(Ljava/lang/String;)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v2, 0x0

    .line 93
    .local v2, "id":I
    const/4 v6, 0x0

    .line 94
    .local v6, "listenTech":I
    const/4 v7, 0x0

    .line 95
    .local v7, "listenProto":I
    const-string v8, ""

    .line 96
    .local v8, "routingType":Ljava/lang/String;
    const/4 v4, 0x0

    .line 97
    .local v4, "powerState":I
    const/4 v5, 0x0

    .line 98
    .local v5, "screenState":I
    const/4 v3, 0x0

    .line 100
    .local v3, "routingMode":I
    const-string v1, "DH"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    const-string v1, "NfcroutingManager"

    const-string v9, "make new NFC eeobject :DH"

    invoke-static {v1, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v2, 0x0

    .line 104
    const-string v8, "ROUTE_ON_WHEN_SCREEN_ON"

    .line 106
    invoke-direct {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computePowerState(Ljava/lang/String;)I

    move-result v4

    .line 107
    invoke-direct {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computeScreenState(Ljava/lang/String;)I

    move-result v5

    .line 108
    invoke-virtual {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computeRoutingMode(Ljava/lang/String;)I

    move-result v3

    .line 110
    const/4 v6, 0x1

    .line 111
    const/4 v7, 0x1

    .line 161
    :goto_0
    new-instance v0, Lcom/android/nfc/cardemulation/NfcEeObject;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/android/nfc/cardemulation/NfcEeObject;-><init>(Ljava/lang/String;IIIIII)V

    .line 164
    .local v0, "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    iget-object v1, p0, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->mNfcEeObjectDB:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Lcom/android/nfc/cardemulation/NfcEeObject;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    .end local v0    # "nfcEeObj":Lcom/android/nfc/cardemulation/NfcEeObject;
    :cond_0
    :goto_1
    return-void

    .line 114
    :cond_1
    const-string v1, "ESE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 115
    const/4 v2, 0x1

    .line 118
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v9, "CscFeature_NFC_ConfigCardModeRoutingTypeForEse"

    const-string v10, "ROUTE_ON_WHEN_POWER_ON"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 122
    invoke-direct {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computePowerState(Ljava/lang/String;)I

    move-result v4

    .line 123
    invoke-direct {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computeScreenState(Ljava/lang/String;)I

    move-result v5

    .line 124
    invoke-virtual {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computeRoutingMode(Ljava/lang/String;)I

    move-result v3

    .line 126
    const/4 v6, 0x3

    .line 128
    const/4 v7, 0x1

    goto :goto_0

    .line 130
    :cond_2
    const-string v1, "UICC"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 131
    const/4 v2, 0x2

    .line 133
    const-string v8, "ROUTE_ON_ALWAYS"

    .line 135
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v9, "CscFeature_NFC_ConfigCardModeRoutingTypeForUicc"

    const-string v10, "ROUTE_ON_ALWAYS"

    invoke-virtual {v1, v9, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 140
    invoke-direct {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computePowerState(Ljava/lang/String;)I

    move-result v4

    .line 141
    invoke-direct {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computeScreenState(Ljava/lang/String;)I

    move-result v5

    .line 142
    invoke-virtual {p0, v8}, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->computeRoutingMode(Ljava/lang/String;)I

    move-result v3

    .line 144
    const-string v1, "TGY"

    const-string v9, "ro.csc.sales_code"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 145
    const/4 v6, 0x7

    .line 154
    :goto_2
    const/4 v7, 0x1

    goto :goto_0

    .line 150
    :cond_3
    const/4 v6, 0x3

    goto :goto_2

    .line 157
    :cond_4
    sget-boolean v1, Lcom/android/nfc/cardemulation/NfcEeObjectUtil;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcroutingManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is not supported. It is error"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
