.class public Lcom/android/nfc/cardemulation/DynamicDBManager;
.super Ljava/lang/Object;
.source "DynamicDBManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;
    }
.end annotation


# static fields
.field private static final AIDGROUP_TABLE:Ljava/lang/String; = "aidgroup"

.field private static final AID_TABLE:Ljava/lang/String; = "aid"

.field private static final DBNAME:Ljava/lang/String; = "db.dat"

.field private static final DBVERSION:I = 0x2

.field private static final DEFAULT_TABLE:Ljava/lang/String; = "defaultservice"

.field private static final KEY_AID_NAME:Ljava/lang/String; = "aid"

.field private static final KEY_BANNER_NAME:Ljava/lang/String; = "banner"

.field private static final KEY_CATEGORY_NAME:Ljava/lang/String; = "category"

.field private static final KEY_CLASS_NAME:Ljava/lang/String; = "class"

.field private static final KEY_DEFAULTID_NAME:Ljava/lang/String; = "defaultid"

.field private static final KEY_DEFAULT_NAME:Ljava/lang/String; = "defaultservice"

.field private static final KEY_DESCRIPTION_NAME:Ljava/lang/String; = "description"

.field private static final KEY_GROUPID_NAME:Ljava/lang/String; = "groupid"

.field private static final KEY_ID_NAME:Ljava/lang/String; = "id"

.field private static final KEY_LOCATION_NAME:Ljava/lang/String; = "location"

.field private static final KEY_NAME_NAME:Ljava/lang/String; = "name"

.field private static final KEY_OFFHOSTSERVICEID_NAME:Ljava/lang/String; = "offhostserviceid"

.field private static final KEY_PACKAGE_NAME:Ljava/lang/String; = "package"

.field private static final KEY_PRIMARY_KEY_NAME:Ljava/lang/String; = "_id"

.field private static final KEY_VIRTUALCLASS_NAME:Ljava/lang/String; = "virtualclass"

.field private static final OFFHOSTSERVICE_TABLE:Ljava/lang/String; = "offhostservice"

.field private static final PACKAGE_TABLE:Ljava/lang/String; = "package"

.field private static final TAG:Ljava/lang/String; = "DynamicDBManager"

.field private static final saveImg:Z = true


# instance fields
.field private final db:Landroid/database/sqlite/SQLiteDatabase;

.field private final helper:Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;

.field private final mContext:Landroid/content/Context;

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    .line 64
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mWindowManager:Landroid/view/WindowManager;

    .line 66
    new-instance v0, Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;

    const-string v1, "db.dat"

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->helper:Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;

    .line 67
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->helper:Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;

    invoke-virtual {v0}, Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 68
    return-void
.end method

.method private createOffHostServiceContentValues(Lcom/gsma/services/nfc/OffHostService;)Landroid/content/ContentValues;
    .locals 5
    .param p1, "s"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 632
    if-nez p1, :cond_0

    .line 633
    const-string v3, "DynamicDBManager"

    const-string v4, "service is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    const/4 v2, 0x0

    .line 657
    :goto_0
    return-object v2

    .line 636
    :cond_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 637
    .local v2, "v":Landroid/content/ContentValues;
    const-string v3, "virtualclass"

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getVirtualClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const-string v3, "package"

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    const-string v3, "class"

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    const-string v3, "id"

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 641
    const-string v3, "description"

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const-string v3, "location"

    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getBanner()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 645
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v3

    invoke-direct {p0, v3, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->saveBanner(ILandroid/graphics/drawable/Drawable;)Ljava/lang/String;

    move-result-object v1

    .line 646
    .local v1, "fileName":Ljava/lang/String;
    const-string v3, "banner"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteBanner(Ljava/lang/String;)Z
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 356
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 357
    :cond_0
    const-string v3, "DynamicDBManager"

    const-string v4, "name is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const/4 v2, 0x0

    .line 367
    :goto_0
    return v2

    .line 360
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 361
    .local v1, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 363
    .local v2, "ret":Z
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private enc(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1063
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 1064
    :cond_0
    const/4 v4, 0x0

    .line 1072
    :goto_0
    return-object v4

    .line 1066
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1067
    .local v2, "length":I
    const-string v1, "Adk3ldnLDq24HFldfo018fghclJy7DLjd9h56LXR"

    .line 1068
    .local v1, "key":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1069
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_2

    .line 1070
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    xor-int/2addr v4, v5

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1069
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1072
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private findOffHostService(Lcom/gsma/services/nfc/OffHostService;)I
    .locals 6
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 461
    if-nez p1, :cond_1

    .line 462
    const-string v4, "DynamicDBManager"

    const-string v5, "service is null"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    const/4 v1, -0x1

    .line 483
    :cond_0
    :goto_0
    return v1

    .line 465
    :cond_1
    const/4 v1, -0x1

    .line 466
    .local v1, "id":I
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 467
    .local v2, "pkg":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostServices(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 468
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 469
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    .line 470
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 474
    :cond_2
    invoke-direct {p0, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->readOffHostService(Landroid/database/Cursor;)Lcom/gsma/services/nfc/OffHostService;

    move-result-object v3

    .line 475
    .local v3, "read":Lcom/gsma/services/nfc/OffHostService;
    if-eqz v3, :cond_3

    invoke-virtual {v3, p1}, Lcom/gsma/services/nfc/OffHostService;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 476
    const-string v4, "id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 477
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 480
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 481
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private getAllOffHostServices()[Lcom/gsma/services/nfc/OffHostService;
    .locals 2

    .prologue
    .line 787
    invoke-direct {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAllOffHostServices()Landroid/database/Cursor;

    move-result-object v0

    .line 788
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->readOffHostServices(Landroid/database/Cursor;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v1

    return-object v1
.end method

.method private getDisplaySize()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 148
    iget-object v2, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 149
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 150
    .local v1, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 151
    return-object v1
.end method

.method private getScale(Ljava/lang/String;)I
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 203
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 207
    :goto_0
    return v0

    .line 204
    :cond_1
    const/4 v0, 0x1

    .line 205
    .local v0, "scale":I
    const-string v2, "-"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 206
    .local v1, "start":I
    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 207
    goto :goto_0
.end method

.method private loadBanner(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 18
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 250
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_2

    .line 251
    :cond_0
    const-string v13, "DynamicDBManager"

    const-string v14, "name is null"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    const/4 v2, 0x0

    .line 352
    :cond_1
    :goto_0
    return-object v2

    .line 254
    :cond_2
    const/4 v2, 0x0

    .line 255
    .local v2, "banner":Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    .line 256
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v11}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 257
    .local v11, "ops":Landroid/graphics/BitmapFactory$Options;
    const/4 v13, 0x0

    iput-boolean v13, v11, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 258
    sget-object v13, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v13, v11, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 259
    const/4 v13, 0x1

    iput-boolean v13, v11, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 262
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getDisplaySize()Landroid/graphics/Point;

    move-result-object v13

    iget v13, v13, Landroid/graphics/Point;->y:I

    const/16 v14, 0xa00

    if-lt v13, v14, :cond_4

    .line 264
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 265
    if-eqz v4, :cond_3

    .line 266
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v3, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .local v3, "banner":Landroid/graphics/drawable/Drawable;
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getScale(Ljava/lang/String;)I

    move-result v12

    .line 268
    .local v12, "scale":I
    const/4 v13, 0x1

    if-le v12, v13, :cond_f

    .line 269
    mul-int/lit8 v13, v12, 0x3

    int-to-float v13, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_b

    move-result-object v2

    .line 271
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :goto_1
    const/4 v4, 0x0

    .line 349
    .end local v12    # "scale":I
    :cond_3
    :goto_2
    if-eqz v2, :cond_1

    .line 350
    const-string v13, "DynamicDBManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "load W="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", H="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 275
    :cond_4
    :try_start_2
    new-instance v13, Ljava/lang/OutOfMemoryError;

    const-string v14, "next"

    invoke-direct {v13, v14}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v13
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 277
    :catch_0
    move-exception v5

    move-object v3, v2

    .line 279
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .local v5, "e1":Ljava/lang/OutOfMemoryError;
    :goto_3
    :try_start_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 280
    if-eqz v4, :cond_e

    .line 281
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v2, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1

    .line 282
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :try_start_4
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getScale(Ljava/lang/String;)I

    move-result v12

    .line 283
    .restart local v12    # "scale":I
    const/4 v13, 0x1

    if-le v12, v13, :cond_5

    .line 284
    mul-int/lit8 v13, v12, 0x3

    int-to-float v13, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_a

    move-result-object v2

    .line 286
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 288
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v12    # "scale":I
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_1
    move-exception v6

    .line 290
    .local v6, "e2":Ljava/lang/OutOfMemoryError;
    :goto_4
    if-nez v4, :cond_6

    .line 291
    :try_start_5
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 293
    :cond_6
    if-eqz v4, :cond_e

    .line 294
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v2, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2

    .line 295
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :try_start_6
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getScale(Ljava/lang/String;)I

    move-result v12

    .line 296
    .restart local v12    # "scale":I
    const/4 v13, 0x1

    if-le v12, v13, :cond_7

    .line 297
    mul-int/lit8 v13, v12, 0x2

    int-to-float v13, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_9

    move-result-object v2

    .line 299
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 301
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v12    # "scale":I
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_2
    move-exception v7

    .line 303
    .local v7, "e3":Ljava/lang/OutOfMemoryError;
    :goto_5
    if-nez v4, :cond_8

    .line 304
    :try_start_7
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 306
    :cond_8
    if-eqz v4, :cond_e

    .line 307
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v2, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_3

    .line 308
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :try_start_8
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getScale(Ljava/lang/String;)I

    move-result v12

    .line 309
    .restart local v12    # "scale":I
    const/4 v13, 0x1

    if-le v12, v13, :cond_9

    .line 310
    mul-int/lit8 v13, v12, 0x1

    int-to-float v13, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_8

    move-result-object v2

    .line 312
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 314
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v12    # "scale":I
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_3
    move-exception v8

    .line 316
    .local v8, "e4":Ljava/lang/OutOfMemoryError;
    :goto_6
    if-nez v4, :cond_a

    .line 317
    :try_start_9
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 319
    :cond_a
    if-eqz v4, :cond_e

    .line 320
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v2, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_4

    .line 321
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :try_start_a
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getScale(Ljava/lang/String;)I

    move-result v12

    .line 322
    .restart local v12    # "scale":I
    const/4 v13, 0x1

    if-le v12, v13, :cond_b

    .line 323
    int-to-double v14, v12

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    mul-double v14, v14, v16

    double-to-float v13, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_7

    move-result-object v2

    .line 325
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 327
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v12    # "scale":I
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_4
    move-exception v9

    .line 329
    .local v9, "e5":Ljava/lang/OutOfMemoryError;
    :goto_7
    if-nez v4, :cond_c

    .line 330
    :try_start_b
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 332
    :cond_c
    if-eqz v4, :cond_e

    .line 333
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v2, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_5

    .line 334
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :try_start_c
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getScale(Ljava/lang/String;)I

    move-result v12

    .line 335
    .restart local v12    # "scale":I
    const/4 v13, 0x1

    if-le v12, v13, :cond_d

    .line 336
    int-to-double v14, v12

    const-wide v16, 0x3fb999999999999aL    # 0.1

    mul-double v14, v14, v16

    double-to-float v13, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_6

    move-result-object v2

    .line 338
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 340
    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v12    # "scale":I
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_5
    move-exception v10

    move-object v2, v3

    .line 341
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .local v10, "e6":Ljava/lang/OutOfMemoryError;
    :goto_8
    const-string v13, "DynamicDBManager"

    const-string v14, "Can\'t load the banner because of out of memory!"

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 340
    .end local v10    # "e6":Ljava/lang/OutOfMemoryError;
    :catch_6
    move-exception v10

    goto :goto_8

    .line 327
    .end local v9    # "e5":Ljava/lang/OutOfMemoryError;
    :catch_7
    move-exception v9

    move-object v3, v2

    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    goto :goto_7

    .line 314
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v8    # "e4":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_8
    move-exception v8

    move-object v3, v2

    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_6

    .line 301
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v7    # "e3":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_9
    move-exception v7

    move-object v3, v2

    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_5

    .line 288
    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v6    # "e2":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    :catch_a
    move-exception v6

    move-object v3, v2

    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_4

    .line 277
    .end local v5    # "e1":Ljava/lang/OutOfMemoryError;
    :catch_b
    move-exception v5

    goto/16 :goto_3

    .restart local v5    # "e1":Ljava/lang/OutOfMemoryError;
    :cond_e
    move-object v2, v3

    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_2

    .end local v2    # "banner":Landroid/graphics/drawable/Drawable;
    .end local v5    # "e1":Ljava/lang/OutOfMemoryError;
    .restart local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v12    # "scale":I
    :cond_f
    move-object v2, v3

    .end local v3    # "banner":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "banner":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_1
.end method

.method private loadDrawableBanner(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 236
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 237
    :cond_0
    const-string v2, "DynamicDBManager"

    const-string v3, "name is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    const/4 v0, 0x0

    .line 246
    :goto_0
    return-object v0

    .line 240
    :cond_1
    const/4 v0, 0x0

    .line 242
    .local v0, "banner":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 243
    :catch_0
    move-exception v1

    .line 244
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method static open(Landroid/content/Context;)Lcom/android/nfc/cardemulation/DynamicDBManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcom/android/nfc/cardemulation/DynamicDBManager;

    invoke-direct {v0, p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;-><init>(Landroid/content/Context;)V

    .line 72
    .local v0, "handler":Lcom/android/nfc/cardemulation/DynamicDBManager;
    return-object v0
.end method

.method private readOffHostService(Landroid/database/Cursor;)Lcom/gsma/services/nfc/OffHostService;
    .locals 22
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 812
    if-nez p1, :cond_1

    .line 813
    const-string v20, "DynamicDBManager"

    const-string v21, "cursor is null"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    const/16 v17, 0x0

    .line 869
    :cond_0
    :goto_0
    return-object v17

    .line 816
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v18

    .line 817
    .local v18, "size":I
    if-nez v18, :cond_2

    .line 818
    const/16 v17, 0x0

    goto :goto_0

    .line 820
    :cond_2
    const/16 v17, 0x0

    .line 821
    .local v17, "service":Lcom/gsma/services/nfc/OffHostService;
    const-string v20, "virtualclass"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 822
    .local v19, "virtualClassName":Ljava/lang/String;
    const-string v20, "package"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 823
    .local v16, "packageName":Ljava/lang/String;
    const-string v20, "class"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 824
    .local v4, "className":Ljava/lang/String;
    const-string v20, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 825
    .local v12, "id":I
    const-string v20, "description"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 826
    .local v7, "description":Ljava/lang/String;
    const-string v20, "location"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 827
    .local v15, "location":Ljava/lang/String;
    const/4 v9, 0x0

    .line 829
    .local v9, "drawable":Landroid/graphics/drawable/Drawable;
    const-string v20, "banner"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 830
    .local v10, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/nfc/cardemulation/DynamicDBManager;->loadDrawableBanner(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 841
    new-instance v17, Lcom/gsma/services/nfc/OffHostService;

    .end local v17    # "service":Lcom/gsma/services/nfc/OffHostService;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v7, v15, v1, v4}, Lcom/gsma/services/nfc/OffHostService;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    .restart local v17    # "service":Lcom/gsma/services/nfc/OffHostService;
    invoke-virtual/range {v17 .. v17}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setId(I)V

    .line 843
    invoke-virtual/range {v17 .. v17}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setVirtualClassName(Ljava/lang/String;)V

    .line 844
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/gsma/services/nfc/OffHostService;->setBanner(Landroid/graphics/drawable/Drawable;)V

    .line 846
    const-string v20, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 847
    .local v13, "id2":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidGroups(I)Landroid/database/Cursor;

    move-result-object v5

    .line 848
    .local v5, "cursor2":Landroid/database/Cursor;
    if-eqz v5, :cond_0

    .line 849
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v20

    if-lez v20, :cond_7

    .line 851
    :cond_3
    const-string v20, "description"

    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 852
    .local v8, "description2":Ljava/lang/String;
    const-string v20, "category"

    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 853
    .local v3, "category":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v3}, Lcom/gsma/services/nfc/OffHostService;->defineAidGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/gsma/services/nfc/AidGroup;

    move-result-object v11

    .line 854
    .local v11, "group":Lcom/gsma/services/nfc/AidGroup;
    const-string v20, "_id"

    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 855
    .local v14, "id3":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidList(I)Landroid/database/Cursor;

    move-result-object v6

    .line 856
    .local v6, "cursor3":Landroid/database/Cursor;
    if-eqz v6, :cond_6

    .line 857
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v20

    if-lez v20, :cond_5

    .line 859
    :cond_4
    const-string v20, "aid"

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 860
    .local v2, "aid":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/gsma/services/nfc/AidGroup;->addNewAid(Ljava/lang/String;)V

    .line 861
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v20

    if-nez v20, :cond_4

    .line 863
    .end local v2    # "aid":Ljava/lang/String;
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 865
    :cond_6
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v20

    if-nez v20, :cond_3

    .line 867
    .end local v3    # "category":Ljava/lang/String;
    .end local v6    # "cursor3":Landroid/database/Cursor;
    .end local v8    # "description2":Ljava/lang/String;
    .end local v11    # "group":Lcom/gsma/services/nfc/AidGroup;
    .end local v14    # "id3":I
    :cond_7
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private readOffHostServices(Landroid/database/Cursor;)[Lcom/gsma/services/nfc/OffHostService;
    .locals 24
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 873
    if-nez p1, :cond_0

    .line 874
    const-string v22, "DynamicDBManager"

    const-string v23, "cursor is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    const/16 v19, 0x0

    .line 936
    :goto_0
    return-object v19

    .line 877
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 878
    .local v20, "size":I
    if-nez v20, :cond_1

    .line 879
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 880
    const/16 v19, 0x0

    goto :goto_0

    .line 882
    :cond_1
    move/from16 v0, v20

    new-array v0, v0, [Lcom/gsma/services/nfc/OffHostService;

    move-object/from16 v19, v0

    .line 883
    .local v19, "services":[Lcom/gsma/services/nfc/OffHostService;
    const/4 v13, 0x0

    .line 885
    .local v13, "i":I
    :cond_2
    const-string v22, "virtualclass"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 886
    .local v21, "virtualClassName":Ljava/lang/String;
    const-string v22, "package"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 887
    .local v18, "packageName":Ljava/lang/String;
    const-string v22, "class"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 888
    .local v5, "className":Ljava/lang/String;
    const-string v22, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 889
    .local v14, "id":I
    const-string v22, "description"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 890
    .local v8, "description":Ljava/lang/String;
    const-string v22, "location"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 891
    .local v17, "location":Ljava/lang/String;
    const/4 v10, 0x0

    .line 893
    .local v10, "drawable":Landroid/graphics/drawable/Drawable;
    const-string v22, "banner"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 894
    .local v11, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/nfc/cardemulation/DynamicDBManager;->loadDrawableBanner(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 905
    new-instance v22, Lcom/gsma/services/nfc/OffHostService;

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v8, v1, v2, v5}, Lcom/gsma/services/nfc/OffHostService;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v22, v19, v13

    .line 906
    aget-object v22, v19, v13

    invoke-virtual/range {v22 .. v22}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setId(I)V

    .line 907
    aget-object v22, v19, v13

    invoke-virtual/range {v22 .. v22}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->setVirtualClassName(Ljava/lang/String;)V

    .line 908
    aget-object v22, v19, v13

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lcom/gsma/services/nfc/OffHostService;->setBanner(Landroid/graphics/drawable/Drawable;)V

    .line 910
    const-string v22, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 911
    .local v15, "id2":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidGroups(I)Landroid/database/Cursor;

    move-result-object v6

    .line 912
    .local v6, "cursor2":Landroid/database/Cursor;
    if-eqz v6, :cond_8

    .line 913
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-lez v22, :cond_7

    .line 915
    :cond_3
    const-string v22, "description"

    move-object/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 916
    .local v9, "description2":Ljava/lang/String;
    const-string v22, "category"

    move-object/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 917
    .local v4, "category":Ljava/lang/String;
    aget-object v22, v19, v13

    move-object/from16 v0, v22

    invoke-virtual {v0, v9, v4}, Lcom/gsma/services/nfc/OffHostService;->defineAidGroup(Ljava/lang/String;Ljava/lang/String;)Lcom/gsma/services/nfc/AidGroup;

    move-result-object v12

    .line 918
    .local v12, "group":Lcom/gsma/services/nfc/AidGroup;
    const-string v22, "_id"

    move-object/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 919
    .local v16, "id3":I
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidList(I)Landroid/database/Cursor;

    move-result-object v7

    .line 920
    .local v7, "cursor3":Landroid/database/Cursor;
    if-eqz v7, :cond_6

    .line 921
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-lez v22, :cond_5

    .line 923
    :cond_4
    const-string v22, "aid"

    move-object/from16 v0, v22

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 924
    .local v3, "aid":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/nfc/cardemulation/DynamicDBManager;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/gsma/services/nfc/AidGroup;->addNewAid(Ljava/lang/String;)V

    .line 925
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v22

    if-nez v22, :cond_4

    .line 927
    .end local v3    # "aid":Ljava/lang/String;
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 929
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v22

    if-nez v22, :cond_3

    .line 931
    .end local v4    # "category":Ljava/lang/String;
    .end local v7    # "cursor3":Landroid/database/Cursor;
    .end local v9    # "description2":Ljava/lang/String;
    .end local v12    # "group":Lcom/gsma/services/nfc/AidGroup;
    .end local v16    # "id3":I
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 933
    :cond_8
    add-int/lit8 v13, v13, 0x1

    .line 934
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v22

    if-nez v22, :cond_2

    .line 935
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private readOffHostServicesId(Landroid/database/Cursor;)[I
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x0

    .line 792
    if-nez p1, :cond_0

    .line 793
    const-string v3, "DynamicDBManager"

    const-string v4, "cursor is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :goto_0
    return-object v1

    .line 796
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 797
    .local v2, "size":I
    if-nez v2, :cond_1

    .line 798
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 801
    :cond_1
    new-array v1, v2, [I

    .line 802
    .local v1, "list":[I
    const/4 v0, 0x0

    .line 804
    .local v0, "i":I
    :cond_2
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aput v3, v1, v0

    .line 805
    add-int/lit8 v0, v0, 0x1

    .line 806
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 807
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private resize(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 11
    .param p1, "image"    # Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 155
    if-eqz p1, :cond_0

    instance-of v9, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v9, :cond_1

    :cond_0
    move-object v4, p1

    .line 179
    .end local p1    # "image":Landroid/graphics/drawable/Drawable;
    .local v4, "image":Ljava/lang/Object;
    :goto_0
    return-object v4

    .end local v4    # "image":Ljava/lang/Object;
    .restart local p1    # "image":Landroid/graphics/drawable/Drawable;
    :cond_1
    move-object v9, p1

    .line 158
    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 159
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    .line 160
    .local v8, "width":I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 161
    .local v3, "height":I
    const/16 v6, 0x410

    .line 162
    .local v6, "sizeX":I
    const/16 v7, 0x180

    .line 163
    .local v7, "sizeY":I
    if-eqz v8, :cond_2

    if-nez v3, :cond_4

    .line 169
    :cond_2
    :goto_1
    const/4 v1, 0x0

    .line 171
    .local v1, "bitmapResized":Landroid/graphics/Bitmap;
    const/4 v9, 0x0

    :try_start_0
    invoke-static {v0, v6, v7, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 175
    if-eq v1, v0, :cond_3

    .line 176
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 178
    :cond_3
    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "image":Landroid/graphics/drawable/Drawable;
    iget-object v9, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-direct {p1, v9, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .restart local p1    # "image":Landroid/graphics/drawable/Drawable;
    move-object v4, p1

    .line 179
    .restart local v4    # "image":Ljava/lang/Object;
    goto :goto_0

    .line 166
    .end local v1    # "bitmapResized":Landroid/graphics/Bitmap;
    .end local v4    # "image":Ljava/lang/Object;
    :cond_4
    int-to-float v9, v8

    int-to-float v10, v3

    div-float v5, v9, v10

    .line 167
    .local v5, "ratio":F
    int-to-float v9, v6

    div-float/2addr v9, v5

    float-to-int v7, v9

    goto :goto_1

    .line 172
    .end local v5    # "ratio":F
    .restart local v1    # "bitmapResized":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    .line 173
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    throw v2
.end method

.method private resize(Landroid/graphics/drawable/Drawable;F)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "image"    # Landroid/graphics/drawable/Drawable;
    .param p2, "scaleFactor"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 183
    if-eqz p1, :cond_0

    instance-of v6, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v6, :cond_1

    :cond_0
    move-object v3, p1

    .line 199
    .end local p1    # "image":Landroid/graphics/drawable/Drawable;
    .local v3, "image":Ljava/lang/Object;
    :goto_0
    return-object v3

    .end local v3    # "image":Ljava/lang/Object;
    .restart local p1    # "image":Landroid/graphics/drawable/Drawable;
    :cond_1
    move-object v6, p1

    .line 186
    check-cast v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 187
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, p2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 188
    .local v4, "sizeX":I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, p2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 189
    .local v5, "sizeY":I
    const/4 v1, 0x0

    .line 191
    .local v1, "bitmapResized":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    :try_start_0
    invoke-static {v0, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 195
    if-eq v1, v0, :cond_2

    .line 196
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 198
    :cond_2
    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "image":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {p1, v6, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .restart local p1    # "image":Landroid/graphics/drawable/Drawable;
    move-object v3, p1

    .line 199
    .restart local v3    # "image":Ljava/lang/Object;
    goto :goto_0

    .line 192
    .end local v3    # "image":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 193
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    throw v2
.end method

.method private saveBanner(ILandroid/graphics/drawable/Drawable;)Ljava/lang/String;
    .locals 9
    .param p1, "id"    # I
    .param p2, "banner"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 211
    if-nez p2, :cond_0

    .line 212
    const-string v6, "DynamicDBManager"

    const-string v7, "image is null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v3, 0x0

    .line 232
    :goto_0
    return-object v3

    .line 215
    :cond_0
    const/4 v5, 0x1

    .line 218
    .local v5, "scale":I
    invoke-direct {p0, p2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->resize(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 219
    const-string v6, "DynamicDBManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "save W="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", H="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "banner"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".png"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .local v3, "fileName":Ljava/lang/String;
    move-object v6, p2

    .line 221
    check-cast v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-direct {v2, v6, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 224
    .local v2, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 225
    .local v4, "out":Ljava/io/FileOutputStream;
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x64

    invoke-virtual {v0, v6, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 226
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 227
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 229
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 230
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private selectAllOffHostServices()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 987
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "offhostservice"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 988
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 989
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 991
    :cond_0
    return-object v10
.end method

.method private selectAllOffHostServicesId()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1019
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "offhostservice"

    new-array v3, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "id"

    aput-object v6, v3, v5

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1020
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1023
    :cond_0
    return-object v10
.end method

.method private selectDefaultPackage()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 959
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "defaultservice"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 960
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 961
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 963
    :cond_0
    return-object v10
.end method

.method private selectOffHostServicesId(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1027
    if-nez p1, :cond_0

    .line 1028
    const-string v0, "DynamicDBManager"

    const-string v1, "package is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    :goto_0
    return-object v5

    .line 1031
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "offhostservice"

    new-array v3, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "id"

    aput-object v6, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "package=\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1032
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    .line 1033
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    move-object v5, v10

    .line 1035
    goto :goto_0
.end method

.method private selectPackage(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 975
    if-nez p1, :cond_0

    .line 976
    const-string v0, "DynamicDBManager"

    const-string v1, "package is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    :goto_0
    return-object v3

    .line 979
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "package"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "name=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 980
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    .line 981
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    move-object v3, v10

    .line 983
    goto :goto_0
.end method

.method private selectPackageList()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 967
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "package"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 968
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 969
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 971
    :cond_0
    return-object v10
.end method

.method private updateDefaultService(Ljava/lang/String;)J
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 707
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 708
    .local v1, "v":Landroid/content/ContentValues;
    const-string v4, "defaultservice"

    const-string v5, "default"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    const-string v4, "package"

    invoke-virtual {v1, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iget-object v4, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "defaultservice"

    const-string v6, "defaultservice=\'default\'"

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    int-to-long v2, v4

    .line 712
    .local v2, "ret":J
    :try_start_0
    iget-object v4, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 713
    iget-object v4, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "defaultservice"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 714
    iget-object v4, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 718
    iget-object v4, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 720
    :goto_0
    return-wide v2

    .line 715
    :catch_0
    move-exception v0

    .line 716
    .local v0, "e":Landroid/database/SQLException;
    :try_start_1
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 718
    iget-object v4, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->helper:Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;

    invoke-virtual {v0}, Lcom/android/nfc/cardemulation/DynamicDBManager$DBHelper;->close()V

    .line 77
    return-void
.end method

.method getAllOffHostServicesId()[I
    .locals 2

    .prologue
    .line 764
    invoke-direct {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAllOffHostServicesId()Landroid/database/Cursor;

    move-result-object v0

    .line 765
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->readOffHostServicesId(Landroid/database/Cursor;)[I

    move-result-object v1

    return-object v1
.end method

.method getDefaultOffHostService()Lcom/gsma/services/nfc/OffHostService;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 661
    const/4 v2, 0x0

    .line 662
    .local v2, "service":Lcom/gsma/services/nfc/OffHostService;
    const/4 v1, 0x0

    .line 664
    .local v1, "pkg":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectDefaultPackage()Landroid/database/Cursor;

    move-result-object v0

    .line 665
    .local v0, "c":Landroid/database/Cursor;
    if-nez v0, :cond_1

    .line 675
    :cond_0
    :goto_0
    return-object v3

    .line 666
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    .line 667
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 670
    :cond_2
    const-string v4, "package"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 671
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 672
    if-eqz v1, :cond_0

    .line 674
    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getDefaultOffHostService(Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;

    move-result-object v2

    move-object v3, v2

    .line 675
    goto :goto_0
.end method

.method getDefaultOffHostService(Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;
    .locals 7
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 679
    if-nez p1, :cond_1

    .line 680
    const-string v5, "DynamicDBManager"

    const-string v6, "package is null"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 703
    :cond_0
    :goto_0
    return-object v3

    .line 683
    :cond_1
    const/4 v3, 0x0

    .line 684
    .local v3, "service":Lcom/gsma/services/nfc/OffHostService;
    invoke-direct {p0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectPackage(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 685
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 686
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_2

    .line 687
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v3, v4

    .line 688
    goto :goto_0

    .line 690
    :cond_2
    const-string v5, "defaultid"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 691
    .local v2, "defaultId":I
    invoke-virtual {p0, v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostService(I)Landroid/database/Cursor;

    move-result-object v1

    .line 692
    .local v1, "cursor1":Landroid/database/Cursor;
    if-eqz v1, :cond_4

    .line 693
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_3

    .line 694
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 695
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v3, v4

    .line 696
    goto :goto_0

    .line 698
    :cond_3
    invoke-direct {p0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->readOffHostService(Landroid/database/Cursor;)Lcom/gsma/services/nfc/OffHostService;

    move-result-object v3

    .line 699
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 701
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 778
    if-nez p1, :cond_0

    .line 779
    const-string v1, "DynamicDBManager"

    const-string v2, "package is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    const/4 v1, 0x0

    .line 783
    :goto_0
    return-object v1

    .line 782
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostServices(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 783
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->readOffHostServices(Landroid/database/Cursor;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v1

    goto :goto_0
.end method

.method getOffHostServicesId(Ljava/lang/String;)[I
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 769
    if-nez p1, :cond_0

    .line 770
    const-string v1, "DynamicDBManager"

    const-string v2, "package is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    const/4 v1, 0x0

    .line 774
    :goto_0
    return-object v1

    .line 773
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostServicesId(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 774
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->readOffHostServicesId(Landroid/database/Cursor;)[I

    move-result-object v1

    goto :goto_0
.end method

.method getPackageList()[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 940
    invoke-direct {p0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectPackageList()Landroid/database/Cursor;

    move-result-object v0

    .line 941
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 942
    .local v1, "i":I
    if-eqz v0, :cond_0

    .line 943
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 944
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 955
    :cond_0
    :goto_0
    return-object v2

    .line 947
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    new-array v2, v3, [Ljava/lang/String;

    .line 949
    .local v2, "pkgList":[Ljava/lang/String;
    :cond_2
    const-string v3, "name"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 950
    add-int/lit8 v1, v1, 0x1

    .line 951
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 952
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method insertOffHostService(Lcom/gsma/services/nfc/OffHostService;)I
    .locals 25
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 371
    if-nez p1, :cond_0

    .line 372
    const-string v22, "DynamicDBManager"

    const-string v23, "service is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const/16 v22, -0x1

    .line 457
    :goto_0
    return v22

    .line 375
    :cond_0
    const-wide/16 v20, 0x0

    .line 378
    .local v20, "ret":J
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v18

    .line 379
    .local v18, "pkg":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectPackage(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 380
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 381
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-nez v22, :cond_1

    .line 382
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 383
    .local v19, "v":Landroid/content/ContentValues;
    const-string v22, "name"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const-string v22, "defaultid"

    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 386
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    const-string v23, "package"

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 395
    .end local v19    # "v":Landroid/content/ContentValues;
    :cond_1
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 397
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->createOffHostServiceContentValues(Lcom/gsma/services/nfc/OffHostService;)Landroid/content/ContentValues;

    move-result-object v19

    .line 399
    .restart local v19    # "v":Landroid/content/ContentValues;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    const-string v23, "offhostservice"

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 407
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getAidGroups()[Lcom/gsma/services/nfc/AidGroup;

    move-result-object v13

    .line 408
    .local v13, "groups":[Lcom/gsma/services/nfc/AidGroup;
    if-eqz v13, :cond_3

    array-length v0, v13

    move/from16 v22, v0

    if-nez v22, :cond_4

    .line 409
    :cond_3
    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v22, v0

    goto/16 :goto_0

    .line 389
    .end local v13    # "groups":[Lcom/gsma/services/nfc/AidGroup;
    :catch_0
    move-exception v11

    .line 390
    .local v11, "e":Landroid/database/SQLException;
    :try_start_2
    invoke-virtual {v11}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .end local v11    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v22

    .line 402
    :catch_1
    move-exception v11

    .line 403
    .restart local v11    # "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v11}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    .end local v11    # "e":Landroid/database/SQLException;
    :catchall_1
    move-exception v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v22

    .line 411
    .restart local v13    # "groups":[Lcom/gsma/services/nfc/AidGroup;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostService(I)Landroid/database/Cursor;

    move-result-object v10

    .line 412
    const/4 v4, 0x0

    .line 413
    .local v4, "_id":I
    if-eqz v10, :cond_5

    .line 414
    const-string v22, "_id"

    move-object/from16 v0, v22

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 415
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 417
    :cond_5
    move-object v8, v13

    .local v8, "arr$":[Lcom/gsma/services/nfc/AidGroup;
    array-length v0, v8

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    move v15, v14

    .end local v8    # "arr$":[Lcom/gsma/services/nfc/AidGroup;
    .end local v14    # "i$":I
    .end local v16    # "len$":I
    .local v15, "i$":I
    :goto_3
    move/from16 v0, v16

    if-ge v15, v0, :cond_9

    aget-object v12, v8, v15

    .line 418
    .local v12, "g":Lcom/gsma/services/nfc/AidGroup;
    new-instance v19, Landroid/content/ContentValues;

    .end local v19    # "v":Landroid/content/ContentValues;
    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 419
    .restart local v19    # "v":Landroid/content/ContentValues;
    const-string v22, "description"

    invoke-virtual {v12}, Lcom/gsma/services/nfc/AidGroup;->getDescription()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v22, "category"

    invoke-virtual {v12}, Lcom/gsma/services/nfc/AidGroup;->getCategory()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v22, "offhostserviceid"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 423
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    const-string v23, "aidgroup"

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 431
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidGroups(I)Landroid/database/Cursor;

    move-result-object v10

    .line 432
    const/4 v5, 0x0

    .line 433
    .local v5, "_id2":I
    if-eqz v10, :cond_6

    .line 434
    invoke-interface {v10}, Landroid/database/Cursor;->moveToLast()Z

    .line 435
    const-string v22, "_id"

    move-object/from16 v0, v22

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 436
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 438
    :cond_6
    invoke-virtual {v12}, Lcom/gsma/services/nfc/AidGroup;->getAids()[Ljava/lang/String;

    move-result-object v7

    .line 439
    .local v7, "aids":[Ljava/lang/String;
    if-eqz v7, :cond_7

    array-length v0, v7

    move/from16 v22, v0

    if-nez v22, :cond_8

    .line 417
    .end local v15    # "i$":I
    :cond_7
    add-int/lit8 v14, v15, 0x1

    .restart local v14    # "i$":I
    move v15, v14

    .end local v14    # "i$":I
    .restart local v15    # "i$":I
    goto/16 :goto_3

    .line 426
    .end local v5    # "_id2":I
    .end local v7    # "aids":[Ljava/lang/String;
    :catch_2
    move-exception v11

    .line 427
    .restart local v11    # "e":Landroid/database/SQLException;
    :try_start_5
    invoke-virtual {v11}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_4

    .end local v11    # "e":Landroid/database/SQLException;
    :catchall_2
    move-exception v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v22

    .line 442
    .restart local v5    # "_id2":I
    .restart local v7    # "aids":[Ljava/lang/String;
    :cond_8
    move-object v9, v7

    .local v9, "arr$":[Ljava/lang/String;
    array-length v0, v9

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v14, 0x0

    .end local v15    # "i$":I
    .restart local v14    # "i$":I
    :goto_5
    move/from16 v0, v17

    if-ge v14, v0, :cond_7

    aget-object v6, v9, v14

    .line 443
    .local v6, "aid":Ljava/lang/String;
    new-instance v19, Landroid/content/ContentValues;

    .end local v19    # "v":Landroid/content/ContentValues;
    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 444
    .restart local v19    # "v":Landroid/content/ContentValues;
    const-string v22, "aid"

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/nfc/cardemulation/DynamicDBManager;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v22, "groupid"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 447
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    const-string v23, "aid"

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 442
    :goto_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 450
    :catch_3
    move-exception v11

    .line 451
    .restart local v11    # "e":Landroid/database/SQLException;
    :try_start_7
    invoke-virtual {v11}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_6

    .end local v11    # "e":Landroid/database/SQLException;
    :catchall_3
    move-exception v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v22

    .line 457
    .end local v5    # "_id2":I
    .end local v6    # "aid":Ljava/lang/String;
    .end local v7    # "aids":[Ljava/lang/String;
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v12    # "g":Lcom/gsma/services/nfc/AidGroup;
    .end local v14    # "i$":I
    .end local v17    # "len$":I
    .restart local v15    # "i$":I
    :cond_9
    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v22, v0

    goto/16 :goto_0
.end method

.method removeOffHostService(Lcom/gsma/services/nfc/OffHostService;)I
    .locals 14
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    const/4 v7, -0x1

    const/4 v13, 0x0

    .line 487
    if-nez p1, :cond_0

    .line 488
    const-string v10, "DynamicDBManager"

    const-string v11, "service is null"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :goto_0
    return v7

    .line 491
    :cond_0
    const-wide/16 v8, 0x0

    .line 492
    .local v8, "ret":J
    invoke-virtual {p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 493
    .local v6, "pkg":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->findOffHostService(Lcom/gsma/services/nfc/OffHostService;)I

    move-result v5

    .line 494
    .local v5, "id":I
    invoke-virtual {p0, v5}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostService(I)Landroid/database/Cursor;

    move-result-object v2

    .line 495
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_5

    .line 496
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-nez v10, :cond_1

    .line 497
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 500
    :cond_1
    const-string v7, "_id"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 501
    .local v0, "_id":I
    invoke-virtual {p0, v0}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidGroups(I)Landroid/database/Cursor;

    move-result-object v3

    .line 502
    .local v3, "cursor2":Landroid/database/Cursor;
    if-eqz v3, :cond_4

    .line 503
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_3

    .line 505
    :cond_2
    const-string v7, "_id"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 507
    .local v1, "_id2":I
    iget-object v7, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "aid"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "groupid="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    int-to-long v8, v7

    .line 509
    iget-object v7, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "aidgroup"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "offhostserviceid="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    int-to-long v8, v7

    .line 510
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 512
    .end local v1    # "_id2":I
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 516
    :cond_4
    const-string v7, "banner"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 517
    .local v4, "fileName":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/android/nfc/cardemulation/DynamicDBManager;->deleteBanner(Ljava/lang/String;)Z

    .line 519
    iget-object v7, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "offhostservice"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "id="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    int-to-long v8, v7

    .line 520
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 523
    .end local v0    # "_id":I
    .end local v3    # "cursor2":Landroid/database/Cursor;
    .end local v4    # "fileName":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0, v6}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostServices(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 524
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-nez v7, :cond_6

    .line 526
    iget-object v7, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v10, "package"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "name=\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v10, v11, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    int-to-long v8, v7

    .line 528
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 529
    long-to-int v7, v8

    goto/16 :goto_0
.end method

.method removeOffHostServices(Ljava/lang/String;)I
    .locals 10
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    .line 533
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 534
    :cond_0
    const-string v8, "DynamicDBManager"

    const-string v9, "package is null"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_1
    :goto_0
    return v7

    .line 537
    :cond_2
    const-wide/16 v4, 0x0

    .line 538
    .local v4, "ret":J
    invoke-virtual {p0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;

    move-result-object v6

    .line 539
    .local v6, "services":[Lcom/gsma/services/nfc/OffHostService;
    if-eqz v6, :cond_1

    array-length v8, v6

    if-eqz v8, :cond_1

    .line 542
    move-object v0, v6

    .local v0, "arr$":[Lcom/gsma/services/nfc/OffHostService;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 543
    .local v3, "s":Lcom/gsma/services/nfc/OffHostService;
    invoke-virtual {p0, v3}, Lcom/android/nfc/cardemulation/DynamicDBManager;->removeOffHostService(Lcom/gsma/services/nfc/OffHostService;)I

    move-result v7

    int-to-long v4, v7

    .line 542
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 545
    .end local v3    # "s":Lcom/gsma/services/nfc/OffHostService;
    :cond_3
    long-to-int v7, v4

    goto :goto_0
.end method

.method selectAidGroups(I)Landroid/database/Cursor;
    .locals 11
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x0

    .line 1047
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "aidgroup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "offhostserviceid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1048
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 1049
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1051
    :cond_0
    return-object v10
.end method

.method selectAidList(I)Landroid/database/Cursor;
    .locals 11
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x0

    .line 1055
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "aid"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "groupid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1056
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 1057
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1059
    :cond_0
    return-object v10
.end method

.method selectOffHostService(I)Landroid/database/Cursor;
    .locals 11
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x0

    .line 1039
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "offhostservice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1040
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 1041
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1043
    :cond_0
    return-object v10
.end method

.method selectOffHostServices(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1007
    if-nez p1, :cond_0

    .line 1008
    const-string v0, "DynamicDBManager"

    const-string v1, "package is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    :goto_0
    return-object v3

    .line 1011
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "offhostservice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1012
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    .line 1013
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    move-object v3, v10

    .line 1015
    goto :goto_0
.end method

.method selectOffHostServicesWithVirtual(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 995
    if-nez p1, :cond_0

    .line 996
    const-string v0, "DynamicDBManager"

    const-string v1, "package is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    :goto_0
    return-object v3

    .line 999
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "offhostservice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "virtualclass=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1000
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    .line 1001
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    move-object v3, v10

    .line 1003
    goto :goto_0
.end method

.method setDefaultOffHostService(Ljava/lang/String;)Z
    .locals 11
    .param p1, "virtualClassName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 724
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 725
    :cond_0
    const-string v7, "DynamicDBManager"

    const-string v8, "clear the default"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    invoke-direct {p0, v10}, Lcom/android/nfc/cardemulation/DynamicDBManager;->updateDefaultService(Ljava/lang/String;)J

    .line 760
    :goto_0
    return v6

    .line 729
    :cond_1
    const/4 v2, 0x0

    .line 730
    .local v2, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 731
    .local v1, "id":I
    invoke-virtual {p0, p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostServicesWithVirtual(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 732
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    .line 733
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-nez v7, :cond_2

    .line 734
    const-string v7, "DynamicDBManager"

    const-string v8, "Can\'t set the default"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 736
    invoke-direct {p0, v10}, Lcom/android/nfc/cardemulation/DynamicDBManager;->updateDefaultService(Ljava/lang/String;)J

    goto :goto_0

    .line 739
    :cond_2
    const-string v7, "package"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 740
    const-string v7, "id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 741
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 742
    invoke-direct {p0, v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->updateDefaultService(Ljava/lang/String;)J

    .line 748
    invoke-direct {p0, v2}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectPackage(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 749
    if-eqz v0, :cond_5

    .line 750
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-nez v7, :cond_4

    .line 751
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 744
    :cond_3
    const-string v7, "DynamicDBManager"

    const-string v8, "Can\'t set the default"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    invoke-direct {p0, v10}, Lcom/android/nfc/cardemulation/DynamicDBManager;->updateDefaultService(Ljava/lang/String;)J

    goto :goto_0

    .line 754
    :cond_4
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 755
    .local v3, "v":Landroid/content/ContentValues;
    const-string v6, "name"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    const-string v6, "defaultid"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 757
    iget-object v6, p0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "package"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "name=\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v3, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    int-to-long v4, v6

    .line 758
    .local v4, "ret":J
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 760
    .end local v3    # "v":Landroid/content/ContentValues;
    .end local v4    # "ret":J
    :cond_5
    const/4 v6, 0x1

    goto/16 :goto_0
.end method

.method updateOffHostService(Lcom/gsma/services/nfc/OffHostService;)I
    .locals 34
    .param p1, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 549
    if-nez p1, :cond_0

    .line 550
    const-string v30, "DynamicDBManager"

    const-string v31, "service is null"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    const/16 v30, -0x1

    .line 628
    :goto_0
    return v30

    .line 553
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getPackageName()Ljava/lang/String;

    move-result-object v22

    .line 554
    .local v22, "pkg":Ljava/lang/String;
    const-wide/16 v24, 0x0

    .local v24, "ret":J
    const-wide/16 v26, 0x0

    .local v26, "ret2":J
    const-wide/16 v28, 0x0

    .line 556
    .local v28, "ret3":J
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getExtraInfo()Lcom/gsma/services/nfc/OffHostService$extraInfo;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/gsma/services/nfc/OffHostService$extraInfo;->getId()I

    move-result v19

    .line 557
    .local v19, "id":I
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectOffHostService(I)Landroid/database/Cursor;

    move-result-object v12

    .line 558
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 559
    .local v6, "_id":I
    if-eqz v12, :cond_5

    .line 560
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v30

    if-nez v30, :cond_1

    .line 561
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 562
    const/16 v30, -0x1

    goto :goto_0

    .line 564
    :cond_1
    const-string v30, "_id"

    move-object/from16 v0, v30

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 565
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidGroups(I)Landroid/database/Cursor;

    move-result-object v13

    .line 566
    .local v13, "cursor2":Landroid/database/Cursor;
    if-eqz v13, :cond_4

    .line 567
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v30

    if-lez v30, :cond_3

    .line 569
    :cond_2
    const-string v30, "_id"

    move-object/from16 v0, v30

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 570
    .local v7, "_id2":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    const-string v31, "aid"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "groupid="

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    invoke-virtual/range {v30 .. v33}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 571
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v30

    if-nez v30, :cond_2

    .line 573
    .end local v7    # "_id2":I
    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 575
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    const-string v31, "aidgroup"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "offhostserviceid="

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    invoke-virtual/range {v30 .. v33}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 576
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 580
    .end local v13    # "cursor2":Landroid/database/Cursor;
    :cond_5
    invoke-direct/range {p0 .. p1}, Lcom/android/nfc/cardemulation/DynamicDBManager;->createOffHostServiceContentValues(Lcom/gsma/services/nfc/OffHostService;)Landroid/content/ContentValues;

    move-result-object v23

    .line 581
    .local v23, "v":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    const-string v31, "offhostservice"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "id="

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v23

    move-object/from16 v3, v32

    move-object/from16 v4, v33

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v24, v0

    .line 584
    invoke-virtual/range {p1 .. p1}, Lcom/gsma/services/nfc/OffHostService;->getAidGroups()[Lcom/gsma/services/nfc/AidGroup;

    move-result-object v16

    .line 585
    .local v16, "groups":[Lcom/gsma/services/nfc/AidGroup;
    if-eqz v16, :cond_6

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v30, v0

    if-nez v30, :cond_7

    .line 586
    :cond_6
    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v30, v0

    goto/16 :goto_0

    .line 588
    :cond_7
    move-object/from16 v10, v16

    .local v10, "arr$":[Lcom/gsma/services/nfc/AidGroup;
    array-length v0, v10

    move/from16 v20, v0

    .local v20, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    move/from16 v18, v17

    .end local v10    # "arr$":[Lcom/gsma/services/nfc/AidGroup;
    .end local v17    # "i$":I
    .end local v20    # "len$":I
    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_b

    aget-object v15, v10, v18

    .line 589
    .local v15, "g":Lcom/gsma/services/nfc/AidGroup;
    new-instance v23, Landroid/content/ContentValues;

    .end local v23    # "v":Landroid/content/ContentValues;
    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 590
    .restart local v23    # "v":Landroid/content/ContentValues;
    const-string v30, "description"

    invoke-virtual {v15}, Lcom/gsma/services/nfc/AidGroup;->getDescription()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const-string v30, "category"

    invoke-virtual {v15}, Lcom/gsma/services/nfc/AidGroup;->getCategory()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string v30, "offhostserviceid"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 594
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    const-string v31, "aidgroup"

    const/16 v32, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v26

    .line 596
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 602
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/nfc/cardemulation/DynamicDBManager;->selectAidGroups(I)Landroid/database/Cursor;

    move-result-object v12

    .line 603
    const/4 v7, 0x0

    .line 604
    .restart local v7    # "_id2":I
    if-eqz v12, :cond_8

    .line 605
    invoke-interface {v12}, Landroid/database/Cursor;->moveToLast()Z

    .line 606
    const-string v30, "_id"

    move-object/from16 v0, v30

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 607
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 609
    :cond_8
    invoke-virtual {v15}, Lcom/gsma/services/nfc/AidGroup;->getAids()[Ljava/lang/String;

    move-result-object v9

    .line 610
    .local v9, "aids":[Ljava/lang/String;
    if-eqz v9, :cond_9

    array-length v0, v9

    move/from16 v30, v0

    if-nez v30, :cond_a

    .line 588
    .end local v18    # "i$":I
    :cond_9
    add-int/lit8 v17, v18, 0x1

    .restart local v17    # "i$":I
    move/from16 v18, v17

    .end local v17    # "i$":I
    .restart local v18    # "i$":I
    goto/16 :goto_1

    .line 597
    .end local v7    # "_id2":I
    .end local v9    # "aids":[Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 598
    .local v14, "e":Landroid/database/SQLException;
    :try_start_1
    invoke-virtual {v14}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    .end local v14    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v30

    .line 613
    .restart local v7    # "_id2":I
    .restart local v9    # "aids":[Ljava/lang/String;
    :cond_a
    move-object v11, v9

    .local v11, "arr$":[Ljava/lang/String;
    array-length v0, v11

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v17, 0x0

    .end local v18    # "i$":I
    .restart local v17    # "i$":I
    :goto_3
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    aget-object v8, v11, v17

    .line 614
    .local v8, "aid":Ljava/lang/String;
    new-instance v23, Landroid/content/ContentValues;

    .end local v23    # "v":Landroid/content/ContentValues;
    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 615
    .restart local v23    # "v":Landroid/content/ContentValues;
    const-string v30, "aid"

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/nfc/cardemulation/DynamicDBManager;->enc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const-string v30, "groupid"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    const-string v31, "aid"

    const/16 v32, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v28

    .line 620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 613
    :goto_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 621
    :catch_1
    move-exception v14

    .line 622
    .restart local v14    # "e":Landroid/database/SQLException;
    :try_start_3
    invoke-virtual {v14}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_4

    .end local v14    # "e":Landroid/database/SQLException;
    :catchall_1
    move-exception v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/cardemulation/DynamicDBManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v30

    .line 628
    .end local v7    # "_id2":I
    .end local v8    # "aid":Ljava/lang/String;
    .end local v9    # "aids":[Ljava/lang/String;
    .end local v11    # "arr$":[Ljava/lang/String;
    .end local v15    # "g":Lcom/gsma/services/nfc/AidGroup;
    .end local v17    # "i$":I
    .end local v21    # "len$":I
    .restart local v18    # "i$":I
    :cond_b
    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v30, v0

    goto/16 :goto_0
.end method
