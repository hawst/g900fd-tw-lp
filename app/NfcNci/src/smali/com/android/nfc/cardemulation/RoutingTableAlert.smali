.class public Lcom/android/nfc/cardemulation/RoutingTableAlert;
.super Lcom/android/internal/app/AlertActivity;
.source "RoutingTableAlert.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "RoutingTabletAlert"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 94
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SEC_NFC_ADVANCED_SETTING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->startActivity(Landroid/content/Intent;)V

    .line 96
    const-string v1, "RoutingTabletAlert"

    const-string v2, "Show Warning Popup"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    const v1, 0x103048d

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->setTheme(I)V

    .line 54
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lcom/android/nfc/cardemulation/RoutingTableAlert;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    .line 58
    .local v0, "ap":Lcom/android/internal/app/AlertController$AlertParams;
    const v1, 0x7f07003c

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 63
    const-string v1, "RoutingTabletAlert"

    const-string v2, "SETTINGS ACTIVITY STYLE : DEFAULT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const-string v1, "GRIDLIST"

    const-string v2, "DEFAULT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    const v1, 0x7f07003b

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    .line 70
    :goto_0
    const v1, 0x7f070038

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    .line 71
    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    .line 75
    const-string v1, "Vzw"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_StatusBarIconType"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    const v1, 0x7f07003e

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 77
    const v1, 0x7f07003f

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    .line 78
    const v1, 0x7f07002c

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    .line 79
    const v1, 0x7f07003d

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    .line 81
    :cond_0
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    .line 82
    invoke-virtual {p0}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->setupAlert()V

    .line 83
    return-void

    .line 67
    :cond_1
    const v1, 0x7f07003a

    invoke-virtual {p0, v1}, Lcom/android/nfc/cardemulation/RoutingTableAlert;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onResume()V

    .line 88
    const-string v0, "RoutingTabletAlert"

    const-string v1, " onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void
.end method
