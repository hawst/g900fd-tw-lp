.class public Lcom/android/nfc/cardemulation/NfcListenParameterConstants;
.super Ljava/lang/Object;
.source "NfcListenParameterConstants.java"


# static fields
.field public static final AID_ENTRY:I = 0x4

.field public static final NFC_LISTEN_PROTO_ISO_DEP:I = 0x1

.field public static final NFC_LISTEN_PROTO_NFC_DEP:I = 0x2

.field public static final NFC_LISTEN_TECH_A:I = 0x1

.field public static final NFC_LISTEN_TECH_B:I = 0x2

.field public static final NFC_LISTEN_TECH_F:I = 0x4

.field public static final PROTOCOL_ETNRY:I = 0x2

.field public static final ROUTE_OFF:I = 0x1

.field public static final ROUTE_ON_ALWAYS:I = 0x5

.field public static final ROUTE_ON_WHEN_POWER_ON:I = 0x4

.field public static final ROUTE_ON_WHEN_SCREEN_ON:I = 0x2

.field public static final ROUTE_ON_WHEN_SCREEN_UNLOCK:I = 0x3

.field public static final ROUTE_POWER_BATTERY_OFF:I = 0x4

.field public static final ROUTE_POWER_SWICHED_OFF:I = 0x2

.field public static final ROUTE_POWER_SWICHED_ON:I = 0x1

.field public static final ROUTE_SCREEN_OFF:I = 0x8

.field public static final ROUTE_SCREEN_ON_LOCKED:I = 0x10

.field public static final ROUTE_SCREEN_ON_UNLOCKED:I = 0x20

.field public static final TECH_ENTRY:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
