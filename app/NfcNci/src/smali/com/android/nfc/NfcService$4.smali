.class Lcom/android/nfc/NfcService$4;
.super Landroid/content/BroadcastReceiver;
.source "NfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/NfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcService;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcService;)V
    .locals 0

    .prologue
    .line 4554
    iput-object p1, p0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4557
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 4559
    .local v3, "action":Ljava/lang/String;
    const-string v16, "com.sec.android.app.nfctest.NFC_TEST_START"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 4560
    const-string v16, "NfcService"

    const-string v17, "Start Factory Test"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    # setter for: Lcom/android/nfc/NfcService;->mTestMode:Z
    invoke-static/range {v16 .. v17}, Lcom/android/nfc/NfcService;->access$2402(Lcom/android/nfc/NfcService;Z)Z

    .line 4562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # setter for: Lcom/android/nfc/NfcService;->mNoDiscoveryNfcOn:Z
    invoke-static/range {v16 .. v17}, Lcom/android/nfc/NfcService;->access$1202(Lcom/android/nfc/NfcService;Z)Z

    .line 4686
    :cond_0
    :goto_0
    return-void

    .line 4563
    :cond_1
    const-string v16, "com.sec.android.app.nfctest.NFC_TEST_END"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 4564
    const-string v16, "NfcService"

    const-string v17, "END Factory Test"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # setter for: Lcom/android/nfc/NfcService;->mTestMode:Z
    invoke-static/range {v16 .. v17}, Lcom/android/nfc/NfcService;->access$2402(Lcom/android/nfc/NfcService;Z)Z

    .line 4566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # setter for: Lcom/android/nfc/NfcService;->mNoDiscoveryNfcOn:Z
    invoke-static/range {v16 .. v17}, Lcom/android/nfc/NfcService;->access$1202(Lcom/android/nfc/NfcService;Z)Z

    .line 4567
    const-string v16, "NXP_PN547C2"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v16, "NXP_PN548C2"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v16, "CXD2235BGG"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v16, "S3FNRN3"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v16, "S3FWRN5"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v16, "BCM2079x"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 4573
    :cond_2
    const-string v16, "NfcService"

    const-string v17, "NFC No Action"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4575
    :cond_3
    new-instance v16, Lcom/android/nfc/NfcService$ApplyRoutingTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lcom/android/nfc/NfcService$ApplyRoutingTask;-><init>(Lcom/android/nfc/NfcService;)V

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/android/nfc/NfcService$ApplyRoutingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 4577
    :cond_4
    const-string v16, "com.sec.android.app.nfctest.NFC_DISCOVERY_ENABLE"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 4578
    const-string v16, "NfcService"

    const-string v17, "Enable Discovery"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v16

    if-nez v16, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/nfc/NfcService;->mState:I

    move/from16 v16, v0

    const/16 v17, 0x5

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_5

    .line 4580
    const-string v16, "NfcService"

    const-string v17, "Skip due to NFC off"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4583
    :cond_5
    const-string v16, "NfcService"

    const-string v17, "Enable Discovery"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    const/16 v17, 0x3

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lcom/android/nfc/NfcService;->mScreenState:I

    .line 4585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/nfc/NfcService;->mScreenState:I

    move/from16 v17, v0

    # invokes: Lcom/android/nfc/NfcService;->computeDiscoveryParameters(I)Lcom/android/nfc/NfcDiscoveryParameters;
    invoke-static/range {v16 .. v17}, Lcom/android/nfc/NfcService;->access$3400(Lcom/android/nfc/NfcService;I)Lcom/android/nfc/NfcDiscoveryParameters;

    move-result-object v8

    .line 4586
    .local v8, "newParams":Lcom/android/nfc/NfcDiscoveryParameters;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mCurrentDiscoveryParameters:Lcom/android/nfc/NfcDiscoveryParameters;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/android/nfc/NfcDiscoveryParameters;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mScreenStateHelper:Lcom/android/nfc/ScreenStateHelper;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$900(Lcom/android/nfc/NfcService;)Lcom/android/nfc/ScreenStateHelper;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/android/nfc/ScreenStateHelper;->checkScreenState()I

    move-result v16

    const/16 v17, 0x3

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_7

    .line 4588
    :cond_6
    invoke-virtual {v8}, Lcom/android/nfc/NfcDiscoveryParameters;->shouldEnableDiscovery()Z

    move-result v16

    if-eqz v16, :cond_7

    .line 4589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mCurrentDiscoveryParameters:Lcom/android/nfc/NfcDiscoveryParameters;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/nfc/NfcDiscoveryParameters;->shouldEnableDiscovery()Z

    move-result v11

    .line 4590
    .local v11, "shouldRestart":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v8, v11}, Lcom/android/nfc/DeviceHost;->enableDiscovery(Lcom/android/nfc/NfcDiscoveryParameters;Z)V

    .line 4593
    .end local v11    # "shouldRestart":Z
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v8, v0, Lcom/android/nfc/NfcService;->mCurrentDiscoveryParameters:Lcom/android/nfc/NfcDiscoveryParameters;

    goto/16 :goto_0

    .line 4594
    .end local v8    # "newParams":Lcom/android/nfc/NfcDiscoveryParameters;
    :cond_8
    const-string v16, "com.sec.android.app.nfctest.NFC_DISCOVERY_DISABLE"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 4595
    const-string v16, "NfcService"

    const-string v17, "Disable Discovery"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4596
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v16

    if-nez v16, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/nfc/NfcService;->mState:I

    move/from16 v16, v0

    const/16 v17, 0x5

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_9

    .line 4597
    const-string v16, "NfcService"

    const-string v17, "Skip due to NFC off"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4600
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost;->disableDiscovery()V

    goto/16 :goto_0

    .line 4601
    :cond_a
    const-string v16, "com.sec.android.app.nfctest.NFC_ON_NO_DISCOVERY"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_b

    .line 4602
    const-string v16, "NfcService"

    const-string v17, "Disable Discovery When NFC ON"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4603
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    # setter for: Lcom/android/nfc/NfcService;->mNoDiscoveryNfcOn:Z
    invoke-static/range {v16 .. v17}, Lcom/android/nfc/NfcService;->access$1202(Lcom/android/nfc/NfcService;Z)Z

    goto/16 :goto_0

    .line 4604
    :cond_b
    const-string v16, "com.sec.android.app.nfctest.NFC_CHECK_SIM"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 4605
    const-string v16, "NfcService"

    const-string v17, "check SIM I/O"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v16

    if-nez v16, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/nfc/NfcService;->mState:I

    move/from16 v16, v0

    const/16 v17, 0x5

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_c

    .line 4607
    const-string v16, "NfcService"

    const-string v17, "Skip due to NFC off"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4610
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    const/16 v17, 0x0

    invoke-interface/range {v16 .. v17}, Lcom/android/nfc/DeviceHost;->SWPSelfTest(I)I

    move-result v12

    .line 4611
    .local v12, "status":I
    const-string v16, "NfcService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "SWP TEST Result:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4612
    new-instance v10, Landroid/content/Intent;

    const-string v16, "com.sec.android.app.nfctest.NFC_CHECK_SIM_RESPONSE"

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4613
    .local v10, "respIntent":Landroid/content/Intent;
    const-string v16, "SIM_DATA"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4615
    .end local v10    # "respIntent":Landroid/content/Intent;
    .end local v12    # "status":I
    :cond_d
    const-string v16, "com.sec.android.app.nfctest.NFC_CHECK_ESE"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_f

    .line 4616
    const-string v16, "NfcService"

    const-string v17, "check eSE I/O"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v16

    if-nez v16, :cond_e

    .line 4618
    const-string v16, "NfcService"

    const-string v17, "Skip due to NFC off"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4621
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    const/16 v17, 0x1

    invoke-interface/range {v16 .. v17}, Lcom/android/nfc/DeviceHost;->SWPSelfTest(I)I

    move-result v12

    .line 4622
    .restart local v12    # "status":I
    const-string v16, "NfcService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ESE TEST Result:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4623
    new-instance v10, Landroid/content/Intent;

    const-string v16, "com.sec.android.app.nfctest.NFC_CHECK_ESE_RESPONSE"

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4624
    .restart local v10    # "respIntent":Landroid/content/Intent;
    const-string v16, "SIM_DATA"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4626
    .end local v10    # "respIntent":Landroid/content/Intent;
    .end local v12    # "status":I
    :cond_f
    const-string v16, "com.sec.android.app.nfctest.PRBS_TEST_ON"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_12

    .line 4627
    const-string v16, "NfcService"

    const-string v17, "PRBS_ON"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4628
    new-instance v4, Lcom/android/nfc/NfcService$EnableDisableTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Lcom/android/nfc/NfcService$EnableDisableTask;-><init>(Lcom/android/nfc/NfcService;)V

    .line 4629
    .local v4, "disableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x2

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/android/nfc/NfcService$EnableDisableTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 4631
    :try_start_0
    invoke-virtual {v4}, Lcom/android/nfc/NfcService$EnableDisableTask;->get()Ljava/lang/Object;

    .line 4632
    const-string v16, "NXP_PN547C2"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_10

    const-string v16, "NXP_PN548C2"

    const-string v17, "NXP_PN547C2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_11

    .line 4634
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost;->initialize()Z

    .line 4636
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    const-string v17, "TECH"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    const-string v18, "RATE"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    invoke-interface/range {v16 .. v18}, Lcom/android/nfc/DeviceHost;->doPrbsOn(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 4637
    :catch_0
    move-exception v5

    .line 4638
    .local v5, "e":Ljava/lang/Exception;
    const-string v16, "NfcService"

    const-string v17, "failed to prbsOff"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4640
    .end local v4    # "disableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_12
    const-string v16, "com.sec.android.app.nfctest.PRBS_TEST_OFF"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_13

    .line 4641
    const-string v16, "NfcService"

    const-string v17, "PRBS_OFF"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost;->doPrbsOff()V

    .line 4643
    new-instance v6, Lcom/android/nfc/NfcService$EnableDisableTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Lcom/android/nfc/NfcService$EnableDisableTask;-><init>(Lcom/android/nfc/NfcService;)V

    .line 4644
    .local v6, "enableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/android/nfc/NfcService$EnableDisableTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 4646
    :try_start_1
    invoke-virtual {v6}, Lcom/android/nfc/NfcService$EnableDisableTask;->get()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 4647
    :catch_1
    move-exception v5

    .line 4648
    .restart local v5    # "e":Ljava/lang/Exception;
    const-string v16, "NfcService"

    const-string v17, "failed to prbsOff"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4650
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "enableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    :cond_13
    const-string v16, "services.nfc.action.GET_FIRMWARE_VERSION"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_18

    .line 4651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost;->getFWVersion()I

    move-result v14

    .line 4652
    .local v14, "ver":I
    const-string v13, "unknown"

    .line 4653
    .local v13, "vendor":Ljava/lang/String;
    const-string v15, "unknown"

    .line 4654
    .local v15, "version":Ljava/lang/String;
    const-string v16, "NXP_PN547C2"

    const-string v17, "NXP"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_15

    .line 4655
    const-string v13, "NXP"

    .line 4656
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v17, v14, 0x10

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    shr-int/lit8 v17, v14, 0x8

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    and-int/lit16 v0, v14, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 4666
    :cond_14
    :goto_1
    new-instance v10, Landroid/content/Intent;

    const-string v16, "services.nfc.action.GET_FIRMWARE_VERSION_RESPONSE"

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4667
    .restart local v10    # "respIntent":Landroid/content/Intent;
    const-string v16, "vendor"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4668
    const-string v16, "version"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4669
    const-string v16, "package"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4670
    .local v9, "pkg":Ljava/lang/String;
    if-eqz v9, :cond_17

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_17

    .line 4671
    invoke-virtual {v10, v9}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4672
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4658
    .end local v9    # "pkg":Ljava/lang/String;
    .end local v10    # "respIntent":Landroid/content/Intent;
    :cond_15
    const-string v16, "NXP_PN547C2"

    const-string v17, "BCM"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_16

    .line 4659
    const-string v13, "Broadcom"

    .line 4660
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v17, v14, 0x8

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    and-int/lit16 v0, v14, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ".00"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_1

    .line 4661
    :cond_16
    const-string v16, "NXP_PN547C2"

    const-string v17, "S3"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_14

    .line 4662
    const-string v13, "S.LSI"

    .line 4663
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    shr-int/lit8 v17, v14, 0x10

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    shr-int/lit8 v17, v14, 0x8

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    and-int/lit16 v0, v14, 0xff

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_1

    .line 4674
    .restart local v9    # "pkg":Ljava/lang/String;
    .restart local v10    # "respIntent":Landroid/content/Intent;
    :cond_17
    const-string v16, "NfcService"

    const-string v17, "can\'t send response"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4676
    .end local v9    # "pkg":Ljava/lang/String;
    .end local v10    # "respIntent":Landroid/content/Intent;
    .end local v13    # "vendor":Ljava/lang/String;
    .end local v14    # "ver":I
    .end local v15    # "version":Ljava/lang/String;
    :cond_18
    const-string v16, "com.sec.android.app.nfctest.GET_ESE_TYPE"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_19

    .line 4677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost;->doGetSecureElementTechList()I

    move-result v7

    .line 4678
    .local v7, "ese_type":I
    const-string v16, "NfcService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "[eSE] Intent GET_ESE_TYPE from NFC Test app :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4679
    new-instance v10, Landroid/content/Intent;

    const-string v16, "com.sec.android.app.nfctest.GET_ESE_TYPE_RESPONSE"

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4680
    .restart local v10    # "respIntent":Landroid/content/Intent;
    const-string v16, "ESE_TYPE"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4681
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4682
    .end local v7    # "ese_type":I
    .end local v10    # "respIntent":Landroid/content/Intent;
    :cond_19
    const-string v16, "com.sec.android.app.nfctest.SET_ESE_TYPE"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 4683
    const-string v16, "NfcService"

    const-string v17, "[eSE] Intent SET_ESE_TYPE from NFC Test app"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$4;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v16, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v16 .. v16}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v16

    const-string v17, "ESE_TYPE"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    invoke-interface/range {v16 .. v17}, Lcom/android/nfc/DeviceHost;->doSetSecureElementListenTechMask(I)V

    goto/16 :goto_0
.end method
