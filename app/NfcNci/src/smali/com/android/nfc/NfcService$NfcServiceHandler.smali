.class final Lcom/android/nfc/NfcService$NfcServiceHandler;
.super Landroid/os/Handler;
.source "NfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/NfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "NfcServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcService;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcService;)V
    .locals 0

    .prologue
    .line 3548
    iput-object p1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private dispatchTagEndpoint(Lcom/android/nfc/DeviceHost$TagEndpoint;Lcom/android/nfc/NfcService$ReaderModeParams;)V
    .locals 11
    .param p1, "tagEndpoint"    # Lcom/android/nfc/DeviceHost$TagEndpoint;
    .param p2, "readerParams"    # Lcom/android/nfc/NfcService$ReaderModeParams;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 4207
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mTestMode:Z
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$2400(Lcom/android/nfc/NfcService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4209
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1, v9}, Lcom/android/nfc/NfcService;->playSound(I)V

    .line 4210
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v1, v1, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    const-string v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Vibrator;

    .line 4211
    .local v8, "vibe":Landroid/os/Vibrator;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v8, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 4259
    .end local v8    # "vibe":Landroid/os/Vibrator;
    :cond_0
    :goto_0
    return-void

    .line 4216
    :cond_1
    new-instance v0, Landroid/nfc/Tag;

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$TagEndpoint;->getUid()[B

    move-result-object v1

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$TagEndpoint;->getTechList()[I

    move-result-object v2

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$TagEndpoint;->getTechExtras()[Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$TagEndpoint;->getHandle()I

    move-result v4

    iget-object v5, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v5, v5, Lcom/android/nfc/NfcService;->mNfcTagService:Lcom/android/nfc/NfcService$TagService;

    invoke-direct/range {v0 .. v5}, Landroid/nfc/Tag;-><init>([B[I[Landroid/os/Bundle;ILandroid/nfc/INfcTag;)V

    .line 4218
    .local v0, "tag":Landroid/nfc/Tag;
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1, p1}, Lcom/android/nfc/NfcService;->registerTagObject(Lcom/android/nfc/DeviceHost$TagEndpoint;)V

    .line 4219
    if-eqz p2, :cond_3

    .line 4221
    :try_start_0
    iget v1, p2, Lcom/android/nfc/NfcService$ReaderModeParams;->flags:I

    and-int/lit16 v1, v1, 0x100

    if-nez v1, :cond_2

    .line 4222
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/nfc/NfcService;->playSound(I)V

    .line 4224
    :cond_2
    iget-object v1, p2, Lcom/android/nfc/NfcService$ReaderModeParams;->callback:Landroid/nfc/IAppCallback;

    if-eqz v1, :cond_3

    .line 4225
    iget-object v1, p2, Lcom/android/nfc/NfcService$ReaderModeParams;->callback:Landroid/nfc/IAppCallback;

    invoke-interface {v1, v0}, Landroid/nfc/IAppCallback;->onTagDiscovered(Landroid/nfc/Tag;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 4230
    :catch_0
    move-exception v7

    .line 4231
    .local v7, "e":Landroid/os/RemoteException;
    const-string v1, "NfcService"

    const-string v2, "Reader mode remote has died, falling back."

    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4239
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_3
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mNfcDispatcher:Lcom/android/nfc/NfcDispatcher;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$1300(Lcom/android/nfc/NfcService;)Lcom/android/nfc/NfcDispatcher;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/nfc/NfcDispatcher;->dispatchTag(Landroid/nfc/Tag;)I

    move-result v6

    .line 4240
    .local v6, "dispatchResult":I
    if-ne v6, v10, :cond_7

    .line 4241
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$TagEndpoint;->getHandle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/nfc/NfcService;->unregisterObject(I)V

    .line 4242
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1, v10}, Lcom/android/nfc/NfcService;->playSound(I)V

    .line 4248
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isCheckTagConnetion:Z
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$2800(Lcom/android/nfc/NfcService;)Z

    move-result v1

    if-ne v1, v9, :cond_5

    .line 4249
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v1, v1, Lcom/android/nfc/NfcService;->CheckTagConnection:Lcom/android/nfc/NfcService$CheckTagConnectionThread;

    invoke-virtual {v1}, Lcom/android/nfc/NfcService$CheckTagConnectionThread;->cancel()V

    .line 4251
    :cond_5
    const-string v1, "KOREA"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_ConfigOperatorSettingUI"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4252
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$200(Lcom/android/nfc/NfcService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "disable_dialog"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4253
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_6

    const-string v1, "NfcService"

    const-string v2, "CheckTagConnection.start()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4254
    :cond_6
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    new-instance v2, Lcom/android/nfc/NfcService$CheckTagConnectionThread;

    iget-object v3, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    const v4, 0xea60

    invoke-direct {v2, v3, p1, v4}, Lcom/android/nfc/NfcService$CheckTagConnectionThread;-><init>(Lcom/android/nfc/NfcService;Lcom/android/nfc/DeviceHost$TagEndpoint;I)V

    iput-object v2, v1, Lcom/android/nfc/NfcService;->CheckTagConnection:Lcom/android/nfc/NfcService$CheckTagConnectionThread;

    .line 4255
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v1, v1, Lcom/android/nfc/NfcService;->CheckTagConnection:Lcom/android/nfc/NfcService$CheckTagConnectionThread;

    invoke-virtual {v1}, Lcom/android/nfc/NfcService$CheckTagConnectionThread;->start()V

    goto/16 :goto_0

    .line 4233
    .end local v6    # "dispatchResult":I
    :catch_1
    move-exception v7

    .line 4235
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "NfcService"

    const-string v2, "App exception, not dispatching."

    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 4243
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "dispatchResult":I
    :cond_7
    if-ne v6, v9, :cond_4

    .line 4244
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1, v9}, Lcom/android/nfc/NfcService;->playSound(I)V

    goto :goto_1
.end method

.method private llcpActivated(Lcom/android/nfc/DeviceHost$NfcDepEndpoint;)Z
    .locals 4
    .param p1, "device"    # Lcom/android/nfc/DeviceHost$NfcDepEndpoint;

    .prologue
    const/4 v0, 0x1

    .line 4151
    const-string v1, "NfcService"

    const-string v2, "LLCP Activation message"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4153
    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->getMode()I

    move-result v1

    if-nez v1, :cond_7

    .line 4154
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcService"

    const-string v2, "NativeP2pDevice.MODE_P2P_TARGET"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4155
    :cond_0
    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->connect()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 4157
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/nfc/DeviceHost;->doCheckLlcp()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4159
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/nfc/DeviceHost;->doActivateLlcp()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4160
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "NfcService"

    const-string v2, "Initiator Activate LLCP OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4161
    :cond_1
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    monitor-enter v1

    .line 4163
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v2, v2, Lcom/android/nfc/NfcService;->mObjectMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->getHandle()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4164
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4165
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v1, v1, Lcom/android/nfc/NfcService;->mP2pLinkManager:Lcom/android/nfc/P2pLinkManager;

    invoke-virtual {v1}, Lcom/android/nfc/P2pLinkManager;->onLlcpActivated()V

    .line 4202
    :goto_0
    return v0

    .line 4164
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 4169
    :cond_2
    const-string v0, "NfcService"

    const-string v1, "Initiator LLCP activation failed. Disconnect."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4170
    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->disconnect()Z

    .line 4202
    :cond_3
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 4173
    :cond_4
    sget-boolean v0, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v0, :cond_5

    const-string v0, "NfcService"

    const-string v1, "Remote Target does not support LLCP. Disconnect."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4174
    :cond_5
    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->disconnect()Z

    goto :goto_1

    .line 4177
    :cond_6
    sget-boolean v0, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v0, :cond_3

    const-string v0, "NfcService"

    const-string v1, "Cannot connect remote Target. Polling loop restarted."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 4183
    :cond_7
    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->getMode()I

    move-result v1

    if-ne v1, v0, :cond_3

    .line 4184
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_8

    const-string v1, "NfcService"

    const-string v2, "NativeP2pDevice.MODE_P2P_INITIATOR"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4186
    :cond_8
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/nfc/DeviceHost;->doCheckLlcp()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 4188
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/nfc/DeviceHost;->doActivateLlcp()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4189
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_9

    const-string v1, "NfcService"

    const-string v2, "Target Activate LLCP OK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4190
    :cond_9
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    monitor-enter v1

    .line 4192
    :try_start_2
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v2, v2, Lcom/android/nfc/NfcService;->mObjectMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->getHandle()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4193
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4194
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v1, v1, Lcom/android/nfc/NfcService;->mP2pLinkManager:Lcom/android/nfc/P2pLinkManager;

    invoke-virtual {v1}, Lcom/android/nfc/P2pLinkManager;->onLlcpActivated()V

    goto :goto_0

    .line 4193
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 4198
    :cond_a
    const-string v0, "NfcService"

    const-string v1, "checkLlcp failed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private makeAidIntent([B[B)Landroid/content/Intent;
    .locals 4
    .param p1, "aid"    # [B
    .param p2, "param"    # [B

    .prologue
    .line 4125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4128
    .local v0, "aidIntent":Landroid/content/Intent;
    const-string v2, "ISIS"

    # getter for: Lcom/android/nfc/NfcService;->mSecureEventType:Ljava/lang/String;
    invoke-static {}, Lcom/android/nfc/NfcService;->access$2200()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4129
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "NfcService"

    const-string v3, "MAKE : com.gsma.services.nfc.action.TRANSACTION_EVENT"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4130
    :cond_0
    const-string v2, "com.gsma.services.nfc.action.TRANSACTION_EVENT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4131
    const-string v2, "com.gsma.services.nfc.extra.AID"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 4132
    const-string v2, "com.gsma.services.nfc.extra.DATA"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 4134
    const-string v2, "nfc://secure:0/SIM"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 4135
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4145
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v0

    .line 4139
    :cond_1
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_2

    const-string v2, "NfcService"

    const-string v3, "MAKE : com.android.nfc_extras.action.AID_SELECTED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4140
    :cond_2
    const-string v2, "com.android.nfc_extras.action.AID_SELECTED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4141
    const-string v2, "com.android.nfc_extras.extra.AID"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 4142
    const-string v2, "com.android.nfc_extras.extra.DATA"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto :goto_0
.end method

.method private sendGpacRuleBasedBroadcast(Landroid/content/Intent;)V
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4046
    const/4 v6, 0x0

    .line 4047
    .local v6, "packageArray":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 4048
    .local v1, "allowedResults":[Z
    const/4 v3, 0x0

    .line 4050
    .local v3, "gpacInfo":Lcom/android/nfc/NfcService$GpacPermissionInfo;
    iget-object v10, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v10, v10, Lcom/android/nfc/NfcService;->mGpacPermissions:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 4051
    .local v2, "gpacEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/nfc/NfcService$GpacPermissionInfo;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 4052
    .local v0, "action":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "gpacInfo":Lcom/android/nfc/NfcService$GpacPermissionInfo;
    check-cast v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;

    .line 4053
    .restart local v3    # "gpacInfo":Lcom/android/nfc/NfcService$GpacPermissionInfo;
    iget-object v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->actionList:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 4054
    const-string v10, "NfcService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "permission: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->permName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4055
    const-string v10, "NfcService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " Action  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4056
    iget-object v7, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->packageList:Ljava/util/LinkedList;

    .line 4057
    .local v7, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-interface {v7, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "packageArray":[Ljava/lang/String;
    check-cast v6, [Ljava/lang/String;

    .line 4063
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "gpacEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/nfc/NfcService$GpacPermissionInfo;>;"
    .end local v7    # "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6    # "packageArray":[Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_3

    .line 4064
    const-string v10, "NfcService"

    const-string v11, " package is null"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4103
    :cond_2
    return-void

    .line 4069
    :cond_3
    if-eqz v3, :cond_5

    iget-boolean v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->forceUpdate:Z

    if-eqz v10, :cond_5

    .line 4070
    iget-object v10, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v10, v10, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    iget-object v11, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v11, v11, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    iget-object v11, v11, Lcom/android/nfc/HciEventControl;->READER_SIM_S:Ljava/lang/String;

    invoke-virtual {v10, v11, p1, v6}, Lcom/android/nfc/HciEventControl;->isEvtAllowed(Ljava/lang/String;Landroid/content/Intent;[Ljava/lang/String;)[Z

    move-result-object v10

    iput-object v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->allowedResult:[Z

    .line 4073
    iget v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->updateRule:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_5

    .line 4074
    const/4 v10, 0x0

    iput-boolean v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->forceUpdate:Z

    .line 4077
    new-instance v8, Lcom/android/nfc/NfcService$NfcServiceHandler$2;

    invoke-direct {v8, p0}, Lcom/android/nfc/NfcService$NfcServiceHandler$2;-><init>(Lcom/android/nfc/NfcService$NfcServiceHandler;)V

    .line 4091
    .local v8, "task":Ljava/util/TimerTask;
    sget-boolean v10, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v10, :cond_4

    const-string v10, "NfcService"

    const-string v11, "timer is stared"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4092
    :cond_4
    new-instance v9, Ljava/util/Timer;

    invoke-direct {v9}, Ljava/util/Timer;-><init>()V

    .line 4093
    .local v9, "timer":Ljava/util/Timer;
    const-wide/16 v10, 0x1388

    invoke-virtual {v9, v8, v10, v11}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 4097
    .end local v8    # "task":Ljava/util/TimerTask;
    .end local v9    # "timer":Ljava/util/Timer;
    :cond_5
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->allowedResult:[Z

    array-length v10, v10

    if-ge v4, v10, :cond_2

    .line 4098
    iget-object v10, v3, Lcom/android/nfc/NfcService$GpacPermissionInfo;->allowedResult:[Z

    aget-boolean v10, v10, v4

    if-eqz v10, :cond_6

    .line 4099
    aget-object v10, v6, v4

    invoke-virtual {p1, v10}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4100
    iget-object v10, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v10, v10, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    invoke-virtual {v10, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4097
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private sendNfcEeAccessProtectedBroadcast(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4030
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v2, v2, Lcom/android/nfc/NfcService;->mInstalledPackages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 4031
    .local v1, "pkg":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    .line 4032
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mNfceeAccessControl:Lcom/android/nfc/NfceeAccessControl;
    invoke-static {v2}, Lcom/android/nfc/NfcService;->access$1900(Lcom/android/nfc/NfcService;)Lcom/android/nfc/NfceeAccessControl;

    move-result-object v2

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v3}, Lcom/android/nfc/NfceeAccessControl;->check(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4033
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 4034
    const-string v2, "ISIS"

    # getter for: Lcom/android/nfc/NfcService;->mSecureEventType:Ljava/lang/String;
    invoke-static {}, Lcom/android/nfc/NfcService;->access$2200()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4035
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v2, v2, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    const-string v3, "android.permission.NFC"

    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    .line 4037
    :cond_1
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    iget-object v2, v2, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 4042
    .end local v1    # "pkg":Landroid/content/pm/PackageInfo;
    :cond_2
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 52
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 3551
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v48, v0

    sparse-switch v48, :sswitch_data_0

    .line 4017
    const-string v48, "NfcService"

    const-string v49, "Unknown message received"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4020
    :cond_0
    :goto_0
    return-void

    .line 3553
    :sswitch_0
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v39, v0

    .line 3554
    .local v39, "route":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v36, v0

    .line 3555
    .local v36, "power":I
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    .line 3556
    .local v8, "aid":Ljava/lang/String;
    const-string v48, "NfcService"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "MSG_ROUTE_AID route - "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", power "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v48

    invoke-static {v8}, Lcom/android/nfc/NfcService;->hexStringToBytes(Ljava/lang/String;)[B

    move-result-object v49

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    move/from16 v2, v39

    move/from16 v3, v36

    invoke-interface {v0, v1, v2, v3}, Lcom/android/nfc/DeviceHost;->routeAid([BII)Z

    move-result v42

    .line 3562
    .local v42, "success":Z
    if-nez v42, :cond_0

    .line 3563
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_1

    const-string v48, "NfcService"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v49

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, " is not inserted, So we have to remove it"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3564
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mCardEmulationManager:Lcom/android/nfc/cardemulation/CardEmulationManager;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$1100(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/CardEmulationManager;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v0, v8}, Lcom/android/nfc/cardemulation/CardEmulationManager;->onAidAddFailed(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3570
    .end local v8    # "aid":Ljava/lang/String;
    .end local v36    # "power":I
    .end local v39    # "route":I
    .end local v42    # "success":Z
    :sswitch_1
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    .line 3571
    .restart local v8    # "aid":Ljava/lang/String;
    const-string v48, "NfcService"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "MSG_UNROUTE_AID "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v48

    invoke-static {v8}, Lcom/android/nfc/NfcService;->hexStringToBytes(Ljava/lang/String;)[B

    move-result-object v49

    invoke-interface/range {v48 .. v49}, Lcom/android/nfc/DeviceHost;->unrouteAid([B)Z

    goto/16 :goto_0

    .line 3577
    .end local v8    # "aid":Ljava/lang/String;
    :sswitch_2
    const-string v48, "NfcService"

    const-string v49, "MSG_ROUTE_ENTRY"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3578
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v40, v0

    check-cast v40, Landroid/util/Pair;

    .line 3580
    .local v40, "routeInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v46, v0

    .line 3581
    .local v46, "type":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v47, v0

    .line 3582
    .local v47, "value":I
    move-object/from16 v0, v40

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, Ljava/lang/Integer;

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Integer;->intValue()I

    move-result v39

    .line 3583
    .restart local v39    # "route":I
    move-object/from16 v0, v40

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, Ljava/lang/Integer;

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Integer;->intValue()I

    move-result v36

    .line 3585
    .restart local v36    # "power":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v48

    move-object/from16 v0, v48

    move/from16 v1, v46

    move/from16 v2, v47

    move/from16 v3, v39

    move/from16 v4, v36

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/android/nfc/DeviceHost;->setRoutingEntry(IIII)Z

    goto/16 :goto_0

    .line 3590
    .end local v36    # "power":I
    .end local v39    # "route":I
    .end local v40    # "routeInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v46    # "type":I
    .end local v47    # "value":I
    :sswitch_3
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, Ljava/lang/Integer;

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Integer;->intValue()I

    move-result v46

    .line 3591
    .restart local v46    # "type":I
    const-string v48, "NfcService"

    const-string v49, "MSG_UNROUTE_ENTRY"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v48

    move-object/from16 v0, v48

    move/from16 v1, v46

    invoke-interface {v0, v1}, Lcom/android/nfc/DeviceHost;->clearRoutingEntry(I)Z

    goto/16 :goto_0

    .line 3597
    .end local v46    # "type":I
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mP2pLinkManager:Lcom/android/nfc/P2pLinkManager;

    move-object/from16 v49, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, Landroid/nfc/BeamShareData;

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/android/nfc/P2pLinkManager;->onManualBeamInvoke(Landroid/nfc/BeamShareData;)V

    goto/16 :goto_0

    .line 3616
    :sswitch_5
    const-string v48, "NfcService"

    const-string v49, "MSG_COMMIT_ROUTE"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v49, v0

    monitor-enter v49

    .line 3618
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v50, 0x1

    move-object/from16 v0, v48

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService;->applyListenModeRouting(Z)V

    .line 3619
    monitor-exit v49

    goto/16 :goto_0

    :catchall_0
    move-exception v48

    monitor-exit v49
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v48

    .line 3624
    :sswitch_6
    const-string v48, "BCM2079x"

    const-string v49, "NXP_PN547C2"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_0

    .line 3627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mCardEmulationManager:Lcom/android/nfc/cardemulation/CardEmulationManager;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$1100(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/CardEmulationManager;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/android/nfc/cardemulation/CardEmulationManager;->reRouteAllAids()V

    goto/16 :goto_0

    .line 3633
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mDeviceHost:Lcom/android/nfc/DeviceHost;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/DeviceHost;

    move-result-object v48

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v49, v0

    invoke-interface/range {v48 .. v49}, Lcom/android/nfc/DeviceHost;->onPpseRouted(I)Z

    goto/16 :goto_0

    .line 3637
    :sswitch_8
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v31, v0

    check-cast v31, Landroid/nfc/NdefMessage;

    .line 3638
    .local v31, "ndefMsg":Landroid/nfc/NdefMessage;
    new-instance v23, Landroid/os/Bundle;

    invoke-direct/range {v23 .. v23}, Landroid/os/Bundle;-><init>()V

    .line 3639
    .local v23, "extras":Landroid/os/Bundle;
    const-string v48, "ndefmsg"

    move-object/from16 v0, v23

    move-object/from16 v1, v48

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 3640
    const-string v48, "ndefmaxlength"

    const/16 v49, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v48

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3641
    const-string v48, "ndefcardstate"

    const/16 v49, 0x1

    move-object/from16 v0, v23

    move-object/from16 v1, v48

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3642
    const-string v48, "ndeftype"

    const/16 v49, -0x1

    move-object/from16 v0, v23

    move-object/from16 v1, v48

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3643
    const/16 v48, 0x1

    move/from16 v0, v48

    new-array v0, v0, [B

    move-object/from16 v48, v0

    const/16 v49, 0x0

    const/16 v50, 0x0

    aput-byte v50, v48, v49

    const/16 v49, 0x1

    move/from16 v0, v49

    new-array v0, v0, [I

    move-object/from16 v49, v0

    const/16 v50, 0x0

    const/16 v51, 0x6

    aput v51, v49, v50

    const/16 v50, 0x1

    move/from16 v0, v50

    new-array v0, v0, [Landroid/os/Bundle;

    move-object/from16 v50, v0

    const/16 v51, 0x0

    aput-object v23, v50, v51

    invoke-static/range {v48 .. v50}, Landroid/nfc/Tag;->createMockTag([B[I[Landroid/os/Bundle;)Landroid/nfc/Tag;

    move-result-object v43

    .line 3646
    .local v43, "tag":Landroid/nfc/Tag;
    const-string v48, "NfcService"

    const-string v49, "mock NDEF tag, starting corresponding activity"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3647
    const-string v48, "NfcService"

    invoke-virtual/range {v43 .. v43}, Landroid/nfc/Tag;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mNfcDispatcher:Lcom/android/nfc/NfcDispatcher;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$1300(Lcom/android/nfc/NfcService;)Lcom/android/nfc/NfcDispatcher;

    move-result-object v48

    move-object/from16 v0, v48

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcDispatcher;->dispatchTag(Landroid/nfc/Tag;)I

    move-result v18

    .line 3649
    .local v18, "dispatchStatus":I
    const/16 v48, 0x1

    move/from16 v0, v18

    move/from16 v1, v48

    if-ne v0, v1, :cond_2

    .line 3650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x1

    invoke-virtual/range {v48 .. v49}, Lcom/android/nfc/NfcService;->playSound(I)V

    goto/16 :goto_0

    .line 3651
    :cond_2
    const/16 v48, 0x2

    move/from16 v0, v18

    move/from16 v1, v48

    if-ne v0, v1, :cond_0

    .line 3652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x2

    invoke-virtual/range {v48 .. v49}, Lcom/android/nfc/NfcService;->playSound(I)V

    goto/16 :goto_0

    .line 3658
    .end local v18    # "dispatchStatus":I
    .end local v23    # "extras":Landroid/os/Bundle;
    .end local v31    # "ndefMsg":Landroid/nfc/NdefMessage;
    .end local v43    # "tag":Landroid/nfc/Tag;
    :sswitch_9
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_3

    const-string v48, "NfcService"

    const-string v49, "Tag detected, notifying applications"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3659
    :cond_3
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v43, v0

    check-cast v43, Lcom/android/nfc/DeviceHost$TagEndpoint;

    .line 3660
    .local v43, "tag":Lcom/android/nfc/DeviceHost$TagEndpoint;
    const/16 v38, 0x0

    .line 3661
    .local v38, "readerParams":Lcom/android/nfc/NfcService$ReaderModeParams;
    const/16 v37, 0x7d

    .line 3662
    .local v37, "presenceCheckDelay":I
    new-instance v12, Lcom/android/nfc/NfcService$NfcServiceHandler$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/android/nfc/NfcService$NfcServiceHandler$1;-><init>(Lcom/android/nfc/NfcService$NfcServiceHandler;)V

    .line 3670
    .local v12, "callback":Lcom/android/nfc/DeviceHost$TagDisconnectedCallback;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v49, v0

    monitor-enter v49

    .line 3671
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mReaderModeParams:Lcom/android/nfc/NfcService$ReaderModeParams;

    move-object/from16 v38, v0

    .line 3672
    monitor-exit v49
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3673
    if-eqz v38, :cond_5

    .line 3674
    move-object/from16 v0, v38

    iget v0, v0, Lcom/android/nfc/NfcService$ReaderModeParams;->presenceCheckDelay:I

    move/from16 v37, v0

    .line 3675
    move-object/from16 v0, v38

    iget v0, v0, Lcom/android/nfc/NfcService$ReaderModeParams;->flags:I

    move/from16 v48, v0

    move/from16 v0, v48

    and-int/lit16 v0, v0, 0x80

    move/from16 v48, v0

    if-eqz v48, :cond_5

    .line 3676
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_4

    const-string v48, "NfcService"

    const-string v49, "Skipping NDEF detection in reader mode"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3677
    :cond_4
    move-object/from16 v0, v43

    move/from16 v1, v37

    invoke-interface {v0, v1, v12}, Lcom/android/nfc/DeviceHost$TagEndpoint;->startPresenceChecking(ILcom/android/nfc/DeviceHost$TagDisconnectedCallback;)V

    .line 3678
    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/android/nfc/NfcService$NfcServiceHandler;->dispatchTagEndpoint(Lcom/android/nfc/DeviceHost$TagEndpoint;Lcom/android/nfc/NfcService$ReaderModeParams;)V

    goto/16 :goto_0

    .line 3672
    :catchall_1
    move-exception v48

    :try_start_2
    monitor-exit v49
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v48

    .line 3683
    :cond_5
    if-eqz v38, :cond_6

    move-object/from16 v0, v38

    iget v0, v0, Lcom/android/nfc/NfcService$ReaderModeParams;->flags:I

    move/from16 v48, v0

    move/from16 v0, v48

    and-int/lit16 v0, v0, 0x100

    move/from16 v48, v0

    if-nez v48, :cond_9

    :cond_6
    const/16 v34, 0x1

    .line 3685
    .local v34, "playSound":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget v0, v0, Lcom/android/nfc/NfcService;->mScreenState:I

    move/from16 v48, v0

    const/16 v49, 0x3

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_7

    if-eqz v34, :cond_7

    .line 3686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    invoke-virtual/range {v48 .. v49}, Lcom/android/nfc/NfcService;->playSound(I)V

    .line 3688
    :cond_7
    invoke-interface/range {v43 .. v43}, Lcom/android/nfc/DeviceHost$TagEndpoint;->getConnectedTechnology()I

    move-result v48

    const/16 v49, 0xa

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_a

    .line 3693
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_8

    const-string v48, "NfcService"

    const-string v49, "Skipping NDEF detection for NFC Barcode"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3694
    :cond_8
    move-object/from16 v0, v43

    move/from16 v1, v37

    invoke-interface {v0, v1, v12}, Lcom/android/nfc/DeviceHost$TagEndpoint;->startPresenceChecking(ILcom/android/nfc/DeviceHost$TagDisconnectedCallback;)V

    .line 3695
    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/android/nfc/NfcService$NfcServiceHandler;->dispatchTagEndpoint(Lcom/android/nfc/DeviceHost$TagEndpoint;Lcom/android/nfc/NfcService$ReaderModeParams;)V

    goto/16 :goto_0

    .line 3683
    .end local v34    # "playSound":Z
    :cond_9
    const/16 v34, 0x0

    goto :goto_1

    .line 3698
    .restart local v34    # "playSound":Z
    :cond_a
    invoke-interface/range {v43 .. v43}, Lcom/android/nfc/DeviceHost$TagEndpoint;->findAndReadNdef()Landroid/nfc/NdefMessage;

    move-result-object v31

    .line 3701
    .restart local v31    # "ndefMsg":Landroid/nfc/NdefMessage;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mTestMode:Z
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$2400(Lcom/android/nfc/NfcService;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 3702
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    new-instance v49, Landroid/content/Intent;

    const-string v50, "android.nfc.action.TAG_DISCOVERED"

    invoke-direct/range {v49 .. v50}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v48 .. v49}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3706
    :cond_b
    if-eqz v31, :cond_c

    .line 3707
    move-object/from16 v0, v43

    move/from16 v1, v37

    invoke-interface {v0, v1, v12}, Lcom/android/nfc/DeviceHost$TagEndpoint;->startPresenceChecking(ILcom/android/nfc/DeviceHost$TagDisconnectedCallback;)V

    .line 3708
    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/android/nfc/NfcService$NfcServiceHandler;->dispatchTagEndpoint(Lcom/android/nfc/DeviceHost$TagEndpoint;Lcom/android/nfc/NfcService$ReaderModeParams;)V

    goto/16 :goto_0

    .line 3710
    :cond_c
    invoke-interface/range {v43 .. v43}, Lcom/android/nfc/DeviceHost$TagEndpoint;->reconnect()Z

    move-result v48

    if-eqz v48, :cond_d

    .line 3711
    move-object/from16 v0, v43

    move/from16 v1, v37

    invoke-interface {v0, v1, v12}, Lcom/android/nfc/DeviceHost$TagEndpoint;->startPresenceChecking(ILcom/android/nfc/DeviceHost$TagDisconnectedCallback;)V

    .line 3712
    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/android/nfc/NfcService$NfcServiceHandler;->dispatchTagEndpoint(Lcom/android/nfc/DeviceHost$TagEndpoint;Lcom/android/nfc/NfcService$ReaderModeParams;)V

    goto/16 :goto_0

    .line 3714
    :cond_d
    invoke-interface/range {v43 .. v43}, Lcom/android/nfc/DeviceHost$TagEndpoint;->disconnect()Z

    .line 3715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x2

    invoke-virtual/range {v48 .. v49}, Lcom/android/nfc/NfcService;->playSound(I)V

    goto/16 :goto_0

    .line 3720
    .end local v12    # "callback":Lcom/android/nfc/DeviceHost$TagDisconnectedCallback;
    .end local v31    # "ndefMsg":Landroid/nfc/NdefMessage;
    .end local v34    # "playSound":Z
    .end local v37    # "presenceCheckDelay":I
    .end local v38    # "readerParams":Lcom/android/nfc/NfcService$ReaderModeParams;
    .end local v43    # "tag":Lcom/android/nfc/DeviceHost$TagEndpoint;
    :sswitch_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-boolean v0, v0, Lcom/android/nfc/NfcService;->mIsDebugBuild:Z

    move/from16 v48, v0

    if-eqz v48, :cond_e

    .line 3721
    new-instance v7, Landroid/content/Intent;

    const-string v48, "com.android.nfc.action.LLCP_UP"

    move-object/from16 v0, v48

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3722
    .local v7, "actIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3724
    .end local v7    # "actIntent":Landroid/content/Intent;
    :cond_e
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->llcpActivated(Lcom/android/nfc/DeviceHost$NfcDepEndpoint;)Z

    goto/16 :goto_0

    .line 3728
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-boolean v0, v0, Lcom/android/nfc/NfcService;->mIsDebugBuild:Z

    move/from16 v48, v0

    if-eqz v48, :cond_f

    .line 3729
    new-instance v15, Landroid/content/Intent;

    const-string v48, "com.android.nfc.action.LLCP_DOWN"

    move-object/from16 v0, v48

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3730
    .local v15, "deactIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v0, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3732
    .end local v15    # "deactIntent":Landroid/content/Intent;
    :cond_f
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;

    .line 3733
    .local v16, "device":Lcom/android/nfc/DeviceHost$NfcDepEndpoint;
    const/16 v32, 0x0

    .line 3735
    .local v32, "needsDisconnect":Z
    const-string v48, "NfcService"

    const-string v49, "LLCP Link Deactivated message. Restart polling loop."

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v49, v0

    monitor-enter v49

    .line 3738
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mObjectMap:Ljava/util/HashMap;

    move-object/from16 v48, v0

    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->getHandle()I

    move-result v50

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v48

    if-eqz v48, :cond_11

    .line 3740
    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->getMode()I

    move-result v48

    if-nez v48, :cond_13

    .line 3741
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_10

    const-string v48, "NfcService"

    const-string v50, "disconnecting from target"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3742
    :cond_10
    const/16 v32, 0x1

    .line 3747
    :cond_11
    :goto_2
    monitor-exit v49
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 3748
    if-eqz v32, :cond_12

    .line 3749
    invoke-interface/range {v16 .. v16}, Lcom/android/nfc/DeviceHost$NfcDepEndpoint;->disconnect()Z

    .line 3752
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mP2pLinkManager:Lcom/android/nfc/P2pLinkManager;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/android/nfc/P2pLinkManager;->onLlcpDeactivated()V

    goto/16 :goto_0

    .line 3744
    :cond_13
    :try_start_4
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_11

    const-string v48, "NfcService"

    const-string v50, "not disconnecting from initiator"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 3747
    :catchall_2
    move-exception v48

    monitor-exit v49
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v48

    .line 3755
    .end local v16    # "device":Lcom/android/nfc/DeviceHost$NfcDepEndpoint;
    .end local v32    # "needsDisconnect":Z
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mP2pLinkManager:Lcom/android/nfc/P2pLinkManager;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/android/nfc/P2pLinkManager;->onLlcpFirstPacketReceived()V

    goto/16 :goto_0

    .line 3758
    :sswitch_d
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_14

    const-string v48, "NfcService"

    const-string v49, "RF FIELD ACTIVATED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3759
    :cond_14
    new-instance v25, Landroid/content/Intent;

    const-string v48, "com.android.nfc_extras.action.RF_FIELD_ON_DETECTED"

    move-object/from16 v0, v25

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3762
    .local v25, "fieldOnIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3765
    .end local v25    # "fieldOnIntent":Landroid/content/Intent;
    :sswitch_e
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_15

    const-string v48, "NfcService"

    const-string v49, "RF FIELD DEACTIVATED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3766
    :cond_15
    new-instance v24, Landroid/content/Intent;

    const-string v48, "com.android.nfc_extras.action.RF_FIELD_OFF_DETECTED"

    move-object/from16 v0, v24

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3769
    .local v24, "fieldOffIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3772
    .end local v24    # "fieldOffIntent":Landroid/content/Intent;
    :sswitch_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mNfcAdapter:Lcom/android/nfc/NfcService$NfcAdapterService;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/android/nfc/NfcService$NfcAdapterService;->resumePolling()V

    goto/16 :goto_0

    .line 3778
    :sswitch_10
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_16

    const-string v48, "NfcService"

    const-string v49, "APDU Received message"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3779
    :cond_16
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, [B

    move-object/from16 v10, v48

    check-cast v10, [B

    .line 3781
    .local v10, "apduBytes":[B
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 3782
    .local v11, "apduReceivedIntent":Landroid/content/Intent;
    const-string v48, "com.android.nfc_extras.action.APDU_RECEIVED"

    move-object/from16 v0, v48

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3783
    if-eqz v10, :cond_17

    array-length v0, v10

    move/from16 v48, v0

    if-lez v48, :cond_17

    .line 3784
    const-string v48, "com.android.nfc_extras.extra.APDU_BYTES"

    move-object/from16 v0, v48

    invoke-virtual {v11, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3786
    :cond_17
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_18

    const-string v48, "NfcService"

    const-string v49, "Broadcasting com.android.nfc_extras.action.APDU_RECEIVED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3787
    :cond_18
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3793
    .end local v10    # "apduBytes":[B
    .end local v11    # "apduReceivedIntent":Landroid/content/Intent;
    :sswitch_11
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_19

    const-string v48, "NfcService"

    const-string v49, "Card Removal message"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3794
    :cond_19
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    .line 3795
    .local v13, "cardRemovalIntent":Landroid/content/Intent;
    const-string v48, "com.android.nfc_extras.action.EMV_CARD_REMOVAL"

    move-object/from16 v0, v48

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3796
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_1a

    const-string v48, "NfcService"

    const-string v49, "Broadcasting com.android.nfc_extras.action.EMV_CARD_REMOVAL"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3797
    :cond_1a
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3803
    .end local v13    # "cardRemovalIntent":Landroid/content/Intent;
    :sswitch_12
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_1b

    const-string v48, "NfcService"

    const-string v49, "MIFARE access message"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3805
    :cond_1b
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, [B

    move-object/from16 v30, v48

    check-cast v30, [B

    .line 3806
    .local v30, "mifareCmd":[B
    new-instance v28, Landroid/content/Intent;

    invoke-direct/range {v28 .. v28}, Landroid/content/Intent;-><init>()V

    .line 3807
    .local v28, "mifareAccessIntent":Landroid/content/Intent;
    const-string v48, "com.android.nfc_extras.action.MIFARE_ACCESS_DETECTED"

    move-object/from16 v0, v28

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3808
    if-eqz v30, :cond_1d

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v48, v0

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-le v0, v1, :cond_1d

    .line 3809
    const/16 v48, 0x1

    aget-byte v48, v30, v48

    move/from16 v0, v48

    and-int/lit16 v0, v0, 0xff

    move/from16 v29, v0

    .line 3810
    .local v29, "mifareBlock":I
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_1c

    const-string v48, "NfcService"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Mifare Block="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3811
    :cond_1c
    const-string v48, "com.android.nfc_extras.extra.MIFARE_BLOCK"

    move-object/from16 v0, v28

    move-object/from16 v1, v48

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3813
    .end local v29    # "mifareBlock":I
    :cond_1d
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_1e

    const-string v48, "NfcService"

    const-string v49, "Broadcasting com.android.nfc_extras.action.MIFARE_ACCESS_DETECTED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3814
    :cond_1e
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3820
    .end local v28    # "mifareAccessIntent":Landroid/content/Intent;
    .end local v30    # "mifareCmd":[B
    :sswitch_13
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_1f

    const-string v48, "NfcService"

    const-string v49, "SE LISTEN MODE ACTIVATED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3821
    :cond_1f
    new-instance v26, Landroid/content/Intent;

    invoke-direct/range {v26 .. v26}, Landroid/content/Intent;-><init>()V

    .line 3822
    .local v26, "listenModeActivated":Landroid/content/Intent;
    const-string v48, "com.android.nfc_extras.action.SE_LISTEN_ACTIVATED"

    move-object/from16 v0, v26

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3823
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    .line 3826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-boolean v0, v0, Lcom/android/nfc/NfcService;->mIsHceCapable:Z

    move/from16 v48, v0

    if-eqz v48, :cond_0

    .line 3827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mCardEmulationManager:Lcom/android/nfc/cardemulation/CardEmulationManager;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$1100(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/CardEmulationManager;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/android/nfc/cardemulation/CardEmulationManager;->onSecureElementListenActivated()V

    goto/16 :goto_0

    .line 3835
    .end local v26    # "listenModeActivated":Landroid/content/Intent;
    :sswitch_14
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_20

    const-string v48, "NfcService"

    const-string v49, "SE LISTEN MODE DEACTIVATED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3836
    :cond_20
    new-instance v26, Landroid/content/Intent;

    invoke-direct/range {v26 .. v26}, Landroid/content/Intent;-><init>()V

    .line 3837
    .restart local v26    # "listenModeActivated":Landroid/content/Intent;
    const-string v48, "com.android.nfc_extras.action.SE_LISTEN_DEACTIVATED"

    move-object/from16 v0, v26

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3838
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    .line 3841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-boolean v0, v0, Lcom/android/nfc/NfcService;->mIsHceCapable:Z

    move/from16 v48, v0

    if-eqz v48, :cond_0

    .line 3842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->mCardEmulationManager:Lcom/android/nfc/cardemulation/CardEmulationManager;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$1100(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/CardEmulationManager;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/android/nfc/cardemulation/CardEmulationManager;->onSecureElementListenDeacitvaed()V

    goto/16 :goto_0

    .line 3850
    .end local v26    # "listenModeActivated":Landroid/content/Intent;
    :sswitch_15
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_21

    const-string v48, "NfcService"

    const-string v49, "Card Emulation AID_SELECTED message"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3851
    :cond_21
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, [B

    move-object/from16 v8, v48

    check-cast v8, [B

    .line 3853
    .local v8, "aid":[B
    new-instance v9, Landroid/content/Intent;

    const-string v48, "com.android.nfc_extras.action.AID_SELECTED"

    move-object/from16 v0, v48

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3854
    .local v9, "aidIntent":Landroid/content/Intent;
    const-string v48, "com.android.nfc_extras.extra.AID"

    move-object/from16 v0, v48

    invoke-virtual {v9, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3855
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_22

    const-string v48, "NfcService"

    const-string v49, "Broadcasting com.android.nfc_extras.action.AID_SELECTED"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3856
    :cond_22
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3862
    .end local v8    # "aid":[B
    .end local v9    # "aidIntent":Landroid/content/Intent;
    :sswitch_16
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_23

    const-string v48, "NfcService"

    const-string v49, "SE EVENT CONNECTIVITY"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3863
    :cond_23
    new-instance v21, Landroid/content/Intent;

    invoke-direct/range {v21 .. v21}, Landroid/content/Intent;-><init>()V

    .line 3864
    .local v21, "eventConnectivityIntent":Landroid/content/Intent;
    const-string v48, "com.nxp.action.CONNECTIVITY_EVENT_DETECTED"

    move-object/from16 v0, v21

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3865
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    const-string v49, "android.permission.NFC"

    move-object/from16 v0, v48

    move-object/from16 v1, v21

    move-object/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3871
    .end local v21    # "eventConnectivityIntent":Landroid/content/Intent;
    :sswitch_17
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_24

    const-string v48, "NfcService"

    const-string v49, "Card Emulation message"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3876
    :cond_24
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v45, v0

    check-cast v45, Landroid/util/Pair;

    .line 3877
    .local v45, "transactionInfo":Landroid/util/Pair;, "Landroid/util/Pair<[BLandroid/util/Pair;>;"
    move-object/from16 v0, v45

    iget-object v14, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Landroid/util/Pair;

    .line 3878
    .local v14, "dataSrcInfo":Landroid/util/Pair;, "Landroid/util/Pair<[BLjava/lang/Integer;>;"
    move-object/from16 v0, v45

    iget-object v8, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, [B

    .line 3879
    .restart local v8    # "aid":[B
    iget-object v0, v14, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v33, v0

    check-cast v33, [B

    .line 3880
    .local v33, "param":[B
    iget-object v0, v14, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    .line 3884
    .local v22, "eventSrc":Ljava/lang/Integer;
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v8, v1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->makeAidIntent([B[B)Landroid/content/Intent;

    move-result-object v9

    .line 3887
    .restart local v9    # "aidIntent":Landroid/content/Intent;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v48

    const/16 v49, 0x2

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_28

    .line 3889
    const-string v48, "GOOGLE"

    # getter for: Lcom/android/nfc/NfcService;->mSecureEventType:Ljava/lang/String;
    invoke-static {}, Lcom/android/nfc/NfcService;->access$2200()Ljava/lang/String;

    move-result-object v49

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-nez v48, :cond_25

    const-string v48, "ISIS"

    # getter for: Lcom/android/nfc/NfcService;->mSecureEventType:Ljava/lang/String;
    invoke-static {}, Lcom/android/nfc/NfcService;->access$2200()Ljava/lang/String;

    move-result-object v49

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_27

    .line 3890
    :cond_25
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_26

    const-string v48, "NfcService"

    const-string v49, "Broadcasting EVENT"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3891
    :cond_26
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3896
    :cond_27
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    move-object/from16 v0, v48

    move/from16 v1, v49

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v8, v2}, Lcom/android/nfc/HciEventControl;->checkAndSendIntent(I[B[B)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 3897
    :catch_0
    move-exception v19

    .line 3898
    .local v19, "e":Ljava/lang/NullPointerException;
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_0

    const-string v48, "NfcService"

    const-string v49, "mHciEventControl is null"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3904
    .end local v19    # "e":Ljava/lang/NullPointerException;
    :cond_28
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_2a

    .line 3905
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_29

    const-string v48, "NfcService"

    const-string v49, "Broadcasting EVENT"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3906
    :cond_29
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendSeBroadcast(Landroid/content/Intent;)V

    .line 3910
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    move-object/from16 v48, v0

    const/16 v49, 0x1

    move-object/from16 v0, v48

    move/from16 v1, v49

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v8, v2}, Lcom/android/nfc/HciEventControl;->checkAndSendIntent(I[B[B)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 3911
    :catch_1
    move-exception v19

    .line 3912
    .restart local v19    # "e":Ljava/lang/NullPointerException;
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_0

    const-string v48, "NfcService"

    const-string v49, "mHciEventControl is null"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3916
    .end local v19    # "e":Ljava/lang/NullPointerException;
    :cond_2a
    sget-boolean v48, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v48, :cond_0

    const-string v48, "NfcService"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "unexpected src=0x"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v50

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3925
    .end local v8    # "aid":[B
    .end local v9    # "aidIntent":Landroid/content/Intent;
    .end local v14    # "dataSrcInfo":Landroid/util/Pair;, "Landroid/util/Pair<[BLjava/lang/Integer;>;"
    .end local v22    # "eventSrc":Ljava/lang/Integer;
    .end local v33    # "param":[B
    .end local v45    # "transactionInfo":Landroid/util/Pair;, "Landroid/util/Pair<[BLandroid/util/Pair;>;"
    :sswitch_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x1

    # invokes: Lcom/android/nfc/NfcService;->showIcon(Z)V
    invoke-static/range {v48 .. v49}, Lcom/android/nfc/NfcService;->access$2500(Lcom/android/nfc/NfcService;Z)V

    goto/16 :goto_0

    .line 3930
    :sswitch_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    # invokes: Lcom/android/nfc/NfcService;->showIcon(Z)V
    invoke-static/range {v48 .. v49}, Lcom/android/nfc/NfcService;->access$2500(Lcom/android/nfc/NfcService;Z)V

    goto/16 :goto_0

    .line 3937
    :sswitch_1a
    const-string v48, "NfcService"

    const-string v49, "NfcServiceHandler - MSG_CHN_ENABLE_POPUP"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    const-string v49, "statusbar"

    invoke-virtual/range {v48 .. v49}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/app/StatusBarManager;

    .line 3940
    .local v27, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-eqz v27, :cond_2b

    .line 3941
    invoke-virtual/range {v27 .. v27}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 3944
    :cond_2b
    new-instance v5, Landroid/content/Intent;

    const-string v48, "com.android.nfc.NfcChnEnablePopup"

    move-object/from16 v0, v48

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3945
    .local v5, "PopupIntent":Landroid/content/Intent;
    const v48, 0x10008000

    move/from16 v0, v48

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3951
    .end local v5    # "PopupIntent":Landroid/content/Intent;
    .end local v27    # "mStatusBarManager":Landroid/app/StatusBarManager;
    :sswitch_1b
    const-string v48, "NfcService"

    const-string v49, "NfcServiceHandler - MSG_CHN_ENABLE_DIRECT"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3952
    new-instance v20, Lcom/android/nfc/NfcService$EnableDisableTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/android/nfc/NfcService$EnableDisableTask;-><init>(Lcom/android/nfc/NfcService;)V

    .line 3953
    .local v20, "enableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    const/16 v48, 0x1

    move/from16 v0, v48

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    const/16 v50, 0x65

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    aput-object v50, v48, v49

    move-object/from16 v0, v20

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$EnableDisableTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 3957
    .end local v20    # "enableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    :sswitch_1c
    const-string v48, "NfcService"

    const-string v49, "NfcServiceHandler - MSG_CHN_ENABLE_CANCEL"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3958
    new-instance v17, Lcom/android/nfc/NfcService$EnableDisableTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/android/nfc/NfcService$EnableDisableTask;-><init>(Lcom/android/nfc/NfcService;)V

    .line 3959
    .local v17, "disableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    const/16 v48, 0x1

    move/from16 v0, v48

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    const/16 v50, 0x66

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    aput-object v50, v48, v49

    move-object/from16 v0, v17

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$EnableDisableTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 3965
    .end local v17    # "disableTask":Lcom/android/nfc/NfcService$EnableDisableTask;
    :sswitch_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    const-string v49, "statusbar"

    invoke-virtual/range {v48 .. v49}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Landroid/app/StatusBarManager;

    .line 3967
    .local v41, "statusBarManager":Landroid/app/StatusBarManager;
    if-eqz v41, :cond_2c

    .line 3968
    invoke-virtual/range {v41 .. v41}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 3970
    :cond_2c
    new-instance v35, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    const-class v49, Lcom/android/nfc/NfcGsmaPopup;

    move-object/from16 v0, v35

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3971
    .local v35, "popupIntent":Landroid/content/Intent;
    const v48, 0x10008000

    move-object/from16 v0, v35

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3978
    .end local v35    # "popupIntent":Landroid/content/Intent;
    .end local v41    # "statusBarManager":Landroid/app/StatusBarManager;
    :sswitch_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$2600(Lcom/android/nfc/NfcService;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    move-result-object v48

    if-eqz v48, :cond_2d

    .line 3979
    new-instance v44, Lcom/android/nfc/NfcService$EnableDisableTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v49, v0

    # getter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static/range {v49 .. v49}, Lcom/android/nfc/NfcService;->access$2600(Lcom/android/nfc/NfcService;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    move-result-object v49

    move-object/from16 v0, v44

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2}, Lcom/android/nfc/NfcService$EnableDisableTask;-><init>(Lcom/android/nfc/NfcService;Lcom/gsma/services/nfc/INfcControllerCallbacks;)V

    .line 3980
    .local v44, "task":Lcom/android/nfc/NfcService$EnableDisableTask;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    # setter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static/range {v48 .. v49}, Lcom/android/nfc/NfcService;->access$2602(Lcom/android/nfc/NfcService;Lcom/gsma/services/nfc/INfcControllerCallbacks;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    .line 3984
    :goto_3
    const/16 v48, 0x1

    move/from16 v0, v48

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    const/16 v50, 0x1

    invoke-static/range {v50 .. v50}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    aput-object v50, v48, v49

    move-object/from16 v0, v44

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/android/nfc/NfcService$EnableDisableTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 3986
    :try_start_7
    invoke-virtual/range {v44 .. v44}, Lcom/android/nfc/NfcService$EnableDisableTask;->get()Ljava/lang/Object;

    .line 3987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x1

    invoke-virtual/range {v48 .. v49}, Lcom/android/nfc/NfcService;->saveNfcOnSetting(Z)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 3988
    :catch_2
    move-exception v19

    .line 3989
    .local v19, "e":Ljava/lang/Exception;
    const-string v48, "NfcService"

    const-string v49, "failed to enable"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3982
    .end local v19    # "e":Ljava/lang/Exception;
    .end local v44    # "task":Lcom/android/nfc/NfcService$EnableDisableTask;
    :cond_2d
    new-instance v44, Lcom/android/nfc/NfcService$EnableDisableTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/android/nfc/NfcService$EnableDisableTask;-><init>(Lcom/android/nfc/NfcService;)V

    .restart local v44    # "task":Lcom/android/nfc/NfcService$EnableDisableTask;
    goto :goto_3

    .line 3994
    .end local v44    # "task":Lcom/android/nfc/NfcService$EnableDisableTask;
    :sswitch_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$2600(Lcom/android/nfc/NfcService;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    move-result-object v48

    if-eqz v48, :cond_0

    .line 3996
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    # getter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static/range {v48 .. v48}, Lcom/android/nfc/NfcService;->access$2600(Lcom/android/nfc/NfcService;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    move-result-object v48

    const/16 v49, 0x0

    invoke-interface/range {v48 .. v49}, Lcom/gsma/services/nfc/INfcControllerCallbacks;->onEnableNfcController(Z)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_3

    .line 4000
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    const/16 v49, 0x0

    # setter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static/range {v48 .. v49}, Lcom/android/nfc/NfcService;->access$2602(Lcom/android/nfc/NfcService;Lcom/gsma/services/nfc/INfcControllerCallbacks;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    goto/16 :goto_0

    .line 3997
    :catch_3
    move-exception v19

    .line 3998
    .local v19, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v19 .. v19}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 4007
    .end local v19    # "e":Landroid/os/RemoteException;
    :sswitch_20
    const-string v48, "NfcService"

    const-string v49, "NfcServiceHandler - MSG_KOR_TAGCONNECTION"

    invoke-static/range {v48 .. v49}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4009
    new-instance v6, Landroid/content/Intent;

    const-string v48, "com.android.nfc.NfcKorTagConnectionPopup"

    move-object/from16 v0, v48

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4010
    .local v6, "TagConnectionIntent":Landroid/content/Intent;
    const v48, 0x10008000

    move/from16 v0, v48

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4012
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/android/nfc/NfcService;->mContext:Landroid/content/Context;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 3551
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_9
        0x1 -> :sswitch_a
        0x2 -> :sswitch_b
        0x3 -> :sswitch_8
        0x4 -> :sswitch_c
        0x5 -> :sswitch_0
        0x6 -> :sswitch_1
        0x7 -> :sswitch_5
        0x8 -> :sswitch_4
        0x9 -> :sswitch_d
        0xa -> :sswitch_e
        0xb -> :sswitch_f
        0xc -> :sswitch_7
        0xd -> :sswitch_6
        0x66 -> :sswitch_10
        0x67 -> :sswitch_11
        0x68 -> :sswitch_12
        0x69 -> :sswitch_13
        0x6a -> :sswitch_14
        0x6b -> :sswitch_15
        0x6c -> :sswitch_16
        0x6d -> :sswitch_17
        0xc9 -> :sswitch_1a
        0xca -> :sswitch_1b
        0xcb -> :sswitch_1c
        0xd3 -> :sswitch_1d
        0xd4 -> :sswitch_1e
        0xd5 -> :sswitch_1f
        0x12c -> :sswitch_20
        0x1f5 -> :sswitch_18
        0x1f6 -> :sswitch_19
        0x1f7 -> :sswitch_2
        0x1f8 -> :sswitch_3
    .end sparse-switch
.end method

.method public sendSeBroadcast(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4108
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 4110
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mNfcDispatcher:Lcom/android/nfc/NfcDispatcher;
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$1300(Lcom/android/nfc/NfcService;)Lcom/android/nfc/NfcDispatcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/nfc/NfcDispatcher;->resumeAppSwitches()V

    .line 4113
    monitor-enter p0

    .line 4114
    :try_start_0
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcServiceHandler;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mGpacBasedSeEventList:Ljava/util/List;
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$2700(Lcom/android/nfc/NfcService;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4115
    invoke-direct {p0, p1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendGpacRuleBasedBroadcast(Landroid/content/Intent;)V

    .line 4120
    :goto_0
    monitor-exit p0

    .line 4122
    return-void

    .line 4118
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/nfc/NfcService$NfcServiceHandler;->sendNfcEeAccessProtectedBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 4120
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
