.class public Lcom/android/nfc/NfcChnEnablePopup;
.super Landroid/app/Activity;
.source "NfcChnEnablePopup.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NfcChnEnablePopup"


# instance fields
.field private mEventDecision:Z

.field private mNfcChnEnablePopup:Landroid/app/AlertDialog;

.field private mPopup:Landroid/app/AlertDialog$Builder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    .line 37
    iput-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mEventDecision:Z

    return-void
.end method

.method static synthetic access$002(Lcom/android/nfc/NfcChnEnablePopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/nfc/NfcChnEnablePopup;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/android/nfc/NfcChnEnablePopup;->mEventDecision:Z

    return p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const-string v0, "NfcChnEnablePopup"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    .line 48
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 49
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f07002f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 51
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f07000d

    new-instance v2, Lcom/android/nfc/NfcChnEnablePopup$1;

    invoke-direct {v2, p0}, Lcom/android/nfc/NfcChnEnablePopup$1;-><init>(Lcom/android/nfc/NfcChnEnablePopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 60
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f07002c

    new-instance v2, Lcom/android/nfc/NfcChnEnablePopup$2;

    invoke-direct {v2, p0}, Lcom/android/nfc/NfcChnEnablePopup$2;-><init>(Lcom/android/nfc/NfcChnEnablePopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 69
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mPopup:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    .line 70
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 71
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/nfc/NfcChnEnablePopup$3;

    invoke-direct {v1, p0}, Lcom/android/nfc/NfcChnEnablePopup$3;-><init>(Lcom/android/nfc/NfcChnEnablePopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 84
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 86
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 90
    const-string v0, "NfcChnEnablePopup"

    const-string v1, " onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    .line 96
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 97
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 101
    const-string v0, "NfcChnEnablePopup"

    const-string v1, " onPause"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mNfcChnEnablePopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 103
    iget-boolean v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mEventDecision:Z

    if-nez v0, :cond_0

    .line 104
    const-string v0, "NfcChnEnablePopup"

    const-string v1, " onPause- mEventDecision yet close popup"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/nfc/NfcChnEnablePopup;->mEventDecision:Z

    .line 106
    invoke-static {}, Lcom/android/nfc/NfcService;->getInstance()Lcom/android/nfc/NfcService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/nfc/NfcService;->sendChnEnableCancel()V

    .line 107
    invoke-virtual {p0}, Lcom/android/nfc/NfcChnEnablePopup;->finish()V

    .line 110
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 111
    return-void
.end method
