.class public Lcom/android/nfc/DeviceHostImplement;
.super Ljava/lang/Object;
.source "DeviceHostImplement.java"

# interfaces
.implements Lcom/android/nfc/DeviceHost;


# static fields
.field private static final DEFALUT_ARRAY:[I

.field private static final DEFALUT_BOOLEAN:Z = true

.field private static final DEFALUT_INT:I = -0x3e7

.field private static final DEFALUT_STRING:Ljava/lang/String; = "String"

.field static final DEFAULT_BYTEARRARY:[B


# instance fields
.field private FAKE_OBJECT:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [I

    sput-object v0, Lcom/android/nfc/DeviceHostImplement;->DEFALUT_ARRAY:[I

    .line 38
    const/16 v0, 0x400

    new-array v0, v0, [B

    sput-object v0, Lcom/android/nfc/DeviceHostImplement;->DEFAULT_BYTEARRARY:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/nfc/DeviceHostImplement;->FAKE_OBJECT:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public GetDefaultSE()I
    .locals 1

    .prologue
    .line 108
    const/16 v0, -0x3e7

    return v0
.end method

.method public SWPSelfTest(I)I
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 118
    const/16 v0, -0x3e7

    return v0
.end method

.method public canMakeReadOnly(I)Z
    .locals 1
    .param p1, "technology"    # I

    .prologue
    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method public checkFirmware()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public checkFirmware(Z)Z
    .locals 1
    .param p1, "dummy"    # Z

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public clearAidTable()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method public clearRoutingEntry(I)Z
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method public commitRouting()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public createLlcpConnectionlessSocket(ILjava/lang/String;)Lcom/android/nfc/DeviceHost$LlcpConnectionlessSocket;
    .locals 1
    .param p1, "nSap"    # I
    .param p2, "sn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/nfc/LlcpException;
        }
    .end annotation

    .prologue
    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method public createLlcpServerSocket(ILjava/lang/String;III)Lcom/android/nfc/DeviceHost$LlcpServerSocket;
    .locals 1
    .param p1, "nSap"    # I
    .param p2, "sn"    # Ljava/lang/String;
    .param p3, "miu"    # I
    .param p4, "rw"    # I
    .param p5, "linearBufferLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/nfc/LlcpException;
        }
    .end annotation

    .prologue
    .line 174
    const/4 v0, 0x0

    return-object v0
.end method

.method public createLlcpSocket(IIII)Lcom/android/nfc/DeviceHost$LlcpSocket;
    .locals 1
    .param p1, "sap"    # I
    .param p2, "miu"    # I
    .param p3, "rw"    # I
    .param p4, "linearBufferLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/nfc/LlcpException;
        }
    .end annotation

    .prologue
    .line 179
    const/4 v0, 0x0

    return-object v0
.end method

.method public deinitialize()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public disableDiscovery()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public disableScreenOffSuspend()Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public doAbort()V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public doActivateLlcp()Z
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x1

    return v0
.end method

.method public doCheckLlcp()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public doDeselectSecureElement(I)V
    .locals 0
    .param p1, "seID"    # I

    .prologue
    .line 143
    return-void
.end method

.method public doGetSecureElementList()[I
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/android/nfc/DeviceHostImplement;->DEFALUT_ARRAY:[I

    return-object v0
.end method

.method public doGetSecureElementTechList()I
    .locals 1

    .prologue
    .line 124
    const/16 v0, -0x3e7

    return v0
.end method

.method public doPrbsOff()V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public doPrbsOn(II)V
    .locals 0
    .param p1, "tech"    # I
    .param p2, "rate"    # I

    .prologue
    .line 112
    return-void
.end method

.method public doSelectSecureElement(I)V
    .locals 0
    .param p1, "seID"    # I

    .prologue
    .line 140
    return-void
.end method

.method public doSetSEPowerOffState(IZ)V
    .locals 0
    .param p1, "seID"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 131
    return-void
.end method

.method public doSetScreenOrPowerState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 152
    return-void
.end method

.method public doSetSecureElementListenTechMask(I)V
    .locals 0
    .param p1, "tech_mask"    # I

    .prologue
    .line 127
    return-void
.end method

.method public doTestSwp()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    return v0
.end method

.method public dump()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    const-string v0, "String"

    return-object v0
.end method

.method public enableDiscovery(Lcom/android/nfc/NfcDiscoveryParameters;Z)V
    .locals 0
    .param p1, "params"    # Lcom/android/nfc/NfcDiscoveryParameters;
    .param p2, "restart"    # Z

    .prologue
    .line 59
    return-void
.end method

.method public enableScreenOffSuspend()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method public getAidTableSize()I
    .locals 1

    .prologue
    .line 104
    const/16 v0, 0x96

    return v0
.end method

.method public getChipVer()I
    .locals 1

    .prologue
    .line 146
    const/16 v0, -0x3e7

    return v0
.end method

.method public getDefaultLlcpMiu()I
    .locals 1

    .prologue
    .line 215
    const/16 v0, -0x3e7

    return v0
.end method

.method public getDefaultLlcpRwSize()I
    .locals 1

    .prologue
    .line 218
    const/16 v0, -0x3e7

    return v0
.end method

.method public getEseHandleFromGenericId(I)S
    .locals 1
    .param p1, "eseId"    # I

    .prologue
    .line 164
    const/4 v0, -0x1

    return v0
.end method

.method public getExtendedLengthApdusSupported()Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x1

    return v0
.end method

.method public getFWVersion()I
    .locals 1

    .prologue
    .line 121
    const/16 v0, -0x3e7

    return v0
.end method

.method public getGenericEseId(S)I
    .locals 1
    .param p1, "handle"    # S

    .prologue
    .line 161
    const/16 v0, -0x3e7

    return v0
.end method

.method public getMaxTransceiveLength(I)I
    .locals 1
    .param p1, "technology"    # I

    .prologue
    .line 203
    const/16 v0, -0x3e7

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "String"

    return-object v0
.end method

.method public getRoutingTableFreeSpace()I
    .locals 1

    .prologue
    .line 230
    const/16 v0, -0x3e7

    return v0
.end method

.method public getTimeout(I)I
    .locals 1
    .param p1, "technology"    # I

    .prologue
    .line 194
    const/16 v0, -0x3e7

    return v0
.end method

.method public initialize()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method public onPpseRouted(I)Z
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method public reRouteAid([BIZZ)Z
    .locals 1
    .param p1, "aid"    # [B
    .param p2, "route"    # I
    .param p3, "isStopDiscovery"    # Z
    .param p4, "isStartDiscovery"    # Z

    .prologue
    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method public removeHceOffHostAidRoute([B)V
    .locals 0
    .param p1, "aid"    # [B

    .prologue
    .line 86
    return-void
.end method

.method public resetTimeouts()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public routeAid([BII)Z
    .locals 1
    .param p1, "aid"    # [B
    .param p2, "routej"    # I
    .param p3, "powerState"    # I

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public sendRawFrame([B)Z
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public setColdBootFlag(Z)V
    .locals 0
    .param p1, "isColdBoot"    # Z

    .prologue
    .line 233
    return-void
.end method

.method public setDefaultProtoRoute(III)V
    .locals 0
    .param p1, "seID"    # I
    .param p2, "proto_switchon"    # I
    .param p3, "proto_switchoff"    # I

    .prologue
    .line 137
    return-void
.end method

.method public setDefaultRoute(III)Z
    .locals 1
    .param p1, "switch_on"    # I
    .param p2, "switch_off"    # I
    .param p3, "battery_off"    # I

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method public setDefaultRouteDestinations(II)V
    .locals 0
    .param p1, "defaultIsoDepRoute"    # I
    .param p2, "defaultOffHostRoute"    # I

    .prologue
    .line 74
    return-void
.end method

.method public setDefaultTechRoute(III)V
    .locals 0
    .param p1, "seID"    # I
    .param p2, "tech_switchon"    # I
    .param p3, "tech_switchoff"    # I

    .prologue
    .line 134
    return-void
.end method

.method public setHceOffHostAidRoute([BZZZIZZZ)V
    .locals 0
    .param p1, "aid"    # [B
    .param p2, "screenOn"    # Z
    .param p3, "screenOff"    # Z
    .param p4, "screenLock"    # Z
    .param p5, "route"    # I
    .param p6, "switchOn"    # Z
    .param p7, "switchOff"    # Z
    .param p8, "batteryOff"    # Z

    .prologue
    .line 83
    return-void
.end method

.method public setP2pInitiatorModes(I)V
    .locals 0
    .param p1, "modes"    # I

    .prologue
    .line 206
    return-void
.end method

.method public setP2pTargetModes(I)V
    .locals 0
    .param p1, "modes"    # I

    .prologue
    .line 209
    return-void
.end method

.method public setPowerMode(I)Z
    .locals 1
    .param p1, "powerState"    # I

    .prologue
    .line 236
    const/4 v0, 0x1

    return v0
.end method

.method public setRoutingEntry(IIII)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "value"    # I
    .param p3, "route"    # I
    .param p4, "power"    # I

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method public setSeListener(Lcom/android/nfc/DeviceHost$SecureElementListener;)V
    .locals 0
    .param p1, "seListener"    # Lcom/android/nfc/DeviceHost$SecureElementListener;

    .prologue
    .line 41
    return-void
.end method

.method public setStaticRouteByProto(IZZZIZZZ)V
    .locals 0
    .param p1, "protocol"    # I
    .param p2, "screenOn"    # Z
    .param p3, "screenOff"    # Z
    .param p4, "screenLock"    # Z
    .param p5, "route"    # I
    .param p6, "switchOn"    # Z
    .param p7, "switchOff"    # Z
    .param p8, "batteryOff"    # Z

    .prologue
    .line 80
    return-void
.end method

.method public setStaticRouteByTech(IZZZIZZZ)V
    .locals 0
    .param p1, "technology"    # I
    .param p2, "screenOn"    # Z
    .param p3, "screenOff"    # Z
    .param p4, "screenLock"    # Z
    .param p5, "route"    # I
    .param p6, "switchOn"    # Z
    .param p7, "switchOff"    # Z
    .param p8, "batteryOff"    # Z

    .prologue
    .line 77
    return-void
.end method

.method public setTimeout(II)Z
    .locals 1
    .param p1, "technology"    # I
    .param p2, "timeout"    # I

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public startCoverAuth()[B
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lcom/android/nfc/DeviceHostImplement;->DEFAULT_BYTEARRARY:[B

    return-object v0
.end method

.method public stopCoverAuth()Z
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x1

    return v0
.end method

.method public transceiveAuthData([B)[B
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 246
    sget-object v0, Lcom/android/nfc/DeviceHostImplement;->DEFAULT_BYTEARRARY:[B

    return-object v0
.end method

.method public unrouteAid([B)Z
    .locals 1
    .param p1, "aid"    # [B

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method
