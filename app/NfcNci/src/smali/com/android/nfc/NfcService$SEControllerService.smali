.class final Lcom/android/nfc/NfcService$SEControllerService;
.super Lcom/gsma/services/nfc/ISEController$Stub;
.source "NfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/NfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "SEControllerService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcService;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcService;)V
    .locals 0

    .prologue
    .line 4958
    iput-object p1, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {p0}, Lcom/gsma/services/nfc/ISEController$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public enableMultiEvt_transactionReception(Ljava/lang/String;Z)I
    .locals 5
    .param p1, "SEName"    # Ljava/lang/String;
    .param p2, "enable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x3

    .line 5081
    iget-object v3, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v3}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 5082
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "NfcService"

    const-string v3, "Gsma Apis are not Supported in this project"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5083
    :cond_0
    const/4 v2, -0x1

    .line 5105
    :cond_1
    :goto_0
    return v2

    .line 5086
    :cond_2
    # getter for: Lcom/android/nfc/NfcService;->mHideTerminal:Ljava/lang/String;
    invoke-static {}, Lcom/android/nfc/NfcService;->access$3500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ESE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "eSE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 5087
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "eSE is not available on this device"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5090
    :cond_3
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_4

    const-string v3, "NfcService"

    const-string v4, "enableMultiEvt_transactionReception"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5092
    :cond_4
    :try_start_0
    iget-object v3, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v3, v3, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    invoke-virtual {v3, p1}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-eqz v3, :cond_1

    .line 5103
    iget-object v2, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v2, v2, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    invoke-virtual {v2, p1, p2}, Lcom/android/nfc/HciEventControl;->enableMultiEvt_transactionReception(Ljava/lang/String;Z)V

    .line 5105
    const/4 v2, 0x0

    goto :goto_0

    .line 5095
    :catch_0
    move-exception v0

    .line 5096
    .local v0, "e":Landroid/os/RemoteException;
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "Checking CDF failed."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5098
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 5099
    .local v1, "e2":Ljava/lang/NullPointerException;
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "mHciEventControl is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getActiveSecureElement()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 4961
    iget-object v1, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4962
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcService"

    const-string v2, "Gsma Apis are not Supported in this project"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4975
    :cond_0
    :goto_0
    return-object v0

    .line 4965
    :cond_1
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_2

    const-string v1, "NfcService"

    const-string v2, "getActiveSecureElement"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4966
    :cond_2
    iget-object v1, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 4967
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcService"

    const-string v2, "Nfc was not enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4975
    :cond_3
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public setActiveSecureElement(Ljava/lang/String;)I
    .locals 7
    .param p1, "SEName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v3, -0x3

    .line 4980
    iget-object v4, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v4}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 4981
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "NfcService"

    const-string v4, "Gsma Apis are not Supported in this project"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5005
    :cond_0
    :goto_0
    return v2

    .line 4985
    :cond_1
    # getter for: Lcom/android/nfc/NfcService;->mHideTerminal:Ljava/lang/String;
    invoke-static {}, Lcom/android/nfc/NfcService;->access$3500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ESE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "eSE"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4986
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_2

    const-string v2, "NfcService"

    const-string v4, "eSE is not available on this device"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v2, v3

    .line 4987
    goto :goto_0

    .line 4989
    :cond_3
    sget-boolean v4, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v4, :cond_4

    const-string v4, "NfcService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setActiveSecureElement "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4991
    :cond_4
    :try_start_0
    iget-object v4, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v4, v4, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    invoke-virtual {v4}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-nez v4, :cond_7

    move v2, v3

    .line 4992
    goto :goto_0

    .line 4994
    :catch_0
    move-exception v0

    .line 4995
    .local v0, "e":Landroid/os/RemoteException;
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_5

    const-string v2, "NfcService"

    const-string v4, "Checking CDF failed."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move v2, v3

    .line 4996
    goto :goto_0

    .line 4997
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 4998
    .local v1, "e2":Ljava/lang/NullPointerException;
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_6

    const-string v2, "NfcService"

    const-string v4, "Checking CDF failed."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v2, v3

    .line 4999
    goto :goto_0

    .line 5004
    .end local v1    # "e2":Ljava/lang/NullPointerException;
    :cond_7
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "NfcService"

    const-string v4, "API not supported."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;)I
    .locals 11
    .param p1, "intent"    # Landroid/app/PendingIntent;
    .param p2, "filters"    # [Landroid/content/IntentFilter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v6, -0x3

    .line 5036
    iget-object v8, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v8}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 5037
    sget-boolean v6, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v6, :cond_0

    const-string v6, "NfcService"

    const-string v7, "Gsma Apis are not Supported in this project"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5038
    :cond_0
    const/4 v6, -0x1

    .line 5076
    :cond_1
    :goto_0
    return v6

    .line 5040
    :cond_2
    const-string v8, "NfcService"

    const-string v9, "setForegroundDispatch"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5041
    iget-object v8, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v8, v8, Lcom/android/nfc/NfcService;->mSEControllerService:Lcom/android/nfc/NfcService$SEControllerService;

    if-eqz v8, :cond_1

    .line 5045
    :try_start_0
    iget-object v8, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v8, v8, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    invoke-virtual {p0}, Lcom/android/nfc/NfcService$SEControllerService;->getActiveSecureElement()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    if-eqz v8, :cond_1

    .line 5056
    if-nez p1, :cond_3

    if-nez p2, :cond_3

    .line 5057
    iget-object v6, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v6, v6, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    invoke-virtual {v6, v10, v10}, Lcom/android/nfc/HciEventControl;->setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;)V

    move v6, v7

    .line 5058
    goto :goto_0

    .line 5048
    :catch_0
    move-exception v1

    .line 5049
    .local v1, "e":Landroid/os/RemoteException;
    sget-boolean v7, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v7, :cond_1

    const-string v7, "NfcService"

    const-string v8, "Checking CDF failed."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5051
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 5052
    .local v2, "e2":Ljava/lang/NullPointerException;
    sget-boolean v7, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v7, :cond_1

    const-string v7, "NfcService"

    const-string v8, "mHciEventControl is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5062
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :cond_3
    if-eqz p2, :cond_4

    .line 5063
    array-length v6, p2

    if-nez v6, :cond_5

    .line 5064
    const/4 p2, 0x0

    .line 5074
    :cond_4
    iget-object v6, p0, Lcom/android/nfc/NfcService$SEControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v6, v6, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    invoke-virtual {v6, p1, p2}, Lcom/android/nfc/HciEventControl;->setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;)V

    move v6, v7

    .line 5076
    goto :goto_0

    .line 5066
    :cond_5
    move-object v0, p2

    .local v0, "arr$":[Landroid/content/IntentFilter;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v3, v0, v4

    .line 5067
    .local v3, "filter":Landroid/content/IntentFilter;
    if-nez v3, :cond_6

    .line 5068
    const/4 v6, -0x4

    goto :goto_0

    .line 5066
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
