.class final Lcom/android/nfc/NfcService$NfcControllerService;
.super Lcom/gsma/services/nfc/INfcController$Stub;
.source "NfcService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/nfc/NfcService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "NfcControllerService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/nfc/NfcService;


# direct methods
.method constructor <init>(Lcom/android/nfc/NfcService;)V
    .locals 0

    .prologue
    .line 5109
    iput-object p1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {p0}, Lcom/gsma/services/nfc/INfcController$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public defineOffHostService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;
    .locals 2
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "SEName"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "ClassName"    # Ljava/lang/String;

    .prologue
    .line 5229
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SIM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "eSE"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SD"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5234
    :cond_0
    const/4 v0, 0x0

    .line 5236
    :goto_0
    return-object v0

    .line 5235
    :cond_1
    new-instance v0, Lcom/gsma/services/nfc/OffHostService;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/gsma/services/nfc/OffHostService;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5236
    .local v0, "service":Lcom/gsma/services/nfc/OffHostService;
    goto :goto_0
.end method

.method public deleteOffHostService(Ljava/lang/String;Lcom/gsma/services/nfc/OffHostService;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "service"    # Lcom/gsma/services/nfc/OffHostService;

    .prologue
    .line 5241
    if-nez p2, :cond_0

    .line 5242
    const/4 v0, -0x4

    .line 5249
    :goto_0
    return v0

    .line 5244
    :cond_0
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/nfc/cardemulation/DynamicService;->removeOffHostService(Lcom/gsma/services/nfc/OffHostService;)Z

    .line 5245
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/nfc/cardemulation/DynamicService;->commit()V

    .line 5246
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-boolean v0, v0, Lcom/android/nfc/NfcService;->mIsHceCapable:Z

    if-eqz v0, :cond_1

    .line 5247
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/nfc/cardemulation/DynamicService;->invalidateCache(I)V

    .line 5249
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disableCardEmulationMode(Lcom/gsma/services/nfc/INfcControllerCallbacks;)I
    .locals 6
    .param p1, "cb"    # Lcom/gsma/services/nfc/INfcControllerCallbacks;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x3

    .line 5198
    iget-object v4, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v4}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 5199
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "NfcService"

    const-string v3, "Gsma Apis are not Supported in this project"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5200
    :cond_0
    const/4 v2, -0x1

    .line 5224
    :cond_1
    :goto_0
    return v2

    .line 5202
    :cond_2
    sget-boolean v4, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v4, :cond_3

    const-string v4, "NfcService"

    const-string v5, "disableCardEmulationMode"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5203
    :cond_3
    iget-object v4, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v4, v4, Lcom/android/nfc/NfcService;->mSEControllerService:Lcom/android/nfc/NfcService$SEControllerService;

    if-eqz v4, :cond_1

    .line 5207
    :try_start_0
    iget-object v4, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v4, v4, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    iget-object v5, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v5, v5, Lcom/android/nfc/NfcService;->mSEControllerService:Lcom/android/nfc/NfcService$SEControllerService;

    invoke-virtual {v5}, Lcom/android/nfc/NfcService$SEControllerService;->getActiveSecureElement()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_1

    .line 5222
    new-instance v2, Lcom/android/nfc/NfcService$ApplyRoutingTask;

    iget-object v4, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {v2, v4, p1}, Lcom/android/nfc/NfcService$ApplyRoutingTask;-><init>(Lcom/android/nfc/NfcService;Lcom/gsma/services/nfc/INfcControllerCallbacks;)V

    new-array v4, v3, [Ljava/lang/Integer;

    invoke-virtual {v2, v4}, Lcom/android/nfc/NfcService$ApplyRoutingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v2, v3

    .line 5224
    goto :goto_0

    .line 5210
    :catch_0
    move-exception v0

    .line 5211
    .local v0, "e":Landroid/os/RemoteException;
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "Checking CDF failed."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5213
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 5214
    .local v1, "e2":Ljava/lang/NullPointerException;
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "mHciEventControl is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public enableCardEmulationMode(Lcom/gsma/services/nfc/INfcControllerCallbacks;)I
    .locals 7
    .param p1, "cb"    # Lcom/gsma/services/nfc/INfcControllerCallbacks;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    const/4 v2, -0x3

    .line 5158
    iget-object v5, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v5}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 5159
    sget-boolean v2, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "NfcService"

    const-string v3, "Gsma Apis are not Supported in this project"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5160
    :cond_0
    const/4 v2, -0x1

    .line 5193
    :cond_1
    :goto_0
    return v2

    .line 5162
    :cond_2
    sget-boolean v5, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v5, :cond_3

    const-string v5, "NfcService"

    const-string v6, "enableCardEmulationMode"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5163
    :cond_3
    iget-object v5, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v5, v5, Lcom/android/nfc/NfcService;->mSEControllerService:Lcom/android/nfc/NfcService$SEControllerService;

    if-eqz v5, :cond_1

    .line 5166
    iget-object v5, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v5}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v5

    if-nez v5, :cond_4

    move v2, v3

    .line 5167
    goto :goto_0

    .line 5170
    :cond_4
    :try_start_0
    iget-object v5, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v5, v5, Lcom/android/nfc/NfcService;->mHciEventControl:Lcom/android/nfc/HciEventControl;

    iget-object v6, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v6, v6, Lcom/android/nfc/NfcService;->mSEControllerService:Lcom/android/nfc/NfcService$SEControllerService;

    invoke-virtual {v6}, Lcom/android/nfc/NfcService$SEControllerService;->getActiveSecureElement()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/nfc/HciEventControl;->isAllowedForGsma(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-eqz v5, :cond_1

    .line 5182
    iget-object v2, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v2}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v3

    .line 5183
    goto :goto_0

    .line 5173
    :catch_0
    move-exception v0

    .line 5174
    .local v0, "e":Landroid/os/RemoteException;
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "Checking CDF failed."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5177
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 5178
    .local v1, "e2":Ljava/lang/NullPointerException;
    sget-boolean v3, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v3, :cond_1

    const-string v3, "NfcService"

    const-string v4, "mHciEventControl is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 5191
    .end local v1    # "e2":Ljava/lang/NullPointerException;
    :cond_5
    new-instance v2, Lcom/android/nfc/NfcService$ApplyRoutingTask;

    iget-object v3, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-direct {v2, v3, p1}, Lcom/android/nfc/NfcService$ApplyRoutingTask;-><init>(Lcom/android/nfc/NfcService;Lcom/gsma/services/nfc/INfcControllerCallbacks;)V

    new-array v3, v4, [Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Lcom/android/nfc/NfcService$ApplyRoutingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v2, v4

    .line 5193
    goto :goto_0
.end method

.method public enableNfcController(Lcom/gsma/services/nfc/INfcControllerCallbacks;)I
    .locals 3
    .param p1, "cb"    # Lcom/gsma/services/nfc/INfcControllerCallbacks;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 5121
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5122
    sget-boolean v0, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcService"

    const-string v1, "Gsma Apis are not Supported in this project"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5123
    :cond_0
    const/4 v0, -0x1

    .line 5134
    :goto_0
    return v0

    .line 5125
    :cond_1
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_2

    const-string v1, "NfcService"

    const-string v2, "enableNfcController"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5126
    :cond_2
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    iget-object v1, v1, Lcom/android/nfc/NfcService;->mSEControllerService:Lcom/android/nfc/NfcService$SEControllerService;

    if-nez v1, :cond_3

    .line 5127
    const/4 v0, -0x3

    goto :goto_0

    .line 5128
    :cond_3
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 5129
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # setter for: Lcom/android/nfc/NfcService;->gsmaCb:Lcom/gsma/services/nfc/INfcControllerCallbacks;
    invoke-static {v1, p1}, Lcom/android/nfc/NfcService;->access$2602(Lcom/android/nfc/NfcService;Lcom/gsma/services/nfc/INfcControllerCallbacks;)Lcom/gsma/services/nfc/INfcControllerCallbacks;

    .line 5130
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1, v0}, Lcom/android/nfc/NfcService;->sendGsmaPopup(I)V

    goto :goto_0

    .line 5132
    :cond_4
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Lcom/gsma/services/nfc/INfcControllerCallbacks;->onEnableNfcController(Z)V

    goto :goto_0
.end method

.method public getDefaultOffHostService(Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 5271
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/nfc/cardemulation/DynamicService;->getDefaultOffHostService(Ljava/lang/String;)Lcom/gsma/services/nfc/OffHostService;

    move-result-object v0

    return-object v0
.end method

.method public getOffHostServices(Ljava/lang/String;)[Lcom/gsma/services/nfc/OffHostService;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 5254
    iget-object v4, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->mDynamicService:Lcom/android/nfc/cardemulation/DynamicService;
    invoke-static {v4}, Lcom/android/nfc/NfcService;->access$3600(Lcom/android/nfc/NfcService;)Lcom/android/nfc/cardemulation/DynamicService;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/nfc/cardemulation/DynamicService;->getOffHostServices(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 5255
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/gsma/services/nfc/OffHostService;>;"
    if-nez v1, :cond_1

    .line 5266
    :cond_0
    return-object v2

    .line 5258
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5259
    .local v3, "size":I
    if-eqz v3, :cond_0

    .line 5262
    new-array v2, v3, [Lcom/gsma/services/nfc/OffHostService;

    .line 5263
    .local v2, "services":[Lcom/gsma/services/nfc/OffHostService;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 5264
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/gsma/services/nfc/OffHostService;

    aput-object v4, v2, v0

    .line 5263
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public isCardEmulationEnabled()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 5139
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v1}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5140
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "NfcService"

    const-string v2, "Gsma Apis are not Supported in this project"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5153
    :cond_0
    :goto_0
    return v0

    .line 5143
    :cond_1
    sget-boolean v1, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v1, :cond_2

    const-string v1, "NfcService"

    const-string v2, "isCardEmulationEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5144
    :cond_2
    iget-object v1, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v1}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5153
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5112
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    # getter for: Lcom/android/nfc/NfcService;->isGsmaApiSupported:Z
    invoke-static {v0}, Lcom/android/nfc/NfcService;->access$100(Lcom/android/nfc/NfcService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5113
    sget-boolean v0, Lcom/android/nfc/NfcService;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "NfcService"

    const-string v1, "Gsma Apis are not Supported in this project"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5114
    :cond_0
    const/4 v0, 0x0

    .line 5116
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/nfc/NfcService$NfcControllerService;->this$0:Lcom/android/nfc/NfcService;

    invoke-virtual {v0}, Lcom/android/nfc/NfcService;->isNfcEnabled()Z

    move-result v0

    goto :goto_0
.end method
