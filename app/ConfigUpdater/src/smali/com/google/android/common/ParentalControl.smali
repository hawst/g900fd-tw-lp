.class public Lcom/google/android/common/ParentalControl;
.super Ljava/lang/Object;
.source "ParentalControl.java"


# static fields
.field private static final APN_ALREADY_ACTIVE:I = 0x0

.field private static final APN_REQUEST_STARTED:I = 0x1

.field private static final DEFAULT_TIMEOUT_MILLIS:J = 0x2932e00L

.field private static final FEATURE_ENABLE_HIPRI:Ljava/lang/String; = "enableHIPRI"

.field private static final HIPRI_ATTEMPTS:I = 0x14

.field private static final HIPRI_ATTEMPT_MILLIS:I = 0x3e8

.field private static final KEY_ENABLED:Ljava/lang/String; = "enabled"

.field private static final KEY_LANDING_URL:Ljava/lang/String; = "landingUrl"

.field private static final LITMUS_URL:Ljava/lang/String; = "http://android.clients.google.com/content/default"

.field private static final PREFS_NAME:Ljava/lang/String; = "ParentalControl"

.field private static final TAG:Ljava/lang/String; = "ParentalControl"

.field public static final VENDING_APP:Ljava/lang/String; = "vending"

.field public static final YOUTUBE_APP:Ljava/lang/String; = "youtube"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLandingPage(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "app"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-static {p0, p1}, Lcom/google/android/common/ParentalControl;->isEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-object v2

    .line 127
    :cond_1
    const-string v3, "ParentalControl"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 128
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "landingUrl"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 129
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method public static getLastCheckState(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 151
    const-string v0, "ParentalControl"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getLastCheckTimeMillis(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    const-string v2, "ParentalControl"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 140
    .local v0, "prefs":Landroid/content/SharedPreferences;
    new-instance v1, Lcom/android/common/OperationScheduler;

    invoke-direct {v1, v0}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    .line 141
    .local v1, "scheduler":Lcom/android/common/OperationScheduler;
    invoke-virtual {v1}, Lcom/android/common/OperationScheduler;->getLastSuccessTimeMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public static isEnabled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "app"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 94
    const-string v3, "ParentalControl"

    const-string v4, "Network request on main thread"

    invoke-static {v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 99
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v3, "parental_control_check_enabled"

    invoke-static {v1, v3, v2}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    .line 112
    :cond_1
    :goto_0
    return v2

    .line 103
    :cond_2
    if-eqz p1, :cond_3

    .line 104
    const-string v3, "parental_control_apps_list"

    invoke-static {v1, v3}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "apps":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    .end local v0    # "apps":Ljava/lang/String;
    :cond_3
    invoke-static {p0}, Lcom/google/android/common/ParentalControl;->maybeCheckState(Landroid/content/Context;)V

    .line 112
    invoke-static {p0}, Lcom/google/android/common/ParentalControl;->getLastCheckState(Landroid/content/Context;)Z

    move-result v2

    goto :goto_0
.end method

.method private static isHipriActive(Landroid/net/ConnectivityManager;)Z
    .locals 1
    .param p0, "cm"    # Landroid/net/ConnectivityManager;

    .prologue
    .line 155
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    return v0
.end method

.method private static maybeCheckState(Landroid/content/Context;)V
    .locals 26
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 197
    .local v7, "cr":Landroid/content/ContentResolver;
    const-string v22, "ParentalControl"

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 198
    .local v13, "prefs":Landroid/content/SharedPreferences;
    new-instance v17, Lcom/android/common/OperationScheduler;

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    .line 199
    .local v17, "scheduler":Lcom/android/common/OperationScheduler;
    new-instance v12, Lcom/android/common/OperationScheduler$Options;

    invoke-direct {v12}, Lcom/android/common/OperationScheduler$Options;-><init>()V

    .line 200
    .local v12, "options":Lcom/android/common/OperationScheduler$Options;
    const-string v22, "parental_control_timeout_in_ms"

    const-wide/32 v24, 0x2932e00

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-static {v7, v0, v1, v2}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v22

    move-wide/from16 v0, v22

    iput-wide v0, v12, Lcom/android/common/OperationScheduler$Options;->periodicIntervalMillis:J

    .line 203
    new-instance v22, Ljava/io/File;

    const-string v23, "/proc/1"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->lastModified()J

    move-result-wide v22

    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->getLastSuccessTimeMillis()J

    move-result-wide v24

    cmp-long v22, v22, v24

    if-lez v22, :cond_0

    .line 204
    const-wide/16 v22, 0x0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 207
    :cond_0
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v22

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    cmp-long v22, v22, v24

    if-lez v22, :cond_1

    .line 289
    :goto_0
    return-void

    .line 211
    :cond_1
    const-string v22, "connectivity"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    .line 213
    .local v6, "cm":Landroid/net/ConnectivityManager;
    if-nez v6, :cond_2

    .line 214
    const-string v22, "ParentalControl"

    const-string v23, "Parental control unchanged: No ConnectivityManager"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 219
    :cond_2
    new-instance v5, Lcom/google/android/common/http/GoogleHttpClient;

    const-string v22, "Android-PC"

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 222
    .local v5, "client":Lcom/google/android/common/http/GoogleHttpClient;
    :try_start_0
    invoke-static {v6}, Lcom/google/android/common/ParentalControl;->waitForHipri(Landroid/net/ConnectivityManager;)Z

    move-result v22

    if-nez v22, :cond_3

    .line 223
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onTransientError()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    .line 287
    const/16 v22, 0x0

    const-string v23, "enableHIPRI"

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    goto :goto_0

    .line 227
    :cond_3
    :try_start_1
    const-string v22, "http://android.clients.google.com/content/default"

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 228
    .local v19, "uri":Landroid/net/Uri;
    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    .line 229
    .local v4, "addr":Ljava/net/InetAddress;
    invoke-virtual {v4}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v11

    .line 230
    .local v11, "octets":[B
    const/16 v22, 0x5

    const/16 v23, 0x3

    aget-byte v23, v11, v23

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    shl-int/lit8 v23, v23, 0x18

    const/16 v24, 0x2

    aget-byte v24, v11, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    shl-int/lit8 v24, v24, 0x10

    or-int v23, v23, v24

    const/16 v24, 0x1

    aget-byte v24, v11, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    shl-int/lit8 v24, v24, 0x8

    or-int v23, v23, v24

    const/16 v24, 0x0

    aget-byte v24, v11, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    or-int v23, v23, v24

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v22

    if-nez v22, :cond_4

    .line 233
    const-string v22, "ParentalControl"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Parental control unchanged: Error rerouting "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onTransientError()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    .line 287
    const/16 v22, 0x0

    const-string v23, "enableHIPRI"

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    goto/16 :goto_0

    .line 238
    :cond_4
    :try_start_2
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getPort()I

    move-result v22

    if-lez v22, :cond_7

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, ":"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getPort()I

    move-result v24

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    :goto_1
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 239
    .local v10, "hostport":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v19

    .line 241
    const-string v22, "ParentalControl"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Attempting litmus URL fetch: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    new-instance v15, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 243
    .local v15, "request":Lorg/apache/http/client/methods/HttpGet;
    const-string v22, "Connection"

    const-string v23, "close"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v15, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {v5, v15}, Lcom/google/android/common/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v16

    .line 245
    .local v16, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v18

    .line 247
    .local v18, "status":I
    const/16 v22, 0xc8

    move/from16 v0, v18

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    .line 248
    const-string v22, "parental_control_expected_response"

    move-object/from16 v0, v22

    invoke-static {v7, v0}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 250
    .local v9, "expected":Ljava/lang/String;
    if-eqz v9, :cond_5

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 253
    :cond_5
    const-string v22, "ParentalControl"

    const-string v23, "Parental control is OFF: Litmus fetch succeeded"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onSuccess()V

    .line 255
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "enabled"

    const/16 v24, 0x0

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 286
    .end local v9    # "expected":Ljava/lang/String;
    :cond_6
    :goto_2
    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    .line 287
    const/16 v22, 0x0

    const-string v23, "enableHIPRI"

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    goto/16 :goto_0

    .line 238
    .end local v10    # "hostport":Ljava/lang/String;
    .end local v15    # "request":Lorg/apache/http/client/methods/HttpGet;
    .end local v16    # "response":Lorg/apache/http/HttpResponse;
    .end local v18    # "status":I
    :cond_7
    :try_start_3
    const-string v22, ""

    goto/16 :goto_1

    .line 258
    .restart local v9    # "expected":Ljava/lang/String;
    .restart local v10    # "hostport":Ljava/lang/String;
    .restart local v15    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "response":Lorg/apache/http/HttpResponse;
    .restart local v18    # "status":I
    :cond_8
    const-string v22, "ParentalControl"

    const-string v23, "Parental control is ON: Litmus content was modified"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onSuccess()V

    .line 260
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "enabled"

    const/16 v24, 0x1

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "landingUrl"

    const-string v24, ""

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 282
    .end local v4    # "addr":Ljava/net/InetAddress;
    .end local v9    # "expected":Ljava/lang/String;
    .end local v10    # "hostport":Ljava/lang/String;
    .end local v11    # "octets":[B
    .end local v15    # "request":Lorg/apache/http/client/methods/HttpGet;
    .end local v16    # "response":Lorg/apache/http/HttpResponse;
    .end local v18    # "status":I
    .end local v19    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 283
    .local v8, "e":Ljava/io/IOException;
    :try_start_4
    const-string v22, "ParentalControl"

    const-string v23, "Parental control unchanged: Litmus fetch failed"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 284
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onTransientError()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 286
    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    .line 287
    const/16 v22, 0x0

    const-string v23, "enableHIPRI"

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    goto/16 :goto_0

    .line 264
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v4    # "addr":Ljava/net/InetAddress;
    .restart local v10    # "hostport":Ljava/lang/String;
    .restart local v11    # "octets":[B
    .restart local v15    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "response":Lorg/apache/http/HttpResponse;
    .restart local v18    # "status":I
    .restart local v19    # "uri":Landroid/net/Uri;
    :cond_9
    const/16 v22, 0x12e

    move/from16 v0, v18

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 265
    :try_start_5
    const-string v22, "parental_control_redirect_regex"

    move-object/from16 v0, v22

    invoke-static {v7, v0}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 267
    .local v21, "whitelist":Ljava/lang/String;
    const-string v22, "location"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v14

    .line 268
    .local v14, "redirect":Lorg/apache/http/Header;
    if-nez v14, :cond_a

    const/16 v20, 0x0

    .line 269
    .local v20, "url":Ljava/lang/String;
    :goto_3
    if-eqz v21, :cond_b

    if-eqz v20, :cond_b

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 271
    const-string v22, "ParentalControl"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Parental control is ON: Litmus redirects to "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onSuccess()V

    .line 273
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "enabled"

    const/16 v24, 0x1

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "landingUrl"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 286
    .end local v4    # "addr":Ljava/net/InetAddress;
    .end local v10    # "hostport":Ljava/lang/String;
    .end local v11    # "octets":[B
    .end local v14    # "redirect":Lorg/apache/http/Header;
    .end local v15    # "request":Lorg/apache/http/client/methods/HttpGet;
    .end local v16    # "response":Lorg/apache/http/HttpResponse;
    .end local v18    # "status":I
    .end local v19    # "uri":Landroid/net/Uri;
    .end local v20    # "url":Ljava/lang/String;
    .end local v21    # "whitelist":Ljava/lang/String;
    :catchall_0
    move-exception v22

    invoke-virtual {v5}, Lcom/google/android/common/http/GoogleHttpClient;->close()V

    .line 287
    const/16 v23, 0x0

    const-string v24, "enableHIPRI"

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v6, v0, v1}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    throw v22

    .line 268
    .restart local v4    # "addr":Ljava/net/InetAddress;
    .restart local v10    # "hostport":Ljava/lang/String;
    .restart local v11    # "octets":[B
    .restart local v14    # "redirect":Lorg/apache/http/Header;
    .restart local v15    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "response":Lorg/apache/http/HttpResponse;
    .restart local v18    # "status":I
    .restart local v19    # "uri":Landroid/net/Uri;
    .restart local v21    # "whitelist":Ljava/lang/String;
    :cond_a
    :try_start_6
    invoke-interface {v14}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v20

    goto :goto_3

    .line 277
    .restart local v20    # "url":Ljava/lang/String;
    :cond_b
    invoke-virtual/range {v17 .. v17}, Lcom/android/common/OperationScheduler;->onTransientError()V

    .line 278
    const-string v22, "ParentalControl"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Parental control unchanged: Unknown litmus redirect "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    if-nez v20, :cond_c

    const-string v20, "(none)"

    .end local v20    # "url":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2
.end method

.method private static waitForHipri(Landroid/net/ConnectivityManager;)Z
    .locals 8
    .param p0, "cm"    # Landroid/net/ConnectivityManager;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 167
    invoke-static {p0}, Lcom/google/android/common/ParentalControl;->isHipriActive(Landroid/net/ConnectivityManager;)Z

    move-result v0

    .line 170
    .local v0, "alreadyActive":Z
    const-string v5, "enableHIPRI"

    invoke-virtual {p0, v4, v5}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v2

    .line 174
    .local v2, "result":I
    if-eqz v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v3

    .line 178
    :cond_1
    if-eqz v2, :cond_2

    if-eq v2, v3, :cond_2

    .line 179
    const-string v3, "ParentalControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Parental control unchanged: Mobile network error, code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 180
    goto :goto_0

    .line 183
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/16 v5, 0x14

    if-ge v1, v5, :cond_3

    .line 184
    const-string v5, "ParentalControl"

    const-string v6, "Waiting 1000ms for mobile network"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const-wide/16 v6, 0x3e8

    invoke-static {v6, v7}, Landroid/os/SystemClock;->sleep(J)V

    .line 186
    invoke-static {p0}, Lcom/google/android/common/ParentalControl;->isHipriActive(Landroid/net/ConnectivityManager;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 183
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 191
    :cond_3
    const-string v3, "ParentalControl"

    const-string v5, "Parental control unchanged: Timed out waiting for mobile network"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 192
    goto :goto_0
.end method
