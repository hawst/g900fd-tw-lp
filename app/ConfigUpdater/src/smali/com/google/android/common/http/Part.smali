.class public abstract Lcom/google/android/common/http/Part;
.super Ljava/lang/Object;
.source "Part.java"


# static fields
.field protected static final BOUNDARY:Ljava/lang/String; = "----------------314159265358979323846"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected static final BOUNDARY_BYTES:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected static final CHARSET:Ljava/lang/String; = "; charset="

.field protected static final CHARSET_BYTES:[B

.field protected static final CONTENT_DISPOSITION:Ljava/lang/String; = "Content-Disposition: form-data; name="

.field protected static final CONTENT_DISPOSITION_BYTES:[B

.field protected static final CONTENT_TRANSFER_ENCODING:Ljava/lang/String; = "Content-Transfer-Encoding: "

.field protected static final CONTENT_TRANSFER_ENCODING_BYTES:[B

.field protected static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type: "

.field protected static final CONTENT_TYPE_BYTES:[B

.field protected static final CRLF:Ljava/lang/String; = "\r\n"

.field protected static final CRLF_BYTES:[B

.field private static final DEFAULT_BOUNDARY_BYTES:[B

.field protected static final EXTRA:Ljava/lang/String; = "--"

.field protected static final EXTRA_BYTES:[B

.field protected static final QUOTE:Ljava/lang/String; = "\""

.field protected static final QUOTE_BYTES:[B


# instance fields
.field private boundaryBytes:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "----------------314159265358979323846"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->BOUNDARY_BYTES:[B

    .line 75
    sget-object v0, Lcom/google/android/common/http/Part;->BOUNDARY_BYTES:[B

    sput-object v0, Lcom/google/android/common/http/Part;->DEFAULT_BOUNDARY_BYTES:[B

    .line 81
    const-string v0, "\r\n"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    .line 87
    const-string v0, "\""

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->QUOTE_BYTES:[B

    .line 94
    const-string v0, "--"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->EXTRA_BYTES:[B

    .line 101
    const-string v0, "Content-Disposition: form-data; name="

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->CONTENT_DISPOSITION_BYTES:[B

    .line 108
    const-string v0, "Content-Type: "

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->CONTENT_TYPE_BYTES:[B

    .line 115
    const-string v0, "; charset="

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->CHARSET_BYTES:[B

    .line 122
    const-string v0, "Content-Transfer-Encoding: "

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/common/http/Part;->CONTENT_TRANSFER_ENCODING_BYTES:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoundary()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132
    const-string v0, "----------------314159265358979323846"

    return-object v0
.end method

.method public static getLengthOfParts([Lcom/google/android/common/http/Part;)J
    .locals 2
    .param p0, "parts"    # [Lcom/google/android/common/http/Part;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    sget-object v0, Lcom/google/android/common/http/Part;->DEFAULT_BOUNDARY_BYTES:[B

    invoke-static {p0, v0}, Lcom/google/android/common/http/Part;->getLengthOfParts([Lcom/google/android/common/http/Part;[B)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getLengthOfParts([Lcom/google/android/common/http/Part;[B)J
    .locals 8
    .param p0, "parts"    # [Lcom/google/android/common/http/Part;
    .param p1, "partBoundary"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 423
    if-nez p0, :cond_0

    .line 424
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v6, "Parts may not be null"

    invoke-direct {v1, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 426
    :cond_0
    const-wide/16 v4, 0x0

    .line 427
    .local v4, "total":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_2

    .line 429
    aget-object v1, p0, v0

    invoke-virtual {v1, p1}, Lcom/google/android/common/http/Part;->setPartBoundary([B)V

    .line 430
    aget-object v1, p0, v0

    invoke-virtual {v1}, Lcom/google/android/common/http/Part;->length()J

    move-result-wide v2

    .line 431
    .local v2, "l":J
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    .line 432
    const-wide/16 v6, -0x1

    .line 440
    .end local v2    # "l":J
    :goto_1
    return-wide v6

    .line 434
    .restart local v2    # "l":J
    :cond_1
    add-long/2addr v4, v2

    .line 427
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 436
    .end local v2    # "l":J
    :cond_2
    sget-object v1, Lcom/google/android/common/http/Part;->EXTRA_BYTES:[B

    array-length v1, v1

    int-to-long v6, v1

    add-long/2addr v4, v6

    .line 437
    array-length v1, p1

    int-to-long v6, v1

    add-long/2addr v4, v6

    .line 438
    sget-object v1, Lcom/google/android/common/http/Part;->EXTRA_BYTES:[B

    array-length v1, v1

    int-to-long v6, v1

    add-long/2addr v4, v6

    .line 439
    sget-object v1, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    array-length v1, v1

    int-to-long v6, v1

    add-long/2addr v4, v6

    move-wide v6, v4

    .line 440
    goto :goto_1
.end method

.method public static sendParts(Ljava/io/OutputStream;[Lcom/google/android/common/http/Part;)V
    .locals 1
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "parts"    # [Lcom/google/android/common/http/Part;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    sget-object v0, Lcom/google/android/common/http/Part;->DEFAULT_BOUNDARY_BYTES:[B

    invoke-static {p0, p1, v0}, Lcom/google/android/common/http/Part;->sendParts(Ljava/io/OutputStream;[Lcom/google/android/common/http/Part;[B)V

    .line 364
    return-void
.end method

.method public static sendParts(Ljava/io/OutputStream;[Lcom/google/android/common/http/Part;[B)V
    .locals 3
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "parts"    # [Lcom/google/android/common/http/Part;
    .param p2, "partBoundary"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    if-nez p1, :cond_0

    .line 381
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Parts may not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 383
    :cond_0
    if-eqz p2, :cond_1

    array-length v1, p2

    if-nez v1, :cond_2

    .line 384
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "partBoundary may not be empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 386
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 388
    aget-object v1, p1, v0

    invoke-virtual {v1, p2}, Lcom/google/android/common/http/Part;->setPartBoundary([B)V

    .line 389
    aget-object v1, p1, v0

    invoke-virtual {v1, p0}, Lcom/google/android/common/http/Part;->send(Ljava/io/OutputStream;)V

    .line 386
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 391
    :cond_3
    sget-object v1, Lcom/google/android/common/http/Part;->EXTRA_BYTES:[B

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 392
    invoke-virtual {p0, p2}, Ljava/io/OutputStream;->write([B)V

    .line 393
    sget-object v1, Lcom/google/android/common/http/Part;->EXTRA_BYTES:[B

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 394
    sget-object v1, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 395
    return-void
.end method


# virtual methods
.method public abstract getCharSet()Ljava/lang/String;
.end method

.method public abstract getContentType()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method protected getPartBoundary()[B
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/common/http/Part;->boundaryBytes:[B

    if-nez v0, :cond_0

    .line 174
    sget-object v0, Lcom/google/android/common/http/Part;->DEFAULT_BOUNDARY_BYTES:[B

    .line 176
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/common/http/Part;->boundaryBytes:[B

    goto :goto_0
.end method

.method public abstract getTransferEncoding()Ljava/lang/String;
.end method

.method public isRepeatable()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x1

    return v0
.end method

.method public length()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->lengthOfData()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 331
    const-wide/16 v2, -0x1

    .line 340
    :goto_0
    return-wide v2

    .line 333
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 334
    .local v0, "overhead":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/google/android/common/http/Part;->sendStart(Ljava/io/OutputStream;)V

    .line 335
    invoke-virtual {p0, v0}, Lcom/google/android/common/http/Part;->sendDispositionHeader(Ljava/io/OutputStream;)V

    .line 336
    invoke-virtual {p0, v0}, Lcom/google/android/common/http/Part;->sendContentTypeHeader(Ljava/io/OutputStream;)V

    .line 337
    invoke-virtual {p0, v0}, Lcom/google/android/common/http/Part;->sendTransferEncodingHeader(Ljava/io/OutputStream;)V

    .line 338
    invoke-virtual {p0, v0}, Lcom/google/android/common/http/Part;->sendEndOfHeader(Ljava/io/OutputStream;)V

    .line 339
    invoke-virtual {p0, v0}, Lcom/google/android/common/http/Part;->sendEnd(Ljava/io/OutputStream;)V

    .line 340
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->lengthOfData()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method protected abstract lengthOfData()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public send(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendStart(Ljava/io/OutputStream;)V

    .line 311
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendDispositionHeader(Ljava/io/OutputStream;)V

    .line 312
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendContentTypeHeader(Ljava/io/OutputStream;)V

    .line 313
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendTransferEncodingHeader(Ljava/io/OutputStream;)V

    .line 314
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendEndOfHeader(Ljava/io/OutputStream;)V

    .line 315
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendData(Ljava/io/OutputStream;)V

    .line 316
    invoke-virtual {p0, p1}, Lcom/google/android/common/http/Part;->sendEnd(Ljava/io/OutputStream;)V

    .line 317
    return-void
.end method

.method protected sendContentTypeHeader(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->getContentType()Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "contentType":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 236
    sget-object v2, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 237
    sget-object v2, Lcom/google/android/common/http/Part;->CONTENT_TYPE_BYTES:[B

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 238
    invoke-static {v1}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->getCharSet()Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "charSet":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 241
    sget-object v2, Lcom/google/android/common/http/Part;->CHARSET_BYTES:[B

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 242
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 245
    .end local v0    # "charSet":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected abstract sendData(Ljava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected sendDispositionHeader(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    sget-object v0, Lcom/google/android/common/http/Part;->CONTENT_DISPOSITION_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 222
    sget-object v0, Lcom/google/android/common/http/Part;->QUOTE_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 223
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 224
    sget-object v0, Lcom/google/android/common/http/Part;->QUOTE_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 225
    return-void
.end method

.method protected sendEnd(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    sget-object v0, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 298
    return-void
.end method

.method protected sendEndOfHeader(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    sget-object v0, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 272
    sget-object v0, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 273
    return-void
.end method

.method protected sendStart(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    sget-object v0, Lcom/google/android/common/http/Part;->EXTRA_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 209
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->getPartBoundary()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 210
    sget-object v0, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 211
    return-void
.end method

.method protected sendTransferEncodingHeader(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->getTransferEncoding()Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "transferEncoding":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 258
    sget-object v1, Lcom/google/android/common/http/Part;->CRLF_BYTES:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 259
    sget-object v1, Lcom/google/android/common/http/Part;->CONTENT_TRANSFER_ENCODING_BYTES:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 260
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 262
    :cond_0
    return-void
.end method

.method setPartBoundary([B)V
    .locals 0
    .param p1, "boundaryBytes"    # [B

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/common/http/Part;->boundaryBytes:[B

    .line 189
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/google/android/common/http/Part;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
