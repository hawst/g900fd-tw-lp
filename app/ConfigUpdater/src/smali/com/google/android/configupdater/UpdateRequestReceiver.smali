.class public abstract Lcom/google/android/configupdater/UpdateRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UpdateRequestReceiver.java"


# instance fields
.field private final RETRY_INTERVAL_MILLIS:J

.field private final TAG:Ljava/lang/String;

.field private final mConfig:Lcom/google/android/configupdater/Config;

.field private final mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

.field private final mGservices:Lcom/google/android/configupdater/GservicesHelper;

.field private final mState:Lcom/google/android/configupdater/StoredState;


# direct methods
.method protected constructor <init>(Lcom/google/android/configupdater/Config;Lcom/google/android/configupdater/StoredState;Lcom/google/android/configupdater/DownloadManagerHelper;Lcom/google/android/configupdater/GservicesHelper;)V
    .locals 2
    .param p1, "c"    # Lcom/google/android/configupdater/Config;
    .param p2, "s"    # Lcom/google/android/configupdater/StoredState;
    .param p3, "dm"    # Lcom/google/android/configupdater/DownloadManagerHelper;
    .param p4, "gs"    # Lcom/google/android/configupdater/GservicesHelper;

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 36
    const-string v0, "UpdateRequestReceiver"

    iput-object v0, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->TAG:Ljava/lang/String;

    .line 37
    const-wide/32 v0, 0x36ee80

    iput-wide v0, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->RETRY_INTERVAL_MILLIS:J

    .line 48
    iput-object p1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    .line 49
    iput-object p2, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mState:Lcom/google/android/configupdater/StoredState;

    .line 50
    iput-object p3, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    .line 51
    iput-object p4, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mGservices:Lcom/google/android/configupdater/GservicesHelper;

    .line 52
    return-void
.end method

.method private getContentUrl(Landroid/content/Context;Landroid/content/Intent;)Ljava/net/URL;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 257
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-string v1, "CONTENT_URL"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "contentLocation":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 260
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mGservices:Lcom/google/android/configupdater/GservicesHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/configupdater/GservicesHelper;->getContentUrl(Landroid/content/Context;)Ljava/net/URL;

    move-result-object v1

    .line 262
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getMetadataUrl(Landroid/content/Context;Landroid/content/Intent;)Ljava/net/URL;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 272
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-string v1, "METADATA_URL"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "metadataLocation":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 275
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mGservices:Lcom/google/android/configupdater/GservicesHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/configupdater/GservicesHelper;->getMetadataUrl(Landroid/content/Context;)Ljava/net/URL;

    move-result-object v1

    .line 277
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleContentDownload(Landroid/content/Context;Ljava/net/URL;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadPath"    # Ljava/net/URL;

    .prologue
    .line 230
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v1}, Lcom/google/android/configupdater/Config;->getUpdateService()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 231
    .local v0, "forwardingIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v1}, Lcom/google/android/configupdater/Config;->getNewContentAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-string v1, "CONTENT_DOWNLOAD_PATH"

    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 234
    return-void
.end method

.method private handleDownloadCompletedIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v1, p2}, Lcom/google/android/configupdater/DownloadManagerHelper;->isContentDownloadCompleteIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v1, p2}, Lcom/google/android/configupdater/DownloadManagerHelper;->getLocalPathOrNull(Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v0

    .line 137
    .local v0, "localPath":Ljava/net/URL;
    if-nez v0, :cond_1

    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateRequestReceiver;->retry(Landroid/content/Context;)V

    .line 150
    .end local v0    # "localPath":Ljava/net/URL;
    :cond_0
    :goto_0
    return-void

    .line 141
    .restart local v0    # "localPath":Ljava/net/URL;
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/configupdater/UpdateRequestReceiver;->handleContentDownload(Landroid/content/Context;Ljava/net/URL;)V

    goto :goto_0

    .line 142
    .end local v0    # "localPath":Ljava/net/URL;
    :cond_2
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v1, p2}, Lcom/google/android/configupdater/DownloadManagerHelper;->isMetadataDownloadCompleteIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v1, p2}, Lcom/google/android/configupdater/DownloadManagerHelper;->getLocalPathOrNull(Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v0

    .line 144
    .restart local v0    # "localPath":Ljava/net/URL;
    if-nez v0, :cond_3

    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateRequestReceiver;->retry(Landroid/content/Context;)V

    goto :goto_0

    .line 148
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/google/android/configupdater/UpdateRequestReceiver;->handleMetadataDownload(Landroid/content/Context;Ljava/net/URL;)V

    goto :goto_0
.end method

.method private handleGservicesChangedIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 162
    const/4 v0, 0x0

    .line 163
    .local v0, "changed":Z
    :try_start_0
    iget-object v6, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mGservices:Lcom/google/android/configupdater/GservicesHelper;

    invoke-virtual {v6, p1}, Lcom/google/android/configupdater/GservicesHelper;->getContentUrl(Landroid/content/Context;)Ljava/net/URL;

    move-result-object v2

    .line 164
    .local v2, "gservicesContentUrl":Ljava/net/URL;
    iget-object v6, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v6}, Lcom/google/android/configupdater/DownloadManagerHelper;->getContentUrl()Ljava/net/URL;

    move-result-object v4

    .line 165
    .local v4, "storedContentUrl":Ljava/net/URL;
    iget-object v6, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mGservices:Lcom/google/android/configupdater/GservicesHelper;

    invoke-virtual {v6, p1}, Lcom/google/android/configupdater/GservicesHelper;->getMetadataUrl(Landroid/content/Context;)Ljava/net/URL;

    move-result-object v3

    .line 166
    .local v3, "gservicesMetadataUrl":Ljava/net/URL;
    iget-object v6, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v6}, Lcom/google/android/configupdater/DownloadManagerHelper;->getMetadataUrl()Ljava/net/URL;

    move-result-object v5

    .line 167
    .local v5, "storedMetadataUrl":Ljava/net/URL;
    invoke-virtual {v2, v4}, Ljava/net/URL;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 168
    iget-object v6, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v6, v2}, Lcom/google/android/configupdater/DownloadManagerHelper;->setContentUrl(Ljava/net/URL;)V

    .line 169
    const/4 v0, 0x1

    .line 171
    :cond_0
    invoke-virtual {v3, v5}, Ljava/net/URL;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 172
    iget-object v6, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v6, v3}, Lcom/google/android/configupdater/DownloadManagerHelper;->setMetadataUrl(Ljava/net/URL;)V

    .line 173
    const/4 v0, 0x1

    .line 175
    :cond_1
    if-eqz v0, :cond_2

    .line 176
    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/configupdater/UpdateRequestReceiver;->startUpdate(Landroid/content/Context;Ljava/net/URL;Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    .end local v2    # "gservicesContentUrl":Ljava/net/URL;
    .end local v3    # "gservicesMetadataUrl":Ljava/net/URL;
    .end local v4    # "storedContentUrl":Ljava/net/URL;
    .end local v5    # "storedMetadataUrl":Ljava/net/URL;
    :cond_2
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v1

    .line 179
    .local v1, "e":Ljava/net/MalformedURLException;
    const-string v6, "UpdateRequestReceiver"

    const-string v7, "Received malformed URL while handling Gservices.CHANGED_ACTION"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleMetadataDownload(Landroid/content/Context;Ljava/net/URL;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadPath"    # Ljava/net/URL;

    .prologue
    .line 244
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v1}, Lcom/google/android/configupdater/Config;->getUpdateService()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 245
    .local v0, "forwardingIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v1}, Lcom/google/android/configupdater/Config;->getNewMetadataAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-string v1, "METADATA_DOWNLOAD_PATH"

    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 248
    return-void
.end method

.method private handleUpdateRequestIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 201
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->getContentUrl(Landroid/content/Context;Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v0

    .line 202
    .local v0, "contentUrl":Ljava/net/URL;
    invoke-direct {p0, p1, p2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->getMetadataUrl(Landroid/content/Context;Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v2

    .line 203
    .local v2, "metadataUrl":Ljava/net/URL;
    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->startUpdate(Landroid/content/Context;Ljava/net/URL;Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    .end local v0    # "contentUrl":Ljava/net/URL;
    .end local v2    # "metadataUrl":Ljava/net/URL;
    :goto_0
    return-void

    .line 204
    :catch_0
    move-exception v1

    .line 205
    .local v1, "e":Ljava/net/MalformedURLException;
    const-string v3, "UpdateRequestReceiver"

    const-string v4, "Received malformed URL while handling an update request."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ignoreIncomingRequests()Z
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mState:Lcom/google/android/configupdater/StoredState;

    invoke-virtual {v0}, Lcom/google/android/configupdater/StoredState;->getUpdateTimeStarted()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-wide/32 v2, 0x1499700

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private retry(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 291
    const-string v5, "UpdateRequestReceiver"

    const-string v6, "Download failed, retrying..."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v5}, Lcom/google/android/configupdater/Config;->getUserUpdateAction()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 293
    .local v1, "i":Landroid/content/Intent;
    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-direct {v5, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 294
    invoke-static {p1, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 295
    .local v4, "pi":Landroid/app/PendingIntent;
    const-string v5, "alarm"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 296
    .local v0, "alarmMgr":Landroid/app/AlarmManager;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/32 v8, 0x36ee80

    add-long v2, v6, v8

    .line 297
    .local v2, "oneHourFromNow":J
    const/4 v5, 0x3

    invoke-virtual {v0, v5, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 298
    return-void
.end method

.method private setUpPrefs(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mState:Lcom/google/android/configupdater/StoredState;

    invoke-virtual {v0, p1}, Lcom/google/android/configupdater/StoredState;->setup(Landroid/content/Context;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setup(Landroid/content/Context;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mGservices:Lcom/google/android/configupdater/GservicesHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/configupdater/GservicesHelper;->setup(Landroid/content/Context;)V

    .line 121
    return-void
.end method

.method private startUpdate(Landroid/content/Context;Ljava/net/URL;Ljava/net/URL;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentUrl"    # Ljava/net/URL;
    .param p3, "metadataUrl"    # Ljava/net/URL;

    .prologue
    .line 216
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v1}, Lcom/google/android/configupdater/Config;->getUpdateService()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 217
    .local v0, "forwardingIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    invoke-virtual {v1}, Lcom/google/android/configupdater/Config;->getStartUpdateAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-string v1, "CONTENT_URL"

    invoke-virtual {p2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    iget-object v1, p0, Lcom/google/android/configupdater/UpdateRequestReceiver;->mConfig:Lcom/google/android/configupdater/Config;

    const-string v1, "METADATA_URL"

    invoke-virtual {p3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 221
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateRequestReceiver;->setUpPrefs(Landroid/content/Context;)V

    .line 101
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    invoke-direct {p0, p1, p2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->handleDownloadCompletedIntent(Landroid/content/Context;Landroid/content/Intent;)V

    .line 115
    :goto_0
    return-void

    .line 104
    :cond_0
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    invoke-direct {p0, p1, p2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->handleGservicesChangedIntent(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 106
    :cond_1
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 107
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateRequestReceiver;->ignoreIncomingRequests()Z

    move-result v1

    if-nez v1, :cond_2

    .line 108
    invoke-direct {p0, p1, p2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->handleUpdateRequestIntent(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 110
    :cond_2
    const-string v1, "UpdateRequestReceiver"

    const-string v2, "ignoring update request"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 113
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/configupdater/UpdateRequestReceiver;->handleUpdateRequestIntent(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
