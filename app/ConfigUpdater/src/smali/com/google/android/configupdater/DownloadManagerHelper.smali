.class public Lcom/google/android/configupdater/DownloadManagerHelper;
.super Ljava/lang/Object;
.source "DownloadManagerHelper.java"


# static fields
.field public static final CONTENT_DOWNLOAD_ID_KEY:Ljava/lang/String; = "CONTENT_DOWNLOAD_ID"

.field public static final CONTENT_URL_KEY:Ljava/lang/String; = "CONTENT_URL"

.field public static final DOWNLOAD_PATH:Ljava/lang/String; = "downloads"

.field public static final METADATA_DOWNLOAD_ID_KEY:Ljava/lang/String; = "METADATA_DOWNLOAD_ID"

.field public static final METADATA_URL_KEY:Ljava/lang/String; = "METADATA_URL"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDownloadDir:Ljava/io/File;

.field protected mDownloadManager:Landroid/app/DownloadManager;

.field protected final mPrefsName:Ljava/lang/String;

.field protected mStorage:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "prefsName"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "ConfigUpdaterDownloadManagerHelper"

    iput-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->TAG:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mPrefsName:Ljava/lang/String;

    .line 55
    return-void
.end method

.method private copyLocally(Ljava/lang/String;J)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "downloadId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 210
    .local v1, "origin":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadDir:Ljava/io/File;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 211
    .local v0, "destination":Ljava/io/File;
    invoke-static {v1, v0}, Lcom/google/common/io/Files;->copy(Ljava/io/File;Ljava/io/File;)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->getApiVersion()I

    move-result v2

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    .line 213
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setReadable(ZZ)Z

    .line 215
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public clear()V
    .locals 6

    .prologue
    .line 229
    iget-object v1, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v2, 0x2

    new-array v2, v2, [J

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->getContentDownloadId()J

    move-result-wide v4

    aput-wide v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->getMetadataDownloadId()J

    move-result-wide v4

    aput-wide v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager;->remove([J)I

    .line 230
    iget-object v1, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mStorage:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 231
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "CONTENT_DOWNLOAD_ID"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 232
    const-string v1, "METADATA_DOWNLOAD_ID"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 233
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 234
    return-void
.end method

.method public deleteLocalDownloads()V
    .locals 6

    .prologue
    .line 219
    iget-object v5, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadDir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 220
    .local v2, "downloads":[Ljava/io/File;
    if-nez v2, :cond_1

    .line 226
    :cond_0
    return-void

    .line 223
    :cond_1
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 224
    .local v1, "download":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 223
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public downloadWasSuccessful(Landroid/content/Intent;)Z
    .locals 11
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 187
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 188
    .local v4, "extras":Landroid/os/Bundle;
    const-string v8, "extra_download_id"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 189
    .local v2, "downloadId":J
    iget-object v8, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadManager:Landroid/app/DownloadManager;

    new-instance v9, Landroid/app/DownloadManager$Query;

    invoke-direct {v9}, Landroid/app/DownloadManager$Query;-><init>()V

    new-array v10, v6, [J

    aput-wide v2, v10, v7

    invoke-virtual {v9, v10}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 190
    .local v1, "cur":Landroid/database/Cursor;
    if-nez v1, :cond_0

    .line 191
    const-string v6, "ConfigUpdaterDownloadManagerHelper"

    const-string v8, "Could not query DownloadManager"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :goto_0
    return v7

    .line 194
    :cond_0
    const/4 v5, 0x0

    .line 196
    .local v5, "wasSuccessful":Z
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 197
    const-string v8, "status"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 198
    .local v0, "column":I
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_1

    move v5, v6

    .line 203
    .end local v0    # "column":I
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v7, v5

    .line 205
    goto :goto_0

    .restart local v0    # "column":I
    :cond_1
    move v5, v7

    .line 198
    goto :goto_1

    .line 200
    .end local v0    # "column":I
    :cond_2
    :try_start_1
    const-string v6, "ConfigUpdaterDownloadManagerHelper"

    const-string v7, "Could not get current download status, assuming failure..."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 203
    :catchall_0
    move-exception v6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v6
.end method

.method protected enqueue(Ljava/net/URL;)J
    .locals 4
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 126
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 127
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/app/DownloadManager$Request;

    invoke-direct {v0, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 128
    .local v0, "request":Landroid/app/DownloadManager$Request;
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 129
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    .line 130
    iget-object v2, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v2, v0}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v2

    return-wide v2
.end method

.method public enqueueContent(Ljava/net/URL;)V
    .locals 2
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lcom/google/android/configupdater/DownloadManagerHelper;->enqueue(Ljava/net/URL;)J

    move-result-wide v0

    .line 135
    .local v0, "downloadId":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setContentDownloadId(J)V

    .line 136
    return-void
.end method

.method public enqueueMetadata(Ljava/net/URL;)V
    .locals 2
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lcom/google/android/configupdater/DownloadManagerHelper;->enqueue(Ljava/net/URL;)J

    move-result-wide v0

    .line 140
    .local v0, "downloadId":J
    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setMetadataDownloadId(J)V

    .line 141
    return-void
.end method

.method protected getApiVersion()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 238
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public getContentDownloadId()J
    .locals 4

    .prologue
    .line 103
    :try_start_0
    const-string v1, "CONTENT_DOWNLOAD_ID"

    invoke-virtual {p0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 105
    :goto_0
    return-wide v2

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "ignored":Ljava/lang/NumberFormatException;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getContentUrl()Ljava/net/URL;
    .locals 3

    .prologue
    .line 79
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v2, "CONTENT_URL"

    invoke-virtual {p0, v2}, Lcom/google/android/configupdater/DownloadManagerHelper;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    return-object v1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/net/MalformedURLException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLocalPathOrNull(Landroid/content/Intent;)Ljava/net/URL;
    .locals 12
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 156
    invoke-virtual {p0, p1}, Lcom/google/android/configupdater/DownloadManagerHelper;->downloadWasSuccessful(Landroid/content/Intent;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-object v7

    .line 159
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 160
    .local v4, "extras":Landroid/os/Bundle;
    const-string v8, "extra_download_id"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 161
    .local v2, "downloadId":J
    iget-object v8, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadManager:Landroid/app/DownloadManager;

    new-instance v9, Landroid/app/DownloadManager$Query;

    invoke-direct {v9}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v10, 0x1

    new-array v10, v10, [J

    const/4 v11, 0x0

    aput-wide v2, v10, v11

    invoke-virtual {v9, v10}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 162
    .local v0, "cur":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 165
    const-string v8, "local_filename"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 166
    .local v5, "filenameColumn":I
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 167
    .local v6, "localPath":Ljava/lang/String;
    if-eqz v6, :cond_0

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 170
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 173
    :try_start_0
    invoke-direct {p0, v6, v2, v3}, Lcom/google/android/configupdater/DownloadManagerHelper;->copyLocally(Ljava/lang/String;J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 180
    :try_start_1
    new-instance v8, Ljava/net/URL;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "file://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v7, v8

    goto :goto_0

    .line 174
    :catch_0
    move-exception v1

    .line 175
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "ConfigUpdaterDownloadManagerHelper"

    const-string v9, "Could not copy file from cache"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 181
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 182
    .local v1, "e":Ljava/net/MalformedURLException;
    goto :goto_0
.end method

.method public getMetadataDownloadId()J
    .locals 4

    .prologue
    .line 115
    :try_start_0
    const-string v1, "METADATA_DOWNLOAD_ID"

    invoke-virtual {p0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 117
    :goto_0
    return-wide v2

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "ignored":Ljava/lang/NumberFormatException;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getMetadataUrl()Ljava/net/URL;
    .locals 3

    .prologue
    .line 91
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v2, "METADATA_URL"

    invoke-virtual {p0, v2}, Lcom/google/android/configupdater/DownloadManagerHelper;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-object v1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/net/MalformedURLException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mStorage:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isContentDownloadCompleteIntent(Landroid/content/Intent;)Z
    .locals 6
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 145
    .local v2, "extras":Landroid/os/Bundle;
    const-string v3, "extra_download_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 146
    .local v0, "downloadId":J
    invoke-virtual {p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->getContentDownloadId()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isMetadataDownloadCompleteIntent(Landroid/content/Intent;)Z
    .locals 6
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 151
    .local v2, "extras":Landroid/os/Bundle;
    const-string v3, "extra_download_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 152
    .local v0, "downloadId":J
    invoke-virtual {p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->getMetadataDownloadId()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public setContentDownloadId(J)V
    .locals 3
    .param p1, "downloadId"    # J

    .prologue
    .line 110
    const-string v0, "CONTENT_DOWNLOAD_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public setContentUrl(Ljava/net/URL;)V
    .locals 2
    .param p1, "value"    # Ljava/net/URL;

    .prologue
    .line 86
    const-string v0, "CONTENT_URL"

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public setMetadataDownloadId(J)V
    .locals 3
    .param p1, "downloadId"    # J

    .prologue
    .line 122
    const-string v0, "METADATA_DOWNLOAD_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method public setMetadataUrl(Ljava/net/URL;)V
    .locals 2
    .param p1, "value"    # Ljava/net/URL;

    .prologue
    .line 98
    const-string v0, "METADATA_URL"

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method protected setValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mStorage:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 75
    return-void
.end method

.method public setup(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mPrefsName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mStorage:Landroid/content/SharedPreferences;

    .line 59
    invoke-virtual {p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->getApiVersion()I

    move-result v0

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 60
    const-string v0, "downloads"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadDir:Ljava/io/File;

    .line 66
    :goto_0
    const-string v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadManager:Landroid/app/DownloadManager;

    .line 67
    return-void

    .line 62
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "downloads"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadDir:Ljava/io/File;

    .line 64
    iget-object v0, p0, Lcom/google/android/configupdater/DownloadManagerHelper;->mDownloadDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    goto :goto_0
.end method
