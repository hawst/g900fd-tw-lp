.class public abstract Lcom/google/android/configupdater/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field public static final EXTRA_CONTENT_PATH:Ljava/lang/String; = "CONTENT_DOWNLOAD_PATH"

.field public static final EXTRA_CONTENT_URL:Ljava/lang/String; = "CONTENT_URL"

.field public static final EXTRA_METADATA_PATH:Ljava/lang/String; = "METADATA_DOWNLOAD_PATH"

.field public static final EXTRA_METADATA_URL:Ljava/lang/String; = "METADATA_URL"

.field public static final INSTALL_EXTRA_CONTENT_PATH:Ljava/lang/String; = "CONTENT_PATH"

.field public static final INSTALL_EXTRA_REQUIRED_HASH:Ljava/lang/String; = "REQUIRED_HASH"

.field public static final INSTALL_EXTRA_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field public static final INSTALL_EXTRA_VERSION_NUMBER:Ljava/lang/String; = "VERSION"

.field public static final MAX_DELAY:J = 0x1499700L

.field public static final METADATA_REQUIRED_HASH_PREFIX:Ljava/lang/String; = "REQUIRED_HASH:"

.field public static final METADATA_SIGNATURE_PREFIX:Ljava/lang/String; = "SIGNATURE:"

.field public static final METADATA_VERSION_PREFIX:Ljava/lang/String; = "VERSION:"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getInstallAction()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNewContentAction()Ljava/lang/String;
.end method

.method public abstract getNewMetadataAction()Ljava/lang/String;
.end method

.method public abstract getStartUpdateAction()Ljava/lang/String;
.end method

.method public abstract getUpdateService()Ljava/lang/Class;
.end method

.method public abstract getUserUpdateAction()Ljava/lang/String;
.end method
