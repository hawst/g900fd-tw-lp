.class public Lcom/google/android/configupdater/SmsShortCodes/SmsShortCodesUpdateRequestReceiver;
.super Lcom/google/android/configupdater/UpdateRequestReceiver;
.source "SmsShortCodesUpdateRequestReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/configupdater/SmsShortCodes/SmsShortCodesConfig;

    invoke-direct {v0}, Lcom/google/android/configupdater/SmsShortCodes/SmsShortCodesConfig;-><init>()V

    new-instance v1, Lcom/google/android/configupdater/StoredState;

    const-string v2, "SmsShortCodesState"

    invoke-direct {v1, v2}, Lcom/google/android/configupdater/StoredState;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/configupdater/DownloadManagerHelper;

    const-string v3, "SmsShortCodesDownload"

    invoke-direct {v2, v3}, Lcom/google/android/configupdater/DownloadManagerHelper;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/configupdater/GservicesHelper;

    const-string v4, "sms_short_codes"

    invoke-direct {v3, v4}, Lcom/google/android/configupdater/GservicesHelper;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/configupdater/UpdateRequestReceiver;-><init>(Lcom/google/android/configupdater/Config;Lcom/google/android/configupdater/StoredState;Lcom/google/android/configupdater/DownloadManagerHelper;Lcom/google/android/configupdater/GservicesHelper;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/configupdater/SmsShortCodes/SmsShortCodesConfig;Lcom/google/android/configupdater/StoredState;Lcom/google/android/configupdater/DownloadManagerHelper;Lcom/google/android/configupdater/GservicesHelper;)V
    .locals 0
    .param p1, "c"    # Lcom/google/android/configupdater/SmsShortCodes/SmsShortCodesConfig;
    .param p2, "s"    # Lcom/google/android/configupdater/StoredState;
    .param p3, "dm"    # Lcom/google/android/configupdater/DownloadManagerHelper;
    .param p4, "gs"    # Lcom/google/android/configupdater/GservicesHelper;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/configupdater/UpdateRequestReceiver;-><init>(Lcom/google/android/configupdater/Config;Lcom/google/android/configupdater/StoredState;Lcom/google/android/configupdater/DownloadManagerHelper;Lcom/google/android/configupdater/GservicesHelper;)V

    .line 41
    return-void
.end method
