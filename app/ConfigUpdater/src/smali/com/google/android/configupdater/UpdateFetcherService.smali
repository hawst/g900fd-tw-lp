.class public abstract Lcom/google/android/configupdater/UpdateFetcherService;
.super Landroid/app/IntentService;
.source "UpdateFetcherService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UpdateFetcherService"

.field public static apiVersion:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/google/android/configupdater/UpdateFetcherService;->apiVersion:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "UpdateFetcherService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method private cleanup()V
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/configupdater/StoredState;->clear()V

    .line 371
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/configupdater/DownloadManagerHelper;->clear()V

    .line 372
    return-void
.end method

.method private doneInstall()V
    .locals 2

    .prologue
    .line 362
    const-string v0, "UpdateFetcherService"

    const-string v1, "doneInstall"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    .line 364
    return-void
.end method

.method private getContentPathFromIntent(Landroid/content/Intent;)Ljava/net/URL;
    .locals 1
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v0, "CONTENT_DOWNLOAD_PATH"

    invoke-direct {p0, p1, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->getUrlFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method private getContentUrlFromIntent(Landroid/content/Intent;)Ljava/net/URL;
    .locals 1
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v0, "CONTENT_URL"

    invoke-direct {p0, p1, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->getUrlFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method private getMetadataPathFromIntent(Landroid/content/Intent;)Ljava/net/URL;
    .locals 1
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v0, "METADATA_DOWNLOAD_PATH"

    invoke-direct {p0, p1, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->getUrlFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method private getMetadataUrlFromIntent(Landroid/content/Intent;)Ljava/net/URL;
    .locals 1
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v0, "METADATA_URL"

    invoke-direct {p0, p1, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->getUrlFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method private getUrlFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/net/URL;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 397
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 399
    .local v1, "extras":Landroid/os/Bundle;
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-virtual {v1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    :goto_0
    return-object v2

    .line 400
    :catch_0
    move-exception v0

    .line 401
    .local v0, "e":Ljava/net/MalformedURLException;
    const-string v2, "UpdateFetcherService"

    const-string v3, "Got malformed URL"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private handleCompletedContentDownload(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/configupdater/StoredState;->setAlternativeContentPath(Ljava/lang/String;)V

    .line 311
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateFetcherService;->startInstall(Ljava/lang/String;)V

    .line 312
    return-void
.end method

.method private handleCompletedMetadataDownload(Ljava/lang/String;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 197
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateFetcherService;->parseUpdate(Ljava/lang/String;)V

    .line 198
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->startContentDownload()V
    :try_end_0
    .catch Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
    const-string v1, "UpdateFetcherService"

    const-string v2, "Could not parse update"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 201
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    goto :goto_0
.end method

.method private parseRequiredHash(Ljava/lang/String;)V
    .locals 4
    .param p1, "hashLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v2, "REQUIRED_HASH:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 258
    new-instance v2, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v3, "Cannot parse invalid required hash"

    invoke-direct {v2, v3}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 260
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 261
    .local v1, "hashLen":I
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v2, "REQUIRED_HASH:"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "hash":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/configupdater/StoredState;->setAlternativeRequiredHash(Ljava/lang/String;)V

    .line 264
    return-void
.end method

.method private parseSignature(Ljava/lang/String;)V
    .locals 5
    .param p1, "signatureLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v3, "SIGNATURE:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 232
    new-instance v3, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v4, "Cannot parse invalid signature"

    invoke-direct {v3, v4}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 234
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 235
    .local v2, "sigLineLen":I
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v3, "SIGNATURE:"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 236
    .local v0, "prefixLen":I
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "sig":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/configupdater/StoredState;->setAlternativeSignature(Ljava/lang/String;)V

    .line 238
    return-void
.end method

.method private parseUpdate(Ljava/lang/String;)V
    .locals 8
    .param p1, "metadataFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
        }
    .end annotation

    .prologue
    .line 211
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v6, v7}, Lcom/google/common/io/Files;->toString(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    .local v0, "alternativeMetadata":Ljava/lang/String;
    const-string v6, "\n"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 216
    .local v3, "lines":[Ljava/lang/String;
    array-length v6, v3

    const/4 v7, 0x3

    if-ge v6, v7, :cond_0

    .line 217
    new-instance v6, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v7, "Metadata is not correctly formatted"

    invoke-direct {v6, v7}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 212
    .end local v0    # "alternativeMetadata":Ljava/lang/String;
    .end local v3    # "lines":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/io/IOException;
    new-instance v6, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v7, "Could not read alternative metadata"

    invoke-direct {v6, v7, v1}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v6

    .line 219
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "alternativeMetadata":Ljava/lang/String;
    .restart local v3    # "lines":[Ljava/lang/String;
    :cond_0
    const/4 v6, 0x0

    aget-object v4, v3, v6

    .line 220
    .local v4, "signatureLine":Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v5, v3, v6

    .line 221
    .local v5, "versionLine":Ljava/lang/String;
    const/4 v6, 0x2

    aget-object v2, v3, v6

    .line 222
    .local v2, "hashLine":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/google/android/configupdater/UpdateFetcherService;->parseSignature(Ljava/lang/String;)V

    .line 223
    invoke-direct {p0, v5}, Lcom/google/android/configupdater/UpdateFetcherService;->parseVersion(Ljava/lang/String;)V

    .line 224
    invoke-direct {p0, v2}, Lcom/google/android/configupdater/UpdateFetcherService;->parseRequiredHash(Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method private parseVersion(Ljava/lang/String;)V
    .locals 4
    .param p1, "versionLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
        }
    .end annotation

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v2, "VERSION:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 245
    new-instance v2, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v3, "Cannot parse invalid version"

    invoke-direct {v2, v3}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v2, "VERSION:"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 248
    .local v1, "prefixLen":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "altVersion":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->validateVersion(Ljava/lang/String;)V

    .line 250
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/configupdater/StoredState;->setAlternativeVersionNumber(Ljava/lang/String;)V

    .line 251
    return-void
.end method

.method public static resetForTest()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 408
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/google/android/configupdater/UpdateFetcherService;->apiVersion:I

    .line 409
    return-void
.end method

.method private startContentDownload()V
    .locals 3

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/configupdater/StoredState;->getAlternativeContentUrl()Ljava/net/URL;

    move-result-object v0

    .line 291
    .local v0, "url":Ljava/net/URL;
    if-nez v0, :cond_0

    .line 292
    const-string v1, "UpdateFetcherService"

    const-string v2, "Don\'t have a valid content url!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    .line 299
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->urlIsFile(Ljava/net/URL;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {v0}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/configupdater/UpdateFetcherService;->handleCompletedContentDownload(Ljava/lang/String;)V

    goto :goto_0

    .line 297
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/configupdater/DownloadManagerHelper;->enqueueContent(Ljava/net/URL;)V

    goto :goto_0
.end method

.method private startInstall(Ljava/lang/String;)V
    .locals 9
    .param p1, "altContentPath"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x15

    .line 318
    const-string v6, "UpdateFetcherService"

    const-string v7, "Update downloaded, starting installation"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    if-nez p1, :cond_0

    .line 320
    const-string v6, "UpdateFetcherService"

    const-string v7, "Content path == null!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    .line 359
    :goto_0
    return-void

    .line 324
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/configupdater/StoredState;->getAlternativeSignature()Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, "altSignature":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 326
    const-string v6, "UpdateFetcherService"

    const-string v7, "Signature == null!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    goto :goto_0

    .line 330
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/configupdater/StoredState;->getAlternativeVersionNumber()Ljava/lang/String;

    move-result-object v2

    .line 331
    .local v2, "altVersion":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 332
    const-string v6, "UpdateFetcherService"

    const-string v7, "Version == null!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    goto :goto_0

    .line 336
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/configupdater/StoredState;->getAlternativeRequiredHash()Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "altRequiredHash":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 338
    const-string v6, "UpdateFetcherService"

    const-string v7, "Required hash == null!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    goto :goto_0

    .line 342
    :cond_3
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 343
    .local v4, "installIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/configupdater/Config;->getInstallAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v6, "android"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    sget v6, Lcom/google/android/configupdater/UpdateFetcherService;->apiVersion:I

    if-ge v6, v8, :cond_4

    .line 346
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v6, "CONTENT_PATH"

    invoke-virtual {v4, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v6, "SIGNATURE"

    invoke-virtual {v4, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v6, "VERSION"

    invoke-virtual {v4, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    const-string v6, "REQUIRED_HASH"

    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    sget v6, Lcom/google/android/configupdater/UpdateFetcherService;->apiVersion:I

    if-lt v6, v8, :cond_5

    .line 352
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 353
    .local v3, "f":Ljava/io/File;
    const-string v6, "com.google.android.configupdater.fileprovider"

    invoke-static {p0, v6, v3}, Landroid/support/v4/content/FileProvider;->getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 354
    .local v5, "uri":Landroid/net/Uri;
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 355
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 357
    .end local v3    # "f":Ljava/io/File;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_5
    invoke-virtual {p0, v4}, Lcom/google/android/configupdater/UpdateFetcherService;->sendBroadcast(Landroid/content/Intent;)V

    .line 358
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->doneInstall()V

    goto/16 :goto_0
.end method

.method private startMetadataDownload()V
    .locals 3

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/configupdater/StoredState;->getAlternativeMetadataUrl()Ljava/net/URL;

    move-result-object v0

    .line 171
    .local v0, "url":Ljava/net/URL;
    if-nez v0, :cond_0

    .line 172
    const-string v1, "UpdateFetcherService"

    const-string v2, "Don\'t have a valid metadata url!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    .line 179
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/configupdater/UpdateFetcherService;->urlIsFile(Ljava/net/URL;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {v0}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/configupdater/UpdateFetcherService;->handleCompletedMetadataDownload(Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/configupdater/DownloadManagerHelper;->enqueueMetadata(Ljava/net/URL;)V

    goto :goto_0
.end method

.method private startUpdate(Ljava/net/URL;Ljava/net/URL;)V
    .locals 4
    .param p1, "contentUrl"    # Ljava/net/URL;
    .param p2, "metadataUrl"    # Ljava/net/URL;

    .prologue
    .line 147
    const-string v0, "UpdateFetcherService"

    const-string v1, "Update started"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/configupdater/DownloadManagerHelper;->deleteLocalDownloads()V

    .line 150
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->cleanup()V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/configupdater/StoredState;->setUpdateTimeStarted(J)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/configupdater/StoredState;->setAlternativeContentUrl(Ljava/net/URL;)V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/configupdater/StoredState;->setAlternativeMetadataUrl(Ljava/net/URL;)V

    .line 154
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->startMetadataDownload()V

    .line 155
    return-void
.end method

.method private urlIsFile(Ljava/net/URL;)Z
    .locals 2
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 377
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private validateVersion(Ljava/lang/String;)V
    .locals 3
    .param p1, "version"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;
        }
    .end annotation

    .prologue
    .line 271
    const/16 v1, 0xa

    :try_start_0
    invoke-static {p1, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    if-gez v1, :cond_0

    .line 272
    new-instance v1, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v2, "Version is negative"

    invoke-direct {v1, v2}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;

    const-string v2, "Version is not a valid number"

    invoke-direct {v1, v2, v0}, Lcom/google/android/configupdater/UpdateFetcherService$MetadataVerificationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 277
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract getConfig()Lcom/google/android/configupdater/Config;
.end method

.method public abstract getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;
.end method

.method public abstract getState()Lcom/google/android/configupdater/StoredState;
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->setup()V

    .line 106
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 107
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/configupdater/Config;->getStartUpdateAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateFetcherService;->getMetadataUrlFromIntent(Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v4

    .line 109
    .local v4, "metadataUrl":Ljava/net/URL;
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateFetcherService;->getContentUrlFromIntent(Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v2

    .line 110
    .local v2, "contentUrl":Ljava/net/URL;
    if-eqz v4, :cond_0

    if-nez v2, :cond_1

    .line 111
    :cond_0
    const-string v5, "UpdateFetcherService"

    const-string v6, "New update does not have both metadata and content URLs, ignoring"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    .end local v2    # "contentUrl":Ljava/net/URL;
    .end local v4    # "metadataUrl":Ljava/net/URL;
    :goto_0
    return-void

    .line 114
    .restart local v2    # "contentUrl":Ljava/net/URL;
    .restart local v4    # "metadataUrl":Ljava/net/URL;
    :cond_1
    invoke-direct {p0, v2, v4}, Lcom/google/android/configupdater/UpdateFetcherService;->startUpdate(Ljava/net/URL;Ljava/net/URL;)V

    goto :goto_0

    .line 115
    .end local v2    # "contentUrl":Ljava/net/URL;
    .end local v4    # "metadataUrl":Ljava/net/URL;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/configupdater/Config;->getNewMetadataAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateFetcherService;->getMetadataPathFromIntent(Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v3

    .line 117
    .local v3, "metadataPath":Ljava/net/URL;
    if-nez v3, :cond_3

    .line 118
    const-string v5, "UpdateFetcherService"

    const-string v6, "Update does not have a metadata path, ignoring"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    :cond_3
    invoke-virtual {v3}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/configupdater/UpdateFetcherService;->handleCompletedMetadataDownload(Ljava/lang/String;)V

    goto :goto_0

    .line 122
    .end local v3    # "metadataPath":Ljava/net/URL;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getConfig()Lcom/google/android/configupdater/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/configupdater/Config;->getNewContentAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/configupdater/UpdateFetcherService;->getContentPathFromIntent(Landroid/content/Intent;)Ljava/net/URL;

    move-result-object v1

    .line 124
    .local v1, "contentPath":Ljava/net/URL;
    if-nez v1, :cond_5

    .line 125
    const-string v5, "UpdateFetcherService"

    const-string v6, "Update does not have a content path, ignoring"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 128
    :cond_5
    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/configupdater/UpdateFetcherService;->handleCompletedContentDownload(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    .end local v1    # "contentPath":Ljava/net/URL;
    :cond_6
    const-string v5, "UpdateFetcherService"

    const-string v6, "Got invalid intent"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected setup()V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getState()Lcom/google/android/configupdater/StoredState;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/configupdater/StoredState;->setup(Landroid/content/Context;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/configupdater/UpdateFetcherService;->getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/configupdater/DownloadManagerHelper;->setup(Landroid/content/Context;)V

    .line 65
    return-void
.end method
