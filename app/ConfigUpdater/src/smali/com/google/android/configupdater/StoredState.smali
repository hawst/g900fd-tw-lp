.class public Lcom/google/android/configupdater/StoredState;
.super Ljava/lang/Object;
.source "StoredState.java"


# static fields
.field public static final ALT_CONTENT_PATH_KEY:Ljava/lang/String; = "ALT_CONTENT_PATH"

.field public static final ALT_CONTENT_URL_KEY:Ljava/lang/String; = "ALT_CONTENT_URL"

.field public static final ALT_METADATA_URL_KEY:Ljava/lang/String; = "ALT_METADATA_URL"

.field public static final ALT_REQUIRED_HASH_KEY:Ljava/lang/String; = "ALT_REQUIRED_HASH"

.field public static final ALT_SIGNATURE_KEY:Ljava/lang/String; = "ALT_SIGNATURE"

.field public static final ALT_UPDATE_TIME_STARTED_KEY:Ljava/lang/String; = "UPDATE_TIME_STARTED"

.field public static final ALT_VERSION_NUMBER_KEY:Ljava/lang/String; = "ALT_VERSION_NUMBER"


# instance fields
.field public final mPrefsName:Ljava/lang/String;

.field protected mStoredState:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefsName"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/configupdater/StoredState;->mPrefsName:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method protected clear()V
    .locals 2

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/configupdater/StoredState;->mStoredState:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 49
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "ALT_CONTENT_URL"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 50
    const-string v1, "ALT_METADATA_URL"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 51
    const-string v1, "ALT_CONTENT_PATH"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 52
    const-string v1, "ALT_SIGNATURE"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 53
    const-string v1, "ALT_VERSION_NUMBER"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 54
    const-string v1, "ALT_REQUIRED_HASH"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 55
    const-string v1, "UPDATE_TIME_STARTED"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 56
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 57
    return-void
.end method

.method public getAlternativeContentPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const-string v0, "ALT_CONTENT_PATH"

    invoke-virtual {p0, v0}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlternativeContentUrl()Ljava/net/URL;
    .locals 3

    .prologue
    .line 69
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v2, "ALT_CONTENT_URL"

    invoke-virtual {p0, v2}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-object v1

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/net/MalformedURLException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAlternativeMetadataUrl()Ljava/net/URL;
    .locals 3

    .prologue
    .line 81
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v2, "ALT_METADATA_URL"

    invoke-virtual {p0, v2}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-object v1

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/net/MalformedURLException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAlternativeRequiredHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string v0, "ALT_REQUIRED_HASH"

    invoke-virtual {p0, v0}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlternativeSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    const-string v0, "ALT_SIGNATURE"

    invoke-virtual {p0, v0}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlternativeVersionNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "ALT_VERSION_NUMBER"

    invoke-virtual {p0, v0}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateTimeStarted()J
    .locals 4

    .prologue
    .line 125
    :try_start_0
    const-string v1, "UPDATE_TIME_STARTED"

    invoke-virtual {p0, v1}, Lcom/google/android/configupdater/StoredState;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 127
    :goto_0
    return-wide v2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "ignored":Ljava/lang/NumberFormatException;
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method protected getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/configupdater/StoredState;->mStoredState:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAlternativeContentPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 96
    const-string v0, "ALT_CONTENT_PATH"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public setAlternativeContentUrl(Ljava/net/URL;)V
    .locals 2
    .param p1, "value"    # Ljava/net/URL;

    .prologue
    .line 76
    const-string v0, "ALT_CONTENT_URL"

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public setAlternativeMetadataUrl(Ljava/net/URL;)V
    .locals 2
    .param p1, "value"    # Ljava/net/URL;

    .prologue
    .line 88
    const-string v0, "ALT_METADATA_URL"

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public setAlternativeRequiredHash(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 120
    const-string v0, "ALT_REQUIRED_HASH"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public setAlternativeSignature(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 104
    const-string v0, "ALT_SIGNATURE"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public setAlternativeVersionNumber(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v0, "ALT_VERSION_NUMBER"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public setUpdateTimeStarted(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 132
    const-string v0, "UPDATE_TIME_STARTED"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/configupdater/StoredState;->setValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method protected setValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/configupdater/StoredState;->mStoredState:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 65
    return-void
.end method

.method public setup(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/configupdater/StoredState;->mPrefsName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/configupdater/StoredState;->mStoredState:Landroid/content/SharedPreferences;

    .line 45
    return-void
.end method
