.class public Lcom/google/android/configupdater/SELinux/SELinuxConfig;
.super Lcom/google/android/configupdater/Config;
.source "SELinuxConfig.java"


# static fields
.field public static final downloadName:Ljava/lang/String; = "SELinuxDownload"

.field public static final gservicesName:Ljava/lang/String; = "selinux"

.field public static final stateName:Ljava/lang/String; = "SELinuxState"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/configupdater/Config;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstallAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "android.intent.action.UPDATE_SEPOLICY"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const-string v0, "SELinux"

    return-object v0
.end method

.method public getNewContentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "com.google.android.configupdater.SELinux.NEW_CONTENT"

    return-object v0
.end method

.method public getNewMetadataAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "com.google.android.configupdater.SELinux.NEW_METADATA"

    return-object v0
.end method

.method public getStartUpdateAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.google.android.configupdater.SELinux.START"

    return-object v0
.end method

.method public getUpdateService()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/configupdater/SELinux/SELinuxUpdateFetcherService;

    return-object v0
.end method

.method public getUserUpdateAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "com.google.android.configupdater.SELinux.UPDATE_SEPOLICY"

    return-object v0
.end method
