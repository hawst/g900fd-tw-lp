.class public Lcom/google/android/configupdater/CertPin/CertPinConfig;
.super Lcom/google/android/configupdater/Config;
.source "CertPinConfig.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/configupdater/Config;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstallAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "android.intent.action.UPDATE_PINS"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const-string v0, "CertPin"

    return-object v0
.end method

.method public getNewContentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "com.google.android.configupdater.CertPin.NEW_CONTENT"

    return-object v0
.end method

.method public getNewMetadataAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string v0, "com.google.android.configupdater.CertPin.NEW_METADATA"

    return-object v0
.end method

.method public getStartUpdateAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "com.google.android.configupdater.CertPin.START"

    return-object v0
.end method

.method public getUpdateService()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;

    return-object v0
.end method

.method public getUserUpdateAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string v0, "com.google.android.configupdater.CertPin.UPDATE_CERT_PINS"

    return-object v0
.end method
