.class public Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;
.super Lcom/google/android/configupdater/UpdateFetcherService;
.source "CertPinUpdateFetcherService.java"


# static fields
.field public static mConfig:Lcom/google/android/configupdater/Config;

.field public static mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

.field public static mState:Lcom/google/android/configupdater/StoredState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/configupdater/CertPin/CertPinConfig;

    invoke-direct {v0}, Lcom/google/android/configupdater/CertPin/CertPinConfig;-><init>()V

    sput-object v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;->mConfig:Lcom/google/android/configupdater/Config;

    .line 28
    new-instance v0, Lcom/google/android/configupdater/StoredState;

    const-string v1, "CertPinState"

    invoke-direct {v0, v1}, Lcom/google/android/configupdater/StoredState;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;->mState:Lcom/google/android/configupdater/StoredState;

    .line 29
    new-instance v0, Lcom/google/android/configupdater/DownloadManagerHelper;

    const-string v1, "CertPinDownloads"

    invoke-direct {v0, v1}, Lcom/google/android/configupdater/DownloadManagerHelper;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/configupdater/UpdateFetcherService;-><init>()V

    return-void
.end method


# virtual methods
.method public getConfig()Lcom/google/android/configupdater/Config;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;->mConfig:Lcom/google/android/configupdater/Config;

    return-object v0
.end method

.method public getDownloader()Lcom/google/android/configupdater/DownloadManagerHelper;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;->mDownloader:Lcom/google/android/configupdater/DownloadManagerHelper;

    return-object v0
.end method

.method public getState()Lcom/google/android/configupdater/StoredState;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/configupdater/CertPin/CertPinUpdateFetcherService;->mState:Lcom/google/android/configupdater/StoredState;

    return-object v0
.end method
