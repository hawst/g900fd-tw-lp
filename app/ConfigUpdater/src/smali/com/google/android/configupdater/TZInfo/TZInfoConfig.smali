.class public Lcom/google/android/configupdater/TZInfo/TZInfoConfig;
.super Lcom/google/android/configupdater/Config;
.source "TZInfoConfig.java"


# static fields
.field public static final downloadName:Ljava/lang/String; = "TZInfoDownload"

.field public static final gservicesName:Ljava/lang/String; = "tzinfo"

.field public static final stateName:Ljava/lang/String; = "TZInfoState"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/configupdater/Config;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstallAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "android.intent.action.UPDATE_TZINFO"

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const-string v0, "TZInfo"

    return-object v0
.end method

.method public getNewContentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "com.google.android.configupdater.TZInfo.NEW_CONTENT"

    return-object v0
.end method

.method public getNewMetadataAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "com.google.android.configupdater.TZInfo.NEW_METADATA"

    return-object v0
.end method

.method public getStartUpdateAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.google.android.configupdater.TZInfo.START"

    return-object v0
.end method

.method public getUpdateService()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/configupdater/TZInfo/TZInfoUpdateFetcherService;

    return-object v0
.end method

.method public getUserUpdateAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "com.google.android.configupdater.TZInfo.UPDATE_TZINFO"

    return-object v0
.end method
