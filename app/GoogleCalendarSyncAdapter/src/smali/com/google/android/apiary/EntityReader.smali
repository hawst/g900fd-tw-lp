.class public Lcom/google/android/apiary/EntityReader;
.super Ljava/lang/Object;
.source "EntityReader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apiary/EntityReader$EntityItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final mEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mEntityQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final mEntryEndMarker:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mEventQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile mForcedClosed:Z

.field private final mHandler:Lcom/google/android/apiary/ItemAndEntityHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apiary/ItemAndEntityHandler",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mIdQueryColumn:Ljava/lang/String;

.field private final mLogTag:Ljava/lang/String;

.field private final mNumRemoteExceptions:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mSelection:Ljava/lang/String;

.field private volatile mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/apiary/EntityReader$EntityItem;Lcom/google/android/apiary/ItemAndEntityHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "logTag"    # Ljava/lang/String;
    .param p7, "idColumn"    # Ljava/lang/String;
    .param p8, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;TT;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<TT;>;>;",
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<TT;>;",
            "Lcom/google/android/apiary/ItemAndEntityHandler",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<TT;>;"
    .local p2, "eventQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<TT;>;"
    .local p3, "entryEndMarker":Ljava/lang/Object;, "TT;"
    .local p4, "entityQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/apiary/EntityReader$EntityItem<TT;>;>;"
    .local p5, "entityEndMarker":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<TT;>;"
    .local p6, "handler":Lcom/google/android/apiary/ItemAndEntityHandler;, "Lcom/google/android/apiary/ItemAndEntityHandler<TT;>;"
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apiary/EntityReader;->mNumRemoteExceptions:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 66
    iput-object p1, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/google/android/apiary/EntityReader;->mEventQueue:Ljava/util/concurrent/BlockingQueue;

    .line 68
    iput-object p4, p0, Lcom/google/android/apiary/EntityReader;->mEntityQueue:Ljava/util/concurrent/BlockingQueue;

    .line 69
    iput-object p3, p0, Lcom/google/android/apiary/EntityReader;->mEntryEndMarker:Ljava/lang/Object;

    .line 70
    iput-object p5, p0, Lcom/google/android/apiary/EntityReader;->mEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;

    .line 71
    iput-object p6, p0, Lcom/google/android/apiary/EntityReader;->mHandler:Lcom/google/android/apiary/ItemAndEntityHandler;

    .line 72
    iput-object p8, p0, Lcom/google/android/apiary/EntityReader;->mSelection:Ljava/lang/String;

    .line 73
    iput-boolean v1, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    .line 74
    iput-object p7, p0, Lcom/google/android/apiary/EntityReader;->mIdQueryColumn:Ljava/lang/String;

    .line 75
    return-void
.end method

.method private readBatch(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 142
    .local p0, "this":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<TT;>;"
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v9, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/google/android/apiary/EntityReader;->mIdQueryColumn:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " in ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 143
    .local v7, "sb":Ljava/lang/StringBuilder;
    const-string v8, ""

    .line 144
    .local v8, "separator":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 145
    .local v3, "entry":Ljava/lang/Object;, "TT;"
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string v8, ","

    .line 147
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mHandler:Lcom/google/android/apiary/ItemAndEntityHandler;

    invoke-interface {v9, v3}, Lcom/google/android/apiary/ItemAndEntityHandler;->itemToSourceId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    .end local v3    # "entry":Ljava/lang/Object;, "TT;"
    :cond_0
    const-string v9, ")"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mSelection:Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 151
    const-string v9, " AND "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apiary/EntityReader;->mSelection:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 155
    .local v1, "entities":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/content/Entity;>;"
    :try_start_0
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 156
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "querying batch of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " entities for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_2
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mHandler:Lcom/google/android/apiary/ItemAndEntityHandler;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, -0x1

    invoke-interface {v9, v10, v11, v12}, Lcom/google/android/apiary/ItemAndEntityHandler;->newEntityIterator(Ljava/lang/String;[Ljava/lang/String;I)Landroid/content/EntityIterator;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 160
    .local v6, "iterator":Landroid/content/EntityIterator;
    :goto_1
    :try_start_1
    iget-boolean v9, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-nez v9, :cond_4

    invoke-interface {v6}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 161
    iget-boolean v9, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v9, :cond_3

    .line 173
    :try_start_2
    invoke-interface {v6}, Landroid/content/EntityIterator;->close()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_2 .. :try_end_2} :catch_1

    .line 180
    .end local v6    # "iterator":Landroid/content/EntityIterator;
    :goto_2
    return-void

    .line 164
    .restart local v6    # "iterator":Landroid/content/EntityIterator;
    :cond_3
    :try_start_3
    invoke-interface {v6}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Entity;

    .line 165
    .local v2, "entity":Landroid/content/Entity;
    invoke-virtual {v2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apiary/EntityReader;->mIdQueryColumn:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 166
    .local v5, "id":Ljava/lang/String;
    invoke-interface {v1, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 173
    .end local v2    # "entity":Landroid/content/Entity;
    .end local v5    # "id":Ljava/lang/String;
    :catchall_0
    move-exception v9

    :try_start_4
    invoke-interface {v6}, Landroid/content/EntityIterator;->close()V

    throw v9
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_4 .. :try_end_4} :catch_1

    .line 175
    .end local v6    # "iterator":Landroid/content/EntityIterator;
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mNumRemoteExceptions:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_2

    .line 168
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v6    # "iterator":Landroid/content/EntityIterator;
    :cond_4
    :try_start_5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 169
    .restart local v3    # "entry":Ljava/lang/Object;, "TT;"
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mHandler:Lcom/google/android/apiary/ItemAndEntityHandler;

    invoke-interface {v9, v3}, Lcom/google/android/apiary/ItemAndEntityHandler;->itemToSourceId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Entity;

    .line 170
    .restart local v2    # "entity":Landroid/content/Entity;
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mEntityQueue:Ljava/util/concurrent/BlockingQueue;

    new-instance v10, Lcom/google/android/apiary/EntityReader$EntityItem;

    invoke-direct {v10, v3, v2}, Lcom/google/android/apiary/EntityReader$EntityItem;-><init>(Ljava/lang/Object;Landroid/content/Entity;)V

    invoke-interface {v9, v10}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 173
    .end local v2    # "entity":Landroid/content/Entity;
    .end local v3    # "entry":Ljava/lang/Object;, "TT;"
    :cond_5
    :try_start_6
    invoke-interface {v6}, Landroid/content/EntityIterator;->close()V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 177
    .end local v6    # "iterator":Landroid/content/EntityIterator;
    :catch_1
    move-exception v0

    .line 178
    .local v0, "e":Lcom/google/android/apiary/ParseException;
    iget-object v9, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    const-string v10, "Error while reading batch"

    invoke-static {v9, v10, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private readEntities()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<TT;>;"
    const/4 v4, 0x2

    .line 101
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    const-string v3, "readEntities"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 105
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-nez v2, :cond_3

    .line 106
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mEventQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    .line 107
    .local v1, "entry":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mEntryEndMarker:Ljava/lang/Object;

    if-ne v1, v2, :cond_6

    .line 108
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    const-string v3, "read idAndEntry end marker from queue"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 112
    invoke-direct {p0, v0}, Lcom/google/android/apiary/EntityReader;->readBatch(Ljava/util/List;)V

    .line 113
    iget-boolean v2, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-eqz v2, :cond_4

    .line 131
    .end local v1    # "entry":Ljava/lang/Object;, "TT;"
    :cond_3
    :goto_1
    return-void

    .line 116
    .restart local v1    # "entry":Ljava/lang/Object;, "TT;"
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 118
    :cond_5
    iget-object v2, p0, Lcom/google/android/apiary/EntityReader;->mEntityQueue:Ljava/util/concurrent/BlockingQueue;

    iget-object v3, p0, Lcom/google/android/apiary/EntityReader;->mEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;

    invoke-interface {v2, v3}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    goto :goto_1

    .line 121
    :cond_6
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0xa

    if-lt v2, v3, :cond_1

    .line 125
    invoke-direct {p0, v0}, Lcom/google/android/apiary/EntityReader;->readBatch(Ljava/util/List;)V

    .line 126
    iget-boolean v2, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-nez v2, :cond_3

    .line 129
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 93
    .local p0, "this":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<TT;>;"
    iget-object v0, p0, Lcom/google/android/apiary/EntityReader;->mThread:Ljava/lang/Thread;

    .line 94
    .local v0, "thread":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 95
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    .line 96
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 98
    :cond_0
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<TT;>;"
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 78
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apiary/EntityReader;->mThread:Ljava/lang/Thread;

    .line 79
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 81
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apiary/EntityReader;->readEntities()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    iput-object v1, p0, Lcom/google/android/apiary/EntityReader;->mThread:Ljava/lang/Thread;

    .line 86
    iget-boolean v0, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EntityReader thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_1
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 85
    iput-object v1, p0, Lcom/google/android/apiary/EntityReader;->mThread:Ljava/lang/Thread;

    .line 86
    iget-boolean v0, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EntityReader thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/apiary/EntityReader;->mThread:Ljava/lang/Thread;

    .line 86
    iget-boolean v1, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 87
    :cond_3
    iget-object v1, p0, Lcom/google/android/apiary/EntityReader;->mLogTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EntityReader thread ended: mForcedClosed is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apiary/EntityReader;->mForcedClosed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v0
.end method
