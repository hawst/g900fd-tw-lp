.class public Lcom/google/android/apiary/GoogleRequestInitializer;
.super Ljava/lang/Object;
.source "GoogleRequestInitializer.java"

# interfaces
.implements Lcom/google/api/client/http/HttpExecuteInterceptor;
.implements Lcom/google/api/client/http/HttpRequestInitializer;
.implements Lcom/google/api/client/http/HttpUnsuccessfulResponseHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apiary/GoogleRequestInitializer$1;,
        Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAuthToken:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mLogTag:Ljava/lang/String;

.field private mRequestConnectTimeout:I

.field private mRequestReadTimeout:I

.field private final mScope:Ljava/lang/String;

.field private final mSyncAuthority:Ljava/lang/String;

.field private mUserAgentString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scope"    # Ljava/lang/String;
    .param p3, "logTag"    # Ljava/lang/String;
    .param p4, "syncAuthority"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    .line 46
    iput v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestConnectTimeout:I

    .line 47
    iput v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestReadTimeout:I

    .line 61
    iput-object p1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mScope:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mSyncAuthority:Ljava/lang/String;

    .line 65
    return-void
.end method


# virtual methods
.method public getAuthToken()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mSyncAuthority:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mScope:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mSyncAuthority:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getTokenWithNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1

    .line 155
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    return-object v0

    .line 146
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mScope:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getTokenWithNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 149
    :catch_0
    move-exception v6

    .line 150
    .local v6, "e":Ljava/io/IOException;
    new-instance v0, Lcom/google/android/apiary/AuthenticationException;

    const-string v1, "Could not get an auth token"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 151
    .end local v6    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 152
    .local v6, "e":Lcom/google/android/gms/auth/GoogleAuthException;
    new-instance v0, Lcom/google/android/apiary/AuthenticationException;

    const-string v1, "Could not get an auth token"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public handleResponse(Lcom/google/api/client/http/HttpRequest;Lcom/google/api/client/http/HttpResponse;Z)Z
    .locals 2
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "response"    # Lcom/google/api/client/http/HttpResponse;
    .param p3, "retrySupported"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p2}, Lcom/google/api/client/http/HttpResponse;->getStatusCode()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    const-string v1, "Got a 401. Invalidating token"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getNumberOfRetries()I

    move-result v0

    if-lez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    const-string v1, "Retrying request"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/GoogleAuthUtil;->invalidateToken(Landroid/content/Context;Ljava/lang/String;)V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    .line 169
    const/4 v0, 0x1

    .line 171
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Lcom/google/api/client/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;

    .prologue
    .line 74
    invoke-virtual {p1, p0}, Lcom/google/api/client/http/HttpRequest;->setInterceptor(Lcom/google/api/client/http/HttpExecuteInterceptor;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/api/client/http/HttpRequest;->setUnsuccessfulResponseHandler(Lcom/google/api/client/http/HttpUnsuccessfulResponseHandler;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpRequest;->setNumberOfRetries(I)Lcom/google/api/client/http/HttpRequest;

    .line 78
    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestConnectTimeout:I

    if-lez v0, :cond_0

    .line 79
    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestConnectTimeout:I

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setConnectTimeout(I)Lcom/google/api/client/http/HttpRequest;

    .line 82
    :cond_0
    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestReadTimeout:I

    if-lez v0, :cond_1

    .line 83
    iget v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mRequestReadTimeout:I

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setReadTimeout(I)Lcom/google/api/client/http/HttpRequest;

    .line 85
    :cond_1
    return-void
.end method

.method public intercept(Lcom/google/api/client/http/HttpRequest;)V
    .locals 13
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apiary/GoogleRequestInitializer;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "authToken":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v2

    .line 91
    .local v2, "headers":Lcom/google/api/client/http/HttpHeaders;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "OAuth "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/google/api/client/http/HttpHeaders;->setAuthorization(Ljava/lang/String;)Lcom/google/api/client/http/HttpHeaders;

    .line 92
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v7

    .line 95
    .local v7, "url":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v7}, Lcom/google/api/client/http/GenericUrl;->build()Ljava/lang/String;

    move-result-object v3

    .line 96
    .local v3, "original":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/common/http/UrlRules;->getRules(Landroid/content/ContentResolver;)Lcom/google/android/common/http/UrlRules;

    move-result-object v6

    .line 97
    .local v6, "rules":Lcom/google/android/common/http/UrlRules;
    invoke-virtual {v6, v3}, Lcom/google/android/common/http/UrlRules;->matchRule(Ljava/lang/String;)Lcom/google/android/common/http/UrlRules$Rule;

    move-result-object v5

    .line 98
    .local v5, "rule":Lcom/google/android/common/http/UrlRules$Rule;
    invoke-virtual {v5, v3}, Lcom/google/android/common/http/UrlRules$Rule;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "rewritten":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 101
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Blocked by "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v5, Lcom/google/android/common/http/UrlRules$Rule;->mName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    new-instance v10, Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;

    const/4 v11, 0x0

    invoke-direct {v10, v5, v11}, Lcom/google/android/apiary/GoogleRequestInitializer$BlockedRequestException;-><init>(Lcom/google/android/common/http/UrlRules$Rule;Lcom/google/android/apiary/GoogleRequestInitializer$1;)V

    throw v10

    .line 103
    :cond_0
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 104
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    const/4 v11, 0x2

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 105
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Rule "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v5, Lcom/google/android/common/http/UrlRules$Rule;->mName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " -> "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_1
    new-instance v7, Lcom/google/api/client/http/GenericUrl;

    .end local v7    # "url":Lcom/google/api/client/http/GenericUrl;
    invoke-direct {v7, v4}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    .line 108
    .restart local v7    # "url":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {p1, v7}, Lcom/google/api/client/http/HttpRequest;->setUrl(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    .line 111
    :cond_2
    const-string v10, "ifmatch"

    invoke-virtual {v7, v10}, Lcom/google/api/client/http/GenericUrl;->getFirst(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 112
    .local v1, "etag":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 113
    invoke-virtual {v2, v1}, Lcom/google/api/client/http/HttpHeaders;->setIfMatch(Ljava/lang/String;)Lcom/google/api/client/http/HttpHeaders;

    .line 114
    const-string v10, "ifmatch"

    invoke-virtual {v7, v10}, Lcom/google/api/client/http/GenericUrl;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    sget-object v11, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 120
    .local v8, "userAgent":Ljava/lang/StringBuilder;
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mUserAgentString:Ljava/lang/String;

    if-eqz v10, :cond_5

    .line 121
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mUserAgentString:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :goto_0
    const-string v10, "userAgentPackage"

    invoke-virtual {v7, v10}, Lcom/google/api/client/http/GenericUrl;->getFirst(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 126
    .local v9, "userAgentExtra":Ljava/lang/String;
    if-eqz v9, :cond_4

    .line 127
    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const-string v10, "userAgentPackage"

    invoke-virtual {v7, v10}, Lcom/google/api/client/http/GenericUrl;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    :cond_4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/google/api/client/http/HttpHeaders;->setUserAgent(Ljava/lang/String;)Lcom/google/api/client/http/HttpHeaders;

    .line 131
    return-void

    .line 123
    .end local v9    # "userAgentExtra":Ljava/lang/String;
    :cond_5
    iget-object v10, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mLogTag:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 1
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAccountName:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mAuthToken:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setUserAgentString(Ljava/lang/String;)V
    .locals 0
    .param p1, "userAgentString"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/apiary/GoogleRequestInitializer;->mUserAgentString:Ljava/lang/String;

    .line 176
    return-void
.end method
