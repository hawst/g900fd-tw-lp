.class public Lcom/google/android/apiary/FeedFetcher;
.super Ljava/lang/Object;
.source "FeedFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private volatile mAuthenticationFailed:Z

.field private final mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

.field private volatile mForcedClosed:Z

.field private volatile mIoException:Z

.field private final mItemClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mItemEndMarker:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mItemQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mItemsName:Ljava/lang/String;

.field private final mJsonFactory:Lcom/google/api/client/json/JsonFactory;

.field protected final mLogTag:Ljava/lang/String;

.field private volatile mPartialSyncUnavailable:Z

.field protected final mRequest:Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

.field private volatile mResourceUnavailable:Z

.field private volatile mRetryAfter:J

.field private volatile mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;Ljava/lang/String;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "jsonFactory"    # Lcom/google/api/client/json/JsonFactory;
    .param p2, "request"    # Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;
    .param p3, "itemsName"    # Ljava/lang/String;
    .param p7, "logTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/json/JsonFactory;",
            "Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/util/concurrent/BlockingQueue",
            "<TT;>;TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    .local p4, "itemClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p5, "itemQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<TT;>;"
    .local p6, "itemEndMarker":Ljava/lang/Object;, "TT;"
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 36
    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z

    .line 37
    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z

    .line 38
    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mResourceUnavailable:Z

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J

    .line 40
    iput-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    .line 51
    iput-object p1, p0, Lcom/google/android/apiary/FeedFetcher;->mJsonFactory:Lcom/google/api/client/json/JsonFactory;

    .line 52
    iput-object p2, p0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    .line 53
    iput-object p3, p0, Lcom/google/android/apiary/FeedFetcher;->mItemsName:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/google/android/apiary/FeedFetcher;->mItemClass:Ljava/lang/Class;

    .line 55
    iput-object p5, p0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    .line 56
    iput-object p6, p0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    .line 57
    iput-object p7, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    .line 58
    return-void
.end method

.method private closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/google/api/client/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->ignore()V

    .line 200
    return-void
.end method

.method private fetchFeed()V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    const/4 v12, 0x0

    .line 78
    .local v12, "response":Lcom/google/api/client/http/HttpResponse;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;->executeUnparsed()Lcom/google/api/client/http/HttpResponse;

    move-result-object v12

    .line 79
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_9

    .line 80
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apiary/FeedFetcher;->onExecute(Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;)V

    .line 81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mJsonFactory:Lcom/google/api/client/json/JsonFactory;

    move-object/from16 v17, v0

    invoke-virtual {v12}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v18

    invoke-virtual {v12}, Lcom/google/api/client/http/HttpResponse;->getContentCharset()Ljava/nio/charset/Charset;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Lcom/google/api/client/json/JsonParser;

    move-result-object v11

    .line 83
    .local v11, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 84
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 85
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v16

    .line 86
    .local v16, "token":Lcom/google/api/client/json/JsonToken;
    const/4 v9, 0x0

    .line 87
    .local v9, "nextPageToken":Ljava/lang/String;
    :goto_1
    sget-object v17, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_8

    .line 88
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0
    :try_end_0
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v17, :cond_2

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 177
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 180
    :cond_0
    if-eqz v12, :cond_1

    .line 182
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 188
    .end local v9    # "nextPageToken":Ljava/lang/String;
    .end local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .end local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :cond_1
    :goto_2
    return-void

    .line 183
    .restart local v9    # "nextPageToken":Ljava/lang/String;
    .restart local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .restart local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :catch_0
    move-exception v4

    .line 184
    .local v4, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IOException, while closing connection"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 91
    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->getText()Ljava/lang/String;

    move-result-object v8

    .line 92
    .local v8, "key":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 93
    const-string v17, "nextPageToken"

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 94
    const-class v17, Ljava/lang/String;

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "nextPageToken":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 111
    .restart local v9    # "nextPageToken":Ljava/lang/String;
    :cond_3
    :goto_3
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v16

    .line 112
    goto :goto_1

    .line 95
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemsName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 98
    :goto_4
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 99
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v17

    sget-object v18, Lcom/google/api/client/json/JsonToken;->END_ARRAY:Lcom/google/api/client/json/JsonToken;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemClass:Ljava/lang/Class;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 120
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "nextPageToken":Ljava/lang/String;
    .end local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .end local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :catch_1
    move-exception v4

    .line 121
    .local v4, "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "GoogleJsonResponseException, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    invoke-virtual {v4}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v14

    .line 123
    .local v14, "statusCode":I
    sparse-switch v14, :sswitch_data_0

    .line 166
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 176
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 177
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_5

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 180
    :cond_5
    if-eqz v12, :cond_1

    .line 182
    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 183
    :catch_2
    move-exception v4

    .line 184
    .local v4, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IOException, while closing connection"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 105
    .end local v4    # "e":Ljava/io/IOException;
    .end local v14    # "statusCode":I
    .restart local v8    # "key":Ljava/lang/String;
    .restart local v9    # "nextPageToken":Ljava/lang/String;
    .restart local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .restart local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :cond_6
    :try_start_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v8}, Lcom/google/android/apiary/FeedFetcher;->parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z

    move-result v10

    .line 106
    .local v10, "parsed":Z
    if-nez v10, :cond_3

    .line 108
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->skipChildren()Lcom/google/api/client/json/JsonParser;
    :try_end_5
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_3

    .line 169
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "nextPageToken":Ljava/lang/String;
    .end local v10    # "parsed":Z
    .end local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .end local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :catch_3
    move-exception v4

    .line 170
    .local v4, "e":Lcom/google/android/apiary/AuthenticationException;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "AuthenticationException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 171
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 177
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_7

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 180
    :cond_7
    if-eqz v12, :cond_1

    .line 182
    :try_start_7
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_2

    .line 183
    :catch_4
    move-exception v4

    .line 184
    .local v4, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IOException, while closing connection"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 113
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v9    # "nextPageToken":Ljava/lang/String;
    .restart local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .restart local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :cond_8
    if-nez v9, :cond_b

    .line 176
    .end local v9    # "nextPageToken":Ljava/lang/String;
    .end local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .end local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 177
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_a

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 180
    :cond_a
    if-eqz v12, :cond_1

    .line 182
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_2

    .line 183
    :catch_5
    move-exception v4

    .line 184
    .restart local v4    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IOException, while closing connection"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 116
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v9    # "nextPageToken":Ljava/lang/String;
    .restart local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .restart local v16    # "token":Lcom/google/api/client/json/JsonToken;
    :cond_b
    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    move-object/from16 v17, v0

    const-string v18, "pageToken"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v9}, Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mRequest:Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;->executeUnparsed()Lcom/google/api/client/http/HttpResponse;
    :try_end_9
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v12

    .line 119
    goto/16 :goto_0

    .line 126
    .end local v9    # "nextPageToken":Ljava/lang/String;
    .end local v11    # "parser":Lcom/google/api/client/json/JsonParser;
    .end local v16    # "token":Lcom/google/api/client/json/JsonToken;
    .local v4, "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .restart local v14    # "statusCode":I
    :sswitch_0
    :try_start_a
    invoke-static {v4}, Lcom/google/android/apiary/FeedFetcher;->isPartialSyncUnavailable(Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 127
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_5

    .line 176
    .end local v4    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .end local v14    # "statusCode":I
    :catchall_0
    move-exception v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 177
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v18, v0

    if-nez v18, :cond_c

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 180
    :cond_c
    if-eqz v12, :cond_d

    .line 182
    :try_start_b
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    .line 185
    :cond_d
    :goto_6
    throw v17

    .line 129
    .restart local v4    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .restart local v14    # "statusCode":I
    :cond_e
    const/16 v17, 0x1

    :try_start_c
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z

    goto/16 :goto_5

    .line 133
    :sswitch_1
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z

    goto/16 :goto_5

    .line 136
    :sswitch_2
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z

    goto/16 :goto_5

    .line 139
    :sswitch_3
    invoke-virtual {v4}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v7

    .line 140
    .local v7, "headers":Lcom/google/api/client/http/HttpHeaders;
    if-eqz v7, :cond_f

    invoke-virtual {v7}, Lcom/google/api/client/http/HttpHeaders;->getRetryAfter()Ljava/lang/String;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v13

    .line 146
    .local v13, "retryAfterString":Ljava/lang/String;
    :goto_7
    if-eqz v13, :cond_10

    .line 148
    :try_start_d
    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/16 v22, 0x3e8

    div-long v20, v20, v22

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 163
    :goto_8
    const/16 v17, 0x1

    :try_start_e
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mResourceUnavailable:Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_5

    .line 140
    .end local v13    # "retryAfterString":Ljava/lang/String;
    :cond_f
    const/4 v13, 0x0

    goto :goto_7

    .line 150
    .restart local v13    # "retryAfterString":Ljava/lang/String;
    :catch_6
    move-exception v5

    .line 153
    .local v5, "e1":Ljava/lang/NumberFormatException;
    :try_start_f
    new-instance v15, Landroid/text/format/Time;

    invoke-direct {v15}, Landroid/text/format/Time;-><init>()V

    .line 154
    .local v15, "time":Landroid/text/format/Time;
    invoke-virtual {v15, v13}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 155
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v18

    const-wide/16 v20, 0x3e8

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J
    :try_end_f
    .catch Landroid/util/TimeFormatException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_8

    .line 156
    .end local v15    # "time":Landroid/text/format/Time;
    :catch_7
    move-exception v6

    .line 157
    .local v6, "e2":Landroid/util/TimeFormatException;
    :try_start_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Unable to parse "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 161
    .end local v5    # "e1":Ljava/lang/NumberFormatException;
    .end local v6    # "e2":Landroid/util/TimeFormatException;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "No Retry-After header"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 172
    .end local v4    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .end local v7    # "headers":Lcom/google/api/client/http/HttpHeaders;
    .end local v13    # "retryAfterString":Ljava/lang/String;
    .end local v14    # "statusCode":I
    :catch_8
    move-exception v4

    .line 173
    .local v4, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IOException, ignoring rest of feed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 174
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 177
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    move/from16 v17, v0

    if-nez v17, :cond_11

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mItemEndMarker:Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 180
    :cond_11
    if-eqz v12, :cond_1

    .line 182
    :try_start_11
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apiary/FeedFetcher;->closeResponse(Lcom/google/api/client/http/HttpResponse;)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9

    goto/16 :goto_2

    .line 183
    :catch_9
    move-exception v4

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "IOException, while closing connection"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 183
    .end local v4    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v4

    .line 184
    .restart local v4    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "IOException, while closing connection"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 123
    nop

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x191 -> :sswitch_1
        0x19a -> :sswitch_2
        0x1f7 -> :sswitch_3
    .end sparse-switch
.end method

.method private static isPartialSyncUnavailable(Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;)Z
    .locals 6
    .param p0, "e"    # Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getDetails()Lcom/google/api/client/googleapis/json/GoogleJsonError;

    move-result-object v0

    .line 257
    .local v0, "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {v0}, Lcom/google/api/client/googleapis/json/GoogleJsonError;->getErrors()Ljava/util/List;

    move-result-object v2

    .line 259
    .local v2, "errors":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;>;"
    if-eqz v2, :cond_1

    .line 260
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;

    .line 261
    .local v1, "error":Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;
    const-string v4, "updatedMinTooLongAgo"

    invoke-virtual {v1}, Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;->getReason()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 262
    const/4 v4, 0x1

    .line 267
    .end local v1    # "error":Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;
    .end local v2    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/client/googleapis/json/GoogleJsonError$ErrorInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 231
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    .line 232
    .local v0, "thread":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 233
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    .line 234
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 236
    :cond_0
    return-void
.end method

.method public getRetryAfter()J
    .locals 2

    .prologue
    .line 248
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-wide v0, p0, Lcom/google/android/apiary/FeedFetcher;->mRetryAfter:J

    return-wide v0
.end method

.method public isAuthenticationFailed()Z
    .locals 1

    .prologue
    .line 244
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mAuthenticationFailed:Z

    return v0
.end method

.method public isIoException()Z
    .locals 1

    .prologue
    .line 215
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mIoException:Z

    return v0
.end method

.method public isPartialSyncUnavailable()Z
    .locals 1

    .prologue
    .line 239
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mPartialSyncUnavailable:Z

    return v0
.end method

.method public isResourceUnavailable()Z
    .locals 1

    .prologue
    .line 252
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mResourceUnavailable:Z

    return v0
.end method

.method protected onExecute(Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;)V
    .locals 0
    .param p1, "request"    # Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    .prologue
    .line 196
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    return-void
.end method

.method protected parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z
    .locals 1
    .param p1, "parser"    # Lcom/google/api/client/json/JsonParser;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 211
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 61
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    .line 62
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 64
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apiary/FeedFetcher;->fetchFeed()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    iput-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    .line 69
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    :goto_0
    return-void

    .line 65
    :catch_0
    move-exception v0

    .line 68
    iput-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    .line 69
    iget-boolean v0, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mThread:Ljava/lang/Thread;

    .line 69
    iget-boolean v1, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 70
    :cond_3
    iget-object v1, p0, Lcom/google/android/apiary/FeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FeedFetcher thread ended: mForcedClosed is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apiary/FeedFetcher;->mForcedClosed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    throw v0
.end method

.method public waitForEnvelope()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 226
    .local p0, "this":Lcom/google/android/apiary/FeedFetcher;, "Lcom/google/android/apiary/FeedFetcher<TT;>;"
    iget-object v0, p0, Lcom/google/android/apiary/FeedFetcher;->mEnvelopeParsedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 227
    return-void
.end method
