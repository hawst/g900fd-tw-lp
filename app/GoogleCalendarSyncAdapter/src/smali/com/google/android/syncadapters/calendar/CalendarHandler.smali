.class public Lcom/google/android/syncadapters/calendar/CalendarHandler;
.super Ljava/lang/Object;
.source "CalendarHandler.java"

# interfaces
.implements Lcom/google/android/apiary/ItemAndEntityHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apiary/ItemAndEntityHandler",
        "<",
        "Lcom/google/api/services/calendar/model/CalendarListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mClient:Lcom/google/api/services/calendar/Calendar;

.field private final mProvider:Landroid/content/ContentProviderClient;


# direct methods
.method public constructor <init>(Lcom/google/api/services/calendar/Calendar;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "client"    # Lcom/google/api/services/calendar/Calendar;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    .line 50
    iput-object p2, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mProvider:Landroid/content/ContentProviderClient;

    .line 51
    iput-object p3, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mAccount:Landroid/accounts/Account;

    .line 52
    return-void
.end method

.method public static calendarEntryToContentValues(Lcom/google/api/services/calendar/model/CalendarListEntry;Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 11
    .param p0, "calendarListEntry"    # Lcom/google/api/services/calendar/model/CalendarListEntry;
    .param p1, "map"    # Landroid/content/ContentValues;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 231
    invoke-virtual {p1}, Landroid/content/ContentValues;->clear()V

    .line 233
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getId()Ljava/lang/String;

    move-result-object v4

    .line 234
    .local v4, "id":Ljava/lang/String;
    const-string v7, "cal_sync1"

    invoke-virtual {p1, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v7, "ownerAccount"

    invoke-virtual {p1, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getSummary()Ljava/lang/String;

    move-result-object v5

    .line 237
    .local v5, "summary":Ljava/lang/String;
    const-string v7, "name"

    invoke-virtual {p1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getSummaryOverride()Ljava/lang/String;

    move-result-object v6

    .line 239
    .local v6, "summaryOverride":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 240
    const-string v7, "calendar_displayName"

    invoke-virtual {p1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :goto_0
    const-string v7, "calendar_timezone"

    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getTimeZone()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getColorId()Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, "colorId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getBackgroundColor()Ljava/lang/String;

    move-result-object v3

    .line 250
    .local v3, "colorString":Ljava/lang/String;
    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const/16 v10, 0x10

    invoke-static {v7, v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/high16 v10, -0x1000000

    or-int v0, v7, v10

    .line 251
    .local v0, "backgroundColor":I
    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getCalendarColorForId(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 252
    .local v1, "color":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v0, v7, :cond_1

    .line 253
    const-string v7, "calendar_color_index"

    invoke-virtual {p1, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v7, "calendar_color"

    invoke-virtual {p1, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 260
    :goto_1
    const-string v10, "cal_sync4"

    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getSelected()Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v7, v9}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    move v7, v8

    :goto_2
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p1, v10, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 262
    const-string v7, "cal_sync5"

    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getHidden()Ljava/lang/Boolean;

    move-result-object v10

    invoke-static {v10, v9}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v10

    if-eqz v10, :cond_3

    :goto_3
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 265
    const-string v7, "calendar_access_level"

    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getAccessRole()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->getAccessLevel(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 269
    const-string v7, "cal_sync8"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 272
    return-object v4

    .line 242
    .end local v0    # "backgroundColor":I
    .end local v1    # "color":Ljava/lang/Integer;
    .end local v2    # "colorId":Ljava/lang/String;
    .end local v3    # "colorString":Ljava/lang/String;
    :cond_0
    const-string v7, "calendar_displayName"

    invoke-virtual {p1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 256
    .restart local v0    # "backgroundColor":I
    .restart local v1    # "color":Ljava/lang/Integer;
    .restart local v2    # "colorId":Ljava/lang/String;
    .restart local v3    # "colorString":Ljava/lang/String;
    :cond_1
    const-string v7, "calendar_color"

    invoke-static {v0}, Lcom/google/android/syncadapters/calendar/Utils;->getDisplayColorFromColor(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p1, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 257
    const-string v7, "calendar_color_index"

    const-string v10, ""

    invoke-virtual {p1, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v7, v9

    .line 260
    goto :goto_2

    :cond_3
    move v8, v9

    .line 262
    goto :goto_3
.end method


# virtual methods
.method public applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/CalendarListEntry;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    .locals 6
    .param p2, "item"    # Lcom/google/api/services/calendar/model/CalendarListEntry;
    .param p3, "entity"    # Landroid/content/Entity;
    .param p4, "clearDirtyFlag"    # Z
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .param p6, "extra"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/google/api/services/calendar/model/CalendarListEntry;",
            "Landroid/content/Entity;",
            "Z",
            "Landroid/content/SyncResult;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const-string v0, "CalendarSyncAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "CalendarSyncAdapter"

    const-string v1, "============= applyItemToEntity ============="

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v0, "CalendarSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-string v0, "CalendarSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "entity is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    if-nez p3, :cond_1

    .line 202
    new-instance v0, Lcom/google/android/apiary/ParseException;

    const-string v1, "Inserts not supported. Entity should not be null"

    invoke-direct {v0, v1}, Lcom/google/android/apiary/ParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {p2, v2}, Lcom/google/android/syncadapters/calendar/CalendarHandler;->calendarEntryToContentValues(Lcom/google/api/services/calendar/model/CalendarListEntry;Landroid/content/ContentValues;)Ljava/lang/String;

    .line 207
    const-string v0, "dirty"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    invoke-virtual {p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 210
    .local v3, "rowId":Ljava/lang/Long;
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 217
    return-void
.end method

.method public contentValuesToCalendarEntry(Landroid/content/ContentValues;)Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 11
    .param p1, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 277
    new-instance v8, Lcom/google/api/services/calendar/model/CalendarListEntry;

    invoke-direct {v8}, Lcom/google/api/services/calendar/model/CalendarListEntry;-><init>()V

    .line 278
    .local v8, "entry":Lcom/google/api/services/calendar/model/CalendarListEntry;
    const-string v0, "calendar_displayName"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    .line 279
    const-string v0, "calendar_timezone"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    .line 280
    const-string v0, "calendar_color_index"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 281
    const-string v0, "CalendarSyncAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    const-string v0, "CalendarSyncAdapter"

    const-string v1, "Missing CALENDAR_COLOR_KEY, fetching from provider"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v10, [Ljava/lang/String;

    const-string v4, "calendar_color_index"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 292
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 294
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    const-string v0, "calendar_color_index"

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 302
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_2
    const-string v0, "calendar_color_index"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 303
    .local v6, "colorId":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 304
    const-string v0, "#%06x"

    new-array v1, v10, [Ljava/lang/Object;

    const-string v2, "calendar_color"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0xffffff

    and-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->setBackgroundColor(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    .line 310
    :goto_0
    return-object v8

    .line 298
    .end local v6    # "colorId":Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 308
    .end local v7    # "cursor":Landroid/database/Cursor;
    .restart local v6    # "colorId":Ljava/lang/String;
    :cond_3
    invoke-virtual {v8, v6}, Lcom/google/api/services/calendar/model/CalendarListEntry;->setColorId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    goto :goto_0
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    const-string v0, "deleted"

    return-object v0
.end method

.method public getEntitySelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    const-string v0, "dirty = 1"

    return-object v0
.end method

.method public getEntityUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 61
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public itemToSourceId(Lcom/google/api/services/calendar/model/CalendarListEntry;)Ljava/lang/String;
    .locals 1
    .param p1, "item"    # Lcom/google/api/services/calendar/model/CalendarListEntry;

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic itemToSourceId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Lcom/google/api/services/calendar/model/CalendarListEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarHandler;->itemToSourceId(Lcom/google/api/services/calendar/model/CalendarListEntry;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newEntityIterator(Ljava/lang/String;[Ljava/lang/String;I)Landroid/content/EntityIterator;
    .locals 6
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .param p3, "limit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 67
    sget-object v0, Landroid/provider/CalendarContract$CalendarEntity;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 68
    .local v1, "uri":Landroid/net/Uri;
    if-gtz p3, :cond_0

    move-object v5, v2

    .line 70
    .local v5, "sortOrder":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mProvider:Landroid/content/ContentProviderClient;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/CalendarContract$CalendarEntity;->newEntityIterator(Landroid/database/Cursor;)Landroid/content/EntityIterator;

    move-result-object v0

    return-object v0

    .line 68
    .end local v5    # "sortOrder":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id LIMIT "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public sendEntityToServer(Landroid/content/Entity;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 23
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Entity;",
            "Landroid/content/SyncResult;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/ParseException;,
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v22

    .line 86
    .local v22, "values":Landroid/content/ContentValues;
    const-string v3, "cal_sync1"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 87
    .local v13, "calendarId":Ljava/lang/String;
    const-string v3, "deleted"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_2

    const/16 v18, 0x1

    .line 90
    .local v18, "isDeleted":Z
    :goto_0
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v12

    .line 91
    .local v12, "activeTag":I
    const/16 v21, 0x0

    .line 93
    .local v21, "tag":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v2, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v3, "mutators"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "mutators"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 102
    .local v19, "mutators":Ljava/lang/String;
    :goto_1
    if-eqz v13, :cond_0

    if-eqz v18, :cond_4

    .line 103
    :cond_0
    :try_start_0
    const-string v3, "CalendarSyncAdapter"

    const-string v5, "Calendars only support updates. No Insert or Delete operations are allowed"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 106
    .local v4, "updateValues":Landroid/content/ContentValues;
    const-string v3, "deleted"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 107
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 114
    const/4 v7, 0x0

    .line 169
    .end local v4    # "updateValues":Landroid/content/ContentValues;
    .local v7, "result":Lcom/google/api/services/calendar/model/CalendarListEntry;
    :goto_2
    if-eqz v21, :cond_1

    .line 170
    const/4 v3, 0x1

    move/from16 v0, v21

    invoke-static {v0, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 172
    :cond_1
    invoke-static {v12}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 175
    const/4 v9, 0x1

    const/4 v11, 0x0

    move-object/from16 v5, p0

    move-object v6, v2

    move-object/from16 v8, p1

    move-object/from16 v10, p2

    invoke-virtual/range {v5 .. v11}, Lcom/google/android/syncadapters/calendar/CalendarHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/CalendarListEntry;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V

    .line 176
    .end local v2    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v7    # "result":Lcom/google/api/services/calendar/model/CalendarListEntry;
    :goto_3
    return-object v2

    .line 87
    .end local v12    # "activeTag":I
    .end local v18    # "isDeleted":Z
    .end local v19    # "mutators":Ljava/lang/String;
    .end local v21    # "tag":I
    :cond_2
    const/16 v18, 0x0

    goto :goto_0

    .line 98
    .restart local v2    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v12    # "activeTag":I
    .restart local v18    # "isDeleted":Z
    .restart local v21    # "tag":I
    :cond_3
    const/16 v19, 0x0

    goto :goto_1

    .line 116
    .restart local v19    # "mutators":Ljava/lang/String;
    :cond_4
    or-int/lit8 v21, v12, 0x2

    .line 117
    :try_start_1
    invoke-static/range {v21 .. v21}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 123
    const-string v3, "calendar_color_index"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 124
    const-string v3, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 125
    const-string v3, "CalendarSyncAdapter"

    const-string v5, "Missing CALENDAR_COLOR_KEY, fetching from provider"

    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v3, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v3, 0x1

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v8, "calendar_color_index"

    aput-object v8, v7, v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v15

    .line 134
    .local v15, "cursor":Landroid/database/Cursor;
    if-eqz v15, :cond_7

    .line 136
    :try_start_2
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 137
    const-string v3, "calendar_color_index"

    const/4 v5, 0x0

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 140
    :cond_6
    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 146
    .end local v15    # "cursor":Landroid/database/Cursor;
    :cond_7
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/syncadapters/calendar/CalendarHandler;->contentValuesToCalendarEntry(Landroid/content/ContentValues;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    move-result-object v17

    .line 148
    .local v17, "event":Lcom/google/api/services/calendar/model/CalendarListEntry;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar;->calendarList()Lcom/google/api/services/calendar/Calendar$CalendarList;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v13, v0}, Lcom/google/api/services/calendar/Calendar$CalendarList;->patch(Ljava/lang/String;Lcom/google/api/services/calendar/model/CalendarListEntry;)Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;

    move-result-object v20

    .line 150
    .local v20, "request":Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;
    const-string v3, "userAgentPackage"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;

    .line 151
    invoke-virtual/range {v20 .. v20}, Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;->execute()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/calendar/model/CalendarListEntry;
    :try_end_4
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .restart local v7    # "result":Lcom/google/api/services/calendar/model/CalendarListEntry;
    goto/16 :goto_2

    .line 140
    .end local v7    # "result":Lcom/google/api/services/calendar/model/CalendarListEntry;
    .end local v17    # "event":Lcom/google/api/services/calendar/model/CalendarListEntry;
    .end local v20    # "request":Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;
    .restart local v15    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    :try_start_5
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 169
    .end local v15    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v3

    if-eqz v21, :cond_8

    .line 170
    const/4 v5, 0x1

    move/from16 v0, v21

    invoke-static {v0, v5}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 172
    :cond_8
    invoke-static {v12}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v3

    .line 152
    :catch_0
    move-exception v16

    .line 153
    .local v16, "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v14

    .line 154
    .local v14, "code":I
    packed-switch v14, :pswitch_data_0

    .line 163
    const-string v3, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t update calendar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": Forbidden"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 164
    const/4 v2, 0x0

    .line 169
    .end local v2    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v21, :cond_9

    .line 170
    const/4 v3, 0x1

    move/from16 v0, v21

    invoke-static {v0, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 172
    :cond_9
    invoke-static {v12}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_3

    .line 156
    .restart local v2    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :pswitch_0
    :try_start_7
    const-string v3, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t update calendar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": Forbidden"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/CalendarHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar;->calendarList()Lcom/google/api/services/calendar/Calendar$CalendarList;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/google/api/services/calendar/Calendar$CalendarList;->get(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$CalendarList$Get;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar$CalendarList$Get;->execute()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/calendar/model/CalendarListEntry;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 159
    .restart local v7    # "result":Lcom/google/api/services/calendar/model/CalendarListEntry;
    goto/16 :goto_2

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x193
        :pswitch_0
    .end packed-switch
.end method
