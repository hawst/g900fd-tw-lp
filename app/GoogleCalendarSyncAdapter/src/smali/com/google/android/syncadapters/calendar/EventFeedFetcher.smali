.class public Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
.super Lcom/google/android/apiary/FeedFetcher;
.source "EventFeedFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apiary/FeedFetcher",
        "<",
        "Lcom/google/api/services/calendar/model/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccessRole:Ljava/lang/String;

.field private volatile mDefaultReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation
.end field

.field private final mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

.field private volatile mLastUpdated:Ljava/lang/String;

.field private mMaxAttendees:I

.field private final mMaxResults:I

.field private final mPrimary:Z

.field private final mThreadStatsTag:I

.field private volatile mTimeZone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/services/calendar/Calendar$Events$List;Ljava/util/concurrent/BlockingQueue;Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;IIZI)V
    .locals 9
    .param p1, "jsonFactory"    # Lcom/google/api/client/json/JsonFactory;
    .param p2, "request"    # Lcom/google/api/services/calendar/Calendar$Events$List;
    .param p4, "eventEndMarker"    # Lcom/google/api/services/calendar/model/Event;
    .param p5, "logTag"    # Ljava/lang/String;
    .param p6, "feedSyncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .param p7, "maxAttendees"    # I
    .param p8, "maxResults"    # I
    .param p9, "primary"    # Z
    .param p10, "threadStatsTag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/json/JsonFactory;",
            "Lcom/google/api/services/calendar/Calendar$Events$List;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/api/services/calendar/model/Event;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            "Ljava/lang/String;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;",
            "IIZI)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p3, "eventQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/api/services/calendar/model/Event;>;"
    const-string v4, "items"

    const-class v5, Lcom/google/api/services/calendar/model/Event;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apiary/FeedFetcher;-><init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;Ljava/lang/String;Ljava/lang/Class;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    .line 58
    iput-object p6, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    .line 59
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    .line 60
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxResults:I

    .line 61
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mPrimary:Z

    .line 62
    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    .line 64
    invoke-virtual {p0, p2}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setRequestParams(Lcom/google/api/services/calendar/Calendar$Events$List;)V

    .line 65
    return-void
.end method

.method private setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V
    .locals 4
    .param p1, "request"    # Lcom/google/api/services/calendar/Calendar$Events$List;
    .param p2, "feedState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .param p3, "noIncremental"    # Z

    .prologue
    .line 195
    iget v2, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxResults:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 197
    const-string v2, "do_incremental_sync"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 199
    .local v0, "doIncrementalSync":Z
    const-string v2, "feed_updated_time"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "feedUpdatedTime":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    .line 203
    if-eqz v1, :cond_0

    .line 205
    invoke-static {v1}, Lcom/google/api/client/util/DateTime;->parseRfc3339(Ljava/lang/String;)Lcom/google/api/client/util/DateTime;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setUpdatedMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 209
    :cond_0
    iget v2, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 210
    return-void
.end method


# virtual methods
.method public getAccessRole()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mAccessRole:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultReminders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    return-object v0
.end method

.method public getLastUpdated()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLastUpdated:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method protected onExecute(Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;)V
    .locals 7
    .param p1, "request"    # Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;

    .prologue
    .line 214
    iget-object v4, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Saving inProgress state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 216
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lcom/google/api/client/googleapis/services/json/AbstractGoogleJsonClientRequest;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 217
    .local v3, "params":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 218
    .local v2, "name":Ljava/lang/String;
    const-string v4, "calendarId"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 219
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 222
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "params":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    iget-object v4, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    invoke-virtual {v4, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->setInProgressParams(Ljava/util/Map;)V

    .line 223
    return-void
.end method

.method protected parseField(Lcom/google/api/client/json/JsonParser;Ljava/lang/String;)Z
    .locals 4
    .param p1, "parser"    # Lcom/google/api/client/json/JsonParser;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 78
    const-string v0, "timeZone"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mTimeZone:Ljava/lang/String;

    move v0, v1

    .line 95
    :goto_0
    return v0

    .line 82
    :cond_0
    const-string v0, "updated"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLastUpdated:Ljava/lang/String;

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_1
    const-string v0, "defaultReminders"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    .line 88
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mDefaultReminders:Ljava/util/List;

    const-class v2, Lcom/google/api/services/calendar/model/EventReminder;

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseArray(Ljava/util/Collection;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)V

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_2
    const-string v0, "accessRole"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mAccessRole:Ljava/lang/String;

    move v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    or-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 71
    invoke-super {p0}, Lcom/google/android/apiary/FeedFetcher;->run()V

    .line 72
    iget v0, p0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mThreadStatsTag:I

    or-int/lit8 v0, v0, 0x4

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 74
    return-void
.end method

.method protected setRequestParams(Lcom/google/api/services/calendar/Calendar$Events$List;)V
    .locals 22
    .param p1, "request"    # Lcom/google/api/services/calendar/Calendar$Events$List;

    .prologue
    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getInProgressParams()Ljava/util/Map;

    move-result-object v10

    .line 124
    .local v10, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v10, :cond_2

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Resuming inProgress sync: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 127
    .local v7, "param":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 129
    .local v5, "key":Ljava/lang/String;
    const-string v18, "timeMin"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "timeMax"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "updatedMin"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 130
    :cond_0
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/api/client/util/DateTime;->parseRfc3339(Ljava/lang/String;)Lcom/google/api/client/util/DateTime;

    move-result-object v11

    .line 134
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, Lcom/google/api/services/calendar/Calendar$Events$List;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$List;

    goto :goto_0

    .line 132
    :cond_1
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    .local v11, "value":Ljava/lang/Object;
    goto :goto_1

    .line 139
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "key":Ljava/lang/String;
    .end local v7    # "param":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v11    # "value":Ljava/lang/Object;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->isGDataUpgrade()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "upgrade_min_start"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    .line 143
    .local v14, "upgradeMinStart":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "upgrade_max_start"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 145
    .local v12, "upgradeMaxStart":J
    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v14, v15, v1}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 146
    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v12, v13, v1}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 147
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mMaxAttendees:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 192
    .end local v12    # "upgradeMaxStart":J
    .end local v14    # "upgradeMinStart":J
    :cond_3
    :goto_2
    return-void

    .line 151
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "window_end"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    .line 152
    .local v16, "windowEnd":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const-string v19, "new_window_end"

    const-wide/16 v20, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 155
    .local v8, "moveWindowEnd":J
    const-wide/16 v18, 0x0

    cmp-long v18, v8, v18

    if-nez v18, :cond_7

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mLogTag:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mFeedSyncState: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", startMaxMs: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_5
    const-wide/16 v18, 0x0

    cmp-long v18, v16, v18

    if-lez v18, :cond_6

    .line 163
    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 165
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/Calendar$Events$List;->getUpdatedMin()Lcom/google/api/client/util/DateTime;

    move-result-object v18

    if-nez v18, :cond_3

    .line 166
    new-instance v6, Landroid/text/format/Time;

    const-string v18, "UTC"

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 167
    .local v6, "minTime":Landroid/text/format/Time;
    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    .line 168
    iget v0, v6, Landroid/text/format/Time;->year:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    iput v0, v6, Landroid/text/format/Time;->year:I

    .line 169
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 170
    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v20

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    goto/16 :goto_2

    .line 177
    .end local v6    # "minTime":Landroid/text/format/Time;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->mFeedSyncState:Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->setBaseParams(Lcom/google/api/services/calendar/Calendar$Events$List;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Z)V

    .line 181
    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMax(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    .line 182
    new-instance v18, Lcom/google/api/client/util/DateTime;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v8, v9, v1}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/Calendar$Events$List;->setTimeMin(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/Calendar$Events$List;

    goto/16 :goto_2
.end method
