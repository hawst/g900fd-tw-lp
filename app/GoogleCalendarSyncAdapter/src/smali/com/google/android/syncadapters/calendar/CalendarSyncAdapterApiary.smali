.class public Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
.super Lcom/google/android/common/LoggingThreadedSyncAdapter;
.source "CalendarSyncAdapterApiary.java"


# static fields
.field private static final CALENDARS_PROJECTION:[Ljava/lang/String;

.field private static final mCalendarColors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final mCalendarColorsGsf:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final mEventColors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final mEventColorsGsf:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final sEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<",
            "Lcom/google/api/services/calendar/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field static final sEventEndMarker:Lcom/google/api/services/calendar/model/Event;


# instance fields
.field private final mAccountsWithColors:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mClient:Lcom/google/api/services/calendar/Calendar;

.field private mGoogleRequestInitializer:Lcom/google/android/apiary/GoogleRequestInitializer;

.field private mHttpTransport:Lcom/google/api/client/http/HttpTransport;

.field private mUpdatedColorsFromGsf:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 112
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    .line 113
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColorsGsf:Ljava/util/Map;

    .line 114
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    .line 115
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColorsGsf:Ljava/util/Map;

    .line 117
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "1"

    const v2, -0x538da2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "2"

    const v2, -0x2f949c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "3"

    const v2, -0x7c5de

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "4"

    const v2, -0x5a8c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "5"

    const v2, -0x8ac9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "6"

    const/16 v2, -0x52ba

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "7"

    const v2, -0xbd296e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "8"

    const v2, -0xe9589b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "9"

    const v2, -0x842eb8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "10"

    const v2, -0x4c2394

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "11"

    const v2, -0x4167d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "12"

    const v2, -0x52e9b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "13"

    const v2, -0x6d1e40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "14"

    const v2, -0x601e19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "15"

    const v2, -0x603919

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "16"

    const v2, -0xb67919

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "17"

    const v2, -0x656301

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "18"

    const v2, -0x466501

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "19"

    const v2, -0x3d3d3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "20"

    const v2, -0x354241

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "21"

    const v2, -0x335954

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "22"

    const v2, -0x96e4e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "23"

    const v2, -0x328b1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    const-string v1, "24"

    const v2, -0x5b851e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "1"

    const v2, -0x5b4204

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "2"

    const v2, -0x851841

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "3"

    const v2, -0x245201

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "4"

    const/16 v2, -0x7784

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "5"

    const v2, -0x428a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "6"

    const/16 v2, -0x4788

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "7"

    const v2, -0xb92925

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "8"

    const v2, -0x1e1e1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "9"

    const v2, -0xab7b13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "10"

    const v2, -0xae48b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    const-string v1, "11"

    const v2, -0x23ded9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "dirty"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "visible"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sync_events"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "calendar_timezone"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "calendar_color_index"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "cal_sync1"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "cal_sync4"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "cal_sync5"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->CALENDARS_PROJECTION:[Ljava/lang/String;

    .line 230
    new-instance v0, Lcom/google/api/services/calendar/model/Event;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sEventEndMarker:Lcom/google/api/services/calendar/model/Event;

    .line 231
    new-instance v0, Lcom/google/android/apiary/EntityReader$EntityItem;

    invoke-direct {v0, v3, v3}, Lcom/google/android/apiary/EntityReader$EntityItem;-><init>(Ljava/lang/Object;Landroid/content/Entity;)V

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 257
    invoke-direct {p0, p1, v4}, Lcom/google/android/common/LoggingThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 251
    new-instance v3, Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-direct {v3}, Lcom/google/api/client/http/javanet/NetHttpTransport;-><init>()V

    iput-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mHttpTransport:Lcom/google/api/client/http/HttpTransport;

    .line 253
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mAccountsWithColors:Ljava/util/Set;

    .line 254
    iput-boolean v4, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mUpdatedColorsFromGsf:Z

    .line 259
    new-instance v3, Lcom/google/android/apiary/GoogleRequestInitializer;

    const-string v4, "oauth2:https://www.googleapis.com/auth/calendar"

    const-string v5, "CalendarSyncAdapter"

    const-string v6, "com.android.calendar"

    invoke-direct {v3, p1, v4, v5, v6}, Lcom/google/android/apiary/GoogleRequestInitializer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mGoogleRequestInitializer:Lcom/google/android/apiary/GoogleRequestInitializer;

    .line 263
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 264
    .local v1, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v2, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 266
    .local v2, "versionCode":I
    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mGoogleRequestInitializer:Lcom/google/android/apiary/GoogleRequestInitializer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apiary/GoogleRequestInitializer;->setUserAgentString(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "versionCode":I
    :goto_0
    new-instance v3, Lcom/google/api/services/calendar/Calendar$Builder;

    iget-object v4, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mHttpTransport:Lcom/google/api/client/http/HttpTransport;

    new-instance v5, Lcom/google/api/client/extensions/android/json/AndroidJsonFactory;

    invoke-direct {v5}, Lcom/google/api/client/extensions/android/json/AndroidJsonFactory;-><init>()V

    iget-object v6, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mGoogleRequestInitializer:Lcom/google/android/apiary/GoogleRequestInitializer;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/api/services/calendar/Calendar$Builder;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpRequestInitializer;)V

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar$Builder;->build()Lcom/google/api/services/calendar/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    .line 275
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 276
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v3, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 277
    new-instance v3, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterBroadcastReceiver;

    invoke-direct {v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterBroadcastReceiver;-><init>()V

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 278
    return-void

    .line 267
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private addDefaultCalendar(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 17
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 2545
    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 2546
    .local v9, "calendarId":Ljava/lang/String;
    const-string v2, "CalendarSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2547
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding default calendar for account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2549
    :cond_0
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 2550
    .local v16, "values":Landroid/content/ContentValues;
    const-string v2, "account_name"

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2551
    const-string v2, "account_type"

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2552
    const-string v2, "cal_sync1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2553
    const-string v2, "ownerAccount"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2554
    const-string v2, "calendar_displayName"

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2555
    const-string v2, "sync_events"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2556
    const-string v2, "visible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2557
    const-string v2, "cal_sync4"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2558
    const-string v2, "cal_sync5"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2559
    const-string v2, "allowedReminders"

    const-string v3, "0,1,2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2560
    const-string v2, "allowedAttendeeTypes"

    const-string v3, "0,1,2"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2561
    const-string v2, "allowedAvailability"

    const-string v3, "0,1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2562
    const-string v2, "calendar_color"

    sget v3, Lcom/google/android/syncadapters/calendar/HandlerUtils;->DEFAULT_CALENDAR_INT_COLOR:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2566
    const-string v2, "calendar_timezone"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2567
    const-string v2, "calendar_access_level"

    const/16 v3, 0x2bc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2572
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lcom/google/android/apiary/ProviderHelper;->insertProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v13

    .line 2576
    .local v13, "uri":Landroid/net/Uri;
    if-nez v13, :cond_2

    .line 2577
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot add default calendar for account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2617
    :cond_1
    :goto_0
    return-void

    .line 2581
    :cond_2
    invoke-static {v13}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v14

    .line 2584
    .local v14, "newCalId":J
    const/4 v8, 0x0

    .line 2586
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "account_type=?"

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const-string v6, "cal_sync1=?"

    aput-object v6, v2, v5

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget-object v7, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v7, v6, v2

    const/4 v2, 0x1

    aput-object v9, v6, v2

    const-string v7, "calendar_access_level"

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2593
    if-eqz v8, :cond_5

    .line 2594
    :cond_3
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2595
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 2597
    .local v10, "calId":J
    cmp-long v2, v10, v14

    if-eqz v2, :cond_3

    .line 2600
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 2601
    .local v12, "map":Landroid/content/ContentValues;
    const-string v2, "sync_events"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2602
    const-string v2, "visible"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2604
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v12, v3, v4}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2613
    .end local v10    # "calId":J
    .end local v12    # "map":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_4

    .line 2614
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 2613
    :cond_5
    if-eqz v8, :cond_1

    .line 2614
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method static addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "eventId"    # Ljava/lang/Long;
    .param p3, "allowYielding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/Long;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1995
    .local p0, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1996
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    if-eqz p2, :cond_0

    .line 1997
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 1999
    :cond_0
    invoke-virtual {v0, p3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2000
    return-void
.end method

.method static addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "eventId"    # Ljava/lang/Long;
    .param p4, "eventBackRef"    # Ljava/lang/Integer;
    .param p5, "allowYielding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2005
    .local p0, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 2009
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    if-eqz p3, :cond_0

    .line 2010
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2012
    :cond_0
    if-eqz p4, :cond_1

    .line 2014
    const-string v1, "event_id"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 2016
    :cond_1
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2017
    return-void
.end method

.method private addMissingColors(Ljava/util/ArrayList;Landroid/accounts/Account;ILjava/util/Map;Landroid/net/Uri;Ljava/util/Set;)V
    .locals 9
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "colorType"    # I
    .param p5, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 680
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p4, "colorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p6, "existingColorKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 681
    .local v7, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 682
    .local v5, "key":Ljava/lang/String;
    invoke-interface {p6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 683
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v3, p5

    move v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->insertColor(Landroid/accounts/Account;Ljava/util/ArrayList;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/Integer;)V

    .line 684
    invoke-interface {p6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 687
    .end local v5    # "key":Ljava/lang/String;
    .end local v7    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_1
    return-void
.end method

.method static addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "rowId"    # Ljava/lang/Long;
    .param p4, "rowIdBackRef"    # Ljava/lang/Integer;
    .param p5, "allowYielding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p0, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2022
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 2027
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    if-eqz p3, :cond_0

    .line 2028
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 2030
    :cond_0
    if-eqz p4, :cond_1

    .line 2031
    const-string v1, "_id=?"

    new-array v2, v4, [Ljava/lang/String;

    aput-object v5, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 2032
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    .line 2034
    :cond_1
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2035
    return-void
.end method

.method private applyOperations(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/EventHandler;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V
    .locals 9
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "handler"    # Lcom/google/android/syncadapters/calendar/EventHandler;
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .param p6, "syncInfo"    # Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .param p7, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p8, "feedSyncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Lcom/google/android/syncadapters/calendar/EventHandler;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<",
            "Lcom/google/api/services/calendar/model/Event;",
            ">;>;",
            "Landroid/content/SyncResult;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2071
    .local p3, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p4, "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    :try_start_0
    invoke-direct {p0, p1, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2082
    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    .line 2083
    invoke-interface {p4}, Ljava/util/List;->clear()V

    .line 2085
    :goto_0
    return-void

    .line 2072
    :catch_0
    move-exception v8

    .local v8, "e":Lcom/google/android/apiary/ParseException;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    .line 2073
    :try_start_1
    invoke-direct/range {v0 .. v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperationsSingleEntityMode(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/EventHandler;Ljava/util/List;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2082
    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    .line 2083
    invoke-interface {p4}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 2082
    .end local v8    # "e":Lcom/google/android/apiary/ParseException;
    :catchall_0
    move-exception v0

    invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V

    .line 2083
    invoke-interface {p4}, Ljava/util/List;->clear()V

    throw v0
.end method

.method private applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/ParseException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2042
    .local p2, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2043
    invoke-static {p1, p2}, Lcom/google/android/apiary/ProviderHelper;->applyBatchProvider(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v1

    .line 2045
    .local v1, "result":[Landroid/content/ContentProviderResult;
    const-string v2, "CalendarSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2046
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Batch applied successfully, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " results"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/TransactionTooLargeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2057
    .end local v1    # "result":[Landroid/content/ContentProviderResult;
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 2059
    return-void

    .line 2049
    :catch_0
    move-exception v0

    .line 2050
    .local v0, "e":Landroid/os/TransactionTooLargeException;
    :try_start_1
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Error applying batch, an unknown number of yield points succeeded"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2051
    new-instance v2, Lcom/google/android/apiary/ParseException;

    const-string v3, "error while applying batch"

    invoke-direct {v2, v3, v0}, Lcom/google/android/apiary/ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2057
    .end local v0    # "e":Landroid/os/TransactionTooLargeException;
    :catchall_0
    move-exception v2

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    throw v2

    .line 2052
    :catch_1
    move-exception v0

    .line 2053
    .local v0, "e":Landroid/content/OperationApplicationException;
    :try_start_2
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error applying batch, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->getNumSuccessfulYieldPoints()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " yield points succeeded"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2055
    new-instance v2, Lcom/google/android/apiary/ParseException;

    const-string v3, "error while applying batch"

    invoke-direct {v2, v3, v0}, Lcom/google/android/apiary/ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private applyOperationsSingleEntityMode(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/EventHandler;Ljava/util/List;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V
    .locals 14
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "handler"    # Lcom/google/android/syncadapters/calendar/EventHandler;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .param p5, "syncInfo"    # Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .param p6, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p7, "feedSyncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Lcom/google/android/syncadapters/calendar/EventHandler;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apiary/EntityReader$EntityItem",
            "<",
            "Lcom/google/api/services/calendar/model/Event;",
            ">;>;",
            "Landroid/content/SyncResult;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2095
    .local p3, "entityItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    const-string v2, "CalendarSyncAdapter"

    const-string v6, "Failed to apply a batch of entity operations. Retrying in single mode"

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2096
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 2097
    .local v3, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apiary/EntityReader$EntityItem;

    .line 2098
    .local v10, "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2099
    const-string v2, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2100
    const-string v2, "CalendarSyncAdapter"

    const-string v6, "applyOperationsSingleEntityMode: noticed a cancel, bailing out"

    invoke-static {v2, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2140
    .end local v10    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    :cond_0
    return-void

    .line 2104
    .restart local v10    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2105
    iget-object v4, v10, Lcom/google/android/apiary/EntityReader$EntityItem;->entry:Ljava/lang/Object;

    check-cast v4, Lcom/google/api/services/calendar/model/Event;

    .line 2106
    .local v4, "entry":Lcom/google/api/services/calendar/model/Event;
    iget-object v5, v10, Lcom/google/android/apiary/EntityReader$EntityItem;->entity:Landroid/content/Entity;

    .line 2107
    .local v5, "entity":Landroid/content/Entity;
    const-string v2, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "CalendarSyncAdapterFine"

    const/4 v6, 0x2

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2108
    :cond_2
    const-string v2, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calling applyItemToEntity for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2111
    :cond_3
    const/4 v6, 0x0

    move-object/from16 v2, p2

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    :try_start_0
    invoke-virtual/range {v2 .. v8}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2123
    const-string v2, "lastFetchedId"

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v0, v2, v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2125
    const-string v2, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2126
    :cond_4
    const-string v2, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Processing event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2128
    :cond_5
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->newUpdateOperation()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2129
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v2, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v12, 0x1

    add-long/2addr v6, v12

    iput-wide v6, v2, Landroid/content/SyncStats;->numEntries:J

    .line 2131
    :try_start_1
    invoke-direct {p0, p1, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_1
    .catch Lcom/google/android/apiary/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 2132
    :catch_0
    move-exception v9

    .line 2135
    .local v9, "e":Lcom/google/android/apiary/ParseException;
    const-string v2, "CalendarSyncAdapter"

    const-string v6, "Failed to apply entity operations in single mode. Ignoring."

    invoke-static {v2, v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 2117
    .end local v9    # "e":Lcom/google/android/apiary/ParseException;
    :catch_1
    move-exception v9

    .line 2120
    .local v9, "e":Ljava/lang/Exception;
    const-string v2, "CalendarSyncAdapter"

    const-string v6, "Failed to apply item to entity in single mode. Ignoring."

    invoke-static {v2, v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private cleanupForUnsyncedCalendars(Landroid/accounts/Account;Landroid/content/ContentProviderClient;)V
    .locals 22
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 740
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-static {v0, v4, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getOrCreate(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v21

    .line 744
    .local v21, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "sync_events"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "cal_sync1"

    aput-object v7, v6, v4

    sget-object v7, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v9, v8, v4

    const/4 v4, 0x1

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v9, v8, v4

    const/4 v4, 0x2

    const-string v9, "0"

    aput-object v9, v8, v4

    const-string v9, "_id"

    move-object/from16 v4, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 756
    .local v14, "cursor":Landroid/database/Cursor;
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v17

    .line 758
    .local v17, "eventsUri":Landroid/net/Uri;
    const/16 v18, 0x0

    .line 761
    .local v18, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 762
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 763
    .local v12, "calId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v12, v4

    if-gez v4, :cond_1

    .line 764
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 765
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found a non valid CalendarId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 830
    .end local v12    # "calId":J
    :catch_0
    move-exception v16

    .line 831
    .local v16, "e":Landroid/content/OperationApplicationException;
    :try_start_1
    const-string v4, "CalendarSyncAdapter"

    const-string v5, "Cannot process cleanup Events operations"

    move-object/from16 v0, v16

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 833
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 835
    .end local v16    # "e":Landroid/content/OperationApplicationException;
    :goto_1
    return-void

    .line 769
    .restart local v12    # "calId":J
    :cond_1
    const/4 v4, 0x1

    :try_start_2
    invoke-interface {v14, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 770
    .local v20, "syncEvent":I
    const/4 v4, 0x2

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 773
    .local v11, "calendarId":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v15

    .line 776
    .local v15, "data":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    if-eqz v15, :cond_2

    invoke-virtual {v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->size()I

    move-result v4

    if-nez v4, :cond_6

    :cond_2
    const/16 v19, 0x0

    .line 779
    .local v19, "savedSyncEvent":I
    :goto_2
    move/from16 v0, v20

    move/from16 v1, v19

    if-eq v0, v1, :cond_0

    .line 785
    if-nez v18, :cond_3

    .line 786
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v18

    .line 790
    :cond_3
    if-eqz v15, :cond_5

    .line 791
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 792
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Will clean feedSyncState for CalendarId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and Feed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    :cond_4
    invoke-virtual {v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->clear()V

    .line 799
    :cond_5
    invoke-static/range {v17 .. v17}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 809
    .local v10, "builder":Landroid/content/ContentProviderOperation$Builder;
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "calendar_id=?"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_sync_id IS NOT NULL"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "dirty=?"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "deleted=?"

    aput-object v6, v4, v5

    invoke-static {v4}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {v10, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 820
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 833
    .end local v10    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v11    # "calendarId":Ljava/lang/String;
    .end local v12    # "calId":J
    .end local v15    # "data":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .end local v19    # "savedSyncEvent":I
    .end local v20    # "syncEvent":I
    :catchall_0
    move-exception v4

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v4

    .line 776
    .restart local v11    # "calendarId":Ljava/lang/String;
    .restart local v12    # "calId":J
    .restart local v15    # "data":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .restart local v20    # "syncEvent":I
    :cond_6
    const/16 v19, 0x1

    goto/16 :goto_2

    .line 824
    .end local v11    # "calendarId":Ljava/lang/String;
    .end local v12    # "calId":J
    .end local v15    # "data":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .end local v20    # "syncEvent":I
    :cond_7
    if-eqz v18, :cond_8

    .line 826
    :try_start_3
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->newUpdateOperation()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apiary/ProviderHelper;->applyBatchProvider(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_3
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 833
    :cond_8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method

.method public static getCalendarColorForId(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 2711
    sget-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private getCount(Landroid/content/ContentProviderClient;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "client"    # Landroid/content/ContentProviderClient;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2394
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_count"

    aput-object v0, v2, v7

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2400
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    move v0, v7

    .line 2408
    :goto_0
    return v0

    .line 2405
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    .line 2406
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2408
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private getCurrentCalendars(Landroid/accounts/Account;)Ljava/util/HashSet;
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1465
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1466
    .local v6, "calendarIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1467
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "account_name=?"

    aput-object v4, v3, v8

    const-string v4, "account_type=?"

    aput-object v4, v3, v9

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/String;

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v8

    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1472
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 1474
    :goto_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1475
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1478
    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1481
    :cond_1
    return-object v6
.end method

.method private getExpectedFeeds(Landroid/accounts/Account;)Ljava/util/HashSet;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 838
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v7

    .line 839
    .local v7, "expectedFeeds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 840
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v11, [Ljava/lang/String;

    const-string v3, "cal_sync1"

    aput-object v3, v2, v10

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v10

    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v4, v11

    const/4 v5, 0x2

    const-string v10, "1"

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 846
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 847
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 850
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 851
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 852
    .local v9, "id":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://www.google.com/calendar/feeds/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v9}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/private/full"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 855
    .local v8, "feedUrl":Ljava/lang/String;
    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 858
    .end local v8    # "feedUrl":Ljava/lang/String;
    .end local v9    # "id":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 860
    return-object v7
.end method

.method private getNumChanges(Landroid/content/SyncResult;)J
    .locals 4
    .param p1, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 2429
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v0, Landroid/content/SyncStats;->numInserts:J

    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v2, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v0, v2

    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v2, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private getOrCreateSyncState(Landroid/content/Context;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "feedId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1831
    invoke-static {p0, p1, p3, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getOrCreate(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    .line 1835
    .local v0, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    invoke-virtual {v0, p4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->hasFeed(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1836
    invoke-virtual {v0, p4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->addFeed(Ljava/lang/String;)V

    .line 1837
    invoke-virtual {v0, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 1840
    :cond_0
    return-object v0
.end method

.method private getServerDiffsForFeed(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/os/Bundle;Landroid/content/SyncResult;Ljava/lang/String;Z)V
    .locals 18
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "extras"    # Landroid/os/Bundle;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .param p5, "feedId"    # Ljava/lang/String;
    .param p6, "moveWindowSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 1077
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p5

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getOrCreateSyncState(Landroid/content/Context;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v10

    .line 1080
    .local v10, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1081
    .local v4, "contentResolver":Landroid/content/ContentResolver;
    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "sync_events"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "calendar_timezone"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "calendar_access_level"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "account_name=?"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "account_type=?"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "cal_sync1=?"

    aput-object v9, v7, v8

    invoke-static {v7}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v12, v8, v9

    const/4 v9, 0x1

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v12, v8, v9

    const/4 v9, 0x2

    aput-object p5, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1095
    .local v14, "cursor":Landroid/database/Cursor;
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 1098
    .local v17, "values":Landroid/content/ContentValues;
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    .line 1148
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1150
    :goto_0
    return-void

    .line 1102
    :cond_0
    :try_start_1
    new-instance v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;

    invoke-direct {v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;-><init>()V

    .line 1103
    .local v11, "syncInfo":Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    const/4 v5, 0x0

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    .line 1104
    const/4 v5, 0x1

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    const/4 v15, 0x1

    .line 1105
    .local v15, "syncEvents":Z
    :goto_1
    const/4 v5, 0x2

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    .line 1106
    .local v16, "timezone":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->accessLevel:I

    .line 1108
    if-nez v15, :cond_3

    .line 1111
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1112
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "Ignoring sync request for non-syncable feed."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148
    :cond_1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1104
    .end local v15    # "syncEvents":Z
    .end local v16    # "timezone":Ljava/lang/String;
    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    .restart local v15    # "syncEvents":Z
    .restart local v16    # "timezone":Ljava/lang/String;
    :cond_3
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v12, p5

    move/from16 v13, p6

    .line 1117
    :try_start_2
    invoke-direct/range {v5 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getServerDiffsImpl(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/os/Bundle;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Ljava/lang/String;Z)V

    .line 1127
    invoke-virtual/range {p4 .. p4}, Landroid/content/SyncResult;->hasError()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    if-eqz v5, :cond_4

    .line 1148
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1132
    :cond_4
    :try_start_3
    iget-object v5, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1133
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1134
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Timezone changed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " => "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    :cond_5
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentValues;->clear()V

    .line 1137
    const-string v5, "calendar_timezone"

    iget-object v6, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    iget-wide v6, v11, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1, v6, v7}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1148
    :cond_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .end local v11    # "syncInfo":Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .end local v15    # "syncEvents":Z
    .end local v16    # "timezone":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v5
.end method

.method private getServerDiffsImpl(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/os/Bundle;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Ljava/lang/String;Z)V
    .locals 71
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "extras"    # Landroid/os/Bundle;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .param p5, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p6, "syncInfo"    # Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .param p7, "calendarSyncId"    # Ljava/lang/String;
    .param p8, "moveWindowSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 1164
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1462
    :cond_0
    :goto_0
    return-void

    .line 1167
    :cond_1
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1168
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "starting getServerDiffs for account "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    :cond_2
    move-object/from16 v0, p7

    move-object/from16 v1, p6

    iput-object v0, v1, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarSyncId:Ljava/lang/String;

    .line 1172
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    .line 1173
    .local v17, "contentResolver":Landroid/content/ContentResolver;
    const-string v5, "google_calendar_sync_entry_fetch_queue_size"

    const/16 v6, 0x32

    move-object/from16 v0, v17

    invoke-static {v0, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v54

    .line 1177
    .local v54, "entryFetchQueueSize":I
    const-string v5, "google_calendar_sync_entity_fetch_queue_size"

    const/16 v6, 0x32

    move-object/from16 v0, v17

    invoke-static {v0, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v50

    .line 1182
    .local v50, "entityFetchQueueSize":I
    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    move/from16 v0, v54

    invoke-direct {v7, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 1184
    .local v7, "eventQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/api/services/calendar/model/Event;>;"
    new-instance v22, Ljava/util/concurrent/LinkedBlockingQueue;

    move-object/from16 v0, v22

    move/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 1187
    .local v22, "entities":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    const/16 v55, 0x0

    .line 1188
    .local v55, "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    const/16 v52, 0x0

    .line 1190
    .local v52, "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    const/16 v60, 0x0

    .line 1191
    .local v60, "numRecords":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v64

    .line 1192
    .local v64, "startTimeMs":J
    const/16 v56, 0x0

    .line 1193
    .local v56, "fetchSuccessful":Z
    move-object/from16 v0, p5

    move-object/from16 v1, p7

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v10

    .line 1194
    .local v10, "feedSyncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1195
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FeedSyncState is: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " for Url: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getSyncWindowEnd()J

    move-result-wide v44

    .line 1201
    .local v44, "desiredWindowEnd":J
    :try_start_0
    move/from16 v0, p8

    move-wide/from16 v1, v44

    invoke-static {v0, v1, v2, v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->selectServerSyncMode(ZJLcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)I

    move-result v67

    .line 1203
    .local v67, "syncMode":I
    packed-switch v67, :pswitch_data_0

    .line 1237
    :cond_4
    :goto_1
    :pswitch_0
    const-string v5, "google_calendar_sync_num_events_per_batch"

    const/16 v6, 0xc8

    move-object/from16 v0, v17

    invoke-static {v0, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v12

    .line 1241
    .local v12, "maxResults":I
    const-string v5, "google_calendar_sync_max_attendees"

    const/16 v6, 0x32

    move-object/from16 v0, v17

    invoke-static {v0, v5, v6}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    .line 1245
    .local v11, "maxAttendees":I
    const-string v5, "window_end"

    const-wide/16 v8, 0x0

    invoke-virtual {v10, v5, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    move-object/from16 v0, p6

    iput-wide v8, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->slidingWindowEnd:J

    .line 1248
    new-instance v4, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v5}, Lcom/google/api/services/calendar/Calendar;->getJsonFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v6}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Lcom/google/api/services/calendar/Calendar$Events;->list(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$List;

    move-result-object v6

    sget-object v8, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sEventEndMarker:Lcom/google/api/services/calendar/model/Event;

    const-string v9, "CalendarSyncAdapter"

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-virtual {v0, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v14

    invoke-direct/range {v4 .. v14}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;-><init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/services/calendar/Calendar$Events$List;Ljava/util/concurrent/BlockingQueue;Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;IIZI)V
    :try_end_0
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/apiary/ParseException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1260
    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .local v4, "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    :try_start_1
    new-instance v57, Ljava/lang/Thread;

    const-string v5, "EventFeedFetcher"

    move-object/from16 v0, v57

    invoke-direct {v0, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1261
    .local v57, "fetcherThread":Ljava/lang/Thread;
    invoke-virtual/range {v57 .. v57}, Ljava/lang/Thread;->start()V

    .line 1262
    new-instance v13, Lcom/google/android/syncadapters/calendar/EventHandler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v18, p7

    invoke-direct/range {v13 .. v18}, Lcom/google/android/syncadapters/calendar/EventHandler;-><init>(Lcom/google/api/services/calendar/Calendar;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 1265
    .local v13, "handler":Lcom/google/android/syncadapters/calendar/EventHandler;
    new-instance v18, Lcom/google/android/apiary/EntityReader;

    const-string v19, "CalendarSyncAdapter"

    sget-object v21, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sEventEndMarker:Lcom/google/api/services/calendar/model/Event;

    sget-object v23, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;

    const-string v25, "_sync_id"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cal_sync1=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p7

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v20, v7

    move-object/from16 v24, v13

    invoke-direct/range {v18 .. v26}, Lcom/google/android/apiary/EntityReader;-><init>(Ljava/lang/String;Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/apiary/EntityReader$EntityItem;Lcom/google/android/apiary/ItemAndEntityHandler;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Lcom/google/android/apiary/ParseException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1274
    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .local v18, "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    :try_start_2
    new-instance v53, Ljava/lang/Thread;

    const-string v5, "EntityReader"

    move-object/from16 v0, v53

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1275
    .local v53, "entityReaderThread":Ljava/lang/Thread;
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Thread;->start()V

    .line 1277
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1278
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "starting processing of fetched entries"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1281
    :cond_5
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->waitForEnvelope()V

    .line 1282
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1283
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "Feed envelope parsed"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    :cond_6
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->getTimeZone()Ljava/lang/String;

    move-result-object v69

    .line 1287
    .local v69, "timeZone":Ljava/lang/String;
    if-eqz v69, :cond_8

    .line 1288
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1289
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Setting timezone to "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v69

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1291
    :cond_7
    move-object/from16 v0, v69

    move-object/from16 v1, p6

    iput-object v0, v1, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    .line 1293
    :cond_8
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->getDefaultReminders()Ljava/util/List;

    move-result-object v43

    .line 1294
    .local v43, "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    if-eqz v43, :cond_9

    .line 1295
    move-object/from16 v0, v43

    move-object/from16 v1, p6

    iput-object v0, v1, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    .line 1298
    :cond_9
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v26

    .line 1299
    .local v26, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v5, "google_calendar_sync_num_applications_per_batch"

    const-wide/16 v8, 0x14

    move-object/from16 v0, v17

    invoke-static {v0, v5, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v58

    .line 1304
    .local v58, "numApplicationsPerBatch":J
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->getAccessRole()Ljava/lang/String;

    move-result-object v42

    .line 1305
    .local v42, "accessRole":Ljava/lang/String;
    if-eqz v42, :cond_1d

    .line 1306
    invoke-static/range {v42 .. v42}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->getAccessLevel(Ljava/lang/String;)I

    move-result v41

    .line 1307
    .local v41, "accessLevel":I
    move-object/from16 v0, p6

    iget v5, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->accessLevel:I

    move/from16 v0, v41

    if-eq v0, v5, :cond_1d

    .line 1308
    new-instance v70, Landroid/content/ContentValues;

    invoke-direct/range {v70 .. v70}, Landroid/content/ContentValues;-><init>()V

    .line 1309
    .local v70, "values":Landroid/content/ContentValues;
    const-string v5, "calendar_access_level"

    invoke-static/range {v41 .. v41}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v70

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1310
    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p6

    iget-wide v8, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-static {v5, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v70

    invoke-static {v0, v5, v1, v6, v8}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1318
    const/16 v5, 0x64

    move/from16 v0, v41

    if-eq v0, v5, :cond_a

    move-object/from16 v0, p6

    iget v5, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->accessLevel:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_1d

    .line 1320
    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    move-object/from16 v3, p7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->resetSyncStateForFeed(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Ljava/lang/String;)V

    .line 1321
    const-string v5, "com.android.calendar"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v5, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1322
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Access level changed for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ". "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "Requesting full sync"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1445
    if-eqz v56, :cond_b

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_b

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1447
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .local v48, "endTimeMs":J
    if-eqz v56, :cond_1c

    const-string v46, "SUCCESS"

    .line 1449
    .local v46, "disposition":Ljava/lang/String;
    :goto_2
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_c
    if-eqz v4, :cond_d

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_d
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1206
    .end local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .end local v11    # "maxAttendees":I
    .end local v12    # "maxResults":I
    .end local v13    # "handler":Lcom/google/android/syncadapters/calendar/EventHandler;
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v41    # "accessLevel":I
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v57    # "fetcherThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v69    # "timeZone":Ljava/lang/String;
    .end local v70    # "values":Landroid/content/ContentValues;
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    :pswitch_1
    :try_start_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateProviderForInitialSync(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;)V

    .line 1208
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1209
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Performing initial sync on calendarId: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p6

    iget-wide v8, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": window end = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v44

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    :cond_e
    const-string v5, "window_end"

    move-wide/from16 v0, v44

    invoke-virtual {v10, v5, v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V
    :try_end_3
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/apiary/ParseException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_1

    .line 1428
    .end local v67    # "syncMode":I
    :catch_0
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    move-object/from16 v4, v55

    .line 1431
    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .local v47, "e":Lcom/google/android/apiary/AuthenticationException;
    :goto_3
    :try_start_4
    throw v47
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1445
    .end local v47    # "e":Lcom/google/android/apiary/AuthenticationException;
    :catchall_0
    move-exception v5

    :goto_4
    if-eqz v56, :cond_f

    const-string v6, "CalendarSyncAdapter"

    const/4 v8, 0x2

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_f

    const-string v6, "CalendarSyncAdapterP"

    const/4 v8, 0x2

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1447
    :cond_f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_3e

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_5
    const-string v6, "CalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": processed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v60

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " records in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v14, v48, v64

    invoke-virtual {v8, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms from feed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p7

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", updated time is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "feed_updated_time"

    const/4 v14, 0x0

    invoke-virtual {v10, v9, v14}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_10
    if-eqz v4, :cond_11

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_11
    if-eqz v18, :cond_12

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    :cond_12
    throw v5

    .line 1216
    .end local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v67    # "syncMode":I
    :pswitch_2
    :try_start_5
    const-string v5, "new_window_end"

    move-wide/from16 v0, v44

    invoke-virtual {v10, v5, v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V
    :try_end_5
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/android/apiary/ParseException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_b
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_1

    .line 1432
    .end local v67    # "syncMode":I
    :catch_1
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    move-object/from16 v4, v55

    .line 1433
    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .local v47, "e":Ljava/io/IOException;
    :goto_6
    :try_start_6
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "getServerDiffs failed"

    move-object/from16 v0, v47

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1434
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v8, v14

    iput-wide v8, v5, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1445
    if-eqz v56, :cond_13

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_13

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1447
    :cond_13
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_3a

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_7
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_14
    if-eqz v4, :cond_15

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_15
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1221
    .end local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v47    # "e":Ljava/io/IOException;
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v67    # "syncMode":I
    :pswitch_3
    :try_start_7
    new-instance v66, Landroid/os/Bundle;

    invoke-direct/range {v66 .. v66}, Landroid/os/Bundle;-><init>()V

    .line 1222
    .local v66, "syncExtras":Landroid/os/Bundle;
    const-string v5, "moveWindow"

    const/4 v6, 0x1

    move-object/from16 v0, v66

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1223
    const-string v5, "feed"

    move-object/from16 v0, v66

    move-object/from16 v1, p7

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226
    const-string v5, "force"

    const/4 v6, 0x1

    move-object/from16 v0, v66

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1227
    const/4 v5, 0x0

    const-string v6, "com.android.calendar"

    move-object/from16 v0, v66

    invoke-static {v5, v6, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_7
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/android/apiary/ParseException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_1

    .line 1435
    .end local v66    # "syncExtras":Landroid/os/Bundle;
    .end local v67    # "syncMode":I
    :catch_2
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    move-object/from16 v4, v55

    .line 1436
    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .local v47, "e":Landroid/os/RemoteException;
    :goto_8
    :try_start_8
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "getServerDiffs failed"

    move-object/from16 v0, v47

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1437
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v8, v14

    iput-wide v8, v5, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1445
    if-eqz v56, :cond_16

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_16

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1447
    :cond_16
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_3b

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_9
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_17
    if-eqz v4, :cond_18

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_18
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1230
    .end local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v47    # "e":Landroid/os/RemoteException;
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v67    # "syncMode":I
    :pswitch_4
    const-wide/16 v8, 0x0

    cmp-long v5, v44, v8

    if-gtz v5, :cond_4

    .line 1232
    :try_start_9
    const-string v5, "window_end"

    const-wide/16 v8, 0x0

    invoke-virtual {v10, v5, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V
    :try_end_9
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/google/android/apiary/ParseException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_1

    .line 1438
    .end local v67    # "syncMode":I
    :catch_3
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    move-object/from16 v4, v55

    .line 1439
    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .local v47, "e":Lcom/google/android/apiary/ParseException;
    :goto_a
    :try_start_a
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "getServerDiffs failed"

    move-object/from16 v0, v47

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1440
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v14, 0x1

    add-long/2addr v8, v14

    iput-wide v8, v5, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1445
    if-eqz v56, :cond_19

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_19

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1447
    :cond_19
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_3c

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_b
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_1a
    if-eqz v4, :cond_1b

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_1b
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1448
    .end local v47    # "e":Lcom/google/android/apiary/ParseException;
    .restart local v11    # "maxAttendees":I
    .restart local v12    # "maxResults":I
    .restart local v13    # "handler":Lcom/google/android/syncadapters/calendar/EventHandler;
    .restart local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v41    # "accessLevel":I
    .restart local v42    # "accessRole":Ljava/lang/String;
    .restart local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v48    # "endTimeMs":J
    .restart local v53    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v57    # "fetcherThread":Ljava/lang/Thread;
    .restart local v58    # "numApplicationsPerBatch":J
    .restart local v67    # "syncMode":I
    .restart local v69    # "timeZone":Ljava/lang/String;
    .restart local v70    # "values":Landroid/content/ContentValues;
    :cond_1c
    const-string v46, "FAILURE"

    goto/16 :goto_2

    .line 1331
    .end local v41    # "accessLevel":I
    .end local v48    # "endTimeMs":J
    .end local v70    # "values":Landroid/content/ContentValues;
    :cond_1d
    :try_start_b
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v27

    .line 1333
    .local v27, "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    :cond_1e
    :goto_c
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v5

    if-eqz v5, :cond_24

    .line 1334
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1335
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getServerDiffs: noticed a cancel during feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", bailing out"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_b .. :try_end_b} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_a
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1445
    :cond_1f
    if-eqz v56, :cond_20

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_20

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 1447
    :cond_20
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_23

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_d
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_21
    if-eqz v4, :cond_22

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_22
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1448
    .restart local v48    # "endTimeMs":J
    :cond_23
    const-string v46, "FAILURE"

    goto :goto_d

    .line 1343
    .end local v48    # "endTimeMs":J
    :cond_24
    :try_start_c
    invoke-interface/range {v22 .. v22}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v51

    check-cast v51, Lcom/google/android/apiary/EntityReader$EntityItem;

    .line 1344
    .local v51, "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    sget-object v5, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sEntityEndMarker:Lcom/google/android/apiary/EntityReader$EntityItem;

    move-object/from16 v0, v51

    if-ne v0, v5, :cond_28

    move-object/from16 v23, p0

    move-object/from16 v24, p2

    move-object/from16 v25, v13

    move-object/from16 v28, p4

    move-object/from16 v29, p6

    move-object/from16 v30, p5

    move-object/from16 v31, v10

    .line 1346
    invoke-direct/range {v23 .. v31}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/EventHandler;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V

    .line 1407
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->isPartialSyncUnavailable()Z

    move-result v5

    if-eqz v5, :cond_2f

    .line 1408
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    move-object/from16 v3, p7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->resetSyncStateForFeed(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Ljava/lang/String;)V

    .line 1409
    const-string v5, "com.android.calendar"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v5, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1410
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Partial sync unavailable for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ". Requesting full sync"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_c .. :try_end_c} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_a
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1445
    if-eqz v56, :cond_25

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_25

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1447
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_2e

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_e
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_26
    if-eqz v4, :cond_27

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_27
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1357
    :cond_28
    :try_start_d
    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1358
    move-object/from16 v0, v51

    iget-object v0, v0, Lcom/google/android/apiary/EntityReader$EntityItem;->entry:Ljava/lang/Object;

    move-object/from16 v30, v0

    check-cast v30, Lcom/google/api/services/calendar/model/Event;

    .line 1359
    .local v30, "entry":Lcom/google/api/services/calendar/model/Event;
    move-object/from16 v0, v51

    iget-object v0, v0, Lcom/google/android/apiary/EntityReader$EntityItem;->entity:Landroid/content/Entity;

    move-object/from16 v31, v0

    .line 1360
    .local v31, "entity":Landroid/content/Entity;
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_29

    const-string v5, "CalendarSyncAdapterFine"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 1361
    :cond_29
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "calling applyItemToEntity for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v30 .. v30}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    :cond_2a
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I
    :try_end_d
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_d .. :try_end_d} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_a
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result v61

    .line 1365
    .local v61, "previousSize":I
    const/16 v32, 0x0

    move-object/from16 v28, v13

    move-object/from16 v29, v26

    move-object/from16 v33, p4

    move-object/from16 v34, p6

    :try_start_e
    invoke-virtual/range {v28 .. v34}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    :try_end_e
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_7
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_e .. :try_end_e} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_a
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1380
    :try_start_f
    const-string v5, "lastFetchedId"

    invoke-virtual/range {v30 .. v30}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v5, v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1382
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_2b

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 1383
    :cond_2b
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Processing event "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v30 .. v30}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1385
    :cond_2c
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->newUpdateOperation()Landroid/content/ContentProviderOperation;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1386
    add-int/lit8 v60, v60, 0x1

    .line 1387
    move-object/from16 v0, p4

    iget-object v5, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v5, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v14, 0x1

    add-long/2addr v8, v14

    iput-wide v8, v5, Landroid/content/SyncStats;->numEntries:J

    .line 1388
    move/from16 v0, v60

    int-to-long v8, v0

    rem-long v8, v8, v58

    const-wide/16 v14, 0x0

    cmp-long v5, v8, v14

    if-nez v5, :cond_1e

    move-object/from16 v32, p0

    move-object/from16 v33, p2

    move-object/from16 v34, v13

    move-object/from16 v35, v26

    move-object/from16 v36, v27

    move-object/from16 v37, p4

    move-object/from16 v38, p6

    move-object/from16 v39, p5

    move-object/from16 v40, v10

    .line 1389
    invoke-direct/range {v32 .. v40}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/EventHandler;Ljava/util/ArrayList;Ljava/util/List;Landroid/content/SyncResult;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)V

    .line 1398
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_2d

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1400
    :cond_2d
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Applied "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c

    .line 1428
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .end local v30    # "entry":Lcom/google/api/services/calendar/model/Event;
    .end local v31    # "entity":Landroid/content/Entity;
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v61    # "previousSize":I
    .end local v69    # "timeZone":Ljava/lang/String;
    :catch_4
    move-exception v47

    goto/16 :goto_3

    .line 1371
    .restart local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .restart local v30    # "entry":Lcom/google/api/services/calendar/model/Event;
    .restart local v31    # "entity":Landroid/content/Entity;
    .restart local v42    # "accessRole":Ljava/lang/String;
    .restart local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v53    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v58    # "numApplicationsPerBatch":J
    .restart local v61    # "previousSize":I
    .restart local v69    # "timeZone":Ljava/lang/String;
    :catch_5
    move-exception v68

    .line 1374
    .local v68, "t":Ljava/lang/NullPointerException;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v61

    move-object/from16 v3, v68

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->skipEntry(Ljava/util/List;ILjava/lang/Throwable;)V

    goto/16 :goto_c

    .line 1432
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .end local v30    # "entry":Lcom/google/api/services/calendar/model/Event;
    .end local v31    # "entity":Landroid/content/Entity;
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v61    # "previousSize":I
    .end local v68    # "t":Ljava/lang/NullPointerException;
    .end local v69    # "timeZone":Ljava/lang/String;
    :catch_6
    move-exception v47

    goto/16 :goto_6

    .line 1376
    .restart local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .restart local v30    # "entry":Lcom/google/api/services/calendar/model/Event;
    .restart local v31    # "entity":Landroid/content/Entity;
    .restart local v42    # "accessRole":Ljava/lang/String;
    .restart local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v53    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v58    # "numApplicationsPerBatch":J
    .restart local v61    # "previousSize":I
    .restart local v69    # "timeZone":Ljava/lang/String;
    :catch_7
    move-exception v68

    .line 1377
    .local v68, "t":Ljava/lang/IllegalArgumentException;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v61

    move-object/from16 v3, v68

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->skipEntry(Ljava/util/List;ILjava/lang/Throwable;)V
    :try_end_f
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_f .. :try_end_f} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_a
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_c

    .line 1435
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .end local v30    # "entry":Lcom/google/api/services/calendar/model/Event;
    .end local v31    # "entity":Landroid/content/Entity;
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v61    # "previousSize":I
    .end local v68    # "t":Ljava/lang/IllegalArgumentException;
    .end local v69    # "timeZone":Ljava/lang/String;
    :catch_8
    move-exception v47

    goto/16 :goto_8

    .line 1448
    .restart local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .restart local v42    # "accessRole":Ljava/lang/String;
    .restart local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v48    # "endTimeMs":J
    .restart local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v53    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v58    # "numApplicationsPerBatch":J
    .restart local v69    # "timeZone":Ljava/lang/String;
    :cond_2e
    const-string v46, "FAILURE"

    goto/16 :goto_e

    .line 1413
    .end local v48    # "endTimeMs":J
    :cond_2f
    :try_start_10
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->isIoException()Z

    move-result v5

    if-eqz v5, :cond_30

    .line 1414
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Connection failed during feed read of "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1438
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v69    # "timeZone":Ljava/lang/String;
    :catch_9
    move-exception v47

    goto/16 :goto_a

    .line 1416
    .restart local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .restart local v42    # "accessRole":Ljava/lang/String;
    .restart local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v53    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v58    # "numApplicationsPerBatch":J
    .restart local v69    # "timeZone":Ljava/lang/String;
    :cond_30
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->isAuthenticationFailed()Z

    move-result v5

    if-eqz v5, :cond_34

    .line 1417
    new-instance v5, Lcom/google/android/apiary/AuthenticationException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Authentication error reading feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apiary/AuthenticationException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_10
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_10} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_10 .. :try_end_10} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_a
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 1441
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v69    # "timeZone":Ljava/lang/String;
    :catch_a
    move-exception v47

    .line 1442
    .end local v11    # "maxAttendees":I
    .end local v12    # "maxResults":I
    .end local v13    # "handler":Lcom/google/android/syncadapters/calendar/EventHandler;
    .end local v57    # "fetcherThread":Ljava/lang/Thread;
    .end local v67    # "syncMode":I
    .local v47, "e":Ljava/lang/InterruptedException;
    :goto_f
    :try_start_11
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "getServerDiffs interrupted"

    move-object/from16 v0, v47

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1443
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 1445
    if-eqz v56, :cond_31

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_31

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 1447
    :cond_31
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_3d

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_10
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_32
    if-eqz v4, :cond_33

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_33
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1420
    .end local v47    # "e":Ljava/lang/InterruptedException;
    .restart local v11    # "maxAttendees":I
    .restart local v12    # "maxResults":I
    .restart local v13    # "handler":Lcom/google/android/syncadapters/calendar/EventHandler;
    .restart local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .restart local v42    # "accessRole":Ljava/lang/String;
    .restart local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v53    # "entityReaderThread":Ljava/lang/Thread;
    .restart local v57    # "fetcherThread":Ljava/lang/Thread;
    .restart local v58    # "numApplicationsPerBatch":J
    .restart local v67    # "syncMode":I
    .restart local v69    # "timeZone":Ljava/lang/String;
    :cond_34
    :try_start_12
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->isResourceUnavailable()Z

    move-result v5

    if-eqz v5, :cond_35

    .line 1421
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->getRetryAfter()J

    move-result-wide v62

    .line 1422
    .local v62, "retryAfter":J
    move-object/from16 v0, p4

    iget-wide v8, v0, Landroid/content/SyncResult;->delayUntil:J

    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    move-object/from16 v0, p4

    iput-wide v8, v0, Landroid/content/SyncResult;->delayUntil:J

    .line 1423
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Resource unavailable, retryAfter = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v62

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1425
    .end local v62    # "retryAfter":J
    :cond_35
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->getLastUpdated()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v32, p0

    move-object/from16 v33, p1

    move-object/from16 v34, p2

    move-object/from16 v35, p5

    move-object/from16 v37, p7

    move-object/from16 v38, p6

    invoke-direct/range {v32 .. v38}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateSyncStateAfterFeedRead(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;)V
    :try_end_12
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_12 .. :try_end_12} :catch_4
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_12} :catch_8
    .catch Lcom/google/android/apiary/ParseException; {:try_start_12 .. :try_end_12} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_a
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 1427
    const/16 v56, 0x1

    .line 1445
    if-eqz v56, :cond_36

    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_36

    const-string v5, "CalendarSyncAdapterP"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 1447
    :cond_36
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v48

    .line 1448
    .restart local v48    # "endTimeMs":J
    if-eqz v56, :cond_39

    const-string v46, "SUCCESS"

    .line 1449
    .restart local v46    # "disposition":Ljava/lang/String;
    :goto_11
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v46

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ": processed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v60

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " records in "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v48, v64

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ms from feed "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", updated time is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "feed_updated_time"

    const/4 v9, 0x0

    invoke-virtual {v10, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    .end local v46    # "disposition":Ljava/lang/String;
    .end local v48    # "endTimeMs":J
    :cond_37
    if-eqz v4, :cond_38

    .line 1456
    invoke-virtual {v4}, Lcom/google/android/syncadapters/calendar/EventFeedFetcher;->close()V

    .line 1458
    :cond_38
    if-eqz v18, :cond_0

    .line 1459
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apiary/EntityReader;->close()V

    goto/16 :goto_0

    .line 1448
    .restart local v48    # "endTimeMs":J
    :cond_39
    const-string v46, "FAILURE"

    goto :goto_11

    .end local v11    # "maxAttendees":I
    .end local v12    # "maxResults":I
    .end local v13    # "handler":Lcom/google/android/syncadapters/calendar/EventHandler;
    .end local v26    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v27    # "entityItemsInBatch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;>;"
    .end local v42    # "accessRole":Ljava/lang/String;
    .end local v43    # "defaultReminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .end local v51    # "entityItem":Lcom/google/android/apiary/EntityReader$EntityItem;, "Lcom/google/android/apiary/EntityReader$EntityItem<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v53    # "entityReaderThread":Ljava/lang/Thread;
    .end local v57    # "fetcherThread":Ljava/lang/Thread;
    .end local v58    # "numApplicationsPerBatch":J
    .end local v67    # "syncMode":I
    .end local v69    # "timeZone":Ljava/lang/String;
    .local v47, "e":Ljava/io/IOException;
    :cond_3a
    const-string v46, "FAILURE"

    goto/16 :goto_7

    .local v47, "e":Landroid/os/RemoteException;
    :cond_3b
    const-string v46, "FAILURE"

    goto/16 :goto_9

    .local v47, "e":Lcom/google/android/apiary/ParseException;
    :cond_3c
    const-string v46, "FAILURE"

    goto/16 :goto_b

    .local v47, "e":Ljava/lang/InterruptedException;
    :cond_3d
    const-string v46, "FAILURE"

    goto/16 :goto_10

    .end local v47    # "e":Ljava/lang/InterruptedException;
    :cond_3e
    const-string v46, "FAILURE"

    goto/16 :goto_5

    .line 1445
    .end local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v48    # "endTimeMs":J
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    :catchall_1
    move-exception v5

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    move-object/from16 v4, v55

    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    goto/16 :goto_4

    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v11    # "maxAttendees":I
    .restart local v12    # "maxResults":I
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v67    # "syncMode":I
    :catchall_2
    move-exception v5

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    goto/16 :goto_4

    .line 1441
    .end local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .end local v11    # "maxAttendees":I
    .end local v12    # "maxResults":I
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .end local v67    # "syncMode":I
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    :catch_b
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    move-object/from16 v4, v55

    .end local v55    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    .restart local v4    # "eventFetcher":Lcom/google/android/syncadapters/calendar/EventFeedFetcher;
    goto/16 :goto_f

    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v11    # "maxAttendees":I
    .restart local v12    # "maxResults":I
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v67    # "syncMode":I
    :catch_c
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    goto/16 :goto_f

    .line 1438
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    :catch_d
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    goto/16 :goto_a

    .line 1435
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    :catch_e
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    goto/16 :goto_8

    .line 1432
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    :catch_f
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    goto/16 :goto_6

    .line 1428
    .end local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    :catch_10
    move-exception v47

    move-object/from16 v18, v52

    .end local v52    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    .restart local v18    # "entityReader":Lcom/google/android/apiary/EntityReader;, "Lcom/google/android/apiary/EntityReader<Lcom/google/api/services/calendar/model/Event;>;"
    goto/16 :goto_3

    .line 1203
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSyncWindowEnd()J
    .locals 12

    .prologue
    .line 1850
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 1851
    .local v6, "cr":Landroid/content/ContentResolver;
    const-string v4, "google_calendar_sync_window_update_days2"

    const-wide/16 v10, 0x1e

    invoke-static {v6, v4, v10, v11}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 1854
    .local v8, "updateDays":J
    const-wide/32 v4, 0x5265c00

    mul-long v2, v8, v4

    .line 1855
    .local v2, "advanceInterval":J
    const-string v4, "google_calendar_sync_window_days2"

    const-wide/16 v10, 0x16d

    invoke-static {v6, v4, v10, v11}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    add-long v0, v4, v8

    .line 1858
    .local v0, "window":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getSyncWindowEnd(JJJ)J

    move-result-wide v4

    return-wide v4
.end method

.method static getSyncWindowEnd(JJJ)J
    .locals 8
    .param p0, "window"    # J
    .param p2, "advanceInterval"    # J
    .param p4, "now"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 1872
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1873
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSyncWindowEnd: window: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", advanceInterval: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", now: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    :cond_0
    cmp-long v4, p0, v2

    if-lez v4, :cond_1

    cmp-long v4, p2, v2

    if-lez v4, :cond_1

    .line 1879
    const-wide/32 v2, 0x5265c00

    mul-long/2addr v2, p0

    add-long v0, p4, v2

    .line 1886
    .local v0, "endOfWindow":J
    div-long v2, v0, p2

    mul-long/2addr v2, p2

    .line 1888
    .end local v0    # "endOfWindow":J
    :cond_1
    return-wide v2
.end method

.method private insertBatch(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentProviderClient;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .local p3, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const/4 v4, 0x2

    .line 1779
    const-string v1, "CalendarSyncAdapter"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1780
    const-string v1, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bulk inserting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1782
    :cond_0
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/ContentValues;

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/content/ContentValues;

    invoke-static {p2, v2, v1}, Lcom/google/android/apiary/ProviderHelper;->bulkInsertProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    .line 1786
    .local v0, "count":I
    const-string v1, "CalendarSyncAdapter"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1787
    const-string v1, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Inserted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1789
    :cond_1
    return-void
.end method

.method private insertColor(Landroid/accounts/Account;Ljava/util/ArrayList;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/Integer;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "colorType"    # I
    .param p5, "colorKey"    # Ljava/lang/String;
    .param p6, "newColor"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/net/Uri;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 610
    .local p2, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 611
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "account_name"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    const-string v1, "account_type"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    const-string v1, "color_type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 614
    const-string v1, "color_index"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v1, "color"

    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 616
    invoke-static {p3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    return-void
.end method

.method private isAuthenticationException(Lcom/google/api/client/http/HttpResponseException;)Z
    .locals 2
    .param p1, "e"    # Lcom/google/api/client/http/HttpResponseException;

    .prologue
    .line 1057
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isCanceled()Z
    .locals 1

    .prologue
    .line 704
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    return v0
.end method

.method static isSameCalendarData(Landroid/content/ContentValues;Landroid/content/ContentValues;)Z
    .locals 1
    .param p0, "oldValues"    # Landroid/content/ContentValues;
    .param p1, "newValues"    # Landroid/content/ContentValues;

    .prologue
    .line 1807
    const-string v0, "name"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "calendar_displayName"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "calendar_timezone"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "calendar_color_index"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "calendar_color"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "calendar_access_level"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cal_sync1"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cal_sync4"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cal_sync5"

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSameValue(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 2
    .param p0, "oldValues"    # Landroid/content/ContentValues;
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 1820
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821
    invoke-virtual {p0, p2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 1823
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSyncable(Landroid/accounts/Account;)Z
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 714
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "cl"

    invoke-static {v5}, Lcom/google/android/gsf/GoogleLoginServiceConstants;->featureForService(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, p1, v3, v4, v5}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v1

    invoke-interface {v1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 726
    :goto_0
    return v1

    .line 722
    :catch_0
    move-exception v0

    .line 723
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 724
    .end local v0    # "e":Landroid/accounts/AuthenticatorException;
    :catch_1
    move-exception v0

    .line 725
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "CalendarSyncAdapter"

    const-string v3, "Operation cancelled while querying AccountManager"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v2

    .line 726
    goto :goto_0
.end method

.method private loadGsfColors(Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .param p1, "gsfKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 690
    .local p2, "colorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, p1, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 694
    .local v1, "gsfString":Ljava/lang/String;
    invoke-interface {p2}, Ljava/util/Map;->clear()V

    .line 695
    if-eqz v1, :cond_0

    .line 696
    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getColorMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 697
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 698
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 701
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    return-void
.end method

.method private logErrorWithEntity(Ljava/lang/String;Landroid/content/Entity;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "entity"    # Landroid/content/Entity;
    .param p3, "e"    # Ljava/lang/Exception;

    .prologue
    .line 2342
    const-string v3, "CalendarSyncAdapter"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2343
    const-string v3, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2352
    :goto_0
    return-void

    .line 2345
    :cond_0
    invoke-virtual {p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    .line 2346
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 2347
    .local v0, "id":Ljava/lang/Long;
    const-string v3, "_sync_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2348
    .local v1, "syncId":Ljava/lang/String;
    const-string v3, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_sync_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private performSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 33
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 375
    const-string v7, "CalendarSyncAdapter"

    const/4 v11, 0x3

    invoke-static {v7, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 376
    invoke-virtual/range {p2 .. p2}, Landroid/os/Bundle;->isEmpty()Z

    .line 377
    const-string v7, "CalendarSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "PerformSync for account: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, ", with extras: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v28

    .line 382
    .local v28, "isSyncable":I
    if-gez v28, :cond_1

    .line 383
    invoke-direct/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSyncable(Landroid/accounts/Account;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/16 v28, 0x1

    .line 384
    :goto_0
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 387
    const-string v7, "com.android.calendar"

    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v7, v11}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 391
    :cond_1
    const-string v7, "initialize"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 517
    :cond_2
    :goto_1
    return-void

    .line 383
    :cond_3
    const/16 v28, 0x0

    goto :goto_0

    .line 396
    :cond_4
    if-lez v28, :cond_2

    .line 401
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->cleanupForUnsyncedCalendars(Landroid/accounts/Account;Landroid/content/ContentProviderClient;)V

    .line 404
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    .line 405
    .local v17, "contentResolver":Landroid/content/ContentResolver;
    const-string v7, "com.android.calendar"

    const-string v11, "cl"

    invoke-direct/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getExpectedFeeds(Landroid/accounts/Account;)Ljava/util/HashSet;

    move-result-object v13

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1, v7, v11, v13}, Lcom/google/android/gsf/SubscribedFeeds;->manageSubscriptions(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Z

    .line 408
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v7

    if-nez v7, :cond_2

    .line 412
    const-string v7, "deletions_override"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 414
    .local v10, "overrideTooManyDeletions":Z
    const-string v7, "discard_deletions"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v27

    .line 416
    .local v27, "discardLocalDeletions":Z
    if-eqz v27, :cond_5

    .line 417
    new-instance v32, Landroid/content/ContentValues;

    invoke-direct/range {v32 .. v32}, Landroid/content/ContentValues;-><init>()V

    .line 418
    .local v32, "values":Landroid/content/ContentValues;
    const-string v7, "deleted"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v32

    invoke-virtual {v0, v7, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 419
    sget-object v7, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v7, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v7

    const-string v11, "deleted=1"

    const/4 v13, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, v32

    invoke-static {v0, v7, v1, v11, v13}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 427
    .end local v32    # "values":Landroid/content/ContentValues;
    :cond_5
    const-string v7, "upload"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v31

    .line 429
    .local v31, "uploadOnly":Z
    if-nez v31, :cond_6

    .line 430
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getServerDiffs(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V

    .line 431
    invoke-virtual/range {p5 .. p5}, Landroid/content/SyncResult;->hasError()Z

    move-result v7

    if-nez v7, :cond_2

    .line 435
    :cond_6
    const-string v7, "google_calendar_sync_max_loop_attempts"

    const-wide/16 v14, 0x6

    move-object/from16 v0, v17

    invoke-static {v0, v7, v14, v15}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 440
    .local v8, "maxLoopAttempts":J
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v6

    .line 443
    .local v6, "activeTag":I
    const/high16 v7, 0x1000000

    or-int/2addr v7, v6

    invoke-static {v7}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 445
    :try_start_0
    const-string v7, "CalendarSyncAdapter"

    const/4 v11, 0x3

    invoke-static {v7, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 446
    const-string v7, "CalendarSyncAdapter"

    const-string v11, "Local Calendar changes"

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_7
    new-instance v12, Lcom/google/android/syncadapters/calendar/CalendarHandler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-direct {v12, v7, v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarHandler;-><init>(Lcom/google/api/services/calendar/Calendar;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    move-object/from16 v7, p0

    move-object/from16 v11, p4

    move-object/from16 v13, p5

    invoke-direct/range {v7 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->processLocalChangesForHandler(JZLandroid/content/ContentProviderClient;Lcom/google/android/apiary/ItemAndEntityHandler;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455
    const/high16 v7, 0x1000000

    or-int/2addr v7, v6

    const/4 v11, 0x1

    invoke-static {v7, v11}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 457
    invoke-static {v6}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 461
    const/high16 v7, 0x2000000

    or-int/2addr v7, v6

    invoke-static {v7}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 463
    :try_start_1
    const-string v7, "CalendarSyncAdapter"

    const/4 v11, 0x3

    invoke-static {v7, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 464
    const-string v7, "CalendarSyncAdapter"

    const-string v11, "Local Event changes"

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_8
    sget-object v7, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v7, v0}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addCallerIsSyncAdapterParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v12

    .line 474
    .local v12, "url":Landroid/net/Uri;
    const/4 v7, 0x1

    new-array v13, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v11, "_id"

    aput-object v11, v13, v7

    const-string v14, "dirty=1 AND lastSynced=1"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v11, p4

    invoke-virtual/range {v11 .. v16}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v26

    .line 480
    .local v26, "cursor":Landroid/database/Cursor;
    if-eqz v26, :cond_a

    .line 482
    :try_start_2
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v30

    .line 483
    .local v30, "numEvents":I
    const-string v7, "CalendarSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Found "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v30

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " events marked dirty & lastSynced"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    if-lez v30, :cond_9

    .line 485
    const-string v7, "dirty=1 AND lastSynced=1"

    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v12, v7, v11}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v29

    .line 489
    .local v29, "n":I
    if-lez v29, :cond_9

    .line 490
    const-string v7, "CalendarSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Deleted "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " events marked dirty & lastSynced"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 494
    .end local v29    # "n":I
    :cond_9
    :try_start_3
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 498
    .end local v30    # "numEvents":I
    :cond_a
    new-instance v13, Lcom/google/android/syncadapters/calendar/EventHandler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    const/16 v18, 0x0

    move-object/from16 v15, p1

    move-object/from16 v16, p4

    invoke-direct/range {v13 .. v18}, Lcom/google/android/syncadapters/calendar/EventHandler;-><init>(Lcom/google/api/services/calendar/Calendar;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/ContentResolver;Ljava/lang/String;)V

    move-object/from16 v19, p0

    move-wide/from16 v20, v8

    move/from16 v22, v10

    move-object/from16 v23, p4

    move-object/from16 v24, v13

    move-object/from16 v25, p5

    invoke-direct/range {v19 .. v25}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->processLocalChangesForHandler(JZLandroid/content/ContentProviderClient;Lcom/google/android/apiary/ItemAndEntityHandler;Landroid/content/SyncResult;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 505
    const/high16 v7, 0x2000000

    or-int/2addr v7, v6

    const/4 v11, 0x1

    invoke-static {v7, v11}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 507
    invoke-static {v6}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 510
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v7

    if-nez v7, :cond_2

    .line 514
    const-string v7, "CalendarSyncAdapter"

    const/4 v11, 0x3

    invoke-static {v7, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 515
    const-string v7, "CalendarSyncAdapter"

    const-string v11, "PerformSync: sync is complete"

    invoke-static {v7, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 455
    .end local v12    # "url":Landroid/net/Uri;
    .end local v26    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v7

    const/high16 v11, 0x1000000

    or-int/2addr v11, v6

    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 457
    invoke-static {v6}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v7

    .line 494
    .restart local v12    # "url":Landroid/net/Uri;
    .restart local v26    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v7

    :try_start_4
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 505
    .end local v12    # "url":Landroid/net/Uri;
    .end local v26    # "cursor":Landroid/database/Cursor;
    :catchall_2
    move-exception v7

    const/high16 v11, 0x2000000

    or-int/2addr v11, v6

    const/4 v13, 0x1

    invoke-static {v11, v13}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 507
    invoke-static {v6}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v7
.end method

.method private processAccountCalendar(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/api/services/calendar/model/CalendarListEntry;ZLjava/util/HashSet;Ljava/util/ArrayList;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/os/Bundle;)V
    .locals 34
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "calendarEntry"    # Lcom/google/api/services/calendar/model/CalendarListEntry;
    .param p4, "initialSync"    # Z
    .param p7, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p8, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentProviderClient;",
            "Lcom/google/api/services/calendar/model/CalendarListEntry;",
            "Z",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1551
    .local p5, "calendarsNotSeenYet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    .local p6, "inserts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1552
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Calendar: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1554
    :cond_0
    new-instance v33, Landroid/content/ContentValues;

    invoke-direct/range {v33 .. v33}, Landroid/content/ContentValues;-><init>()V

    .line 1555
    .local v33, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p3

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarHandler;->calendarEntryToContentValues(Lcom/google/api/services/calendar/model/CalendarListEntry;Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v14

    .line 1558
    .local v14, "calendarId":Ljava/lang/String;
    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "account_type=?"

    aput-object v8, v4, v7

    const/4 v7, 0x1

    const-string v8, "cal_sync1=?"

    aput-object v8, v4, v7

    invoke-static {v4}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v9, v8, v4

    const/4 v4, 0x1

    aput-object v14, v8, v4

    const-string v9, "calendar_access_level"

    move-object/from16 v4, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1565
    .local v13, "c":Landroid/database/Cursor;
    if-eqz v13, :cond_14

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_14

    .line 1568
    const/16 v18, 0x1

    .line 1569
    .local v18, "isNewFeedForAccount":Z
    :cond_1
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1570
    const/4 v4, 0x1

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1572
    .local v12, "accountName":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1573
    const-string v4, "canPartiallyUpdate"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1574
    const/16 v18, 0x0

    .line 1579
    .end local v12    # "accountName":Ljava/lang/String;
    :cond_2
    if-nez v18, :cond_7

    .line 1580
    const/4 v4, 0x0

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1586
    .local v10, "_id":J
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1589
    const/4 v4, 0x2

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_6

    const/16 v30, 0x1

    .line 1592
    .local v30, "syncDirty":Z
    :goto_0
    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    .line 1593
    .local v28, "oldValues":Landroid/content/ContentValues;
    const-string v4, "calendar_access_level"

    const/4 v5, 0x3

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1595
    const-string v4, "name"

    const/4 v5, 0x6

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    const-string v4, "calendar_displayName"

    const/4 v5, 0x7

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    const-string v4, "calendar_timezone"

    const/16 v5, 0x8

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1601
    const-string v4, "calendar_color_index"

    const/16 v5, 0xa

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1603
    const-string v4, "calendar_color"

    const/16 v5, 0x9

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    const-string v4, "cal_sync1"

    const/16 v5, 0xb

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    const-string v4, "cal_sync4"

    const/16 v5, 0xc

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1609
    const-string v4, "cal_sync5"

    const/16 v5, 0xd

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1612
    move-object/from16 v0, v28

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isSameCalendarData(Landroid/content/ContentValues;Landroid/content/ContentValues;)Z

    move-result v4

    if-nez v4, :cond_a

    if-nez v30, :cond_a

    .line 1613
    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v32

    .line 1616
    .local v32, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1623
    const/4 v4, 0x3

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 1625
    .local v27, "oldAccessLevel":I
    const-string v4, "calendar_access_level"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 1628
    .local v26, "newAccessLevel":I
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1629
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updating existing calendar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    :cond_3
    move/from16 v0, v27

    move/from16 v1, v26

    if-eq v0, v1, :cond_a

    .line 1632
    const/16 v4, 0x64

    move/from16 v0, v27

    if-eq v0, v4, :cond_4

    const/16 v4, 0x64

    move/from16 v0, v26

    if-ne v0, v4, :cond_a

    .line 1634
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getId()Ljava/lang/String;

    move-result-object v15

    .line 1635
    .local v15, "calendarSyncId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2, v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->resetSyncStateForFeed(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Ljava/lang/String;)V

    .line 1636
    const-string v4, "com.android.calendar"

    move-object/from16 v0, p1

    move-object/from16 v1, p8

    invoke-static {v0, v4, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1638
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Access level changed for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Requesting full sync"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1737
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1772
    .end local v10    # "_id":J
    .end local v15    # "calendarSyncId":Ljava/lang/String;
    .end local v18    # "isNewFeedForAccount":Z
    .end local v26    # "newAccessLevel":I
    .end local v27    # "oldAccessLevel":I
    .end local v28    # "oldValues":Landroid/content/ContentValues;
    .end local v30    # "syncDirty":Z
    .end local v32    # "uri":Landroid/net/Uri;
    :cond_5
    :goto_1
    return-void

    .line 1589
    .restart local v10    # "_id":J
    .restart local v18    # "isNewFeedForAccount":Z
    :cond_6
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 1651
    .end local v10    # "_id":J
    :cond_7
    :try_start_1
    const-string v4, "cal_sync4"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    .line 1656
    .local v17, "entrySelected":Z
    invoke-interface {v13}, Landroid/database/Cursor;->moveToLast()Z

    .line 1658
    const/4 v4, 0x3

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1660
    .local v20, "maxAccessLevel":J
    const/4 v4, 0x0

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 1663
    .local v22, "maxAccessLevelCalId":J
    const/4 v4, 0x1

    const/4 v5, 0x5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v4, v5, :cond_b

    const/16 v25, 0x1

    .line 1665
    .local v25, "maxAccessLevelSyncEvents":Z
    :goto_2
    const/4 v4, 0x1

    const/4 v5, 0x4

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v4, v5, :cond_c

    const/16 v24, 0x1

    .line 1670
    .local v24, "maxAccessLevelSelected":Z
    :goto_3
    invoke-virtual/range {p3 .. p3}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getAccessRole()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->getAccessLevel(Ljava/lang/String;)I

    move-result v16

    .line 1673
    .local v16, "entryAccessLevel":I
    move/from16 v0, v16

    int-to-long v4, v0

    cmp-long v4, v20, v4

    if-ltz v4, :cond_d

    .line 1678
    const-string v4, "sync_events"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1679
    const-string v4, "visible"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1681
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1682
    const-string v4, "CalendarSyncAdapter"

    const-string v5, "New feed with lower access level: just need to add it"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1722
    :cond_8
    :goto_4
    const-string v4, "account_name"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1723
    const-string v4, "account_type"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1724
    const-string v4, "canPartiallyUpdate"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1725
    const-string v4, "allowedReminders"

    const-string v5, "0,1,2"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1726
    const-string v4, "allowedAttendeeTypes"

    const-string v5, "0,1,2"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    const-string v4, "allowedAvailability"

    const-string v5, "0,1"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    const-string v4, "maxReminders"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1729
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1730
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding new calendar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1732
    :cond_9
    move-object/from16 v0, p6

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1737
    .end local v16    # "entryAccessLevel":I
    .end local v17    # "entrySelected":Z
    .end local v20    # "maxAccessLevel":J
    .end local v22    # "maxAccessLevelCalId":J
    .end local v24    # "maxAccessLevelSelected":Z
    .end local v25    # "maxAccessLevelSyncEvents":Z
    :cond_a
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1768
    .end local v18    # "isNewFeedForAccount":Z
    :goto_5
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0x64

    if-lt v4, v5, :cond_5

    .line 1769
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->insertBatch(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 1770
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    .line 1663
    .restart local v17    # "entrySelected":Z
    .restart local v18    # "isNewFeedForAccount":Z
    .restart local v20    # "maxAccessLevel":J
    .restart local v22    # "maxAccessLevelCalId":J
    :cond_b
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 1665
    .restart local v25    # "maxAccessLevelSyncEvents":Z
    :cond_c
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 1690
    .restart local v16    # "entryAccessLevel":I
    .restart local v24    # "maxAccessLevelSelected":Z
    :cond_d
    if-nez v25, :cond_e

    if-eqz v24, :cond_f

    .line 1691
    :cond_e
    :try_start_2
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 1692
    .local v19, "mapOldToUpdate":Landroid/content/ContentValues;
    const-string v4, "sync_events"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1693
    const-string v4, "visible"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1695
    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v22

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-static {v0, v4, v1, v5, v6}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1703
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1704
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updating existing calendar that has lower access level"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1713
    .end local v19    # "mapOldToUpdate":Landroid/content/ContentValues;
    :cond_f
    const-string v5, "sync_events"

    if-nez v25, :cond_10

    if-eqz v17, :cond_12

    :cond_10
    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1715
    const-string v5, "visible"

    if-nez v24, :cond_11

    if-eqz v17, :cond_13

    :cond_11
    const/4 v4, 0x1

    :goto_7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1717
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1718
    const-string v4, "CalendarSyncAdapter"

    const-string v5, "New feed with higher access level: swapped visible and sync status"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_4

    .line 1737
    .end local v16    # "entryAccessLevel":I
    .end local v17    # "entrySelected":Z
    .end local v20    # "maxAccessLevel":J
    .end local v22    # "maxAccessLevelCalId":J
    .end local v24    # "maxAccessLevelSelected":Z
    .end local v25    # "maxAccessLevelSyncEvents":Z
    :catchall_0
    move-exception v4

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1713
    .restart local v16    # "entryAccessLevel":I
    .restart local v17    # "entrySelected":Z
    .restart local v20    # "maxAccessLevel":J
    .restart local v22    # "maxAccessLevelCalId":J
    .restart local v24    # "maxAccessLevelSelected":Z
    .restart local v25    # "maxAccessLevelSyncEvents":Z
    :cond_12
    const/4 v4, 0x0

    goto :goto_6

    .line 1715
    :cond_13
    const/4 v4, 0x0

    goto :goto_7

    .line 1741
    .end local v16    # "entryAccessLevel":I
    .end local v17    # "entrySelected":Z
    .end local v18    # "isNewFeedForAccount":Z
    .end local v20    # "maxAccessLevel":J
    .end local v22    # "maxAccessLevelCalId":J
    .end local v24    # "maxAccessLevelSelected":Z
    .end local v25    # "maxAccessLevelSyncEvents":Z
    :cond_14
    if-eqz v13, :cond_15

    .line 1742
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1746
    :cond_15
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    if-eqz v4, :cond_17

    const-string v4, "com.android.calendar"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    const/16 v31, 0x1

    .line 1749
    .local v31, "syncOn":Z
    :goto_8
    if-eqz p4, :cond_18

    invoke-virtual/range {p3 .. p3}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getSelected()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-virtual/range {p3 .. p3}, Lcom/google/api/services/calendar/model/CalendarListEntry;->getHidden()Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v4

    if-nez v4, :cond_18

    if-eqz v31, :cond_18

    const/16 v29, 0x1

    .line 1753
    .local v29, "syncAndDisplay":Z
    :goto_9
    const-string v5, "sync_events"

    if-eqz v29, :cond_19

    const/4 v4, 0x1

    :goto_a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1754
    const-string v5, "visible"

    if-eqz v29, :cond_1a

    const/4 v4, 0x1

    :goto_b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1755
    const-string v4, "account_name"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1756
    const-string v4, "account_type"

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    const-string v4, "canPartiallyUpdate"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1758
    const-string v4, "allowedReminders"

    const-string v5, "0,1,2"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1759
    const-string v4, "allowedAttendeeTypes"

    const-string v5, "0,1,2"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    const-string v4, "allowedAvailability"

    const-string v5, "0,1"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1761
    const-string v4, "maxReminders"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1762
    const-string v4, "CalendarSyncAdapter"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1763
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding new calendar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1765
    :cond_16
    move-object/from16 v0, p6

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 1746
    .end local v29    # "syncAndDisplay":Z
    .end local v31    # "syncOn":Z
    :cond_17
    const/16 v31, 0x0

    goto/16 :goto_8

    .line 1749
    .restart local v31    # "syncOn":Z
    :cond_18
    const/16 v29, 0x0

    goto/16 :goto_9

    .line 1753
    .restart local v29    # "syncAndDisplay":Z
    :cond_19
    const/4 v4, 0x0

    goto/16 :goto_a

    .line 1754
    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_b
.end method

.method private processAccountCalendars(Landroid/accounts/Account;Landroid/content/ContentProviderClient;ZLjava/util/HashSet;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/os/Bundle;)V
    .locals 17
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "initialSync"    # Z
    .param p5, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p6, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentProviderClient;",
            "Z",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/syncadapters/calendar/CalendarSyncState;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1507
    .local p4, "calendarsNotSeenYet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1508
    .local v9, "inserts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar;->calendarList()Lcom/google/api/services/calendar/Calendar$CalendarList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar$CalendarList;->list()Lcom/google/api/services/calendar/Calendar$CalendarList$List;

    move-result-object v3

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$CalendarList$List;

    move-result-object v16

    .line 1510
    .local v16, "request":Lcom/google/api/services/calendar/Calendar$CalendarList$List;
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->execute()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/calendar/model/CalendarList;

    .line 1512
    .local v12, "calendarList":Lcom/google/api/services/calendar/model/CalendarList;
    :goto_0
    invoke-virtual {v12}, Lcom/google/api/services/calendar/model/CalendarList;->getItems()Ljava/util/List;

    move-result-object v13

    .line 1513
    .local v13, "calendars":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/CalendarListEntry;>;"
    if-eqz v13, :cond_0

    .line 1514
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/calendar/model/CalendarListEntry;

    .local v6, "calendarEntry":Lcom/google/api/services/calendar/model/CalendarListEntry;
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    .line 1515
    invoke-direct/range {v3 .. v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->processAccountCalendar(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/api/services/calendar/model/CalendarListEntry;ZLjava/util/HashSet;Ljava/util/ArrayList;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/os/Bundle;)V

    goto :goto_1

    .line 1526
    .end local v6    # "calendarEntry":Lcom/google/api/services/calendar/model/CalendarListEntry;
    .end local v14    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {v12}, Lcom/google/api/services/calendar/model/CalendarList;->getNextPageToken()Ljava/lang/String;

    move-result-object v15

    .line 1527
    .local v15, "nextPageToken":Ljava/lang/String;
    const-string v3, "CalendarSyncAdapter"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1528
    const-string v3, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendarList.nextPageToken: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1530
    :cond_1
    if-nez v15, :cond_3

    .line 1537
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1538
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->insertBatch(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 1540
    :cond_2
    return-void

    .line 1533
    :cond_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$CalendarList$List;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;->execute()Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "calendarList":Lcom/google/api/services/calendar/model/CalendarList;
    check-cast v12, Lcom/google/api/services/calendar/model/CalendarList;

    .line 1534
    .restart local v12    # "calendarList":Lcom/google/api/services/calendar/model/CalendarList;
    goto :goto_0
.end method

.method private processLocalChangesForHandler(JZLandroid/content/ContentProviderClient;Lcom/google/android/apiary/ItemAndEntityHandler;Landroid/content/SyncResult;)V
    .locals 9
    .param p1, "maxLoopAttempts"    # J
    .param p3, "overrideTooManyDeletions"    # Z
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p6, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(JZ",
            "Landroid/content/ContentProviderClient;",
            "Lcom/google/android/apiary/ItemAndEntityHandler",
            "<TT;>;",
            "Landroid/content/SyncResult;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/ParseException;,
            Lcom/google/android/apiary/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 2236
    .local p5, "handler":Lcom/google/android/apiary/ItemAndEntityHandler;, "Lcom/google/android/apiary/ItemAndEntityHandler<TT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    int-to-long v6, v0

    cmp-long v1, v6, p1

    if-gez v1, :cond_0

    .line 2237
    invoke-direct {p0, p6}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getNumChanges(Landroid/content/SyncResult;)J

    move-result-wide v4

    .line 2239
    .local v4, "numChangesBefore":J
    invoke-virtual {p0, p4, p6, p5, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->processLocalChanges(Landroid/content/ContentProviderClient;Landroid/content/SyncResult;Lcom/google/android/apiary/ItemAndEntityHandler;Z)V

    .line 2241
    invoke-direct {p0, p6}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getNumChanges(Landroid/content/SyncResult;)J

    move-result-wide v2

    .line 2242
    .local v2, "numChangesAfter":J
    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2246
    .end local v2    # "numChangesAfter":J
    .end local v4    # "numChangesBefore":J
    :cond_0
    return-void

    .line 2236
    .restart local v2    # "numChangesAfter":J
    .restart local v4    # "numChangesBefore":J
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private repairWrongDefaults(Landroid/content/ContentProviderClient;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/EventHandler;I)V
    .locals 12
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "calendarId"    # Ljava/lang/String;
    .param p3, "eventHandler"    # Lcom/google/android/syncadapters/calendar/EventHandler;
    .param p4, "maxAttendees"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2663
    const/4 v0, 0x4

    :try_start_0
    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "cal_sync1=?"

    aput-object v6, v0, v4

    const/4 v4, 0x1

    const-string v6, "dirty=0"

    aput-object v6, v0, v4

    const/4 v4, 0x2

    const-string v6, "lastSynced=0"

    aput-object v6, v0, v4

    const/4 v4, 0x3

    const-string v6, "(guestsCanInviteOthers=0 OR guestsCanSeeGuests=0)"

    aput-object v6, v0, v4

    invoke-static {v0}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    const/4 v6, -0x1

    invoke-virtual {p3, v0, v4, v6}, Lcom/google/android/syncadapters/calendar/EventHandler;->newEntityIterator(Ljava/lang/String;[Ljava/lang/String;I)Landroid/content/EntityIterator;
    :try_end_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 2672
    .local v9, "iterator":Landroid/content/EntityIterator;
    :try_start_1
    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    .line 2673
    .local v5, "syncResult":Landroid/content/SyncResult;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 2674
    .local v1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2675
    invoke-interface {v9}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Entity;

    .line 2676
    .local v3, "entity":Landroid/content/Entity;
    invoke-virtual {v3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v11

    .line 2677
    .local v11, "values":Landroid/content/ContentValues;
    const-string v0, "_sync_id"

    invoke-virtual {v11, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 2679
    .local v8, "eventId":Ljava/lang/String;
    :try_start_2
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v0}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v0

    invoke-virtual {v0, p2, v8}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/calendar/model/Event;

    .line 2682
    .local v2, "event":Lcom/google/api/services/calendar/model/Event;
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2691
    :try_start_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 2692
    .local v10, "size":I
    const/16 v0, 0x14

    if-le v10, v0, :cond_0

    .line 2693
    const-string v0, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Repairing "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " events"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2694
    invoke-direct {p0, p1, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2703
    .end local v1    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v2    # "event":Lcom/google/api/services/calendar/model/Event;
    .end local v3    # "entity":Landroid/content/Entity;
    .end local v5    # "syncResult":Landroid/content/SyncResult;
    .end local v8    # "eventId":Ljava/lang/String;
    .end local v10    # "size":I
    .end local v11    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v9}, Landroid/content/EntityIterator;->close()V

    throw v0
    :try_end_4
    .catch Lcom/google/android/apiary/ParseException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2705
    .end local v9    # "iterator":Landroid/content/EntityIterator;
    :catch_0
    move-exception v7

    .line 2706
    .local v7, "e":Lcom/google/android/apiary/ParseException;
    const-string v0, "CalendarSyncAdapter"

    const-string v4, "Failed to repair events on upgrade."

    invoke-static {v0, v4, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2708
    .end local v7    # "e":Lcom/google/android/apiary/ParseException;
    :goto_1
    return-void

    .line 2684
    .restart local v1    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v3    # "entity":Landroid/content/Entity;
    .restart local v5    # "syncResult":Landroid/content/SyncResult;
    .restart local v8    # "eventId":Ljava/lang/String;
    .restart local v9    # "iterator":Landroid/content/EntityIterator;
    .restart local v11    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v7

    .line 2688
    .local v7, "e":Lcom/google/api/client/http/HttpResponseException;
    :try_start_5
    const-string v0, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to resync event "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 2697
    .end local v3    # "entity":Landroid/content/Entity;
    .end local v7    # "e":Lcom/google/api/client/http/HttpResponseException;
    .end local v8    # "eventId":Ljava/lang/String;
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 2698
    .restart local v10    # "size":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 2699
    const-string v0, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Repairing "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " events"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2700
    invoke-direct {p0, p1, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2703
    :cond_2
    :try_start_6
    invoke-interface {v9}, Landroid/content/EntityIterator;->close()V
    :try_end_6
    .catch Lcom/google/android/apiary/ParseException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1
.end method

.method private resetSyncStateForFeed(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Ljava/lang/String;)V
    .locals 1
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p3, "calendarId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2159
    invoke-virtual {p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v0

    .line 2160
    .local v0, "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    if-eqz v0, :cond_0

    .line 2161
    invoke-virtual {v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->clear()V

    .line 2163
    :cond_0
    invoke-virtual {p2, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 2164
    return-void
.end method

.method static selectServerSyncMode(ZJLcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;)I
    .locals 15
    .param p0, "moveWindowSync"    # Z
    .param p1, "desiredWindowEnd"    # J
    .param p3, "feedSyncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    .prologue
    const/4 v8, 0x0

    const-wide/16 v12, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x3

    .line 1911
    const-string v11, "do_incremental_sync"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 1913
    .local v7, "incremental":Z
    const-string v11, "window_end"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12, v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1915
    .local v4, "feedDataWindowEnd":J
    const-string v11, "feed_updated_time"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "lastFetchedId"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    :cond_0
    move v6, v9

    .line 1918
    .local v6, "hasIncrementalData":Z
    :goto_0
    if-nez v6, :cond_3

    .line 1919
    const-string v9, "CalendarSyncAdapter"

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1920
    const-string v9, "CalendarSyncAdapter"

    const-string v10, "Sync mode: No last updated time present for feed, preparing for full sync"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    :cond_1
    :goto_1
    return v8

    .end local v6    # "hasIncrementalData":Z
    :cond_2
    move v6, v8

    .line 1915
    goto :goto_0

    .line 1924
    .restart local v6    # "hasIncrementalData":Z
    :cond_3
    if-eqz v7, :cond_d

    .line 1926
    cmp-long v8, p1, v12

    if-gtz v8, :cond_5

    .line 1927
    const-string v8, "CalendarSyncAdapter"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1928
    const-string v8, "CalendarSyncAdapter"

    const-string v10, "Sync mode: No sliding window defined"

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v8, v9

    .line 1930
    goto :goto_1

    .line 1931
    :cond_5
    if-eqz p0, :cond_9

    .line 1932
    const-string v8, "new_window_end"

    move-object/from16 v0, p3

    invoke-virtual {v0, v8, v12, v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1934
    .local v2, "feedDataMoveWindowEnd":J
    cmp-long v8, v2, v12

    if-lez v8, :cond_7

    .line 1935
    const-string v8, "CalendarSyncAdapter"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1936
    const-string v8, "CalendarSyncAdapter"

    const-string v10, "Sync mode: Already moving the sync window.  Ignoring request."

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v8, v9

    .line 1938
    goto :goto_1

    .line 1942
    :cond_7
    const-string v8, "CalendarSyncAdapter"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1943
    const-string v8, "CalendarSyncAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Sync mode: Moving the sliding window to "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-wide/from16 v0, p1

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move v8, v10

    .line 1945
    goto :goto_1

    .line 1947
    .end local v2    # "feedDataMoveWindowEnd":J
    :cond_9
    cmp-long v8, p1, v4

    if-lez v8, :cond_b

    .line 1948
    const-string v8, "CalendarSyncAdapter"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1949
    const-string v8, "CalendarSyncAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync mode: Scheduling sliding window move from "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-wide/from16 v0, p1

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    :cond_a
    const/4 v8, 0x4

    goto/16 :goto_1

    .line 1954
    :cond_b
    const-string v8, "CalendarSyncAdapter"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1955
    const-string v8, "CalendarSyncAdapter"

    const-string v10, "Sync mode: default incremental sync"

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    move v8, v9

    .line 1957
    goto/16 :goto_1

    .line 1960
    :cond_d
    const-string v8, "CalendarSyncAdapter"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1961
    const-string v8, "CalendarSyncAdapter"

    const-string v10, "Sync mode: continuing full sync"

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    move v8, v9

    .line 1963
    goto/16 :goto_1
.end method

.method private sendEntityToServer(Landroid/content/ContentProviderClient;Landroid/content/Entity;Lcom/google/android/apiary/ItemAndEntityHandler;Landroid/content/SyncResult;)V
    .locals 8
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "entity"    # Landroid/content/Entity;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/content/Entity;",
            "Lcom/google/android/apiary/ItemAndEntityHandler",
            "<TT;>;",
            "Landroid/content/SyncResult;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/ParseException;,
            Ljava/io/IOException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2362
    .local p3, "handler":Lcom/google/android/apiary/ItemAndEntityHandler;, "Lcom/google/android/apiary/ItemAndEntityHandler<TT;>;"
    invoke-virtual {p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v4

    .line 2363
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "original_id"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "original_id"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2365
    const-string v5, "original_sync_id"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2366
    .local v2, "originalSyncId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2387
    .end local v2    # "originalSyncId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 2374
    :cond_1
    invoke-interface {p3, p2, p4}, Lcom/google/android/apiary/ItemAndEntityHandler;->sendEntityToServer(Landroid/content/Entity;Landroid/content/SyncResult;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2376
    .local v1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v1, :cond_0

    .line 2377
    const/4 v5, 0x0

    new-array v3, v5, [Landroid/content/ContentProviderResult;

    .line 2379
    .local v3, "result":[Landroid/content/ContentProviderResult;
    :try_start_0
    invoke-static {p1, v1}, Lcom/google/android/apiary/ProviderHelper;->applyBatchProvider(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2383
    :goto_1
    const-string v5, "CalendarSyncAdapter"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2384
    const-string v5, "CalendarSyncAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "results are: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-static {v7, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2380
    :catch_0
    move-exception v0

    .line 2381
    .local v0, "e":Landroid/content/OperationApplicationException;
    const-string v5, "CalendarSyncAdapter"

    const-string v6, "error applying batch"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private skipEntry(Ljava/util/List;ILjava/lang/Throwable;)V
    .locals 2
    .param p2, "previousSize"    # I
    .param p3, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;I",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2147
    .local p1, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const-string v0, "CalendarSyncAdapter"

    const-string v1, "Entry failed, skipping "

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2148
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 2150
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 2152
    :cond_0
    return-void
.end method

.method private updateCalendarsFromServerFeed(Landroid/accounts/Account;Landroid/content/ContentProviderClient;ZLcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/os/Bundle;Landroid/content/SyncResult;)V
    .locals 20
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "initialSync"    # Z
    .param p4, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p5, "extras"    # Landroid/os/Bundle;
    .param p6, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/ParseException;,
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1018
    invoke-direct/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getCurrentCalendars(Landroid/accounts/Account;)Ljava/util/HashSet;

    move-result-object v6

    .line 1020
    .local v6, "calendarsNotSeenYet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v9

    .line 1021
    .local v9, "activeTag":I
    const/high16 v2, 0x1000000

    or-int v16, v9, v2

    .line 1022
    .local v16, "tag":I
    invoke-static/range {v16 .. v16}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 1024
    :try_start_0
    invoke-direct/range {v2 .. v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->processAccountCalendars(Landroid/accounts/Account;Landroid/content/ContentProviderClient;ZLjava/util/HashSet;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1040
    const/4 v2, 0x1

    move/from16 v0, v16

    invoke-static {v0, v2}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1042
    invoke-static {v9}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1046
    sget-object v12, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 1047
    .local v12, "calendarContentUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 1048
    .local v13, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1050
    .local v10, "calId":J
    invoke-static {v12, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v13, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1026
    .end local v10    # "calId":J
    .end local v12    # "calendarContentUri":Landroid/net/Uri;
    .end local v13    # "cr":Landroid/content/ContentResolver;
    .end local v15    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v14

    .line 1029
    .local v14, "e":Lcom/google/android/apiary/AuthenticationException;
    :try_start_1
    throw v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1040
    .end local v14    # "e":Lcom/google/android/apiary/AuthenticationException;
    :catchall_0
    move-exception v2

    const/4 v3, 0x1

    move/from16 v0, v16

    invoke-static {v0, v3}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1042
    invoke-static {v9}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v2

    .line 1030
    :catch_1
    move-exception v14

    .line 1031
    .local v14, "e":Ljava/io/IOException;
    :try_start_2
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Unable to get calendar account "

    invoke-static {v2, v3, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1032
    if-eqz p3, :cond_0

    .line 1035
    throw v14

    .line 1037
    :cond_0
    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v18, 0x1

    add-long v4, v4, v18

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1040
    const/4 v2, 0x1

    move/from16 v0, v16

    invoke-static {v0, v2}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 1042
    invoke-static {v9}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 1053
    .end local v14    # "e":Ljava/io/IOException;
    :cond_1
    return-void
.end method

.method private updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 7
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 2529
    iget-object v6, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mAccountsWithColors:Ljava/util/Set;

    monitor-enter v6

    .line 2530
    const/4 v3, 0x0

    :try_start_0
    sget-object v4, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColors:Ljava/util/Map;

    sget-object v5, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColorsGsf:Ljava/util/Map;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;ILjava/util/Map;Ljava/util/Map;)V

    .line 2532
    sget-boolean v0, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v0, :cond_0

    .line 2533
    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColors:Ljava/util/Map;

    sget-object v5, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColorsGsf:Ljava/util/Map;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;ILjava/util/Map;Ljava/util/Map;)V

    .line 2536
    :cond_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mAccountsWithColors:Ljava/util/Set;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2537
    monitor-exit v6

    .line 2538
    return-void

    .line 2537
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;ILjava/util/Map;Ljava/util/Map;)V
    .locals 28
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "colorType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/accounts/Account;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 526
    .local p4, "colorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p5, "colorMapGsf":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v27

    .line 527
    .local v27, "typeStr":Ljava/lang/String;
    sget-object v3, Landroid/provider/CalendarContract$Colors;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "color_index"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string v6, "color"

    aput-object v6, v4, v2

    sget-object v5, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v9, v6, v2

    const/4 v2, 0x1

    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v9, v6, v2

    const/4 v2, 0x2

    aput-object v27, v6, v2

    const-string v7, "color_index"

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 533
    .local v24, "cursor":Landroid/database/Cursor;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 535
    .local v4, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v2, Landroid/provider/CalendarContract$Colors;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    .line 536
    .local v5, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v22

    .line 537
    .local v22, "existingColorKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v24, :cond_4

    .line 539
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 540
    const/4 v2, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 541
    .local v15, "key":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 542
    const/4 v2, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 543
    .local v23, "color":I
    move-object/from16 v0, p5

    invoke-interface {v0, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 544
    .local v8, "newColor":Ljava/lang/Integer;
    if-nez v8, :cond_1

    .line 545
    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "newColor":Ljava/lang/Integer;
    check-cast v8, Ljava/lang/Integer;

    .line 547
    .restart local v8    # "newColor":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move/from16 v0, v23

    if-eq v0, v2, :cond_0

    .line 548
    const-string v2, "CalendarSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 549
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updating color "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "for type "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " to "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "temp-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .local v7, "tempKey":Ljava/lang/String;
    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move/from16 v6, p3

    .line 565
    invoke-direct/range {v2 .. v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->insertColor(Landroid/accounts/Account;Ljava/util/ArrayList;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object v12, v4

    move/from16 v13, p3

    move-object v14, v7

    .line 568
    invoke-direct/range {v9 .. v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateObjectsColor(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;)V

    .line 571
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR:Ljava/lang/String;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p2

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v10, v6, v9

    const/4 v9, 0x1

    move-object/from16 v0, p2

    iget-object v10, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v10, v6, v9

    const/4 v9, 0x2

    aput-object v27, v6, v9

    const/4 v9, 0x3

    aput-object v15, v6, v9

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v10, p0

    move-object/from16 v11, p2

    move-object v12, v4

    move-object v13, v5

    move/from16 v14, p3

    move-object/from16 v16, v8

    .line 578
    invoke-direct/range {v10 .. v16}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->insertColor(Landroid/accounts/Account;Ljava/util/ArrayList;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object v13, v4

    move/from16 v14, p3

    move-object/from16 v16, v7

    .line 581
    invoke-direct/range {v10 .. v16}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateObjectsColor(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;)V

    .line 584
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR:Ljava/lang/String;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p2

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v10, v6, v9

    const/4 v9, 0x1

    move-object/from16 v0, p2

    iget-object v10, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v10, v6, v9

    const/4 v9, 0x2

    aput-object v27, v6, v9

    const/4 v9, 0x3

    aput-object v7, v6, v9

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 592
    .end local v7    # "tempKey":Ljava/lang/String;
    .end local v8    # "newColor":Ljava/lang/Integer;
    .end local v15    # "key":Ljava/lang/String;
    .end local v23    # "color":I
    :catchall_0
    move-exception v2

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_3
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 595
    :cond_4
    new-instance v20, Ljava/util/HashMap;

    move-object/from16 v0, v20

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 596
    .local v20, "combinedColorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Map$Entry;

    .line 597
    .local v25, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 598
    .restart local v15    # "key":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 599
    invoke-interface/range {v25 .. v25}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v15, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .end local v15    # "key":Ljava/lang/String;
    .end local v25    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_6
    move-object/from16 v16, p0

    move-object/from16 v17, v4

    move-object/from16 v18, p2

    move/from16 v19, p3

    move-object/from16 v21, v5

    .line 602
    invoke-direct/range {v16 .. v22}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addMissingColors(Ljava/util/ArrayList;Landroid/accounts/Account;ILjava/util/Map;Landroid/net/Uri;Ljava/util/Set;)V

    .line 603
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 604
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 606
    :cond_7
    return-void
.end method

.method private updateObjectsColor(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p4, "colorType"    # I
    .param p5, "colorKey"    # Ljava/lang/String;
    .param p6, "oldColorKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/accounts/Account;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 630
    .local p3, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-nez p4, :cond_1

    .line 631
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_CALENDARS_ACCOUNT_AND_COLOR:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p6, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "calendar_color_index"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v5, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v0

    const/4 v0, 0x1

    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 646
    .local v7, "calendarsCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 647
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 649
    .local v6, "calendarIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 653
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 655
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 656
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calendar_id IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-static {v1, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 658
    .local v8, "whereId":Ljava/lang/String;
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addCallerIsSyncAdapterParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    const/4 v2, 0x1

    const-string v3, "eventColor_index=?"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p6, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "eventColor_index"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private updateProviderForInitialSync(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;)V
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "syncInfo"    # Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1975
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1976
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "CalendarSyncAdapter"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1977
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Performing initial sync on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p3, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1979
    :cond_0
    const-string v2, "sync_data4"

    const-string v3, "local android etag magic value"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1981
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_sync_id IS NOT NULL"

    aput-object v4, v3, v8

    const-string v4, "calendar_id=?"

    aput-object v4, v3, v6

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    iget-wide v6, p3, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {p2, v2, v1, v3, v4}, Lcom/google/android/apiary/ProviderHelper;->updateProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1987
    .local v0, "count":I
    const-string v2, "CalendarSyncAdapter"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1988
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Applied ETAG_MAGIC_VALUE to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rows for Calendar: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p3, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1991
    :cond_1
    return-void
.end method

.method private updateSyncStateAfterFeedRead(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;)V
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p4, "updatedMin"    # Ljava/lang/String;
    .param p5, "calendarId"    # Ljava/lang/String;
    .param p6, "syncInfo"    # Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 2174
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 2176
    .local v6, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v3

    .line 2177
    .local v3, "feedSyncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    const-string v7, "lastFetchedId"

    invoke-virtual {v3, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->remove(Ljava/lang/String;)V

    .line 2178
    const-string v7, "upgrade_min_start"

    invoke-virtual {v3, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->remove(Ljava/lang/String;)V

    .line 2179
    const-string v7, "upgrade_max_start"

    invoke-virtual {v3, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->remove(Ljava/lang/String;)V

    .line 2180
    const-string v7, "in_progress_params"

    invoke-virtual {v3, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->remove(Ljava/lang/String;)V

    .line 2182
    const-string v7, "new_window_end"

    const-wide/16 v8, 0x0

    invoke-virtual {v3, v7, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2184
    .local v4, "feedSyncStateMoveWindowEnd":J
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_4

    .line 2186
    const-string v7, "CalendarSyncAdapter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2187
    const-string v7, "CalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sliding sync window advanced to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2189
    :cond_0
    const-string v7, "new_window_end"

    const-wide/16 v8, 0x0

    invoke-virtual {v3, v7, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V

    .line 2190
    const-string v7, "window_end"

    invoke-virtual {v3, v7, v4, v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V

    .line 2198
    :goto_0
    const-string v7, "do_incremental_sync"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2200
    const-string v7, "CalendarSyncAdapter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2201
    const-string v7, "CalendarSyncAdapter"

    const-string v8, "switching from full to incremental"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2206
    :cond_1
    sget-object v7, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "sync_data4=?"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "calendar_id=?"

    aput-object v10, v8, v9

    invoke-static {v8}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "local android etag magic value"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iget-wide v12, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2214
    const-string v7, "do_incremental_sync"

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putBoolean(Ljava/lang/String;Z)V

    .line 2217
    :cond_2
    const-string v7, "CalendarSyncAdapter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2218
    const-string v7, "CalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Writing back feedSyncState: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->newUpdateOperation()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2223
    :try_start_0
    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/google/android/apiary/ProviderHelper;->applyBatchProvider(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2227
    return-void

    .line 2196
    :cond_4
    const-string v7, "feed_updated_time"

    move-object/from16 v0, p4

    invoke-virtual {v3, v7, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2224
    :catch_0
    move-exception v2

    .line 2225
    .local v2, "e":Landroid/content/OperationApplicationException;
    new-instance v7, Lcom/google/android/apiary/ParseException;

    const-string v8, "unable to update sync state after successful feed read"

    invoke-direct {v7, v8, v2}, Lcom/google/android/apiary/ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
.end method


# virtual methods
.method public getColorMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 12
    .param p1, "colorsString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2435
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 2436
    .local v1, "colorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v9, "\\|"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2437
    .local v3, "colorStrings":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v2, v0, v5

    .line 2438
    .local v2, "colorString":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 2439
    .local v8, "strings":[Ljava/lang/String;
    array-length v9, v8

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 2441
    const/4 v9, 0x0

    :try_start_0
    aget-object v6, v8, v9

    .line 2442
    .local v6, "index":Ljava/lang/String;
    const/4 v9, 0x1

    aget-object v9, v8, v9

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v1, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2437
    .end local v6    # "index":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2443
    :catch_0
    move-exception v4

    .line 2444
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    const-string v9, "CalendarSyncAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error parsing color value: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2445
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v4

    .line 2446
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v9, "CalendarSyncAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error parsing color value: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2450
    .end local v2    # "colorString":Ljava/lang/String;
    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v8    # "strings":[Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public getServerDiffs(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 40
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 869
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v22

    .line 870
    .local v22, "context":Landroid/content/Context;
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    .line 871
    .local v23, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    const-string v4, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v36, 0x1

    .line 872
    .local v36, "syncingSingleFeed":Z
    :goto_0
    if-eqz p2, :cond_2

    const-string v4, "metafeedonly"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v35, 0x1

    .line 875
    .local v35, "syncingMetafeedOnly":Z
    :goto_1
    if-eqz v36, :cond_5

    .line 876
    if-eqz v35, :cond_3

    .line 877
    const-string v4, "CalendarSyncAdapter"

    const-string v5, "\'metafeedonly\' and \'feed\' extras both set. They are not compatible."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    :cond_0
    :goto_2
    return-void

    .line 871
    .end local v35    # "syncingMetafeedOnly":Z
    .end local v36    # "syncingSingleFeed":Z
    :cond_1
    const/16 v36, 0x0

    goto :goto_0

    .line 872
    .restart local v36    # "syncingSingleFeed":Z
    :cond_2
    const/16 v35, 0x0

    goto :goto_1

    .line 881
    .restart local v35    # "syncingMetafeedOnly":Z
    :cond_3
    const-string v4, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 883
    .local v9, "feedId":Ljava/lang/String;
    const-string v4, "http"

    invoke-virtual {v9, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 884
    const-string v4, "/"

    invoke-virtual {v9, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v32

    .line 885
    .local v32, "pathComponents":[Ljava/lang/String;
    move-object/from16 v0, v32

    array-length v4, v0

    const/4 v5, 0x5

    if-le v4, v5, :cond_4

    const-string v4, "feeds"

    const/4 v5, 0x4

    aget-object v5, v32, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 886
    const/4 v4, 0x5

    aget-object v4, v32, v4

    invoke-static {v4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 887
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Changed feedId -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    .end local v32    # "pathComponents":[Ljava/lang/String;
    :cond_4
    const-string v4, "moveWindow"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .local v10, "moveWindowSync":Z
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p2

    move-object/from16 v8, p4

    .line 891
    invoke-direct/range {v4 .. v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getServerDiffsForFeed(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/os/Bundle;Landroid/content/SyncResult;Ljava/lang/String;Z)V

    goto :goto_2

    .line 900
    .end local v9    # "feedId":Ljava/lang/String;
    .end local v10    # "moveWindowSync":Z
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getOrCreate(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v15

    .line 902
    .local v15, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    invoke-virtual {v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->isFirstSeen()Z

    move-result v14

    .line 903
    .local v14, "firstSeen":Z
    if-eqz v14, :cond_6

    .line 904
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    :cond_6
    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p3

    move-object/from16 v16, p2

    move-object/from16 v17, p4

    .line 906
    invoke-direct/range {v11 .. v17}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateCalendarsFromServerFeed(Landroid/accounts/Account;Landroid/content/ContentProviderClient;ZLcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    .line 907
    if-eqz v14, :cond_9

    .line 908
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setFirstSeen(Z)V

    .line 909
    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 912
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saved-calendar-settings-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 915
    .local v27, "filename":Ljava/lang/String;
    :try_start_0
    new-instance v28, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v28

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 918
    .local v28, "in":Ljava/io/BufferedReader;
    :try_start_1
    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v37

    .line 920
    .local v37, "uri":Landroid/net/Uri;
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 923
    .local v30, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_7
    :goto_3
    invoke-virtual/range {v28 .. v28}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v29

    .line 924
    .local v29, "line":Ljava/lang/String;
    if-nez v29, :cond_a

    .line 955
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_8

    .line 956
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 959
    :cond_8
    :try_start_2
    invoke-virtual/range {v28 .. v28}, Ljava/io/BufferedReader;->close()V

    .line 960
    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 969
    .end local v27    # "filename":Ljava/lang/String;
    .end local v28    # "in":Ljava/io/BufferedReader;
    .end local v29    # "line":Ljava/lang/String;
    .end local v30    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v37    # "uri":Landroid/net/Uri;
    :cond_9
    :goto_4
    if-nez v35, :cond_0

    .line 975
    sget-object v17, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v4, 0x0

    const-string v5, "cal_sync1"

    aput-object v5, v18, v4

    sget-object v19, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v20, v4

    const/4 v4, 0x1

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v20, v4

    const/4 v4, 0x2

    const-string v5, "1"

    aput-object v5, v20, v4

    const/16 v21, 0x0

    move-object/from16 v16, v23

    invoke-virtual/range {v16 .. v21}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 981
    .local v24, "cursor":Landroid/database/Cursor;
    new-instance v33, Landroid/os/Bundle;

    invoke-direct/range {v33 .. v33}, Landroid/os/Bundle;-><init>()V

    .line 984
    .local v33, "syncExtras":Landroid/os/Bundle;
    :goto_5
    :try_start_3
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 985
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 988
    .restart local v9    # "feedId":Ljava/lang/String;
    invoke-virtual/range {v33 .. v33}, Landroid/os/Bundle;->clear()V

    .line 989
    move-object/from16 v0, v33

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 990
    const-string v4, "feed"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    const-string v4, "com.android.calendar"

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v4, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    .line 994
    .end local v9    # "feedId":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v4

    .line 928
    .end local v24    # "cursor":Landroid/database/Cursor;
    .end local v33    # "syncExtras":Landroid/os/Bundle;
    .restart local v27    # "filename":Ljava/lang/String;
    .restart local v28    # "in":Ljava/io/BufferedReader;
    .restart local v29    # "line":Ljava/lang/String;
    .restart local v30    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v37    # "uri":Landroid/net/Uri;
    :cond_a
    :try_start_4
    const-string v4, ":"

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v31

    .line 929
    .local v31, "p":I
    if-gtz v31, :cond_b

    .line 930
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid settings line: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_3

    .line 959
    .end local v29    # "line":Ljava/lang/String;
    .end local v30    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v31    # "p":I
    .end local v37    # "uri":Landroid/net/Uri;
    :catchall_1
    move-exception v4

    :try_start_5
    invoke-virtual/range {v28 .. v28}, Ljava/io/BufferedReader;->close()V

    .line 960
    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    throw v4
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 962
    .end local v28    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v4

    goto/16 :goto_4

    .line 933
    .restart local v28    # "in":Ljava/io/BufferedReader;
    .restart local v29    # "line":Ljava/lang/String;
    .restart local v30    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v31    # "p":I
    .restart local v37    # "uri":Landroid/net/Uri;
    :cond_b
    add-int/lit8 v4, v31, -0x1

    :try_start_6
    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v34, v4, -0x30

    .line 934
    .local v34, "synced":I
    const-string v4, ":"

    add-int/lit8 v5, v31, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v31

    .line 935
    if-gtz v31, :cond_c

    .line 936
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid settings line: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 939
    :cond_c
    add-int/lit8 v4, v31, -0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v39, v4, -0x30

    .line 940
    .local v39, "visible":I
    add-int/lit8 v4, v31, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 941
    .local v25, "displayName":Ljava/lang/String;
    new-instance v38, Landroid/content/ContentValues;

    invoke-direct/range {v38 .. v38}, Landroid/content/ContentValues;-><init>()V

    .line 942
    .local v38, "values":Landroid/content/ContentValues;
    const-string v5, "sync_events"

    if-nez v34, :cond_d

    const/4 v4, 0x0

    :goto_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 943
    const-string v5, "visible"

    if-nez v39, :cond_e

    const/4 v4, 0x0

    :goto_7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 944
    invoke-static/range {v37 .. v37}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "calendar_displayName=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v25, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 950
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0x64

    if-le v4, v5, :cond_7

    .line 951
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 952
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->clear()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_3

    .line 942
    :cond_d
    const/4 v4, 0x1

    goto :goto_6

    .line 943
    :cond_e
    const/4 v4, 0x1

    goto :goto_7

    .line 964
    .end local v25    # "displayName":Ljava/lang/String;
    .end local v28    # "in":Ljava/io/BufferedReader;
    .end local v29    # "line":Ljava/lang/String;
    .end local v30    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v31    # "p":I
    .end local v34    # "synced":I
    .end local v37    # "uri":Landroid/net/Uri;
    .end local v38    # "values":Landroid/content/ContentValues;
    .end local v39    # "visible":I
    :catch_1
    move-exception v26

    .line 965
    .local v26, "e":Ljava/io/IOException;
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException while reading file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 994
    .end local v26    # "e":Ljava/io/IOException;
    .end local v27    # "filename":Ljava/lang/String;
    .restart local v24    # "cursor":Landroid/database/Cursor;
    .restart local v33    # "syncExtras":Landroid/os/Bundle;
    :cond_f
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2
.end method

.method protected hasTooManyChanges(JJ)Z
    .locals 11
    .param p1, "numEntries"    # J
    .param p3, "numChanges"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 2413
    cmp-long v6, p1, v4

    if-nez v6, :cond_0

    .line 2416
    .local v4, "percentChanged":J
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "google_calendar_sync_num_allowed_simultaneous changes"

    const-wide/16 v8, 0x5

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    .line 2420
    .local v0, "numAllowedSimultaneousChanges":J
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "google_calendar_sync_percent_allowed_simultaneous_changes"

    const-wide/16 v8, 0x14

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    .line 2424
    .local v2, "percentAllowedSimultaneousChanges":J
    cmp-long v6, p3, v0

    if-lez v6, :cond_1

    cmp-long v6, v4, v2

    if-lez v6, :cond_1

    const/4 v6, 0x1

    :goto_1
    return v6

    .line 2413
    .end local v0    # "numAllowedSimultaneousChanges":J
    .end local v2    # "percentAllowedSimultaneousChanges":J
    .end local v4    # "percentChanged":J
    :cond_0
    const-wide/16 v6, 0x64

    mul-long/2addr v6, p3

    div-long v4, v6, p1

    goto :goto_0

    .line 2424
    .restart local v0    # "numAllowedSimultaneousChanges":J
    .restart local v2    # "percentAllowedSimultaneousChanges":J
    .restart local v4    # "percentChanged":J
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public declared-synchronized onAccountsUpdated()V
    .locals 19

    .prologue
    .line 2457
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v10

    .line 2458
    .local v10, "accountsWithCalendar":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/accounts/Account;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v13

    .line 2459
    .local v13, "context":Landroid/content/Context;
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.calendar"

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v2

    .line 2461
    .local v2, "provider":Landroid/content/ContentProviderClient;
    if-nez v2, :cond_0

    .line 2462
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Provider not found while updating accounts"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2525
    :goto_0
    monitor-exit p0

    return-void

    .line 2466
    :cond_0
    :try_start_1
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "account_name"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "account_type"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 2473
    .local v12, "c":Landroid/database/Cursor;
    if-nez v12, :cond_1

    .line 2475
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Received an onAccountsChanged() but has not found any sync"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2523
    :try_start_2
    invoke-virtual {v2}, Landroid/content/ContentProviderClient;->release()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2457
    .end local v2    # "provider":Landroid/content/ContentProviderClient;
    .end local v10    # "accountsWithCalendar":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/accounts/Account;>;"
    .end local v12    # "c":Landroid/database/Cursor;
    .end local v13    # "context":Landroid/content/Context;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 2479
    .restart local v2    # "provider":Landroid/content/ContentProviderClient;
    .restart local v10    # "accountsWithCalendar":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/accounts/Account;>;"
    .restart local v12    # "c":Landroid/database/Cursor;
    .restart local v13    # "context":Landroid/content/Context;
    :cond_1
    :goto_1
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2480
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 2481
    .local v17, "name":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 2482
    .local v18, "type":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2483
    :cond_2
    const-string v3, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Account name or type are empty: Cursor position="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v12}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' type=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 2491
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "type":Ljava/lang/String;
    :catchall_1
    move-exception v3

    :try_start_4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2518
    .end local v12    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v14

    .line 2519
    .local v14, "e":Landroid/os/RemoteException;
    :try_start_5
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Failed to update the provider"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2523
    :try_start_6
    invoke-virtual {v2}, Landroid/content/ContentProviderClient;->release()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 2487
    .end local v14    # "e":Landroid/os/RemoteException;
    .restart local v12    # "c":Landroid/database/Cursor;
    .restart local v17    # "name":Ljava/lang/String;
    .restart local v18    # "type":Ljava/lang/String;
    :cond_3
    :try_start_7
    new-instance v8, Landroid/accounts/Account;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v8, v0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2488
    .local v8, "account":Landroid/accounts/Account;
    invoke-virtual {v10, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    .line 2491
    .end local v8    # "account":Landroid/accounts/Account;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "type":Ljava/lang/String;
    :cond_4
    :try_start_8
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 2494
    :try_start_9
    invoke-static {v13}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.google"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "service_cl"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v3

    invoke-interface {v3}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/accounts/Account;

    .line 2502
    .local v9, "accounts":[Landroid/accounts/Account;
    move-object v11, v9

    .local v11, "arr$":[Landroid/accounts/Account;
    array-length v0, v11

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    aget-object v8, v11, v15

    .line 2503
    .restart local v8    # "account":Landroid/accounts/Account;
    invoke-virtual {v10, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2505
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDefaultCalendar(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 2507
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    :try_end_9
    .catch Landroid/accounts/OperationCanceledException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Lcom/google/android/apiary/ParseException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 2502
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 2509
    .end local v8    # "account":Landroid/accounts/Account;
    .end local v9    # "accounts":[Landroid/accounts/Account;
    .end local v11    # "arr$":[Landroid/accounts/Account;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    :catch_1
    move-exception v14

    .line 2510
    .local v14, "e":Landroid/accounts/OperationCanceledException;
    :try_start_a
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Unable to get calendar accounts"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 2523
    .end local v14    # "e":Landroid/accounts/OperationCanceledException;
    :cond_6
    :goto_3
    :try_start_b
    invoke-virtual {v2}, Landroid/content/ContentProviderClient;->release()Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 2511
    :catch_2
    move-exception v14

    .line 2512
    .local v14, "e":Ljava/io/IOException;
    :try_start_c
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Unable to get calendar accounts"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_3

    .line 2520
    .end local v12    # "c":Landroid/database/Cursor;
    .end local v14    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v14

    .line 2521
    .local v14, "e":Lcom/google/android/apiary/ParseException;
    :try_start_d
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Failed to update the provider"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 2523
    :try_start_e
    invoke-virtual {v2}, Landroid/content/ContentProviderClient;->release()Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 2513
    .end local v14    # "e":Lcom/google/android/apiary/ParseException;
    .restart local v12    # "c":Landroid/database/Cursor;
    :catch_4
    move-exception v14

    .line 2514
    .local v14, "e":Landroid/accounts/AuthenticatorException;
    :try_start_f
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Unable to get calendar accounts"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    goto :goto_3

    .line 2523
    .end local v12    # "c":Landroid/database/Cursor;
    .end local v14    # "e":Landroid/accounts/AuthenticatorException;
    :catchall_2
    move-exception v3

    :try_start_10
    invoke-virtual {v2}, Landroid/content/ContentProviderClient;->release()Z

    throw v3
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 2515
    .restart local v12    # "c":Landroid/database/Cursor;
    :catch_5
    move-exception v14

    .line 2516
    .local v14, "e":Lcom/google/android/apiary/ParseException;
    :try_start_11
    const-string v3, "CalendarSyncAdapter"

    const-string v4, "Unable to get calendar accounts"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Lcom/google/android/apiary/ParseException; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    goto :goto_3
.end method

.method public onPerformLoggedSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    const/4 v3, 0x2

    const-wide/16 v8, 0x1

    const/4 v6, 0x1

    .line 318
    const-class v2, Lcom/google/api/client/http/HttpTransport;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    const-class v2, Lcom/google/api/client/http/HttpTransport;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 321
    :cond_0
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/common/GoogleTrafficStats;->getDomainType(Ljava/lang/String;)I

    move-result v1

    .line 322
    .local v1, "tag":I
    invoke-static {v1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 323
    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mGoogleRequestInitializer:Lcom/google/android/apiary/GoogleRequestInitializer;

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apiary/GoogleRequestInitializer;->setEmail(Ljava/lang/String;)V

    .line 326
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mUpdatedColorsFromGsf:Z

    if-nez v2, :cond_1

    .line 327
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorMapFromGsf()V

    .line 328
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mUpdatedColorsFromGsf:Z

    .line 330
    :cond_1
    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mAccountsWithColors:Ljava/util/Set;

    monitor-enter v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/apiary/ParseException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 331
    :try_start_1
    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mAccountsWithColors:Ljava/util/Set;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 332
    invoke-direct {p0, p4, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 334
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    :try_start_2
    invoke-direct/range {p0 .. p5}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->performSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V

    .line 336
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 337
    const-string v2, "CalendarSyncAdapter"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 338
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stopping Sync for Account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "as it is cancelled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/apiary/ParseException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 362
    :cond_3
    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 365
    :goto_0
    return-void

    .line 334
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/android/apiary/ParseException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_5
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Exception in onPerformLoggedSync "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 344
    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v8

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 362
    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 345
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 346
    .local v0, "e":Lcom/google/api/client/http/HttpResponseException;
    :try_start_6
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Exception in onPerformLoggedSync "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 347
    invoke-direct {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isAuthenticationException(Lcom/google/api/client/http/HttpResponseException;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 348
    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v4, v8

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 362
    :goto_1
    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 350
    :cond_4
    :try_start_7
    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v8

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    .line 362
    .end local v0    # "e":Lcom/google/api/client/http/HttpResponseException;
    :catchall_1
    move-exception v2

    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v2

    .line 352
    :catch_2
    move-exception v0

    .line 353
    .local v0, "e":Lcom/google/android/apiary/AuthenticationException;
    :try_start_8
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Exception in onPerformLoggedSync "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 354
    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v4, v8

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 362
    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 355
    .end local v0    # "e":Lcom/google/android/apiary/AuthenticationException;
    :catch_3
    move-exception v0

    .line 356
    .local v0, "e":Ljava/io/IOException;
    :try_start_9
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Exception in onPerformLoggedSync "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 357
    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v8

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 362
    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 358
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 359
    .local v0, "e":Lcom/google/android/apiary/ParseException;
    :try_start_a
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Exception in onPerformLoggedSync "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 360
    iget-object v2, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v4, v8

    iput-wide v4, v2, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 362
    invoke-static {v1, v6}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 363
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto/16 :goto_0
.end method

.method processLocalChanges(Landroid/content/ContentProviderClient;Landroid/content/SyncResult;Lcom/google/android/apiary/ItemAndEntityHandler;Z)V
    .locals 24
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "syncResult"    # Landroid/content/SyncResult;
    .param p4, "overrideTooManyDeletions"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/content/SyncResult;",
            "Lcom/google/android/apiary/ItemAndEntityHandler",
            "<TT;>;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apiary/ParseException;,
            Lcom/google/android/apiary/AuthenticationException;
        }
    .end annotation

    .prologue
    .line 2254
    .local p3, "handler":Lcom/google/android/apiary/ItemAndEntityHandler;, "Lcom/google/android/apiary/ItemAndEntityHandler<TT;>;"
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 2339
    :goto_0
    return-void

    .line 2258
    :cond_0
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apiary/ItemAndEntityHandler;->getEntitySelection()Ljava/lang/String;

    move-result-object v10

    .line 2259
    .local v10, "handlerSelection":Ljava/lang/String;
    if-nez v10, :cond_1

    .line 2260
    const-string v18, "CalendarSyncAdapter"

    const-string v19, "EntityIterator cannot have a null selection parameter"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2268
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    const-string v19, "google_calendar_max_local_changes_in_batch"

    const/16 v20, 0x14

    invoke-static/range {v18 .. v20}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    .line 2273
    .local v13, "maxLocalChangesInBatch":I
    :try_start_0
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") AND "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ">?"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 2274
    .local v14, "selection":Ljava/lang/String;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/String;

    const/16 v18, 0x0

    const-string v19, "-1"

    aput-object v19, v15, v18

    .line 2276
    .local v15, "selectionArgs":[Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p3

    invoke-interface {v0, v14, v15, v13}, Lcom/google/android/apiary/ItemAndEntityHandler;->newEntityIterator(Ljava/lang/String;[Ljava/lang/String;I)Landroid/content/EntityIterator;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 2279
    .local v12, "iterator":Landroid/content/EntityIterator;
    :try_start_1
    invoke-interface {v12}, Landroid/content/EntityIterator;->hasNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v18

    if-nez v18, :cond_2

    .line 2332
    :try_start_2
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2335
    .end local v12    # "iterator":Landroid/content/EntityIterator;
    .end local v14    # "selection":Ljava/lang/String;
    .end local v15    # "selectionArgs":[Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 2337
    .local v7, "e":Landroid/os/RemoteException;
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Landroid/content/SyncStats;->numIoExceptions:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0

    .line 2282
    .end local v7    # "e":Landroid/os/RemoteException;
    .restart local v12    # "iterator":Landroid/content/EntityIterator;
    .restart local v14    # "selection":Ljava/lang/String;
    .restart local v15    # "selectionArgs":[Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apiary/ItemAndEntityHandler;->getEntityUri()Landroid/net/Uri;

    move-result-object v9

    .line 2283
    .local v9, "entityUri":Landroid/net/Uri;
    if-nez p4, :cond_8

    if-eqz v9, :cond_8

    .line 2284
    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getCount(Landroid/content/ContentProviderClient;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 2285
    .local v16, "total":I
    const/4 v6, 0x0

    .line 2286
    .local v6, "deletes":I
    :cond_3
    :goto_2
    invoke-interface {v12}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 2287
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v18

    if-eqz v18, :cond_4

    .line 2332
    :try_start_4
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 2290
    :cond_4
    :try_start_5
    invoke-interface {v12}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Entity;

    .line 2291
    .local v8, "entity":Landroid/content/Entity;
    invoke-virtual {v8}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v17

    .line 2292
    .local v17, "values":Landroid/content/ContentValues;
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apiary/ItemAndEntityHandler;->getDeletedColumnName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-eqz v18, :cond_5

    const/4 v11, 0x1

    .line 2294
    .local v11, "isDeleted":Z
    :goto_3
    if-eqz v11, :cond_3

    .line 2295
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2292
    .end local v11    # "isDeleted":Z
    :cond_5
    const/4 v11, 0x0

    goto :goto_3

    .line 2298
    .end local v8    # "entity":Landroid/content/Entity;
    .end local v17    # "values":Landroid/content/ContentValues;
    :cond_6
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v18, v0

    int-to-long v0, v6

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->hasTooManyChanges(JJ)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 2299
    const-string v18, "CalendarSyncAdapter"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "runSyncLoop: Too many deletions were found in provider "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", not doing any more updates"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2301
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/SyncStats;->clear()V

    .line 2302
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v18, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Landroid/content/SyncStats;->numEntries:J

    .line 2303
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v18, v0

    int-to-long v0, v6

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Landroid/content/SyncStats;->numDeletes:J

    .line 2304
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p2

    iput-boolean v0, v1, Landroid/content/SyncResult;->tooManyDeletions:Z

    .line 2305
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2332
    :try_start_6
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 2308
    :cond_7
    :try_start_7
    invoke-interface {v12}, Landroid/content/EntityIterator;->reset()V

    .line 2311
    .end local v6    # "deletes":I
    .end local v16    # "total":I
    :cond_8
    :goto_4
    invoke-interface {v12}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_a

    .line 2312
    invoke-static {}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->isCanceled()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v18

    if-eqz v18, :cond_9

    .line 2332
    :try_start_8
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_0

    .line 2315
    :cond_9
    :try_start_9
    invoke-interface {v12}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Entity;

    .line 2316
    .restart local v8    # "entity":Landroid/content/Entity;
    const/16 v18, 0x0

    invoke-virtual {v8}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v19

    const-string v20, "_id"

    invoke-virtual/range {v19 .. v20}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v15, v18
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2318
    :try_start_a
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->sendEntityToServer(Landroid/content/ContentProviderClient;Landroid/content/Entity;Lcom/google/android/apiary/ItemAndEntityHandler;Landroid/content/SyncResult;)V
    :try_end_a
    .catch Lcom/google/android/apiary/ParseException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/google/android/apiary/AuthenticationException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_4

    .line 2319
    :catch_1
    move-exception v7

    .line 2320
    .local v7, "e":Lcom/google/android/apiary/ParseException;
    :try_start_b
    const-string v18, "Error with entity"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v8, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->logErrorWithEntity(Ljava/lang/String;Landroid/content/Entity;Ljava/lang/Exception;)V

    .line 2321
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Landroid/content/SyncStats;->numParseExceptions:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_4

    .line 2332
    .end local v7    # "e":Lcom/google/android/apiary/ParseException;
    .end local v8    # "entity":Landroid/content/Entity;
    .end local v9    # "entityUri":Landroid/net/Uri;
    :catchall_0
    move-exception v18

    :try_start_c
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V

    throw v18
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_0

    .line 2322
    .restart local v8    # "entity":Landroid/content/Entity;
    .restart local v9    # "entityUri":Landroid/net/Uri;
    :catch_2
    move-exception v7

    .line 2325
    .local v7, "e":Lcom/google/android/apiary/AuthenticationException;
    :try_start_d
    throw v7

    .line 2326
    .end local v7    # "e":Lcom/google/android/apiary/AuthenticationException;
    :catch_3
    move-exception v7

    .line 2327
    .local v7, "e":Ljava/io/IOException;
    const-string v18, "Error with entity"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v8, v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->logErrorWithEntity(Ljava/lang/String;Landroid/content/Entity;Ljava/lang/Exception;)V

    .line 2328
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Landroid/content/SyncStats;->numIoExceptions:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_4

    .line 2332
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "entity":Landroid/content/Entity;
    :cond_a
    :try_start_e
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_1
.end method

.method public repairWrongDefaults(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 10
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    .line 2630
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 2631
    .local v7, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "cal_sync1"

    aput-object v3, v2, v9

    const-string v3, "sync_events=1"

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2637
    .local v6, "calendarCursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 2655
    :goto_0
    return-void

    .line 2641
    :cond_0
    :try_start_0
    const-string v1, "google_calendar_sync_max_attendees"

    const/16 v2, 0x32

    invoke-static {v7, v1, v2}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 2645
    .local v8, "maxAttendees":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2646
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2647
    .local v5, "calendarId":Ljava/lang/String;
    new-instance v0, Lcom/google/android/syncadapters/calendar/EventHandler;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mClient:Lcom/google/api/services/calendar/Calendar;

    move-object v2, p2

    move-object v3, p1

    move-object v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/EventHandler;-><init>(Lcom/google/api/services/calendar/Calendar;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 2649
    .local v0, "eventHandler":Lcom/google/android/syncadapters/calendar/EventHandler;
    invoke-direct {p0, p1, v5, v0, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->repairWrongDefaults(Landroid/content/ContentProviderClient;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/EventHandler;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2653
    .end local v0    # "eventHandler":Lcom/google/android/syncadapters/calendar/EventHandler;
    .end local v5    # "calendarId":Ljava/lang/String;
    .end local v8    # "maxAttendees":I
    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    .restart local v8    # "maxAttendees":I
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public updateColorMapFromGsf()V
    .locals 9

    .prologue
    .line 281
    const-string v7, "google_calendar_calendar_colors"

    sget-object v8, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mCalendarColorsGsf:Ljava/util/Map;

    invoke-direct {p0, v7, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->loadGsfColors(Ljava/lang/String;Ljava/util/Map;)V

    .line 282
    const-string v7, "google_calendar_event_colors"

    sget-object v8, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->mEventColorsGsf:Ljava/util/Map;

    invoke-direct {p0, v7, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->loadGsfColors(Ljava/lang/String;Ljava/util/Map;)V

    .line 284
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "com.android.calendar"

    invoke-virtual {v7, v8}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v6

    .line 286
    .local v6, "provider":Landroid/content/ContentProviderClient;
    if-nez v6, :cond_0

    .line 289
    const-string v7, "CalendarSyncAdapter"

    const-string v8, "Provider not found while updating colors"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :goto_0
    return-void

    .line 293
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    const-string v8, "com.google"

    invoke-virtual {v7, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 295
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v0, v2, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    .local v0, "account":Landroid/accounts/Account;
    :try_start_1
    invoke-direct {p0, v6, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorsInProvider(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/apiary/ParseException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 298
    :catch_0
    move-exception v3

    .line 299
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    const-string v7, "CalendarSyncAdapter"

    const-string v8, "Failed to update colors"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 307
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catchall_0
    move-exception v7

    invoke-virtual {v6}, Landroid/content/ContentProviderClient;->release()Z

    throw v7

    .line 300
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    .restart local v2    # "arr$":[Landroid/accounts/Account;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :catch_1
    move-exception v3

    .line 301
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v7, "CalendarSyncAdapter"

    const-string v8, "Failed to update colors"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 302
    .end local v3    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v3

    .line 303
    .local v3, "e":Lcom/google/android/apiary/ParseException;
    const-string v7, "CalendarSyncAdapter"

    const-string v8, "Failed to update colors"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 307
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v3    # "e":Lcom/google/android/apiary/ParseException;
    :cond_1
    invoke-virtual {v6}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0
.end method
