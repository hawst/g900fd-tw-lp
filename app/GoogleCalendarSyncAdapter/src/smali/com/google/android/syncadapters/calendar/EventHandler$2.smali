.class Lcom/google/android/syncadapters/calendar/EventHandler$2;
.super Ljava/lang/Object;
.source "EventHandler.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/api/services/calendar/model/EventReminder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/syncadapters/calendar/EventHandler;


# direct methods
.method constructor <init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V
    .locals 0

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/EventHandler$2;->this$0:Lcom/google/android/syncadapters/calendar/EventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/api/services/calendar/model/EventReminder;Lcom/google/api/services/calendar/model/EventReminder;)I
    .locals 3
    .param p1, "o1"    # Lcom/google/api/services/calendar/model/EventReminder;
    .param p2, "o2"    # Lcom/google/api/services/calendar/model/EventReminder;

    .prologue
    .line 1085
    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/EventReminder;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/EventReminder;->getMethod()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/syncadapters/calendar/EventHandler;->compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->access$000(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 1086
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 1087
    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/EventReminder;->getMinutes()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/EventReminder;->getMinutes()Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/google/android/syncadapters/calendar/EventHandler;->compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->access$000(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 1089
    :cond_0
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1082
    check-cast p1, Lcom/google/api/services/calendar/model/EventReminder;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/api/services/calendar/model/EventReminder;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/syncadapters/calendar/EventHandler$2;->compare(Lcom/google/api/services/calendar/model/EventReminder;Lcom/google/api/services/calendar/model/EventReminder;)I

    move-result v0

    return v0
.end method
