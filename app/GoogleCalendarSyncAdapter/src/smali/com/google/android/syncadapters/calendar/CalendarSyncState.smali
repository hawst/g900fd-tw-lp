.class public Lcom/google/android/syncadapters/calendar/CalendarSyncState;
.super Ljava/lang/Object;
.source "CalendarSyncState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;,
        Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    }
.end annotation


# static fields
.field private static final QUERY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mData:Lorg/json/JSONObject;

.field private final uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 790
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "sync_events"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "cal_sync1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cal_sync4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cal_sync5"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->QUERY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    .line 129
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    .line 130
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setVersion(I)V

    .line 131
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setFirstSeen(Z)V

    .line 132
    sget-boolean v0, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setJellyBean(Z)V

    .line 133
    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "data"    # Lorg/json/JSONObject;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    .line 137
    iput-object p2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    .line 138
    return-void
.end method

.method private static applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    .locals 4
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 499
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_0
    const-string v1, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Applying operations: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-virtual {p0, p1}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    :goto_0
    return-void

    .line 501
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Landroid/content/OperationApplicationException;
    const-string v1, "CalendarSyncAdapter"

    const-string v2, "Failed to apply operations while upgrading from ICS to JB"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 5
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 363
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 364
    .local v2, "values":Landroid/content/ContentValues;
    new-instance v0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    invoke-direct {v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>()V

    .line 365
    .local v0, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    const-string v3, "data"

    invoke-static {v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 366
    const-string v3, "account_name"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v3, "account_type"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    sget-object v3, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 370
    .local v1, "uri":Landroid/net/Uri;
    new-instance v3, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-direct {v3, v1, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V

    return-object v3
.end method

.method private static findUpgradeMethod(I)Ljava/lang/reflect/Method;
    .locals 6
    .param p0, "currentVersion"    # I

    .prologue
    .line 577
    :try_start_0
    const-class v1, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upgradeFrom"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-class v5, Landroid/content/ContentProviderClient;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-class v5, Landroid/accounts/Account;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing upgrade from version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static fromBytes(Landroid/net/Uri;[BLandroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "syncStateData"    # [B
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 151
    if-eqz p1, :cond_0

    .line 153
    :try_start_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    .line 154
    .local v1, "json":Ljava/lang/String;
    new-instance v2, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, p0, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v1    # "json":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Lorg/json/JSONException;
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->fromParcelBytes(Landroid/net/Uri;[BLandroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v2

    goto :goto_0

    .line 159
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    const-string v2, "CalendarSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Resetting sync state for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v2, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method static fromParcelBytes(Landroid/net/Uri;[BLandroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 26
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "syncStateData"    # [B
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 168
    if-eqz p1, :cond_8

    .line 169
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v19

    .line 170
    .local v19, "parcel":Landroid/os/Parcel;
    const/16 v23, 0x0

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 171
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 174
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->readInt()I

    move-result v15

    .line 175
    .local v15, "magic":I
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->readInt()I

    move-result v22

    .line 176
    .local v22, "version":I
    const v23, -0x21524111

    move/from16 v0, v23

    if-ne v15, v0, :cond_0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 177
    :cond_0
    const-string v23, "CalendarSyncAdapter"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Invalid MAGIC or VERSION: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-static {v15}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    const/16 v20, 0x0

    .line 248
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->recycle()V

    .line 251
    .end local v15    # "magic":I
    .end local v19    # "parcel":Landroid/os/Parcel;
    .end local v22    # "version":I
    :goto_0
    return-object v20

    .line 181
    .restart local v19    # "parcel":Landroid/os/Parcel;
    :catch_0
    move-exception v6

    .line 182
    .local v6, "e":Ljava/lang/RuntimeException;
    :try_start_1
    const-string v23, "CalendarSyncAdapter"

    const-string v24, "Error Parsing"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    const/16 v20, 0x0

    .line 248
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 185
    .end local v6    # "e":Ljava/lang/RuntimeException;
    .restart local v15    # "magic":I
    .restart local v22    # "version":I
    :cond_1
    :try_start_2
    const-string v23, "CalendarSyncAdapter"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "GData Sync State found, upgrading to V3. Account: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    new-instance v20, Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    new-instance v23, Lorg/json/JSONObject;

    invoke-direct/range {v23 .. v23}, Lorg/json/JSONObject;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;-><init>(Landroid/net/Uri;Lorg/json/JSONObject;)V

    .line 187
    .local v20, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    const/16 v23, 0x6

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setVersion(I)V

    .line 188
    sget-boolean v23, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setJellyBean(Z)V

    .line 189
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setFirstSeen(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    :try_start_3
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 197
    .local v4, "bundle":Landroid/os/Bundle;
    :try_start_4
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 198
    .local v10, "gdataFeed":Ljava/lang/String;
    const-string v23, "CalendarSyncAdapter"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Converting state for feed "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    const-string v23, "http"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 206
    invoke-static {v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getCalendarIdFromGdataFeedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 207
    .local v5, "calendarId":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->addFeed(Ljava/lang/String;)V

    .line 208
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v7

    .line 209
    .local v7, "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    move-object/from16 v0, p2

    invoke-static {v0, v7, v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setEventsRange(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Ljava/lang/String;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v11

    .line 212
    .local v11, "hasEvents":Z
    :try_start_5
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v21

    .line 213
    .local v21, "value":Landroid/os/Bundle;
    invoke-virtual/range {v21 .. v21}, Landroid/os/Bundle;->isEmpty()Z
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 218
    if-eqz v11, :cond_3

    .line 221
    :try_start_6
    const-string v23, "upgrade_max_start"

    const-wide/16 v24, 0x0

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v7, v0, v1, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    .line 222
    .local v16, "max":J
    const-string v23, "window_end"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 223
    .local v8, "end":J
    cmp-long v23, v8, v16

    if-lez v23, :cond_2

    .line 224
    const-string v23, "upgrade_max_start"

    move-object/from16 v0, v23

    invoke-virtual {v7, v0, v8, v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 248
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "calendarId":Ljava/lang/String;
    .end local v7    # "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .end local v8    # "end":J
    .end local v10    # "gdataFeed":Ljava/lang/String;
    .end local v11    # "hasEvents":Z
    .end local v15    # "magic":I
    .end local v16    # "max":J
    .end local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .end local v21    # "value":Landroid/os/Bundle;
    .end local v22    # "version":I
    :catchall_0
    move-exception v23

    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->recycle()V

    throw v23

    .line 193
    .restart local v15    # "magic":I
    .restart local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .restart local v22    # "version":I
    :catch_1
    move-exception v6

    .line 194
    .restart local v6    # "e":Ljava/lang/RuntimeException;
    :try_start_7
    const-string v23, "CalendarSyncAdapter"

    const-string v24, "Error Parsing"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 195
    const/16 v20, 0x0

    .line 248
    .end local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->recycle()V

    goto/16 :goto_0

    .line 214
    .end local v6    # "e":Ljava/lang/RuntimeException;
    .restart local v4    # "bundle":Landroid/os/Bundle;
    .restart local v5    # "calendarId":Ljava/lang/String;
    .restart local v7    # "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .restart local v10    # "gdataFeed":Ljava/lang/String;
    .restart local v11    # "hasEvents":Z
    .restart local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    :catch_2
    move-exception v6

    .line 215
    .restart local v6    # "e":Ljava/lang/RuntimeException;
    :try_start_8
    const-string v23, "CalendarSyncAdapter"

    const-string v24, "Error Parsing"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 216
    const/16 v20, 0x0

    .line 248
    .end local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->recycle()V

    goto/16 :goto_0

    .line 229
    .end local v6    # "e":Ljava/lang/RuntimeException;
    .restart local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .restart local v21    # "value":Landroid/os/Bundle;
    :cond_3
    :try_start_9
    invoke-virtual/range {v21 .. v21}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 230
    .local v14, "key":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    .line 231
    .local v18, "o":Ljava/lang/Object;
    move-object/from16 v0, v18

    instance-of v0, v0, Ljava/lang/String;

    move/from16 v23, v0

    if-eqz v23, :cond_5

    .line 232
    check-cast v18, Ljava/lang/String;

    .end local v18    # "o":Ljava/lang/Object;
    move-object/from16 v0, v18

    invoke-virtual {v7, v14, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 233
    .restart local v18    # "o":Ljava/lang/Object;
    :cond_5
    move-object/from16 v0, v18

    instance-of v0, v0, Ljava/lang/Boolean;

    move/from16 v23, v0

    if-eqz v23, :cond_6

    .line 234
    check-cast v18, Ljava/lang/Boolean;

    .end local v18    # "o":Ljava/lang/Object;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v7, v14, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 235
    .restart local v18    # "o":Ljava/lang/Object;
    :cond_6
    move-object/from16 v0, v18

    instance-of v0, v0, Ljava/lang/Long;

    move/from16 v23, v0

    if-eqz v23, :cond_4

    .line 236
    check-cast v18, Ljava/lang/Long;

    .end local v18    # "o":Ljava/lang/Object;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v7, v14, v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V

    goto :goto_2

    .line 241
    .end local v5    # "calendarId":Ljava/lang/String;
    .end local v7    # "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .end local v10    # "gdataFeed":Ljava/lang/String;
    .end local v11    # "hasEvents":Z
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "key":Ljava/lang/String;
    .end local v21    # "value":Landroid/os/Bundle;
    :cond_7
    invoke-static/range {p2 .. p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeCalendars(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 242
    invoke-static/range {p2 .. p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeEvents(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 243
    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 244
    const-string v23, "com.android.calendar"

    new-instance v24, Landroid/os/Bundle;

    invoke-direct/range {v24 .. v24}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 248
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->recycle()V

    goto/16 :goto_0

    .line 251
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v15    # "magic":I
    .end local v19    # "parcel":Landroid/os/Parcel;
    .end local v20    # "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .end local v22    # "version":I
    :cond_8
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method private static getCalendarIdFromGdataFeedUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "oldId"    # Ljava/lang/String;

    .prologue
    .line 274
    const-string v4, "https://www.google.com/calendar/feeds/"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 285
    .end local p0    # "oldId":Ljava/lang/String;
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    .local v1, "end":I
    .local v2, "id":Ljava/lang/String;
    .local v3, "start":I
    :goto_0
    return-object p0

    .line 278
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v1    # "end":I
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "start":I
    .restart local p0    # "oldId":Ljava/lang/String;
    :cond_0
    const-string v4, "https://www.google.com/calendar/feeds/"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 279
    .restart local v3    # "start":I
    const/16 v4, 0x2f

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 280
    .restart local v1    # "end":I
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 282
    .restart local v2    # "id":Ljava/lang/String;
    :try_start_0
    const-string v4, "UTF-8"

    invoke-static {v2, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .restart local v0    # "e":Ljava/io/UnsupportedEncodingException;
    move-object p0, v2

    .line 285
    goto :goto_0
.end method

.method public static getOrCreate(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 4
    .param p0, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 377
    sget-object v2, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p2, v2, p3}, Landroid/provider/SyncStateContract$Helpers;->getWithUri(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)Landroid/util/Pair;

    move-result-object v1

    .line 379
    .local v1, "syncStateUriAndData":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/net/Uri;[B>;"
    if-nez v1, :cond_1

    .line 380
    invoke-static {p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    .line 397
    :cond_0
    :goto_0
    return-object v0

    .line 382
    :cond_1
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/net/Uri;

    invoke-static {v2, p3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, [B

    invoke-static {v3, v2, p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->fromBytes(Landroid/net/Uri;[BLandroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    .line 387
    .local v0, "syncState":Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    if-nez v0, :cond_2

    .line 388
    const-string v2, "CalendarSyncAdapter"

    const-string v3, "Can\'t upgrade, wipe and resync"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    invoke-static {p1, p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getSyncStateForWipeAndResync(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    goto :goto_0

    .line 392
    :cond_2
    invoke-static {p0, v0, p1, p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeVersion(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 393
    sget-boolean v2, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->isJellyBean()Z

    move-result v2

    if-nez v2, :cond_0

    .line 394
    invoke-direct {v0, p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeFromIcsToJb(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method private static getSyncStateForWipeAndResync(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 927
    const-string v0, "CalendarSyncAdapter"

    const-string v1, "Upgrading to Apiary Sync Adapter"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    invoke-static {p0, p1, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeEventsAndCalendars(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 932
    const-string v0, "com.android.calendar"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p2, v0, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 933
    invoke-static {p1, p2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->create(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    move-result-object v0

    return-object v0
.end method

.method private static sanitizeRecurrence(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 4
    .param p0, "rule"    # Ljava/lang/String;
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 487
    if-eqz p0, :cond_0

    .line 488
    invoke-static {p0}, Lcom/google/android/syncadapters/calendar/Utils;->sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 489
    .local v0, "goodRule":Ljava/lang/String;
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 490
    const-string v1, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fixed bad "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'=>\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    invoke-virtual {p1, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    .end local v0    # "goodRule":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static setEventsRange(Landroid/content/ContentProviderClient;Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;Ljava/lang/String;)Z
    .locals 12
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "feedState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .param p2, "gdataFeed"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 333
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "dtstart"

    aput-object v3, v2, v0

    const-string v3, "cal_sync1=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const-string v5, "dtstart"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 340
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 342
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 344
    .local v10, "start":J
    const-string v0, "upgrade_min_start"

    const-wide/32 v2, 0x5265c00

    sub-long v2, v10, v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V

    .line 346
    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 347
    .local v8, "end":J
    :goto_0
    const-string v0, "upgrade_max_start"

    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v8

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    const/4 v0, 0x1

    .line 351
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 354
    .end local v8    # "end":J
    .end local v10    # "start":J
    :goto_1
    return v0

    .restart local v10    # "start":J
    :cond_0
    move-wide v8, v10

    .line 346
    goto :goto_0

    .line 351
    .end local v10    # "start":J
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 354
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 351
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B
    .locals 1
    .param p0, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private static transformSyncIds(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;)V
    .locals 20
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "like"    # Ljava/lang/String;
    .param p3, "t"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 424
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    .line 425
    .local v3, "uri":Landroid/net/Uri;
    const/4 v2, 0x6

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "_sync_id"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "original_sync_id"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "cal_sync1"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    const-string v5, "rrule"

    aput-object v5, v4, v2

    const/4 v2, 0x5

    const-string v5, "exrule"

    aput-object v5, v4, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_sync_id LIKE \'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\' OR "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "original_sync_id"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " LIKE \'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 439
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_3

    .line 441
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v13

    .line 442
    .local v13, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 443
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 444
    .local v16, "providerId":J
    const/4 v2, 0x3

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 445
    .local v8, "calendarId":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 446
    .local v18, "syncId":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 447
    .local v14, "origSyncId":Ljava/lang/String;
    const/4 v2, 0x4

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 448
    .local v15, "rrule":Ljava/lang/String;
    const/4 v2, 0x5

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 449
    .local v12, "exrule":Ljava/lang/String;
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 450
    .local v19, "values":Landroid/content/ContentValues;
    const-string v2, "_sync_id"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v8, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;->transform(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const-string v2, "original_sync_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v8, v14}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;->transform(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 457
    .local v11, "eventUri":Landroid/net/Uri;
    :try_start_1
    const-string v2, "rrule"

    move-object/from16 v0, v19

    invoke-static {v15, v0, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->sanitizeRecurrence(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 458
    const-string v2, "exrule"

    move-object/from16 v0, v19

    invoke-static {v12, v0, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->sanitizeRecurrence(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 459
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    :goto_1
    :try_start_2
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v4, 0x64

    if-le v2, v4, :cond_0

    .line 473
    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 474
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 481
    .end local v8    # "calendarId":Ljava/lang/String;
    .end local v11    # "eventUri":Landroid/net/Uri;
    .end local v12    # "exrule":Ljava/lang/String;
    .end local v13    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v14    # "origSyncId":Ljava/lang/String;
    .end local v15    # "rrule":Ljava/lang/String;
    .end local v16    # "providerId":J
    .end local v18    # "syncId":Ljava/lang/String;
    .end local v19    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2

    .line 464
    .restart local v8    # "calendarId":Ljava/lang/String;
    .restart local v11    # "eventUri":Landroid/net/Uri;
    .restart local v12    # "exrule":Ljava/lang/String;
    .restart local v13    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v14    # "origSyncId":Ljava/lang/String;
    .restart local v15    # "rrule":Ljava/lang/String;
    .restart local v16    # "providerId":J
    .restart local v18    # "syncId":Ljava/lang/String;
    .restart local v19    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v10

    .line 465
    .local v10, "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    :try_start_3
    const-string v2, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad recurrence rule in event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Removing it from database"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 467
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 477
    .end local v8    # "calendarId":Ljava/lang/String;
    .end local v10    # "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    .end local v11    # "eventUri":Landroid/net/Uri;
    .end local v12    # "exrule":Ljava/lang/String;
    .end local v14    # "origSyncId":Ljava/lang/String;
    .end local v15    # "rrule":Ljava/lang/String;
    .end local v16    # "providerId":J
    .end local v18    # "syncId":Ljava/lang/String;
    .end local v19    # "values":Landroid/content/ContentValues;
    :cond_1
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 478
    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 481
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 484
    .end local v13    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_3
    return-void
.end method

.method private static upgradeCalendars(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 14
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 292
    const-string v0, "CalendarSyncAdapter"

    const-string v1, "Upgrading calendars"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v13

    const-string v0, "cal_sync1"

    aput-object v0, v2, v12

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/String;

    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v13

    iget-object v0, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v0, v4, v12

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 298
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 300
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 301
    .local v8, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 302
    .local v9, "values":Landroid/content/ContentValues;
    const-string v1, "cal_sync2"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v9, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string v1, "cal_sync3"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v9, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 305
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 306
    .local v10, "rowId":J
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 307
    .local v7, "feedId":Ljava/lang/String;
    const-string v0, "cal_sync1"

    invoke-static {v7}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getCalendarIdFromGdataFeedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 316
    invoke-static {p0, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 317
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 324
    .end local v7    # "feedId":Ljava/lang/String;
    .end local v8    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v9    # "values":Landroid/content/ContentValues;
    .end local v10    # "rowId":J
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 320
    .restart local v8    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    :try_start_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 321
    invoke-static {p0, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 324
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 327
    .end local v8    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_3
    return-void
.end method

.method private static upgradeEvents(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 2
    .param p0, "provider"    # Landroid/content/ContentProviderClient;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 256
    const-string v0, "CalendarSyncAdapter"

    const-string v1, "Upgrading events"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v0, "http://www.google.com/calendar/feeds/%/events/%"

    new-instance v1, Lcom/google/android/syncadapters/calendar/CalendarSyncState$1;

    invoke-direct {v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$1;-><init>()V

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->transformSyncIds(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;)V

    .line 271
    return-void
.end method

.method private upgradeFrom(ILcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 7
    .param p1, "currentVersion"    # I
    .param p2, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    .line 534
    invoke-static {p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->findUpgradeMethod(I)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 536
    .local v3, "upgradeMethod":Ljava/lang/reflect/Method;
    const/4 v4, 0x4

    :try_start_0
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    const/4 v5, 0x3

    aput-object p5, v4, v5

    invoke-virtual {v3, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 539
    .local v2, "newVersion":I
    if-gez v2, :cond_1

    .line 540
    add-int/lit8 v2, p1, 0x1

    .line 551
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setVersion(I)V

    .line 552
    invoke-virtual {p0, p4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 553
    const-string v4, "CalendarSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Completed upgrade from version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    return v2

    .line 544
    :cond_1
    if-le v2, p1, :cond_2

    if-le v2, v6, :cond_0

    .line 545
    :cond_2
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Upgrade method "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " returned invalid new version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " <= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " or > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 555
    .end local v2    # "newVersion":I
    :catch_0
    move-exception v1

    .line 556
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 557
    .local v0, "cause":Ljava/lang/Throwable;
    if-nez v0, :cond_3

    .line 558
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Invocation failed with null cause."

    invoke-direct {v4, v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 560
    :cond_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_4

    .line 561
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 563
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_4
    instance-of v4, v0, Landroid/os/RemoteException;

    if-eqz v4, :cond_5

    .line 564
    check-cast v0, Landroid/os/RemoteException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 566
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_5
    instance-of v4, v0, Ljava/io/IOException;

    if-eqz v4, :cond_6

    .line 567
    check-cast v0, Ljava/io/IOException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 569
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_6
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Failed to invoke upgrade Method"

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 570
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v1

    .line 571
    .local v1, "e":Ljava/lang/IllegalAccessException;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Failed to invoke upgrade Method"

    invoke-direct {v4, v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method private upgradeFromIcsToJb(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 2
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 403
    const-string v0, "%\n%"

    new-instance v1, Lcom/google/android/syncadapters/calendar/CalendarSyncState$2;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$2;-><init>(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)V

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->transformSyncIds(Landroid/content/ContentProviderClient;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/syncadapters/calendar/CalendarSyncState$IdTransformer;)V

    .line 413
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setJellyBean(Z)V

    .line 414
    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 415
    return-void
.end method

.method private static upgradeVersion(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Lcom/google/android/syncadapters/calendar/CalendarSyncState;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 7
    .param p0, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p1, "syncState"    # Lcom/google/android/syncadapters/calendar/CalendarSyncState;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    .line 513
    invoke-virtual {p1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getVersion()I

    move-result v1

    .line 515
    .local v1, "currentVersion":I
    :goto_0
    if-ge v1, v6, :cond_0

    move-object v0, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 516
    invoke-direct/range {v0 .. v5}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->upgradeFrom(ILcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I

    move-result v1

    goto :goto_0

    .line 519
    :cond_0
    if-le v1, v6, :cond_1

    .line 520
    const-string v0, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wipe Data on Downgrade from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    invoke-virtual {p1, p2, p3, p4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeDataAndForceFullSync(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 522
    invoke-virtual {p1, v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setVersion(I)V

    .line 523
    invoke-virtual {p1, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    .line 525
    :cond_1
    return-void
.end method

.method private static wipeEventsAndCalendars(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 939
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 943
    .local v1, "calendarsUri":Landroid/net/Uri;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "calendar_displayName"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "sync_events"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "visible"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 952
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 954
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saved-calendar-settings-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v8

    .line 958
    .local v8, "filename":Ljava/lang/String;
    :try_start_1
    new-instance v10, Ljava/io/PrintWriter;

    const/4 v0, 0x0

    invoke-virtual {p0, v8, v0}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 960
    .local v10, "out":Ljava/io/PrintWriter;
    :goto_0
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 961
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 962
    .local v9, "name":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 963
    .local v11, "sync":I
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 964
    .local v12, "visible":I
    const-string v0, "%d:%d:%s\n"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v9, v2, v3

    invoke-virtual {v10, v0, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 967
    .end local v9    # "name":Ljava/lang/String;
    .end local v11    # "sync":I
    .end local v12    # "visible":I
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v10}, Ljava/io/PrintWriter;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 969
    .end local v10    # "out":Ljava/io/PrintWriter;
    :catch_0
    move-exception v7

    .line 970
    .local v7, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    const-string v0, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to create save file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 975
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 978
    .end local v8    # "filename":Ljava/lang/String;
    :cond_0
    const-string v0, "account_name=? AND account_type=?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 981
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "account_name=? AND account_type=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 985
    return-void

    .line 967
    .restart local v8    # "filename":Ljava/lang/String;
    .restart local v10    # "out":Ljava/io/PrintWriter;
    :cond_1
    :try_start_5
    invoke-virtual {v10}, Ljava/io/PrintWriter;->close()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 971
    .end local v10    # "out":Ljava/io/PrintWriter;
    :catch_1
    move-exception v7

    .line 972
    .local v7, "e":Ljava/io/IOException;
    :try_start_6
    const-string v0, "CalendarSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to write settings to save file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 975
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "filename":Ljava/lang/String;
    :catchall_1
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public addFeed(Ljava/lang/String;)V
    .locals 2
    .param p1, "feedId"    # Ljava/lang/String;

    .prologue
    .line 1063
    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1067
    :goto_0
    return-void

    .line 1064
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .locals 5
    .param p1, "calendarId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1045
    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1047
    :try_start_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v3, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, p0, v3, v4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;-><init>(Lcom/google/android/syncadapters/calendar/CalendarSyncState;Lorg/json/JSONObject;Lcom/google/android/syncadapters/calendar/CalendarSyncState$1;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1053
    :goto_0
    return-object v1

    .line 1048
    :catch_0
    move-exception v0

    .line 1049
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "CalendarSyncAdapter"

    const-string v3, "Bad feed object in sync state"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    .line 1050
    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    move-object v1, v2

    .line 1053
    goto :goto_0
.end method

.method public getVersion()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 998
    :try_start_0
    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v3, "version"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v3, "version"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1000
    :cond_0
    :goto_0
    return v1

    .line 999
    :catch_0
    move-exception v0

    .line 1000
    .local v0, "e":Lorg/json/JSONException;
    goto :goto_0
.end method

.method public hasFeed(Ljava/lang/String;)Z
    .locals 1
    .param p1, "feedId"    # Ljava/lang/String;

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isFirstSeen()Z
    .locals 3

    .prologue
    .line 1014
    :try_start_0
    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v2, "firstSeen"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1016
    :goto_0
    return v1

    .line 1015
    :catch_0
    move-exception v0

    .line 1016
    .local v0, "e":Lorg/json/JSONException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isJellyBean()Z
    .locals 3

    .prologue
    .line 1030
    :try_start_0
    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v2, "jellyBeanOrNewer"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1032
    :goto_0
    return v1

    .line 1031
    :catch_0
    move-exception v0

    .line 1032
    .local v0, "e":Lorg/json/JSONException;
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public newUpdateOperation()Landroid/content/ContentProviderOperation;
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/SyncStateContract$Helpers;->newUpdateOperation(Landroid/net/Uri;[B)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public setFirstSeen(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1022
    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v1, "firstSeen"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1026
    :goto_0
    return-void

    .line 1023
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setJellyBean(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 1038
    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v1, "jellyBeanOrNewer"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1042
    :goto_0
    return-void

    .line 1039
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setVersion(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1006
    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    const-string v1, "version"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1010
    :goto_0
    return-void

    .line 1007
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateInProvider(Landroid/content/ContentProviderClient;)V
    .locals 2
    .param p1, "provider"    # Landroid/content/ContentProviderClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 988
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->uri:Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->toBytes(Lcom/google/android/syncadapters/calendar/CalendarSyncState;)[B

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/provider/SyncStateContract$Helpers;->update(Landroid/content/ContentProviderClient;Landroid/net/Uri;[B)V

    .line 989
    return-void
.end method

.method public upgradeFrom0(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 1
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 601
    invoke-virtual {p0, p2, p3, p4}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeDataAndForceFullSync(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 602
    const/4 v0, -0x1

    return v0
.end method

.method public upgradeFrom1(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 6
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 611
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 612
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "allowedReminders"

    const-string v2, "0,1,2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p4}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p4, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p3, v1, v0, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 620
    const/4 v1, -0x1

    return v1
.end method

.method public upgradeFrom2(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 1
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    invoke-virtual {p1, p3, p4}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->repairWrongDefaults(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 630
    const/4 v0, -0x1

    return v0
.end method

.method public upgradeFrom3(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 16
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 641
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-le v2, v4, :cond_0

    .line 643
    const/4 v2, -0x1

    .line 703
    :goto_0
    return v2

    .line 646
    :cond_0
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v3

    .line 647
    .local v3, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    const-string v5, "exdate LIKE \'%\' || x\'0A\' || \'%\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p3

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 653
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 655
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v15

    .line 656
    .local v15, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_1
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 658
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 659
    .local v12, "id":J
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 660
    .local v9, "cv":Landroid/content/ContentValues;
    const-string v2, "_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v9, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 661
    const-string v2, "eventStatus"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 662
    const-string v2, "dtstart"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 663
    const-string v2, "dtend"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 664
    const-string v2, "duration"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 665
    const-string v2, "eventTimezone"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 667
    const-string v2, "allDay"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 668
    const-string v2, "rrule"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 669
    const-string v2, "rdate"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 670
    const-string v2, "exrule"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 671
    const-string v2, "exdate"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 673
    const-string v2, "exdate"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 674
    .local v11, "exdate":Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/syncadapters/calendar/Utils;->collateDatesByTimeZone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 675
    .local v14, "newExdate":Ljava/lang/String;
    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 678
    const-string v2, "exdate"

    invoke-virtual {v9, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 682
    .local v10, "eventUri":Landroid/net/Uri;
    const-string v2, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Collating EXDATE for event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    const-string v2, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  Old value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    const-string v2, "CalendarSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  New value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v4, 0x64

    if-le v2, v4, :cond_1

    .line 691
    move-object/from16 v0, p3

    invoke-static {v0, v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 692
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 699
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v10    # "eventUri":Landroid/net/Uri;
    .end local v11    # "exdate":Ljava/lang/String;
    .end local v12    # "id":J
    .end local v14    # "newExdate":Ljava/lang/String;
    .end local v15    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 695
    .restart local v15    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_2
    :try_start_1
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 696
    move-object/from16 v0, p3

    invoke-static {v0, v15}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 699
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 703
    .end local v15    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_4
    const/4 v2, -0x1

    goto/16 :goto_0
.end method

.method public upgradeFrom4(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 18
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 721
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "rrule LIKE \'%;UNTIL=%\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p3

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 727
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 728
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v16

    .line 730
    .local v16, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 731
    const-string v2, "rrule"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 733
    .local v17, "rrule":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/google/android/syncadapters/calendar/Utils;->sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 734
    .local v14, "newRule":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 735
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 736
    .local v9, "cv":Landroid/content/ContentValues;
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 737
    .local v12, "id":J
    const-string v2, "_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 738
    const-string v2, "rrule"

    invoke-virtual {v9, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string v2, "eventStatus"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 743
    const-string v2, "dtstart"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 745
    const-string v2, "dtend"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 747
    const-string v2, "duration"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 749
    const-string v2, "eventTimezone"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 751
    const-string v2, "allDay"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 753
    const-string v2, "rdate"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 755
    const-string v2, "exrule"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 757
    const-string v2, "exdate"

    invoke-static {v8, v9, v2}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 760
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 770
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v12    # "id":J
    .end local v14    # "newRule":Ljava/lang/String;
    .end local v17    # "rrule":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 772
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 773
    .local v10, "execute":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/ContentProviderOperation;

    .line 774
    .local v15, "operation":Landroid/content/ContentProviderOperation;
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x64

    if-le v2, v3, :cond_2

    .line 776
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 777
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 780
    .end local v15    # "operation":Landroid/content/ContentProviderOperation;
    :cond_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 781
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->applyOperations(Landroid/content/ContentProviderClient;Ljava/util/ArrayList;)V

    .line 785
    .end local v10    # "execute":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v16    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_4
    const/4 v2, -0x1

    return v2
.end method

.method public upgradeFrom5(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 19
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 818
    const/4 v14, 0x0

    .line 820
    .local v14, "foundSyncAdapter":Z
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 821
    .local v17, "pm":Landroid/content/pm/PackageManager;
    const/16 v4, 0x80

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/PackageInfo;

    .line 822
    .local v16, "packageInfo":Landroid/content/pm/PackageInfo;
    const-string v4, "com.google.android.syncadapters.calendar"

    move-object/from16 v0, v16

    iget-object v6, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 823
    const/4 v14, 0x1

    goto :goto_0

    .line 827
    .end local v16    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    if-nez v14, :cond_2

    .line 830
    const/4 v4, -0x1

    .line 881
    :goto_1
    return v4

    .line 836
    :cond_2
    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    .line 837
    .local v5, "uri":Landroid/net/Uri;
    sget-object v6, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->QUERY_PROJECTION:[Ljava/lang/String;

    const-string v7, "account_name=? AND account_type=?"

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p4

    iget-object v9, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v9, v8, v4

    const/4 v4, 0x1

    move-object/from16 v0, p4

    iget-object v9, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v9, v8, v4

    const/4 v9, 0x0

    move-object/from16 v4, p3

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 843
    .local v12, "cursor":Landroid/database/Cursor;
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 845
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 847
    .local v11, "calendarIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    const-string v4, "sync_events"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_4

    .line 849
    const/4 v4, -0x1

    goto :goto_1

    .line 851
    :cond_4
    const-string v4, "cal_sync1"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 853
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 856
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 857
    .local v18, "values":Landroid/content/ContentValues;
    const-string v4, "sync_events"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 858
    const-string v4, "visible"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 860
    const-string v4, "account_name=? AND account_type=? AND cal_sync4=? AND cal_sync5=?"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p4

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p4

    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "1"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "0"

    aput-object v8, v6, v7

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v5, v1, v4, v6}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 869
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 870
    .local v10, "calendarId":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v13

    .line 871
    .local v13, "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    if-eqz v13, :cond_5

    .line 872
    invoke-virtual {v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->clear()V

    .line 874
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->updateInProvider(Landroid/content/ContentProviderClient;)V

    goto :goto_2

    .line 878
    .end local v10    # "calendarId":Ljava/lang/String;
    .end local v11    # "calendarIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v13    # "feedState":Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;
    .end local v18    # "values":Landroid/content/ContentValues;
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeDataAndForceFullSync(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 881
    :cond_7
    const/4 v4, -0x1

    goto/16 :goto_1
.end method

.method public upgradeFrom6(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)I
    .locals 6
    .param p1, "syncAdapter"    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 892
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 893
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "allowedReminders"

    const-string v2, "0,1,2,4"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p4}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p4, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p3, v1, v0, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 901
    const/4 v1, -0x1

    return v1
.end method

.method public wipeDataAndForceFullSync(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "provider"    # Landroid/content/ContentProviderClient;
    .param p3, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 910
    invoke-static {p1, p2, p3}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->wipeEventsAndCalendars(Landroid/content/Context;Landroid/content/ContentProviderClient;Landroid/accounts/Account;)V

    .line 911
    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->mData:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    .line 912
    .local v0, "iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 913
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 914
    .local v1, "key":Ljava/lang/String;
    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_0

    .line 915
    invoke-virtual {p0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->getFeedState(Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState$FeedState;->clear()V

    goto :goto_0

    .line 919
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncState;->setFirstSeen(Z)V

    .line 920
    const-string v2, "com.android.calendar"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {p3, v2, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 921
    return-void
.end method
