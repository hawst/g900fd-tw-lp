.class Lcom/google/android/syncadapters/calendar/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"

# interfaces
.implements Lcom/google/android/apiary/ItemAndEntityHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apiary/ItemAndEntityHandler",
        "<",
        "Lcom/google/api/services/calendar/model/Event;",
        ">;"
    }
.end annotation


# static fields
.field private static final ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static UNSYNCED_EVENT_KEYS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mCalendarId:Ljava/lang/String;

.field private final mClient:Lcom/google/api/services/calendar/Calendar;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mEventPlusPattern:Ljava/util/regex/Pattern;

.field private mProvider:Landroid/content/ContentProviderClient;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 158
    new-array v4, v10, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "reminders"

    aput-object v6, v4, v5

    const-string v5, "extendedProperties"

    aput-object v5, v4, v7

    const-string v5, "ifmatch"

    aput-object v5, v4, v8

    const-string v5, "userAgentPackage"

    aput-object v5, v4, v9

    invoke-static {v4}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    sput-object v4, Lcom/google/android/syncadapters/calendar/EventHandler;->UNSYNCED_EVENT_KEYS:Ljava/util/Set;

    .line 2031
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2032
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v4, "needsAction"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2033
    const-string v4, "accepted"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2034
    const-string v4, "declined"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2035
    const-string v4, "tentative"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2036
    sput-object v2, Lcom/google/android/syncadapters/calendar/EventHandler;->ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;

    .line 2038
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    sput-object v4, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    .line 2040
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2041
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 2042
    .local v3, "originalValue":Ljava/lang/Integer;
    sget-object v4, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2043
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " was already encountered"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2046
    :cond_0
    sget-object v4, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 2048
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "originalValue":Ljava/lang/Integer;
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/calendar/Calendar;Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 3
    .param p1, "client"    # Lcom/google/api/services/calendar/Calendar;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "provider"    # Landroid/content/ContentProviderClient;
    .param p4, "contentResolver"    # Landroid/content/ContentResolver;
    .param p5, "calendarId"    # Ljava/lang/String;

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    .line 181
    iput-object p2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    .line 182
    iput-object p3, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    .line 183
    iput-object p4, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mContentResolver:Landroid/content/ContentResolver;

    .line 184
    iput-object p5, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    .line 186
    const-string v1, "google_calendar_event_plus_pattern"

    const-string v2, "^http[s]?://plus(\\.[a-z0-9]+)*\\.google\\.com/events/"

    invoke-static {p4, v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "regex":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mEventPlusPattern:Ljava/util/regex/Pattern;

    .line 189
    return-void
.end method

.method static synthetic access$000(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/Comparable;
    .param p1, "x1"    # Ljava/lang/Comparable;

    .prologue
    .line 86
    invoke-static {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    return v0
.end method

.method private addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 15
    .param p1, "eventValues"    # Landroid/content/ContentValues;
    .param p3, "event"    # Lcom/google/api/services/calendar/model/Event;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentValues;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1104
    .local p2, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1105
    .local v2, "attendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Entity$NamedContentValues;

    .line 1106
    .local v7, "namedContentValues":Landroid/content/Entity$NamedContentValues;
    iget-object v10, v7, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    .line 1107
    .local v10, "uri":Landroid/net/Uri;
    iget-object v11, v7, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    .line 1108
    .local v11, "values":Landroid/content/ContentValues;
    sget-object v12, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12, v10}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1109
    new-instance v1, Lcom/google/api/services/calendar/model/EventAttendee;

    invoke-direct {v1}, Lcom/google/api/services/calendar/model/EventAttendee;-><init>()V

    .line 1110
    .local v1, "attendee":Lcom/google/api/services/calendar/model/EventAttendee;
    const-string v12, "attendeeName"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/google/api/services/calendar/model/EventAttendee;->setDisplayName(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    .line 1111
    const-string v12, "attendeeEmail"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1112
    .local v3, "email":Ljava/lang/String;
    const-string v12, "attendeeIdentity"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1114
    .local v6, "identity":Ljava/lang/String;
    if-eqz v6, :cond_3

    const-string v12, "gprofile:"

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1119
    const-string v12, "gprofile:"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1132
    .local v5, "id":Ljava/lang/String;
    :goto_1
    if-eqz v5, :cond_6

    .line 1133
    const-string v12, "id"

    invoke-virtual {v1, v12, v5}, Lcom/google/api/services/calendar/model/EventAttendee;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/EventAttendee;

    .line 1138
    :goto_2
    const-string v12, "attendeeStatus"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1140
    .local v8, "providerStatus":I
    sget-object v12, Lcom/google/android/syncadapters/calendar/EventHandler;->PROVIDER_TYPE_TO_ENTRY_ATTENDEE:Landroid/util/SparseArray;

    invoke-virtual {v12, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1141
    .local v9, "status":Ljava/lang/String;
    if-nez v9, :cond_1

    .line 1142
    const-string v12, "EventHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unknown attendee status: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    const-string v9, "needsAction"

    .line 1145
    :cond_1
    invoke-virtual {v1, v9}, Lcom/google/api/services/calendar/model/EventAttendee;->setResponseStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    .line 1147
    const-string v12, "attendeeType"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_2

    .line 1148
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/google/api/services/calendar/model/EventAttendee;->setOptional(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/EventAttendee;

    .line 1150
    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1120
    .end local v5    # "id":Ljava/lang/String;
    .end local v8    # "providerStatus":I
    .end local v9    # "status":Ljava/lang/String;
    :cond_3
    if-eqz v3, :cond_4

    const-string v12, "@profile.calendar.google.com"

    invoke-virtual {v3, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1123
    const/4 v12, 0x0

    const-string v13, "@profile.calendar.google.com"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "id":Ljava/lang/String;
    goto :goto_1

    .line 1124
    .end local v5    # "id":Ljava/lang/String;
    :cond_4
    const-string v12, "sync_data3"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1128
    const-string v12, "sync_data3"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "id":Ljava/lang/String;
    goto :goto_1

    .line 1130
    .end local v5    # "id":Ljava/lang/String;
    :cond_5
    const/4 v5, 0x0

    .restart local v5    # "id":Ljava/lang/String;
    goto :goto_1

    .line 1135
    :cond_6
    invoke-virtual {v1, v3}, Lcom/google/api/services/calendar/model/EventAttendee;->setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventAttendee;

    goto :goto_2

    .line 1153
    .end local v1    # "attendee":Lcom/google/api/services/calendar/model/EventAttendee;
    .end local v3    # "email":Ljava/lang/String;
    .end local v5    # "id":Ljava/lang/String;
    .end local v6    # "identity":Ljava/lang/String;
    .end local v7    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .end local v10    # "uri":Landroid/net/Uri;
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_8

    .line 1154
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/api/services/calendar/model/Event;->setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    .line 1156
    :cond_8
    return-void
.end method

.method private static addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 999
    .local p0, "oldProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p1, "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p0, :cond_0

    move-object v2, p1

    .line 1013
    .end local p1    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local v2, "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-object v2

    .line 1003
    .end local v2    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local p1    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    if-nez p1, :cond_1

    .line 1004
    new-instance p1, Ljava/util/HashMap;

    .end local p1    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 1006
    .restart local p1    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1007
    .local v1, "key":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1010
    const-string v3, ""

    invoke-interface {p1, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .end local v1    # "key":Ljava/lang/String;
    :cond_3
    move-object v2, p1

    .line 1013
    .end local p1    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v2    # "newProperties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0
.end method

.method private addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 10
    .param p2, "event"    # Lcom/google/api/services/calendar/model/Event;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1216
    .local p1, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v0

    .line 1217
    .local v0, "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    if-nez v0, :cond_0

    .line 1218
    new-instance v0, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .end local v0    # "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    invoke-direct {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;-><init>()V

    .line 1219
    .restart local v0    # "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    invoke-virtual {p2, v0}, Lcom/google/api/services/calendar/model/Event;->setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;

    .line 1221
    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v4

    .line 1222
    .local v4, "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v4, :cond_1

    .line 1223
    new-instance v4, Ljava/util/HashMap;

    .end local v4    # "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1224
    .restart local v4    # "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setPrivate(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .line 1226
    :cond_1
    invoke-virtual {v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v5

    .line 1227
    .local v5, "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v5, :cond_2

    .line 1228
    new-instance v5, Ljava/util/HashMap;

    .end local v5    # "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1229
    .restart local v5    # "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v0, v5}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setShared(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .line 1231
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Entity$NamedContentValues;

    .line 1232
    .local v3, "namedContentValues":Landroid/content/Entity$NamedContentValues;
    iget-object v6, v3, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    .line 1233
    .local v6, "uri":Landroid/net/Uri;
    iget-object v8, v3, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    .line 1234
    .local v8, "values":Landroid/content/ContentValues;
    sget-object v9, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1235
    const-string v9, "name"

    invoke-virtual {v8, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1236
    .local v2, "name":Ljava/lang/String;
    const-string v9, "value"

    invoke-virtual {v8, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1237
    .local v7, "value":Ljava/lang/String;
    const-string v9, "shared:"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1238
    const-string v9, "shared:"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1240
    :cond_4
    const-string v9, "private:"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1241
    const-string v9, "private:"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1244
    :cond_5
    invoke-interface {v4, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1248
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "value":Ljava/lang/String;
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_6
    return-void
.end method

.method private addRecurrenceToEntry(Lcom/android/calendarcommon2/ICalendar$Component;Lcom/google/api/services/calendar/model/Event;)V
    .locals 6
    .param p1, "component"    # Lcom/android/calendarcommon2/ICalendar$Component;
    .param p2, "event"    # Lcom/google/api/services/calendar/model/Event;

    .prologue
    .line 1251
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1252
    .local v4, "recurrence":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/android/calendarcommon2/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1253
    .local v3, "propertyName":Ljava/lang/String;
    const-string v5, "RRULE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "RDATE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "EXRULE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "EXDATE"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1257
    :cond_1
    invoke-virtual {p1, v3}, Lcom/android/calendarcommon2/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendarcommon2/ICalendar$Property;

    .line 1258
    .local v2, "property":Lcom/android/calendarcommon2/ICalendar$Property;
    invoke-virtual {v2}, Lcom/android/calendarcommon2/ICalendar$Property;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1262
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "property":Lcom/android/calendarcommon2/ICalendar$Property;
    .end local v3    # "propertyName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2, v4}, Lcom/google/api/services/calendar/model/Event;->setRecurrence(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    .line 1263
    return-void
.end method

.method private addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V
    .locals 15
    .param p2, "event"    # Lcom/google/api/services/calendar/model/Event;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Entity$NamedContentValues;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1160
    .local p1, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1161
    .local v8, "overrides":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1162
    .local v1, "alarmValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Entity$NamedContentValues;

    .line 1163
    .local v7, "namedContentValues":Landroid/content/Entity$NamedContentValues;
    iget-object v11, v7, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    .line 1164
    .local v11, "uri":Landroid/net/Uri;
    iget-object v12, v7, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    .line 1165
    .local v12, "values":Landroid/content/ContentValues;
    sget-object v13, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v13, v11}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1166
    const-string v13, "method"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1167
    .local v5, "method":I
    const-string v13, "minutes"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 1168
    .local v6, "minutes":Ljava/lang/Integer;
    new-instance v13, Lcom/google/api/services/calendar/model/EventReminder;

    invoke-direct {v13}, Lcom/google/api/services/calendar/model/EventReminder;-><init>()V

    invoke-virtual {v13, v6}, Lcom/google/api/services/calendar/model/EventReminder;->setMinutes(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/EventReminder;

    move-result-object v9

    .line 1169
    .local v9, "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    packed-switch v5, :pswitch_data_0

    .line 1194
    const-string v13, "popup"

    invoke-virtual {v9, v13}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    .line 1195
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199
    .end local v5    # "method":I
    .end local v6    # "minutes":Ljava/lang/Integer;
    .end local v9    # "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x5

    if-ne v13, v14, :cond_0

    .line 1203
    .end local v7    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .end local v11    # "uri":Landroid/net/Uri;
    .end local v12    # "values":Landroid/content/ContentValues;
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_3

    .line 1204
    new-instance v10, Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct {v10}, Lcom/google/api/services/calendar/model/Event$Reminders;-><init>()V

    .line 1205
    .local v10, "reminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v10, v13}, Lcom/google/api/services/calendar/model/Event$Reminders;->setUseDefault(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event$Reminders;

    .line 1206
    invoke-virtual {v10, v8}, Lcom/google/api/services/calendar/model/Event$Reminders;->setOverrides(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event$Reminders;

    .line 1207
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/google/api/services/calendar/model/Event;->setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;

    .line 1209
    .end local v10    # "reminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_4

    .line 1210
    move-object/from16 v0, p2

    invoke-direct {p0, v1, v0}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 1212
    :cond_4
    return-void

    .line 1171
    .restart local v5    # "method":I
    .restart local v6    # "minutes":Ljava/lang/Integer;
    .restart local v7    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    .restart local v9    # "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    .restart local v11    # "uri":Landroid/net/Uri;
    .restart local v12    # "values":Landroid/content/ContentValues;
    :pswitch_0
    const-string v13, "popup"

    invoke-virtual {v9, v13}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    .line 1172
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1175
    :pswitch_1
    const-string v13, "email"

    invoke-virtual {v9, v13}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    .line 1176
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1179
    :pswitch_2
    const-string v13, "sms"

    invoke-virtual {v9, v13}, Lcom/google/api/services/calendar/model/EventReminder;->setMethod(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventReminder;

    .line 1180
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1185
    :pswitch_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1186
    .local v2, "contentValues":Landroid/content/ContentValues;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "private:alarmReminder-"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1188
    .local v4, "key":Ljava/lang/String;
    const-string v13, "name"

    invoke-virtual {v2, v13, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    const-string v13, "value"

    invoke-virtual {v2, v13, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1190
    new-instance v13, Landroid/content/Entity$NamedContentValues;

    sget-object v14, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v13, v14, v2}, Landroid/content/Entity$NamedContentValues;-><init>(Landroid/net/Uri;Landroid/content/ContentValues;)V

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1169
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "recurrence"    # Ljava/lang/String;
    .param p1, "more"    # Ljava/lang/String;

    .prologue
    .line 2025
    if-nez p0, :cond_0

    .end local p1    # "more":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "more":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static compareObjects(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 1067
    .local p0, "o1":Ljava/lang/Comparable;, "TT;"
    .local p1, "o2":Ljava/lang/Comparable;, "TT;"
    if-nez p0, :cond_1

    .line 1068
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1070
    :goto_0
    return v0

    .line 1068
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1070
    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private convertValuesToPartialDiff(Landroid/content/Entity;Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    .locals 26
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "oldEntity"    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 879
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v23

    .line 880
    .local v23, "values":Landroid/content/ContentValues;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v20

    .line 882
    .local v20, "oldValues":Landroid/content/ContentValues;
    new-instance v4, Landroid/content/ContentValues;

    move-object/from16 v0, v23

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 883
    .local v4, "diffValues":Landroid/content/ContentValues;
    invoke-virtual/range {v23 .. v23}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 884
    .local v7, "name":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    .line 885
    .local v22, "value":Ljava/lang/Object;
    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    .line 886
    .local v19, "oldValue":Ljava/lang/Object;
    const-string v24, "eventColor_index"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    move-object/from16 v24, v22

    .line 887
    check-cast v24, Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_1

    move-object/from16 v24, v19

    check-cast v24, Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 891
    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 895
    :cond_1
    if-nez v22, :cond_2

    if-eqz v19, :cond_3

    :cond_2
    if-eqz v22, :cond_0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 896
    :cond_3
    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 902
    .end local v7    # "name":Ljava/lang/String;
    .end local v19    # "oldValue":Ljava/lang/Object;
    .end local v22    # "value":Ljava/lang/Object;
    :cond_4
    const-string v24, "rrule"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_8

    const-string v24, "rrule"

    invoke-virtual/range {v23 .. v24}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_8

    .line 904
    const-string v24, "dtstart"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_5

    .line 905
    const-string v24, "dtstart"

    const-string v25, "dtstart"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 907
    :cond_5
    const-string v24, "dtend"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_6

    .line 908
    const-string v24, "dtend"

    const-string v25, "dtend"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 910
    :cond_6
    const-string v24, "eventTimezone"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_7

    .line 911
    const-string v24, "eventTimezone"

    const-string v25, "eventTimezone"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    :cond_7
    const-string v24, "allDay"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_8

    .line 914
    const-string v24, "allDay"

    const-string v25, "allDay"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 917
    :cond_8
    const-string v24, "allDay"

    invoke-virtual/range {v23 .. v24}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 918
    .local v8, "newAllDay":I
    const-string v24, "allDay"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 919
    .local v13, "oldAllDay":I
    const-string v24, "dtstart"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_9

    const-string v24, "dtend"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 920
    :cond_9
    const-string v24, "allDay"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 922
    :cond_a
    const-string v24, "allDay"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 923
    const-string v24, "dtstart"

    const-string v25, "dtstart"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 924
    const-string v24, "dtend"

    const-string v25, "dtend"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 927
    :cond_b
    new-instance v3, Landroid/content/Entity;

    invoke-direct {v3, v4}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    .line 928
    .local v3, "diffEntity":Landroid/content/Entity;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v5

    .line 932
    .local v5, "event":Lcom/google/api/services/calendar/model/Event;
    if-eq v8, v13, :cond_c

    .line 933
    if-nez v8, :cond_14

    .line 934
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    .line 935
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    .line 942
    :cond_c
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v21

    .line 943
    .local v21, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v18

    .line 945
    .local v18, "oldSubValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    new-instance v10, Lcom/google/api/services/calendar/model/Event;

    invoke-direct {v10}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    .line 946
    .local v10, "newEvent":Lcom/google/api/services/calendar/model/Event;
    new-instance v15, Lcom/google/api/services/calendar/model/Event;

    invoke-direct {v15}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    .line 947
    .local v15, "oldEvent":Lcom/google/api/services/calendar/model/Event;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 948
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v15}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 949
    invoke-virtual {v10}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v12

    .line 950
    .local v12, "newReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    invoke-virtual {v15}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v17

    .line 951
    .local v17, "oldReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v12, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameReminders(Lcom/google/api/services/calendar/model/Event$Reminders;Lcom/google/api/services/calendar/model/Event$Reminders;)Z

    move-result v24

    if-nez v24, :cond_e

    .line 952
    if-nez v12, :cond_d

    .line 954
    new-instance v24, Lcom/google/api/services/calendar/model/Event$Reminders;

    invoke-direct/range {v24 .. v24}, Lcom/google/api/services/calendar/model/Event$Reminders;-><init>()V

    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/Event$Reminders;->setUseDefault(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v24

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/Event$Reminders;->setOverrides(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v12

    .line 957
    :cond_d
    invoke-virtual {v5, v12}, Lcom/google/api/services/calendar/model/Event;->setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;

    .line 960
    :cond_e
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 961
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v15}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 962
    invoke-virtual {v10}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v9

    .line 963
    .local v9, "newAttendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    invoke-virtual {v15}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v14

    .line 964
    .local v14, "oldAttendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v14}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameAttendees(Ljava/util/List;Ljava/util/List;)Z

    move-result v24

    if-nez v24, :cond_10

    .line 965
    if-eqz v14, :cond_f

    if-nez v9, :cond_f

    .line 967
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "newAttendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 969
    .restart local v9    # "newAttendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    :cond_f
    invoke-virtual {v5, v9}, Lcom/google/api/services/calendar/model/Event;->setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;

    .line 972
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 973
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v15}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 974
    invoke-virtual {v10}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v11

    .line 975
    .local v11, "newProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    invoke-virtual {v15}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v16

    .line 976
    .local v16, "oldProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v11, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->sameExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Z

    move-result v24

    if-nez v24, :cond_13

    .line 978
    if-eqz v16, :cond_12

    .line 981
    if-nez v11, :cond_11

    .line 982
    new-instance v11, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .end local v11    # "newProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    invoke-direct {v11}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;-><init>()V

    .line 984
    .restart local v11    # "newProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    :cond_11
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v24

    invoke-virtual {v11}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/google/android/syncadapters/calendar/EventHandler;->addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setPrivate(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .line 986
    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v24

    invoke-virtual {v11}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/google/android/syncadapters/calendar/EventHandler;->addDeletedProperties(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->setShared(Ljava/util/Map;)Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .line 989
    :cond_12
    invoke-virtual {v5, v11}, Lcom/google/api/services/calendar/model/Event;->setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;

    .line 991
    :cond_13
    return-object v5

    .line 937
    .end local v9    # "newAttendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    .end local v10    # "newEvent":Lcom/google/api/services/calendar/model/Event;
    .end local v11    # "newProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .end local v12    # "newReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .end local v14    # "oldAttendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    .end local v15    # "oldEvent":Lcom/google/api/services/calendar/model/Event;
    .end local v16    # "oldProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .end local v17    # "oldReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .end local v18    # "oldSubValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    .end local v21    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    :cond_14
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    .line 938
    invoke-virtual {v5}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v24

    sget-object v25, Lcom/google/api/client/util/Data;->NULL_DATE_TIME:Lcom/google/api/client/util/DateTime;

    invoke-virtual/range {v24 .. v25}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    goto/16 :goto_1
.end method

.method private static createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 4
    .param p0, "date"    # J
    .param p2, "allDay"    # Z

    .prologue
    .line 868
    new-instance v0, Lcom/google/api/services/calendar/model/EventDateTime;

    invoke-direct {v0}, Lcom/google/api/services/calendar/model/EventDateTime;-><init>()V

    .line 869
    .local v0, "dateTime":Lcom/google/api/services/calendar/model/EventDateTime;
    if-eqz p2, :cond_0

    .line 870
    new-instance v1, Lcom/google/api/client/util/DateTime;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, p1, v3}, Lcom/google/api/client/util/DateTime;-><init>(ZJLjava/lang/Integer;)V

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/EventDateTime;->setDate(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    .line 874
    :goto_0
    return-object v0

    .line 872
    :cond_0
    new-instance v1, Lcom/google/api/client/util/DateTime;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/google/api/client/util/DateTime;-><init>(JI)V

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/EventDateTime;->setDateTime(Lcom/google/api/client/util/DateTime;)Lcom/google/api/services/calendar/model/EventDateTime;

    goto :goto_0
.end method

.method private fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;
    .locals 2
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 621
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->newEntityIterator(Ljava/lang/String;[Ljava/lang/String;I)Landroid/content/EntityIterator;

    move-result-object v0

    .line 623
    .local v0, "iterator":Landroid/content/EntityIterator;
    :try_start_0
    invoke-interface {v0}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    invoke-interface {v0}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Entity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    :goto_0
    return-object v1

    .line 626
    :cond_0
    const/4 v1, 0x0

    .line 629
    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/content/EntityIterator;->close()V

    throw v1
.end method

.method private getAttendeeSet(Ljava/util/List;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1043
    .local p1, "attendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    if-nez p1, :cond_0

    .line 1044
    const/4 v0, 0x0

    .line 1063
    :goto_0
    return-object v0

    .line 1046
    :cond_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/EventHandler$1;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/EventHandler$1;-><init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newTreeSet(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    .line 1062
    .local v0, "set":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private getCalendarOwnerAccount(J)Ljava/lang/String;
    .locals 7
    .param p1, "calId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1990
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "ownerAccount"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1998
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 2000
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2001
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2004
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2007
    :cond_0
    :goto_0
    return-object v3

    .line 2004
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getRecurrenceDate(Lcom/android/calendarcommon2/ICalendar$Property;)Ljava/lang/String;
    .locals 4
    .param p0, "property"    # Lcom/android/calendarcommon2/ICalendar$Property;

    .prologue
    .line 2015
    const-string v2, "TZID"

    invoke-virtual {p0, v2}, Lcom/android/calendarcommon2/ICalendar$Property;->getFirstParameter(Ljava/lang/String;)Lcom/android/calendarcommon2/ICalendar$Parameter;

    move-result-object v1

    .line 2016
    .local v1, "tzid":Lcom/android/calendarcommon2/ICalendar$Parameter;
    if-eqz v1, :cond_0

    .line 2017
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lcom/android/calendarcommon2/ICalendar$Parameter;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2021
    .local v0, "date":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 2019
    .end local v0    # "date":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "date":Ljava/lang/String;
    goto :goto_0
.end method

.method private getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;
    .locals 2
    .param p1, "reminders"    # Lcom/google/api/services/calendar/model/Event$Reminders;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/calendar/model/Event$Reminders;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/calendar/model/EventReminder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1079
    if-nez p1, :cond_0

    .line 1080
    const/4 v0, 0x0

    .line 1093
    :goto_0
    return-object v0

    .line 1082
    :cond_0
    new-instance v1, Lcom/google/android/syncadapters/calendar/EventHandler$2;

    invoke-direct {v1, p0}, Lcom/google/android/syncadapters/calendar/EventHandler$2;-><init>(Lcom/google/android/syncadapters/calendar/EventHandler;)V

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newTreeSet(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    .line 1092
    .local v0, "set":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/api/services/calendar/model/EventReminder;>;"
    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private static hasOnlyUnsyncedKeys(Lcom/google/api/services/calendar/model/Event;)Z
    .locals 3
    .param p0, "event"    # Lcom/google/api/services/calendar/model/Event;

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/Event;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 586
    .local v1, "key":Ljava/lang/String;
    sget-object v2, Lcom/google/android/syncadapters/calendar/EventHandler;->UNSYNCED_EVENT_KEYS:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 587
    const/4 v2, 0x0

    .line 590
    .end local v1    # "key":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/String;)V
    .locals 14
    .param p2, "eventIdIfUpdate"    # Ljava/lang/Long;
    .param p3, "eventBackRefIfInsert"    # Ljava/lang/Integer;
    .param p5, "scope"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1538
    .local p1, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    .local p4, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p4, :cond_2

    .line 1539
    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 1540
    .local v12, "prop":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1541
    .local v11, "key":Ljava/lang/String;
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1544
    .local v13, "value":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1548
    const-string v1, "alarmReminder"

    invoke-virtual {v11, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1549
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1550
    .local v3, "extendedPropertyValues":Landroid/content/ContentValues;
    const-string v1, "name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string v1, "value"

    invoke-virtual {v3, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    sget-object v1, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v6, 0x0

    move-object v1, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0

    .line 1561
    .end local v3    # "extendedPropertyValues":Landroid/content/ContentValues;
    :cond_1
    sget-boolean v1, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v1, :cond_0

    .line 1565
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1566
    .local v6, "reminderValues":Landroid/content/ContentValues;
    const-string v1, "method"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1567
    const-string v1, "minutes"

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1568
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    const/4 v9, 0x0

    move-object v4, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-static/range {v4 .. v9}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_0

    .line 1579
    .end local v6    # "reminderValues":Landroid/content/ContentValues;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/String;
    .end local v12    # "prop":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "value":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o1"    # Ljava/lang/Object;
    .param p2, "o2"    # Ljava/lang/Object;

    .prologue
    .line 1097
    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private parseDuration(Ljava/lang/String;)J
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 606
    if-nez p1, :cond_0

    .line 615
    :goto_0
    return-wide v2

    .line 609
    :cond_0
    new-instance v0, Lcom/android/calendarcommon2/Duration;

    invoke-direct {v0}, Lcom/android/calendarcommon2/Duration;-><init>()V

    .line 611
    .local v0, "duration":Lcom/android/calendarcommon2/Duration;
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/android/calendarcommon2/Duration;->parse(Ljava/lang/String;)V

    .line 612
    invoke-virtual {v0}, Lcom/android/calendarcommon2/Duration;->getMillis()J
    :try_end_0
    .catch Lcom/android/calendarcommon2/DateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 613
    :catch_0
    move-exception v1

    .line 614
    .local v1, "e":Lcom/android/calendarcommon2/DateException;
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad DURATION: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sameAttendees(Ljava/util/List;Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1039
    .local p1, "attendees1":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    .local p2, "attendees2":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getAttendeeSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getAttendeeSet(Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private sameExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Z
    .locals 4
    .param p1, "properties1"    # Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .param p2, "properties2"    # Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1030
    if-nez p1, :cond_2

    .line 1031
    if-nez p2, :cond_1

    .line 1033
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1031
    goto :goto_0

    .line 1033
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private sameReminders(Lcom/google/api/services/calendar/model/Event$Reminders;Lcom/google/api/services/calendar/model/Event$Reminders;)Z
    .locals 2
    .param p1, "reminders1"    # Lcom/google/api/services/calendar/model/Event$Reminders;
    .param p2, "reminders2"    # Lcom/google/api/services/calendar/model/Event$Reminders;

    .prologue
    .line 1075
    invoke-direct {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getReminderSet(Lcom/google/api/services/calendar/model/Event$Reminders;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->isEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setDtendFromDuration(Landroid/content/ContentValues;J)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "duration"    # J

    .prologue
    .line 595
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-ltz v1, :cond_0

    .line 596
    const-string v1, "dtstart"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 597
    .local v0, "dtstart":Ljava/lang/Long;
    if-nez v0, :cond_1

    .line 598
    const-string v1, "EventHandler"

    const-string v2, "Event has DURATION but no DTSTART"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    .end local v0    # "dtstart":Ljava/lang/Long;
    :cond_0
    :goto_0
    return-void

    .line 600
    .restart local v0    # "dtstart":Ljava/lang/Long;
    :cond_1
    const-string v1, "dtend"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0
.end method

.method private touchDescription(Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;)V
    .locals 2
    .param p1, "event"    # Lcom/google/api/services/calendar/model/Event;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 1017
    if-nez p2, :cond_0

    .line 1018
    const-string p2, " "

    .line 1024
    :goto_0
    invoke-virtual {p1, p2}, Lcom/google/api/services/calendar/model/Event;->setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 1025
    return-void

    .line 1019
    :cond_0
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1020
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 1022
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method public applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V
    .locals 47
    .param p2, "event"    # Lcom/google/api/services/calendar/model/Event;
    .param p3, "entity"    # Landroid/content/Entity;
    .param p4, "clearDirtyFlag"    # Z
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .param p6, "extra"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/google/api/services/calendar/model/Event;",
            "Landroid/content/Entity;",
            "Z",
            "Landroid/content/SyncResult;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 1278
    .local p1, "operations":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    const-string v2, "EventHandler"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1279
    const-string v2, "EventHandler"

    const-string v3, "============= applyItemToEntity ============="

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "event is "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1281
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "entity is "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    :cond_0
    if-nez p3, :cond_3

    const/16 v37, 0x1

    .line 1286
    .local v37, "isInsert":Z
    :goto_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .local v4, "eventValuesServer":Landroid/content/ContentValues;
    move-object/from16 v5, p6

    .line 1287
    check-cast v5, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;

    .line 1293
    .local v5, "syncInfo":Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    if-eqz p3, :cond_4

    .line 1294
    invoke-virtual/range {p3 .. p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "calendar_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1300
    .local v6, "calendarId":J
    :goto_1
    if-nez p2, :cond_6

    const/16 v29, 0x1

    .line 1303
    .local v29, "entryState":I
    :goto_2
    if-eqz p4, :cond_1

    .line 1304
    const-string v2, "dirty"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1306
    :cond_1
    const/4 v2, 0x1

    move/from16 v0, v29

    if-ne v0, v2, :cond_7

    .line 1307
    if-eqz p3, :cond_2

    .line 1308
    invoke-virtual/range {p3 .. p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v31

    .line 1310
    .local v31, "eventId":Ljava/lang/Long;
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v2, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    .line 1316
    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v2, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v16, 0x1

    add-long v8, v8, v16

    iput-wide v8, v2, Landroid/content/SyncStats;->numDeletes:J

    .line 1533
    .end local v31    # "eventId":Ljava/lang/Long;
    :cond_2
    :goto_3
    return-void

    .line 1284
    .end local v4    # "eventValuesServer":Landroid/content/ContentValues;
    .end local v5    # "syncInfo":Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .end local v6    # "calendarId":J
    .end local v29    # "entryState":I
    .end local v37    # "isInsert":Z
    :cond_3
    const/16 v37, 0x0

    goto :goto_0

    .line 1295
    .restart local v4    # "eventValuesServer":Landroid/content/ContentValues;
    .restart local v5    # "syncInfo":Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .restart local v37    # "isInsert":Z
    :cond_4
    if-eqz v5, :cond_5

    .line 1296
    iget-wide v6, v5, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    .restart local v6    # "calendarId":J
    goto :goto_1

    .line 1298
    .end local v6    # "calendarId":J
    :cond_5
    const-wide/16 v6, -0x1

    .restart local v6    # "calendarId":J
    goto :goto_1

    :cond_6
    move-object/from16 v2, p0

    move-object/from16 v3, p2

    .line 1300
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->entryToContentValues(Lcom/google/api/services/calendar/model/Event;Landroid/content/ContentValues;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;J)I

    move-result v29

    goto :goto_2

    .line 1318
    .restart local v29    # "entryState":I
    :cond_7
    if-nez v29, :cond_2

    .line 1319
    const-string v2, "EventHandler"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1320
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Got eventEntry from server: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    :cond_8
    if-eqz v37, :cond_9

    .line 1326
    sget-object v2, Landroid/provider/CalendarContract$EventsEntity;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newAssertQuery(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "_sync_id=?"

    aput-object v9, v3, v8

    const/4 v8, 0x1

    const-string v9, "calendar_id=?"

    aput-object v9, v3, v8

    invoke-static {v3}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v13, "_sync_id"

    invoke-virtual {v4, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v8, v9

    const/4 v9, 0x1

    const-string v13, "calendar_id"

    invoke-virtual {v4, v13}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v8, v9

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1338
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    .line 1339
    .local v30, "eventBackRefIfInsert":Ljava/lang/Integer;
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p1

    move-object v10, v4

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 1344
    const/4 v11, 0x0

    .line 1345
    .local v11, "eventIdIfUpdate":Ljava/lang/Long;
    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v2, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v16, 0x1

    add-long v8, v8, v16

    iput-wide v8, v2, Landroid/content/SyncStats;->numInserts:J

    move-object/from16 v12, v30

    .line 1381
    .end local v30    # "eventBackRefIfInsert":Ljava/lang/Integer;
    .local v12, "eventBackRefIfInsert":Ljava/lang/Integer;
    :goto_4
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "hasAlarm"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v34

    .line 1383
    .local v34, "hasAlarm":Z
    if-eqz v34, :cond_11

    .line 1385
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v32

    .line 1386
    .local v32, "eventReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    if-eqz v32, :cond_11

    .line 1387
    invoke-virtual/range {v32 .. v32}, Lcom/google/api/services/calendar/model/Event$Reminders;->getUseDefault()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_d

    if-nez v5, :cond_c

    const/16 v42, 0x0

    .line 1390
    .local v42, "reminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    :goto_5
    if-eqz v42, :cond_11

    .line 1391
    invoke-interface/range {v42 .. v42}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    .local v36, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/google/api/services/calendar/model/EventReminder;

    .line 1392
    .local v41, "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 1394
    .local v10, "reminderValues":Landroid/content/ContentValues;
    invoke-virtual/range {v41 .. v41}, Lcom/google/api/services/calendar/model/EventReminder;->getMethod()Ljava/lang/String;

    move-result-object v38

    .line 1395
    .local v38, "method":Ljava/lang/String;
    const-string v2, "popup"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1396
    const-string v2, "method"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1413
    :goto_7
    invoke-virtual/range {v41 .. v41}, Lcom/google/api/services/calendar/model/EventReminder;->getMinutes()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v39

    .line 1414
    .local v39, "minutes":I
    const-string v2, "minutes"

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1417
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v13, 0x0

    move-object/from16 v8, p1

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_6

    .line 1347
    .end local v10    # "reminderValues":Landroid/content/ContentValues;
    .end local v11    # "eventIdIfUpdate":Ljava/lang/Long;
    .end local v12    # "eventBackRefIfInsert":Ljava/lang/Integer;
    .end local v32    # "eventReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .end local v34    # "hasAlarm":Z
    .end local v36    # "i$":Ljava/util/Iterator;
    .end local v38    # "method":Ljava/lang/String;
    .end local v39    # "minutes":I
    .end local v41    # "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    .end local v42    # "reminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    :cond_9
    const/16 v30, 0x0

    .line 1348
    .restart local v30    # "eventBackRefIfInsert":Ljava/lang/Integer;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    .line 1349
    .restart local v11    # "eventIdIfUpdate":Ljava/lang/Long;
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v8, p1

    move-object v10, v4

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 1356
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v11, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    .line 1360
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "hasAlarm"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    if-eqz v5, :cond_b

    .line 1367
    :cond_a
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v11, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    .line 1372
    :cond_b
    sget-object v2, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v11, v3}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V

    .line 1377
    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v2, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v16, 0x1

    add-long v8, v8, v16

    iput-wide v8, v2, Landroid/content/SyncStats;->numUpdates:J

    move-object/from16 v12, v30

    .end local v30    # "eventBackRefIfInsert":Ljava/lang/Integer;
    .restart local v12    # "eventBackRefIfInsert":Ljava/lang/Integer;
    goto/16 :goto_4

    .line 1387
    .restart local v32    # "eventReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .restart local v34    # "hasAlarm":Z
    :cond_c
    iget-object v0, v5, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    move-object/from16 v42, v0

    goto/16 :goto_5

    :cond_d
    invoke-virtual/range {v32 .. v32}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v42

    goto/16 :goto_5

    .line 1398
    .restart local v10    # "reminderValues":Landroid/content/ContentValues;
    .restart local v36    # "i$":Ljava/util/Iterator;
    .restart local v38    # "method":Ljava/lang/String;
    .restart local v41    # "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    .restart local v42    # "reminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    :cond_e
    const-string v2, "email"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1399
    const-string v2, "method"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_7

    .line 1401
    :cond_f
    const-string v2, "sms"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1402
    const-string v2, "method"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_7

    .line 1408
    :cond_10
    const-string v2, "method"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1409
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown reminder method: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "should not happen!"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 1431
    .end local v10    # "reminderValues":Landroid/content/ContentValues;
    .end local v32    # "eventReminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .end local v36    # "i$":Ljava/util/Iterator;
    .end local v38    # "method":Ljava/lang/String;
    .end local v41    # "reminder":Lcom/google/api/services/calendar/model/EventReminder;
    .end local v42    # "reminders":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    :cond_11
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/services/calendar/model/Event;->getAttendees()Ljava/util/List;

    move-result-object v25

    .line 1432
    .local v25, "attendees":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventAttendee;>;"
    if-eqz v25, :cond_1d

    .line 1433
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    .restart local v36    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/api/services/calendar/model/EventAttendee;

    .line 1434
    .local v22, "attendee":Lcom/google/api/services/calendar/model/EventAttendee;
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 1435
    .local v26, "attendeesValues":Landroid/content/ContentValues;
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/model/EventAttendee;->getDisplayName()Ljava/lang/String;

    move-result-object v27

    .line 1436
    .local v27, "displayName":Ljava/lang/String;
    if-nez v27, :cond_12

    .line 1437
    const-string v27, ""

    .line 1439
    :cond_12
    const-string v2, "attendeeName"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/model/EventAttendee;->getEmail()Ljava/lang/String;

    move-result-object v28

    .line 1441
    .local v28, "email":Ljava/lang/String;
    if-eqz v28, :cond_13

    .line 1442
    const-string v2, "attendeeEmail"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    :cond_13
    const-string v2, "id"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/google/api/services/calendar/model/EventAttendee;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 1445
    .local v24, "attendeeId":Ljava/lang/String;
    if-eqz v24, :cond_16

    .line 1446
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/model/EventAttendee;->getSelf()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v44

    .line 1447
    .local v44, "self":Z
    sget-boolean v2, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v2, :cond_14

    .line 1448
    const-string v2, "attendeeIdNamespace"

    const-string v3, "com.google"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    const-string v2, "attendeeIdentity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "gprofile:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    :cond_14
    if-eqz v44, :cond_15

    .line 1460
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1461
    .local v15, "values":Landroid/content/ContentValues;
    const-string v2, "sync_data3"

    move-object/from16 v0, v24

    invoke-virtual {v15, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v14

    const/16 v18, 0x0

    move-object/from16 v13, p1

    move-object/from16 v16, v11

    move-object/from16 v17, v12

    invoke-static/range {v13 .. v18}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 1472
    .end local v15    # "values":Landroid/content/ContentValues;
    :cond_15
    if-eqz v44, :cond_19

    .line 1473
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-gez v2, :cond_18

    .line 1475
    const-string v2, "EventHandler"

    const-string v3, "Missing calendarId, can\'t get owner"

    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1476
    const/16 v23, 0x0

    .line 1484
    .local v23, "attendeeEmail":Ljava/lang/String;
    :goto_9
    if-nez v23, :cond_1a

    .line 1485
    const-string v2, "attendeeEmail"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "@profile.calendar.google.com"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    .end local v23    # "attendeeEmail":Ljava/lang/String;
    .end local v44    # "self":Z
    :cond_16
    :goto_a
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/model/EventAttendee;->getResponseStatus()Ljava/lang/String;

    move-result-object v43

    .line 1493
    .local v43, "responseStatus":Ljava/lang/String;
    sget-object v2, Lcom/google/android/syncadapters/calendar/EventHandler;->ENTRY_TYPE_TO_PROVIDER_ATTENDEE:Ljava/util/HashMap;

    move-object/from16 v0, v43

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Ljava/lang/Integer;

    .line 1495
    .local v45, "status":Ljava/lang/Integer;
    if-nez v45, :cond_17

    .line 1496
    const-string v2, "EventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown attendee status "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v43

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    .line 1499
    :cond_17
    const-string v2, "attendeeStatus"

    move-object/from16 v0, v26

    move-object/from16 v1, v45

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1501
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/model/EventAttendee;->getOrganizer()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v40, 0x2

    .line 1503
    .local v40, "rel":I
    :goto_b
    const-string v2, "attendeeRelationship"

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1505
    invoke-virtual/range {v22 .. v22}, Lcom/google/api/services/calendar/model/EventAttendee;->getOptional()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_1c

    const/16 v46, 0x2

    .line 1507
    .local v46, "type":I
    :goto_c
    const-string v2, "attendeeType"

    invoke-static/range {v46 .. v46}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1509
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v17

    const/16 v21, 0x0

    move-object/from16 v16, p1

    move-object/from16 v18, v26

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    invoke-static/range {v16 .. v21}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addInsertOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    goto/16 :goto_8

    .line 1478
    .end local v40    # "rel":I
    .end local v43    # "responseStatus":Ljava/lang/String;
    .end local v45    # "status":Ljava/lang/Integer;
    .end local v46    # "type":I
    .restart local v44    # "self":Z
    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/syncadapters/calendar/EventHandler;->getCalendarOwnerAccount(J)Ljava/lang/String;

    move-result-object v23

    .restart local v23    # "attendeeEmail":Ljava/lang/String;
    goto/16 :goto_9

    .line 1482
    .end local v23    # "attendeeEmail":Ljava/lang/String;
    :cond_19
    const/16 v23, 0x0

    .restart local v23    # "attendeeEmail":Ljava/lang/String;
    goto/16 :goto_9

    .line 1488
    :cond_1a
    const-string v2, "attendeeEmail"

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1501
    .end local v23    # "attendeeEmail":Ljava/lang/String;
    .end local v44    # "self":Z
    .restart local v43    # "responseStatus":Ljava/lang/String;
    .restart local v45    # "status":Ljava/lang/Integer;
    :cond_1b
    const/16 v40, 0x1

    goto :goto_b

    .line 1505
    .restart local v40    # "rel":I
    :cond_1c
    const/16 v46, 0x1

    goto :goto_c

    .line 1521
    .end local v22    # "attendee":Lcom/google/api/services/calendar/model/EventAttendee;
    .end local v24    # "attendeeId":Ljava/lang/String;
    .end local v26    # "attendeesValues":Landroid/content/ContentValues;
    .end local v27    # "displayName":Ljava/lang/String;
    .end local v28    # "email":Ljava/lang/String;
    .end local v36    # "i$":Ljava/util/Iterator;
    .end local v40    # "rel":I
    .end local v43    # "responseStatus":Ljava/lang/String;
    .end local v45    # "status":Ljava/lang/Integer;
    :cond_1d
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "hasExtendedProperties"

    invoke-virtual {v4, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v35

    .line 1523
    .local v35, "hasExtendedProperties":Z
    if-nez v35, :cond_1e

    if-eqz v34, :cond_2

    .line 1524
    :cond_1e
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v33

    .line 1525
    .local v33, "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    if-eqz v33, :cond_2

    .line 1526
    invoke-virtual/range {v33 .. v33}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v20

    const-string v21, "shared:"

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v18, v11

    move-object/from16 v19, v12

    invoke-direct/range {v16 .. v21}, Lcom/google/android/syncadapters/calendar/EventHandler;->insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/String;)V

    .line 1528
    invoke-virtual/range {v33 .. v33}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v20

    const-string v21, "private:"

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v18, v11

    move-object/from16 v19, v12

    invoke-direct/range {v16 .. v21}, Lcom/google/android/syncadapters/calendar/EventHandler;->insertExtendedProperties(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    .locals 40
    .param p1, "entity"    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 634
    new-instance v18, Lcom/google/api/services/calendar/model/Event;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/calendar/model/Event;-><init>()V

    .line 635
    .local v18, "event":Lcom/google/api/services/calendar/model/Event;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v38

    .line 636
    .local v38, "values":Landroid/content/ContentValues;
    const-string v4, "_sync_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 637
    const-string v4, "_sync_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/syncadapters/calendar/Utils;->extractEventId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 640
    :cond_0
    const-string v4, "eventStatus"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 643
    const-string v4, "eventStatus"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    .line 644
    .local v24, "localStatus":Ljava/lang/Integer;
    if-nez v24, :cond_1

    .line 645
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    .line 647
    :cond_1
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 659
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected value for status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using tentative."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const-string v34, "tentative"

    .line 664
    .local v34, "status":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 667
    .end local v24    # "localStatus":Ljava/lang/Integer;
    .end local v34    # "status":Ljava/lang/String;
    :cond_2
    const-string v4, "accessLevel"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 670
    const-string v4, "accessLevel"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    .line 671
    .local v26, "localVisibility":Ljava/lang/Integer;
    if-nez v26, :cond_3

    .line 672
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    .line 674
    :cond_3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 689
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected value for visibility: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using default visibility."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    const-string v39, "default"

    .line 694
    .local v39, "visibility":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, v18

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setVisibility(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 697
    .end local v26    # "localVisibility":Ljava/lang/Integer;
    .end local v39    # "visibility":Ljava/lang/String;
    :cond_4
    const-string v4, "availability"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 699
    const-string v4, "availability"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v25

    .line 700
    .local v25, "localTransparency":Ljava/lang/Integer;
    if-nez v25, :cond_5

    .line 701
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    .line 703
    :cond_5
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    .line 712
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected value for transparency: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using opaque transparency."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    const-string v37, "opaque"

    .line 717
    .local v37, "transparency":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, v18

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setTransparency(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 721
    .end local v25    # "localTransparency":Ljava/lang/Integer;
    .end local v37    # "transparency":Ljava/lang/String;
    :cond_6
    const-string v4, "title"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 722
    const-string v4, "title"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 726
    :cond_7
    const-string v4, "description"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 727
    const-string v4, "description"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 731
    :cond_8
    const-string v4, "eventLocation"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 732
    const-string v4, "eventLocation"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setLocation(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 735
    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v35

    .line 738
    .local v35, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, v35

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/syncadapters/calendar/EventHandler;->addAttendeesToEntry(Landroid/content/ContentValues;Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 741
    new-instance v22, Lcom/android/calendarcommon2/ICalendar$Component;

    const-string v4, "DUMMY"

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v5}, Lcom/android/calendarcommon2/ICalendar$Component;-><init>(Ljava/lang/String;Lcom/android/calendarcommon2/ICalendar$Component;)V

    .line 743
    .local v22, "iCal":Lcom/android/calendarcommon2/ICalendar$Component;
    const-string v4, "RRULE"

    const-string v5, "rrule"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertiesForRuleStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    const-string v4, "RDATE"

    const-string v5, "rdate"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertyForDateStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    const-string v4, "EXRULE"

    const-string v5, "exrule"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertiesForRuleStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    const-string v4, "EXDATE"

    const-string v5, "exdate"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-static {v0, v4, v5}, Lcom/android/calendarcommon2/RecurrenceSet;->addPropertyForDateStr(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    invoke-virtual/range {v22 .. v22}, Lcom/android/calendarcommon2/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_1a

    .line 748
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRecurrenceToEntry(Lcom/android/calendarcommon2/ICalendar$Component;Lcom/google/api/services/calendar/model/Event;)V

    .line 749
    const/16 v23, 0x1

    .line 754
    .local v23, "isRecurring":Z
    :goto_3
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "allDay"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 756
    .local v10, "allDay":Z
    const-string v4, "eventTimezone"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 758
    .local v36, "timezone":Ljava/lang/String;
    invoke-static/range {v36 .. v36}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 759
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v36

    .line 760
    const/16 v21, 0x0

    .line 765
    .local v21, "hasTimezone":Z
    :goto_4
    const-string v4, "dtstart"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 766
    const-string v4, "dtstart"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 767
    .local v16, "dtstart":J
    move-wide/from16 v0, v16

    invoke-static {v0, v1, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v13

    .line 768
    .local v13, "dateTime":Lcom/google/api/services/calendar/model/EventDateTime;
    if-nez v23, :cond_a

    if-eqz v21, :cond_b

    .line 770
    :cond_a
    move-object/from16 v0, v36

    invoke-virtual {v13, v0}, Lcom/google/api/services/calendar/model/EventDateTime;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    .line 772
    :cond_b
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/google/api/services/calendar/model/Event;->setStart(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    .line 776
    .end local v13    # "dateTime":Lcom/google/api/services/calendar/model/EventDateTime;
    .end local v16    # "dtstart":J
    :cond_c
    const-string v4, "dtend"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 777
    const-string v4, "dtend"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 778
    .local v14, "dtend":J
    invoke-static {v14, v15, v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v13

    .line 779
    .restart local v13    # "dateTime":Lcom/google/api/services/calendar/model/EventDateTime;
    if-nez v23, :cond_d

    if-eqz v21, :cond_e

    .line 781
    :cond_d
    move-object/from16 v0, v36

    invoke-virtual {v13, v0}, Lcom/google/api/services/calendar/model/EventDateTime;->setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/EventDateTime;

    .line 783
    :cond_e
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/google/api/services/calendar/model/Event;->setEnd(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    .line 787
    .end local v13    # "dateTime":Lcom/google/api/services/calendar/model/EventDateTime;
    .end local v14    # "dtend":J
    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addRemindersToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 790
    const-string v4, "hasExtendedProperties"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    .line 791
    .local v20, "hasExtendedProperties":Ljava/lang/Integer;
    if-eqz v20, :cond_10

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_10

    .line 792
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->addExtendedPropertiesToEntry(Ljava/util/ArrayList;Lcom/google/api/services/calendar/model/Event;)V

    .line 795
    :cond_10
    const-wide/16 v30, -0x1

    .line 796
    .local v30, "originalStartTime":J
    const-string v4, "original_sync_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 797
    .local v29, "originalId":Ljava/lang/String;
    const-string v4, "originalInstanceTime"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 798
    const-string v4, "originalInstanceTime"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    .line 800
    :cond_11
    const-wide/16 v4, -0x1

    cmp-long v4, v30, v4

    if-eqz v4, :cond_14

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_14

    .line 803
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "originalAllDay"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v28

    .line 805
    .local v28, "origAllDay":Z
    new-instance v32, Landroid/text/format/Time;

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 806
    .local v32, "originalTime":Landroid/text/format/Time;
    move-object/from16 v0, v32

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 808
    move-wide/from16 v0, v30

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->createEventDateTime(JZ)Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setOriginalStartTime(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;

    .line 809
    invoke-static/range {v29 .. v29}, Lcom/google/android/syncadapters/calendar/Utils;->extractEventId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setRecurringEventId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 810
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "sync_data2"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "hasAttendeeData"

    aput-object v8, v6, v7

    const-string v7, "_sync_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v29, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 819
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_14

    .line 821
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 822
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 823
    .local v33, "sequence":I
    if-eqz v33, :cond_12

    .line 824
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setSequence(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/Event;

    .line 826
    :cond_12
    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 827
    .local v19, "hasAttendeeData":I
    if-nez v19, :cond_13

    .line 828
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 832
    .end local v19    # "hasAttendeeData":I
    .end local v33    # "sequence":I
    :cond_13
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 837
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v28    # "origAllDay":Z
    .end local v32    # "originalTime":Landroid/text/format/Time;
    :cond_14
    const-string v4, "guestsCanInviteOthers"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 838
    const-string v4, "guestsCanInviteOthers"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_1c

    const/4 v4, 0x1

    :goto_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanInviteOthers(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    .line 841
    :cond_15
    const-string v4, "guestsCanModify"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 842
    const-string v4, "guestsCanModify"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_1d

    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanModify(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    .line 844
    :cond_16
    const-string v4, "guestsCanSeeGuests"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 845
    const-string v4, "guestsCanSeeGuests"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_1e

    const/4 v4, 0x1

    :goto_7
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setGuestsCanSeeOtherGuests(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    .line 849
    :cond_17
    const-string v4, "organizer"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 850
    new-instance v27, Lcom/google/api/services/calendar/model/Event$Organizer;

    invoke-direct/range {v27 .. v27}, Lcom/google/api/services/calendar/model/Event$Organizer;-><init>()V

    .line 851
    .local v27, "organizer":Lcom/google/api/services/calendar/model/Event$Organizer;
    const-string v4, "organizer"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event$Organizer;->setEmail(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event$Organizer;

    .line 852
    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event;->setOrganizer(Lcom/google/api/services/calendar/model/Event$Organizer;)Lcom/google/api/services/calendar/model/Event;

    .line 855
    .end local v27    # "organizer":Lcom/google/api/services/calendar/model/Event$Organizer;
    :cond_18
    const-string v4, "eventColor_index"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 856
    const-string v4, "eventColor_index"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 857
    .local v11, "colorId":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 858
    sget-object v4, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setColorId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    .line 864
    .end local v11    # "colorId":Ljava/lang/String;
    :cond_19
    :goto_8
    return-object v18

    .line 649
    .end local v10    # "allDay":Z
    .end local v20    # "hasExtendedProperties":Ljava/lang/Integer;
    .end local v21    # "hasTimezone":Z
    .end local v22    # "iCal":Lcom/android/calendarcommon2/ICalendar$Component;
    .end local v23    # "isRecurring":Z
    .end local v29    # "originalId":Ljava/lang/String;
    .end local v30    # "originalStartTime":J
    .end local v35    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    .end local v36    # "timezone":Ljava/lang/String;
    .restart local v24    # "localStatus":Ljava/lang/Integer;
    :pswitch_0
    const-string v34, "cancelled"

    .line 650
    .restart local v34    # "status":Ljava/lang/String;
    goto/16 :goto_0

    .line 652
    .end local v34    # "status":Ljava/lang/String;
    :pswitch_1
    const-string v34, "confirmed"

    .line 653
    .restart local v34    # "status":Ljava/lang/String;
    goto/16 :goto_0

    .line 655
    .end local v34    # "status":Ljava/lang/String;
    :pswitch_2
    const-string v34, "tentative"

    .line 656
    .restart local v34    # "status":Ljava/lang/String;
    goto/16 :goto_0

    .line 676
    .end local v24    # "localStatus":Ljava/lang/Integer;
    .end local v34    # "status":Ljava/lang/String;
    .restart local v26    # "localVisibility":Ljava/lang/Integer;
    :pswitch_3
    const-string v39, "default"

    .line 677
    .restart local v39    # "visibility":Ljava/lang/String;
    goto/16 :goto_1

    .line 679
    .end local v39    # "visibility":Ljava/lang/String;
    :pswitch_4
    const-string v39, "confidential"

    .line 680
    .restart local v39    # "visibility":Ljava/lang/String;
    goto/16 :goto_1

    .line 682
    .end local v39    # "visibility":Ljava/lang/String;
    :pswitch_5
    const-string v39, "private"

    .line 683
    .restart local v39    # "visibility":Ljava/lang/String;
    goto/16 :goto_1

    .line 685
    .end local v39    # "visibility":Ljava/lang/String;
    :pswitch_6
    const-string v39, "public"

    .line 686
    .restart local v39    # "visibility":Ljava/lang/String;
    goto/16 :goto_1

    .line 705
    .end local v26    # "localVisibility":Ljava/lang/Integer;
    .end local v39    # "visibility":Ljava/lang/String;
    .restart local v25    # "localTransparency":Ljava/lang/Integer;
    :pswitch_7
    const-string v37, "opaque"

    .line 706
    .restart local v37    # "transparency":Ljava/lang/String;
    goto/16 :goto_2

    .line 708
    .end local v37    # "transparency":Ljava/lang/String;
    :pswitch_8
    const-string v37, "transparent"

    .line 709
    .restart local v37    # "transparency":Ljava/lang/String;
    goto/16 :goto_2

    .line 751
    .end local v25    # "localTransparency":Ljava/lang/Integer;
    .end local v37    # "transparency":Ljava/lang/String;
    .restart local v22    # "iCal":Lcom/android/calendarcommon2/ICalendar$Component;
    .restart local v35    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    :cond_1a
    const/16 v23, 0x0

    .restart local v23    # "isRecurring":Z
    goto/16 :goto_3

    .line 762
    .restart local v10    # "allDay":Z
    .restart local v36    # "timezone":Ljava/lang/String;
    :cond_1b
    const/16 v21, 0x1

    .restart local v21    # "hasTimezone":Z
    goto/16 :goto_4

    .line 832
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v20    # "hasExtendedProperties":Ljava/lang/Integer;
    .restart local v28    # "origAllDay":Z
    .restart local v29    # "originalId":Ljava/lang/String;
    .restart local v30    # "originalStartTime":J
    .restart local v32    # "originalTime":Landroid/text/format/Time;
    :catchall_0
    move-exception v4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v4

    .line 838
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v28    # "origAllDay":Z
    .end local v32    # "originalTime":Landroid/text/format/Time;
    :cond_1c
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 842
    :cond_1d
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 845
    :cond_1e
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 860
    .restart local v11    # "colorId":Ljava/lang/String;
    :cond_1f
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/google/api/services/calendar/model/Event;->setColorId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;

    goto :goto_8

    .line 647
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 674
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 703
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected entryToContentValues(Lcom/google/api/services/calendar/model/Event;Landroid/content/ContentValues;Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;J)I
    .locals 70
    .param p1, "event"    # Lcom/google/api/services/calendar/model/Event;
    .param p2, "map"    # Landroid/content/ContentValues;
    .param p3, "syncInfo"    # Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;
    .param p4, "calendarId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 1629
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->clear()V

    .line 1632
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v65, v0

    invoke-static/range {v64 .. v65}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 1633
    .local v27, "id":Ljava/lang/String;
    const-string v64, "sync_data4"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEtag()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1634
    const-string v64, "_sync_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1635
    const-string v64, "sync_data1"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getICalUID()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1636
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getSequence()Ljava/lang/Integer;

    move-result-object v51

    .line 1637
    .local v51, "sequence":Ljava/lang/Integer;
    if-eqz v51, :cond_0

    .line 1638
    const-string v64, "sync_data2"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v51

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1640
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getStatus()Ljava/lang/String;

    move-result-object v55

    .line 1643
    .local v55, "status":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getRecurringEventId()Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v65, v0

    invoke-static/range {v64 .. v65}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 1645
    .local v46, "recurringEventId":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getOriginalStartTime()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v37

    .line 1646
    .local v37, "originalStartTime":Lcom/google/api/services/calendar/model/EventDateTime;
    const/16 v28, 0x0

    .line 1647
    .local v28, "isRecurrenceException":Z
    invoke-static/range {v46 .. v46}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v64

    if-nez v64, :cond_1

    if-eqz v37, :cond_1

    .line 1650
    const/16 v28, 0x1

    .line 1651
    invoke-virtual/range {v37 .. v37}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Lcom/google/api/client/util/DateTime;

    move-result-object v9

    .line 1652
    .local v9, "date":Lcom/google/api/client/util/DateTime;
    if-eqz v9, :cond_5

    .line 1653
    invoke-virtual {v9}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v38

    .line 1654
    .local v38, "originalTime":J
    const/16 v36, 0x1

    .line 1660
    .local v36, "originalAllDay":I
    :goto_0
    const-string v64, "original_sync_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1662
    const-string v64, "originalInstanceTime"

    const-wide/16 v66, 0x3e8

    div-long v66, v38, v66

    const-wide/16 v68, 0x3e8

    mul-long v66, v66, v68

    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1663
    const-string v64, "originalAllDay"

    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1667
    sget-boolean v64, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-nez v64, :cond_1

    .line 1668
    const-string v64, "cancelled"

    move-object/from16 v0, v64

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_1

    .line 1669
    const-string v64, "dtstart"

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1670
    const-string v64, "dtend"

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1671
    const-string v64, "eventTimezone"

    const-string v65, "UTC"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678
    .end local v9    # "date":Lcom/google/api/client/util/DateTime;
    .end local v36    # "originalAllDay":I
    .end local v38    # "originalTime":J
    :cond_1
    if-eqz v55, :cond_2

    const-string v64, "confirmed"

    move-object/from16 v0, v64

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_6

    .line 1679
    :cond_2
    const/4 v15, 0x1

    .line 1691
    .local v15, "eventStatus":I
    :goto_1
    const-string v64, "eventStatus"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1694
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getUpdated()Lcom/google/api/client/util/DateTime;

    move-result-object v62

    .line 1696
    .local v62, "updated":Lcom/google/api/client/util/DateTime;
    if-eqz v62, :cond_3

    .line 1697
    new-instance v58, Landroid/text/format/Time;

    const-string v64, "UTC"

    move-object/from16 v0, v58

    move-object/from16 v1, v64

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1698
    .local v58, "time":Landroid/text/format/Time;
    invoke-virtual/range {v62 .. v62}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v64

    move-object/from16 v0, v58

    move-wide/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 1699
    const-string v64, "sync_data5"

    const/16 v65, 0x0

    move-object/from16 v0, v58

    move/from16 v1, v65

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    .end local v58    # "time":Landroid/text/format/Time;
    :cond_3
    if-eqz p3, :cond_4

    .line 1705
    const-string v64, "calendar_id"

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarId:J

    move-wide/from16 v66, v0

    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1708
    :cond_4
    if-eqz v28, :cond_a

    const/16 v64, 0x2

    move/from16 v0, v64

    if-ne v15, v0, :cond_a

    .line 1710
    const/16 v64, 0x0

    .line 1985
    .end local v15    # "eventStatus":I
    .end local v62    # "updated":Lcom/google/api/client/util/DateTime;
    :goto_2
    return v64

    .line 1656
    .restart local v9    # "date":Lcom/google/api/client/util/DateTime;
    :cond_5
    invoke-virtual/range {v37 .. v37}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v10

    .line 1657
    .local v10, "dateTime":Lcom/google/api/client/util/DateTime;
    invoke-virtual {v10}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v38

    .line 1658
    .restart local v38    # "originalTime":J
    const/16 v36, 0x0

    .restart local v36    # "originalAllDay":I
    goto/16 :goto_0

    .line 1680
    .end local v9    # "date":Lcom/google/api/client/util/DateTime;
    .end local v10    # "dateTime":Lcom/google/api/client/util/DateTime;
    .end local v36    # "originalAllDay":I
    .end local v38    # "originalTime":J
    :cond_6
    const-string v64, "cancelled"

    move-object/from16 v0, v64

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_8

    .line 1681
    if-nez v28, :cond_7

    .line 1682
    const/16 v64, 0x1

    goto :goto_2

    .line 1684
    :cond_7
    const/4 v15, 0x2

    .restart local v15    # "eventStatus":I
    goto :goto_1

    .line 1685
    .end local v15    # "eventStatus":I
    :cond_8
    const-string v64, "tentative"

    move-object/from16 v0, v64

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_9

    .line 1686
    const/4 v15, 0x0

    .restart local v15    # "eventStatus":I
    goto/16 :goto_1

    .line 1688
    .end local v15    # "eventStatus":I
    :cond_9
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Invalid status: "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1689
    const/16 v64, 0x2

    goto :goto_2

    .line 1713
    .restart local v15    # "eventStatus":I
    .restart local v62    # "updated":Lcom/google/api/client/util/DateTime;
    :cond_a
    const-string v64, "dirty"

    const/16 v65, 0x0

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1716
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getStart()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v53

    .line 1726
    .local v53, "start":Lcom/google/api/services/calendar/model/EventDateTime;
    invoke-virtual/range {v53 .. v53}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Lcom/google/api/client/util/DateTime;

    move-result-object v54

    .line 1730
    .local v54, "startDate":Lcom/google/api/client/util/DateTime;
    if-eqz v54, :cond_16

    .line 1731
    const-string v64, "eventTimezone"

    const-string v65, "UTC"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1732
    const/4 v6, 0x1

    .line 1733
    .local v6, "allDay":I
    invoke-virtual/range {v54 .. v54}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v56

    .line 1734
    .local v56, "startMillis":J
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Lcom/google/api/services/calendar/model/EventDateTime;->getDate()Lcom/google/api/client/util/DateTime;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v16

    .line 1749
    .local v16, "endMillis":J
    :goto_3
    const-string v64, "allDay"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1753
    const-string v64, "dtstart"

    const-wide/16 v66, 0x3e8

    div-long v66, v56, v66

    const-wide/16 v68, 0x3e8

    mul-long v66, v66, v68

    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1756
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getVisibility()Ljava/lang/String;

    move-result-object v63

    .line 1757
    .local v63, "visibility":Ljava/lang/String;
    if-eqz v63, :cond_b

    const-string v64, "default"

    invoke-virtual/range {v63 .. v64}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_19

    .line 1758
    :cond_b
    const-string v64, "accessLevel"

    const/16 v65, 0x0

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1775
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getTransparency()Ljava/lang/String;

    move-result-object v61

    .line 1776
    .local v61, "transparency":Ljava/lang/String;
    if-eqz v61, :cond_c

    const-string v64, "opaque"

    move-object/from16 v0, v61

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_1d

    .line 1777
    :cond_c
    const-string v64, "availability"

    const/16 v65, 0x0

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1785
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getSummary()Ljava/lang/String;

    move-result-object v60

    .line 1786
    .local v60, "title":Ljava/lang/String;
    if-nez v60, :cond_d

    .line 1787
    const-string v60, ""

    .line 1789
    :cond_d
    const-string v64, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v60

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getDescription()Ljava/lang/String;

    move-result-object v11

    .line 1793
    .local v11, "description":Ljava/lang/String;
    if-nez v11, :cond_e

    .line 1794
    const-string v11, ""

    .line 1796
    :cond_e
    const-string v64, "description"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1799
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getLocation()Ljava/lang/String;

    move-result-object v31

    .line 1800
    .local v31, "location":Ljava/lang/String;
    if-nez v31, :cond_f

    .line 1801
    const-string v31, ""

    .line 1803
    :cond_f
    const-string v64, "eventLocation"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806
    sget-boolean v64, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v64, :cond_11

    .line 1808
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getHtmlLink()Ljava/lang/String;

    move-result-object v23

    .line 1809
    .local v23, "htmlLink":Ljava/lang/String;
    if-eqz v23, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mEventPlusPattern:Ljava/util/regex/Pattern;

    move-object/from16 v64, v0

    move-object/from16 v0, v64

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Ljava/util/regex/Matcher;->find()Z

    move-result v64

    if-eqz v64, :cond_10

    .line 1810
    const-string v64, "customAppUri"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811
    const-string v64, "customAppPackage"

    const-string v65, "com.google.android.apps.plus"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getColorId()Ljava/lang/String;

    move-result-object v7

    .line 1816
    .local v7, "colorId":Ljava/lang/String;
    if-eqz v7, :cond_11

    .line 1817
    const-string v64, "eventColor_index"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1822
    .end local v7    # "colorId":Ljava/lang/String;
    .end local v23    # "htmlLink":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;

    move-result-object v48

    .line 1823
    .local v48, "reminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    if-eqz v48, :cond_12

    .line 1825
    invoke-virtual/range {v48 .. v48}, Lcom/google/api/services/calendar/model/Event$Reminders;->getUseDefault()Ljava/lang/Boolean;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v64

    if-eqz v64, :cond_20

    .line 1826
    if-nez p3, :cond_1f

    const/16 v47, 0x0

    .line 1830
    .local v47, "reminderList":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    :goto_6
    if-eqz v47, :cond_12

    invoke-interface/range {v47 .. v47}, Ljava/util/List;->isEmpty()Z

    move-result v64

    if-nez v64, :cond_12

    .line 1833
    const-string v64, "hasAlarm"

    const/16 v65, 0x1

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1837
    .end local v47    # "reminderList":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    move-result-object v20

    .line 1838
    .local v20, "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    const/16 v22, 0x0

    .line 1839
    .local v22, "hasExtendedProperties":I
    const/16 v21, 0x1

    .line 1840
    .local v21, "hasAlarmProperty":Z
    if-eqz v20, :cond_13

    .line 1841
    invoke-virtual/range {v20 .. v20}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getPrivate()Ljava/util/Map;

    move-result-object v40

    .line 1842
    .local v40, "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {v20 .. v20}, Lcom/google/api/services/calendar/model/Event$ExtendedProperties;->getShared()Ljava/util/Map;

    move-result-object v52

    .line 1844
    .local v52, "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v52, :cond_21

    invoke-interface/range {v52 .. v52}, Ljava/util/Map;->isEmpty()Z

    move-result v64

    if-nez v64, :cond_21

    .line 1845
    const/16 v22, 0x1

    .line 1863
    .end local v40    # "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v52    # "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_13
    :goto_7
    const-string v64, "hasExtendedProperties"

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1864
    if-eqz v21, :cond_14

    .line 1866
    const-string v64, "hasAlarm"

    const/16 v65, 0x1

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1869
    :cond_14
    const-string v65, "hasAttendeeData"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getAttendeesOmitted()Ljava/lang/Boolean;

    move-result-object v64

    const/16 v66, 0x0

    move-object/from16 v0, v64

    move/from16 v1, v66

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v64

    if-eqz v64, :cond_24

    const/16 v64, 0x0

    :goto_8
    invoke-static/range {v64 .. v64}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v64

    move-object/from16 v0, p2

    move-object/from16 v1, v65

    move-object/from16 v2, v64

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1873
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getRecurrence()Ljava/util/List;

    move-result-object v45

    .line 1874
    .local v45, "recurrence":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v49, 0x0

    .line 1875
    .local v49, "rrule":Ljava/lang/String;
    const/16 v44, 0x0

    .line 1876
    .local v44, "rdate":Ljava/lang/String;
    const/16 v19, 0x0

    .line 1877
    .local v19, "exrule":Ljava/lang/String;
    const/16 v18, 0x0

    .line 1878
    .local v18, "exdate":Ljava/lang/String;
    if-eqz v45, :cond_32

    .line 1879
    invoke-interface/range {v45 .. v45}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_15
    :goto_9
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v64

    if-eqz v64, :cond_2b

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 1880
    .local v30, "line":Ljava/lang/String;
    if-nez v30, :cond_25

    .line 1881
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Invalid null recurrence line in event "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 1736
    .end local v6    # "allDay":I
    .end local v11    # "description":Ljava/lang/String;
    .end local v16    # "endMillis":J
    .end local v18    # "exdate":Ljava/lang/String;
    .end local v19    # "exrule":Ljava/lang/String;
    .end local v20    # "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .end local v21    # "hasAlarmProperty":Z
    .end local v22    # "hasExtendedProperties":I
    .end local v30    # "line":Ljava/lang/String;
    .end local v31    # "location":Ljava/lang/String;
    .end local v44    # "rdate":Ljava/lang/String;
    .end local v45    # "recurrence":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v48    # "reminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .end local v49    # "rrule":Ljava/lang/String;
    .end local v56    # "startMillis":J
    .end local v60    # "title":Ljava/lang/String;
    .end local v61    # "transparency":Ljava/lang/String;
    .end local v63    # "visibility":Ljava/lang/String;
    :cond_16
    const/4 v6, 0x0

    .line 1737
    .restart local v6    # "allDay":I
    invoke-virtual/range {v53 .. v53}, Lcom/google/api/services/calendar/model/EventDateTime;->getTimeZone()Ljava/lang/String;

    move-result-object v59

    .line 1738
    .local v59, "timeZone":Ljava/lang/String;
    if-eqz v59, :cond_17

    .line 1739
    const-string v64, "eventTimezone"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v59

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    :goto_a
    invoke-virtual/range {v53 .. v53}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v10

    .line 1746
    .restart local v10    # "dateTime":Lcom/google/api/client/util/DateTime;
    invoke-virtual {v10}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v56

    .line 1747
    .restart local v56    # "startMillis":J
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getEnd()Lcom/google/api/services/calendar/model/EventDateTime;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Lcom/google/api/services/calendar/model/EventDateTime;->getDateTime()Lcom/google/api/client/util/DateTime;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v16

    .restart local v16    # "endMillis":J
    goto/16 :goto_3

    .line 1740
    .end local v10    # "dateTime":Lcom/google/api/client/util/DateTime;
    .end local v16    # "endMillis":J
    .end local v56    # "startMillis":J
    :cond_17
    if-eqz p3, :cond_18

    .line 1741
    const-string v64, "eventTimezone"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->calendarTimezone:Ljava/lang/String;

    move-object/from16 v65, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 1743
    :cond_18
    const-string v64, "eventTimezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 1760
    .end local v59    # "timeZone":Ljava/lang/String;
    .restart local v16    # "endMillis":J
    .restart local v56    # "startMillis":J
    .restart local v63    # "visibility":Ljava/lang/String;
    :cond_19
    const-string v64, "confidential"

    invoke-virtual/range {v63 .. v64}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_1a

    .line 1761
    const-string v64, "accessLevel"

    const/16 v65, 0x1

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 1763
    :cond_1a
    const-string v64, "private"

    invoke-virtual/range {v63 .. v64}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_1b

    .line 1764
    const-string v64, "accessLevel"

    const/16 v65, 0x2

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 1766
    :cond_1b
    const-string v64, "public"

    invoke-virtual/range {v63 .. v64}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_1c

    .line 1767
    const-string v64, "accessLevel"

    const/16 v65, 0x3

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 1770
    :cond_1c
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Unexpected visibility: "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1771
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1778
    .restart local v61    # "transparency":Ljava/lang/String;
    :cond_1d
    const-string v64, "transparent"

    move-object/from16 v0, v61

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_1e

    .line 1779
    const-string v64, "availability"

    const/16 v65, 0x1

    invoke-static/range {v65 .. v65}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 1781
    :cond_1e
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Unexpected transparency: "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1782
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1826
    .restart local v11    # "description":Ljava/lang/String;
    .restart local v31    # "location":Ljava/lang/String;
    .restart local v48    # "reminders":Lcom/google/api/services/calendar/model/Event$Reminders;
    .restart local v60    # "title":Ljava/lang/String;
    :cond_1f
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncInfo;->defaultReminders:Ljava/util/List;

    move-object/from16 v47, v0

    goto/16 :goto_6

    .line 1828
    :cond_20
    invoke-virtual/range {v48 .. v48}, Lcom/google/api/services/calendar/model/Event$Reminders;->getOverrides()Ljava/util/List;

    move-result-object v47

    .restart local v47    # "reminderList":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    goto/16 :goto_6

    .line 1846
    .end local v47    # "reminderList":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/calendar/model/EventReminder;>;"
    .restart local v20    # "extendedProperties":Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .restart local v21    # "hasAlarmProperty":Z
    .restart local v22    # "hasExtendedProperties":I
    .restart local v40    # "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v52    # "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_21
    if-eqz v40, :cond_13

    .line 1851
    invoke-interface/range {v40 .. v40}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v64

    invoke-interface/range {v64 .. v64}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .local v24, "i$":Ljava/util/Iterator;
    :cond_22
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v64

    if-eqz v64, :cond_13

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 1852
    .local v29, "key":Ljava/lang/String;
    const-string v64, "alarmReminder"

    move-object/from16 v0, v29

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v64

    if-eqz v64, :cond_23

    .line 1853
    const/16 v21, 0x1

    .line 1857
    :goto_b
    if-eqz v21, :cond_22

    const/16 v64, 0x1

    move/from16 v0, v22

    move/from16 v1, v64

    if-ne v0, v1, :cond_22

    goto/16 :goto_7

    .line 1855
    :cond_23
    const/16 v22, 0x1

    goto :goto_b

    .line 1869
    .end local v24    # "i$":Ljava/util/Iterator;
    .end local v29    # "key":Ljava/lang/String;
    .end local v40    # "privateProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v52    # "sharedProps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_24
    const/16 v64, 0x1

    goto/16 :goto_8

    .line 1886
    .restart local v18    # "exdate":Ljava/lang/String;
    .restart local v19    # "exrule":Ljava/lang/String;
    .restart local v30    # "line":Ljava/lang/String;
    .restart local v44    # "rdate":Ljava/lang/String;
    .restart local v45    # "recurrence":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v49    # "rrule":Ljava/lang/String;
    :cond_25
    :try_start_0
    new-instance v64, Lcom/android/calendarcommon2/ICalendar$Component;

    const-string v65, "DUMMY"

    const/16 v66, 0x0

    invoke-direct/range {v64 .. v66}, Lcom/android/calendarcommon2/ICalendar$Component;-><init>(Ljava/lang/String;Lcom/android/calendarcommon2/ICalendar$Component;)V

    move-object/from16 v0, v64

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/android/calendarcommon2/ICalendar;->parseComponent(Lcom/android/calendarcommon2/ICalendar$Component;Ljava/lang/String;)Lcom/android/calendarcommon2/ICalendar$Component;
    :try_end_0
    .catch Lcom/android/calendarcommon2/ICalendar$FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1893
    .local v8, "component":Lcom/android/calendarcommon2/ICalendar$Component;
    invoke-virtual {v8}, Lcom/android/calendarcommon2/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v43

    .line 1894
    .local v43, "propertyNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v43 .. v43}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_26
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v64

    if-eqz v64, :cond_15

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    .line 1895
    .local v32, "name":Ljava/lang/String;
    move-object/from16 v0, v32

    invoke-virtual {v8, v0}, Lcom/android/calendarcommon2/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v41

    .line 1896
    .local v41, "properties":Ljava/util/List;, "Ljava/util/List<Lcom/android/calendarcommon2/ICalendar$Property;>;"
    invoke-interface/range {v41 .. v41}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :goto_c
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v64

    if-eqz v64, :cond_26

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/android/calendarcommon2/ICalendar$Property;

    .line 1897
    .local v42, "property":Lcom/android/calendarcommon2/ICalendar$Property;
    const-string v64, "RRULE"

    move-object/from16 v0, v32

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_27

    .line 1899
    :try_start_1
    invoke-virtual/range {v42 .. v42}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v64

    invoke-static/range {v64 .. v64}, Lcom/google/android/syncadapters/calendar/Utils;->sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, v49

    move-object/from16 v1, v64

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v49

    goto :goto_c

    .line 1888
    .end local v8    # "component":Lcom/android/calendarcommon2/ICalendar$Component;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v32    # "name":Ljava/lang/String;
    .end local v41    # "properties":Ljava/util/List;, "Ljava/util/List<Lcom/android/calendarcommon2/ICalendar$Property;>;"
    .end local v42    # "property":Lcom/android/calendarcommon2/ICalendar$Property;
    .end local v43    # "propertyNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v14

    .line 1889
    .local v14, "e":Lcom/android/calendarcommon2/ICalendar$FormatException;
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Invalid recurrence line in event "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, ": "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, v64

    move-object/from16 v1, v65

    invoke-static {v0, v1, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1890
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1901
    .end local v14    # "e":Lcom/android/calendarcommon2/ICalendar$FormatException;
    .restart local v8    # "component":Lcom/android/calendarcommon2/ICalendar$Component;
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v32    # "name":Ljava/lang/String;
    .restart local v41    # "properties":Ljava/util/List;, "Ljava/util/List<Lcom/android/calendarcommon2/ICalendar$Property;>;"
    .restart local v42    # "property":Lcom/android/calendarcommon2/ICalendar$Property;
    .restart local v43    # "propertyNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_1
    move-exception v14

    .line 1902
    .local v14, "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Invalid recurrence line in event "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, ": "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1903
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1905
    .end local v14    # "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    :cond_27
    const-string v64, "RDATE"

    move-object/from16 v0, v32

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_28

    .line 1906
    invoke-static/range {v42 .. v42}, Lcom/google/android/syncadapters/calendar/EventHandler;->getRecurrenceDate(Lcom/android/calendarcommon2/ICalendar$Property;)Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, v44

    move-object/from16 v1, v64

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    goto/16 :goto_c

    .line 1907
    :cond_28
    const-string v64, "EXRULE"

    move-object/from16 v0, v32

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_29

    .line 1909
    :try_start_2
    invoke-virtual/range {v42 .. v42}, Lcom/android/calendarcommon2/ICalendar$Property;->getValue()Ljava/lang/String;

    move-result-object v64

    invoke-static/range {v64 .. v64}, Lcom/google/android/syncadapters/calendar/Utils;->sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, v19

    move-object/from16 v1, v64

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v19

    goto/16 :goto_c

    .line 1911
    :catch_2
    move-exception v14

    .line 1912
    .restart local v14    # "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Invalid recurrence line in event "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, ": "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1913
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1915
    .end local v14    # "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    :cond_29
    const-string v64, "EXDATE"

    move-object/from16 v0, v32

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v64

    if-eqz v64, :cond_2a

    .line 1916
    invoke-static/range {v42 .. v42}, Lcom/google/android/syncadapters/calendar/EventHandler;->getRecurrenceDate(Lcom/android/calendarcommon2/ICalendar$Property;)Ljava/lang/String;

    move-result-object v64

    move-object/from16 v0, v18

    move-object/from16 v1, v64

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/EventHandler;->appendRecurrenceString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_c

    .line 1918
    :cond_2a
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Invalid recurrence line in event "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, ": "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    invoke-static/range {v64 .. v65}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1919
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1924
    .end local v8    # "component":Lcom/android/calendarcommon2/ICalendar$Component;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v30    # "line":Ljava/lang/String;
    .end local v32    # "name":Ljava/lang/String;
    .end local v41    # "properties":Ljava/util/List;, "Ljava/util/List<Lcom/android/calendarcommon2/ICalendar$Property;>;"
    .end local v42    # "property":Lcom/android/calendarcommon2/ICalendar$Property;
    .end local v43    # "propertyNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2b
    if-eqz v18, :cond_2c

    sget v64, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v65, 0x11

    move/from16 v0, v64

    move/from16 v1, v65

    if-gt v0, v1, :cond_2c

    .line 1925
    invoke-static/range {v18 .. v18}, Lcom/google/android/syncadapters/calendar/Utils;->collateDatesByTimeZone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1928
    :cond_2c
    :try_start_3
    new-instance v64, Lcom/android/calendarcommon2/RecurrenceSet;

    move-object/from16 v0, v64

    move-object/from16 v1, v49

    move-object/from16 v2, v44

    move-object/from16 v3, v19

    move-object/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendarcommon2/RecurrenceSet;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1933
    sub-long v64, v16, v56

    const-wide/16 v66, 0x3e8

    div-long v12, v64, v66

    .line 1934
    .local v12, "duration":J
    if-nez v6, :cond_31

    .line 1935
    const-string v64, "duration"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "P"

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, "S"

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1943
    .end local v12    # "duration":J
    :goto_d
    const-string v64, "rrule"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    const-string v64, "rdate"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1945
    const-string v64, "exrule"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1946
    const-string v64, "exdate"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1948
    const-string v65, "guestsCanInviteOthers"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanInviteOthers()Ljava/lang/Boolean;

    move-result-object v64

    const/16 v66, 0x1

    move-object/from16 v0, v64

    move/from16 v1, v66

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v64

    if-eqz v64, :cond_33

    const/16 v64, 0x1

    :goto_e
    invoke-static/range {v64 .. v64}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v64

    move-object/from16 v0, p2

    move-object/from16 v1, v65

    move-object/from16 v2, v64

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1950
    const-string v65, "guestsCanModify"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanModify()Ljava/lang/Boolean;

    move-result-object v64

    const/16 v66, 0x0

    move-object/from16 v0, v64

    move/from16 v1, v66

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v64

    if-eqz v64, :cond_34

    const/16 v64, 0x1

    :goto_f
    invoke-static/range {v64 .. v64}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v64

    move-object/from16 v0, p2

    move-object/from16 v1, v65

    move-object/from16 v2, v64

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1952
    const-string v65, "guestsCanSeeGuests"

    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getGuestsCanSeeOtherGuests()Ljava/lang/Boolean;

    move-result-object v64

    const/16 v66, 0x1

    move-object/from16 v0, v64

    move/from16 v1, v66

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v64

    if-eqz v64, :cond_35

    const/16 v64, 0x1

    :goto_10
    invoke-static/range {v64 .. v64}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v64

    move-object/from16 v0, p2

    move-object/from16 v1, v65

    move-object/from16 v2, v64

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1955
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/services/calendar/model/Event;->getOrganizer()Lcom/google/api/services/calendar/model/Event$Organizer;

    move-result-object v33

    .line 1956
    .local v33, "organizer":Lcom/google/api/services/calendar/model/Event$Organizer;
    if-eqz v33, :cond_30

    .line 1962
    const/16 v34, 0x0

    .line 1963
    .local v34, "organizerEmail":Ljava/lang/String;
    invoke-virtual/range {v33 .. v33}, Lcom/google/api/services/calendar/model/Event$Organizer;->getSelf()Ljava/lang/Boolean;

    move-result-object v64

    const/16 v65, 0x0

    invoke-static/range {v64 .. v65}, Lcom/google/android/syncadapters/calendar/Utils;->getBooleanValue(Ljava/lang/Boolean;Z)Z

    move-result v50

    .line 1964
    .local v50, "self":Z
    if-eqz v50, :cond_2d

    .line 1965
    const-wide/16 v64, 0x0

    cmp-long v64, p4, v64

    if-gez v64, :cond_36

    .line 1967
    const-string v64, "EventHandler"

    const-string v65, "Missing calendarId, can\'t get owner"

    new-instance v66, Ljava/lang/Throwable;

    invoke-direct/range {v66 .. v66}, Ljava/lang/Throwable;-><init>()V

    invoke-static/range {v64 .. v66}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1972
    :cond_2d
    :goto_11
    if-nez v34, :cond_2e

    .line 1973
    invoke-virtual/range {v33 .. v33}, Lcom/google/api/services/calendar/model/Event$Organizer;->getEmail()Ljava/lang/String;

    move-result-object v34

    .line 1975
    :cond_2e
    if-nez v34, :cond_2f

    .line 1977
    const-string v64, "id"

    move-object/from16 v0, v33

    move-object/from16 v1, v64

    invoke-virtual {v0, v1}, Lcom/google/api/services/calendar/model/Event$Organizer;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    .line 1978
    .local v35, "organizerId":Ljava/lang/String;
    if-eqz v35, :cond_2f

    .line 1979
    new-instance v64, Ljava/lang/StringBuilder;

    invoke-direct/range {v64 .. v64}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v64

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v64

    const-string v65, "@profile.calendar.google.com"

    invoke-virtual/range {v64 .. v65}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v64

    invoke-virtual/range {v64 .. v64}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 1982
    .end local v35    # "organizerId":Ljava/lang/String;
    :cond_2f
    const-string v64, "organizer"

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    .end local v34    # "organizerEmail":Ljava/lang/String;
    .end local v50    # "self":Z
    :cond_30
    const/16 v64, 0x0

    goto/16 :goto_2

    .line 1929
    .end local v33    # "organizer":Lcom/google/api/services/calendar/model/Event$Organizer;
    :catch_3
    move-exception v14

    .line 1930
    .restart local v14    # "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    const-string v64, "EventHandler"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "Unable to parse recurrence in event "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, ": "

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    move-object/from16 v0, v65

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, v64

    move-object/from16 v1, v65

    invoke-static {v0, v1, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1931
    const/16 v64, 0x2

    goto/16 :goto_2

    .line 1937
    .end local v14    # "e":Lcom/android/calendarcommon2/EventRecurrence$InvalidFormatException;
    .restart local v12    # "duration":J
    :cond_31
    const-string v64, "duration"

    new-instance v65, Ljava/lang/StringBuilder;

    invoke-direct/range {v65 .. v65}, Ljava/lang/StringBuilder;-><init>()V

    const-string v66, "P"

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    const-wide/32 v66, 0x15180

    div-long v66, v12, v66

    invoke-virtual/range {v65 .. v67}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v65

    const-string v66, "D"

    invoke-virtual/range {v65 .. v66}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v65

    invoke-virtual/range {v65 .. v65}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1941
    .end local v12    # "duration":J
    :cond_32
    const-string v64, "dtend"

    const-wide/16 v66, 0x3e8

    div-long v66, v16, v66

    const-wide/16 v68, 0x3e8

    mul-long v66, v66, v68

    invoke-static/range {v66 .. v67}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v65

    move-object/from16 v0, p2

    move-object/from16 v1, v64

    move-object/from16 v2, v65

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_d

    .line 1948
    :cond_33
    const/16 v64, 0x0

    goto/16 :goto_e

    .line 1950
    :cond_34
    const/16 v64, 0x0

    goto/16 :goto_f

    .line 1952
    :cond_35
    const/16 v64, 0x0

    goto/16 :goto_10

    .line 1969
    .restart local v33    # "organizer":Lcom/google/api/services/calendar/model/Event$Organizer;
    .restart local v34    # "organizerEmail":Ljava/lang/String;
    .restart local v50    # "self":Z
    :cond_36
    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->getCalendarOwnerAccount(J)Ljava/lang/String;

    move-result-object v34

    goto/16 :goto_11
.end method

.method public getDeletedColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1267
    const-string v0, "deleted"

    return-object v0
.end method

.method public getEntitySelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2060
    const-string v0, "_sync_id IS NULL OR (_sync_id IS NOT NULL AND lastSynced = 0 AND (dirty != 0 OR deleted != 0))"

    return-object v0
.end method

.method public getEntityUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 196
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public itemToSourceId(Lcom/google/api/services/calendar/model/Event;)Ljava/lang/String;
    .locals 2
    .param p1, "event"    # Lcom/google/api/services/calendar/model/Event;

    .prologue
    .line 192
    invoke-virtual {p1}, Lcom/google/api/services/calendar/model/Event;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/syncadapters/calendar/Utils;->createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic itemToSourceId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 86
    check-cast p1, Lcom/google/api/services/calendar/model/Event;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->itemToSourceId(Lcom/google/api/services/calendar/model/Event;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public newEntityIterator(Ljava/lang/String;[Ljava/lang/String;I)Landroid/content/EntityIterator;
    .locals 6
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .param p3, "limit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 201
    sget-object v0, Landroid/provider/CalendarContract$EventsEntity;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v3}, Lcom/google/android/syncadapters/SyncAdapterUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 202
    .local v1, "uri":Landroid/net/Uri;
    if-gtz p3, :cond_0

    move-object v5, v2

    .line 203
    .local v5, "sortOrder":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apiary/ProviderHelper;->queryProvider(Landroid/content/ContentProviderClient;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    invoke-static {v0, v2}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentProviderClient;)Landroid/content/EntityIterator;

    move-result-object v0

    return-object v0

    .line 202
    .end local v5    # "sortOrder":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id LIMIT "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public sendEntityToServer(Landroid/content/Entity;Landroid/content/SyncResult;)Ljava/util/ArrayList;
    .locals 44
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Entity;",
            "Landroid/content/SyncResult;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;,
            Lcom/google/android/apiary/ParseException;
        }
    .end annotation

    .prologue
    .line 216
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v43

    .line 217
    .local v43, "values":Landroid/content/ContentValues;
    const-string v4, "cal_sync1"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 219
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Entity with no calendar id found: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/16 v37, 0x0

    .line 581
    :goto_0
    return-object v37

    .line 223
    :cond_0
    const-string v4, "local android etag magic value"

    const-string v5, "sync_data4"

    move-object/from16 v0, v43

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 225
    const-string v4, "EventHandler"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 226
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Entity with magic ETAG found: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_1
    const/16 v37, 0x0

    goto :goto_0

    .line 231
    :cond_2
    const-string v4, "_sync_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 233
    .local v38, "providerId":Ljava/lang/String;
    if-eqz v38, :cond_3

    const-string v4, "SYNC_ERROR: "

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 237
    const-string v5, "_sync_id"

    const/4 v4, 0x0

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v43

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const/16 v29, 0x0

    .line 242
    .local v29, "eventId":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v19, "primary"

    .line 244
    .local v19, "calendarId":Ljava/lang/String;
    :goto_2
    const-string v4, "deleted"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_5

    const/16 v31, 0x1

    .line 246
    .local v31, "isDeleted":Z
    :goto_3
    if-eqz v31, :cond_6

    if-nez v29, :cond_6

    .line 247
    const-string v4, "EventHandler"

    const-string v5, "Local event deleted. Do nothing"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const/16 v37, 0x0

    goto/16 :goto_0

    .line 240
    .end local v19    # "calendarId":Ljava/lang/String;
    .end local v29    # "eventId":Ljava/lang/String;
    .end local v31    # "isDeleted":Z
    :cond_3
    invoke-static/range {v38 .. v38}, Lcom/google/android/syncadapters/calendar/Utils;->extractEventId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .restart local v29    # "eventId":Ljava/lang/String;
    goto :goto_1

    .line 242
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mCalendarId:Ljava/lang/String;

    move-object/from16 v19, v0

    goto :goto_2

    .line 244
    .restart local v19    # "calendarId":Ljava/lang/String;
    :cond_5
    const/16 v31, 0x0

    goto :goto_3

    .line 252
    .restart local v31    # "isDeleted":Z
    :cond_6
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v18

    .line 253
    .local v18, "activeTag":I
    const/16 v42, 0x0

    .line 255
    .local v42, "tag":I
    const-string v4, "duration"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->parseDuration(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/syncadapters/calendar/EventHandler;->setDtendFromDuration(Landroid/content/ContentValues;J)V

    .line 257
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v37, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mContentResolver:Landroid/content/ContentResolver;

    const-string v5, "google_calendar_sync_max_attendees"

    const/16 v8, 0x32

    invoke-static {v4, v5, v8}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v32

    .line 270
    .local v32, "maxAttendees":I
    :try_start_0
    const-string v4, "mutators"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "mutators"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 272
    .local v34, "mutators":Ljava/lang/String;
    :goto_4
    if-nez v29, :cond_16

    .line 277
    const-string v4, "eventColor_index"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 278
    const-string v4, "EventHandler"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 279
    const-string v4, "EventHandler"

    const-string v5, "Missing EVENT_COLOR_KEY, fetching from provider"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v5, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v8, 0x2

    new-array v6, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "eventColor"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, "eventColor_index"

    aput-object v9, v6, v8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v24

    .line 289
    .local v24, "cursor":Landroid/database/Cursor;
    if-eqz v24, :cond_9

    .line 291
    :try_start_1
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 292
    const-string v4, "eventColor"

    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 293
    const-string v4, "eventColor_index"

    const/4 v5, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    :cond_8
    :try_start_2
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 303
    .end local v24    # "cursor":Landroid/database/Cursor;
    :cond_9
    const-string v4, "originalInstanceTime"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "original_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 305
    :cond_a
    const-string v4, "rrule"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 306
    const-string v4, "rdate"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 307
    const-string v4, "exrule"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 308
    const-string v4, "exdate"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 310
    :cond_b
    or-int/lit8 v42, v18, 0x1

    .line 311
    invoke-static/range {v42 .. v42}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 312
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v28

    .line 314
    .local v28, "event":Lcom/google/api/services/calendar/model/Event;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v4, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->insert(Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    move-result-object v40

    .line 317
    .local v40, "request":Lcom/google/api/services/calendar/Calendar$Events$Insert;
    const-string v4, "maxAttendees"

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    .line 318
    const-string v4, "userAgentPackage"

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v4, v1}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Insert;

    .line 319
    invoke-virtual/range {v40 .. v40}, Lcom/google/api/services/calendar/Calendar$Events$Insert;->execute()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/calendar/model/Event;
    :try_end_3
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 574
    .end local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    .end local v40    # "request":Lcom/google/api/services/calendar/Calendar$Events$Insert;
    .local v13, "result":Lcom/google/api/services/calendar/model/Event;
    :goto_5
    if-eqz v42, :cond_c

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_c
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 580
    const/4 v15, 0x1

    const/16 v17, 0x0

    move-object/from16 v11, p0

    move-object/from16 v12, v37

    move-object/from16 v14, p1

    move-object/from16 v16, p2

    invoke-virtual/range {v11 .. v17}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 270
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    .end local v34    # "mutators":Ljava/lang/String;
    :cond_d
    const/16 v34, 0x0

    goto/16 :goto_4

    .line 296
    .restart local v24    # "cursor":Landroid/database/Cursor;
    .restart local v34    # "mutators":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_4
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 574
    .end local v24    # "cursor":Landroid/database/Cursor;
    .end local v34    # "mutators":Ljava/lang/String;
    :catchall_1
    move-exception v4

    if-eqz v42, :cond_e

    .line 575
    const/4 v5, 0x1

    move/from16 v0, v42

    invoke-static {v0, v5}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_e
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    throw v4

    .line 320
    .restart local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    .restart local v34    # "mutators":Ljava/lang/String;
    :catch_0
    move-exception v27

    .line 321
    .local v27, "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    :try_start_5
    invoke-virtual/range {v27 .. v27}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v41

    .line 322
    .local v41, "statusCode":I
    const/16 v4, 0x193

    move/from16 v0, v41

    if-ne v0, v4, :cond_11

    .line 323
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t insert event into "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Forbidden"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-virtual/range {v28 .. v28}, Lcom/google/api/services/calendar/model/Event;->getRecurringEventId()Ljava/lang/String;

    move-result-object v39

    .line 325
    .local v39, "recurringEventId":Ljava/lang/String;
    if-eqz v39, :cond_f

    .line 326
    const-string v4, "EventHandler"

    const-string v5, "Refresh original event"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/calendar/model/Event;

    .line 330
    .local v6, "originalEvent":Lcom/google/api/services/calendar/model/Event;
    const-string v4, "_sync_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v39, v5, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/syncadapters/calendar/EventHandler;->fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;

    move-result-object v7

    .line 332
    .local v7, "originalEntity":Landroid/content/Entity;
    const/4 v8, 0x1

    const/4 v10, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, v37

    move-object/from16 v9, p2

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/syncadapters/calendar/EventHandler;->applyItemToEntity(Ljava/util/List;Lcom/google/api/services/calendar/model/Event;Landroid/content/Entity;ZLandroid/content/SyncResult;Ljava/lang/Object;)V

    .line 337
    .end local v6    # "originalEvent":Lcom/google/api/services/calendar/model/Event;
    .end local v7    # "originalEntity":Landroid/content/Entity;
    :cond_f
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v4, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-static {v0, v4, v5, v8}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addDeleteOperation(Ljava/util/List;Landroid/net/Uri;Ljava/lang/Long;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 574
    if-eqz v42, :cond_10

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_10
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 345
    .end local v39    # "recurringEventId":Ljava/lang/String;
    :cond_11
    const/16 v4, 0x190

    move/from16 v0, v41

    if-lt v0, v4, :cond_14

    const/16 v4, 0x1f4

    move/from16 v0, v41

    if-ge v0, v4, :cond_14

    .line 350
    :try_start_6
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 351
    .local v10, "updateValues":Landroid/content/ContentValues;
    const-string v4, "dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 352
    invoke-virtual/range {v27 .. v27}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getDetails()Lcom/google/api/client/googleapis/json/GoogleJsonError;

    move-result-object v25

    .line 353
    .local v25, "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    if-eqz v25, :cond_13

    invoke-virtual/range {v25 .. v25}, Lcom/google/api/client/googleapis/json/GoogleJsonError;->getMessage()Ljava/lang/String;

    move-result-object v4

    :goto_6
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    .line 355
    .local v33, "message":Ljava/lang/String;
    const-string v4, "_sync_id"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SYNC_ERROR: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const-string v4, "_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v8, v37

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 574
    if-eqz v42, :cond_12

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_12
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 353
    .end local v33    # "message":Ljava/lang/String;
    :cond_13
    :try_start_7
    invoke-static/range {v41 .. v41}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_6

    .line 365
    .end local v10    # "updateValues":Landroid/content/ContentValues;
    .end local v25    # "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    :cond_14
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t insert event in "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 366
    const/16 v37, 0x0

    .line 574
    .end local v37    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v42, :cond_15

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_15
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 368
    .end local v27    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .end local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    .end local v41    # "statusCode":I
    .restart local v37    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_16
    if-eqz v31, :cond_19

    .line 370
    or-int/lit8 v42, v18, 0x3

    .line 371
    :try_start_8
    invoke-static/range {v42 .. v42}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 373
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v4, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->delete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    move-result-object v40

    .line 376
    .local v40, "request":Lcom/google/api/services/calendar/Calendar$Events$Delete;
    const-string v4, "userAgentPackage"

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v4, v1}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Delete;

    .line 377
    invoke-virtual/range {v40 .. v40}, Lcom/google/api/services/calendar/Calendar$Events$Delete;->execute()Ljava/lang/Object;
    :try_end_9
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 378
    const/4 v13, 0x0

    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 379
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    .end local v40    # "request":Lcom/google/api/services/calendar/Calendar$Events$Delete;
    :catch_1
    move-exception v27

    .line 380
    .restart local v27    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    :try_start_a
    invoke-virtual/range {v27 .. v27}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v20

    .line 381
    .local v20, "code":I
    sparse-switch v20, :sswitch_data_0

    .line 411
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t delete event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 412
    const/16 v4, 0x190

    move/from16 v0, v20

    if-lt v0, v4, :cond_17

    const/16 v4, 0x1f4

    move/from16 v0, v20

    if-ge v0, v4, :cond_17

    .line 414
    const/4 v13, 0x0

    .line 415
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 383
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :sswitch_0
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t delete event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Forbidden"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 386
    .restart local v10    # "updateValues":Landroid/content/ContentValues;
    const-string v4, "deleted"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 387
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const-string v4, "_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v8, v37

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V

    .line 394
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v4, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Get;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/calendar/model/Event;

    .line 397
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 400
    .end local v10    # "updateValues":Landroid/content/ContentValues;
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :sswitch_1
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t delete event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Not found"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v13, 0x0

    .line 403
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 406
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :sswitch_2
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t delete event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Deleted from server"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 408
    const/4 v13, 0x0

    .line 409
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 418
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :cond_17
    const/16 v37, 0x0

    .line 574
    .end local v37    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v42, :cond_18

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_18
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 424
    .end local v20    # "code":I
    .end local v27    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .restart local v37    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_19
    or-int/lit8 v42, v18, 0x2

    .line 425
    :try_start_b
    invoke-static/range {v42 .. v42}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 426
    const-string v4, "_sync_id =? AND lastSynced =?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v38, v5, v8

    const/4 v8, 0x1

    const-string v9, "1"

    aput-object v9, v5, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/syncadapters/calendar/EventHandler;->fetchEntity(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Entity;

    move-result-object v35

    .line 434
    .local v35, "oldEntity":Landroid/content/Entity;
    const-string v4, "eventColor_index"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 435
    const-string v4, "EventHandler"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 436
    const-string v4, "EventHandler"

    const-string v5, "Missing EVENT_COLOR_KEY, fetching from provider"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_1a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mProvider:Landroid/content/ContentProviderClient;

    sget-object v12, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v13, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "eventColor"

    aput-object v5, v13, v4

    const/4 v4, 0x1

    const-string v5, "eventColor_index"

    aput-object v5, v13, v4

    const/4 v4, 0x2

    const-string v5, "dirty"

    aput-object v5, v13, v4

    const-string v14, "_sync_id=?"

    const/4 v4, 0x1

    new-array v15, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v29, v15, v4

    const/16 v16, 0x0

    invoke-virtual/range {v11 .. v16}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v24

    .line 448
    .restart local v24    # "cursor":Landroid/database/Cursor;
    if-eqz v24, :cond_1e

    .line 450
    :cond_1b
    :goto_7
    :try_start_c
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 451
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 452
    .local v22, "color":J
    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 453
    .local v21, "colorKey":Ljava/lang/String;
    const/4 v4, 0x2

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 454
    .local v26, "dirty":I
    const/4 v4, 0x1

    move/from16 v0, v26

    if-ne v0, v4, :cond_1c

    .line 455
    const-string v4, "eventColor"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 456
    const-string v4, "eventColor_index"

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_7

    .line 464
    .end local v21    # "colorKey":Ljava/lang/String;
    .end local v22    # "color":J
    .end local v26    # "dirty":I
    :catchall_2
    move-exception v4

    :try_start_d
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 457
    .restart local v21    # "colorKey":Ljava/lang/String;
    .restart local v22    # "color":J
    .restart local v26    # "dirty":I
    :cond_1c
    if-eqz v35, :cond_1b

    .line 458
    :try_start_e
    invoke-virtual/range {v35 .. v35}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v36

    .line 459
    .local v36, "oldValues":Landroid/content/ContentValues;
    const-string v4, "eventColor"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 460
    const-string v4, "eventColor_index"

    move-object/from16 v0, v36

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_7

    .line 464
    .end local v21    # "colorKey":Ljava/lang/String;
    .end local v22    # "color":J
    .end local v26    # "dirty":I
    .end local v36    # "oldValues":Landroid/content/ContentValues;
    :cond_1d
    :try_start_f
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 470
    .end local v24    # "cursor":Landroid/database/Cursor;
    :cond_1e
    if-nez v35, :cond_21

    .line 471
    :try_start_10
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertEntityToEntry(Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v28

    .line 472
    .restart local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    const-string v4, "hasAttendeeData"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_1f

    .line 473
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    .line 475
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    move-object/from16 v2, v28

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/api/services/calendar/Calendar$Events;->update(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Update;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    move-result-object v40

    .line 478
    .local v40, "request":Lcom/google/api/services/calendar/Calendar$Events$Update;
    const-string v4, "maxAttendees"

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Update;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    .line 479
    const-string v4, "sync_data4"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 482
    const-string v4, "ifmatch"

    const-string v5, "sync_data4"

    move-object/from16 v0, v43

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Update;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    .line 484
    :cond_20
    const-string v4, "userAgentPackage"

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v4, v1}, Lcom/google/api/services/calendar/Calendar$Events$Update;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Update;

    .line 485
    invoke-virtual/range {v40 .. v40}, Lcom/google/api/services/calendar/Calendar$Events$Update;->execute()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/calendar/model/Event;

    .line 486
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 487
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    .end local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    .end local v40    # "request":Lcom/google/api/services/calendar/Calendar$Events$Update;
    :cond_21
    invoke-virtual/range {v35 .. v35}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v36

    .line 488
    .restart local v36    # "oldValues":Landroid/content/ContentValues;
    const-string v4, "duration"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->parseDuration(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/syncadapters/calendar/EventHandler;->setDtendFromDuration(Landroid/content/ContentValues;J)V

    .line 490
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v35

    invoke-direct {v0, v1, v2}, Lcom/google/android/syncadapters/calendar/EventHandler;->convertValuesToPartialDiff(Landroid/content/Entity;Landroid/content/Entity;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v28

    .line 491
    .restart local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    invoke-virtual/range {v28 .. v28}, Lcom/google/api/services/calendar/model/Event;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_23

    .line 492
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No diffs found for event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v4, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "dirty"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_10
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 574
    if-eqz v42, :cond_22

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_22
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 504
    :cond_23
    :try_start_11
    invoke-static/range {v28 .. v28}, Lcom/google/android/syncadapters/calendar/EventHandler;->hasOnlyUnsyncedKeys(Lcom/google/api/services/calendar/model/Event;)Z

    move-result v30

    .line 505
    .local v30, "hasOnlyUnsyncedKeys":Z
    if-eqz v30, :cond_24

    .line 506
    const-string v4, "description"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lcom/google/android/syncadapters/calendar/EventHandler;->touchDescription(Lcom/google/api/services/calendar/model/Event;Ljava/lang/String;)V

    .line 508
    :cond_24
    const-string v4, "hasAttendeeData"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_25

    .line 509
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/api/services/calendar/model/Event;->setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;

    .line 511
    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    move-object/from16 v2, v28

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/api/services/calendar/Calendar$Events;->patch(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/calendar/model/Event;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    move-result-object v5

    if-nez v30, :cond_26

    const/4 v4, 0x1

    :goto_8
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->setSendNotifications(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    move-result-object v40

    .line 514
    .local v40, "request":Lcom/google/api/services/calendar/Calendar$Events$Patch;
    const-string v4, "maxAttendees"

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    .line 515
    const-string v4, "userAgentPackage"

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v4, v1}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/Calendar$Events$Patch;

    .line 516
    invoke-virtual/range {v40 .. v40}, Lcom/google/api/services/calendar/Calendar$Events$Patch;->execute()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/calendar/model/Event;
    :try_end_11
    .catch Lcom/google/api/client/googleapis/json/GoogleJsonResponseException; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 511
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    .end local v40    # "request":Lcom/google/api/services/calendar/Calendar$Events$Patch;
    :cond_26
    const/4 v4, 0x0

    goto :goto_8

    .line 518
    .end local v28    # "event":Lcom/google/api/services/calendar/model/Event;
    .end local v30    # "hasOnlyUnsyncedKeys":Z
    .end local v36    # "oldValues":Landroid/content/ContentValues;
    :catch_2
    move-exception v27

    .line 519
    .restart local v27    # "e":Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    :try_start_12
    invoke-virtual/range {v27 .. v27}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v20

    .line 520
    .restart local v20    # "code":I
    sparse-switch v20, :sswitch_data_1

    .line 550
    const/16 v4, 0x190

    move/from16 v0, v20

    if-lt v0, v4, :cond_29

    const/16 v4, 0x1f4

    move/from16 v0, v20

    if-ge v0, v4, :cond_29

    .line 553
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 554
    .restart local v10    # "updateValues":Landroid/content/ContentValues;
    const-string v4, "dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v10, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 555
    invoke-virtual/range {v27 .. v27}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getDetails()Lcom/google/api/client/googleapis/json/GoogleJsonError;

    move-result-object v25

    .line 556
    .restart local v25    # "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    if-eqz v25, :cond_28

    invoke-virtual/range {v25 .. v25}, Lcom/google/api/client/googleapis/json/GoogleJsonError;->getMessage()Ljava/lang/String;

    move-result-object v4

    :goto_9
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    .line 558
    .restart local v33    # "message":Ljava/lang/String;
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mAccount:Landroid/accounts/Account;

    invoke-static {v4, v5}, Lcom/google/android/syncadapters/calendar/HandlerUtils;->addQueryParameters(Landroid/net/Uri;Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v9

    const-string v4, "_id"

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v8, v37

    invoke-static/range {v8 .. v13}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->addUpdateOperation(Ljava/util/List;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/Long;Ljava/lang/Integer;Z)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 574
    if-eqz v42, :cond_27

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_27
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 522
    .end local v10    # "updateValues":Landroid/content/ContentValues;
    .end local v25    # "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    .end local v33    # "message":Ljava/lang/String;
    :sswitch_3
    :try_start_13
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t update event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Forbidden"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v4, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Get;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/calendar/model/Event;

    .line 527
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 530
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :sswitch_4
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t update event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Not found"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    const/4 v13, 0x0

    .line 533
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 536
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :sswitch_5
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t update event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Deleted from server"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    const/4 v13, 0x0

    .line 539
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 543
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    :sswitch_6
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t update event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": Conflict detected"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/syncadapters/calendar/EventHandler;->mClient:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar;->events()Lcom/google/api/services/calendar/Calendar$Events;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v4, v0, v1}, Lcom/google/api/services/calendar/Calendar$Events;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/calendar/Calendar$Events$Get;->setMaxAttendees(Ljava/lang/Integer;)Lcom/google/api/services/calendar/Calendar$Events$Get;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/calendar/Calendar$Events$Get;->execute()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/calendar/model/Event;

    .line 548
    .restart local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    goto/16 :goto_5

    .line 556
    .end local v13    # "result":Lcom/google/api/services/calendar/model/Event;
    .restart local v10    # "updateValues":Landroid/content/ContentValues;
    .restart local v25    # "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    :cond_28
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_9

    .line 568
    .end local v10    # "updateValues":Landroid/content/ContentValues;
    .end local v25    # "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    :cond_29
    const-string v4, "EventHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t update event "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 569
    const/16 v37, 0x0

    .line 574
    .end local v37    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v42, :cond_2a

    .line 575
    const/4 v4, 0x1

    move/from16 v0, v42

    invoke-static {v0, v4}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 577
    :cond_2a
    invoke-static/range {v18 .. v18}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    goto/16 :goto_0

    .line 381
    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_0
        0x194 -> :sswitch_1
        0x19e -> :sswitch_2
    .end sparse-switch

    .line 520
    :sswitch_data_1
    .sparse-switch
        0x193 -> :sswitch_3
        0x194 -> :sswitch_4
        0x19c -> :sswitch_6
        0x19e -> :sswitch_5
    .end sparse-switch
.end method
