.class public Lcom/google/android/syncadapters/calendar/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final JELLY_BEAN_OR_HIGHER:Z

.field public static final WHERE_ACCOUNT_AND_COLOR:Ljava/lang/String;

.field public static final WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

.field public static final WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

.field public static final WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

.field public static final WHERE_CALENDARS_ACCOUNT_AND_COLOR:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "account_name=?"

    aput-object v3, v2, v1

    const-string v3, "account_type=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    .line 60
    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "sync_events=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_SYNC:Ljava/lang/String;

    .line 62
    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "color_type=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

    .line 64
    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "color_index=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_COLOR:Ljava/lang/String;

    .line 66
    new-array v2, v4, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_ACCOUNT_AND_TYPE:Ljava/lang/String;

    aput-object v3, v2, v1

    const-string v3, "calendar_color_index=?"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/syncadapters/calendar/Utils;->makeWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/syncadapters/calendar/Utils;->WHERE_CALENDARS_ACCOUNT_AND_COLOR:Ljava/lang/String;

    .line 72
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    :goto_0
    sput-boolean v0, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static collateDatesByTimeZone(Ljava/lang/String;)Ljava/lang/String;
    .locals 17
    .param p0, "dates"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v10

    .line 165
    .local v10, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v15, "\n"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 166
    .local v8, "lines":[Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v7, v1, v4

    .line 168
    .local v7, "line":Ljava/lang/String;
    const/16 v15, 0x3b

    invoke-virtual {v7, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    .line 171
    .local v11, "p":I
    if-gez v11, :cond_1

    .line 172
    const-string v13, ""

    .line 173
    .local v13, "tz":Ljava/lang/String;
    move-object v14, v7

    .line 179
    .local v14, "values":Ljava/lang/String;
    :goto_1
    invoke-interface {v10, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 180
    .local v9, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v9, :cond_0

    .line 181
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .restart local v9    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v10, v13, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    :cond_0
    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 175
    .end local v9    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "tz":Ljava/lang/String;
    .end local v14    # "values":Ljava/lang/String;
    :cond_1
    const/4 v15, 0x0

    invoke-virtual {v7, v15, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 176
    .restart local v13    # "tz":Ljava/lang/String;
    add-int/lit8 v15, v11, 0x1

    invoke-virtual {v7, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "values":Ljava/lang/String;
    goto :goto_1

    .line 189
    .end local v7    # "line":Ljava/lang/String;
    .end local v11    # "p":I
    .end local v13    # "tz":Ljava/lang/String;
    .end local v14    # "values":Ljava/lang/String;
    :cond_2
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v15

    invoke-static {v15}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 190
    .local v2, "entries":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;>;"
    new-instance v15, Lcom/google/android/syncadapters/calendar/Utils$1;

    invoke-direct {v15}, Lcom/google/android/syncadapters/calendar/Utils$1;-><init>()V

    invoke-static {v2, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 199
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 200
    .local v12, "sb":Ljava/lang/StringBuilder;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 201
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 202
    .restart local v13    # "tz":Ljava/lang/String;
    const-string v16, ","

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Iterable;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v14

    .line 203
    .restart local v14    # "values":Ljava/lang/String;
    const-string v15, ""

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 204
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x3b

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    :cond_3
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string v15, "\n"

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 210
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v13    # "tz":Ljava/lang/String;
    .end local v14    # "values":Ljava/lang/String;
    :cond_4
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    .line 211
    .local v6, "length":I
    if-lez v6, :cond_5

    .line 212
    add-int/lit8 v15, v6, -0x1

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 214
    :cond_5
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    return-object v15
.end method

.method public static createEventId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "calendarId"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    .end local p0    # "id":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "id":Ljava/lang/String;
    :cond_1
    sget-boolean v0, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static extractEventId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 95
    if-nez p0, :cond_1

    .line 96
    const/4 p0, 0x0

    .line 102
    .local v0, "i":I
    :cond_0
    :goto_0
    return-object p0

    .line 98
    .end local v0    # "i":I
    :cond_1
    const-string v1, "\n"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 99
    .restart local v0    # "i":I
    if-ltz v0, :cond_0

    .line 100
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method static getBooleanValue(Ljava/lang/Boolean;Z)Z
    .locals 0
    .param p0, "value"    # Ljava/lang/Boolean;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 133
    if-nez p0, :cond_0

    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .restart local p1    # "defaultValue":Z
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0
.end method

.method public static getDisplayColorFromColor(I)I
    .locals 5
    .param p0, "color"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 137
    sget-boolean v1, Lcom/google/android/syncadapters/calendar/Utils;->JELLY_BEAN_OR_HIGHER:Z

    if-eqz v1, :cond_0

    .line 144
    .end local p0    # "color":I
    :goto_0
    return p0

    .line 140
    .restart local p0    # "color":I
    :cond_0
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 141
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 142
    aget v1, v0, v3

    const v2, 0x3fa66666    # 1.3f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v3

    .line 143
    aget v1, v0, v4

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    aput v1, v0, v4

    .line 144
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result p0

    goto :goto_0
.end method

.method public static varargs makeWhere([Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "parts"    # [Ljava/lang/String;

    .prologue
    .line 84
    const-string v0, " AND "

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static sanitizeRecurrence(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "rule"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 108
    if-nez p0, :cond_0

    .line 129
    :goto_0
    return-object v3

    .line 111
    :cond_0
    new-instance v0, Lcom/android/calendarcommon2/EventRecurrence;

    invoke-direct {v0}, Lcom/android/calendarcommon2/EventRecurrence;-><init>()V

    .line 112
    .local v0, "recurrence":Lcom/android/calendarcommon2/EventRecurrence;
    invoke-virtual {v0, p0}, Lcom/android/calendarcommon2/EventRecurrence;->parse(Ljava/lang/String;)V

    .line 116
    iget-object v2, v0, Lcom/android/calendarcommon2/EventRecurrence;->until:Ljava/lang/String;

    .line 117
    .local v2, "until":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 118
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 119
    .local v1, "time":Landroid/text/format/Time;
    invoke-virtual {v1, v2}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 120
    iget v4, v1, Landroid/text/format/Time;->year:I

    const/16 v5, 0x7f6

    if-lt v4, v5, :cond_1

    .line 126
    iput-object v3, v0, Lcom/android/calendarcommon2/EventRecurrence;->until:Ljava/lang/String;

    .line 129
    .end local v1    # "time":Landroid/text/format/Time;
    :cond_1
    invoke-virtual {v0}, Lcom/android/calendarcommon2/EventRecurrence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
