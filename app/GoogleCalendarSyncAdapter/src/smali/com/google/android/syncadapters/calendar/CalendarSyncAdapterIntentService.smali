.class public Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterIntentService;
.super Landroid/app/IntentService;
.source "CalendarSyncAdapterIntentService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "CalendarSyncAdapterIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 16
    .local v1, "applicationContext":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterService;->getOrMakeSyncAdapter(Landroid/content/Context;)Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;

    move-result-object v2

    .line 19
    .local v2, "syncAdapter":Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 21
    invoke-virtual {v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->updateColorMapFromGsf()V

    .line 25
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const-string v3, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 23
    invoke-virtual {v2}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapterApiary;->onAccountsUpdated()V

    goto :goto_0
.end method
