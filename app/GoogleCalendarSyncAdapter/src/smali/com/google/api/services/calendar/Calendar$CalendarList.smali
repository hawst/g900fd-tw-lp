.class public Lcom/google/api/services/calendar/Calendar$CalendarList;
.super Ljava/lang/Object;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/services/calendar/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CalendarList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;,
        Lcom/google/api/services/calendar/Calendar$CalendarList$List;,
        Lcom/google/api/services/calendar/Calendar$CalendarList$Get;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/api/services/calendar/Calendar;


# direct methods
.method public constructor <init>(Lcom/google/api/services/calendar/Calendar;)V
    .locals 0

    .prologue
    .line 844
    iput-object p1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList;->this$0:Lcom/google/api/services/calendar/Calendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1455
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Lcom/google/api/services/calendar/Calendar$CalendarList$Get;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 954
    new-instance v0, Lcom/google/api/services/calendar/Calendar$CalendarList$Get;

    invoke-direct {v0, p0, p1}, Lcom/google/api/services/calendar/Calendar$CalendarList$Get;-><init>(Lcom/google/api/services/calendar/Calendar$CalendarList;Ljava/lang/String;)V

    .line 955
    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList;->this$0:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v1, v0}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/googleapis/services/AbstractGoogleClientRequest;)V

    .line 956
    return-object v0
.end method

.method public list()Lcom/google/api/services/calendar/Calendar$CalendarList$List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1165
    new-instance v0, Lcom/google/api/services/calendar/Calendar$CalendarList$List;

    invoke-direct {v0, p0}, Lcom/google/api/services/calendar/Calendar$CalendarList$List;-><init>(Lcom/google/api/services/calendar/Calendar$CalendarList;)V

    .line 1166
    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList;->this$0:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v1, v0}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/googleapis/services/AbstractGoogleClientRequest;)V

    .line 1167
    return-object v0
.end method

.method public patch(Ljava/lang/String;Lcom/google/api/services/calendar/model/CalendarListEntry;)Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1326
    new-instance v0, Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/api/services/calendar/Calendar$CalendarList$Patch;-><init>(Lcom/google/api/services/calendar/Calendar$CalendarList;Ljava/lang/String;Lcom/google/api/services/calendar/model/CalendarListEntry;)V

    .line 1327
    iget-object v1, p0, Lcom/google/api/services/calendar/Calendar$CalendarList;->this$0:Lcom/google/api/services/calendar/Calendar;

    invoke-virtual {v1, v0}, Lcom/google/api/services/calendar/Calendar;->initialize(Lcom/google/api/client/googleapis/services/AbstractGoogleClientRequest;)V

    .line 1328
    return-object v0
.end method
