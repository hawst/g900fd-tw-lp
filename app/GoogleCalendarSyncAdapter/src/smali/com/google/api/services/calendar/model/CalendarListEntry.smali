.class public final Lcom/google/api/services/calendar/model/CalendarListEntry;
.super Lcom/google/api/client/json/GenericJson;
.source "CalendarListEntry.java"


# instance fields
.field private accessRole:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private backgroundColor:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private colorId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private hidden:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private selected:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private summary:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private summaryOverride:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private timeZone:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/google/api/client/json/GenericJson;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->clone()Lcom/google/api/services/calendar/model/CalendarListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/api/client/util/GenericData;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->clone()Lcom/google/api/services/calendar/model/CalendarListEntry;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 1

    .prologue
    .line 539
    invoke-super {p0}, Lcom/google/api/client/json/GenericJson;->clone()Lcom/google/api/client/json/GenericJson;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/calendar/model/CalendarListEntry;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/CalendarListEntry;->clone()Lcom/google/api/services/calendar/model/CalendarListEntry;

    move-result-object v0

    return-object v0
.end method

.method public getAccessRole()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->accessRole:Ljava/lang/String;

    return-object v0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->backgroundColor:Ljava/lang/String;

    return-object v0
.end method

.method public getColorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->colorId:Ljava/lang/String;

    return-object v0
.end method

.method public getHidden()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->hidden:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getSelected()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->selected:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getSummaryOverride()Ljava/lang/String;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->summaryOverride:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/json/GenericJson;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/calendar/model/CalendarListEntry;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/util/GenericData;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/calendar/model/CalendarListEntry;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/CalendarListEntry;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 1

    .prologue
    .line 534
    invoke-super {p0, p1, p2}, Lcom/google/api/client/json/GenericJson;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/json/GenericJson;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/calendar/model/CalendarListEntry;

    return-object v0
.end method

.method public setBackgroundColor(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->backgroundColor:Ljava/lang/String;

    .line 203
    return-object p0
.end method

.method public setColorId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->colorId:Ljava/lang/String;

    .line 222
    return-object p0
.end method

.method public setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->summary:Ljava/lang/String;

    .line 495
    return-object p0
.end method

.method public setTimeZone(Ljava/lang/String;)Lcom/google/api/services/calendar/model/CalendarListEntry;
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/api/services/calendar/model/CalendarListEntry;->timeZone:Ljava/lang/String;

    .line 529
    return-object p0
.end method
