.class public final Lcom/google/api/services/calendar/model/Event;
.super Lcom/google/api/client/json/GenericJson;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/calendar/model/Event$Reminders;,
        Lcom/google/api/services/calendar/model/Event$Organizer;,
        Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    }
.end annotation


# instance fields
.field private attendees:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation
.end field

.field private attendeesOmitted:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private colorId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private description:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private end:Lcom/google/api/services/calendar/model/EventDateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private etag:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private extendedProperties:Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private guestsCanInviteOthers:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private guestsCanModify:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private guestsCanSeeOtherGuests:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private htmlLink:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private iCalUID:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private location:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private organizer:Lcom/google/api/services/calendar/model/Event$Organizer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private originalStartTime:Lcom/google/api/services/calendar/model/EventDateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private recurrence:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private recurringEventId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private reminders:Lcom/google/api/services/calendar/model/Event$Reminders;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private sequence:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private start:Lcom/google/api/services/calendar/model/EventDateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private summary:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private transparency:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private updated:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field private visibility:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/api/services/calendar/model/EventAttendee;

    invoke-static {v0}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/api/client/json/GenericJson;-><init>()V

    .line 1876
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/google/api/client/json/GenericJson;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/Event;->clone()Lcom/google/api/services/calendar/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/api/client/util/GenericData;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/Event;->clone()Lcom/google/api/services/calendar/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/calendar/model/Event;
    .locals 1

    .prologue
    .line 1224
    invoke-super {p0}, Lcom/google/api/client/json/GenericJson;->clone()Lcom/google/api/client/json/GenericJson;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/calendar/model/Event;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/api/services/calendar/model/Event;->clone()Lcom/google/api/services/calendar/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public getAttendees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->attendees:Ljava/util/List;

    return-object v0
.end method

.method public getAttendeesOmitted()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->attendeesOmitted:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getColorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->colorId:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEnd()Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->end:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getExtendedProperties()Lcom/google/api/services/calendar/model/Event$ExtendedProperties;
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->extendedProperties:Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    return-object v0
.end method

.method public getGuestsCanInviteOthers()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanInviteOthers:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getGuestsCanModify()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanModify:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getGuestsCanSeeOtherGuests()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanSeeOtherGuests:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getHtmlLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 785
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->htmlLink:Ljava/lang/String;

    return-object v0
.end method

.method public getICalUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->iCalUID:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 853
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getOrganizer()Lcom/google/api/services/calendar/model/Event$Organizer;
    .locals 1

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->organizer:Lcom/google/api/services/calendar/model/Event$Organizer;

    return-object v0
.end method

.method public getOriginalStartTime()Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 1

    .prologue
    .line 943
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->originalStartTime:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object v0
.end method

.method public getRecurrence()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->recurrence:Ljava/util/List;

    return-object v0
.end method

.method public getRecurringEventId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->recurringEventId:Ljava/lang/String;

    return-object v0
.end method

.method public getReminders()Lcom/google/api/services/calendar/model/Event$Reminders;
    .locals 1

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->reminders:Lcom/google/api/services/calendar/model/Event$Reminders;

    return-object v0
.end method

.method public getSequence()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->sequence:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStart()Lcom/google/api/services/calendar/model/EventDateTime;
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->start:Lcom/google/api/services/calendar/model/EventDateTime;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->status:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getTransparency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->transparency:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdated()Lcom/google/api/client/util/DateTime;
    .locals 1

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->updated:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method

.method public getVisibility()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/google/api/services/calendar/model/Event;->visibility:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/json/GenericJson;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/calendar/model/Event;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/util/GenericData;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/calendar/model/Event;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/calendar/model/Event;
    .locals 1

    .prologue
    .line 1219
    invoke-super {p0, p1, p2}, Lcom/google/api/client/json/GenericJson;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/json/GenericJson;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/calendar/model/Event;

    return-object v0
.end method

.method public setAttendees(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/calendar/model/EventAttendee;",
            ">;)",
            "Lcom/google/api/services/calendar/model/Event;"
        }
    .end annotation

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->attendees:Ljava/util/List;

    .line 381
    return-object p0
.end method

.method public setAttendeesOmitted(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->attendeesOmitted:Ljava/lang/Boolean;

    .line 404
    return-object p0
.end method

.method public setColorId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->colorId:Ljava/lang/String;

    .line 452
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 502
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->description:Ljava/lang/String;

    .line 503
    return-object p0
.end method

.method public setEnd(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 521
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->end:Lcom/google/api/services/calendar/model/EventDateTime;

    .line 522
    return-object p0
.end method

.method public setExtendedProperties(Lcom/google/api/services/calendar/model/Event$ExtendedProperties;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->extendedProperties:Lcom/google/api/services/calendar/model/Event$ExtendedProperties;

    .line 603
    return-object p0
.end method

.method public setGuestsCanInviteOthers(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 638
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanInviteOthers:Ljava/lang/Boolean;

    .line 639
    return-object p0
.end method

.method public setGuestsCanModify(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanModify:Ljava/lang/Boolean;

    .line 686
    return-object p0
.end method

.method public setGuestsCanSeeOtherGuests(Ljava/lang/Boolean;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 731
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->guestsCanSeeOtherGuests:Ljava/lang/Boolean;

    .line 732
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->id:Ljava/lang/String;

    .line 828
    return-object p0
.end method

.method public setLocation(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->location:Ljava/lang/String;

    .line 862
    return-object p0
.end method

.method public setOrganizer(Lcom/google/api/services/calendar/model/Event$Organizer;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 932
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->organizer:Lcom/google/api/services/calendar/model/Event$Organizer;

    .line 933
    return-object p0
.end method

.method public setOriginalStartTime(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 953
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->originalStartTime:Lcom/google/api/services/calendar/model/EventDateTime;

    .line 954
    return-object p0
.end method

.method public setRecurrence(Ljava/util/List;)Lcom/google/api/services/calendar/model/Event;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/services/calendar/model/Event;"
        }
    .end annotation

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->recurrence:Ljava/util/List;

    .line 1020
    return-object p0
.end method

.method public setRecurringEventId(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1038
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->recurringEventId:Ljava/lang/String;

    .line 1039
    return-object p0
.end method

.method public setReminders(Lcom/google/api/services/calendar/model/Event$Reminders;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->reminders:Lcom/google/api/services/calendar/model/Event$Reminders;

    .line 1056
    return-object p0
.end method

.method public setSequence(Ljava/lang/Integer;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1072
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->sequence:Ljava/lang/Integer;

    .line 1073
    return-object p0
.end method

.method public setStart(Lcom/google/api/services/calendar/model/EventDateTime;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1112
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->start:Lcom/google/api/services/calendar/model/EventDateTime;

    .line 1113
    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1133
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->status:Ljava/lang/String;

    .line 1134
    return-object p0
.end method

.method public setSummary(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1150
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->summary:Ljava/lang/String;

    .line 1151
    return-object p0
.end method

.method public setTransparency(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1171
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->transparency:Ljava/lang/String;

    .line 1172
    return-object p0
.end method

.method public setVisibility(Ljava/lang/String;)Lcom/google/api/services/calendar/model/Event;
    .locals 0

    .prologue
    .line 1213
    iput-object p1, p0, Lcom/google/api/services/calendar/model/Event;->visibility:Ljava/lang/String;

    .line 1214
    return-object p0
.end method
