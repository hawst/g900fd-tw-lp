.class public Lcom/qti/backupagent/vcard/ExportProcessor;
.super Ljava/lang/Object;
.source "ExportProcessor.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "VCardExport"


# instance fields
.field private volatile mCanceled:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mDone:Z

.field private final mExportRequest:Lcom/qti/backupagent/vcard/ExportRequest;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/qti/backupagent/vcard/ExportRequest;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "exportRequest"    # Lcom/qti/backupagent/vcard/ExportRequest;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mContext:Landroid/content/Context;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mResolver:Landroid/content/ContentResolver;

    .line 63
    iput-object p2, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mExportRequest:Lcom/qti/backupagent/vcard/ExportRequest;

    .line 64
    return-void
.end method

.method private doCancelNotification()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method private doFinishNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 187
    return-void
.end method

.method private doProgressNotification(Landroid/net/Uri;II)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "totalCount"    # I
    .param p3, "currentCount"    # I

    .prologue
    .line 181
    return-void
.end method

.method private runInternal()V
    .locals 23

    .prologue
    .line 88
    const-string v4, "VCardExport"

    const-string v5, "vCard export has started."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qti/backupagent/vcard/ExportProcessor;->mExportRequest:Lcom/qti/backupagent/vcard/ExportRequest;

    move-object/from16 v16, v0

    .line 90
    .local v16, "request":Lcom/qti/backupagent/vcard/ExportRequest;
    const/4 v10, 0x0

    .line 91
    .local v10, "composer":Lcom/android/vcard/VCardComposer;
    const/16 v21, 0x0

    .line 92
    .local v21, "writer":Ljava/io/Writer;
    const/16 v17, 0x0

    .line 94
    .local v17, "successful":Z
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/qti/backupagent/vcard/ExportProcessor;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 95
    const-string v4, "VCardExport"

    const-string v5, "Export request is cancelled before handling the request"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    if-eqz v10, :cond_0

    .line 164
    invoke-virtual {v10}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_0
    if-eqz v21, :cond_1

    .line 168
    :try_start_1
    invoke-virtual/range {v21 .. v21}, Ljava/io/Writer;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    move-object v3, v10

    .line 174
    .end local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .local v3, "composer":Lcom/android/vcard/VCardComposer;
    :goto_1
    return-void

    .line 169
    .end local v3    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v10    # "composer":Lcom/android/vcard/VCardComposer;
    :catch_0
    move-exception v12

    .line 170
    .local v12, "e":Ljava/io/IOException;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    .end local v12    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/qti/backupagent/vcard/ExportRequest;->destUri:Landroid/net/Uri;

    move-object/from16 v19, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    .local v19, "uri":Landroid/net/Uri;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/qti/backupagent/vcard/ExportProcessor;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v15

    .line 109
    .local v15, "outputStream":Ljava/io/OutputStream;
    :try_start_4
    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/qti/backupagent/vcard/ExportRequest;->exportType:Ljava/lang/String;

    .line 111
    .local v14, "exportType":Ljava/lang/String;
    invoke-static {v14}, Lcom/android/vcard/VCardConfig;->getVCardTypeFromString(Ljava/lang/String;)I

    move-result v20

    .line 113
    .local v20, "vcardType":I
    new-instance v3, Lcom/android/vcard/VCardComposer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/qti/backupagent/vcard/ExportProcessor;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    move/from16 v0, v20

    invoke-direct {v3, v4, v0, v5}, Lcom/android/vcard/VCardComposer;-><init>(Landroid/content/Context;IZ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 120
    .end local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v3    # "composer":Lcom/android/vcard/VCardComposer;
    :try_start_5
    new-instance v22, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-direct {v4, v15}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 121
    .end local v21    # "writer":Ljava/io/Writer;
    .local v22, "writer":Ljava/io/Writer;
    :try_start_6
    sget-object v4, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "for_export_only"

    const-string v6, "1"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 125
    .local v9, "contentUriForRawContactsEntity":Landroid/net/Uri;
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v9}, Lcom/android/vcard/VCardComposer;->init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 128
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->getErrorReason()Ljava/lang/String;

    move-result-object v13

    .line 129
    .local v13, "errorReason":Ljava/lang/String;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initialization of vCard composer failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 163
    if-eqz v3, :cond_3

    .line 164
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_3
    if-eqz v22, :cond_4

    .line 168
    :try_start_7
    invoke-virtual/range {v22 .. v22}, Ljava/io/Writer;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_4
    :goto_2
    move-object/from16 v21, v22

    .line 171
    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v21    # "writer":Ljava/io/Writer;
    goto/16 :goto_1

    .line 102
    .end local v3    # "composer":Lcom/android/vcard/VCardComposer;
    .end local v9    # "contentUriForRawContactsEntity":Landroid/net/Uri;
    .end local v13    # "errorReason":Ljava/lang/String;
    .end local v14    # "exportType":Ljava/lang/String;
    .end local v15    # "outputStream":Ljava/io/OutputStream;
    .end local v20    # "vcardType":I
    .restart local v10    # "composer":Lcom/android/vcard/VCardComposer;
    :catch_1
    move-exception v12

    .line 103
    .local v12, "e":Ljava/io/FileNotFoundException;
    :try_start_8
    const-string v4, "VCardExport"

    const-string v5, "FileNotFoundException thrown"

    invoke-static {v4, v5, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 163
    if-eqz v10, :cond_5

    .line 164
    invoke-virtual {v10}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_5
    if-eqz v21, :cond_6

    .line 168
    :try_start_9
    invoke-virtual/range {v21 .. v21}, Ljava/io/Writer;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .end local v12    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_3
    move-object v3, v10

    .line 171
    .end local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v3    # "composer":Lcom/android/vcard/VCardComposer;
    goto/16 :goto_1

    .line 169
    .end local v3    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v12    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v12

    .line 170
    .local v12, "e":Ljava/io/IOException;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 169
    .end local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .end local v12    # "e":Ljava/io/IOException;
    .end local v21    # "writer":Ljava/io/Writer;
    .restart local v3    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v9    # "contentUriForRawContactsEntity":Landroid/net/Uri;
    .restart local v13    # "errorReason":Ljava/lang/String;
    .restart local v14    # "exportType":Ljava/lang/String;
    .restart local v15    # "outputStream":Ljava/io/OutputStream;
    .restart local v20    # "vcardType":I
    .restart local v22    # "writer":Ljava/io/Writer;
    :catch_3
    move-exception v12

    .line 170
    .restart local v12    # "e":Ljava/io/IOException;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 133
    .end local v12    # "e":Ljava/io/IOException;
    .end local v13    # "errorReason":Ljava/lang/String;
    :cond_7
    :try_start_a
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->getCount()I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result v18

    .line 134
    .local v18, "total":I
    if-nez v18, :cond_a

    .line 163
    if-eqz v3, :cond_8

    .line 164
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_8
    if-eqz v22, :cond_9

    .line 168
    :try_start_b
    invoke-virtual/range {v22 .. v22}, Ljava/io/Writer;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    :cond_9
    :goto_4
    move-object/from16 v21, v22

    .line 171
    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v21    # "writer":Ljava/io/Writer;
    goto/16 :goto_1

    .line 169
    .end local v21    # "writer":Ljava/io/Writer;
    .restart local v22    # "writer":Ljava/io/Writer;
    :catch_4
    move-exception v12

    .line 170
    .restart local v12    # "e":Ljava/io/IOException;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 138
    .end local v12    # "e":Ljava/io/IOException;
    :cond_a
    const/4 v11, 0x1

    .line 139
    .local v11, "current":I
    :goto_5
    :try_start_c
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_11

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/qti/backupagent/vcard/ExportProcessor;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 141
    const-string v4, "VCardExport"

    const-string v5, "Export request is cancelled during composing vCard"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 163
    if-eqz v3, :cond_b

    .line 164
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_b
    if-eqz v22, :cond_c

    .line 168
    :try_start_d
    invoke-virtual/range {v22 .. v22}, Ljava/io/Writer;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5

    :cond_c
    :goto_6
    move-object/from16 v21, v22

    .line 171
    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v21    # "writer":Ljava/io/Writer;
    goto/16 :goto_1

    .line 169
    .end local v21    # "writer":Ljava/io/Writer;
    .restart local v22    # "writer":Ljava/io/Writer;
    :catch_5
    move-exception v12

    .line 170
    .restart local v12    # "e":Ljava/io/IOException;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 145
    .end local v12    # "e":Ljava/io/IOException;
    :cond_d
    :try_start_e
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->createOneEntry()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 154
    :try_start_f
    rem-int/lit8 v4, v11, 0x64

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    .line 155
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v11}, Lcom/qti/backupagent/vcard/ExportProcessor;->doProgressNotification(Landroid/net/Uri;II)V

    .line 157
    :cond_e
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 146
    :catch_6
    move-exception v12

    .line 147
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->getErrorReason()Ljava/lang/String;

    move-result-object v13

    .line 148
    .restart local v13    # "errorReason":Ljava/lang/String;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to read a contact: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 163
    if-eqz v3, :cond_f

    .line 164
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_f
    if-eqz v22, :cond_10

    .line 168
    :try_start_10
    invoke-virtual/range {v22 .. v22}, Ljava/io/Writer;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_7

    :cond_10
    :goto_7
    move-object/from16 v21, v22

    .line 171
    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v21    # "writer":Ljava/io/Writer;
    goto/16 :goto_1

    .line 169
    .end local v21    # "writer":Ljava/io/Writer;
    .restart local v22    # "writer":Ljava/io/Writer;
    :catch_7
    move-exception v12

    .line 170
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 159
    .end local v12    # "e":Ljava/io/IOException;
    .end local v13    # "errorReason":Ljava/lang/String;
    :cond_11
    :try_start_11
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Successfully finished exporting vCard "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/qti/backupagent/vcard/ExportRequest;->destUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 161
    const/16 v17, 0x1

    .line 163
    if-eqz v3, :cond_12

    .line 164
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_12
    if-eqz v22, :cond_13

    .line 168
    :try_start_12
    invoke-virtual/range {v22 .. v22}, Ljava/io/Writer;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_8

    :cond_13
    :goto_8
    move-object/from16 v21, v22

    .line 174
    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v21    # "writer":Ljava/io/Writer;
    goto/16 :goto_1

    .line 169
    .end local v21    # "writer":Ljava/io/Writer;
    .restart local v22    # "writer":Ljava/io/Writer;
    :catch_8
    move-exception v12

    .line 170
    .restart local v12    # "e":Ljava/io/IOException;
    const-string v4, "VCardExport"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 163
    .end local v3    # "composer":Lcom/android/vcard/VCardComposer;
    .end local v9    # "contentUriForRawContactsEntity":Landroid/net/Uri;
    .end local v11    # "current":I
    .end local v12    # "e":Ljava/io/IOException;
    .end local v14    # "exportType":Ljava/lang/String;
    .end local v15    # "outputStream":Ljava/io/OutputStream;
    .end local v18    # "total":I
    .end local v19    # "uri":Landroid/net/Uri;
    .end local v20    # "vcardType":I
    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v21    # "writer":Ljava/io/Writer;
    :catchall_0
    move-exception v4

    move-object v3, v10

    .end local v10    # "composer":Lcom/android/vcard/VCardComposer;
    .restart local v3    # "composer":Lcom/android/vcard/VCardComposer;
    :goto_9
    if-eqz v3, :cond_14

    .line 164
    invoke-virtual {v3}, Lcom/android/vcard/VCardComposer;->terminate()V

    .line 166
    :cond_14
    if-eqz v21, :cond_15

    .line 168
    :try_start_13
    invoke-virtual/range {v21 .. v21}, Ljava/io/Writer;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_9

    .line 171
    :cond_15
    :goto_a
    throw v4

    .line 169
    :catch_9
    move-exception v12

    .line 170
    .restart local v12    # "e":Ljava/io/IOException;
    const-string v5, "VCardExport"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException is thrown during close(). Ignored. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 163
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v14    # "exportType":Ljava/lang/String;
    .restart local v15    # "outputStream":Ljava/io/OutputStream;
    .restart local v19    # "uri":Landroid/net/Uri;
    .restart local v20    # "vcardType":I
    :catchall_1
    move-exception v4

    goto :goto_9

    .end local v21    # "writer":Ljava/io/Writer;
    .restart local v22    # "writer":Ljava/io/Writer;
    :catchall_2
    move-exception v4

    move-object/from16 v21, v22

    .end local v22    # "writer":Ljava/io/Writer;
    .restart local v21    # "writer":Ljava/io/Writer;
    goto :goto_9
.end method

.method private translateComposerError(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 177
    return-object p1
.end method


# virtual methods
.method public declared-synchronized cancel(Z)Z
    .locals 3
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    const/4 v0, 0x1

    .line 190
    monitor-enter p0

    :try_start_0
    const-string v1, "VCardExport"

    const-string v2, "received cancel request"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-boolean v1, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mDone:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 192
    :cond_0
    const/4 v0, 0x0

    .line 195
    :goto_0
    monitor-exit p0

    return v0

    .line 194
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mCanceled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getRequest()Lcom/qti/backupagent/vcard/ExportRequest;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mExportRequest:Lcom/qti/backupagent/vcard/ExportRequest;

    return-object v0
.end method

.method public declared-synchronized isCancelled()Z
    .locals 1

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDone()Z
    .locals 1

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 69
    :try_start_0
    invoke-direct {p0}, Lcom/qti/backupagent/vcard/ExportProcessor;->runInternal()V

    .line 71
    invoke-virtual {p0}, Lcom/qti/backupagent/vcard/ExportProcessor;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    invoke-direct {p0}, Lcom/qti/backupagent/vcard/ExportProcessor;->doCancelNotification()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 81
    :cond_0
    monitor-enter p0

    .line 82
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mDone:Z

    .line 83
    monitor-exit p0

    .line 85
    return-void

    .line 83
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v1, "VCardExport"

    const-string v2, "OutOfMemoryError thrown during import"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 81
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v1

    monitor-enter p0

    .line 82
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/qti/backupagent/vcard/ExportProcessor;->mDone:Z

    .line 83
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v1

    .line 77
    :catch_1
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_4
    const-string v1, "VCardExport"

    const-string v2, "RuntimeException thrown during export"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 79
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 83
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method
