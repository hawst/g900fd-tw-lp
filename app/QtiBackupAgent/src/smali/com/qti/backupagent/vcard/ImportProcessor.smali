.class public Lcom/qti/backupagent/vcard/ImportProcessor;
.super Ljava/lang/Object;
.source "ImportProcessor.java"

# interfaces
.implements Lcom/android/vcard/VCardEntryHandler;


# static fields
.field private static final DEBUG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "VCardImport"


# instance fields
.field private volatile mCanceled:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentCount:I

.field private volatile mDone:Z

.field private final mFailedUris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final mImportRequest:Lcom/qti/backupagent/vcard/ImportRequest;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mTotalCount:I

.field private mVCardParser:Lcom/android/vcard/VCardParser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/qti/backupagent/vcard/ImportRequest;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "request"    # Lcom/qti/backupagent/vcard/ImportRequest;

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mFailedUris:Ljava/util/List;

    .line 72
    iput v1, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mCurrentCount:I

    .line 73
    iput v1, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mTotalCount:I

    .line 76
    iput-object p1, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mContext:Landroid/content/Context;

    .line 77
    iget-object v0, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mResolver:Landroid/content/ContentResolver;

    .line 79
    iput-object p2, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mImportRequest:Lcom/qti/backupagent/vcard/ImportRequest;

    .line 80
    return-void
.end method

.method private readOneVCard(Ljava/io/InputStream;ILjava/lang/String;Lcom/android/vcard/VCardInterpreter;[I)Z
    .locals 9
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "vcardType"    # I
    .param p3, "charset"    # Ljava/lang/String;
    .param p4, "interpreter"    # Lcom/android/vcard/VCardInterpreter;
    .param p5, "possibleVCardVersions"    # [I

    .prologue
    .line 186
    const/4 v4, 0x0

    .line 187
    .local v4, "successful":Z
    array-length v3, p5

    .line 188
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 189
    aget v5, p5, v2

    .line 191
    .local v5, "vcardVersion":I
    if-lez v2, :cond_0

    :try_start_0
    instance-of v6, p4, Lcom/android/vcard/VCardEntryConstructor;

    if-eqz v6, :cond_0

    .line 193
    move-object v0, p4

    check-cast v0, Lcom/android/vcard/VCardEntryConstructor;

    move-object v6, v0

    invoke-virtual {v6}, Lcom/android/vcard/VCardEntryConstructor;->clear()V

    .line 200
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/vcard/exception/VCardNestedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/vcard/exception/VCardNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/android/vcard/exception/VCardVersionException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/android/vcard/exception/VCardException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 201
    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    :try_start_1
    new-instance v6, Lcom/android/vcard/VCardParser_V30;

    invoke-direct {v6, p2}, Lcom/android/vcard/VCardParser_V30;-><init>(I)V

    :goto_1
    iput-object v6, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mVCardParser:Lcom/android/vcard/VCardParser;

    .line 204
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :try_start_2
    iget-object v6, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mVCardParser:Lcom/android/vcard/VCardParser;

    invoke-virtual {v6, p1, p4}, Lcom/android/vcard/VCardParser;->parse(Ljava/io/InputStream;Lcom/android/vcard/VCardInterpreter;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/vcard/exception/VCardNestedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/android/vcard/exception/VCardNotSupportedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/android/vcard/exception/VCardVersionException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/android/vcard/exception/VCardException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207
    const/4 v4, 0x1

    .line 224
    if-eqz p1, :cond_1

    .line 226
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9

    .line 233
    .end local v5    # "vcardVersion":I
    :cond_1
    :goto_2
    return v4

    .line 201
    .restart local v5    # "vcardVersion":I
    :cond_2
    :try_start_4
    new-instance v6, Lcom/android/vcard/VCardParser_V21;

    invoke-direct {v6, p2}, Lcom/android/vcard/VCardParser_V21;-><init>(I)V

    goto :goto_1

    .line 204
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v6
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/android/vcard/exception/VCardNestedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/android/vcard/exception/VCardNotSupportedException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/android/vcard/exception/VCardVersionException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lcom/android/vcard/exception/VCardException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/io/IOException;
    :try_start_6
    const-string v6, "VCardImport"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException was emitted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 224
    if-eqz p1, :cond_3

    .line 226
    :try_start_7
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_a

    .line 188
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 211
    :catch_1
    move-exception v1

    .line 212
    .local v1, "e":Lcom/android/vcard/exception/VCardNestedException;
    :try_start_8
    const-string v6, "VCardImport"

    const-string v7, "Nested Exception is found."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 224
    if-eqz p1, :cond_3

    .line 226
    :try_start_9
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_3

    .line 227
    :catch_2
    move-exception v6

    goto :goto_3

    .line 213
    .end local v1    # "e":Lcom/android/vcard/exception/VCardNestedException;
    :catch_3
    move-exception v1

    .line 214
    .local v1, "e":Lcom/android/vcard/exception/VCardNotSupportedException;
    :try_start_a
    const-string v6, "VCardImport"

    invoke-virtual {v1}, Lcom/android/vcard/exception/VCardNotSupportedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 224
    if-eqz p1, :cond_3

    .line 226
    :try_start_b
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_3

    .line 227
    :catch_4
    move-exception v6

    goto :goto_3

    .line 215
    .end local v1    # "e":Lcom/android/vcard/exception/VCardNotSupportedException;
    :catch_5
    move-exception v1

    .line 216
    .local v1, "e":Lcom/android/vcard/exception/VCardVersionException;
    add-int/lit8 v6, v3, -0x1

    if-ne v2, v6, :cond_4

    .line 217
    :try_start_c
    const-string v6, "VCardImport"

    const-string v7, "Appropriate version for this vCard is not found."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 224
    :cond_4
    if-eqz p1, :cond_3

    .line 226
    :try_start_d
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_3

    .line 227
    :catch_6
    move-exception v6

    goto :goto_3

    .line 221
    .end local v1    # "e":Lcom/android/vcard/exception/VCardVersionException;
    :catch_7
    move-exception v1

    .line 222
    .local v1, "e":Lcom/android/vcard/exception/VCardException;
    :try_start_e
    const-string v6, "VCardImport"

    invoke-virtual {v1}, Lcom/android/vcard/exception/VCardException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 224
    if-eqz p1, :cond_3

    .line 226
    :try_start_f
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    goto :goto_3

    .line 227
    :catch_8
    move-exception v6

    goto :goto_3

    .line 224
    .end local v1    # "e":Lcom/android/vcard/exception/VCardException;
    :catchall_1
    move-exception v6

    if-eqz p1, :cond_5

    .line 226
    :try_start_10
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    .line 228
    :cond_5
    :goto_4
    throw v6

    .line 227
    :catch_9
    move-exception v6

    goto :goto_2

    .local v1, "e":Ljava/io/IOException;
    :catch_a
    move-exception v6

    goto :goto_3

    .end local v1    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v7

    goto :goto_4
.end method

.method private runInternal()V
    .locals 17

    .prologue
    .line 112
    const-string v1, "VCardImport"

    const-string v15, "vCard import has started."

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/qti/backupagent/vcard/ImportProcessor;->mImportRequest:Lcom/qti/backupagent/vcard/ImportRequest;

    .line 115
    .local v12, "request":Lcom/qti/backupagent/vcard/ImportRequest;
    iget v1, v12, Lcom/qti/backupagent/vcard/ImportRequest;->vcardVersion:I

    if-nez v1, :cond_3

    .line 120
    const/4 v1, 0x2

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    .line 130
    .local v6, "possibleVCardVersions":[I
    :goto_0
    iget-object v14, v12, Lcom/qti/backupagent/vcard/ImportRequest;->uri:Landroid/net/Uri;

    .line 131
    .local v14, "uri":Landroid/net/Uri;
    iget-object v7, v12, Lcom/qti/backupagent/vcard/ImportRequest;->account:Landroid/accounts/Account;

    .line 132
    .local v7, "account":Landroid/accounts/Account;
    iget v3, v12, Lcom/qti/backupagent/vcard/ImportRequest;->estimatedVCardType:I

    .line 133
    .local v3, "estimatedVCardType":I
    iget-object v4, v12, Lcom/qti/backupagent/vcard/ImportRequest;->estimatedCharset:Ljava/lang/String;

    .line 134
    .local v4, "estimatedCharset":Ljava/lang/String;
    iget v10, v12, Lcom/qti/backupagent/vcard/ImportRequest;->entryCount:I

    .line 135
    .local v10, "entryCount":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/qti/backupagent/vcard/ImportProcessor;->mTotalCount:I

    add-int/2addr v1, v10

    move-object/from16 v0, p0

    iput v1, v0, Lcom/qti/backupagent/vcard/ImportProcessor;->mTotalCount:I

    .line 137
    new-instance v5, Lcom/android/vcard/VCardEntryConstructor;

    invoke-direct {v5, v3, v7, v4}, Lcom/android/vcard/VCardEntryConstructor;-><init>(ILandroid/accounts/Account;Ljava/lang/String;)V

    .line 139
    .local v5, "constructor":Lcom/android/vcard/VCardEntryConstructor;
    new-instance v8, Lcom/android/vcard/VCardEntryCommitter;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/qti/backupagent/vcard/ImportProcessor;->mResolver:Landroid/content/ContentResolver;

    invoke-direct {v8, v1}, Lcom/android/vcard/VCardEntryCommitter;-><init>(Landroid/content/ContentResolver;)V

    .line 140
    .local v8, "committer":Lcom/android/vcard/VCardEntryCommitter;
    invoke-virtual {v5, v8}, Lcom/android/vcard/VCardEntryConstructor;->addEntryHandler(Lcom/android/vcard/VCardEntryHandler;)V

    .line 141
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/android/vcard/VCardEntryConstructor;->addEntryHandler(Lcom/android/vcard/VCardEntryHandler;)V

    .line 143
    const/4 v2, 0x0

    .line 144
    .local v2, "is":Ljava/io/InputStream;
    const/4 v13, 0x0

    .line 146
    .local v13, "successful":Z
    if-eqz v14, :cond_4

    .line 147
    :try_start_0
    const-string v1, "VCardImport"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "start importing one vCard (Uri: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/qti/backupagent/vcard/ImportProcessor;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v1, v14}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 154
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    move-object/from16 v1, p0

    .line 155
    invoke-direct/range {v1 .. v6}, Lcom/qti/backupagent/vcard/ImportProcessor;->readOneVCard(Ljava/io/InputStream;ILjava/lang/String;Lcom/android/vcard/VCardInterpreter;[I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    .line 161
    :cond_1
    if-eqz v2, :cond_2

    .line 163
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 172
    :cond_2
    :goto_2
    if-eqz v13, :cond_6

    .line 175
    const-string v1, "VCardImport"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Successfully finished importing one vCard file: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-virtual {v8}, Lcom/android/vcard/VCardEntryCommitter;->getCreatedUris()Ljava/util/ArrayList;

    .line 181
    :goto_3
    return-void

    .line 125
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "estimatedVCardType":I
    .end local v4    # "estimatedCharset":Ljava/lang/String;
    .end local v5    # "constructor":Lcom/android/vcard/VCardEntryConstructor;
    .end local v6    # "possibleVCardVersions":[I
    .end local v7    # "account":Landroid/accounts/Account;
    .end local v8    # "committer":Lcom/android/vcard/VCardEntryCommitter;
    .end local v10    # "entryCount":I
    .end local v13    # "successful":Z
    .end local v14    # "uri":Landroid/net/Uri;
    :cond_3
    const/4 v1, 0x1

    new-array v6, v1, [I

    const/4 v1, 0x0

    iget v15, v12, Lcom/qti/backupagent/vcard/ImportRequest;->vcardVersion:I

    aput v15, v6, v1

    .restart local v6    # "possibleVCardVersions":[I
    goto/16 :goto_0

    .line 149
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "estimatedVCardType":I
    .restart local v4    # "estimatedCharset":Ljava/lang/String;
    .restart local v5    # "constructor":Lcom/android/vcard/VCardEntryConstructor;
    .restart local v7    # "account":Landroid/accounts/Account;
    .restart local v8    # "committer":Lcom/android/vcard/VCardEntryCommitter;
    .restart local v10    # "entryCount":I
    .restart local v13    # "successful":Z
    .restart local v14    # "uri":Landroid/net/Uri;
    :cond_4
    :try_start_2
    iget-object v1, v12, Lcom/qti/backupagent/vcard/ImportRequest;->data:[B

    if-eqz v1, :cond_0

    .line 150
    const-string v1, "VCardImport"

    const-string v15, "start importing one vCard (byte[])"

    invoke-static {v1, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    new-instance v11, Ljava/io/ByteArrayInputStream;

    iget-object v1, v12, Lcom/qti/backupagent/vcard/ImportRequest;->data:[B

    invoke-direct {v11, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v2    # "is":Ljava/io/InputStream;
    .local v11, "is":Ljava/io/InputStream;
    move-object v2, v11

    .end local v11    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto :goto_1

    .line 158
    :catch_0
    move-exception v9

    .line 159
    .local v9, "e":Ljava/io/IOException;
    const/4 v13, 0x0

    .line 161
    if-eqz v2, :cond_2

    .line 163
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 164
    :catch_1
    move-exception v1

    goto :goto_2

    .line 161
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    if-eqz v2, :cond_5

    .line 163
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 166
    :cond_5
    :goto_4
    throw v1

    .line 178
    :cond_6
    const-string v1, "VCardImport"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Failed to read one vCard file: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/qti/backupagent/vcard/ImportProcessor;->mFailedUris:Ljava/util/List;

    invoke-interface {v1, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 164
    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v15

    goto :goto_4

    .line 120
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method


# virtual methods
.method public onEnd()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onEntryCreated(Lcom/android/vcard/VCardEntry;)V
    .locals 1
    .param p1, "entry"    # Lcom/android/vcard/VCardEntry;

    .prologue
    .line 91
    iget v0, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mCurrentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mCurrentCount:I

    .line 92
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 97
    :try_start_0
    invoke-direct {p0}, Lcom/qti/backupagent/vcard/ImportProcessor;->runInternal()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 105
    monitor-enter p0

    .line 106
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mDone:Z

    .line 107
    monitor-exit p0

    .line 109
    return-void

    .line 107
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v1, "VCardImport"

    const-string v2, "OutOfMemoryError thrown during import"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 100
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 105
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v1

    monitor-enter p0

    .line 106
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/qti/backupagent/vcard/ImportProcessor;->mDone:Z

    .line 107
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v1

    .line 101
    :catch_1
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_4
    const-string v1, "VCardImport"

    const-string v2, "RuntimeException thrown during import"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 103
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 107
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method
