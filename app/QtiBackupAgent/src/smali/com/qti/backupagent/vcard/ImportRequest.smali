.class public Lcom/qti/backupagent/vcard/ImportRequest;
.super Ljava/lang/Object;
.source "ImportRequest.java"


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final data:[B

.field public final displayName:Ljava/lang/String;

.field public final entryCount:I

.field public final estimatedCharset:Ljava/lang/String;

.field public final estimatedVCardType:I

.field public final uri:Landroid/net/Uri;

.field public final vcardVersion:I


# direct methods
.method public constructor <init>(Landroid/accounts/Account;[BLandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "data"    # [B
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "displayName"    # Ljava/lang/String;
    .param p5, "estimatedType"    # I
    .param p6, "estimatedCharset"    # Ljava/lang/String;
    .param p7, "vcardVersion"    # I
    .param p8, "entryCount"    # I

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/qti/backupagent/vcard/ImportRequest;->account:Landroid/accounts/Account;

    .line 111
    iput-object p2, p0, Lcom/qti/backupagent/vcard/ImportRequest;->data:[B

    .line 112
    iput-object p3, p0, Lcom/qti/backupagent/vcard/ImportRequest;->uri:Landroid/net/Uri;

    .line 113
    iput-object p4, p0, Lcom/qti/backupagent/vcard/ImportRequest;->displayName:Ljava/lang/String;

    .line 114
    iput p5, p0, Lcom/qti/backupagent/vcard/ImportRequest;->estimatedVCardType:I

    .line 115
    iput-object p6, p0, Lcom/qti/backupagent/vcard/ImportRequest;->estimatedCharset:Ljava/lang/String;

    .line 116
    iput p7, p0, Lcom/qti/backupagent/vcard/ImportRequest;->vcardVersion:I

    .line 117
    iput p8, p0, Lcom/qti/backupagent/vcard/ImportRequest;->entryCount:I

    .line 118
    return-void
.end method
