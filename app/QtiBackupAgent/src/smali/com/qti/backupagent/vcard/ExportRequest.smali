.class public Lcom/qti/backupagent/vcard/ExportRequest;
.super Ljava/lang/Object;
.source "ExportRequest.java"


# instance fields
.field public final destUri:Landroid/net/Uri;

.field public final exportType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "destUri"    # Landroid/net/Uri;

    .prologue
    .line 34
    const-string v0, "default"

    invoke-direct {p0, p1, v0}, Lcom/qti/backupagent/vcard/ExportRequest;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p1, "destUri"    # Landroid/net/Uri;
    .param p2, "exportType"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/qti/backupagent/vcard/ExportRequest;->destUri:Landroid/net/Uri;

    .line 39
    iput-object p2, p0, Lcom/qti/backupagent/vcard/ExportRequest;->exportType:Ljava/lang/String;

    .line 40
    return-void
.end method
