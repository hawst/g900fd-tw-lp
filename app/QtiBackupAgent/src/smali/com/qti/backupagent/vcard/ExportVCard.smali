.class public Lcom/qti/backupagent/vcard/ExportVCard;
.super Ljava/lang/Object;
.source "ExportVCard.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static constructExportRequest(Ljava/lang/String;)Lcom/qti/backupagent/vcard/ExportRequest;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 33
    .local v0, "mDestinationUri":Landroid/net/Uri;
    new-instance v1, Lcom/qti/backupagent/vcard/ExportRequest;

    invoke-direct {v1, v0}, Lcom/qti/backupagent/vcard/ExportRequest;-><init>(Landroid/net/Uri;)V

    return-object v1
.end method
