.class public Lcom/qti/backupagent/QtiBackupAgent;
.super Landroid/app/backup/BackupAgentHelper;
.source "QtiBackupAgent.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QtiBackupAgent"


# instance fields
.field private final DEBUG:Z

.field editor:Landroid/content/SharedPreferences$Editor;

.field settings:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/backup/BackupAgentHelper;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qti/backupagent/QtiBackupAgent;->DEBUG:Z

    return-void
.end method

.method private doBackup()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/qti/backupagent/QtiBackupContacts;

    const-string v1, "contacts.vcf"

    invoke-direct {v0, p0, v1}, Lcom/qti/backupagent/QtiBackupContacts;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    .local v0, "contactsBackup":Lcom/qti/backupagent/QtiBackupContacts;
    invoke-virtual {v0}, Lcom/qti/backupagent/QtiBackupContacts;->backupContacts()V

    .line 32
    return-void
.end method

.method private doRestore(Ljava/io/File;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 37
    const-string v1, "contacts.vcf"

    .line 38
    .local v1, "contactsFile":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    new-instance v0, Lcom/qti/backupagent/QtiBackupContacts;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/qti/backupagent/QtiBackupContacts;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    .local v0, "contactsBackup":Lcom/qti/backupagent/QtiBackupContacts;
    invoke-virtual {v0, p1}, Lcom/qti/backupagent/QtiBackupContacts;->restoreContacts(Ljava/io/File;)V

    .line 42
    .end local v0    # "contactsBackup":Lcom/qti/backupagent/QtiBackupContacts;
    :cond_0
    return-void
.end method


# virtual methods
.method public onFullBackup(Landroid/app/backup/FullBackupDataOutput;)V
    .locals 0
    .param p1, "data"    # Landroid/app/backup/FullBackupDataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/qti/backupagent/QtiBackupAgent;->doBackup()V

    .line 47
    invoke-super {p0, p1}, Landroid/app/backup/BackupAgentHelper;->onFullBackup(Landroid/app/backup/FullBackupDataOutput;)V

    .line 48
    return-void
.end method

.method public onRestoreFile(Landroid/os/ParcelFileDescriptor;JLjava/io/File;IJJ)V
    .locals 2
    .param p1, "data"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "size"    # J
    .param p4, "destination"    # Ljava/io/File;
    .param p5, "type"    # I
    .param p6, "mode"    # J
    .param p8, "mtime"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    const-string v0, "com.qti.backupagent.xml"

    .line 56
    .local v0, "settingsFile":Ljava/lang/String;
    invoke-virtual {p4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-super/range {p0 .. p9}, Landroid/app/backup/BackupAgentHelper;->onRestoreFile(Landroid/os/ParcelFileDescriptor;JLjava/io/File;IJJ)V

    .line 60
    invoke-direct {p0, p4}, Lcom/qti/backupagent/QtiBackupAgent;->doRestore(Ljava/io/File;)V

    goto :goto_0
.end method
