.class public Lcom/qti/backupagent/QtiBackupContacts;
.super Ljava/lang/Object;
.source "QtiBackupContacts.java"


# instance fields
.field private final DEBUG:Z

.field private final TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mfile:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vfile"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qti/backupagent/QtiBackupContacts;->DEBUG:Z

    .line 30
    const-string v0, "QtiBackupContacts"

    iput-object v0, p0, Lcom/qti/backupagent/QtiBackupContacts;->TAG:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/qti/backupagent/QtiBackupContacts;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/qti/backupagent/QtiBackupContacts;->mfile:Ljava/lang/String;

    .line 35
    return-void
.end method

.method private exportRequest(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {p1}, Lcom/qti/backupagent/vcard/ExportVCard;->constructExportRequest(Ljava/lang/String;)Lcom/qti/backupagent/vcard/ExportRequest;

    move-result-object v1

    .line 44
    .local v1, "request":Lcom/qti/backupagent/vcard/ExportRequest;
    new-instance v0, Lcom/qti/backupagent/vcard/ExportProcessor;

    iget-object v2, p0, Lcom/qti/backupagent/QtiBackupContacts;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/qti/backupagent/vcard/ExportProcessor;-><init>(Landroid/content/Context;Lcom/qti/backupagent/vcard/ExportRequest;)V

    .line 45
    .local v0, "ep":Lcom/qti/backupagent/vcard/ExportProcessor;
    invoke-virtual {v0}, Lcom/qti/backupagent/vcard/ExportProcessor;->run()V

    .line 46
    return-void
.end method

.method private importRequest(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 55
    :try_start_0
    iget-object v2, p0, Lcom/qti/backupagent/QtiBackupContacts;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, v4}, Lcom/qti/backupagent/vcard/ImportVCard;->constructImportRequest(Landroid/content/Context;[BLandroid/net/Uri;Ljava/lang/String;)Lcom/qti/backupagent/vcard/ImportRequest;

    move-result-object v1

    .line 56
    .local v1, "request":Lcom/qti/backupagent/vcard/ImportRequest;
    new-instance v0, Lcom/qti/backupagent/vcard/ImportProcessor;

    iget-object v2, p0, Lcom/qti/backupagent/QtiBackupContacts;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/qti/backupagent/vcard/ImportProcessor;-><init>(Landroid/content/Context;Lcom/qti/backupagent/vcard/ImportRequest;)V

    .line 57
    .local v0, "ip":Lcom/qti/backupagent/vcard/ImportProcessor;
    invoke-virtual {v0}, Lcom/qti/backupagent/vcard/ImportProcessor;->run()V
    :try_end_0
    .catch Lcom/android/vcard/exception/VCardException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v0    # "ip":Lcom/qti/backupagent/vcard/ImportProcessor;
    .end local v1    # "request":Lcom/qti/backupagent/vcard/ImportRequest;
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v2

    goto :goto_0

    .line 59
    :catch_1
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public backupContacts()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/qti/backupagent/QtiBackupContacts;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/qti/backupagent/QtiBackupContacts;->mfile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/qti/backupagent/QtiBackupContacts;->exportRequest(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public restoreContacts(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 49
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/qti/backupagent/QtiBackupContacts;->importRequest(Landroid/net/Uri;)V

    .line 50
    return-void
.end method
