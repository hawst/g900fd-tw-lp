.class public Lcom/qti/backupagent/QtiBackupActivity;
.super Landroid/app/Activity;
.source "QtiBackupActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final CONTACTS:Ljava/lang/String; = "CONTACTS"

.field public static final CONTACTS_FILE:Ljava/lang/String; = "contacts.vcf"

.field public static final PREFS_NAME:Ljava/lang/String; = "com.qti.backupagent"

.field private static final TAG:Ljava/lang/String; = "QtiBackupActivity"


# instance fields
.field editor:Landroid/content/SharedPreferences$Editor;

.field settings:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method getSettingsTokenById(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 38
    packed-switch p1, :pswitch_data_0

    .line 42
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 40
    :pswitch_0
    const-string v0, "CONTACTS"

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x7f070000
        :pswitch_0
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 70
    :pswitch_0
    const/high16 v0, 0x7f070000

    invoke-virtual {p0, v0, p2}, Lcom/qti/backupagent/QtiBackupActivity;->saveSettingsById(IZ)V

    .line 71
    if-nez p2, :cond_0

    .line 72
    const-string v0, "contacts.vcf"

    invoke-virtual {p0, v0}, Lcom/qti/backupagent/QtiBackupActivity;->deleteFile(Ljava/lang/String;)Z

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x7f070000
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/qti/backupagent/QtiBackupActivity;->setContentView(I)V

    .line 30
    invoke-virtual {p0}, Lcom/qti/backupagent/QtiBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.qti.backupagent"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/qti/backupagent/QtiBackupActivity;->settings:Landroid/content/SharedPreferences;

    .line 31
    iget-object v1, p0, Lcom/qti/backupagent/QtiBackupActivity;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, p0, Lcom/qti/backupagent/QtiBackupActivity;->editor:Landroid/content/SharedPreferences$Editor;

    .line 33
    const/high16 v1, 0x7f070000

    invoke-virtual {p0, v1}, Lcom/qti/backupagent/QtiBackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 34
    .local v0, "contactsSwitch":Landroid/widget/Switch;
    invoke-virtual {p0, v0}, Lcom/qti/backupagent/QtiBackupActivity;->switchConfig(Landroid/widget/Switch;)V

    .line 35
    return-void
.end method

.method saveSettingsById(IZ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "value"    # Z

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/qti/backupagent/QtiBackupActivity;->getSettingsTokenById(I)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "token":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 60
    iget-object v1, p0, Lcom/qti/backupagent/QtiBackupActivity;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 61
    iget-object v1, p0, Lcom/qti/backupagent/QtiBackupActivity;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 64
    :cond_0
    return-void
.end method

.method switchConfig(Landroid/widget/Switch;)V
    .locals 4
    .param p1, "s"    # Landroid/widget/Switch;

    .prologue
    .line 47
    if-eqz p1, :cond_1

    .line 48
    invoke-virtual {p1}, Landroid/widget/Switch;->getId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/qti/backupagent/QtiBackupActivity;->getSettingsTokenById(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "token":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 50
    iget-object v2, p0, Lcom/qti/backupagent/QtiBackupActivity;->settings:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 51
    .local v0, "isChecked":Z
    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 53
    .end local v0    # "isChecked":Z
    :cond_0
    invoke-virtual {p1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 55
    .end local v1    # "token":Ljava/lang/String;
    :cond_1
    return-void
.end method
