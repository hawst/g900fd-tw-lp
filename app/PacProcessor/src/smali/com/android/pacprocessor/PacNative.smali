.class public Lcom/android/pacprocessor/PacNative;
.super Ljava/lang/Object;
.source "PacNative.java"


# instance fields
.field private mIsActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "jni_pacprocessor"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method private native createV8ParserNativeLocked()Z
.end method

.method private native destroyV8ParserNativeLocked()Z
.end method

.method private native makeProxyRequestNativeLocked(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native setProxyScriptNativeLocked(Ljava/lang/String;)Z
.end method


# virtual methods
.method public declared-synchronized makeProxyRequest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "host"    # Ljava/lang/String;

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/android/pacprocessor/PacNative;->makeProxyRequestNativeLocked(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "ret":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 77
    :cond_0
    const-string v1, "PacProxy"

    const-string v2, "v8 Proxy request failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    const/4 v0, 0x0

    .line 80
    :cond_1
    monitor-exit p0

    return-object v0

    .line 75
    .end local v0    # "ret":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setCurrentProxyScript(Ljava/lang/String;)Z
    .locals 2
    .param p1, "script"    # Ljava/lang/String;

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/pacprocessor/PacNative;->setProxyScriptNativeLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "PacProxy"

    const-string v1, "Unable to parse proxy script."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    const/4 v0, 0x1

    .line 71
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startPacSupport()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 47
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/pacprocessor/PacNative;->createV8ParserNativeLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const-string v1, "PacProxy"

    const-string v2, "Unable to Create v8 Proxy Parser."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :goto_0
    monitor-exit p0

    return v0

    .line 51
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/android/pacprocessor/PacNative;->mIsActive:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    const/4 v0, 0x0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopPacSupport()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 56
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/android/pacprocessor/PacNative;->mIsActive:Z

    if-eqz v1, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/android/pacprocessor/PacNative;->destroyV8ParserNativeLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    const-string v0, "PacProxy"

    const-string v1, "Unable to Destroy v8 Proxy Parser."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    const/4 v0, 0x1

    .line 63
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 61
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/android/pacprocessor/PacNative;->mIsActive:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
