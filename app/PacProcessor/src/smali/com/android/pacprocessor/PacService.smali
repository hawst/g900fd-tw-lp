.class public Lcom/android/pacprocessor/PacService;
.super Landroid/app/Service;
.source "PacService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/pacprocessor/PacService$ProxyServiceStub;
    }
.end annotation


# instance fields
.field private mPacNative:Lcom/android/pacprocessor/PacNative;

.field private mStub:Lcom/android/pacprocessor/PacService$ProxyServiceStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 65
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/android/pacprocessor/PacNative;

    invoke-direct {v0}, Lcom/android/pacprocessor/PacNative;-><init>()V

    iput-object v0, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    .line 60
    new-instance v0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;

    iget-object v1, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-direct {v0, v1}, Lcom/android/pacprocessor/PacService$ProxyServiceStub;-><init>(Lcom/android/pacprocessor/PacNative;)V

    iput-object v0, p0, Lcom/android/pacprocessor/PacService;->mStub:Lcom/android/pacprocessor/PacService$ProxyServiceStub;

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/android/pacprocessor/PacService;->mStub:Lcom/android/pacprocessor/PacService$ProxyServiceStub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 40
    iget-object v0, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/android/pacprocessor/PacNative;

    invoke-direct {v0}, Lcom/android/pacprocessor/PacNative;-><init>()V

    iput-object v0, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    .line 42
    new-instance v0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;

    iget-object v1, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-direct {v0, v1}, Lcom/android/pacprocessor/PacService$ProxyServiceStub;-><init>(Lcom/android/pacprocessor/PacNative;)V

    iput-object v0, p0, Lcom/android/pacprocessor/PacService;->mStub:Lcom/android/pacprocessor/PacService$ProxyServiceStub;

    .line 44
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 49
    iget-object v0, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-virtual {v0}, Lcom/android/pacprocessor/PacNative;->stopPacSupport()Z

    .line 51
    iput-object v1, p0, Lcom/android/pacprocessor/PacService;->mPacNative:Lcom/android/pacprocessor/PacNative;

    .line 52
    iput-object v1, p0, Lcom/android/pacprocessor/PacService;->mStub:Lcom/android/pacprocessor/PacService$ProxyServiceStub;

    .line 54
    :cond_0
    return-void
.end method
