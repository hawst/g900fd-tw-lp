.class Lcom/android/pacprocessor/PacService$ProxyServiceStub;
.super Lcom/android/net/IProxyService$Stub;
.source "PacService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/pacprocessor/PacService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProxyServiceStub"
.end annotation


# instance fields
.field private final mPacNative:Lcom/android/pacprocessor/PacNative;


# direct methods
.method public constructor <init>(Lcom/android/pacprocessor/PacNative;)V
    .locals 0
    .param p1, "pacNative"    # Lcom/android/pacprocessor/PacNative;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/android/net/IProxyService$Stub;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;->mPacNative:Lcom/android/pacprocessor/PacNative;

    .line 70
    return-void
.end method


# virtual methods
.method public resolvePacFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 76
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-char v1, v0, v3

    .line 78
    .local v1, "c":C
    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-nez v5, :cond_0

    const/16 v5, 0x2e

    if-eq v1, v5, :cond_0

    const/16 v5, 0x2d

    if-eq v1, v5, :cond_0

    .line 79
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Invalid host was passed"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .end local v0    # "arr$":[C
    .end local v1    # "c":C
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catch_0
    move-exception v2

    .line 84
    .local v2, "e":Ljava/net/MalformedURLException;
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Invalid URL was passed"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 77
    .end local v2    # "e":Ljava/net/MalformedURLException;
    .restart local v0    # "arr$":[C
    .restart local v1    # "c":C
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 82
    .end local v1    # "c":C
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-virtual {v5, p2, p1}, Lcom/android/pacprocessor/PacNative;->makeProxyRequest(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    return-object v5
.end method

.method public setPacFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "script"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 91
    const-string v0, "PacService"

    const-string v1, "Only system user is allowed to call setPacFile"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-virtual {v0, p1}, Lcom/android/pacprocessor/PacNative;->setCurrentProxyScript(Ljava/lang/String;)Z

    .line 95
    return-void
.end method

.method public startPacSystem()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 100
    const-string v0, "PacService"

    const-string v1, "Only system user is allowed to call startPacSystem"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-virtual {v0}, Lcom/android/pacprocessor/PacNative;->startPacSupport()Z

    .line 104
    return-void
.end method

.method public stopPacSystem()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 109
    const-string v0, "PacService"

    const-string v1, "Only system user is allowed to call stopPacSystem"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/android/pacprocessor/PacService$ProxyServiceStub;->mPacNative:Lcom/android/pacprocessor/PacNative;

    invoke-virtual {v0}, Lcom/android/pacprocessor/PacNative;->stopPacSupport()Z

    .line 113
    return-void
.end method
