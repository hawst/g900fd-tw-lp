.class public Lcom/android/dreams/phototable/PhotoTableDream;
.super Landroid/service/dreams/DreamService;
.source "PhotoTableDream.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 50
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    .line 51
    const-string v6, "PhotoTableDream"

    invoke-virtual {p0, v6, v7}, Lcom/android/dreams/phototable/PhotoTableDream;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-static {v6}, Lcom/android/dreams/phototable/AlbumSettings;->getAlbumSettings(Landroid/content/SharedPreferences;)Lcom/android/dreams/phototable/AlbumSettings;

    move-result-object v4

    .line 53
    .local v4, "settings":Lcom/android/dreams/phototable/AlbumSettings;
    new-instance v3, Lcom/android/dreams/phototable/PhotoSourcePlexor;

    const-string v6, "FlipperDream"

    invoke-virtual {p0, v6, v7}, Lcom/android/dreams/phototable/PhotoTableDream;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-direct {v3, p0, v6}, Lcom/android/dreams/phototable/PhotoSourcePlexor;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 54
    .local v3, "plexor":Lcom/android/dreams/phototable/PhotoSourcePlexor;
    invoke-virtual {v3}, Lcom/android/dreams/phototable/PhotoSourcePlexor;->findAlbums()Ljava/util/Collection;

    move-result-object v2

    .line 55
    .local v2, "mlist":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/dreams/phototable/PhotoSource$AlbumData;>;"
    new-instance v5, Ljava/util/HashSet;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 56
    .local v5, "validAlbumIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 57
    .local v0, "albumData":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    iget-object v6, v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    .end local v0    # "albumData":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    :cond_0
    invoke-virtual {v4, v5}, Lcom/android/dreams/phototable/AlbumSettings;->pruneObsoleteSettings(Ljava/util/Collection;)V

    .line 60
    invoke-virtual {v4}, Lcom/android/dreams/phototable/AlbumSettings;->isConfigured()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 61
    const v6, 0x7f040006

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/PhotoTableDream;->setContentView(I)V

    .line 65
    :goto_1
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/PhotoTableDream;->setFullscreen(Z)V

    .line 66
    return-void

    .line 63
    :cond_1
    const v6, 0x7f040001

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/PhotoTableDream;->setContentView(I)V

    goto :goto_1
.end method

.method public onDreamingStarted()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 30
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStarted()V

    .line 32
    const v3, 0x7f0b0007

    invoke-virtual {p0, v3}, Lcom/android/dreams/phototable/PhotoTableDream;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/BummerView;

    .line 33
    .local v0, "bummer":Lcom/android/dreams/phototable/BummerView;
    if-eqz v0, :cond_0

    .line 34
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/dreams/phototable/PhotoTableDream;->setInteractive(Z)V

    .line 35
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTableDream;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 36
    .local v1, "resources":Landroid/content/res/Resources;
    const/high16 v3, 0x7f080000

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const v4, 0x7f080001

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-virtual {v0, v5, v3, v4}, Lcom/android/dreams/phototable/BummerView;->setAnimationParams(ZII)V

    .line 41
    .end local v1    # "resources":Landroid/content/res/Resources;
    :cond_0
    const v3, 0x7f0b000e

    invoke-virtual {p0, v3}, Lcom/android/dreams/phototable/PhotoTableDream;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/dreams/phototable/PhotoTable;

    .line 42
    .local v2, "table":Lcom/android/dreams/phototable/PhotoTable;
    if-eqz v2, :cond_1

    .line 43
    invoke-virtual {p0, v5}, Lcom/android/dreams/phototable/PhotoTableDream;->setInteractive(Z)V

    .line 44
    invoke-virtual {v2, p0}, Lcom/android/dreams/phototable/PhotoTable;->setDream(Landroid/service/dreams/DreamService;)V

    .line 46
    :cond_1
    return-void
.end method
