.class Lcom/android/dreams/phototable/FlipperDreamSettings$1;
.super Ljava/lang/Object;
.source "FlipperDreamSettings.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dreams/phototable/FlipperDreamSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;


# direct methods
.method constructor <init>(Lcom/android/dreams/phototable/FlipperDreamSettings;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 2
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 61
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$000(Lcom/android/dreams/phototable/FlipperDreamSettings;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 62
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/4 v5, 0x0

    .line 66
    iget-object v4, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    invoke-virtual {v4}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 67
    iget-object v4, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    iget-object v4, v4, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSettings:Landroid/content/SharedPreferences;

    invoke-static {v4}, Lcom/android/dreams/phototable/AlbumSettings;->getAlbumSettings(Landroid/content/SharedPreferences;)Lcom/android/dreams/phototable/AlbumSettings;

    move-result-object v0

    .line 68
    .local v0, "albumsettings":Lcom/android/dreams/phototable/AlbumSettings;
    iget-object v4, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;
    invoke-static {v4}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$000(Lcom/android/dreams/phototable/FlipperDreamSettings;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 69
    .local v3, "pos":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    invoke-static {v4}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$100(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 70
    .local v1, "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    if-eqz v1, :cond_0

    .line 71
    iget-object v6, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    iget-object v4, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/android/dreams/phototable/AlbumSettings;->isAlbumEnabled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v0, v6, v4}, Lcom/android/dreams/phototable/AlbumSettings;->setAlbumEnabled(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_1

    .line 73
    .end local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    .end local v3    # "pos":Ljava/lang/Integer;
    :cond_2
    iget-object v4, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    invoke-static {v4}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$100(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->refreshList()V

    .line 74
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "shiftPressState"    # Z
    .param p7, "ctrlPressState"    # Z
    .param p8, "penPressState"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$000(Lcom/android/dreams/phototable/FlipperDreamSettings;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$000(Lcom/android/dreams/phototable/FlipperDreamSettings;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$000(Lcom/android/dreams/phototable/FlipperDreamSettings;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
