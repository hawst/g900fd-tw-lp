.class public Lcom/android/dreams/phototable/AlbumDataAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AlbumDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dreams/phototable/AlbumDataAdapter$1;,
        Lcom/android/dreams/phototable/AlbumDataAdapter$ItemClickListener;,
        Lcom/android/dreams/phototable/AlbumDataAdapter$TitleComparator;,
        Lcom/android/dreams/phototable/AlbumDataAdapter$RecencyComparator;,
        Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
        ">;"
    }
.end annotation


# static fields
.field public static final REMOVABLE_SD_DIR_PATH:Ljava/lang/String;


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLayout:I

.field private final mListener:Lcom/android/dreams/phototable/AlbumDataAdapter$ItemClickListener;

.field private final mSettings:Lcom/android/dreams/phototable/AlbumSettings;

.field private final mValidAlbumIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-le v0, v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    const-string v0, "/storage/extSdCard/"

    :goto_0
    sput-object v0, Lcom/android/dreams/phototable/AlbumDataAdapter;->REMOVABLE_SD_DIR_PATH:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "/mnt/extSdCard/"

    goto :goto_0

    :cond_1
    const-string v0, "/mnt/sdcard/extStorages/"

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;ILjava/util/List;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/SharedPreferences;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/android/dreams/phototable/PhotoSource$AlbumData;>;"
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 60
    invoke-static {p2}, Lcom/android/dreams/phototable/AlbumSettings;->getAlbumSettings(Landroid/content/SharedPreferences;)Lcom/android/dreams/phototable/AlbumSettings;

    move-result-object v2

    iput-object v2, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    .line 61
    iput p3, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mLayout:I

    .line 62
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 63
    new-instance v2, Lcom/android/dreams/phototable/AlbumDataAdapter$ItemClickListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/dreams/phototable/AlbumDataAdapter$ItemClickListener;-><init>(Lcom/android/dreams/phototable/AlbumDataAdapter;Lcom/android/dreams/phototable/AlbumDataAdapter$1;)V

    iput-object v2, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mListener:Lcom/android/dreams/phototable/AlbumDataAdapter$ItemClickListener;

    .line 65
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    iput-object v2, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mValidAlbumIds:Ljava/util/HashSet;

    .line 66
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 67
    .local v0, "albumData":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    iget-object v2, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mValidAlbumIds:Ljava/util/HashSet;

    iget-object v3, v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    .end local v0    # "albumData":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    :cond_0
    iget-object v2, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    iget-object v3, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mValidAlbumIds:Ljava/util/HashSet;

    invoke-virtual {v2, v3}, Lcom/android/dreams/phototable/AlbumSettings;->pruneObsoleteSettings(Ljava/util/Collection;)V

    .line 70
    return-void
.end method

.method static synthetic access$100(Lcom/android/dreams/phototable/AlbumDataAdapter;)Lcom/android/dreams/phototable/AlbumSettings;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/AlbumDataAdapter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    return-object v0
.end method

.method private isSDCardPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 211
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/dreams/phototable/AlbumDataAdapter;->REMOVABLE_SD_DIR_PATH:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public areAllSelected()Z
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    iget-object v1, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mValidAlbumIds:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumSettings;->areAllEnabled(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 92
    move-object v3, p2

    .line 93
    .local v3, "item":Landroid/view/View;
    if-nez v3, :cond_0

    .line 94
    iget-object v8, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v9, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mLayout:I

    invoke-virtual {v8, v9, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 96
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 98
    .local v1, "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    const v8, 0x7f0b0006

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 99
    .local v5, "vCheckBox":Landroid/view/View;
    if-eqz v5, :cond_1

    instance-of v8, v5, Landroid/widget/CheckBox;

    if-eqz v8, :cond_1

    move-object v0, v5

    .line 100
    check-cast v0, Landroid/widget/CheckBox;

    .line 101
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->isSelected(I)Z

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    const v8, 0x7f0b0003

    invoke-virtual {v0, v8, v1}, Landroid/widget/CheckBox;->setTag(ILjava/lang/Object;)V

    .line 105
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_1
    const v8, 0x7f0b0005

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 106
    .local v6, "vImageView":Landroid/view/View;
    if-eqz v6, :cond_2

    instance-of v8, v6, Landroid/widget/ImageView;

    if-eqz v8, :cond_2

    move-object v2, v6

    .line 107
    check-cast v2, Landroid/widget/ImageView;

    .line 108
    .local v2, "imageView":Landroid/widget/ImageView;
    const v8, 0x7f020002

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 109
    iget-object v8, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->thumbnailUrl:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/android/dreams/phototable/AlbumDataAdapter;->isSDCardPath(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 110
    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    .end local v2    # "imageView":Landroid/widget/ImageView;
    :cond_2
    :goto_0
    const v8, 0x7f0b0004

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 117
    .local v7, "vTextView":Landroid/view/View;
    if-eqz v7, :cond_3

    instance-of v8, v7, Landroid/widget/TextView;

    if-eqz v8, :cond_3

    move-object v4, v7

    .line 118
    check-cast v4, Landroid/widget/TextView;

    .line 119
    .local v4, "textView":Landroid/widget/TextView;
    iget-object v8, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    .end local v4    # "textView":Landroid/widget/TextView;
    :cond_3
    iget-object v8, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mListener:Lcom/android/dreams/phototable/AlbumDataAdapter$ItemClickListener;

    invoke-virtual {v3, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-object v3

    .line 112
    .end local v7    # "vTextView":Landroid/view/View;
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    :cond_4
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public isSelected(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 74
    .local v0, "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    iget-object v1, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    iget-object v2, v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/dreams/phototable/AlbumSettings;->isAlbumEnabled(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public selectAll(Z)V
    .locals 2
    .param p1, "select"    # Z

    .prologue
    .line 82
    if-eqz p1, :cond_0

    .line 83
    iget-object v0, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    iget-object v1, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mValidAlbumIds:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumSettings;->enableAllAlbums(Ljava/util/Collection;)V

    .line 87
    :goto_0
    invoke-virtual {p0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->notifyDataSetChanged()V

    .line 88
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/AlbumDataAdapter;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumSettings;->disableAllAlbums()V

    goto :goto_0
.end method
