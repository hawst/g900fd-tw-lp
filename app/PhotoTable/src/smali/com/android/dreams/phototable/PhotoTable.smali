.class public Lcom/android/dreams/phototable/PhotoTable;
.super Landroid/widget/FrameLayout;
.source "PhotoTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;,
        Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;,
        Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;,
        Lcom/android/dreams/phototable/PhotoTable$FocusReaper;,
        Lcom/android/dreams/phototable/PhotoTable$Launcher;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sRNG:Ljava/util/Random;


# instance fields
.field private final mBackgroudOptimization:Z

.field private mBackground:Landroid/view/ViewGroup;

.field private final mDragGestureDetector:Lcom/android/dreams/phototable/DragGestureDetector;

.field private mDream:Landroid/service/dreams/DreamService;

.field private final mDropInterpolator:Landroid/view/animation/Interpolator;

.field private final mDropPeriod:I

.field private final mEdgeSwipeDetector:Lcom/android/dreams/phototable/EdgeSwipeDetector;

.field private final mFastDropPeriod:I

.field private mFocus:Landroid/view/View;

.field private final mFocusReaper:Lcom/android/dreams/phototable/PhotoTable$FocusReaper;

.field private mHeight:I

.field private mHighlightColor:I

.field private final mImageRatio:F

.field private final mImageRotationLimit:F

.field private final mInset:I

.field private mIsLandscape:Z

.field private final mKeyboardInterpreter:Lcom/android/dreams/phototable/KeyboardInterpreter;

.field private final mLauncher:Lcom/android/dreams/phototable/PhotoTable$Launcher;

.field private mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

.field private mLongSide:I

.field private final mMaxFocusTime:I

.field private final mMaxSelectionTime:I

.field private final mNowDropDelay:I

.field private mOnDeck:[Landroid/view/View;

.field private final mOnTable:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mPhotoLaunchTask:Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;

.field private final mPhotoSource:Lcom/android/dreams/phototable/PhotoSource;

.field private final mPickUpDuration:J

.field private final mRedealCount:I

.field private final mResources:Landroid/content/res/Resources;

.field private mScrim:Landroid/view/View;

.field private mSelection:Landroid/view/View;

.field private final mSelectionReaper:Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;

.field private mShortSide:I

.field private mStageLeft:Landroid/view/ViewGroup;

.field private mStarted:Z

.field private final mStoryModeEnabled:Z

.field private final mTableCapacity:I

.field private final mTableRatio:F

.field private final mTapToExit:Z

.field private final mThrowInterpolator:Landroid/view/animation/Interpolator;

.field private final mThrowRotation:F

.field private final mThrowSpeed:F

.field private final mWaitingToJoinBackground:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/android/dreams/phototable/PhotoTable;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/dreams/phototable/PhotoTable;->$assertionsDisabled:Z

    .line 82
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/android/dreams/phototable/PhotoTable;->sRNG:Ljava/util/Random;

    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "as"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const v4, 0x49742400    # 1000000.0f

    .line 131
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 132
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    .line 133
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mInset:I

    .line 134
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDropPeriod:I

    .line 135
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFastDropPeriod:I

    .line 136
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mNowDropDelay:I

    .line 137
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mImageRatio:F

    .line 138
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mTableRatio:F

    .line 139
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mImageRotationLimit:F

    .line 140
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowSpeed:F

    .line 141
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    .line 142
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f08000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowRotation:F

    .line 143
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mTableCapacity:I

    .line 144
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mRedealCount:I

    .line 145
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f090002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mTapToExit:Z

    .line 146
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStoryModeEnabled:Z

    .line 147
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackgroudOptimization:Z

    .line 148
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f070004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mHighlightColor:I

    .line 149
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mMaxSelectionTime:I

    .line 150
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f08001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mMaxFocusTime:I

    .line 151
    new-instance v0, Lcom/android/dreams/phototable/SoftLandingInterpolator;

    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f08000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f080010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-direct {v0, v1, v2}, Lcom/android/dreams/phototable/SoftLandingInterpolator;-><init>(FF)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowInterpolator:Landroid/view/animation/Interpolator;

    .line 154
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f080011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDropInterpolator:Landroid/view/animation/Interpolator;

    .line 156
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    .line 157
    new-instance v0, Lcom/android/dreams/phototable/PhotoSourcePlexor;

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "PhotoTableDream"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/dreams/phototable/PhotoSourcePlexor;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSource;

    .line 159
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mWaitingToJoinBackground:Ljava/util/Set;

    .line 160
    new-instance v0, Lcom/android/dreams/phototable/PhotoTable$Launcher;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/PhotoTable$Launcher;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLauncher:Lcom/android/dreams/phototable/PhotoTable$Launcher;

    .line 161
    new-instance v0, Lcom/android/dreams/phototable/PhotoTable$FocusReaper;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/PhotoTable$FocusReaper;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocusReaper:Lcom/android/dreams/phototable/PhotoTable$FocusReaper;

    .line 162
    new-instance v0, Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelectionReaper:Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;

    .line 163
    new-instance v0, Lcom/android/dreams/phototable/DragGestureDetector;

    invoke-direct {v0, p1, p0}, Lcom/android/dreams/phototable/DragGestureDetector;-><init>(Landroid/content/Context;Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDragGestureDetector:Lcom/android/dreams/phototable/DragGestureDetector;

    .line 164
    new-instance v0, Lcom/android/dreams/phototable/EdgeSwipeDetector;

    invoke-direct {v0, p1, p0}, Lcom/android/dreams/phototable/EdgeSwipeDetector;-><init>(Landroid/content/Context;Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mEdgeSwipeDetector:Lcom/android/dreams/phototable/EdgeSwipeDetector;

    .line 165
    new-instance v0, Lcom/android/dreams/phototable/KeyboardInterpreter;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/KeyboardInterpreter;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mKeyboardInterpreter:Lcom/android/dreams/phototable/KeyboardInterpreter;

    .line 166
    new-array v0, v6, [Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    .line 167
    new-array v0, v6, [Landroid/view/View;

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    .line 168
    iput-boolean v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mStarted:Z

    .line 169
    return-void
.end method

.method static synthetic access$000(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDropPeriod:I

    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 54
    invoke-static {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/android/dreams/phototable/PhotoTable;->placeOnDeck(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->recycle(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->throwOnTable(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/dreams/phototable/PhotoTable;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mTableCapacity:I

    return v0
.end method

.method static synthetic access$1600(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mRedealCount:I

    return v0
.end method

.method static synthetic access$1700(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFastDropPeriod:I

    return v0
.end method

.method static synthetic access$1800(Lcom/android/dreams/phototable/PhotoTable;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/dreams/phototable/PhotoTable;->resolveBackgroundQueue()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/dreams/phototable/PhotoTable;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->getBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/dreams/phototable/PhotoTable;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStageLeft:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mNowDropDelay:I

    return v0
.end method

.method static synthetic access$2200(Lcom/android/dreams/phototable/PhotoTable;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mWaitingToJoinBackground:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mShortSide:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/dreams/phototable/PhotoTable;)Lcom/android/dreams/phototable/PhotoSource;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSource;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/dreams/phototable/PhotoTable;Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;
    .param p1, "x1"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 54
    invoke-static {p0, p1, p2}, Lcom/android/dreams/phototable/PhotoTable;->applyFrame(Lcom/android/dreams/phototable/PhotoTable;Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/dreams/phototable/PhotoTable;)[Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    return v0
.end method

.method static synthetic access$900(Lcom/android/dreams/phototable/PhotoTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/PhotoTable;

    .prologue
    .line 54
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    return v0
.end method

.method private static applyFrame(Lcom/android/dreams/phototable/PhotoTable;Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Bitmap;)Landroid/view/View;
    .locals 12
    .param p0, "table"    # Lcom/android/dreams/phototable/PhotoTable;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "decodedPhoto"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 467
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 469
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040004

    const/4 v3, 0x0

    invoke-virtual {v7, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .local v9, "photo":Landroid/view/View;
    move-object v6, v9

    .line 470
    check-cast v6, Landroid/widget/ImageView;

    .line 471
    .local v6, "image":Landroid/widget/ImageView;
    const/4 v2, 0x2

    new-array v8, v2, [Landroid/graphics/drawable/Drawable;

    .line 472
    .local v8, "layers":[Landroid/graphics/drawable/Drawable;
    iget v11, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 473
    .local v11, "photoWidth":I
    iget v10, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 474
    .local v10, "photoHeight":I
    if-eqz p2, :cond_0

    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v2, :cond_0

    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v2, :cond_1

    .line 475
    :cond_0
    const/4 v9, 0x0

    .line 491
    :goto_0
    return-object v9

    .line 477
    :cond_1
    invoke-virtual {p2, v4}, Landroid/graphics/Bitmap;->setHasMipMap(Z)V

    .line 478
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    invoke-direct {v2, v3, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v2, v8, v1

    .line 479
    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f020001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v8, v4

    .line 480
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v8}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 481
    .local v0, "layerList":Landroid/graphics/drawable/LayerDrawable;
    iget v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mInset:I

    iget v3, p0, Lcom/android/dreams/phototable/PhotoTable;->mInset:I

    iget v4, p0, Lcom/android/dreams/phototable/PhotoTable;->mInset:I

    iget v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mInset:I

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 483
    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 485
    const v1, 0x7f0b0001

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 486
    const/high16 v1, 0x7f0b0000

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 488
    new-instance v1, Lcom/android/dreams/phototable/PhotoTouchListener;

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/dreams/phototable/PhotoTouchListener;-><init>(Landroid/content/Context;Lcom/android/dreams/phototable/PhotoTable;)V

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private cross([D[D)D
    .locals 6
    .param p1, "a"    # [D
    .param p2, "b"    # [D

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 340
    aget-wide v0, p1, v4

    aget-wide v2, p2, v5

    mul-double/2addr v0, v2

    aget-wide v2, p1, v5

    aget-wide v4, p2, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private exitStageLeft(Landroid/view/View;)V
    .locals 3
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v2, -0x2

    .line 904
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->removeViewFromParent(Landroid/view/View;)V

    .line 905
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStageLeft:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 907
    return-void
.end method

.method private getBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 974
    if-nez p1, :cond_0

    move-object v5, v6

    .line 1000
    :goto_0
    return-object v5

    .line 977
    :cond_0
    const/4 v4, 0x0

    .line 978
    .local v4, "layers":Landroid/graphics/drawable/LayerDrawable;
    const/4 v1, 0x0

    .line 980
    .local v1, "bitmap":Landroid/graphics/drawable/BitmapDrawable;
    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/widget/ImageView;

    move-object v3, v0

    .line 981
    .local v3, "image":Landroid/widget/ImageView;
    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 986
    .end local v3    # "image":Landroid/widget/ImageView;
    :goto_1
    if-nez v4, :cond_1

    move-object v5, v6

    .line 987
    goto :goto_0

    .line 982
    :catch_0
    move-exception v2

    .line 983
    .local v2, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v2}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_1

    .line 991
    .end local v2    # "e":Ljava/lang/ClassCastException;
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v1, v0
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 996
    :goto_2
    if-nez v1, :cond_2

    move-object v5, v6

    .line 997
    goto :goto_0

    .line 992
    :catch_1
    move-exception v2

    .line 993
    .restart local v2    # "e":Ljava/lang/ClassCastException;
    invoke-virtual {v2}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_2

    .line 1000
    .end local v2    # "e":Ljava/lang/ClassCastException;
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0
.end method

.method private getCenter(Landroid/view/View;)[D
    .locals 7
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 348
    const v3, 0x7f0b0001

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v2, v3

    .line 349
    .local v2, "width":F
    const/high16 v3, 0x7f0b0000

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v1, v3

    .line 350
    .local v1, "height":F
    const/4 v3, 0x2

    new-array v0, v3, [D

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v4

    div-float v5, v2, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    div-float v5, v1, v6

    add-float/2addr v4, v5

    neg-float v4, v4

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 352
    .local v0, "center":[D
    return-object v0
.end method

.method private isInBackground(Landroid/view/View;)Z
    .locals 2
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 925
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackgroudOptimization:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static lerp(FFF)F
    .locals 1
    .param p0, "a"    # F
    .param p1, "b"    # F
    .param p2, "f"    # F

    .prologue
    .line 304
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method private static varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 1064
    return-void
.end method

.method private moveToBackground(Landroid/view/View;)V
    .locals 3
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v2, -0x2

    .line 896
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackgroudOptimization:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->isInBackground(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 897
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->removeViewFromParent(Landroid/view/View;)V

    .line 898
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 901
    :cond_0
    return-void
.end method

.method private moveToForeground(Landroid/view/View;)V
    .locals 2
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x2

    .line 917
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackgroudOptimization:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->isInBackground(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 919
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/android/dreams/phototable/PhotoTable;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 922
    :cond_0
    return-void
.end method

.method private norm([D)D
    .locals 4
    .param p1, "a"    # [D

    .prologue
    .line 344
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    const/4 v2, 0x1

    aget-wide v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private pickUp(Landroid/view/View;)V
    .locals 11
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 938
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v1, v5

    .line 939
    .local v1, "photoWidth":F
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v0, v5

    .line 941
    .local v0, "photoHeight":F
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v0

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 943
    .local v2, "scale":F
    const-string v5, "scale is %f"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 944
    const-string v5, "target it"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 945
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getWidth()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v5, v1

    div-float v3, v5, v8

    .line 946
    .local v3, "x":F
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getHeight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v5, v0

    div-float v4, v5, v8

    .line 948
    .local v4, "y":F
    invoke-virtual {p1}, Landroid/view/View;->getRotation()F

    move-result v5

    invoke-direct {p0, v5}, Lcom/android/dreams/phototable/PhotoTable;->wrapAngle(F)F

    move-result v5

    invoke-virtual {p1, v5}, Landroid/view/View;->setRotation(F)V

    .line 950
    const-string v5, "animate it"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    iget-object v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mWaitingToJoinBackground:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 953
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->moveToForeground(Landroid/view/View;)V

    .line 954
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6, v8}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Lcom/android/dreams/phototable/PhotoTable$6;

    invoke-direct {v6, p0, p1}, Lcom/android/dreams/phototable/PhotoTable$6;-><init>(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 971
    return-void
.end method

.method private placeOnDeck(Landroid/view/View;I)V
    .locals 11
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "slot"    # I

    .prologue
    const/4 v8, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 722
    iget-object v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    array-length v6, v6

    if-ge p2, v6, :cond_1

    .line 723
    iget-object v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v6, v6, p2

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v6, v6, p2

    if-eq v6, p1, :cond_0

    .line 724
    iget-object v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v6, v6, p2

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/android/dreams/phototable/PhotoTable;->fadeAway(Landroid/view/View;Z)V

    .line 726
    :cond_0
    iget-object v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aput-object p1, v6, p2

    .line 727
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v2, v6

    .line 728
    .local v2, "photoWidth":F
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v1, v6

    .line 729
    .local v1, "photoHeight":F
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v1

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 731
    .local v3, "scale":F
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v2

    div-float v4, v6, v10

    .line 732
    .local v4, "x":F
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v1

    div-float v5, v6, v10

    .line 734
    .local v5, "y":F
    iget v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/android/dreams/phototable/PhotoTable;->mInset:I

    int-to-float v7, v7

    mul-float/2addr v7, v10

    sub-float v7, v2, v7

    mul-float/2addr v7, v3

    add-float/2addr v6, v7

    div-float v0, v6, v10

    .line 735
    .local v0, "offset":F
    const/4 v6, 0x1

    if-ne p2, v6, :cond_2

    const/high16 v6, 0x3f800000    # 1.0f

    :goto_0
    mul-float/2addr v6, v0

    add-float/2addr v4, v6

    .line 737
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    iget-wide v8, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v6, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7, v10}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 748
    .end local v0    # "offset":F
    .end local v1    # "photoHeight":F
    .end local v2    # "photoWidth":F
    .end local v3    # "scale":F
    .end local v4    # "x":F
    .end local v5    # "y":F
    :cond_1
    return-void

    .line 735
    .restart local v0    # "offset":F
    .restart local v1    # "photoHeight":F
    .restart local v2    # "photoWidth":F
    .restart local v3    # "scale":F
    .restart local v4    # "x":F
    .restart local v5    # "y":F
    :cond_2
    const/high16 v6, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method private promoteSelection()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 252
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    iget v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mMaxSelectionTime:I

    invoke-virtual {p0, v1}, Lcom/android/dreams/phototable/PhotoTable;->scheduleSelectionReaper(I)V

    .line 254
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 255
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 256
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/android/dreams/phototable/PhotoTable;->moveToTopOfPile(Landroid/view/View;)V

    .line 257
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/android/dreams/phototable/PhotoTable;->pickUp(Landroid/view/View;)V

    .line 258
    iget-boolean v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mStoryModeEnabled:Z

    if-eqz v1, :cond_2

    .line 259
    const/4 v0, 0x0

    .local v0, "slot":I
    :goto_0
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 260
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 262
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    invoke-virtual {v1, v5}, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;->cancel(Z)Z

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    .line 265
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    new-instance v2, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    invoke-direct {v2, p0, v0}, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;-><init>(Lcom/android/dreams/phototable/PhotoTable;I)V

    aput-object v2, v1, v0

    .line 266
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    new-array v2, v5, [Landroid/view/View;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 259
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    .end local v0    # "slot":I
    :cond_2
    return-void
.end method

.method private static randMultiDrop(IFFII)Landroid/graphics/PointF;
    .locals 9
    .param p0, "n"    # I
    .param p1, "i"    # F
    .param p2, "j"    # F
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 326
    const-string v5, "randMultiDrop (%d, %f, %f, %d, %d)"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    const/4 v5, 0x5

    new-array v0, v5, [F

    fill-array-data v0, :array_0

    .line 328
    .local v0, "cx":[F
    const/4 v5, 0x5

    new-array v1, v5, [F

    fill-array-data v1, :array_1

    .line 329
    .local v1, "cy":[F
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    .line 330
    array-length v5, v0

    rem-int v5, p0, v5

    aget v3, v0, v5

    .line 331
    .local v3, "x":F
    array-length v5, v0

    rem-int v5, p0, v5

    aget v4, v1, v5

    .line 332
    .local v4, "y":F
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 333
    .local v2, "p":Landroid/graphics/PointF;
    int-to-float v5, p3

    mul-float/2addr v5, v3

    const v6, 0x3d4ccccd    # 0.05f

    int-to-float v7, p3

    mul-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float/2addr v5, v6

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 334
    int-to-float v5, p4

    mul-float/2addr v5, v4

    const v6, 0x3d4ccccd    # 0.05f

    int-to-float v7, p4

    mul-float/2addr v6, v7

    mul-float/2addr v6, p2

    add-float/2addr v5, v6

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 335
    const-string v5, "randInCenter returning %f, %f"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v8, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    return-object v2

    .line 327
    nop

    :array_0
    .array-data 4
        0x3e99999a    # 0.3f
        0x3e99999a    # 0.3f
        0x3f000000    # 0.5f
        0x3f333333    # 0.7f
        0x3f333333    # 0.7f
    .end array-data

    .line 328
    :array_1
    .array-data 4
        0x3e99999a    # 0.3f
        0x3f333333    # 0.7f
        0x3f000000    # 0.5f
        0x3e99999a    # 0.3f
        0x3f333333    # 0.7f
    .end array-data
.end method

.method static randfrange(FF)F
    .locals 1
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 308
    sget-object v0, Lcom/android/dreams/phototable/PhotoTable;->sRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/android/dreams/phototable/PhotoTable;->lerp(FFF)F

    move-result v0

    return v0
.end method

.method private recycle(Landroid/view/View;)V
    .locals 2
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 1004
    if-eqz p1, :cond_1

    .line 1005
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->removeViewFromParent(Landroid/view/View;)V

    .line 1006
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->getBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1007
    .local v0, "bmp":Landroid/graphics/Bitmap;
    instance-of v1, p1, Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 1008
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "photo":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1010
    :cond_0
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSource;

    invoke-virtual {v1, v0}, Lcom/android/dreams/phototable/PhotoSource;->recycle(Landroid/graphics/Bitmap;)V

    .line 1013
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    return-void
.end method

.method private removeViewFromParent(Landroid/view/View;)V
    .locals 1
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 910
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 911
    .local v0, "parent":Landroid/view/ViewParent;
    if-eqz v0, :cond_0

    .line 912
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 914
    :cond_0
    return-void
.end method

.method private resolveBackgroundQueue()V
    .locals 3

    .prologue
    .line 675
    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mWaitingToJoinBackground:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 676
    .local v1, "photo":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/android/dreams/phototable/PhotoTable;->moveToBackground(Landroid/view/View;)V

    goto :goto_0

    .line 678
    .end local v1    # "photo":Landroid/view/View;
    :cond_0
    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mWaitingToJoinBackground:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 679
    return-void
.end method

.method private throwOnTable(Landroid/view/View;)V
    .locals 2
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 761
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    .line 762
    const-string v0, "start offscreen"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 763
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowRotation:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 764
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setX(F)V

    .line 765
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setY(F)V

    .line 767
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1, v0}, Lcom/android/dreams/phototable/PhotoTable;->dropOnTable(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 768
    return-void
.end method

.method private wrapAngle(F)F
    .locals 4
    .param p1, "angle"    # F

    .prologue
    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v2, 0x43b40000    # 360.0f

    .line 930
    add-float v0, p1, v3

    .line 931
    .local v0, "result":F
    rem-float v1, v0, v2

    add-float/2addr v1, v2

    rem-float v0, v1, v2

    .line 932
    sub-float/2addr v0, v3

    .line 933
    return v0
.end method


# virtual methods
.method public clearFocus()V
    .locals 2

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getFocus()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->setHighlight(Landroid/view/View;Z)V

    .line 285
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocus:Landroid/view/View;

    .line 286
    return-void
.end method

.method public clearSelection()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 191
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/android/dreams/phototable/PhotoTable;->dropOnTable(Landroid/view/View;)V

    .line 193
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSource;

    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/android/dreams/phototable/PhotoTable;->getBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/dreams/phototable/PhotoSource;->donePaging(Landroid/graphics/Bitmap;)V

    .line 194
    iget-boolean v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mStoryModeEnabled:Z

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/android/dreams/phototable/PhotoTable;->fadeInBackground(Landroid/view/View;)V

    .line 197
    :cond_0
    iput-object v3, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    .line 199
    :cond_1
    const/4 v0, 0x0

    .local v0, "slot":I
    :goto_0
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 200
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 201
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1, v4}, Lcom/android/dreams/phototable/PhotoTable;->fadeAway(Landroid/view/View;Z)V

    .line 202
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aput-object v3, v1, v0

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_3

    .line 206
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;->cancel(Z)Z

    .line 207
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mLoadOnDeckTasks:[Lcom/android/dreams/phototable/PhotoTable$LoadNaturalSiblingTask;

    aput-object v3, v1, v0

    .line 199
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_4
    return-void
.end method

.method public dropOnTable(Landroid/view/View;)V
    .locals 1
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 847
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDropInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1, v0}, Lcom/android/dreams/phototable/PhotoTable;->dropOnTable(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 848
    return-void
.end method

.method public dropOnTable(Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .locals 17
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 852
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/dreams/phototable/PhotoTable;->mImageRotationLimit:F

    neg-float v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/dreams/phototable/PhotoTable;->mImageRotationLimit:F

    invoke-static {v12, v13}, Lcom/android/dreams/phototable/PhotoTable;->randfrange(FF)F

    move-result v2

    .line 853
    .local v2, "angle":F
    sget-object v12, Lcom/android/dreams/phototable/PhotoTable;->sRNG:Ljava/util/Random;

    invoke-virtual {v12}, Ljava/util/Random;->nextInt()I

    move-result v12

    sget-object v13, Lcom/android/dreams/phototable/PhotoTable;->sRNG:Ljava/util/Random;

    invoke-virtual {v13}, Ljava/util/Random;->nextGaussian()D

    move-result-wide v14

    double-to-float v13, v14

    sget-object v14, Lcom/android/dreams/phototable/PhotoTable;->sRNG:Ljava/util/Random;

    invoke-virtual {v14}, Ljava/util/Random;->nextGaussian()D

    move-result-wide v14

    double-to-float v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    move/from16 v16, v0

    invoke-static/range {v12 .. v16}, Lcom/android/dreams/phototable/PhotoTable;->randMultiDrop(IFFII)Landroid/graphics/PointF;

    move-result-object v7

    .line 856
    .local v7, "p":Landroid/graphics/PointF;
    iget v8, v7, Landroid/graphics/PointF;->x:F

    .line 857
    .local v8, "x":F
    iget v10, v7, Landroid/graphics/PointF;->y:F

    .line 859
    .local v10, "y":F
    const-string v12, "drop it at %f, %f"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 861
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getX()F

    move-result v9

    .line 862
    .local v9, "x0":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getY()F

    move-result v11

    .line 864
    .local v11, "y0":F
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    int-to-float v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v8, v12

    .line 865
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/dreams/phototable/PhotoTable;->mShortSide:I

    int-to-float v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    sub-float/2addr v10, v12

    .line 866
    const-string v12, "fixed offset is %f, %f "

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 868
    sub-float v5, v8, v9

    .line 869
    .local v5, "dx":F
    sub-float v6, v10, v11

    .line 871
    .local v6, "dy":F
    float-to-double v12, v5

    float-to-double v14, v6

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v12

    double-to-float v3, v12

    .line 872
    .local v3, "dist":F
    const/high16 v12, 0x447a0000    # 1000.0f

    mul-float/2addr v12, v3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/dreams/phototable/PhotoTable;->mThrowSpeed:F

    div-float/2addr v12, v13

    float-to-int v4, v12

    .line 873
    .local v4, "duration":I
    const/16 v12, 0x3e8

    invoke-static {v4, v12}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 875
    const-string v12, "animate it"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v12, v13}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 877
    invoke-direct/range {p0 .. p0}, Lcom/android/dreams/phototable/PhotoTable;->resolveBackgroundQueue()V

    .line 878
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/dreams/phototable/PhotoTable;->mTableRatio:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/dreams/phototable/PhotoTable;->mImageRatio:F

    div-float/2addr v13, v14

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/dreams/phototable/PhotoTable;->mTableRatio:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/dreams/phototable/PhotoTable;->mImageRatio:F

    div-float/2addr v13, v14

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    invoke-virtual {v12, v2}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    invoke-virtual {v12, v10}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    int-to-long v14, v4

    invoke-virtual {v12, v14, v15}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    new-instance v13, Lcom/android/dreams/phototable/PhotoTable$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v13, v0, v1}, Lcom/android/dreams/phototable/PhotoTable$5;-><init>(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;)V

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 893
    return-void
.end method

.method public fadeAway(Landroid/view/View;Z)V
    .locals 4
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "replace"    # Z

    .prologue
    .line 684
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 685
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->exitStageLeft(Landroid/view/View;)V

    .line 686
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 687
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 688
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/dreams/phototable/PhotoTable$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/dreams/phototable/PhotoTable$3;-><init>(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;Z)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 705
    return-void
.end method

.method public fadeInBackground(Landroid/view/View;)V
    .locals 4
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 646
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackgroudOptimization:Z

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mWaitingToJoinBackground:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 648
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/dreams/phototable/PhotoTable$1;

    invoke-direct {v1, p0}, Lcom/android/dreams/phototable/PhotoTable$1;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 672
    :goto_0
    return-void

    .line 659
    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->bringChildToFront(Landroid/view/View;)V

    .line 660
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->bringChildToFront(Landroid/view/View;)V

    .line 661
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/dreams/phototable/PhotoTable$2;

    invoke-direct {v1, p0}, Lcom/android/dreams/phototable/PhotoTable$2;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public fadeOutBackground(Landroid/view/View;)V
    .locals 4
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 625
    invoke-direct {p0}, Lcom/android/dreams/phototable/PhotoTable;->resolveBackgroundQueue()V

    .line 626
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackgroudOptimization:Z

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 641
    :goto_0
    return-void

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 633
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 634
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->bringChildToFront(Landroid/view/View;)V

    .line 635
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->bringChildToFront(Landroid/view/View;)V

    .line 636
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mPickUpDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public fling(Landroid/view/View;)V
    .locals 14
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 785
    const/4 v0, 0x2

    new-array v11, v0, [F

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v11, v0

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v11, v0

    .line 787
    .local v11, "o":[F
    const/4 v0, 0x2

    new-array v6, v0, [F

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    aput v1, v6, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v1

    aput v1, v6, v0

    .line 788
    .local v6, "a":[F
    const/4 v0, 0x2

    new-array v7, v0, [F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aget v1, v11, v1

    aput v1, v7, v0

    const/4 v0, 0x1

    const/4 v1, 0x1

    aget v1, v6, v1

    const/4 v2, 0x0

    aget v2, v11, v2

    add-float/2addr v1, v2

    const/4 v2, 0x0

    aget v2, v6, v2

    sub-float/2addr v1, v2

    aput v1, v7, v0

    .line 789
    .local v7, "b":[F
    const/4 v0, 0x2

    new-array v8, v0, [F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aget v1, v6, v1

    const/4 v2, 0x1

    aget v2, v11, v2

    add-float/2addr v1, v2

    const/4 v2, 0x1

    aget v2, v6, v2

    sub-float/2addr v1, v2

    aput v1, v8, v0

    const/4 v0, 0x1

    const/4 v1, 0x1

    aget v1, v11, v1

    aput v1, v8, v0

    .line 790
    .local v8, "c":[F
    const/4 v0, 0x2

    new-array v9, v0, [F

    fill-array-data v9, :array_0

    .line 791
    .local v9, "delta":[F
    const/4 v0, 0x0

    aget v0, v7, v0

    const/4 v1, 0x0

    aget v1, v6, v1

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const/4 v2, 0x1

    aget v2, v7, v2

    const/4 v3, 0x1

    aget v3, v6, v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    const/4 v2, 0x0

    aget v2, v8, v2

    const/4 v3, 0x0

    aget v3, v6, v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    const/4 v5, 0x1

    aget v5, v8, v5

    const/4 v12, 0x1

    aget v12, v6, v12

    sub-float/2addr v5, v12

    float-to-double v12, v5

    invoke-static {v2, v3, v12, v13}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 792
    const/4 v0, 0x0

    const/4 v1, 0x0

    aget v1, v7, v1

    const/4 v2, 0x0

    aget v2, v6, v2

    sub-float/2addr v1, v2

    aput v1, v9, v0

    .line 793
    const/4 v0, 0x1

    const/4 v1, 0x1

    aget v1, v7, v1

    const/4 v2, 0x1

    aget v2, v6, v2

    sub-float/2addr v1, v2

    aput v1, v9, v0

    .line 799
    :goto_0
    const/4 v0, 0x0

    aget v0, v9, v0

    float-to-double v0, v0

    const/4 v2, 0x1

    aget v2, v9, v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v10, v0

    .line 800
    .local v10, "dist":F
    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowSpeed:F

    div-float/2addr v0, v1

    float-to-int v4, v0

    .line 801
    .local v4, "duration":I
    const/4 v0, 0x0

    aget v2, v9, v0

    const/4 v0, 0x1

    aget v3, v9, v0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/dreams/phototable/PhotoTable;->fling(Landroid/view/View;FFIZ)V

    .line 802
    return-void

    .line 795
    .end local v4    # "duration":I
    .end local v10    # "dist":F
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    aget v1, v8, v1

    const/4 v2, 0x0

    aget v2, v6, v2

    sub-float/2addr v1, v2

    aput v1, v9, v0

    .line 796
    const/4 v0, 0x1

    const/4 v1, 0x1

    aget v1, v8, v1

    const/4 v2, 0x1

    aget v2, v6, v2

    sub-float/2addr v1, v2

    aput v1, v9, v0

    goto :goto_0

    .line 790
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public fling(Landroid/view/View;FFIZ)V
    .locals 4
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "dx"    # F
    .param p3, "dy"    # F
    .param p4, "duration"    # I
    .param p5, "spin"    # Z

    .prologue
    .line 806
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getFocus()Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 807
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/android/dreams/phototable/PhotoTable;->moveFocus(Landroid/view/View;F)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 808
    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0, p1, v1}, Lcom/android/dreams/phototable/PhotoTable;->moveFocus(Landroid/view/View;F)Landroid/view/View;

    .line 811
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->moveToForeground(Landroid/view/View;)V

    .line 812
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/ViewPropertyAnimator;->xBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/view/ViewPropertyAnimator;->yBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p4

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 819
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    if-eqz p5, :cond_1

    .line 820
    iget v1, p0, Lcom/android/dreams/phototable/PhotoTable;->mThrowRotation:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    .line 823
    :cond_1
    float-to-int v1, p2

    float-to-int v2, p3

    invoke-virtual {p0, p1, v1, v2}, Lcom/android/dreams/phototable/PhotoTable;->photoOffTable(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 824
    const-string v1, "fling away"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 825
    new-instance v1, Lcom/android/dreams/phototable/PhotoTable$4;

    invoke-direct {v1, p0, p1}, Lcom/android/dreams/phototable/PhotoTable$4;-><init>(Lcom/android/dreams/phototable/PhotoTable;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 832
    :cond_2
    return-void
.end method

.method public getFocus()Landroid/view/View;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocus:Landroid/view/View;

    return-object v0
.end method

.method public getSelection()Landroid/view/View;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    return-object v0
.end method

.method public hasFocus()Z
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocus:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSelection()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x1

    return v0
.end method

.method public launch()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 611
    const-string v0, "launching"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 612
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->setSystemUiVisibility(I)V

    .line 613
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v0

    if-nez v0, :cond_1

    .line 614
    const-string v0, "inflate it"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 615
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoLaunchTask:Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoLaunchTask:Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 617
    :cond_0
    new-instance v0, Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;-><init>(Lcom/android/dreams/phototable/PhotoTable;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoLaunchTask:Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;

    .line 618
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mPhotoLaunchTask:Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/PhotoTable$PhotoLaunchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 621
    :cond_1
    return-void
.end method

.method public move(Landroid/view/View;FFF)V
    .locals 1
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "a"    # F

    .prologue
    .line 752
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 753
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 754
    float-to-int v0, p2

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setX(F)V

    .line 755
    float-to-int v0, p3

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setY(F)V

    .line 756
    float-to-int v0, p4

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 757
    return-void
.end method

.method public move(Landroid/view/View;FFZ)V
    .locals 5
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "dx"    # F
    .param p3, "dy"    # F
    .param p4, "drop"    # Z

    .prologue
    .line 771
    if-eqz p1, :cond_0

    .line 772
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v2

    add-float v0, v2, p2

    .line 773
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    add-float v1, v2, p3

    .line 774
    .local v1, "y":F
    invoke-virtual {p1, v0}, Landroid/view/View;->setX(F)V

    .line 775
    invoke-virtual {p1, v1}, Landroid/view/View;->setY(F)V

    .line 776
    const-string v2, "PhotoTable"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] + ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    if-eqz p4, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->photoOffTable(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 778
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/android/dreams/phototable/PhotoTable;->fadeAway(Landroid/view/View;Z)V

    .line 781
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_0
    return-void
.end method

.method public moveFocus(Landroid/view/View;F)Landroid/view/View;
    .locals 1
    .param p1, "focus"    # Landroid/view/View;
    .param p2, "direction"    # F

    .prologue
    .line 356
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/dreams/phototable/PhotoTable;->moveFocus(Landroid/view/View;FF)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public moveFocus(Landroid/view/View;FF)Landroid/view/View;
    .locals 26
    .param p1, "focus"    # Landroid/view/View;
    .param p2, "direction"    # F
    .param p3, "angle"    # F

    .prologue
    .line 360
    if-nez p1, :cond_1

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->size()I

    move-result v20

    if-lez v20, :cond_0

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/View;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->setFocus(Landroid/view/View;)V

    .line 396
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/dreams/phototable/PhotoTable;->getFocus()Landroid/view/View;

    move-result-object v20

    :goto_1
    return-object v20

    .line 365
    :cond_1
    move/from16 v0, p2

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    .line 366
    .local v6, "alpha":D
    const/high16 v20, 0x43340000    # 180.0f

    move/from16 v0, p3

    move/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    .line 367
    .local v12, "beta":D
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [D

    move-object/from16 v18, v0

    const/16 v20, 0x0

    sub-double v22, v6, v12

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    aput-wide v22, v18, v20

    const/16 v20, 0x1

    sub-double v22, v6, v12

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    aput-wide v22, v18, v20

    .line 369
    .local v18, "left":[D
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [D

    move-object/from16 v19, v0

    const/16 v20, 0x0

    add-double v22, v6, v12

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    aput-wide v22, v19, v20

    const/16 v20, 0x1

    add-double v22, v6, v12

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    aput-wide v22, v19, v20

    .line 371
    .local v19, "right":[D
    invoke-direct/range {p0 .. p1}, Lcom/android/dreams/phototable/PhotoTable;->getCenter(Landroid/view/View;)[D

    move-result-object v4

    .line 372
    .local v4, "a":[D
    const/4 v10, 0x0

    .line 373
    .local v10, "bestFocus":Landroid/view/View;
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 374
    .local v8, "bestDistance":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    .line 375
    .local v11, "candidate":Landroid/view/View;
    move-object/from16 v0, p1

    if-eq v11, v0, :cond_2

    .line 376
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/dreams/phototable/PhotoTable;->getCenter(Landroid/view/View;)[D

    move-result-object v5

    .line 377
    .local v5, "b":[D
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v14, v0, [D

    const/16 v20, 0x0

    const/16 v21, 0x0

    aget-wide v22, v5, v21

    const/16 v21, 0x0

    aget-wide v24, v4, v21

    sub-double v22, v22, v24

    aput-wide v22, v14, v20

    const/16 v20, 0x1

    const/16 v21, 0x1

    aget-wide v22, v5, v21

    const/16 v21, 0x1

    aget-wide v24, v4, v21

    sub-double v22, v22, v24

    aput-wide v22, v14, v20

    .line 379
    .local v14, "delta":[D
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v14, v1}, Lcom/android/dreams/phototable/PhotoTable;->cross([D[D)D

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmpl-double v20, v20, v22

    if-lez v20, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v14, v1}, Lcom/android/dreams/phototable/PhotoTable;->cross([D[D)D

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmpg-double v20, v20, v22

    if-gez v20, :cond_2

    .line 380
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/dreams/phototable/PhotoTable;->norm([D)D

    move-result-wide v16

    .line 381
    .local v16, "distance":D
    cmpl-double v20, v8, v16

    if-lez v20, :cond_2

    .line 382
    move-wide/from16 v8, v16

    .line 383
    move-object v10, v11

    goto :goto_2

    .line 388
    .end local v5    # "b":[D
    .end local v11    # "candidate":Landroid/view/View;
    .end local v14    # "delta":[D
    .end local v16    # "distance":D
    :cond_3
    if-nez v10, :cond_4

    .line 389
    const/high16 v20, 0x43340000    # 180.0f

    cmpg-float v20, p3, v20

    if-gez v20, :cond_0

    .line 390
    const/high16 v20, 0x43340000    # 180.0f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/dreams/phototable/PhotoTable;->moveFocus(Landroid/view/View;FF)Landroid/view/View;

    move-result-object v20

    goto/16 :goto_1

    .line 393
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/dreams/phototable/PhotoTable;->setFocus(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public moveToTopOfPile(Landroid/view/View;)V
    .locals 1
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    .line 710
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->isInBackground(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 715
    :goto_0
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->invalidate()V

    .line 716
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 717
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    .line 718
    return-void

    .line 713
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->bringChildToFront(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 173
    const v0, 0x7f0b0011

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mBackground:Landroid/view/ViewGroup;

    .line 174
    const v0, 0x7f0b0010

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStageLeft:Landroid/view/ViewGroup;

    .line 175
    const v0, 0x7f0b000f

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mScrim:Landroid/view/View;

    .line 176
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mEdgeSwipeDetector:Lcom/android/dreams/phototable/EdgeSwipeDetector;

    invoke-virtual {v0, p1}, Lcom/android/dreams/phototable/EdgeSwipeDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDragGestureDetector:Lcom/android/dreams/phototable/DragGestureDetector;

    invoke-virtual {v0, p1}, Lcom/android/dreams/phototable/DragGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 401
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mKeyboardInterpreter:Lcom/android/dreams/phototable/KeyboardInterpreter;

    invoke-virtual {v0, p1, p2}, Lcom/android/dreams/phototable/KeyboardInterpreter;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 427
    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 428
    const-string v6, "onLayout (%d, %d, %d, %d)"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    :goto_0
    sub-int v6, p5, p3

    iput v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    .line 434
    sub-int v6, p4, p2

    iput v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    .line 436
    iget v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mImageRatio:F

    iget v7, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    iget v8, p0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mLongSide:I

    .line 437
    iget v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mImageRatio:F

    iget v7, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    iget v8, p0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mShortSide:I

    .line 439
    iget v6, p0, Lcom/android/dreams/phototable/PhotoTable;->mWidth:I

    iget v7, p0, Lcom/android/dreams/phototable/PhotoTable;->mHeight:I

    if-le v6, v7, :cond_1

    .line 440
    .local v2, "isLandscape":Z
    :goto_1
    iget-boolean v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mIsLandscape:Z

    if-eq v5, v2, :cond_5

    .line 441
    iget-object v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 442
    .local v3, "photo":Landroid/view/View;
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getSelection()Landroid/view/View;

    move-result-object v5

    if-eq v3, v5, :cond_0

    .line 443
    invoke-virtual {p0, v3}, Lcom/android/dreams/phototable/PhotoTable;->dropOnTable(Landroid/view/View;)V

    goto :goto_2

    .line 429
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "isLandscape":Z
    .end local v3    # "photo":Landroid/view/View;
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    move v2, v5

    .line 439
    goto :goto_1

    .line 446
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "isLandscape":Z
    :cond_2
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 447
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getSelection()Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/dreams/phototable/PhotoTable;->pickUp(Landroid/view/View;)V

    .line 448
    const/4 v4, 0x0

    .local v4, "slot":I
    :goto_3
    iget-object v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    array-length v5, v5

    if-ge v4, v5, :cond_4

    .line 449
    iget-object v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v5, v5, v4

    if-eqz v5, :cond_3

    .line 450
    iget-object v5, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v5, v5, v4

    invoke-direct {p0, v5, v4}, Lcom/android/dreams/phototable/PhotoTable;->placeOnDeck(Landroid/view/View;I)V

    .line 448
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 454
    .end local v4    # "slot":I
    :cond_4
    iput-boolean v2, p0, Lcom/android/dreams/phototable/PhotoTable;->mIsLandscape:Z

    .line 456
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->start()V

    .line 457
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 411
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_2

    .line 412
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 413
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->clearSelection()V

    .line 419
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 421
    :goto_1
    return v0

    .line 415
    :cond_1
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mTapToExit:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDream:Landroid/service/dreams/DreamService;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mDream:Landroid/service/dreams/DreamService;

    invoke-virtual {v0}, Landroid/service/dreams/DreamService;->finish()V

    goto :goto_0

    .line 421
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public photoOffTable(Landroid/view/View;)Z
    .locals 1
    .param p1, "photo"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 834
    invoke-virtual {p0, p1, v0, v0}, Lcom/android/dreams/phototable/PhotoTable;->photoOffTable(Landroid/view/View;II)Z

    move-result v0

    return v0
.end method

.method public photoOffTable(Landroid/view/View;II)Z
    .locals 4
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    const/4 v3, 0x0

    .line 838
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 839
    .local v0, "hit":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 840
    invoke-virtual {v0, p2, p3}, Landroid/graphics/Rect;->offset(II)V

    .line 841
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getHeight()I

    move-result v2

    if-gt v1, v2, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public refreshFocus()V
    .locals 1

    .prologue
    .line 1044
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mMaxFocusTime:I

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->scheduleFocusReaper(I)V

    .line 1045
    return-void
.end method

.method public refreshSelection()V
    .locals 1

    .prologue
    .line 1035
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mMaxFocusTime:I

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->scheduleSelectionReaper(I)V

    .line 1036
    return-void
.end method

.method public scheduleFocusReaper(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocusReaper:Lcom/android/dreams/phototable/PhotoTable$FocusReaper;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1049
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocusReaper:Lcom/android/dreams/phototable/PhotoTable$FocusReaper;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/dreams/phototable/PhotoTable;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1050
    return-void
.end method

.method public scheduleNext(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLauncher:Lcom/android/dreams/phototable/PhotoTable$Launcher;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1054
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mLauncher:Lcom/android/dreams/phototable/PhotoTable$Launcher;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/dreams/phototable/PhotoTable;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1055
    return-void
.end method

.method public scheduleSelectionReaper(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelectionReaper:Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1040
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelectionReaper:Lcom/android/dreams/phototable/PhotoTable$SelectionReaper;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/dreams/phototable/PhotoTable;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1041
    return-void
.end method

.method public selectNext()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 224
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStoryModeEnabled:Z

    if-eqz v0, :cond_1

    .line 225
    const-string v0, "selectNext"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-direct {p0, v0, v3}, Lcom/android/dreams/phototable/PhotoTable;->placeOnDeck(Landroid/view/View;I)V

    .line 228
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    .line 229
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    .line 230
    invoke-direct {p0}, Lcom/android/dreams/phototable/PhotoTable;->promoteSelection()V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->clearSelection()V

    goto :goto_0
.end method

.method public selectPrevious()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStoryModeEnabled:Z

    if-eqz v0, :cond_1

    .line 239
    const-string v0, "selectPrevious"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->hasSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->placeOnDeck(Landroid/view/View;I)V

    .line 242
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    .line 243
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnDeck:[Landroid/view/View;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    .line 244
    invoke-direct {p0}, Lcom/android/dreams/phototable/PhotoTable;->promoteSelection()V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->clearSelection()V

    goto :goto_0
.end method

.method public setDefaultFocus()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mOnTable:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->setFocus(Landroid/view/View;)V

    .line 292
    :cond_0
    return-void
.end method

.method public setDream(Landroid/service/dreams/DreamService;)V
    .locals 0
    .param p1, "dream"    # Landroid/service/dreams/DreamService;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/android/dreams/phototable/PhotoTable;->mDream:Landroid/service/dreams/DreamService;

    .line 180
    return-void
.end method

.method public setFocus(Landroid/view/View;)V
    .locals 1
    .param p1, "focus"    # Landroid/view/View;

    .prologue
    .line 295
    sget-boolean v0, Lcom/android/dreams/phototable/PhotoTable;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->clearFocus()V

    .line 297
    iput-object p1, p0, Lcom/android/dreams/phototable/PhotoTable;->mFocus:Landroid/view/View;

    .line 298
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/PhotoTable;->moveToTopOfPile(Landroid/view/View;)V

    .line 299
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/dreams/phototable/PhotoTable;->setHighlight(Landroid/view/View;Z)V

    .line 300
    iget v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mMaxFocusTime:I

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->scheduleFocusReaper(I)V

    .line 301
    return-void
.end method

.method public setHighlight(Landroid/view/View;Z)V
    .locals 5
    .param p1, "photo"    # Landroid/view/View;
    .param p2, "highlighted"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1016
    move-object v0, p1

    check-cast v0, Landroid/widget/ImageView;

    .line 1017
    .local v0, "image":Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 1018
    .local v1, "layers":Landroid/graphics/drawable/LayerDrawable;
    if-eqz p2, :cond_0

    .line 1019
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v3, p0, Lcom/android/dreams/phototable/PhotoTable;->mHighlightColor:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1023
    :goto_0
    return-void

    .line 1021
    :cond_0
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_0
.end method

.method public setSelection(Landroid/view/View;)V
    .locals 1
    .param p1, "selected"    # Landroid/view/View;

    .prologue
    .line 213
    if-eqz p1, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTable;->clearSelection()V

    .line 215
    iput-object p1, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    .line 216
    invoke-direct {p0}, Lcom/android/dreams/phototable/PhotoTable;->promoteSelection()V

    .line 217
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStoryModeEnabled:Z

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mSelection:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTable;->fadeOutBackground(Landroid/view/View;)V

    .line 221
    :cond_0
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1027
    iget-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStarted:Z

    if-nez v0, :cond_0

    .line 1028
    const-string v0, "kick it"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoTable;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1029
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dreams/phototable/PhotoTable;->mStarted:Z

    .line 1030
    invoke-virtual {p0, v2}, Lcom/android/dreams/phototable/PhotoTable;->scheduleNext(I)V

    .line 1032
    :cond_0
    return-void
.end method
