.class public abstract Lcom/android/dreams/phototable/CursorPhotoSource;
.super Lcom/android/dreams/phototable/PhotoSource;
.source "CursorPhotoSource.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/android/dreams/phototable/PhotoSource;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected donePaging(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V
    .locals 1
    .param p1, "current"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .prologue
    .line 106
    iget-object v0, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 109
    :cond_0
    return-void
.end method

.method protected abstract findPosition(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V
.end method

.method protected naturalNext(Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .locals 5
    .param p1, "current"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .prologue
    .line 43
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 44
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/CursorPhotoSource;->openCursor(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V

    .line 47
    :cond_1
    const/4 v0, 0x0

    .line 49
    .local v0, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-nez v3, :cond_2

    move-object v1, v0

    .line 69
    .end local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .local v1, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :goto_0
    return-object v1

    .line 53
    .end local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .restart local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :cond_2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/CursorPhotoSource;->findPosition(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V

    .line 54
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    iget v4, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 55
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 57
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_3

    .line 58
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/android/dreams/phototable/CursorPhotoSource;->unpackImageData(Landroid/database/Cursor;Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-result-object v0

    .line 59
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    iput-object v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    .line 60
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iput v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    .line 61
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->uri:Landroid/net/Uri;

    iput-object v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->uri:Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    :goto_1
    move-object v1, v0

    .line 69
    .end local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .restart local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    goto :goto_0

    .line 63
    .end local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .restart local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :catch_0
    move-exception v2

    .line 64
    .local v2, "e":Landroid/database/StaleDataException;
    invoke-virtual {v2}, Landroid/database/StaleDataException;->printStackTrace()V

    goto :goto_1

    .line 65
    .end local v2    # "e":Landroid/database/StaleDataException;
    :catch_1
    move-exception v2

    .line 66
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method protected naturalPrevious(Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .locals 5
    .param p1, "current"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .prologue
    .line 74
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/CursorPhotoSource;->openCursor(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V

    .line 78
    :cond_1
    const/4 v0, 0x0

    .line 80
    .local v0, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-nez v3, :cond_2

    move-object v1, v0

    .line 101
    .end local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .local v1, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :goto_0
    return-object v1

    .line 84
    .end local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .restart local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :cond_2
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/CursorPhotoSource;->findPosition(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V

    .line 85
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    iget v4, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 86
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 88
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v3

    if-nez v3, :cond_3

    .line 90
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/android/dreams/phototable/CursorPhotoSource;->unpackImageData(Landroid/database/Cursor;Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-result-object v0

    .line 91
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    iput-object v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    .line 92
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iput v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    .line 93
    iget-object v3, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->uri:Landroid/net/Uri;

    iput-object v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->uri:Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    :goto_1
    move-object v1, v0

    .line 101
    .end local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .restart local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    goto :goto_0

    .line 95
    .end local v1    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .restart local v0    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :catch_0
    move-exception v2

    .line 96
    .local v2, "e":Landroid/database/StaleDataException;
    invoke-virtual {v2}, Landroid/database/StaleDataException;->printStackTrace()V

    goto :goto_1

    .line 97
    .end local v2    # "e":Landroid/database/StaleDataException;
    :catch_1
    move-exception v2

    .line 98
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method protected abstract openCursor(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V
.end method

.method protected abstract unpackImageData(Landroid/database/Cursor;Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;
.end method
