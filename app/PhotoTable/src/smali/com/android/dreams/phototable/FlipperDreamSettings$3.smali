.class Lcom/android/dreams/phototable/FlipperDreamSettings$3;
.super Landroid/os/Handler;
.source "FlipperDreamSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dreams/phototable/FlipperDreamSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;


# direct methods
.method constructor <init>(Lcom/android/dreams/phototable/FlipperDreamSettings;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    .line 116
    iget v1, p1, Landroid/os/Message;->what:I

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->FIND_ALBUMS:I
    invoke-static {}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$300()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 117
    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    invoke-static {v1}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$100(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    move-result-object v1

    new-instance v2, Lcom/android/dreams/phototable/FlipperDreamSettings$3$1;

    invoke-direct {v2, p0}, Lcom/android/dreams/phototable/FlipperDreamSettings$3$1;-><init>(Lcom/android/dreams/phototable/FlipperDreamSettings$3;)V

    invoke-virtual {v1, v2}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 128
    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    iget-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    invoke-static {v2}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$100(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/dreams/phototable/FlipperDreamSettings;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    invoke-virtual {v1}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 130
    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # invokes: Lcom/android/dreams/phototable/FlipperDreamSettings;->updateActionItem()V
    invoke-static {v1}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$400(Lcom/android/dreams/phototable/FlipperDreamSettings;)V

    .line 131
    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    iget-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    invoke-static {v2}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$100(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    # invokes: Lcom/android/dreams/phototable/FlipperDreamSettings;->showApology(Z)V
    invoke-static {v1, v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$500(Lcom/android/dreams/phototable/FlipperDreamSettings;Z)V

    .line 133
    :cond_0
    return-void

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
