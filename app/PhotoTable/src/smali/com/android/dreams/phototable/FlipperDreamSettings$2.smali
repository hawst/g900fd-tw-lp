.class Lcom/android/dreams/phototable/FlipperDreamSettings$2;
.super Landroid/os/Handler;
.source "FlipperDreamSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/dreams/phototable/FlipperDreamSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;


# direct methods
.method constructor <init>(Lcom/android/dreams/phototable/FlipperDreamSettings;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 95
    iget-object v6, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    new-instance v0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    iget-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    iget-object v2, v2, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSettings:Landroid/content/SharedPreferences;

    const v3, 0x7f040003

    const/high16 v4, 0x7f040000

    new-instance v5, Ljava/util/LinkedList;

    iget-object v7, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSourcePlexor;
    invoke-static {v7}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$200(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/PhotoSourcePlexor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/dreams/phototable/PhotoSourcePlexor;->findAlbums()Ljava/util/Collection;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-direct/range {v0 .. v5}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;IILjava/util/List;)V

    # setter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    invoke-static {v6, v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$102(Lcom/android/dreams/phototable/FlipperDreamSettings;Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    .line 100
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;->this$0:Lcom/android/dreams/phototable/FlipperDreamSettings;

    iget-object v0, v0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandler:Landroid/os/Handler;

    # getter for: Lcom/android/dreams/phototable/FlipperDreamSettings;->FIND_ALBUMS:I
    invoke-static {}, Lcom/android/dreams/phototable/FlipperDreamSettings;->access$300()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 101
    return-void
.end method
