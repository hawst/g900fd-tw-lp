.class public Lcom/android/dreams/phototable/FlipperDreamSettings;
.super Landroid/app/ListActivity;
.source "FlipperDreamSettings.java"


# static fields
.field private static FIND_ALBUMS:I


# instance fields
.field private mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

.field private mAdapterHandler:Landroid/os/Handler;

.field private mContentObserver:Landroid/database/ContentObserver;

.field mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mPhotoSource:Lcom/android/dreams/phototable/PhotoSourcePlexor;

.field private mSelectAll:Landroid/view/MenuItem;

.field private mSelectedViewPostions:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mSettings:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput v0, Lcom/android/dreams/phototable/FlipperDreamSettings;->FIND_ALBUMS:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;

    .line 56
    new-instance v0, Lcom/android/dreams/phototable/FlipperDreamSettings$1;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/FlipperDreamSettings$1;-><init>(Lcom/android/dreams/phototable/FlipperDreamSettings;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    .line 114
    new-instance v0, Lcom/android/dreams/phototable/FlipperDreamSettings$3;

    invoke-direct {v0, p0}, Lcom/android/dreams/phototable/FlipperDreamSettings$3;-><init>(Lcom/android/dreams/phototable/FlipperDreamSettings;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/dreams/phototable/FlipperDreamSettings;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectedViewPostions:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/dreams/phototable/FlipperDreamSettings;Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;)Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;
    .param p1, "x1"    # Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/dreams/phototable/FlipperDreamSettings;)Lcom/android/dreams/phototable/PhotoSourcePlexor;
    .locals 1
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSourcePlexor;

    return-object v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/android/dreams/phototable/FlipperDreamSettings;->FIND_ALBUMS:I

    return v0
.end method

.method static synthetic access$400(Lcom/android/dreams/phototable/FlipperDreamSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->updateActionItem()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/dreams/phototable/FlipperDreamSettings;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/FlipperDreamSettings;->showApology(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/dreams/phototable/FlipperDreamSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/dreams/phototable/FlipperDreamSettings;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->startAdapter()V

    return-void
.end method

.method private showApology(Z)V
    .locals 5
    .param p1, "apologize"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 185
    const v2, 0x7f0b000b

    invoke-virtual {p0, v2}, Lcom/android/dreams/phototable/FlipperDreamSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 186
    .local v0, "empty":Landroid/view/View;
    const v2, 0x7f0b000d

    invoke-virtual {p0, v2}, Lcom/android/dreams/phototable/FlipperDreamSettings;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 187
    .local v1, "sorry":Landroid/view/View;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 188
    if-eqz p1, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 189
    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 191
    :cond_0
    return-void

    :cond_1
    move v2, v4

    .line 188
    goto :goto_0

    :cond_2
    move v4, v3

    .line 189
    goto :goto_1
.end method

.method private startAdapter()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapterHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapterHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 139
    return-void
.end method

.method private updateActionItem()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 199
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->areAllSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    const v1, 0x7f0c0009

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 203
    :cond_3
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    const v1, 0x7f0c0008

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method protected init()V
    .locals 6

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 143
    .local v0, "bar":Landroid/app/ActionBar;
    const/16 v1, 0x8

    .line 144
    .local v1, "flags":I
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 146
    new-instance v2, Lcom/android/dreams/phototable/PhotoSourcePlexor;

    iget-object v3, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSettings:Landroid/content/SharedPreferences;

    invoke-direct {v2, p0, v3}, Lcom/android/dreams/phototable/PhotoSourcePlexor;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mPhotoSource:Lcom/android/dreams/phototable/PhotoSourcePlexor;

    .line 148
    iget-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandler:Landroid/os/Handler;

    sget v3, Lcom/android/dreams/phototable/FlipperDreamSettings;->FIND_ALBUMS:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 150
    invoke-direct {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->startAdapter()V

    .line 152
    new-instance v2, Lcom/android/dreams/phototable/FlipperDreamSettings$4;

    new-instance v3, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3}, Lcom/android/dreams/phototable/FlipperDreamSettings$4;-><init>(Lcom/android/dreams/phototable/FlipperDreamSettings;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mContentObserver:Landroid/database/ContentObserver;

    .line 159
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 162
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    const-string v0, "FlipperDream"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSettings:Landroid/content/SharedPreferences;

    .line 90
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FlipperDreamSettings"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandlerThread:Landroid/os/HandlerThread;

    .line 91
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 92
    new-instance v0, Lcom/android/dreams/phototable/FlipperDreamSettings$2;

    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/dreams/phototable/FlipperDreamSettings$2;-><init>(Lcom/android/dreams/phototable/FlipperDreamSettings;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapterHandler:Landroid/os/Handler;

    .line 103
    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->setContentView(I)V

    .line 104
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 106
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 167
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 168
    const v1, 0x7f0b0012

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mSelectAll:Landroid/view/MenuItem;

    .line 169
    invoke-direct {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->updateActionItem()V

    .line 170
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 214
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapterHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 215
    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 216
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 217
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 175
    if-eqz p1, :cond_1

    .line 176
    const v0, 0x7f0b0012

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 177
    iget-object v2, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    iget-object v0, p0, Lcom/android/dreams/phototable/FlipperDreamSettings;->mAdapter:Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->areAllSelected()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->selectAll(Z)V

    .line 181
    :goto_1
    return v1

    .line 177
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 181
    :cond_1
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 111
    invoke-virtual {p0}, Lcom/android/dreams/phototable/FlipperDreamSettings;->init()V

    .line 112
    return-void
.end method
