.class public abstract Lcom/android/dreams/phototable/PhotoSource;
.super Ljava/lang/Object;
.source "PhotoSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dreams/phototable/PhotoSource$AlbumData;,
        Lcom/android/dreams/phototable/PhotoSource$ImageData;
    }
.end annotation


# instance fields
.field private final mBadImageSkipLimit:I

.field protected final mContext:Landroid/content/Context;

.field private final mFallbackSource:Lcom/android/dreams/phototable/PhotoSource;

.field private final mImageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/graphics/Bitmap;",
            "Lcom/android/dreams/phototable/PhotoSource$ImageData;",
            ">;"
        }
    .end annotation
.end field

.field private final mImageQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$ImageData;",
            ">;"
        }
    .end annotation
.end field

.field private final mMaxCropRatio:F

.field private final mMaxQueueSize:I

.field protected final mRNG:Ljava/util/Random;

.field protected final mResolver:Landroid/content/ContentResolver;

.field protected final mResources:Landroid/content/res/Resources;

.field protected final mSettings:Lcom/android/dreams/phototable/AlbumSettings;

.field protected mSourceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;

    .prologue
    .line 104
    new-instance v0, Lcom/android/dreams/phototable/StockSource;

    invoke-direct {v0, p1, p2}, Lcom/android/dreams/phototable/StockSource;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/android/dreams/phototable/PhotoSource;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/android/dreams/phototable/PhotoSource;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/android/dreams/phototable/PhotoSource;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;
    .param p3, "fallbackSource"    # Lcom/android/dreams/phototable/PhotoSource;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const-string v0, "PhotoTable.PhotoSource"

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mSourceName:Ljava/lang/String;

    .line 109
    iput-object p1, p0, Lcom/android/dreams/phototable/PhotoSource;->mContext:Landroid/content/Context;

    .line 110
    invoke-static {p2}, Lcom/android/dreams/phototable/AlbumSettings;->getAlbumSettings(Landroid/content/SharedPreferences;)Lcom/android/dreams/phototable/AlbumSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    .line 111
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mResolver:Landroid/content/ContentResolver;

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mResources:Landroid/content/res/Resources;

    .line 113
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    .line 114
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f08000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mMaxQueueSize:I

    .line 115
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f08000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x49742400    # 1000000.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mMaxCropRatio:F

    .line 116
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mBadImageSkipLimit:I

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    .line 118
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mRNG:Ljava/util/Random;

    .line 119
    iput-object p3, p0, Lcom/android/dreams/phototable/PhotoSource;->mFallbackSource:Lcom/android/dreams/phototable/PhotoSource;

    .line 120
    return-void
.end method

.method protected static log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 288
    return-void
.end method


# virtual methods
.method public donePaging(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "current"    # Landroid/graphics/Bitmap;

    .prologue
    .line 327
    iget-object v1, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .line 328
    .local v0, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {v0}, Lcom/android/dreams/phototable/PhotoSource$ImageData;->donePaging()V

    .line 331
    :cond_0
    return-void
.end method

.method protected abstract donePaging(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V
.end method

.method protected fillQueue()V
    .locals 3

    .prologue
    .line 123
    const-string v0, "PhotoTable.PhotoSource"

    const-string v1, "filling queue"

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    iget v1, p0, Lcom/android/dreams/phototable/PhotoSource;->mMaxQueueSize:I

    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/android/dreams/phototable/PhotoSource;->findImages(I)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 125
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 126
    const-string v0, "PhotoTable.PhotoSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queue contains: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public abstract findAlbums()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract findImages(I)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$ImageData;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getStream(Lcom/android/dreams/phototable/PhotoSource$ImageData;I)Ljava/io/InputStream;
.end method

.method public load(Lcom/android/dreams/phototable/PhotoSource$ImageData;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    .locals 26
    .param p1, "data"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "longSide"    # I
    .param p4, "shortSide"    # I

    .prologue
    .line 165
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decoding photo resource to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/PhotoSource$ImageData;->getStream(I)Ljava/io/InputStream;

    move-result-object v13

    .line 167
    .local v13, "is":Ljava/io/InputStream;
    if-nez v13, :cond_1

    .line 168
    const/4 v2, 0x0

    .line 277
    :cond_0
    :goto_0
    return-object v2

    .line 170
    :cond_1
    const/4 v2, 0x0

    .line 172
    .local v2, "image":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v9, Ljava/io/BufferedInputStream;

    invoke-direct {v9, v13}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 173
    .local v9, "bis":Ljava/io/BufferedInputStream;
    const/high16 v3, 0x40000

    invoke-virtual {v9, v3}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 175
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 176
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 177
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v3, v4, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 178
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 179
    .local v19, "rawLongSide":I
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 180
    .local v20, "rawShortSide":I
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "I see bounds of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_b

    const/4 v3, -0x1

    move/from16 v0, v20

    if-eq v0, v3, :cond_b

    .line 183
    move/from16 v0, p3

    int-to-float v3, v0

    move/from16 v0, v19

    int-to-float v4, v0

    div-float/2addr v3, v4

    move/from16 v0, p4

    int-to-float v4, v0

    move/from16 v0, v20

    int-to-float v5, v0

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v11

    .line 185
    .local v11, "insideRatio":F
    move/from16 v0, p3

    int-to-float v3, v0

    move/from16 v0, v19

    int-to-float v4, v0

    div-float/2addr v3, v4

    move/from16 v0, p4

    int-to-float v4, v0

    move/from16 v0, v20

    int-to-float v5, v0

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 187
    .local v17, "outsideRatio":F
    div-float v3, v17, v11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/dreams/phototable/PhotoSource;->mMaxCropRatio:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    move/from16 v18, v17

    .line 190
    .local v18, "ratio":F
    :goto_1
    move/from16 v0, v18

    float-to-double v4, v0

    const-wide/high16 v24, 0x3fe0000000000000L    # 0.5

    cmpg-double v3, v4, v24

    if-gez v3, :cond_3

    .line 191
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 192
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v18, v18, v3

    goto :goto_1

    .end local v18    # "ratio":F
    :cond_2
    move/from16 v18, v11

    .line 187
    goto :goto_1

    .line 195
    .restart local v18    # "ratio":F
    :cond_3
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decoding with inSampleSize "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :try_start_1
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :goto_2
    const/4 v3, 0x0

    :try_start_2
    move-object/from16 v0, p2

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 206
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-static {v9, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 207
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 208
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 209
    if-eqz v2, :cond_a

    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_a

    const/4 v3, -0x1

    move/from16 v0, v20

    if-eq v0, v3, :cond_a

    .line 210
    move/from16 v0, p3

    int-to-float v3, v0

    move/from16 v0, v19

    int-to-float v4, v0

    div-float/2addr v3, v4

    move/from16 v0, p4

    int-to-float v4, v0

    move/from16 v0, v20

    int-to-float v5, v0

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v18

    .line 213
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v18, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v4, v3

    const-wide v24, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v3, v4, v24

    if-lez v3, :cond_6

    .line 214
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "still too big, scaling down by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    move-object/from16 v0, p2

    iget v15, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 216
    .local v15, "oldOutWidth":I
    move-object/from16 v0, p2

    iget v14, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 217
    .local v14, "oldOutHeight":I
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    float-to-int v3, v3

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 218
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    float-to-int v3, v3

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 219
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gtz v3, :cond_4

    .line 220
    move-object/from16 v0, p2

    iput v15, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 222
    :cond_4
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v3, :cond_5

    .line 223
    move-object/from16 v0, p2

    iput v14, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 226
    :cond_5
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 231
    .end local v14    # "oldOutHeight":I
    .end local v15    # "oldOutWidth":I
    :cond_6
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I

    if-eqz v3, :cond_8

    .line 232
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rotated by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": fixing"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 234
    .local v7, "matrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->floor(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v5, v0

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 237
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 240
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I

    const/16 v4, 0x5a

    if-eq v3, v4, :cond_7

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I

    const/16 v4, 0x10e

    if-ne v3, v4, :cond_8

    .line 241
    :cond_7
    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v22, v0

    .line 242
    .local v22, "tmp":I
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 243
    move/from16 v0, v22

    move-object/from16 v1, p2

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 247
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v22    # "tmp":I
    :cond_8
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "returning bitmap "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    .end local v11    # "insideRatio":F
    .end local v17    # "outsideRatio":F
    .end local v18    # "ratio":F
    :goto_3
    if-nez v2, :cond_9

    .line 255
    const-string v4, "PhotoTable.PhotoSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stream decoding failed with no error"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    iget-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-eqz v3, :cond_c

    const-string v3, " due to cancelation."

    :goto_4
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 269
    :cond_9
    if-eqz v13, :cond_0

    .line 270
    :try_start_3
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 272
    :catch_0
    move-exception v21

    .line 273
    .local v21, "t":Ljava/lang/Throwable;
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "close fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 198
    .end local v21    # "t":Ljava/lang/Throwable;
    .restart local v11    # "insideRatio":F
    .restart local v17    # "outsideRatio":F
    .restart local v18    # "ratio":F
    :catch_1
    move-exception v12

    .line 200
    .local v12, "ioe":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v9}, Ljava/io/BufferedInputStream;->close()V

    .line 201
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/PhotoSource$ImageData;->getStream(I)Ljava/io/InputStream;

    move-result-object v13

    .line 202
    new-instance v9, Ljava/io/BufferedInputStream;

    .end local v9    # "bis":Ljava/io/BufferedInputStream;
    invoke-direct {v9, v13}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 203
    .restart local v9    # "bis":Ljava/io/BufferedInputStream;
    const-string v3, "PhotoTable.PhotoSource"

    const-string v4, "resetting the stream"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 258
    .end local v9    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "insideRatio":F
    .end local v12    # "ioe":Ljava/io/IOException;
    .end local v17    # "outsideRatio":F
    .end local v18    # "ratio":F
    .end local v19    # "rawLongSide":I
    .end local v20    # "rawShortSide":I
    :catch_2
    move-exception v16

    .line 259
    .local v16, "ome":Ljava/lang/OutOfMemoryError;
    :try_start_5
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OUT OF MEMORY: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 260
    const/4 v2, 0x0

    .line 269
    if-eqz v13, :cond_0

    .line 270
    :try_start_6
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 272
    :catch_3
    move-exception v21

    .line 273
    .restart local v21    # "t":Ljava/lang/Throwable;
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "close fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 249
    .end local v16    # "ome":Ljava/lang/OutOfMemoryError;
    .end local v21    # "t":Ljava/lang/Throwable;
    .restart local v9    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "insideRatio":F
    .restart local v17    # "outsideRatio":F
    .restart local v18    # "ratio":F
    .restart local v19    # "rawLongSide":I
    .restart local v20    # "rawShortSide":I
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 252
    .end local v11    # "insideRatio":F
    .end local v17    # "outsideRatio":F
    .end local v18    # "ratio":F
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 255
    :cond_c
    :try_start_7
    const-string v3, "."
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_4

    .line 261
    .end local v9    # "bis":Ljava/io/BufferedInputStream;
    .end local v19    # "rawLongSide":I
    .end local v20    # "rawShortSide":I
    :catch_4
    move-exception v10

    .line 262
    .local v10, "fnf":Ljava/io/FileNotFoundException;
    :try_start_8
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 263
    const/4 v2, 0x0

    .line 269
    if-eqz v13, :cond_0

    .line 270
    :try_start_9
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_0

    .line 272
    :catch_5
    move-exception v21

    .line 273
    .restart local v21    # "t":Ljava/lang/Throwable;
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "close fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 264
    .end local v10    # "fnf":Ljava/io/FileNotFoundException;
    .end local v21    # "t":Ljava/lang/Throwable;
    :catch_6
    move-exception v12

    .line 265
    .restart local v12    # "ioe":Ljava/io/IOException;
    :try_start_a
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i/o exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 266
    const/4 v2, 0x0

    .line 269
    if-eqz v13, :cond_0

    .line 270
    :try_start_b
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_7

    goto/16 :goto_0

    .line 272
    :catch_7
    move-exception v21

    .line 273
    .restart local v21    # "t":Ljava/lang/Throwable;
    const-string v3, "PhotoTable.PhotoSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "close fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 268
    .end local v12    # "ioe":Ljava/io/IOException;
    .end local v21    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    .line 269
    if-eqz v13, :cond_d

    .line 270
    :try_start_c
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_8

    .line 274
    :cond_d
    :goto_5
    throw v3

    .line 272
    :catch_8
    move-exception v21

    .line 273
    .restart local v21    # "t":Ljava/lang/Throwable;
    const-string v4, "PhotoTable.PhotoSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "close fail: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public naturalNext(Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "current"    # Landroid/graphics/Bitmap;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "longSide"    # I
    .param p4, "shortSide"    # I

    .prologue
    .line 300
    const/4 v1, 0x0

    .line 301
    .local v1, "image":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .line 302
    .local v0, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/android/dreams/phototable/PhotoSource$ImageData;->naturalNext()Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-result-object v2

    .line 304
    .local v2, "next":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    if-eqz v2, :cond_0

    .line 305
    invoke-virtual {p0, v2, p2, p3, p4}, Lcom/android/dreams/phototable/PhotoSource;->load(Lcom/android/dreams/phototable/PhotoSource$ImageData;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 306
    iget-object v3, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    .end local v2    # "next":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :cond_0
    return-object v1
.end method

.method protected abstract naturalNext(Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;
.end method

.method public naturalPrevious(Landroid/graphics/Bitmap;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "current"    # Landroid/graphics/Bitmap;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "longSide"    # I
    .param p4, "shortSide"    # I

    .prologue
    .line 314
    const/4 v1, 0x0

    .line 315
    .local v1, "image":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .line 316
    .local v0, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {v0}, Lcom/android/dreams/phototable/PhotoSource$ImageData;->naturalPrevious()Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-result-object v2

    .line 318
    .local v2, "prev":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    if-eqz v2, :cond_0

    .line 319
    invoke-virtual {p0, v2, p2, p3, p4}, Lcom/android/dreams/phototable/PhotoSource;->load(Lcom/android/dreams/phototable/PhotoSource$ImageData;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 320
    iget-object v3, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    .end local v2    # "prev":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :cond_0
    return-object v1
.end method

.method protected abstract naturalPrevious(Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;
.end method

.method public next(Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "longSide"    # I
    .param p3, "shortSide"    # I

    .prologue
    .line 130
    const-string v4, "PhotoTable.PhotoSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "decoding a picasa resource to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/dreams/phototable/PhotoSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v1, 0x0

    .line 132
    .local v1, "image":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 133
    .local v2, "imageData":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    const/4 v3, 0x0

    .line 135
    .local v3, "tries":I
    :goto_0
    if-nez v1, :cond_3

    iget v4, p0, Lcom/android/dreams/phototable/PhotoSource;->mBadImageSkipLimit:I

    if-ge v3, v4, :cond_3

    .line 136
    iget-object v5, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    monitor-enter v5

    .line 137
    :try_start_0
    iget-object v4, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoSource;->fillQueue()V

    .line 140
    :cond_0
    iget-object v4, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageQueue:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-object v2, v0

    .line 141
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    if-eqz v2, :cond_2

    .line 143
    invoke-virtual {p0, v2, p1, p2, p3}, Lcom/android/dreams/phototable/PhotoSource;->load(Lcom/android/dreams/phototable/PhotoSource$ImageData;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 144
    if-nez v1, :cond_1

    .line 146
    iget v3, p0, Lcom/android/dreams/phototable/PhotoSource;->mBadImageSkipLimit:I

    .line 147
    goto :goto_0

    .line 141
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 149
    :cond_1
    iget-object v4, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const/4 v2, 0x0

    .line 153
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 156
    :cond_3
    if-nez v1, :cond_4

    iget-object v4, p0, Lcom/android/dreams/phototable/PhotoSource;->mFallbackSource:Lcom/android/dreams/phototable/PhotoSource;

    if-eqz v4, :cond_4

    .line 157
    iget-object v4, p0, Lcom/android/dreams/phototable/PhotoSource;->mFallbackSource:Lcom/android/dreams/phototable/PhotoSource;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/dreams/phototable/PhotoSource;->findImages(I)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    check-cast v4, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    invoke-virtual {p0, v4, p1, p2, p3}, Lcom/android/dreams/phototable/PhotoSource;->load(Lcom/android/dreams/phototable/PhotoSource$ImageData;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 161
    :cond_4
    return-object v1
.end method

.method protected pickRandomStart(II)I
    .locals 2
    .param p1, "total"    # I
    .param p2, "max"    # I

    .prologue
    .line 291
    if-lt p2, p1, :cond_0

    .line 292
    const/4 v0, -0x1

    .line 294
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    sub-int v1, p1, p2

    rem-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public recycle(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "trash"    # Landroid/graphics/Bitmap;

    .prologue
    .line 334
    if-eqz p1, :cond_0

    .line 335
    iget-object v0, p0, Lcom/android/dreams/phototable/PhotoSource;->mImageMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 338
    :cond_0
    return-void
.end method
