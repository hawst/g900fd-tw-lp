.class public Lcom/android/dreams/phototable/LocalSource;
.super Lcom/android/dreams/phototable/CursorPhotoSource;
.source "LocalSource.java"


# instance fields
.field private mFoundAlbumIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPosition:I

.field private final mLocalSourceName:Ljava/lang/String;

.field private final mUnknownAlbumName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/android/dreams/phototable/CursorPhotoSource;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 46
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0006

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Photos on Device"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLocalSourceName:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0004

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Unknown"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mUnknownAlbumName:Ljava/lang/String;

    .line 48
    const-string v0, "PhotoTable.LocalSource"

    iput-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mSourceName:Ljava/lang/String;

    .line 49
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    .line 50
    invoke-virtual {p0}, Lcom/android/dreams/phototable/LocalSource;->fillQueue()V

    .line 51
    return-void
.end method

.method public static constructId(ZLjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "internal"    # Z
    .param p1, "bucketId"    # Ljava/lang/String;

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PhotoTable.LocalSource:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p0, :cond_0

    const-string v0, ":i"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private getFoundAlbums()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mFoundAlbumIds:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/android/dreams/phototable/LocalSource;->findAlbums()Ljava/util/Collection;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mFoundAlbumIds:Ljava/util/Set;

    return-object v0
.end method

.method private isInternalId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 254
    const-string v0, "i"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public findAlbums()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const-string v1, "PhotoTable.LocalSource"

    const-string v2, "finding albums"

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 64
    .local v0, "foundAlbums":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/dreams/phototable/PhotoSource$AlbumData;>;"
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/dreams/phototable/LocalSource;->findAlbums(ZLjava/util/HashMap;)V

    .line 67
    const-string v1, "PhotoTable.LocalSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dreams/phototable/LocalSource;->mFoundAlbumIds:Ljava/util/Set;

    .line 69
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    return-object v1
.end method

.method public findAlbums(ZLjava/util/HashMap;)V
    .locals 20
    .param p1, "internal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "foundAlbums":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/dreams/phototable/PhotoSource$AlbumData;>;"
    if-eqz p1, :cond_2

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 75
    .local v3, "uri":Landroid/net/Uri;
    :goto_0
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_data"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "bucket_id"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "bucket_display_name"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "datetaken"

    aput-object v5, v4, v2

    .line 78
    .local v4, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/dreams/phototable/LocalSource;->mResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 79
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    .line 80
    const/4 v2, -0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 82
    const-string v2, "_data"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 83
    .local v11, "dataIndex":I
    const-string v2, "bucket_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 84
    .local v8, "bucketIndex":I
    const-string v2, "bucket_display_name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 85
    .local v13, "nameIndex":I
    const-string v2, "datetaken"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 87
    .local v16, "updatedIndex":I
    if-gez v8, :cond_3

    .line 88
    const-string v2, "PhotoTable.LocalSource"

    const-string v5, "can\'t find the ID column!"

    invoke-static {v2, v5}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 121
    .end local v8    # "bucketIndex":I
    .end local v11    # "dataIndex":I
    .end local v13    # "nameIndex":I
    .end local v16    # "updatedIndex":I
    :cond_1
    return-void

    .line 73
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_2
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 90
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v8    # "bucketIndex":I
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "dataIndex":I
    .restart local v13    # "nameIndex":I
    .restart local v16    # "updatedIndex":I
    :cond_3
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/dreams/phototable/LocalSource;->constructId(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 92
    .local v12, "id":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 93
    .local v10, "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_5

    .line 94
    new-instance v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .end local v10    # "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/android/dreams/phototable/PhotoSource$AlbumData;-><init>(Lcom/android/dreams/phototable/PhotoSource;)V

    .line 95
    .restart local v10    # "data":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    iput-object v12, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    .line 96
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/dreams/phototable/LocalSource;->mLocalSourceName:Ljava/lang/String;

    iput-object v2, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    .line 98
    if-ltz v11, :cond_4

    .line 99
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->thumbnailUrl:Ljava/lang/String;

    .line 102
    :cond_4
    if-ltz v13, :cond_6

    .line 103
    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    .line 108
    :goto_2
    const-string v2, "PhotoTable.LocalSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " found"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    :cond_5
    if-ltz v16, :cond_3

    .line 112
    move/from16 v0, v16

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 113
    .local v14, "updated":J
    iget-wide v6, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->updated:J

    const-wide/16 v18, 0x0

    cmp-long v2, v6, v18

    if-nez v2, :cond_7

    .end local v14    # "updated":J
    :goto_3
    iput-wide v14, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->updated:J

    goto :goto_1

    .line 105
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/dreams/phototable/LocalSource;->mUnknownAlbumName:Ljava/lang/String;

    iput-object v2, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    goto :goto_2

    .line 113
    .restart local v14    # "updated":J
    :cond_7
    iget-wide v6, v10, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->updated:J

    invoke-static {v6, v7, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    goto :goto_3
.end method

.method protected findImages(I)Ljava/util/Collection;
    .locals 6
    .param p1, "howMany"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$ImageData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 194
    const-string v4, "PhotoTable.LocalSource"

    const-string v5, "finding images"

    invoke-static {v4, v5}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 196
    .local v0, "foundImages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/dreams/phototable/PhotoSource$ImageData;>;"
    iget-object v4, p0, Lcom/android/dreams/phototable/LocalSource;->mRNG:Ljava/util/Random;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 197
    .local v1, "internalFirst":Z
    :goto_0
    invoke-virtual {p0, v1, p1, v0}, Lcom/android/dreams/phototable/LocalSource;->findImages(ZILjava/util/LinkedList;)V

    .line 198
    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    sub-int v3, p1, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/android/dreams/phototable/LocalSource;->findImages(ZILjava/util/LinkedList;)V

    .line 199
    const-string v2, "PhotoTable.LocalSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " items."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    return-object v0

    .end local v1    # "internalFirst":Z
    :cond_0
    move v1, v3

    .line 196
    goto :goto_0

    .restart local v1    # "internalFirst":Z
    :cond_1
    move v2, v3

    .line 198
    goto :goto_1
.end method

.method protected findImages(ZILjava/util/LinkedList;)V
    .locals 12
    .param p1, "internal"    # Z
    .param p2, "howMany"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$ImageData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p3, "foundImages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/dreams/phototable/PhotoSource$ImageData;>;"
    if-eqz p1, :cond_2

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 206
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "orientation"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string v4, "bucket_id"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    const-string v4, "bucket_display_name"

    aput-object v4, v2, v0

    .line 208
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, ""

    .line 209
    .local v3, "selection":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/dreams/phototable/LocalSource;->getFoundAlbums()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 210
    .local v10, "id":Ljava/lang/String;
    invoke-direct {p0, v10}, Lcom/android/dreams/phototable/LocalSource;->isInternalId(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    invoke-virtual {v0, v10}, Lcom/android/dreams/phototable/AlbumSettings;->isAlbumEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const-string v0, ":"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 212
    .local v11, "parts":[Ljava/lang/String;
    array-length v0, v11

    const/4 v4, 0x1

    if-le v0, v4, :cond_0

    .line 213
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " OR "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 216
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "bucket_id = \'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 204
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "parts":[Ljava/lang/String;
    :cond_2
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto/16 :goto_0

    .line 220
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 251
    :cond_4
    :goto_2
    return-void

    .line 223
    :cond_5
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 224
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 225
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 227
    .local v8, "dataIndex":I
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, p2, :cond_6

    iget v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    const/4 v4, -0x2

    if-ne v0, v4, :cond_6

    .line 228
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/android/dreams/phototable/LocalSource;->pickRandomStart(II)I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    .line 230
    :cond_6
    iget v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 232
    if-gez v8, :cond_8

    .line 233
    const-string v0, "PhotoTable.LocalSource"

    const-string v4, "can\'t find the DATA column!"

    invoke-static {v0, v4}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_7
    :goto_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 235
    :cond_8
    :goto_4
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v0, p2, :cond_9

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 236
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lcom/android/dreams/phototable/LocalSource;->unpackImageData(Landroid/database/Cursor;Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-result-object v7

    .line 237
    .local v7, "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    iput-object v1, v7, Lcom/android/dreams/phototable/PhotoSource$ImageData;->uri:Landroid/net/Uri;

    .line 238
    invoke-virtual {p3, v7}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    .line 239
    invoke-interface {v6}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iput v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    goto :goto_4

    .line 241
    .end local v7    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :cond_9
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 242
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    .line 244
    :cond_a
    invoke-interface {v6}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 245
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLastPosition:I

    goto :goto_3
.end method

.method protected findPosition(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V
    .locals 5
    .param p1, "data"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .prologue
    const/4 v4, -0x1

    .line 146
    iget v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    if-ne v2, v4, :cond_3

    .line 147
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-nez v2, :cond_0

    .line 148
    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/LocalSource;->openCursor(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V

    .line 150
    :cond_0
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    if-eqz v2, :cond_3

    .line 151
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    const-string v3, "_data"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 152
    .local v0, "dataIndex":I
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v2, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 153
    :cond_1
    :goto_0
    iget v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    if-ne v2, v4, :cond_2

    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 154
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "url":Ljava/lang/String;
    if-eqz v1, :cond_1

    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 156
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    goto :goto_0

    .line 159
    .end local v1    # "url":Ljava/lang/String;
    :cond_2
    iget v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    if-ne v2, v4, :cond_3

    .line 161
    const/4 v2, -0x2

    iput v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    .line 165
    .end local v0    # "dataIndex":I
    :cond_3
    return-void
.end method

.method protected getStream(Lcom/android/dreams/phototable/PhotoSource$ImageData;I)Ljava/io/InputStream;
    .locals 5
    .param p1, "data"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .param p2, "longSide"    # I

    .prologue
    .line 259
    const/4 v1, 0x0

    .line 261
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    const-string v2, "PhotoTable.LocalSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "opening:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fis":Ljava/io/FileInputStream;
    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :goto_0
    return-object v1

    .line 263
    .end local v1    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 264
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "PhotoTable.LocalSource"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const/4 v1, 0x0

    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method protected openCursor(Lcom/android/dreams/phototable/PhotoSource$ImageData;)V
    .locals 8
    .param p1, "data"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .prologue
    const/4 v7, 0x0

    .line 129
    const-string v0, "PhotoTable.LocalSource"

    const-string v1, "opening single album"

    invoke-static {v0, v1}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "orientation"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "bucket_id"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "bucket_display_name"

    aput-object v1, v2, v0

    .line 133
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->albumId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 136
    .local v3, "selection":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->uri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v6

    .line 138
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "PhotoTable.LocalSource"

    const-string v1, "fail to open cursor - so cursor is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iput-object v7, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    .line 140
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected unpackImageData(Landroid/database/Cursor;Lcom/android/dreams/phototable/PhotoSource$ImageData;)Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "data"    # Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .prologue
    .line 169
    if-nez p2, :cond_0

    .line 170
    new-instance p2, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    .end local p2    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    invoke-direct {p2, p0}, Lcom/android/dreams/phototable/PhotoSource$ImageData;-><init>(Lcom/android/dreams/phototable/PhotoSource;)V

    .line 174
    .restart local p2    # "data":Lcom/android/dreams/phototable/PhotoSource$ImageData;
    :cond_0
    :try_start_0
    const-string v4, "_data"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 175
    .local v1, "dataIndex":I
    const-string v4, "orientation"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 176
    .local v3, "orientationIndex":I
    const-string v4, "bucket_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 178
    .local v0, "bucketIndex":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p2, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    .line 179
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p2, Lcom/android/dreams/phototable/PhotoSource$ImageData;->albumId:Ljava/lang/String;

    .line 180
    const/4 v4, -0x1

    iput v4, p2, Lcom/android/dreams/phototable/PhotoSource$ImageData;->position:I

    .line 181
    const/4 v4, 0x0

    iput-object v4, p2, Lcom/android/dreams/phototable/PhotoSource$ImageData;->cursor:Landroid/database/Cursor;

    .line 182
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, p2, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_1

    .line 189
    .end local v0    # "bucketIndex":I
    .end local v1    # "dataIndex":I
    .end local v3    # "orientationIndex":I
    :goto_0
    return-object p2

    .line 183
    :catch_0
    move-exception v2

    .line 184
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 185
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v2

    .line 186
    .local v2, "e":Landroid/database/StaleDataException;
    invoke-virtual {v2}, Landroid/database/StaleDataException;->printStackTrace()V

    goto :goto_0
.end method
