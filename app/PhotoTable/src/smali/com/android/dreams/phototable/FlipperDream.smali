.class public Lcom/android/dreams/phototable/FlipperDream;
.super Landroid/service/dreams/DreamService;
.source "FlipperDream.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 36
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    .line 37
    const-string v6, "FlipperDream"

    invoke-virtual {p0, v6, v7}, Lcom/android/dreams/phototable/FlipperDream;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-static {v6}, Lcom/android/dreams/phototable/AlbumSettings;->getAlbumSettings(Landroid/content/SharedPreferences;)Lcom/android/dreams/phototable/AlbumSettings;

    move-result-object v4

    .line 39
    .local v4, "settings":Lcom/android/dreams/phototable/AlbumSettings;
    new-instance v3, Lcom/android/dreams/phototable/PhotoSourcePlexor;

    const-string v6, "FlipperDream"

    invoke-virtual {p0, v6, v7}, Lcom/android/dreams/phototable/FlipperDream;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-direct {v3, p0, v6}, Lcom/android/dreams/phototable/PhotoSourcePlexor;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 40
    .local v3, "plexor":Lcom/android/dreams/phototable/PhotoSourcePlexor;
    invoke-virtual {v3}, Lcom/android/dreams/phototable/PhotoSourcePlexor;->findAlbums()Ljava/util/Collection;

    move-result-object v2

    .line 41
    .local v2, "mlist":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/dreams/phototable/PhotoSource$AlbumData;>;"
    new-instance v5, Ljava/util/HashSet;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 42
    .local v5, "validAlbumIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    .line 43
    .local v0, "albumData":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    iget-object v6, v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    .end local v0    # "albumData":Lcom/android/dreams/phototable/PhotoSource$AlbumData;
    :cond_0
    invoke-virtual {v4, v5}, Lcom/android/dreams/phototable/AlbumSettings;->pruneObsoleteSettings(Ljava/util/Collection;)V

    .line 46
    invoke-virtual {v4}, Lcom/android/dreams/phototable/AlbumSettings;->isConfigured()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 47
    const v6, 0x7f040002

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/FlipperDream;->setContentView(I)V

    .line 52
    :goto_1
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/FlipperDream;->setFullscreen(Z)V

    .line 53
    return-void

    .line 49
    :cond_1
    const v6, 0x7f040001

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/FlipperDream;->setContentView(I)V

    goto :goto_1
.end method

.method public onDreamingStarted()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStarted()V

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/FlipperDream;->setInteractive(Z)V

    .line 32
    return-void
.end method
