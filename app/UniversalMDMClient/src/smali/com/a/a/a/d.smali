.class public Lcom/a/a/a/d;
.super Ljava/lang/Object;
.source "AsyncHttpResponseHandler.java"

# interfaces
.implements Lcom/a/a/a/m;


# instance fields
.field private handler:Landroid/os/Handler;

.field private oi:Ljava/lang/String;

.field private oj:Ljava/lang/Boolean;

.field private ok:Ljava/net/URI;

.field private ol:[Lorg/apache/http/Header;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const-string v0, "UTF-8"

    iput-object v0, p0, Lcom/a/a/a/d;->oi:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/d;->oj:Ljava/lang/Boolean;

    .line 99
    iput-object v1, p0, Lcom/a/a/a/d;->ok:Ljava/net/URI;

    .line 100
    iput-object v1, p0, Lcom/a/a/a/d;->ol:[Lorg/apache/http/Header;

    .line 168
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Lcom/a/a/a/e;

    invoke-direct {v0, p0}, Lcom/a/a/a/e;-><init>(Lcom/a/a/a/d;)V

    iput-object v0, p0, Lcom/a/a/a/d;->handler:Landroid/os/Handler;

    .line 171
    :cond_0
    return-void
.end method


# virtual methods
.method public B(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 208
    return-void
.end method

.method public a(ILjava/lang/Throwable;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p0, p2, p3}, Lcom/a/a/a/d;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 220
    invoke-virtual {p0, p1, p3}, Lcom/a/a/a/d;->c(ILjava/lang/String;)V

    .line 221
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p0, p1, p3, p4}, Lcom/a/a/a/d;->a(ILjava/lang/Throwable;Ljava/lang/String;)V

    .line 302
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;[B)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 244
    if-nez p3, :cond_0

    move-object v0, v1

    .line 245
    :goto_0
    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/a/a/a/d;->a(I[Lorg/apache/http/Header;Ljava/lang/String;)V

    .line 250
    :goto_1
    return-void

    .line 244
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/a/a/a/d;->eS()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p3, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    const-string v2, "AsyncHttpResponseHandler"

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/a/a/a/d;->a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 314
    if-nez p3, :cond_0

    move-object v0, v1

    .line 315
    :goto_0
    :try_start_0
    invoke-virtual {p0, p1, p2, p4, v0}, Lcom/a/a/a/d;->a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 320
    :goto_1
    return-void

    .line 314
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/a/a/a/d;->eS()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p3, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 316
    :catch_0
    move-exception v0

    .line 317
    const-string v2, "AsyncHttpResponseHandler"

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/a/a/a/d;->a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 260
    return-void
.end method

.method public a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 272
    invoke-virtual {p0, p1}, Lcom/a/a/a/d;->a(Ljava/lang/Throwable;)V

    .line 273
    return-void
.end method

.method public a(Lorg/apache/http/HttpResponse;)V
    .locals 6

    .prologue
    .line 433
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 436
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/a/a/a/d;->a(Lorg/apache/http/HttpEntity;)[B

    move-result-object v1

    .line 438
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 439
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0x12c

    if-lt v2, v3, :cond_1

    .line 440
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v3

    new-instance v4, Lorg/apache/http/client/HttpResponseException;

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v2, v3, v1, v4}, Lcom/a/a/a/d;->b(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    invoke-virtual {p0, v0, v2, v1}, Lcom/a/a/a/d;->b(I[Lorg/apache/http/Header;[B)V

    goto :goto_0
.end method

.method public a([Lorg/apache/http/Header;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/a/a/a/d;->ol:[Lorg/apache/http/Header;

    .line 120
    return-void
.end method

.method a(Lorg/apache/http/HttpEntity;)[B
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 449
    const/4 v0, 0x0

    .line 450
    if-eqz p1, :cond_3

    .line 451
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 452
    if-eqz v4, :cond_3

    .line 453
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    .line 454
    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    .line 455
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "HTTP entity too large to be buffered in memory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    .line 458
    const-wide/16 v2, 0x1000

    .line 461
    :cond_1
    :try_start_0
    new-instance v5, Lorg/apache/http/util/ByteArrayBuffer;

    long-to-int v0, v2

    invoke-direct {v5, v0}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    const/16 v0, 0x1000

    :try_start_1
    new-array v6, v0, [B

    move v0, v1

    .line 466
    :goto_0
    invoke-virtual {v4, v6}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v7, -0x1

    if-eq v1, v7, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v7

    if-nez v7, :cond_2

    .line 467
    add-int/2addr v0, v1

    .line 468
    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v1}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V

    .line 469
    long-to-int v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/d;->c(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 472
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 475
    :catch_0
    move-exception v0

    .line 476
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 477
    new-instance v0, Ljava/io/IOException;

    const-string v1, "File too large to fit into available memory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 474
    invoke-virtual {v5}, Lorg/apache/http/util/ByteArrayBuffer;->toByteArray()[B
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    .line 481
    :cond_3
    return-object v0
.end method

.method public final b(I[Lorg/apache/http/Header;[B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 338
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    invoke-virtual {p0, v2, v0}, Lcom/a/a/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/d;->sendMessage(Landroid/os/Message;)V

    .line 339
    return-void
.end method

.method public final b(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 342
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    aput-object p2, v0, v3

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    invoke-virtual {p0, v3, v0}, Lcom/a/a/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/d;->sendMessage(Landroid/os/Message;)V

    .line 343
    return-void
.end method

.method public b(Ljava/net/URI;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/a/a/a/d;->ok:Ljava/net/URI;

    .line 115
    return-void
.end method

.method public final c(II)V
    .locals 4

    .prologue
    .line 334
    const/4 v0, 0x4

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/d;->sendMessage(Landroid/os/Message;)V

    .line 335
    return-void
.end method

.method public c(ILjava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p0, p2}, Lcom/a/a/a/d;->B(Ljava/lang/String;)V

    .line 233
    return-void
.end method

.method public eR()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/a/a/a/d;->oj:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public eS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/a/a/a/d;->oi:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "UTF-8"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/a/a/a/d;->oi:Ljava/lang/String;

    goto :goto_0
.end method

.method public eT()V
    .locals 0

    .prologue
    .line 326
    return-void
.end method

.method public final eU()V
    .locals 2

    .prologue
    .line 346
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/d;->sendMessage(Landroid/os/Message;)V

    .line 347
    return-void
.end method

.method public final eV()V
    .locals 2

    .prologue
    .line 350
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/d;->sendMessage(Landroid/os/Message;)V

    .line 351
    return-void
.end method

.method public final eW()V
    .locals 2

    .prologue
    .line 354
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/a/a/d;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/a/a/d;->sendMessage(Landroid/os/Message;)V

    .line 355
    return-void
.end method

.method protected handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 361
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 400
    :goto_0
    return-void

    .line 363
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 364
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lt v1, v6, :cond_0

    .line 365
    aget-object v1, v0, v3

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v0, v4

    check-cast v1, [Lorg/apache/http/Header;

    check-cast v1, [Lorg/apache/http/Header;

    aget-object v0, v0, v5

    check-cast v0, [B

    check-cast v0, [B

    invoke-virtual {p0, v2, v1, v0}, Lcom/a/a/a/d;->a(I[Lorg/apache/http/Header;[B)V

    goto :goto_0

    .line 367
    :cond_0
    const-string v0, "AsyncHttpResponseHandler"

    const-string v1, "SUCCESS_MESSAGE didn\'t got enough params"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 371
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 372
    if-eqz v0, :cond_1

    array-length v1, v0

    const/4 v2, 0x4

    if-lt v1, v2, :cond_1

    .line 373
    aget-object v1, v0, v3

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, v0, v4

    check-cast v1, [Lorg/apache/http/Header;

    check-cast v1, [Lorg/apache/http/Header;

    aget-object v2, v0, v5

    check-cast v2, [B

    check-cast v2, [B

    aget-object v0, v0, v6

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {p0, v3, v1, v2, v0}, Lcom/a/a/a/d;->a(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    goto :goto_0

    .line 375
    :cond_1
    const-string v0, "AsyncHttpResponseHandler"

    const-string v1, "FAILURE_MESSAGE didn\'t got enough params"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    :pswitch_2
    invoke-virtual {p0}, Lcom/a/a/a/d;->onStart()V

    goto :goto_0

    .line 382
    :pswitch_3
    invoke-virtual {p0}, Lcom/a/a/a/d;->onFinish()V

    goto :goto_0

    .line 385
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 386
    if-eqz v0, :cond_2

    array-length v1, v0

    if-lt v1, v5, :cond_2

    .line 388
    const/4 v1, 0x0

    :try_start_0
    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/a/a/a/d;->onProgress(II)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 389
    :catch_0
    move-exception v0

    .line 390
    const-string v1, "AsyncHttpResponseHandler"

    const-string v2, "custom onProgress contains an error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 393
    :cond_2
    const-string v0, "AsyncHttpResponseHandler"

    const-string v1, "PROGRESS_MESSAGE didn\'t got enough params"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 397
    :pswitch_5
    invoke-virtual {p0}, Lcom/a/a/a/d;->eT()V

    goto/16 :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/a/a/a/d;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/a/a/a/d;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 427
    :cond_0
    :goto_0
    return-object v0

    .line 421
    :cond_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 422
    if-eqz v0, :cond_0

    .line 423
    iput p1, v0, Landroid/os/Message;->what:I

    .line 424
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0
.end method

.method public onFinish()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public onProgress(II)V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method protected sendMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/a/a/a/d;->eR()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/d;->handler:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 404
    :cond_0
    invoke-virtual {p0, p1}, Lcom/a/a/a/d;->handleMessage(Landroid/os/Message;)V

    .line 408
    :cond_1
    :goto_0
    return-void

    .line 405
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/a/a/a/d;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
