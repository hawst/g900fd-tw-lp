.class public Lcom/a/a/a/a;
.super Ljava/lang/Object;
.source "AsyncHttpClient.java"


# instance fields
.field private nV:I

.field private final nW:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private final nX:Lorg/apache/http/protocol/HttpContext;

.field private nY:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final nZ:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;>;>;>;"
        }
    .end annotation
.end field

.field private final oa:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ob:Z

.field private timeout:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 119
    const/4 v0, 0x0

    const/16 v1, 0x50

    const/16 v2, 0x1bb

    invoke-direct {p0, v0, v1, v2}, Lcom/a/a/a/a;-><init>(ZII)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x1

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput v5, p0, Lcom/a/a/a/a;->nV:I

    .line 106
    const/16 v0, 0x2710

    iput v0, p0, Lcom/a/a/a/a;->timeout:I

    .line 113
    iput-boolean v4, p0, Lcom/a/a/a/a;->ob:Z

    .line 196
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 198
    iget v0, p0, Lcom/a/a/a/a;->timeout:I

    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 199
    new-instance v0, Lorg/apache/http/conn/params/ConnPerRouteBean;

    iget v2, p0, Lcom/a/a/a/a;->nV:I

    invoke-direct {v0, v2}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v1, v0}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 200
    invoke-static {v1, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 202
    iget v0, p0, Lcom/a/a/a/a;->timeout:I

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 203
    iget v0, p0, Lcom/a/a/a/a;->timeout:I

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 204
    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setTcpNoDelay(Lorg/apache/http/params/HttpParams;Z)V

    .line 205
    const/16 v0, 0x2000

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 207
    sget-object v0, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 208
    const-string v0, "android-async-http/%s (http://loopj.com/android-async-http)"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "1.4.4"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 210
    new-instance v2, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v2, v1, p1}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 212
    invoke-static {v5}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    iput-object v0, p0, Lcom/a/a/a/a;->nY:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 213
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a;->nZ:Ljava/util/Map;

    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a;->oa:Ljava/util/Map;

    .line 216
    new-instance v0, Lorg/apache/http/protocol/SyncBasicHttpContext;

    new-instance v3, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v3}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    invoke-direct {v0, v3}, Lorg/apache/http/protocol/SyncBasicHttpContext;-><init>(Lorg/apache/http/protocol/HttpContext;)V

    iput-object v0, p0, Lcom/a/a/a/a;->nX:Lorg/apache/http/protocol/HttpContext;

    .line 217
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0, v2, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 218
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v1, Lcom/a/a/a/a$1;

    invoke-direct {v1, p0}, Lcom/a/a/a/a$1;-><init>(Lcom/a/a/a/a;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 230
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v1, Lcom/a/a/a/a$2;

    invoke-direct {v1, p0}, Lcom/a/a/a/a$2;-><init>(Lcom/a/a/a/a;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 249
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v1, Lcom/a/a/a/n;

    const/4 v2, 0x5

    const/16 v3, 0x5dc

    invoke-direct {v1, v2, v3}, Lcom/a/a/a/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setHttpRequestRetryHandler(Lorg/apache/http/client/HttpRequestRetryHandler;)V

    .line 250
    return-void
.end method

.method public constructor <init>(ZII)V
    .locals 1

    .prologue
    .line 149
    invoke-static {p1, p2, p3}, Lcom/a/a/a/a;->a(ZII)Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/a/a/a;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 150
    return-void
.end method

.method public static a(ZLjava/lang/String;Lcom/a/a/a/j;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 930
    if-eqz p0, :cond_2

    .line 931
    const-string v0, " "

    const-string v1, "%20"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 933
    :goto_0
    if-eqz p2, :cond_0

    .line 934
    invoke-virtual {p2}, Lcom/a/a/a/j;->fd()Ljava/lang/String;

    move-result-object v1

    .line 935
    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 936
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 942
    :cond_0
    :goto_1
    return-object v0

    .line 938
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method static synthetic a(Lcom/a/a/a/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/a/a/a/a;->oa:Ljava/util/Map;

    return-object v0
.end method

.method private a(Lcom/a/a/a/j;Lcom/a/a/a/m;)Lorg/apache/http/HttpEntity;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 953
    .line 956
    if-eqz p1, :cond_0

    .line 957
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/a/a/a/j;->a(Lcom/a/a/a/m;)Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 966
    :cond_0
    :goto_0
    return-object v0

    .line 959
    :catch_0
    move-exception v1

    .line 960
    if-eqz p2, :cond_1

    .line 961
    const/4 v2, 0x0

    invoke-interface {p2, v2, v0, v0, v1}, Lcom/a/a/a/m;->b(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    goto :goto_0

    .line 963
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;Lorg/apache/http/HttpEntity;)Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;
    .locals 0

    .prologue
    .line 981
    if-eqz p2, :cond_0

    .line 982
    invoke-virtual {p1, p2}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 985
    :cond_0
    return-object p1
.end method

.method private static a(ZII)Lorg/apache/http/conn/scheme/SchemeRegistry;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 160
    if-eqz p0, :cond_0

    .line 161
    const-string v0, "AsyncHttpClient"

    const-string v1, "Beware! Using the fix is insecure, as it doesn\'t verify SSL certificates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_0
    if-ge p1, v2, :cond_1

    .line 165
    const/16 p1, 0x50

    .line 166
    const-string v0, "AsyncHttpClient"

    const-string v1, "Invalid HTTP port number specified, defaulting to 80"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :cond_1
    if-ge p2, v2, :cond_2

    .line 170
    const/16 p2, 0x1bb

    .line 171
    const-string v0, "AsyncHttpClient"

    const-string v1, "Invalid HTTPS port number specified, defaulting to 443"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_2
    if-eqz p0, :cond_3

    .line 178
    invoke-static {}, Lcom/a/a/a/h;->fa()Lorg/apache/a/a/a/a;

    move-result-object v0

    .line 182
    :goto_0
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 183
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 184
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    invoke-direct {v2, v3, v0, p2}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 186
    return-object v1

    .line 180
    :cond_3
    invoke-static {}, Lorg/apache/a/a/a/a;->kJ()Lorg/apache/a/a/a/a;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 7

    .prologue
    .line 540
    iget-object v1, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v2, p0, Lcom/a/a/a/a;->nX:Lorg/apache/http/protocol/HttpContext;

    new-instance v3, Lorg/apache/http/client/methods/HttpHead;

    iget-boolean v0, p0, Lcom/a/a/a/a;->ob:Z

    invoke-static {v0, p2, p3}, Lcom/a/a/a/a;->a(ZLjava/lang/String;Lcom/a/a/a/j;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/a/a/a/a;->a(Lorg/apache/http/impl/client/DefaultHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/a/a/a/m;Landroid/content/Context;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 7

    .prologue
    .line 838
    new-instance v3, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v3, p2}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 839
    iget-object v1, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v2, p0, Lcom/a/a/a/a;->nX:Lorg/apache/http/protocol/HttpContext;

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/a/a/a/a;->a(Lorg/apache/http/impl/client/DefaultHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/a/a/a/m;Landroid/content/Context;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 7

    .prologue
    .line 688
    iget-object v1, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v2, p0, Lcom/a/a/a/a;->nX:Lorg/apache/http/protocol/HttpContext;

    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/a/a/a/a;->a(Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;Lorg/apache/http/HttpEntity;)Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    move-result-object v3

    move-object v0, p0

    move-object v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/a/a/a/a;->a(Lorg/apache/http/impl/client/DefaultHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/a/a/a/m;Landroid/content/Context;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 574
    invoke-virtual {p0, v0, p1, v0, p2}, Lcom/a/a/a/a;->b(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lorg/apache/http/impl/client/DefaultHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/a/a/a/m;Landroid/content/Context;)Lcom/a/a/a/i;
    .locals 3

    .prologue
    .line 886
    if-eqz p4, :cond_0

    .line 887
    const-string v0, "Content-Type"

    invoke-interface {p3, v0, p4}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    :cond_0
    invoke-interface {p3}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {p5, v0}, Lcom/a/a/a/m;->a([Lorg/apache/http/Header;)V

    .line 891
    invoke-interface {p3}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-interface {p5, v0}, Lcom/a/a/a/m;->b(Ljava/net/URI;)V

    .line 893
    iget-object v0, p0, Lcom/a/a/a/a;->nY:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/a/a/a/c;

    invoke-direct {v1, p1, p2, p3, p5}, Lcom/a/a/a/c;-><init>(Lorg/apache/http/impl/client/AbstractHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/a/a/a/m;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    .line 895
    if-eqz p6, :cond_2

    .line 897
    iget-object v0, p0, Lcom/a/a/a/a;->nZ:Ljava/util/Map;

    invoke-interface {v0, p6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 898
    if-nez v0, :cond_1

    .line 899
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 900
    iget-object v2, p0, Lcom/a/a/a/a;->nZ:Ljava/util/Map;

    invoke-interface {v2, p6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    :cond_1
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 908
    :cond_2
    new-instance v0, Lcom/a/a/a/i;

    invoke-direct {v0, v1}, Lcom/a/a/a/i;-><init>(Ljava/util/concurrent/Future;)V

    return-object v0
.end method

.method public a(Lorg/apache/a/a/a/a;)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v0

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    const/16 v3, 0x1bb

    invoke-direct {v1, v2, p1, v3}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 404
    return-void
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/a/a/a/a;->oa:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    return-void
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 7

    .prologue
    .line 612
    iget-object v1, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v2, p0, Lcom/a/a/a/a;->nX:Lorg/apache/http/protocol/HttpContext;

    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    iget-boolean v0, p0, Lcom/a/a/a/a;->ob:Z

    invoke-static {v0, p2, p3}, Lcom/a/a/a/a;->a(ZLjava/lang/String;Lcom/a/a/a/j;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/a/a/a/a;->a(Lorg/apache/http/impl/client/DefaultHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/a/a/a/m;Landroid/content/Context;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 7

    .prologue
    .line 790
    iget-object v1, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v2, p0, Lcom/a/a/a/a;->nX:Lorg/apache/http/protocol/HttpContext;

    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/a/a/a/a;->a(Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;Lorg/apache/http/HttpEntity;)Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    move-result-object v3

    move-object v0, p0

    move-object v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/a/a/a/a;->a(Lorg/apache/http/impl/client/DefaultHttpClient;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/a/a/a/m;Landroid/content/Context;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 1

    .prologue
    .line 586
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/a/a/a/a;->b(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 1

    .prologue
    .line 826
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public b(II)V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v1, Lcom/a/a/a/n;

    invoke-direct {v1, p1, p2}, Lcom/a/a/a/n;-><init>(II)V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setHttpRequestRetryHandler(Lorg/apache/http/client/HttpRequestRetryHandler;)V

    .line 414
    return-void
.end method

.method public c(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 6

    .prologue
    .line 671
    invoke-direct {p0, p3, p4}, Lcom/a/a/a/a;->a(Lcom/a/a/a/j;Lcom/a/a/a/m;)Lorg/apache/http/HttpEntity;

    move-result-object v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    .locals 1

    .prologue
    .line 658
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/a/a/a/a;->c(Landroid/content/Context;Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public eO()Lorg/apache/http/client/HttpClient;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    return-object v0
.end method

.method public setTimeout(I)V
    .locals 4

    .prologue
    .line 357
    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_0

    .line 358
    const/16 p1, 0x2710

    .line 359
    :cond_0
    iput p1, p0, Lcom/a/a/a/a;->timeout:I

    .line 360
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 361
    iget v1, p0, Lcom/a/a/a/a;->timeout:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 362
    iget v1, p0, Lcom/a/a/a/a;->timeout:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 363
    iget v1, p0, Lcom/a/a/a/a;->timeout:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 364
    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/a/a/a/a;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 317
    return-void
.end method
