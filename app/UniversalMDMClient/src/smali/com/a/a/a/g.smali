.class public Lcom/a/a/a/g;
.super Lcom/a/a/a/d;
.source "FileAsyncHttpResponseHandler.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private oo:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/a/a/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/a/a/a/g;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/a/a/a/d;-><init>()V

    .line 22
    sget-boolean v0, Lcom/a/a/a/g;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/a/a/a/g;->oo:Ljava/io/File;

    .line 24
    return-void
.end method


# virtual methods
.method public a(ILjava/io/File;)V
    .locals 0

    .prologue
    .line 50
    invoke-virtual {p0, p2}, Lcom/a/a/a/g;->a(Ljava/io/File;)V

    .line 51
    return-void
.end method

.method public a(ILjava/lang/Throwable;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0, p2, p3}, Lcom/a/a/a/g;->a(Ljava/lang/Throwable;Ljava/io/File;)V

    .line 65
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0, p1, p3}, Lcom/a/a/a/g;->a(ILjava/io/File;)V

    .line 55
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0, p1, p3, p4}, Lcom/a/a/a/g;->a(ILjava/lang/Throwable;Ljava/io/File;)V

    .line 70
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;[B)V
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/a/a/a/g;->eY()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/a/a/a/g;->a(I[Lorg/apache/http/Header;Ljava/io/File;)V

    .line 80
    return-void
.end method

.method public a(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/a/a/a/g;->eY()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p4, v0}, Lcom/a/a/a/g;->a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/io/File;)V

    .line 75
    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public a(Ljava/lang/Throwable;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/a/a/a/g;->a(Ljava/lang/Throwable;)V

    .line 60
    return-void
.end method

.method a(Lorg/apache/http/HttpEntity;)[B
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 84
    if-eqz p1, :cond_1

    .line 85
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 86
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    .line 87
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-virtual {p0}, Lcom/a/a/a/g;->eY()Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 88
    if-eqz v1, :cond_1

    .line 90
    const/16 v5, 0x1000

    :try_start_0
    new-array v5, v5, [B

    .line 93
    :goto_0
    invoke-virtual {v1, v5}, Ljava/io/InputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v7

    if-nez v7, :cond_0

    .line 94
    add-int/2addr v0, v6

    .line 95
    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 96
    long-to-int v6, v2

    invoke-virtual {p0, v0, v6}, Lcom/a/a/a/g;->c(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 100
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 101
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    throw v0

    .line 99
    :cond_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 100
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 101
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 105
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected eY()Ljava/io/File;
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/a/a/a/g;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/a/a/a/g;->oo:Ljava/io/File;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/a/a/a/g;->oo:Ljava/io/File;

    return-object v0
.end method
