.class public Lcom/android/exchange/b;
.super Ljava/lang/Object;
.source "DeviceInformation.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field public dA:Ljava/lang/String;

.field public dB:Ljava/lang/String;

.field public dC:Ljava/lang/String;

.field public dD:Ljava/lang/String;

.field public dE:Ljava/lang/String;

.field public dF:Z

.field public dG:Ljava/lang/String;

.field public dH:D

.field public mId:Ljava/lang/String;

.field public mUserAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(D)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "DeviceInformation"

    iput-object v0, p0, Lcom/android/exchange/b;->TAG:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->dA:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->mId:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->dB:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->dC:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->dD:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->dE:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/b;->mUserAgent:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/b;->dF:Z

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/b;->dG:Ljava/lang/String;

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/b;->dH:D

    .line 49
    iput-wide p1, p0, Lcom/android/exchange/b;->dH:D

    .line 50
    return-void
.end method


# virtual methods
.method public b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 57
    if-nez p1, :cond_0

    .line 58
    const-string v0, "DeviceInformation"

    const-string v2, "context is null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 125
    :goto_0
    return v0

    .line 62
    :cond_0
    if-nez p2, :cond_1

    .line 63
    const-string v0, "DeviceInformation"

    const-string v2, "userAgent is null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string p2, ""

    .line 69
    :cond_1
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 68
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 70
    if-nez v0, :cond_2

    .line 71
    const-string v0, "DeviceInformation"

    const-string v2, "tm is null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 72
    goto :goto_0

    .line 75
    :cond_2
    iput-object p2, p0, Lcom/android/exchange/b;->mUserAgent:Ljava/lang/String;

    .line 78
    :try_start_0
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v2, :cond_6

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    :goto_1
    iput-object v2, p0, Lcom/android/exchange/b;->dA:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/b;->mId:Ljava/lang/String;

    .line 86
    iget-object v2, p0, Lcom/android/exchange/b;->mId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 87
    const-string v2, ""

    iput-object v2, p0, Lcom/android/exchange/b;->mId:Ljava/lang/String;

    .line 91
    :cond_3
    :try_start_1
    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    if-eqz v2, :cond_7

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    :goto_2
    iput-object v2, p0, Lcom/android/exchange/b;->dB:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 98
    const-string v2, "Android"

    iput-object v2, p0, Lcom/android/exchange/b;->dC:Ljava/lang/String;

    .line 99
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/b;->dD:Ljava/lang/String;

    .line 101
    iget-object v2, p0, Lcom/android/exchange/b;->dD:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 102
    const-string v2, "English"

    iput-object v2, p0, Lcom/android/exchange/b;->dD:Ljava/lang/String;

    .line 104
    :cond_4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/b;->dE:Ljava/lang/String;

    .line 105
    iget-object v2, p0, Lcom/android/exchange/b;->dE:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 106
    const-string v2, "DeviceInformation"

    const-string v3, "mPhoneNumber is null"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v2, ""

    iput-object v2, p0, Lcom/android/exchange/b;->dE:Ljava/lang/String;

    .line 111
    :cond_5
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/b;->dG:Ljava/lang/String;

    .line 117
    iput-boolean v1, p0, Lcom/android/exchange/b;->dF:Z

    .line 125
    const/4 v0, 0x1

    goto :goto_0

    .line 78
    :cond_6
    :try_start_2
    const-string v2, "Android"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 79
    :catch_0
    move-exception v0

    .line 80
    const-string v2, "DeviceInformation"

    const-string v3, "Accessing Build.Model caused Exception"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 82
    goto/16 :goto_0

    .line 91
    :cond_7
    :try_start_3
    const-string v2, "Android"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 92
    :catch_1
    move-exception v0

    .line 93
    const-string v2, "DeviceInformation"

    const-string v3, "Accessing Build.Product caused Exception"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 95
    goto/16 :goto_0
.end method

.method public h(Z)Lcom/android/exchange/adapter/f;
    .locals 6

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/android/exchange/b;->dH:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 135
    const/4 v0, 0x0

    .line 160
    :goto_0
    return-object v0

    .line 137
    :cond_0
    new-instance v1, Lcom/android/exchange/adapter/f;

    invoke-direct {v1}, Lcom/android/exchange/adapter/f;-><init>()V

    .line 139
    if-eqz p1, :cond_3

    .line 140
    const/16 v0, 0x385

    invoke-virtual {v1, v0}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 144
    :goto_1
    const/16 v0, 0x496

    invoke-virtual {v1, v0}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    const/16 v2, 0x488

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 145
    const/16 v2, 0x497

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->dA:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    const/16 v2, 0x498

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->mId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    const/16 v2, 0x499

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->dB:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 147
    const/16 v2, 0x49a

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->dC:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    const/16 v2, 0x49b

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 148
    iget-object v2, p0, Lcom/android/exchange/b;->dD:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    const/16 v2, 0x49c

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->dE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    .line 149
    iget-wide v2, p0, Lcom/android/exchange/b;->dH:D

    const-wide/high16 v4, 0x402c000000000000L    # 14.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_1

    .line 150
    const/16 v0, 0x4a0

    invoke-virtual {v1, v0}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 151
    const/16 v2, 0x4a1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v2

    .line 152
    iget-boolean v0, p0, Lcom/android/exchange/b;->dF:Z

    if-eqz v0, :cond_4

    const-string v0, "1"

    :goto_2
    invoke-virtual {v2, v0}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 153
    const/16 v2, 0x4a2

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/b;->dG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    .line 155
    :cond_1
    invoke-virtual {v1}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    .line 157
    if-nez p1, :cond_2

    .line 158
    invoke-virtual {v1}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->done()V

    :cond_2
    move-object v0, v1

    .line 160
    goto/16 :goto_0

    .line 142
    :cond_3
    const/16 v0, 0x485

    invoke-virtual {v1, v0}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    goto/16 :goto_1

    .line 152
    :cond_4
    const-string v0, "0"

    goto :goto_2
.end method
