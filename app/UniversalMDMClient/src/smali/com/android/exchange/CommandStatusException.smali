.class public Lcom/android/exchange/CommandStatusException;
.super Lcom/android/exchange/EasException;
.source "CommandStatusException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final mItemId:Ljava/lang/String;

.field public final mStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/exchange/EasException;-><init>()V

    .line 161
    iput p1, p0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/CommandStatusException;->mItemId:Ljava/lang/String;

    .line 163
    return-void
.end method
