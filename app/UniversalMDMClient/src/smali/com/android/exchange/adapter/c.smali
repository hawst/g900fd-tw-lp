.class public Lcom/android/exchange/adapter/c;
.super Lcom/android/exchange/adapter/a;
.source "FourteenProvisionParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/a;-><init>(Ljava/io/InputStream;)V

    .line 16
    return-void
.end method


# virtual methods
.method public y()Z
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/android/exchange/adapter/c;->z()Z

    move-result v0

    return v0
.end method

.method public z()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 24
    .line 25
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/c;->w(I)I

    move-result v1

    const/16 v2, 0x1d6

    if-eq v1, v2, :cond_3

    .line 26
    new-instance v0, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 28
    :cond_0
    iget v1, p0, Lcom/android/exchange/adapter/c;->tag:I

    const/16 v2, 0x1cc

    if-ne v1, v2, :cond_3

    .line 29
    invoke-virtual {p0}, Lcom/android/exchange/adapter/c;->A()I

    move-result v1

    .line 30
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/c;->t(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 31
    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    .line 34
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/c;->u(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 35
    const-string v0, "FourteenProvisionParser"

    .line 36
    const-string v1, "FourteenProvisionParser::parse() - Received status 129, to Block device "

    .line 35
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v1, 0x40001

    .line 38
    const-string v2, "Permission Denied"

    .line 37
    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 40
    :cond_2
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/c;->v(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 41
    const-string v0, "FourteenProvisionParser"

    .line 42
    const-string v2, "FourteenProvisionParser::parse() - Received status 177, too may partnerships "

    .line 41
    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v0, v1}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v0

    .line 27
    :cond_3
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/c;->w(I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    goto :goto_0
.end method
