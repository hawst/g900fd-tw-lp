.class public abstract Lcom/android/exchange/adapter/Parser;
.super Ljava/util/Observable;
.source "Parser.java"


# static fields
.field private static ee:[[Ljava/lang/String;


# instance fields
.field private dV:Z

.field private dW:Z

.field private dX:Z

.field private dY:Ljava/lang/String;

.field dZ:Ljava/io/BufferedOutputStream;

.field private depth:I

.field private ea:Z

.field private eb:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private ec:I

.field private ed:[Ljava/lang/String;

.field private ef:[Ljava/lang/String;

.field private eg:[I

.field public eh:I

.field public ei:I

.field public ej:I

.field private ek:Z

.field public el:[B

.field public em:I

.field private in:Ljava/io/InputStream;

.field public name:Ljava/lang/String;

.field public tag:I

.field public text:Ljava/lang/String;

.field tid:J

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    const/16 v0, 0x96

    new-array v0, v0, [[Ljava/lang/String;

    sput-object v0, Lcom/android/exchange/adapter/Parser;->ee:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;Z)V

    .line 202
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->ck:Z

    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    .line 204
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 4

    .prologue
    const/high16 v2, -0x80000000

    const/4 v0, 0x0

    .line 206
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 70
    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    .line 72
    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dW:Z

    .line 75
    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dX:Z

    .line 77
    const-string v1, "EAS Parser"

    iput-object v1, p0, Lcom/android/exchange/adapter/Parser;->dY:Ljava/lang/String;

    .line 80
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/adapter/Parser;->dZ:Ljava/io/BufferedOutputStream;

    .line 82
    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->ea:Z

    .line 105
    iput v2, p0, Lcom/android/exchange/adapter/Parser;->ec:I

    .line 114
    const/16 v1, 0x20

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/adapter/Parser;->ef:[Ljava/lang/String;

    .line 118
    const/16 v1, 0x80

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/exchange/adapter/Parser;->eg:[I

    .line 124
    iput v2, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    .line 186
    sget-object v1, Lcom/android/exchange/adapter/g;->eF:[[Ljava/lang/String;

    .line 187
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 208
    if-nez p2, :cond_2

    .line 209
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/Parser;->a(Ljava/io/InputStream;)V

    .line 212
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/Parser;->tid:J

    .line 214
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->ck:Z

    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    .line 215
    return-void

    .line 188
    :cond_0
    aget-object v2, v1, v0

    .line 189
    array-length v3, v2

    if-lez v3, :cond_1

    .line 190
    sget-object v3, Lcom/android/exchange/adapter/Parser;->ee:[[Ljava/lang/String;

    aput-object v2, v3, v0

    .line 187
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/Parser;->b(Ljava/io/InputStream;)V

    goto :goto_1
.end method

.method private C()I
    .locals 2

    .prologue
    .line 887
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->ec:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 888
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->ec:I

    .line 890
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->ec:I

    return v0
.end method

.method private D()I
    .locals 2

    .prologue
    .line 894
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v0

    .line 895
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 896
    new-instance v0, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 898
    :cond_0
    return v0
.end method

.method private E()I
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 910
    move v2, v0

    move v3, v1

    .line 916
    :goto_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->D()I

    move-result v4

    .line 919
    if-nez v4, :cond_0

    .line 920
    mul-int v0, v2, v3

    return v0

    .line 922
    :cond_0
    if-eqz v0, :cond_1

    const/16 v0, 0x2d

    if-ne v4, v0, :cond_1

    .line 923
    const/4 v0, -0x1

    move v2, v0

    move v0, v1

    .line 925
    goto :goto_0

    .line 930
    :cond_1
    const/16 v0, 0x30

    if-lt v4, v0, :cond_2

    const/16 v0, 0x39

    if-gt v4, v0, :cond_2

    .line 931
    mul-int/lit8 v0, v3, 0xa

    add-int/lit8 v3, v4, -0x30

    add-int/2addr v0, v3

    move v3, v0

    move v0, v1

    .line 932
    goto :goto_0

    .line 933
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Non integer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private F()Ljava/lang/String;
    .locals 3

    .prologue
    .line 955
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 959
    :goto_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v1

    .line 960
    if-nez v1, :cond_0

    .line 967
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 968
    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 969
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 970
    return-object v1

    .line 962
    :cond_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 963
    new-instance v0, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 965
    :cond_1
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method

.method private a(Ljava/io/OutputStream;Ljava/util/Observer;)I
    .locals 11

    .prologue
    .line 974
    const-wide/16 v6, 0x0

    .line 975
    const-wide/16 v4, 0x0

    .line 979
    if-eqz p1, :cond_a

    .line 981
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v8, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 983
    if-eqz p2, :cond_0

    .line 984
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/Parser;->addObserver(Ljava/util/Observer;)V

    .line 991
    :cond_0
    const/4 v2, 0x0

    .line 992
    const/4 v1, 0x0

    .line 993
    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    .line 997
    :cond_1
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->dX:Z

    if-eqz v1, :cond_4

    .line 998
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->dX:Z

    .line 1000
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1001
    new-instance v1, Ljava/lang/String;

    const-string v3, "ATTACHMENT_DOWNLOAD_CANCEL"

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1003
    if-eqz p2, :cond_2

    .line 1004
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1046
    :cond_2
    int-to-long v2, v2

    add-long/2addr v2, v6

    .line 1047
    int-to-long v0, v0

    add-long/2addr v0, v4

    .line 1049
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1050
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1052
    if-eqz p2, :cond_3

    .line 1053
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V

    .line 1006
    :cond_3
    const/4 v0, -0x1

    .line 1062
    :goto_1
    return v0

    .line 1009
    :cond_4
    :try_start_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v1

    .line 1010
    const/4 v9, -0x1

    if-eq v1, v9, :cond_5

    if-nez v1, :cond_7

    .line 1012
    :cond_5
    if-lez v2, :cond_6

    .line 1013
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v1

    .line 1014
    array-length v0, v1

    .line 1015
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1046
    :cond_6
    int-to-long v2, v2

    add-long/2addr v2, v6

    .line 1047
    int-to-long v0, v0

    add-long/2addr v0, v4

    .line 1049
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1050
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1052
    if-eqz p2, :cond_b

    .line 1053
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V

    move-wide v0, v2

    .line 1062
    :goto_2
    long-to-int v0, v0

    goto :goto_1

    .line 1021
    :cond_7
    :try_start_2
    invoke-virtual {v8, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 1022
    add-int/lit8 v2, v2, 0x1

    .line 1024
    const/16 v1, 0x100

    if-ne v2, v1, :cond_1

    .line 1026
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const/4 v9, 0x0

    invoke-static {v1, v9}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v9

    .line 1027
    array-length v1, v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1029
    :try_start_3
    invoke-virtual {p1, v9}, Ljava/io/OutputStream;->write([B)V

    .line 1030
    add-int v0, v3, v2

    .line 1031
    int-to-long v2, v2

    add-long/2addr v6, v2

    .line 1032
    int-to-long v2, v1

    add-long/2addr v4, v2

    .line 1033
    const/4 v2, 0x0

    .line 1035
    const/16 v3, 0x4000

    if-lt v0, v3, :cond_8

    .line 1036
    add-int/lit16 v0, v0, -0x4000

    .line 1038
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1039
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1042
    :cond_8
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->reset()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v3, v0

    move v0, v1

    .line 996
    goto/16 :goto_0

    .line 1045
    :catchall_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 1046
    :goto_3
    int-to-long v2, v2

    add-long/2addr v2, v6

    .line 1047
    int-to-long v2, v1

    add-long/2addr v2, v4

    .line 1049
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1050
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1052
    if-eqz p2, :cond_9

    .line 1053
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V

    .line 1055
    :cond_9
    throw v0

    .line 1058
    :cond_a
    const-wide/16 v0, 0x0

    goto :goto_2

    .line 1045
    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_b
    move-wide v0, v2

    goto :goto_2
.end method

.method private final a(ZLjava/io/OutputStream;Ljava/util/Observer;)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/high16 v4, -0x80000000

    .line 638
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    .line 639
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v3, v5, :cond_1

    .line 640
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    .line 644
    :goto_0
    iget-boolean v3, p0, Lcom/android/exchange/adapter/Parser;->ek:Z

    if-eqz v3, :cond_3

    .line 645
    iput v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 646
    iput-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->ek:Z

    .line 647
    iput v2, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    .line 649
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    if-eqz v0, :cond_0

    .line 651
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    array-length v0, v0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    if-le v0, v1, :cond_2

    .line 652
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    aget-object v0, v0, v1

    .line 653
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " />"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 661
    :cond_0
    :goto_1
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 767
    :goto_2
    return v0

    .line 642
    :cond_1
    iput v4, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    goto :goto_0

    .line 655
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "noContent : new Tag - check Tags.java!!! Added tag = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 656
    :catch_0
    move-exception v0

    .line 657
    const-string v0, "exception occurred while getting next TAG"

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 663
    :cond_3
    iput-object v6, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 664
    iput-object v6, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 665
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->C()I

    move-result v2

    .line 666
    :goto_3
    if-eqz v2, :cond_6

    .line 676
    iput v4, p0, Lcom/android/exchange/adapter/Parser;->ec:I

    .line 677
    sparse-switch v2, :sswitch_data_0

    .line 745
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 747
    and-int/lit8 v3, v2, 0x3f

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    .line 749
    and-int/lit8 v2, v2, 0x40

    if-nez v2, :cond_e

    :goto_4
    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->ek:Z

    .line 750
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    .line 751
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    if-eqz v0, :cond_4

    .line 753
    :try_start_2
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    array-length v0, v0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    if-le v0, v1, :cond_f

    .line 754
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 756
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ef:[Ljava/lang/String;

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    iget-object v2, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    aput-object v2, v0, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 764
    :cond_4
    :goto_5
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->eg:[I

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    iget v2, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    aput v2, v0, v1

    .line 767
    :cond_5
    :goto_6
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    goto :goto_2

    .line 667
    :cond_6
    iput v4, p0, Lcom/android/exchange/adapter/Parser;->ec:I

    .line 669
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->D()I

    move-result v2

    .line 671
    shl-int/lit8 v3, v2, 0x6

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->ej:I

    .line 673
    sget-object v3, Lcom/android/exchange/adapter/Parser;->ee:[[Ljava/lang/String;

    aget-object v2, v3, v2

    iput-object v2, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    .line 674
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->C()I

    move-result v2

    goto :goto_3

    .line 680
    :sswitch_0
    iput v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    goto :goto_6

    .line 684
    :sswitch_1
    iput v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 685
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    if-eqz v0, :cond_7

    .line 686
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ef:[Ljava/lang/String;

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 690
    :cond_7
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->eg:[I

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    aget v0, v0, v1

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    goto :goto_6

    .line 694
    :sswitch_2
    if-nez p2, :cond_a

    .line 696
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 697
    if-eqz p1, :cond_9

    .line 698
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->E()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->em:I

    .line 708
    :goto_7
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    if-eqz v0, :cond_5

    .line 710
    :try_start_3
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    array-length v0, v0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    if-le v0, v1, :cond_c

    .line 711
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 712
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p2, :cond_8

    if-eqz p1, :cond_b

    :cond_8
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->em:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    .line 715
    :catch_1
    move-exception v0

    .line 716
    const-string v0, "exception occurred while getting next TAG"

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 700
    :cond_9
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    goto :goto_7

    .line 703
    :cond_a
    iput-object v6, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 704
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/Parser;->a(Ljava/io/OutputStream;Ljava/util/Observer;)I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->em:I

    goto :goto_7

    .line 712
    :cond_b
    :try_start_4
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    goto :goto_8

    .line 714
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "STR_I : new Tag - check Tags.java!!! Added tag = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_6

    .line 724
    :sswitch_3
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    move-result v0

    .line 725
    new-array v1, v0, [B

    .line 727
    :goto_9
    if-gtz v0, :cond_d

    .line 731
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 734
    iput-object v1, p0, Lcom/android/exchange/adapter/Parser;->el:[B

    goto/16 :goto_6

    .line 728
    :cond_d
    iget-object v2, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    array-length v3, v1

    sub-int/2addr v3, v0

    invoke-virtual {v2, v1, v3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_9

    :cond_e
    move v0, v1

    .line 749
    goto/16 :goto_4

    .line 758
    :cond_f
    :try_start_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "default : new Tag - check Tags.java!!! Added tag = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_5

    .line 759
    :catch_2
    move-exception v0

    .line 760
    const-string v0, "exception occurred while getting next TAG"

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 677
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0xc3 -> :sswitch_3
    .end sparse-switch
.end method

.method private read()I
    .locals 3

    .prologue
    .line 875
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 877
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 878
    iget-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->dW:Z

    if-eqz v1, :cond_0

    .line 879
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->eb:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 883
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private readInt()I
    .locals 3

    .prologue
    .line 939
    const/4 v0, 0x0

    .line 942
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->D()I

    move-result v1

    .line 943
    shl-int/lit8 v0, v0, 0x7

    and-int/lit8 v2, v1, 0x7f

    or-int/2addr v0, v2

    .line 944
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    .line 945
    return v0
.end method


# virtual methods
.method public A()I
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 397
    const/4 v1, 0x1

    invoke-direct {p0, v1, v2, v2}, Lcom/android/exchange/adapter/Parser;->a(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 398
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v1, v3, :cond_0

    .line 410
    :goto_0
    return v0

    .line 402
    :cond_0
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->em:I

    .line 404
    invoke-direct {p0, v0, v2, v2}, Lcom/android/exchange/adapter/Parser;->a(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 406
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v0, v3, :cond_1

    .line 407
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No END found!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    move v0, v1

    .line 410
    goto :goto_0
.end method

.method public B()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 507
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    .line 509
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v3, v3}, Lcom/android/exchange/adapter/Parser;->a(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 516
    new-instance v0, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 510
    :cond_1
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    if-ne v1, v0, :cond_0

    .line 511
    return-void
.end method

.method public G()Z
    .locals 1

    .prologue
    .line 1071
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->ek:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 602
    iput-object p1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    .line 603
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->D()I

    .line 604
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    .line 605
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    .line 606
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    .line 607
    sget-object v0, Lcom/android/exchange/adapter/Parser;->ee:[[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    .line 608
    return-void
.end method

.method b(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    .line 612
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 326
    invoke-direct {p0, v2, v1, v1}, Lcom/android/exchange/adapter/Parser;->a(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 329
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v0, v3, :cond_2

    .line 330
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->dV:Z

    if-eqz v0, :cond_0

    .line 332
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    array-length v0, v0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v1, v1, -0x5

    if-le v0, v1, :cond_1

    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No value for tag: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->ed:[Ljava/lang/String;

    iget v2, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    add-int/lit8 v2, v2, -0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :cond_0
    :goto_0
    const-string v0, ""

    .line 351
    :goto_1
    return-object v0

    .line 335
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getValue() : new Tag - check Tags.java!!! Added tag = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    const-string v0, "exception occurred while getting next TAG"

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_2
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 345
    invoke-direct {p0, v2, v1, v1}, Lcom/android/exchange/adapter/Parser;->a(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 347
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v1, v3, :cond_3

    .line 348
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No END found!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_3
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    iput v1, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    goto :goto_1
.end method

.method log(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 621
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 622
    if-lez v0, :cond_0

    .line 623
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 625
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->dY:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/exchange/adapter/Parser;->tid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    return-void
.end method

.method public w(I)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x3

    .line 475
    and-int/lit8 v1, p1, 0x3f

    iput v1, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    .line 476
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v3, v3}, Lcom/android/exchange/adapter/Parser;->a(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 489
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    if-nez v1, :cond_3

    .line 490
    :goto_0
    return v0

    .line 478
    :cond_1
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 479
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->ej:I

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    .line 480
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    goto :goto_0

    .line 483
    :cond_2
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->ei:I

    iget v2, p0, Lcom/android/exchange/adapter/Parser;->eh:I

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 496
    :cond_3
    new-instance v0, Lcom/android/exchange/adapter/Parser$EodException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EodException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method
