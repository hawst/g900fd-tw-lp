.class public Lcom/android/exchange/adapter/d;
.super Lcom/android/exchange/adapter/Parser;
.source "ProvisionParser.java"


# instance fields
.field dT:Ljava/lang/String;

.field private en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

.field eo:Lcom/android/emailcommon/service/PolicySet;

.field ep:Z

.field eq:[Ljava/lang/String;

.field public er:Ljava/lang/StringBuilder;

.field public es:Ljava/lang/StringBuilder;

.field et:I

.field eu:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/d;->eo:Lcom/android/emailcommon/service/PolicySet;

    .line 51
    const-string v0, "0"

    iput-object v0, p0, Lcom/android/exchange/adapter/d;->dT:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/d;->ep:Z

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/d;->er:Ljava/lang/StringBuilder;

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/d;->es:Ljava/lang/StringBuilder;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/adapter/d;->et:I

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/d;->eu:Z

    .line 70
    iput-object p2, p0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    .line 71
    return-void
.end method

.method private L()V
    .locals 52

    .prologue
    .line 122
    new-instance v47, Ljava/util/ArrayList;

    invoke-direct/range {v47 .. v47}, Ljava/util/ArrayList;-><init>()V

    .line 124
    const/16 v45, 0x0

    .line 125
    const/4 v9, 0x0

    .line 126
    const/4 v7, 0x0

    .line 127
    const/4 v11, 0x0

    .line 128
    const/4 v12, 0x0

    .line 129
    const/16 v25, 0x0

    .line 130
    const/4 v6, 0x0

    .line 135
    const/4 v8, 0x0

    .line 136
    const/4 v10, 0x0

    .line 139
    const/4 v13, 0x1

    .line 140
    const/16 v40, 0x1

    .line 141
    const/4 v14, 0x0

    .line 142
    const/16 v20, 0x1

    .line 143
    const/16 v23, 0x0

    .line 147
    const/16 v26, 0x0

    .line 148
    const/16 v27, 0x0

    .line 150
    const/16 v28, 0x0

    .line 151
    const/16 v29, 0x0

    .line 152
    const/16 v30, 0x0

    .line 153
    const/16 v31, 0x0

    .line 154
    const/16 v32, -0x1

    .line 155
    const/16 v33, -0x1

    .line 156
    const/16 v34, -0x1

    .line 157
    const/16 v35, 0x1

    .line 160
    const/4 v15, 0x1

    .line 161
    const/16 v16, 0x1

    .line 162
    const/16 v17, 0x1

    .line 163
    const/16 v18, 0x1

    .line 164
    const/16 v19, 0x1

    .line 165
    const/16 v21, 0x1

    .line 166
    const/16 v22, 0x1

    .line 168
    const/16 v24, 0x2

    .line 169
    const/16 v36, 0x1

    .line 170
    const/16 v37, 0x1

    .line 174
    const/16 v38, 0x0

    .line 175
    const/16 v39, 0x0

    .line 178
    const-string v42, ""

    .line 180
    const-string v41, ""

    .line 181
    const/16 v43, 0x1

    .line 182
    const/16 v44, 0x1

    .line 185
    const/4 v5, 0x0

    .line 186
    const/4 v4, 0x0

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    move-object/from16 v46, v0

    if-eqz v46, :cond_16

    .line 189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v5, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 190
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/adapter/d;->b(Landroid/content/Context;)Z

    move-result v4

    move-object/from16 v51, v5

    move/from16 v5, v45

    move/from16 v45, v6

    move v6, v9

    move-object/from16 v9, v51

    .line 193
    :goto_0
    const/16 v46, 0x38d

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v46

    const/16 v48, 0x3

    move/from16 v0, v46

    move/from16 v1, v48

    if-ne v0, v1, :cond_3

    .line 480
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    if-eqz v4, :cond_0

    .line 481
    const-string v4, "ProvisionParser"

    new-instance v46, Ljava/lang/StringBuilder;

    const-string v48, "PasswordEnabled = "

    move-object/from16 v0, v46

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v46

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_0
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 486
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/adapter/d;->eq:[Ljava/lang/String;

    .line 487
    const/4 v4, 0x0

    .line 489
    if-nez v9, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    move-object/from16 v45, v0

    if-eqz v45, :cond_1

    .line 490
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v9, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 492
    :cond_1
    if-eqz v9, :cond_2

    .line 493
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v45

    .line 494
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v46

    move v9, v4

    :goto_1
    invoke-interface/range {v46 .. v46}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_15

    .line 500
    :cond_2
    new-instance v4, Lcom/android/emailcommon/service/PolicySet;

    .line 501
    const/4 v9, 0x1

    .line 513
    invoke-direct/range {v4 .. v44}, Lcom/android/emailcommon/service/PolicySet;-><init>(IIIIZZIIZIZZZZZZZZZIIIIIIZZIIIZZZZZZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 500
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/adapter/d;->eo:Lcom/android/emailcommon/service/PolicySet;

    .line 515
    return-void

    .line 194
    :cond_3
    const/16 v46, 0x1

    .line 195
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/d;->tag:I

    move/from16 v48, v0

    packed-switch v48, :pswitch_data_0

    .line 469
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    :cond_4
    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 472
    :goto_2
    if-nez v46, :cond_5

    .line 473
    new-instance v46, Ljava/lang/StringBuilder;

    const-string v48, "Policy not supported: "

    move-object/from16 v0, v46

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/d;->tag:I

    move/from16 v48, v0

    move-object/from16 v0, v46

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v46

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, p0

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/d;->log(Ljava/lang/String;)V

    .line 474
    const/16 v46, 0x0

    move/from16 v0, v46

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/exchange/adapter/d;->eu:Z

    :cond_5
    move-object/from16 v51, v5

    move/from16 v5, v45

    move/from16 v45, v6

    move v6, v9

    move-object/from16 v9, v51

    goto/16 :goto_0

    .line 197
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    .line 198
    const/16 v45, 0x1

    .line 199
    if-nez v6, :cond_4

    .line 200
    const/16 v6, 0x20

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 203
    goto :goto_2

    .line 205
    :pswitch_2
    if-eqz v45, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_6

    .line 206
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v5

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 208
    goto :goto_2

    .line 209
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 210
    goto :goto_2

    .line 212
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    if-eqz v45, :cond_4

    .line 213
    const/16 v6, 0x40

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 214
    goto/16 :goto_2

    .line 218
    :pswitch_4
    if-eqz v45, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_7

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v8

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 221
    goto/16 :goto_2

    .line 222
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 223
    goto/16 :goto_2

    .line 225
    :pswitch_5
    if-eqz v45, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_8

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v7

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    goto/16 :goto_2

    .line 228
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 229
    goto/16 :goto_2

    .line 231
    :pswitch_6
    if-eqz v45, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_9

    .line 232
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v11

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    goto/16 :goto_2

    .line 234
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 235
    goto/16 :goto_2

    .line 237
    :pswitch_7
    if-eqz v45, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_a

    .line 238
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v12

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    goto/16 :goto_2

    .line 240
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 241
    goto/16 :goto_2

    .line 247
    :pswitch_8
    if-eqz v45, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_b

    .line 248
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 249
    const/16 v40, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 250
    goto/16 :goto_2

    .line 251
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 253
    goto/16 :goto_2

    .line 255
    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 256
    const/4 v13, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 258
    goto/16 :goto_2

    .line 260
    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    if-eqz v45, :cond_4

    .line 261
    if-nez v4, :cond_4

    .line 262
    const/4 v10, 0x1

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 264
    goto/16 :goto_2

    .line 266
    :pswitch_b
    if-eqz v13, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_c

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v14

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    goto/16 :goto_2

    .line 269
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 270
    goto/16 :goto_2

    .line 273
    :pswitch_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_e

    if-eqz v45, :cond_e

    .line 274
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->P()Z

    move-result v48

    if-eqz v48, :cond_4

    .line 275
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/exchange/adapter/d;->b(Landroid/content/Context;)Z

    move-result v48

    if-nez v48, :cond_4

    .line 276
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v48

    if-eqz v48, :cond_d

    .line 277
    const/16 v46, 0x0

    .line 279
    const/16 v48, 0x64

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 280
    goto/16 :goto_2

    .line 281
    :cond_d
    const/16 v38, 0x1

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 283
    goto/16 :goto_2

    .line 284
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 285
    goto/16 :goto_2

    .line 293
    :pswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    if-eqz v45, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/exchange/adapter/d;->b(Landroid/content/Context;)Z

    move-result v48

    if-nez v48, :cond_4

    .line 294
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->Q()Z

    move-result v48

    if-eqz v48, :cond_4

    .line 295
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v48

    if-eqz v48, :cond_f

    .line 296
    const/16 v46, 0x0

    .line 298
    const/16 v48, 0x65

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 299
    goto/16 :goto_2

    .line 300
    :cond_f
    const/16 v39, 0x1

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 303
    goto/16 :goto_2

    .line 311
    :pswitch_e
    if-eqz v45, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_10

    .line 312
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v25

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    goto/16 :goto_2

    .line 314
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->B()V

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 315
    goto/16 :goto_2

    .line 317
    :pswitch_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 318
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v48

    if-eqz v48, :cond_11

    .line 319
    const/16 v46, 0x0

    .line 321
    const/16 v48, 0x66

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 322
    goto/16 :goto_2

    .line 323
    :cond_11
    const/4 v15, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 325
    goto/16 :goto_2

    .line 327
    :pswitch_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 328
    const/16 v16, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 330
    goto/16 :goto_2

    .line 332
    :pswitch_11
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 333
    const/16 v17, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 335
    goto/16 :goto_2

    .line 337
    :pswitch_12
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 339
    if-nez v9, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    move-object/from16 v48, v0

    if-eqz v48, :cond_12

    .line 340
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v9, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 341
    :cond_12
    if-eqz v9, :cond_4

    .line 342
    const-string v48, "ro.product.name"

    invoke-static/range {v48 .. v48}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    .line 343
    invoke-static {v9}, Lcom/android/exchange/adapter/d;->c(Landroid/content/Context;)Z

    move-result v48

    if-nez v48, :cond_4

    invoke-static {v9}, Lcom/android/exchange/adapter/d;->d(Landroid/content/Context;)Z

    move-result v48

    if-nez v48, :cond_4

    .line 344
    const/16 v18, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 348
    goto/16 :goto_2

    .line 351
    :pswitch_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 352
    const/16 v19, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 354
    goto/16 :goto_2

    .line 356
    :pswitch_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 357
    const/16 v20, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 359
    goto/16 :goto_2

    .line 361
    :pswitch_15
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 362
    const/16 v21, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 364
    goto/16 :goto_2

    .line 366
    :pswitch_16
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 367
    const/16 v22, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 369
    goto/16 :goto_2

    .line 371
    :pswitch_17
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    .line 374
    if-nez v9, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    move-object/from16 v48, v0

    if-eqz v48, :cond_13

    .line 375
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v9, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 376
    :cond_13
    if-eqz v9, :cond_4

    .line 377
    invoke-static {v9}, Lcom/android/exchange/adapter/d;->c(Landroid/content/Context;)Z

    move-result v48

    if-nez v48, :cond_4

    .line 378
    const/16 v23, 0x1

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 381
    goto/16 :goto_2

    .line 383
    :pswitch_18
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v24

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 384
    goto/16 :goto_2

    .line 386
    :pswitch_19
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v26

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 387
    goto/16 :goto_2

    .line 389
    :pswitch_1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v27

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 390
    goto/16 :goto_2

    .line 392
    :pswitch_1b
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v28

    if-gez v28, :cond_4

    .line 393
    const/16 v28, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 394
    goto/16 :goto_2

    .line 396
    :pswitch_1c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v29

    if-gez v29, :cond_4

    .line 397
    const/16 v29, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 398
    goto/16 :goto_2

    .line 400
    :pswitch_1d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    .line 401
    const/16 v30, 0x1

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 403
    goto/16 :goto_2

    .line 405
    :pswitch_1e
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    const/16 v49, 0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_4

    .line 406
    const/16 v31, 0x1

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 408
    goto/16 :goto_2

    .line 411
    :pswitch_1f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_4

    .line 412
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v32

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 414
    goto/16 :goto_2

    .line 416
    :pswitch_20
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_4

    .line 417
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v33

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 419
    goto/16 :goto_2

    .line 421
    :pswitch_21
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->G()Z

    move-result v48

    if-eqz v48, :cond_4

    .line 422
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v34

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 424
    goto/16 :goto_2

    .line 427
    :pswitch_22
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 428
    const/16 v35, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 430
    goto/16 :goto_2

    .line 432
    :pswitch_23
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 433
    const/16 v36, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 435
    goto/16 :goto_2

    .line 437
    :pswitch_24
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    .line 438
    const/16 v37, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 440
    goto/16 :goto_2

    .line 443
    :pswitch_25
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    if-nez v4, :cond_4

    .line 444
    const/16 v43, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 446
    goto/16 :goto_2

    .line 449
    :pswitch_26
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v48

    if-nez v48, :cond_4

    if-nez v4, :cond_4

    .line 451
    const/16 v44, 0x0

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 455
    goto/16 :goto_2

    .line 460
    :pswitch_27
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/d;->tag:I

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->es:Ljava/lang/StringBuilder;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->er:Ljava/lang/StringBuilder;

    move-object/from16 v50, v0

    move-object/from16 v0, p0

    move/from16 v1, v48

    move-object/from16 v2, v49

    move-object/from16 v3, v50

    invoke-direct {v0, v1, v2, v3}, Lcom/android/exchange/adapter/d;->a(ILjava/lang/StringBuilder;Ljava/lang/StringBuilder;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->er:Ljava/lang/StringBuilder;

    move-object/from16 v48, v0

    if-eqz v48, :cond_14

    if-nez v4, :cond_14

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->er:Ljava/lang/StringBuilder;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v41

    .line 463
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->es:Ljava/lang/StringBuilder;

    move-object/from16 v48, v0

    if-eqz v48, :cond_4

    if-nez v4, :cond_4

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->es:Ljava/lang/StringBuilder;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v51, v9

    move v9, v6

    move/from16 v6, v45

    move/from16 v45, v5

    move-object/from16 v5, v51

    .line 467
    goto/16 :goto_2

    .line 494
    :cond_15
    invoke-interface/range {v46 .. v46}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v47

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/d;->eq:[Ljava/lang/String;

    move-object/from16 v48, v0

    add-int/lit8 v4, v9, 0x1

    move-object/from16 v0, v45

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v47

    aput-object v47, v48, v9

    move v9, v4

    goto/16 :goto_1

    :cond_16
    move-object/from16 v51, v5

    move/from16 v5, v45

    move/from16 v45, v6

    move v6, v9

    move-object/from16 v9, v51

    goto/16 :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x38e
        :pswitch_1
        :pswitch_3
        :pswitch_d
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_b
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_f
        :pswitch_10
        :pswitch_c
        :pswitch_25
        :pswitch_26
        :pswitch_e
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_18
        :pswitch_24
        :pswitch_17
        :pswitch_23
        :pswitch_19
        :pswitch_14
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_15
        :pswitch_13
        :pswitch_0
        :pswitch_16
        :pswitch_27
        :pswitch_0
        :pswitch_27
    .end packed-switch
.end method

.method private M()V
    .locals 2

    .prologue
    .line 772
    :goto_0
    const/16 v0, 0x38a

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 779
    return-void

    .line 773
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/d;->tag:I

    const/16 v1, 0x38d

    if-ne v0, v1, :cond_1

    .line 774
    invoke-direct {p0}, Lcom/android/exchange/adapter/d;->L()V

    goto :goto_0

    .line 776
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->B()V

    goto :goto_0
.end method

.method private N()V
    .locals 4

    .prologue
    .line 782
    const/4 v0, 0x0

    .line 783
    :goto_0
    const/16 v1, 0x387

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 809
    return-void

    .line 784
    :cond_0
    iget v1, p0, Lcom/android/exchange/adapter/d;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 806
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->B()V

    goto :goto_0

    .line 786
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 787
    const-string v1, "ProvisionParser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Policy type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 790
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/d;->dT:Ljava/lang/String;

    goto :goto_0

    .line 793
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v1

    iput v1, p0, Lcom/android/exchange/adapter/d;->et:I

    .line 794
    const-string v1, "ProvisionParser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Policy status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/android/exchange/adapter/d;->et:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 797
    :pswitch_3
    const-string v1, "MS-WAP-Provisioning-XML"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 799
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/d;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 802
    :cond_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/d;->M()V

    goto :goto_0

    .line 784
    nop

    :pswitch_data_0
    .packed-switch 0x388
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private O()V
    .locals 2

    .prologue
    .line 812
    :goto_0
    const/16 v0, 0x386

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 819
    return-void

    .line 813
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/d;->tag:I

    const/16 v1, 0x387

    if-ne v0, v1, :cond_1

    .line 814
    invoke-direct {p0}, Lcom/android/exchange/adapter/d;->N()V

    goto :goto_0

    .line 816
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->B()V

    goto :goto_0
.end method

.method private P()Z
    .locals 4

    .prologue
    .line 850
    iget-object v0, p0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 851
    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 850
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 852
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getStorageEncryptionStatus()I

    move-result v0

    .line 853
    const-string v1, "ProvisionParser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dpm.getStorageEncryptionStatus returns : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()Z
    .locals 4

    .prologue
    .line 858
    iget-object v0, p0, Lcom/android/exchange/adapter/d;->en:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 859
    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 858
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 860
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getExternalSdCardEncryptionStatus()I

    move-result v0

    .line 861
    const-string v1, "ProvisionParser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dpm.getExternalSdCardEncryptionStatus returns : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILjava/lang/StringBuilder;Ljava/lang/StringBuilder;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 536
    const/4 v0, 0x0

    move v1, v0

    .line 539
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 565
    :goto_1
    return v1

    .line 540
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/d;->tag:I

    packed-switch v0, :pswitch_data_0

    .line 556
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->B()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 559
    :catch_0
    move-exception v0

    .line 560
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 561
    throw v0

    .line 542
    :pswitch_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 543
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 544
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    .line 547
    goto :goto_0

    .line 549
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 550
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 551
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    move v1, v2

    .line 554
    goto :goto_0

    .line 562
    :catch_1
    move-exception v0

    .line 563
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 540
    :pswitch_data_0
    .packed-switch 0x3b8
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 672
    const/4 v0, 0x1

    .line 674
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v1

    .line 675
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    const-string v2, "characteristic"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 690
    return v0

    .line 677
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 678
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 679
    const-string v2, "parm"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    const-string v1, "name"

    invoke-interface {p1, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 681
    const-string v2, "4131"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 682
    const-string v1, "value"

    invoke-interface {p1, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 683
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 684
    const/4 v0, 0x0

    .line 673
    goto :goto_0
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 695
    const/4 v0, 0x1

    .line 697
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v2

    .line 698
    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    const-string v3, "characteristic"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 731
    return-void

    .line 700
    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 701
    const-string v2, "parm"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 702
    const-string v2, "name"

    invoke-interface {p1, v5, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 703
    const-string v3, "value"

    invoke-interface {p1, v5, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 704
    const-string v4, "AEFrequencyValue"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 705
    if-eqz v0, :cond_0

    .line 706
    const-string v2, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 707
    iput v1, p2, Lcom/android/exchange/adapter/e;->cF:I

    goto :goto_0

    .line 709
    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3c

    iput v2, p2, Lcom/android/exchange/adapter/e;->cF:I

    goto :goto_0

    .line 712
    :cond_3
    const-string v4, "AEFrequencyType"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 713
    const-string v2, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 716
    goto :goto_0

    :cond_4
    const-string v4, "DeviceWipeThreshold"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 717
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p2, Lcom/android/exchange/adapter/e;->cE:I

    goto :goto_0

    .line 718
    :cond_5
    const-string v4, "CodewordFrequency"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 719
    const-string v4, "MinimumPasswordLength"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 720
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p2, Lcom/android/exchange/adapter/e;->cC:I

    goto/16 :goto_0

    .line 721
    :cond_6
    const-string v4, "PasswordComplexity"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 722
    const-string v2, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 723
    const/16 v2, 0x40

    iput v2, p2, Lcom/android/exchange/adapter/e;->cD:I

    goto/16 :goto_0

    .line 725
    :cond_7
    const/16 v2, 0x20

    iput v2, p2, Lcom/android/exchange/adapter/e;->cD:I

    goto/16 :goto_0
.end method

.method private b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x0

    return v0
.end method

.method private c(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)V
    .locals 3

    .prologue
    .line 736
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v0

    .line 737
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const-string v1, "characteristic"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 746
    return-void

    .line 739
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 740
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 741
    const-string v1, "characteristic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/d;->b(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 5

    .prologue
    .line 866
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_voice_capable"

    const-string v3, "bool"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 867
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 868
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_sms_capable"

    const-string v3, "bool"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 867
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 868
    if-nez v0, :cond_0

    .line 869
    const/4 v0, 0x1

    .line 871
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)V
    .locals 3

    .prologue
    .line 751
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v0

    .line 752
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const-string v1, "wap-provisioningdoc"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 769
    :goto_0
    return-void

    .line 754
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 755
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 756
    const-string v1, "characteristic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    const/4 v0, 0x0

    const-string v1, "type"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 758
    const-string v1, "SecurityPolicy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 759
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/d;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 762
    :cond_2
    const-string v1, "Registry"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 763
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/d;->c(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 877
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.android.mms"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 882
    :goto_0
    return v0

    .line 878
    :catch_0
    move-exception v0

    .line 879
    const-string v0, "ProvisionParser"

    const-string v1, "notSupportSMS"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/exchange/adapter/d;->dT:Ljava/lang/String;

    return-object v0
.end method

.method public I()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/d;->eo:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/exchange/adapter/d;->eu:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    iput-object v1, p0, Lcom/android/exchange/adapter/d;->eo:Lcom/android/emailcommon/service/PolicySet;

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/d;->eu:Z

    .line 96
    iput-object v1, p0, Lcom/android/exchange/adapter/d;->eq:[Ljava/lang/String;

    .line 97
    return-void
.end method

.method public K()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/exchange/adapter/d;->eq:[Ljava/lang/String;

    return-object v0
.end method

.method g(Ljava/lang/String;)V
    .locals 43

    .prologue
    .line 626
    new-instance v42, Lcom/android/exchange/adapter/e;

    invoke-direct/range {v42 .. v43}, Lcom/android/exchange/adapter/e;-><init>(Lcom/android/exchange/adapter/d;)V

    .line 628
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v2

    .line 629
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 630
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v4, "UTF-8"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 631
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 632
    if-nez v3, :cond_0

    .line 633
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 634
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 635
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 636
    const-string v4, "wap-provisioningdoc"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 638
    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v2, v1}, Lcom/android/exchange/adapter/d;->d(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/e;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :cond_0
    new-instance v2, Lcom/android/emailcommon/service/PolicySet;

    move-object/from16 v0, v42

    iget v3, v0, Lcom/android/exchange/adapter/e;->cC:I

    move-object/from16 v0, v42

    iget v4, v0, Lcom/android/exchange/adapter/e;->cD:I

    .line 647
    move-object/from16 v0, v42

    iget v5, v0, Lcom/android/exchange/adapter/e;->cE:I

    move-object/from16 v0, v42

    iget v6, v0, Lcom/android/exchange/adapter/e;->cF:I

    const/4 v7, 0x0

    move-object/from16 v0, v42

    iget-boolean v8, v0, Lcom/android/exchange/adapter/e;->cM:Z

    .line 648
    move-object/from16 v0, v42

    iget v9, v0, Lcom/android/exchange/adapter/e;->ev:I

    move-object/from16 v0, v42

    iget v10, v0, Lcom/android/exchange/adapter/e;->cI:I

    move-object/from16 v0, v42

    iget-boolean v11, v0, Lcom/android/exchange/adapter/e;->cN:Z

    .line 649
    move-object/from16 v0, v42

    iget v12, v0, Lcom/android/exchange/adapter/e;->cO:I

    move-object/from16 v0, v42

    iget-boolean v13, v0, Lcom/android/exchange/adapter/e;->cP:Z

    move-object/from16 v0, v42

    iget-boolean v14, v0, Lcom/android/exchange/adapter/e;->cQ:Z

    move-object/from16 v0, v42

    iget-boolean v15, v0, Lcom/android/exchange/adapter/e;->cR:Z

    .line 650
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cS:Z

    move/from16 v16, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cT:Z

    move/from16 v17, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cU:Z

    move/from16 v18, v0

    .line 651
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cV:Z

    move/from16 v19, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cW:Z

    move/from16 v20, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cX:Z

    move/from16 v21, v0

    .line 652
    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->cY:I

    move/from16 v22, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->cJ:I

    move/from16 v23, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->cZ:I

    move/from16 v24, v0

    .line 653
    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->da:I

    move/from16 v25, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->db:I

    move/from16 v26, v0

    .line 654
    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->dc:I

    move/from16 v27, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->dd:Z

    move/from16 v28, v0

    .line 655
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->de:Z

    move/from16 v29, v0

    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->df:I

    move/from16 v30, v0

    .line 656
    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->dg:I

    move/from16 v31, v0

    .line 657
    move-object/from16 v0, v42

    iget v0, v0, Lcom/android/exchange/adapter/e;->dh:I

    move/from16 v32, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->di:Z

    move/from16 v33, v0

    .line 658
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->dj:Z

    move/from16 v34, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->dk:Z

    move/from16 v35, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cK:Z

    move/from16 v36, v0

    .line 659
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->cL:Z

    move/from16 v37, v0

    .line 660
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->mSimplePasswordEnabled:Z

    move/from16 v38, v0

    .line 661
    move-object/from16 v0, v42

    iget-object v0, v0, Lcom/android/exchange/adapter/e;->ew:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v42

    iget-object v0, v0, Lcom/android/exchange/adapter/e;->ex:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->dn:Z

    move/from16 v41, v0

    .line 662
    move-object/from16 v0, v42

    iget-boolean v0, v0, Lcom/android/exchange/adapter/e;->do:Z

    move/from16 v42, v0

    invoke-direct/range {v2 .. v42}, Lcom/android/emailcommon/service/PolicySet;-><init>(IIIIZZIIZIZZZZZZZZZIIIIIIZZIIIZZZZZZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 646
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/exchange/adapter/d;->eo:Lcom/android/emailcommon/service/PolicySet;

    .line 664
    return-void

    .line 642
    :catch_0
    move-exception v2

    .line 643
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2
.end method

.method public z()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 823
    .line 824
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v0

    const/16 v3, 0x385

    if-eq v0, v3, :cond_3

    .line 825
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 828
    :cond_0
    iget v3, p0, Lcom/android/exchange/adapter/d;->tag:I

    sparse-switch v3, :sswitch_data_0

    .line 843
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->B()V

    .line 827
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/d;->w(I)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 846
    return v0

    .line 830
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/d;->A()I

    move-result v0

    .line 831
    const-string v3, "ProvisionParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Provision status: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    if-ne v0, v2, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 835
    :sswitch_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/d;->O()V

    .line 836
    iget-object v0, p0, Lcom/android/exchange/adapter/d;->eo:Lcom/android/emailcommon/service/PolicySet;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/exchange/adapter/d;->dT:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 839
    :sswitch_2
    iput-boolean v2, p0, Lcom/android/exchange/adapter/d;->ep:Z

    move v0, v2

    .line 841
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 828
    nop

    :sswitch_data_0
    .sparse-switch
        0x386 -> :sswitch_1
        0x38b -> :sswitch_0
        0x38c -> :sswitch_2
    .end sparse-switch
.end method
