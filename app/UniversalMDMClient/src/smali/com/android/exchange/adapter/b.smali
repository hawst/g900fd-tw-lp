.class public Lcom/android/exchange/adapter/b;
.super Ljava/lang/Object;
.source "EasBase64EncodedURI.java"


# instance fields
.field dJ:Z

.field dK:Z

.field dL:Ljava/lang/String;

.field dM:Ljava/lang/String;

.field dN:[B

.field dO:Ljava/lang/String;

.field dP:Ljava/lang/String;

.field dQ:[B

.field dR:Ljava/lang/String;

.field dS:Ljava/lang/String;

.field dT:Ljava/lang/String;

.field dU:[B


# direct methods
.method public constructor <init>(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-array v0, v4, [B

    .line 26
    const/16 v1, 0x79

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dN:[B

    .line 30
    iput-object v2, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    .line 32
    const-string v0, "Android"

    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    .line 34
    new-array v0, v4, [B

    .line 36
    const/16 v1, 0x17

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dQ:[B

    .line 40
    iput-object v2, p0, Lcom/android/exchange/adapter/b;->dR:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/android/exchange/adapter/b;->dS:Ljava/lang/String;

    .line 44
    iput-object v2, p0, Lcom/android/exchange/adapter/b;->dT:Ljava/lang/String;

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 50
    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dU:[B

    .line 292
    iput-boolean p1, p0, Lcom/android/exchange/adapter/b;->dJ:Z

    .line 294
    iput-boolean p2, p0, Lcom/android/exchange/adapter/b;->dK:Z

    .line 296
    iput-object p3, p0, Lcom/android/exchange/adapter/b;->dL:Ljava/lang/String;

    .line 298
    iput-object p4, p0, Lcom/android/exchange/adapter/b;->dM:Ljava/lang/String;

    .line 300
    iput-object v2, p0, Lcom/android/exchange/adapter/b;->dR:Ljava/lang/String;

    .line 302
    iput-object v2, p0, Lcom/android/exchange/adapter/b;->dS:Ljava/lang/String;

    .line 304
    return-void

    .line 48
    nop

    :array_0
    .array-data 1
        0x9t
        0x4t
    .end array-data
.end method

.method public static a(C)C
    .locals 1

    .prologue
    .line 384
    const/16 v0, 0x61

    if-lt p0, v0, :cond_0

    .line 386
    add-int/lit8 v0, p0, -0x57

    int-to-char v0, v0

    .line 394
    :goto_0
    and-int/lit8 v0, v0, 0xf

    int-to-char v0, v0

    .line 396
    return v0

    .line 390
    :cond_0
    add-int/lit8 v0, p0, -0x30

    int-to-char v0, v0

    goto :goto_0
.end method

.method private a(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 402
    if-eqz p2, :cond_0

    .line 404
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 406
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 410
    :cond_0
    return-void
.end method

.method private a(Ljava/io/OutputStream;[BLjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 625
    if-nez p3, :cond_1

    .line 749
    :cond_0
    return-void

    .line 633
    :cond_1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    .line 635
    new-array v2, v8, [B

    .line 637
    aput-byte v8, v2, v7

    .line 643
    invoke-virtual {p3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 645
    :goto_0
    if-lez v0, :cond_0

    .line 651
    const/16 v0, 0x26

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 653
    const/4 v0, -0x1

    if-eq v3, v0, :cond_2

    .line 655
    invoke-virtual {v1, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 659
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 671
    :goto_1
    const/16 v3, 0x3d

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 679
    invoke-virtual {v0, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 681
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 687
    new-array v3, v8, [B

    .line 689
    const/16 v5, 0x3f

    aput-byte v5, v3, v7

    .line 693
    invoke-direct {p0, v4}, Lcom/android/exchange/adapter/b;->f(Ljava/lang/String;)B

    move-result v4

    .line 695
    aget-byte v5, v3, v7

    .line 701
    and-int/lit8 v5, v4, 0xf

    int-to-byte v5, v5

    aput-byte v5, v3, v7

    .line 703
    aget-byte v5, v3, v7

    const/4 v6, 0x7

    if-ne v5, v6, :cond_3

    .line 707
    and-int/lit8 v0, v4, 0x30

    shr-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    .line 709
    aget-byte v3, p2, v7

    or-int/2addr v0, v3

    int-to-byte v0, v0

    aput-byte v0, p2, v7

    .line 739
    :goto_2
    if-eqz v1, :cond_0

    .line 745
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 663
    :cond_2
    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 665
    const/4 v1, 0x0

    goto :goto_1

    .line 715
    :cond_3
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    .line 719
    if-nez v0, :cond_4

    .line 723
    invoke-virtual {p1, v7}, Ljava/io/OutputStream;->write(I)V

    goto :goto_2

    .line 727
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v2, v7

    .line 729
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 731
    invoke-direct {p0, p1, v0}, Lcom/android/exchange/adapter/b;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private e(Ljava/lang/String;)B
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 108
    const-string v1, "sync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 204
    :goto_0
    return v0

    .line 112
    :cond_0
    const-string v1, "sendmail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    const/4 v0, 0x1

    goto :goto_0

    .line 116
    :cond_1
    const-string v1, "smartforward"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 118
    const/4 v0, 0x2

    goto :goto_0

    .line 120
    :cond_2
    const-string v1, "smartreply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 122
    const/4 v0, 0x3

    goto :goto_0

    .line 124
    :cond_3
    const-string v1, "getattachment"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 126
    const/4 v0, 0x4

    goto :goto_0

    .line 128
    :cond_4
    const-string v1, "gethierarchy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 130
    const/4 v0, 0x5

    goto :goto_0

    .line 132
    :cond_5
    const-string v1, "createcollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 134
    const/4 v0, 0x6

    goto :goto_0

    .line 136
    :cond_6
    const-string v1, "deletecollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 138
    const/4 v0, 0x7

    goto :goto_0

    .line 140
    :cond_7
    const-string v1, "movecollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 142
    const/16 v0, 0x8

    goto :goto_0

    .line 144
    :cond_8
    const-string v1, "foldersync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 146
    const/16 v0, 0x9

    goto :goto_0

    .line 148
    :cond_9
    const-string v1, "foldercreate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 150
    const/16 v0, 0xa

    goto :goto_0

    .line 152
    :cond_a
    const-string v1, "folderdelete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 154
    const/16 v0, 0xb

    goto :goto_0

    .line 156
    :cond_b
    const-string v1, "folderupdate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 158
    const/16 v0, 0xc

    goto :goto_0

    .line 160
    :cond_c
    const-string v1, "moveitems"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 162
    const/16 v0, 0xd

    goto/16 :goto_0

    .line 164
    :cond_d
    const-string v1, "getitemestimate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 166
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 168
    :cond_e
    const-string v1, "meetingresponse"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 170
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 172
    :cond_f
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 174
    const/16 v0, 0x10

    goto/16 :goto_0

    .line 176
    :cond_10
    const-string v1, "settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 178
    const/16 v0, 0x11

    goto/16 :goto_0

    .line 180
    :cond_11
    const-string v1, "ping"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 182
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 184
    :cond_12
    const-string v1, "itemoperations"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 186
    const/16 v0, 0x13

    goto/16 :goto_0

    .line 188
    :cond_13
    const-string v1, "provision"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 190
    const/16 v0, 0x14

    goto/16 :goto_0

    .line 192
    :cond_14
    const-string v1, "resolverecipients"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 194
    const/16 v0, 0x15

    goto/16 :goto_0

    .line 196
    :cond_15
    const-string v1, "validatecert"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 198
    const/16 v0, 0x16

    goto/16 :goto_0

    .line 204
    :cond_16
    const/16 v0, 0x17

    goto/16 :goto_0
.end method

.method private f(Ljava/lang/String;)B
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 236
    const-string v1, "attachmentname"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    const/4 v0, 0x0

    .line 284
    :goto_0
    return v0

    .line 240
    :cond_0
    const-string v1, "collectionid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    const/4 v0, 0x1

    goto :goto_0

    .line 244
    :cond_1
    const-string v1, "collectionname"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 246
    const/4 v0, 0x2

    goto :goto_0

    .line 248
    :cond_2
    const-string v1, "itemid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 250
    const/4 v0, 0x3

    goto :goto_0

    .line 252
    :cond_3
    const-string v1, "longid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 254
    const/4 v0, 0x4

    goto :goto_0

    .line 256
    :cond_4
    const-string v1, "parentid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 258
    const/4 v0, 0x5

    goto :goto_0

    .line 260
    :cond_5
    const-string v1, "occurrence"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 262
    const/4 v0, 0x6

    goto :goto_0

    .line 264
    :cond_6
    const-string v1, "options"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 266
    const/4 v0, 0x7

    goto :goto_0

    .line 268
    :cond_7
    const-string v1, "roundtripid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 270
    const/16 v0, 0x8

    goto :goto_0

    .line 272
    :cond_8
    const-string v1, "saveinsent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 274
    const/16 v0, 0x17

    goto :goto_0

    .line 276
    :cond_9
    const-string v1, "acceptmultipart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 278
    const/16 v0, 0x27

    goto :goto_0

    .line 284
    :cond_a
    const/16 v0, 0x3f

    goto :goto_0
.end method

.method private x()Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v9, 0x5

    const/16 v13, 0x10

    const/4 v12, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 414
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 416
    new-array v4, v9, [B

    .line 418
    new-array v5, v2, [B

    .line 424
    new-array v6, v2, [B

    .line 426
    aput-byte v2, v6, v1

    .line 432
    new-instance v7, Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/android/exchange/adapter/b;->dJ:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/exchange/adapter/b;->dK:Z

    if-eqz v0, :cond_1

    const-string v0, "httpts"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "://"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, p0, Lcom/android/exchange/adapter/b;->dL:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 434
    iget-boolean v0, p0, Lcom/android/exchange/adapter/b;->dJ:Z

    if-eqz v0, :cond_3

    const-string v0, ":443"

    :goto_1
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/Microsoft-Server-ActiveSync?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 432
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 440
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dN:[B

    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 444
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dQ:[B

    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 448
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dU:[B

    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 452
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 456
    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 482
    :goto_2
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dT:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 486
    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 546
    :goto_3
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 550
    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 576
    :goto_4
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dR:Ljava/lang/String;

    invoke-direct {p0, v3, v5, v0}, Lcom/android/exchange/adapter/b;->a(Ljava/io/OutputStream;[BLjava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dS:Ljava/lang/String;

    invoke-direct {p0, v3, v5, v0}, Lcom/android/exchange/adapter/b;->a(Ljava/io/OutputStream;[BLjava/lang/String;)V

    .line 584
    aget-byte v0, v5, v1

    if-eqz v0, :cond_0

    .line 588
    const/4 v0, 0x7

    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 592
    invoke-virtual {v3, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 596
    aget-byte v0, v5, v1

    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 608
    :cond_0
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 610
    const-string v2, "Fix It"

    .line 612
    new-instance v2, Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 613
    const-string v0, "EasBase64EncodedURI"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "encoded: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 617
    return-object v0

    .line 432
    :cond_1
    const-string v0, "https"

    goto/16 :goto_0

    :cond_2
    const-string v0, "http"

    goto/16 :goto_0

    .line 434
    :cond_3
    const-string v0, ""

    goto/16 :goto_1

    .line 460
    :cond_4
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v6, v1

    .line 464
    aget-byte v0, v6, v1

    if-le v0, v13, :cond_5

    .line 466
    aget-byte v0, v6, v1

    add-int/lit8 v0, v0, -0x10

    .line 468
    iget-object v8, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    .line 470
    aput-byte v13, v6, v1

    .line 474
    :cond_5
    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 476
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    invoke-direct {p0, v3, v0}, Lcom/android/exchange/adapter/b;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 490
    :cond_6
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dT:Ljava/lang/String;

    const-string v8, "0"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 492
    if-eqz v0, :cond_8

    move v0, v2

    .line 494
    :goto_5
    if-lt v0, v9, :cond_7

    .line 500
    aput-byte v12, v4, v1

    .line 502
    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_3

    .line 496
    :cond_7
    aput-byte v1, v4, v0

    .line 494
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 506
    :cond_8
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dT:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 508
    invoke-static {v8, v9}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v8

    .line 510
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    .line 514
    const/16 v9, 0x8

    if-eq v0, v9, :cond_9

    .line 518
    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto/16 :goto_3

    :cond_9
    move v0, v1

    .line 522
    :goto_6
    if-lt v0, v12, :cond_a

    .line 530
    aput-byte v12, v4, v1

    .line 532
    const-string v0, "EasBase64EncodedURI"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "policyByte: length["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-byte v9, v4, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-byte v9, v4, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 534
    const/4 v9, 0x2

    aget-byte v9, v4, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x3

    aget-byte v9, v4, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-byte v9, v4, v12

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 532
    invoke-static {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_3

    .line 524
    :cond_a
    rsub-int/lit8 v9, v0, 0x4

    mul-int/lit8 v10, v0, 0x2

    invoke-virtual {v8, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Lcom/android/exchange/adapter/b;->a(C)C

    move-result v10

    shl-int/lit8 v10, v10, 0x4

    .line 526
    mul-int/lit8 v11, v0, 0x2

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v8, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    .line 524
    invoke-static {v11}, Lcom/android/exchange/adapter/b;->a(C)C

    move-result v11

    or-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, v4, v9

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 554
    :cond_b
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v6, v1

    .line 558
    aget-byte v0, v6, v1

    if-le v0, v13, :cond_c

    .line 560
    aget-byte v0, v6, v1

    add-int/lit8 v0, v0, -0x10

    .line 562
    iget-object v4, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    .line 564
    aput-byte v13, v6, v1

    .line 568
    :cond_c
    invoke-virtual {v3, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 570
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    invoke-direct {p0, v3, v0}, Lcom/android/exchange/adapter/b;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto/16 :goto_4
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 312
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 314
    const-wide/high16 v2, 0x402c000000000000L    # 14.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    .line 318
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dN:[B

    const/16 v1, -0x73

    aput-byte v1, v0, v4

    .line 328
    :goto_0
    const/16 v0, 0x26

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 330
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 332
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/b;->dR:Ljava/lang/String;

    .line 334
    invoke-virtual {p2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dQ:[B

    invoke-direct {p0, p2}, Lcom/android/exchange/adapter/b;->e(Ljava/lang/String;)B

    move-result v1

    aput-byte v1, v0, v4

    .line 340
    iput-object p3, p0, Lcom/android/exchange/adapter/b;->dS:Ljava/lang/String;

    .line 342
    iput-object p4, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    .line 344
    iput-object p5, p0, Lcom/android/exchange/adapter/b;->dT:Ljava/lang/String;

    .line 346
    iput-object p6, p0, Lcom/android/exchange/adapter/b;->dP:Ljava/lang/String;

    .line 350
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 352
    const/16 v1, 0x10

    if-le v0, v1, :cond_1

    .line 354
    add-int/lit8 v0, v0, -0x10

    .line 356
    iget-object v1, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/b;->dO:Ljava/lang/String;

    .line 362
    :cond_1
    const/4 v0, 0x0

    .line 366
    :try_start_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/b;->x()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 376
    :goto_1
    return-object v0

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/android/exchange/adapter/b;->dN:[B

    const/16 v1, 0x79

    aput-byte v1, v0, v4

    goto :goto_0

    .line 368
    :catch_0
    move-exception v1

    .line 372
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
