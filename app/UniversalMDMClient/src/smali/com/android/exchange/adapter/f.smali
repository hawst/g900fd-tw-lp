.class public Lcom/android/exchange/adapter/f;
.super Ljava/lang/Object;
.source "Serializer.java"


# instance fields
.field private dV:Z

.field depth:I

.field eA:Ljava/io/ByteArrayOutputStream;

.field eB:I

.field eC:[Ljava/lang/String;

.field eD:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private eE:I

.field ez:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/f;-><init>(Z)V

    .line 65
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/f;->dV:Z

    .line 45
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    .line 47
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/adapter/f;->eB:I

    .line 57
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/f;->eC:[Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/f;->eD:Ljava/util/Hashtable;

    .line 74
    if-eqz p1, :cond_0

    .line 76
    :try_start_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/f;->startDocument()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public R()Lcom/android/exchange/adapter/f;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 138
    iget v0, p0, Lcom/android/exchange/adapter/f;->eB:I

    if-ltz v0, :cond_1

    .line 139
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/f;->i(Z)V

    .line 146
    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/exchange/adapter/f;->depth:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/exchange/adapter/f;->depth:I

    .line 147
    return-object p0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 142
    iget-boolean v0, p0, Lcom/android/exchange/adapter/f;->dV:Z

    if-eqz v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "</"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/exchange/adapter/f;->eC:[Ljava/lang/String;

    iget v2, p0, Lcom/android/exchange/adapter/f;->depth:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/f;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;
    .locals 3

    .prologue
    .line 157
    if-nez p2, :cond_0

    .line 158
    const-string v0, "Serializer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Writing null data for tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 161
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    .line 162
    invoke-virtual {p0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    .line 163
    return-object p0
.end method

.method a(Ljava/io/OutputStream;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 240
    const/4 v0, 0x5

    new-array v3, v0, [B

    move v0, v1

    .line 244
    :goto_0
    add-int/lit8 v2, v0, 0x1

    and-int/lit8 v4, p2, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 245
    shr-int/lit8 p2, p2, 0x7

    .line 246
    if-nez p2, :cond_2

    .line 248
    :goto_1
    const/4 v0, 0x1

    if-gt v2, v0, :cond_1

    .line 251
    aget-byte v0, v3, v1

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 252
    iget-boolean v0, p0, Lcom/android/exchange/adapter/f;->dV:Z

    if-eqz v0, :cond_0

    .line 253
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/f;->log(Ljava/lang/String;)V

    .line 255
    :cond_0
    return-void

    .line 249
    :cond_1
    add-int/lit8 v2, v2, -0x1

    aget-byte v0, v3, v2

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method a(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 258
    if-nez p2, :cond_0

    .line 264
    :goto_0
    return-void

    .line 261
    :cond_0
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 262
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 263
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method

.method public done()V
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lcom/android/exchange/adapter/f;->depth:I

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Done received with unclosed tags"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/adapter/f;->a(Ljava/io/OutputStream;I)V

    .line 99
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 100
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 101
    return-void
.end method

.method public h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;
    .locals 3

    .prologue
    .line 189
    if-nez p1, :cond_0

    .line 190
    const-string v0, "Serializer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Writing null text for pending tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/exchange/adapter/f;->eB:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/f;->i(Z)V

    .line 193
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 194
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p0, v0, p1}, Lcom/android/exchange/adapter/f;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 195
    iget-boolean v0, p0, Lcom/android/exchange/adapter/f;->dV:Z

    if-eqz v0, :cond_1

    .line 197
    if-eqz p1, :cond_1

    .line 198
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/f;->log(Ljava/lang/String;)V

    .line 201
    :cond_1
    return-object p0
.end method

.method public i(Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/f;->eB:I

    if-ne v0, v4, :cond_0

    .line 128
    :goto_0
    return-void

    .line 113
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/f;->eB:I

    shr-int/lit8 v2, v0, 0x6

    .line 114
    iget v0, p0, Lcom/android/exchange/adapter/f;->eB:I

    and-int/lit8 v1, v0, 0x3f

    .line 115
    iget v0, p0, Lcom/android/exchange/adapter/f;->eE:I

    if-eq v2, v0, :cond_1

    .line 116
    iput v2, p0, Lcom/android/exchange/adapter/f;->eE:I

    .line 117
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 118
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 121
    :cond_1
    iget-object v3, p0, Lcom/android/exchange/adapter/f;->eA:Ljava/io/ByteArrayOutputStream;

    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 122
    iget-boolean v0, p0, Lcom/android/exchange/adapter/f;->dV:Z

    if-eqz v0, :cond_2

    .line 123
    sget-object v0, Lcom/android/exchange/adapter/g;->eF:[[Ljava/lang/String;

    aget-object v0, v0, v2

    add-int/lit8 v1, v1, -0x5

    aget-object v0, v0, v1

    .line 124
    iget-object v1, p0, Lcom/android/exchange/adapter/f;->eC:[Ljava/lang/String;

    iget v2, p0, Lcom/android/exchange/adapter/f;->depth:I

    aput-object v0, v1, v2

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/f;->log(Ljava/lang/String;)V

    .line 127
    :cond_2
    iput v4, p0, Lcom/android/exchange/adapter/f;->eB:I

    goto :goto_0

    .line 121
    :cond_3
    or-int/lit8 v0, v1, 0x40

    goto :goto_1
.end method

.method log(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 87
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 88
    if-lez v0, :cond_0

    .line 89
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 91
    :cond_0
    const-string v0, "Serializer"

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public startDocument()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 105
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 106
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 107
    return-void
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/exchange/adapter/f;->ez:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public x(I)Lcom/android/exchange/adapter/f;
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/f;->i(Z)V

    .line 132
    iput p1, p0, Lcom/android/exchange/adapter/f;->eB:I

    .line 133
    iget v0, p0, Lcom/android/exchange/adapter/f;->depth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/exchange/adapter/f;->depth:I

    .line 134
    return-object p0
.end method
