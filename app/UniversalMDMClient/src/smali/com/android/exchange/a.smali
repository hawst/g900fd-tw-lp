.class public Lcom/android/exchange/a;
.super Ljava/lang/Object;
.source "CommandStatusException.java"


# static fields
.field private static final dz:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 88
    const/16 v0, 0x32

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 89
    const-string v2, "InvalidContent"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "InvalidWBXML"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "InvalidXML"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "InvalidDateTime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 90
    const-string v2, "InvalidIDCombo"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "InvalidIDs"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "InvalidMIME"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DeviceIdError"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "DeviceTypeError"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 91
    const-string v2, "ServerError"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ServerErrorRetry"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ADAccessDenied"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Quota"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ServerOffline"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 92
    const-string v2, "SendQuota"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "RecipientUnresolved"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ReplyNotAllowed"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SentPreviously"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 93
    const-string v2, "NoRecipient"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "SendFailed"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ReplyFailed"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "AttsTooLarge"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "NoMailbox"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 94
    const-string v2, "CantBeAnonymous"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "UserNotFound"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "UserDisabled"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "NewMailbox"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "LegacyMailbox"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 95
    const-string v2, "DeviceBlocked"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "AccessDenied"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "AcctDisabled"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "SyncStateNF"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "SyncStateLocked"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 96
    const-string v2, "SyncStateCorrupt"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "SyncStateExists"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "SyncStateInvalid"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "BadCommand"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 97
    const-string v2, "BadVersion"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "NotFullyProvisionable"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "RemoteWipe"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "LegacyDevice"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 98
    const-string v2, "NotProvisioned"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "PolicyRefresh"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "BadPolicyKey"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "ExternallyManaged"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 99
    const-string v2, "NoRecurrence"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "UnexpectedClass"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "RemoteHasNoSSL"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "InvalidRequest"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 100
    const-string v2, "ItemNotFound"

    aput-object v2, v0, v1

    .line 88
    sput-object v0, Lcom/android/exchange/a;->dz:[Ljava/lang/String;

    .line 101
    return-void
.end method

.method public static q(I)Z
    .locals 1

    .prologue
    .line 104
    const/16 v0, 0x8e

    if-eq p0, v0, :cond_0

    .line 105
    const/16 v0, 0x8f

    if-eq p0, v0, :cond_0

    .line 106
    const/16 v0, 0x90

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8c

    if-eq p0, v0, :cond_0

    .line 104
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static r(I)Z
    .locals 1

    .prologue
    .line 124
    const/16 v0, 0xb1

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(I)Z
    .locals 1

    .prologue
    .line 130
    const/16 v0, 0x84

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 146
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const/16 v1, 0x65

    if-lt p0, v1, :cond_0

    const/16 v1, 0x96

    if-le p0, v1, :cond_2

    .line 148
    :cond_0
    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_1
    :goto_0
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 150
    :cond_2
    add-int/lit8 v1, p0, -0x65

    .line 151
    sget-object v2, Lcom/android/exchange/a;->dz:[Ljava/lang/String;

    array-length v2, v2

    if-gt v1, v2, :cond_1

    .line 152
    sget-object v2, Lcom/android/exchange/a;->dz:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
