.class public Lcom/android/exchange/a/b;
.super Ljava/lang/Object;
.source "ProxyUtils.java"


# static fields
.field private static eH:Landroid/net/ConnectivityManager;

.field private static eI:Landroid/net/LinkProperties;

.field private static final eJ:Ljava/lang/String;

.field private static final eK:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 22
    sput-object v0, Lcom/android/exchange/a/b;->eH:Landroid/net/ConnectivityManager;

    .line 23
    sput-object v0, Lcom/android/exchange/a/b;->eI:Landroid/net/LinkProperties;

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "numeric=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 25
    const-string v1, "gsm.sim.operator.numeric"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND current=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/a/b;->eJ:Ljava/lang/String;

    .line 27
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/a/b;->eK:Landroid/net/Uri;

    .line 29
    return-void
.end method

.method private static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 108
    .line 114
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    .line 115
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 116
    const-string v4, "_id"

    aput-object v4, v2, v3

    .line 117
    sget-object v3, Lcom/android/exchange/a/b;->eJ:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 114
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 119
    if-eqz v1, :cond_b

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 120
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    move-result v2

    .line 121
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v10, :cond_1

    .line 122
    invoke-static {p0}, Lcom/android/exchange/a/b;->f(Landroid/content/Context;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-result v0

    .line 124
    :cond_0
    if-ne v2, v0, :cond_4

    .line 134
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 135
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_2
    :goto_1
    if-eq v2, v7, :cond_a

    .line 138
    sget-object v0, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 139
    new-array v2, v10, [Ljava/lang/String;

    .line 140
    const-string v0, "proxy"

    aput-object v0, v2, v8

    const-string v0, "port"

    aput-object v0, v2, v9

    .line 142
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 145
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_3
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    .line 146
    if-eqz v1, :cond_9

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 147
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v6

    .line 148
    const/4 v0, 0x1

    :try_start_5
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v7

    move-object v0, v6

    .line 153
    :goto_2
    if-eqz v1, :cond_3

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 157
    :cond_3
    :goto_3
    if-nez p1, :cond_7

    .line 160
    :goto_4
    return-object v0

    .line 127
    :cond_4
    const/4 v3, 0x0

    :try_start_6
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 128
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    move-object v1, v6

    move v2, v7

    .line 132
    :goto_5
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 134
    if-eqz v1, :cond_2

    .line 135
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 133
    :catchall_0
    move-exception v0

    move-object v1, v6

    .line 134
    :goto_6
    if-eqz v1, :cond_5

    .line 135
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 136
    :cond_5
    throw v0

    .line 150
    :catch_1
    move-exception v0

    move-object v1, v6

    .line 151
    :goto_7
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 153
    if-eqz v6, :cond_8

    .line 154
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v0, v1

    goto :goto_3

    .line 152
    :catchall_1
    move-exception v0

    move-object v1, v6

    .line 153
    :goto_8
    if-eqz v1, :cond_6

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 155
    :cond_6
    throw v0

    .line 160
    :cond_7
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 152
    :catchall_2
    move-exception v0

    goto :goto_8

    :catchall_3
    move-exception v0

    move-object v1, v6

    goto :goto_8

    .line 150
    :catch_2
    move-exception v0

    move-object v11, v1

    move-object v1, v6

    move-object v6, v11

    goto :goto_7

    :catch_3
    move-exception v0

    move-object v11, v1

    move-object v1, v6

    move-object v6, v11

    goto :goto_7

    .line 133
    :catchall_4
    move-exception v0

    goto :goto_6

    .line 131
    :catch_4
    move-exception v0

    move v2, v7

    goto :goto_5

    :catch_5
    move-exception v0

    goto :goto_5

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v0, v6

    goto :goto_2

    :cond_a
    move-object v0, v6

    goto :goto_3

    :cond_b
    move v2, v7

    goto/16 :goto_0
.end method

.method private static e(Landroid/content/Context;)Lcom/android/exchange/a/c;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 85
    .line 87
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    sput-object v0, Lcom/android/exchange/a/b;->eH:Landroid/net/ConnectivityManager;

    .line 88
    sget-object v0, Lcom/android/exchange/a/b;->eH:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveLinkProperties()Landroid/net/LinkProperties;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/a/b;->eI:Landroid/net/LinkProperties;

    .line 89
    sget-object v0, Lcom/android/exchange/a/b;->eH:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 90
    sget-object v2, Lcom/android/exchange/a/b;->eI:Landroid/net/LinkProperties;

    if-eqz v2, :cond_0

    .line 91
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 93
    :try_start_0
    const-class v0, Landroid/net/LinkProperties;

    const-string v2, "getHttpProxy"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 94
    new-instance v0, Lcom/android/exchange/a/c;

    sget-object v3, Lcom/android/exchange/a/b;->eI:Landroid/net/LinkProperties;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/exchange/a/c;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-object v0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private static f(Landroid/content/Context;)I
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 167
    .line 168
    const/4 v6, -0x1

    .line 169
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/exchange/a/b;->eK:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 170
    const-string v4, "_id"

    aput-object v4, v2, v5

    .line 171
    const-string v5, "name ASC"

    move-object v4, v3

    .line 169
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 172
    if-eqz v1, :cond_1

    .line 174
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 175
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 176
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 179
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_1
    if-eqz v3, :cond_2

    .line 184
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 185
    :goto_0
    return v0

    .line 178
    :catchall_0
    move-exception v0

    .line 179
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 180
    throw v0

    :cond_2
    move v0, v6

    goto :goto_0
.end method

.method public static getHost(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    .line 44
    invoke-static {p0}, Lcom/android/exchange/a/b;->e(Landroid/content/Context;)Lcom/android/exchange/a/c;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    invoke-static {v0}, Lcom/android/exchange/a/c;->a(Lcom/android/exchange/a/c;)Ljava/lang/String;

    move-result-object v0

    .line 51
    :goto_0
    if-eqz v0, :cond_1

    .line 52
    invoke-static {v0}, Lcom/android/exchange/a/b;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    :goto_1
    const-string v1, "EmailProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "host = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-object v0

    .line 48
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/exchange/a/b;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getPort(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 70
    .line 71
    invoke-static {p0}, Lcom/android/exchange/a/b;->e(Landroid/content/Context;)Lcom/android/exchange/a/c;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-static {v0}, Lcom/android/exchange/a/c;->b(Lcom/android/exchange/a/c;)I

    move-result v0

    .line 77
    :goto_0
    const-string v1, "EmailProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "port = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return v0

    .line 75
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/exchange/a/b;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static i(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 190
    const-string v0, "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 191
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    .line 192
    if-nez v0, :cond_0

    .line 210
    :goto_0
    return-object p0

    .line 196
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 197
    const-string v1, "\\."

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 199
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 200
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 201
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 202
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 203
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 204
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 205
    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    goto :goto_0
.end method
