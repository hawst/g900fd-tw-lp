.class Lcom/android/exchange/a/a$a;
.super Lorg/apache/http/impl/client/DefaultRedirectHandler;
.source "HttpRedirect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/a/a;->a(Lorg/apache/http/client/methods/HttpGet;)Ljava/net/URI;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/apache/http/impl/client/DefaultRedirectHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public isRedirectRequested(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)Z
    .locals 2

    .prologue
    .line 22
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 23
    const/16 v1, 0x12e

    if-eq v0, v1, :cond_0

    .line 24
    const/16 v1, 0x12d

    if-eq v0, v1, :cond_0

    .line 25
    const/16 v1, 0x12f

    if-eq v0, v1, :cond_0

    .line 26
    const/16 v1, 0x133

    if-ne v0, v1, :cond_1

    .line 27
    :cond_0
    const-string v0, "location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-static {v0}, Lcom/android/exchange/a/a;->a(Ljava/net/URI;)V

    .line 33
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
