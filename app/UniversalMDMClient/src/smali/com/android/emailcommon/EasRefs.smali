.class public Lcom/android/emailcommon/EasRefs;
.super Ljava/lang/Object;
.source "EasRefs.java"


# static fields
.field public static DEBUG:Z

.field public static LOGD:Z

.field public static LOG_TAG:Ljava/lang/String;

.field public static ci:Z

.field public static cj:Z

.field public static ck:Z

.field public static cl:Z

.field public static cm:Z

.field public static cn:Z

.field public static co:Z

.field public static final cp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->ci:Z

    .line 35
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    .line 38
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->cj:Z

    .line 40
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->ck:Z

    .line 42
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->cl:Z

    .line 45
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->cm:Z

    .line 48
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->cn:Z

    .line 51
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->co:Z

    .line 101
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "."

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->cp:Ljava/lang/String;

    .line 174
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->LOGD:Z

    .line 175
    const-string v0, "FixIt"

    sput-object v0, Lcom/android/emailcommon/EasRefs;->LOG_TAG:Ljava/lang/String;

    .line 434
    return-void
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/Double;
    .locals 2

    .prologue
    .line 415
    const-string v0, "2.5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 424
    :goto_0
    return-object v0

    .line 417
    :cond_0
    const-string v0, "12.0"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    const-wide/high16 v0, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 419
    :cond_1
    const-string v0, "12.1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 420
    const-wide v0, 0x4028333333333333L    # 12.1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 421
    :cond_2
    const-string v0, "14.0"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 422
    const-wide/high16 v0, 0x402c000000000000L    # 14.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 423
    :cond_3
    const-string v0, "14.1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 424
    const-wide v0, 0x402c333333333333L    # 14.1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 426
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "illegal protocol version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
