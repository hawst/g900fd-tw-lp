.class public Lcom/android/emailcommon/mail/MessagingException;
.super Ljava/lang/Exception;
.source "MessagingException.java"


# static fields
.field public static final serialVersionUID:J = -0x1L


# instance fields
.field protected mExceptionData:Ljava/lang/Object;

.field private final mExceptionString:[Ljava/lang/String;

.field protected mExceptionType:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 504
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 39
    const/16 v0, 0x66

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 45
    const-string v2, " NO_ERROR "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, " UNSPECIFIED_EXCEPTION "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, " IOERROR "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, " TLS_REQUIRED "

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 46
    const-string v2, " AUTH_REQUIRED "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, " GENERAL_SECURITY "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 47
    const-string v2, " AUTHENTICATION_FAILED "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " DUPLICATE_ACCOUNT "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 48
    const-string v2, " SECURITY_POLICIES_REQUIRED "

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 49
    const-string v2, " SECURITY_POLICIES_UNSUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 50
    const-string v2, " PROTOCOL_VERSION_UNSUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 51
    const-string v2, " GAL_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 52
    const-string v2, " OOO_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 53
    const-string v2, " SECURITY_POLICIES_POP3IMAP_DISABLED "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 54
    const-string v2, " EMAIL_SYNC_DISABLED "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 55
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 56
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 57
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 58
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 59
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 60
    const-string v2, " LEGACY_SERVER_NORESPONSE "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 61
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 62
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 63
    const-string v2, " SECURITY_POLICY_ATTACHMENT_MAX_SIZE "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 64
    const-string v2, " SECURITY_POLICY_ATTACHMENT_DISABLED "

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 65
    const-string v2, " ATTACHMENT_DOWNLOAD_FAILED "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 66
    const-string v2, " UNSUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 67
    const-string v2, " LOADMORE_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 68
    const-string v2, " SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 69
    const-string v2, " IN_PROGRESS "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 70
    const-string v2, " ACCOUNT_MOVE_SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 71
    const-string v2, " MESSAGE_NOT_FOUND "

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 72
    const-string v2, " ATTACHMENT_NOT_FOUND "

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 73
    const-string v2, " FOLDER_NOT_DELETED "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 74
    const-string v2, " FOLDER_NOT_RENAMED "

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 75
    const-string v2, " FOLDER_NOT_CREATED "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 76
    const-string v2, " REMOTE_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 77
    const-string v2, " LOGIN_FAILED "

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 78
    const-string v2, " SECURITY_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 79
    const-string v2, " ACCOUNT_UNINITIALIZED "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 80
    const-string v2, " CONNECTION_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 81
    const-string v2, " ERROR_NO_PROTOCOL_SUPPORT "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 82
    const-string v2, " ERROR_SERVER_CONNECT "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " ERROR_GAL_NULL_STRING "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " ERROR_GAL_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 83
    const-string v2, " ERROR_GAL_INVALID_RANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, " ERROR_ITEM "

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " ERROR_OOO_SET_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 84
    const-string v2, " ERROR_OOO_GET_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, " ERROR_OOO_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " ERROR_SERVER_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 85
    const-string v2, " ERROR_SYSTEM_FOLDER "

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, " ERROR_FOLDER_NOT_EXIST "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, " ERROR_FOLDER_EXISTS "

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 86
    const-string v2, " ERROR_FOLDER_PARENT_NOT_FOUND "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " ERROR_EMPTYTRASH_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 87
    const-string v2, " ERROR_EMPTYTRASH_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " ERROR_FETCH_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " ERROR_FETCH_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 88
    const-string v2, " ERROR_FETCH_OUTOFMEMORY "

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, " ERROR_EMPTYTRASH_TIMEOUT "

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, " ERROR_FETCH_NULLMSG "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    .line 89
    const-string v2, " EMPTYTRASH_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, " ERROR_INVALID_PARAM "

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " ERROR_OOO_NOT_SUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    .line 90
    const-string v2, " ERROR_CONNECTING_SERVICE "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " FILE_IOERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, " ERROR_FETCHING_MSG_BODY "

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 91
    const-string v2, " ERROR_SERVICE_RESPONSE_NULL "

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " ERROR_PROCESS_SERVICE_RESPONSE_SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x46

    .line 92
    const-string v2, " ERROR_SERVICE_RESPONSE_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, " ERROR_MAX_RETRY_ATTEMPTS_REACHED "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 93
    const-string v2, " ERROR_NOT_FOUND_IN_DB "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ERROR_LOADING_BODY "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, " ERROR_VIEW_ATTACHMENT "

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    .line 94
    const-string v2, " ERROR_SERVER_ERROR_15MIN_RESTRICTION"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " CERTIFICATE_VALIDATION_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    .line 95
    const-string v2, " AUTODISCOVER_AUTHENTICATION_FAILED "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, " AUTODISCOVER_AUTHENTICATION_RESULT "

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    .line 96
    const-string v2, " AUTHENTICATION_FAILED_OR_SERVER_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, " LICENSE_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    .line 97
    const-string v2, " PROGRESS_CHECK_INCOMIMNG "

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, " PROGRESS_CHECK_OUTGOING "

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " CHECK_SETTINGS_OK "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    .line 98
    const-string v2, " CHECK_SETTINGS_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " CANCEL_CHECK_SETTINGS_SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    .line 99
    const-string v2, " CANCEL_CHECK_SETTINGS_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " ERROR_CONNECTION_READ_TIMEOUT "

    aput-object v2, v0, v1

    const/16 v1, 0x58

    .line 100
    const-string v2, " ERROR_SERVER_ERROR_HTTP_5XX"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, " ERROR_SERVER_ERROR_INSUFFICIENT_STORAGE"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    .line 101
    const-string v2, " ERROR_SERVER_NOT_FOUND"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " ERROR_SERVER_NOT_RESPONDING "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    .line 102
    const-string v2, " ERROR_NETWORK_NOT_AVAILABLE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, " ERROR_DELETE_ACCOUNT_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    .line 103
    const-string v2, " ERROR_NETWORK_TRANSIENT_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " ERROR_NO_NETWORK"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    .line 104
    const-string v2, " ERROR_FOLDER_UPDATE_NOT_ALLOWED"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, " PROGRESS_CHECK_LICENSE_SERVER"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    .line 105
    const-string v2, " ERROR_SEND_MSG_UNLOADED_ATTACHMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, " MAX_DEVICE_PARTNERSHIP_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    .line 106
    const-string v2, " ERROR_CERTIFICATE_NOT_AVAILABLE"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, " DEVICE_BLOCKED_EXCEPTION"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/emailcommon/mail/MessagingException;->mExceptionString:[Ljava/lang/String;

    .line 505
    iput p1, p0, Lcom/android/emailcommon/mail/MessagingException;->mExceptionType:I

    .line 506
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 514
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 39
    const/16 v0, 0x66

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 45
    const-string v2, " NO_ERROR "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, " UNSPECIFIED_EXCEPTION "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, " IOERROR "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, " TLS_REQUIRED "

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 46
    const-string v2, " AUTH_REQUIRED "

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, " GENERAL_SECURITY "

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 47
    const-string v2, " AUTHENTICATION_FAILED "

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, " DUPLICATE_ACCOUNT "

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 48
    const-string v2, " SECURITY_POLICIES_REQUIRED "

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 49
    const-string v2, " SECURITY_POLICIES_UNSUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 50
    const-string v2, " PROTOCOL_VERSION_UNSUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 51
    const-string v2, " GAL_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 52
    const-string v2, " OOO_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 53
    const-string v2, " SECURITY_POLICIES_POP3IMAP_DISABLED "

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 54
    const-string v2, " EMAIL_SYNC_DISABLED "

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 55
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 56
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 57
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 58
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 59
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 60
    const-string v2, " LEGACY_SERVER_NORESPONSE "

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 61
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 62
    const-string v2, " STRANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 63
    const-string v2, " SECURITY_POLICY_ATTACHMENT_MAX_SIZE "

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 64
    const-string v2, " SECURITY_POLICY_ATTACHMENT_DISABLED "

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 65
    const-string v2, " ATTACHMENT_DOWNLOAD_FAILED "

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 66
    const-string v2, " UNSUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 67
    const-string v2, " LOADMORE_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 68
    const-string v2, " SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 69
    const-string v2, " IN_PROGRESS "

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 70
    const-string v2, " ACCOUNT_MOVE_SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 71
    const-string v2, " MESSAGE_NOT_FOUND "

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 72
    const-string v2, " ATTACHMENT_NOT_FOUND "

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 73
    const-string v2, " FOLDER_NOT_DELETED "

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 74
    const-string v2, " FOLDER_NOT_RENAMED "

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 75
    const-string v2, " FOLDER_NOT_CREATED "

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 76
    const-string v2, " REMOTE_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 77
    const-string v2, " LOGIN_FAILED "

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 78
    const-string v2, " SECURITY_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 79
    const-string v2, " ACCOUNT_UNINITIALIZED "

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 80
    const-string v2, " CONNECTION_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 81
    const-string v2, " ERROR_NO_PROTOCOL_SUPPORT "

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 82
    const-string v2, " ERROR_SERVER_CONNECT "

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, " ERROR_GAL_NULL_STRING "

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, " ERROR_GAL_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 83
    const-string v2, " ERROR_GAL_INVALID_RANGE "

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, " ERROR_ITEM "

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, " ERROR_OOO_SET_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 84
    const-string v2, " ERROR_OOO_GET_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, " ERROR_OOO_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, " ERROR_SERVER_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 85
    const-string v2, " ERROR_SYSTEM_FOLDER "

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, " ERROR_FOLDER_NOT_EXIST "

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, " ERROR_FOLDER_EXISTS "

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 86
    const-string v2, " ERROR_FOLDER_PARENT_NOT_FOUND "

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, " ERROR_EMPTYTRASH_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 87
    const-string v2, " ERROR_EMPTYTRASH_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, " ERROR_FETCH_RESPONSE_PARSE "

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, " ERROR_FETCH_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 88
    const-string v2, " ERROR_FETCH_OUTOFMEMORY "

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, " ERROR_EMPTYTRASH_TIMEOUT "

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, " ERROR_FETCH_NULLMSG "

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    .line 89
    const-string v2, " EMPTYTRASH_EXCEPTION "

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, " ERROR_INVALID_PARAM "

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, " ERROR_OOO_NOT_SUPPORTED "

    aput-object v2, v0, v1

    const/16 v1, 0x41

    .line 90
    const-string v2, " ERROR_CONNECTING_SERVICE "

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, " FILE_IOERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, " ERROR_FETCHING_MSG_BODY "

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 91
    const-string v2, " ERROR_SERVICE_RESPONSE_NULL "

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, " ERROR_PROCESS_SERVICE_RESPONSE_SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x46

    .line 92
    const-string v2, " ERROR_SERVICE_RESPONSE_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, " ERROR_MAX_RETRY_ATTEMPTS_REACHED "

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 93
    const-string v2, " ERROR_NOT_FOUND_IN_DB "

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, " ERROR_LOADING_BODY "

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, " ERROR_VIEW_ATTACHMENT "

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    .line 94
    const-string v2, " ERROR_SERVER_ERROR_15MIN_RESTRICTION"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, " CERTIFICATE_VALIDATION_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    .line 95
    const-string v2, " AUTODISCOVER_AUTHENTICATION_FAILED "

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, " AUTODISCOVER_AUTHENTICATION_RESULT "

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    .line 96
    const-string v2, " AUTHENTICATION_FAILED_OR_SERVER_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, " LICENSE_FAILURE "

    aput-object v2, v0, v1

    const/16 v1, 0x51

    .line 97
    const-string v2, " PROGRESS_CHECK_INCOMIMNG "

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, " PROGRESS_CHECK_OUTGOING "

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, " CHECK_SETTINGS_OK "

    aput-object v2, v0, v1

    const/16 v1, 0x54

    .line 98
    const-string v2, " CHECK_SETTINGS_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, " CANCEL_CHECK_SETTINGS_SUCCESS "

    aput-object v2, v0, v1

    const/16 v1, 0x56

    .line 99
    const-string v2, " CANCEL_CHECK_SETTINGS_ERROR "

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, " ERROR_CONNECTION_READ_TIMEOUT "

    aput-object v2, v0, v1

    const/16 v1, 0x58

    .line 100
    const-string v2, " ERROR_SERVER_ERROR_HTTP_5XX"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, " ERROR_SERVER_ERROR_INSUFFICIENT_STORAGE"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    .line 101
    const-string v2, " ERROR_SERVER_NOT_FOUND"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, " ERROR_SERVER_NOT_RESPONDING "

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    .line 102
    const-string v2, " ERROR_NETWORK_NOT_AVAILABLE "

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, " ERROR_DELETE_ACCOUNT_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    .line 103
    const-string v2, " ERROR_NETWORK_TRANSIENT_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, " ERROR_NO_NETWORK"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    .line 104
    const-string v2, " ERROR_FOLDER_UPDATE_NOT_ALLOWED"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, " PROGRESS_CHECK_LICENSE_SERVER"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    .line 105
    const-string v2, " ERROR_SEND_MSG_UNLOADED_ATTACHMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, " MAX_DEVICE_PARTNERSHIP_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    .line 106
    const-string v2, " ERROR_CERTIFICATE_NOT_AVAILABLE"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, " DEVICE_BLOCKED_EXCEPTION"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/emailcommon/mail/MessagingException;->mExceptionString:[Ljava/lang/String;

    .line 515
    iput p1, p0, Lcom/android/emailcommon/mail/MessagingException;->mExceptionType:I

    .line 516
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mExceptionType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/emailcommon/mail/MessagingException;->mExceptionType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
