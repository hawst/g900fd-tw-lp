.class public Lcom/android/emailcommon/utility/a;
.super Ljava/lang/Object;
.source "SSLSocketFactory.java"

# interfaces
.implements Lorg/apache/http/conn/scheme/LayeredSocketFactory;


# static fields
.field public static final dp:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

.field public static final dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

.field public static final dr:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

.field private static final ds:Lcom/android/emailcommon/utility/a;


# instance fields
.field private final dt:Ljavax/net/ssl/SSLContext;

.field private final du:Ljavax/net/ssl/SSLSocketFactory;

.field private final dv:Lorg/apache/http/conn/scheme/HostNameResolver;

.field private dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/a;->dp:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 175
    new-instance v0, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 177
    new-instance v0, Lorg/apache/http/conn/ssl/StrictHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/StrictHostnameVerifier;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/a;->dr:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 182
    new-instance v0, Lcom/android/emailcommon/utility/a;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/a;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/a;->ds:Lcom/android/emailcommon/utility/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    sget-object v0, Lcom/android/emailcommon/utility/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    iput-object v0, p0, Lcom/android/emailcommon/utility/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 258
    iput-object v1, p0, Lcom/android/emailcommon/utility/a;->dt:Ljavax/net/ssl/SSLContext;

    .line 259
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/utility/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    .line 260
    iput-object v1, p0, Lcom/android/emailcommon/utility/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    .line 261
    return-void
.end method

.method public constructor <init>(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    sget-object v0, Lcom/android/emailcommon/utility/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    iput-object v0, p0, Lcom/android/emailcommon/utility/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 246
    iput-object v1, p0, Lcom/android/emailcommon/utility/a;->dt:Ljavax/net/ssl/SSLContext;

    .line 247
    iput-object p1, p0, Lcom/android/emailcommon/utility/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    .line 248
    iput-object v1, p0, Lcom/android/emailcommon/utility/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    .line 249
    return-void
.end method


# virtual methods
.method public a(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V
    .locals 2

    .prologue
    .line 394
    if-nez p1, :cond_0

    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Hostname verifier may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_0
    iput-object p1, p0, Lcom/android/emailcommon/utility/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 398
    return-void
.end method

.method public connectSocket(Ljava/net/Socket;Ljava/lang/String;ILjava/net/InetAddress;ILorg/apache/http/params/HttpParams;)Ljava/net/Socket;
    .locals 5

    .prologue
    .line 297
    if-nez p2, :cond_0

    .line 298
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target host may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    if-nez p6, :cond_1

    .line 301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameters may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_1
    if-eqz p1, :cond_5

    move-object v0, p1

    :goto_0
    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 306
    if-nez p4, :cond_2

    if-lez p5, :cond_4

    .line 309
    :cond_2
    if-gez p5, :cond_3

    .line 310
    const/4 p5, 0x0

    .line 312
    :cond_3
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p4, p5}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 313
    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->bind(Ljava/net/SocketAddress;)V

    .line 316
    :cond_4
    invoke-static {p6}, Lorg/apache/http/params/HttpConnectionParams;->getConnectionTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v2

    .line 317
    invoke-static {p6}, Lorg/apache/http/params/HttpConnectionParams;->getSoTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v3

    .line 320
    iget-object v1, p0, Lcom/android/emailcommon/utility/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    if-eqz v1, :cond_6

    .line 321
    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lcom/android/emailcommon/utility/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    invoke-interface {v4, p2}, Lorg/apache/http/conn/scheme/HostNameResolver;->resolve(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    invoke-direct {v1, v4, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 326
    :goto_1
    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    .line 328
    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 330
    :try_start_0
    iget-object v1, p0, Lcom/android/emailcommon/utility/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-interface {v1, p2, v0}, Lorg/apache/http/conn/ssl/X509HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSocket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    return-object v0

    .line 304
    :cond_5
    invoke-virtual {p0}, Lcom/android/emailcommon/utility/a;->createSocket()Ljava/net/Socket;

    move-result-object v0

    goto :goto_0

    .line 323
    :cond_6
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p2, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    goto :goto_1

    .line 332
    :catch_0
    move-exception v1

    .line 335
    :try_start_1
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 338
    :goto_2
    throw v1

    .line 336
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public createSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/emailcommon/utility/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/android/emailcommon/utility/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 378
    :try_start_0
    iget-object v1, p0, Lcom/android/emailcommon/utility/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-interface {v1, p2, v0}, Lorg/apache/http/conn/ssl/X509HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSocket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    return-object v0

    .line 380
    :catch_0
    move-exception v1

    .line 383
    :try_start_1
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 386
    :goto_0
    throw v1

    .line 384
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public isSecure(Ljava/net/Socket;)Z
    .locals 2

    .prologue
    .line 356
    if-nez p1, :cond_0

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360
    :cond_0
    instance-of v0, p1, Ljavax/net/ssl/SSLSocket;

    if-nez v0, :cond_1

    .line 361
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket not created by this factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_1
    invoke-virtual {p1}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
