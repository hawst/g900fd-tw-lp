.class public abstract Lcom/android/emailcommon/provider/EmailContent;
.super Ljava/lang/Object;
.source "EmailContent.java"


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final cq:Landroid/net/Uri;

.field public static final cr:[Ljava/lang/String;

.field public static final cs:[Ljava/lang/String;

.field public static final ct:[Ljava/lang/String;

.field public static final cu:[Ljava/lang/String;

.field private static cv:I

.field private static cw:I


# instance fields
.field public cx:J

.field public mBaseUri:Landroid/net/Uri;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 55
    const-string v0, "content://com.android.email.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    .line 59
    const-string v0, "content://com.android.email.notifier"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->cq:Landroid/net/Uri;

    .line 70
    new-array v0, v2, [Ljava/lang/String;

    .line 71
    const-string v1, "count(*)"

    aput-object v1, v0, v3

    .line 70
    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->cr:[Ljava/lang/String;

    .line 84
    new-array v0, v2, [Ljava/lang/String;

    .line 85
    const-string v1, "_id"

    aput-object v1, v0, v3

    .line 84
    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->cs:[Ljava/lang/String;

    .line 90
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 91
    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "emailAddress"

    aput-object v1, v0, v2

    const-string v1, "displayName"

    aput-object v1, v0, v4

    .line 90
    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->ct:[Ljava/lang/String;

    .line 103
    new-array v0, v4, [Ljava/lang/String;

    .line 104
    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "syncInterval"

    aput-object v1, v0, v2

    .line 103
    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->cu:[Ljava/lang/String;

    .line 109
    sput v2, Lcom/android/emailcommon/provider/EmailContent;->cv:I

    .line 111
    sput v2, Lcom/android/emailcommon/provider/EmailContent;->cw:I

    .line 143
    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mUri:Landroid/net/Uri;

    .line 139
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent;->cx:J

    .line 150
    return-void
.end method
