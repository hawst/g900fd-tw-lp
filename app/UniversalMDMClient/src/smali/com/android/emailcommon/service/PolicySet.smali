.class public Lcom/android/emailcommon/service/PolicySet;
.super Ljava/lang/Object;
.source "PolicySet.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/emailcommon/service/PolicySet;",
            ">;"
        }
    .end annotation
.end field

.field private static final cB:Ljava/lang/Object;


# instance fields
.field public cC:I

.field public cD:I

.field public cE:I

.field public cF:I

.field public cG:Z

.field public cH:I

.field public cI:I

.field public cJ:I

.field public cK:Z

.field public cL:Z

.field public cM:Z

.field public cN:Z

.field public cO:I

.field public cP:Z

.field public cQ:Z

.field public cR:Z

.field public cS:Z

.field public cT:Z

.field public cU:Z

.field public cV:Z

.field public cW:Z

.field public cX:Z

.field public cY:I

.field public cZ:I

.field public da:I

.field public db:I

.field public dc:I

.field public dd:Z

.field public de:Z

.field public df:I

.field public dg:I

.field public dh:I

.field public di:Z

.field public dj:Z

.field public dk:Z

.field public dl:Ljava/lang/String;

.field public dm:Ljava/lang/String;

.field public dn:Z

.field public do:Z

.field public mSimplePasswordEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/emailcommon/service/PolicySet;->cB:Ljava/lang/Object;

    .line 607
    new-instance v0, Lcom/android/emailcommon/service/PolicySet$1;

    invoke-direct {v0}, Lcom/android/emailcommon/service/PolicySet$1;-><init>()V

    sput-object v0, Lcom/android/emailcommon/service/PolicySet;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 615
    return-void
.end method

.method public constructor <init>(IIIIZZIIZIZZZZZZZZZIIIIIIZZIIIZZZZZZLjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4

    .prologue
    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const-string v1, ""

    iput-object v1, p0, Lcom/android/emailcommon/service/PolicySet;->dl:Ljava/lang/String;

    .line 249
    const-string v1, ""

    iput-object v1, p0, Lcom/android/emailcommon/service/PolicySet;->dm:Ljava/lang/String;

    .line 250
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->dn:Z

    .line 251
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->do:Z

    .line 332
    if-nez p2, :cond_1

    .line 333
    const/4 v2, 0x0

    .line 334
    const/4 p4, 0x0

    .line 335
    const/4 p1, 0x0

    .line 336
    const/4 v1, 0x0

    .line 337
    const/4 p8, 0x0

    .line 338
    const/4 p7, 0x0

    .line 375
    :cond_0
    :goto_0
    iput p1, p0, Lcom/android/emailcommon/service/PolicySet;->cC:I

    .line 385
    const/16 v3, 0x40

    if-ne p2, v3, :cond_7

    const/4 v3, 0x2

    if-le v1, v3, :cond_7

    .line 386
    const/16 v3, 0x60

    iput v3, p0, Lcom/android/emailcommon/service/PolicySet;->cD:I

    .line 391
    :goto_1
    iput v2, p0, Lcom/android/emailcommon/service/PolicySet;->cE:I

    .line 392
    iput p4, p0, Lcom/android/emailcommon/service/PolicySet;->cF:I

    .line 393
    iput-boolean p5, p0, Lcom/android/emailcommon/service/PolicySet;->cG:Z

    .line 394
    iput p7, p0, Lcom/android/emailcommon/service/PolicySet;->cH:I

    .line 395
    iput p8, p0, Lcom/android/emailcommon/service/PolicySet;->cI:I

    .line 396
    iput v1, p0, Lcom/android/emailcommon/service/PolicySet;->cJ:I

    .line 397
    move/from16 v0, p34

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cK:Z

    .line 398
    move/from16 v0, p35

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cL:Z

    .line 410
    iput-boolean p6, p0, Lcom/android/emailcommon/service/PolicySet;->cM:Z

    .line 412
    iput-boolean p9, p0, Lcom/android/emailcommon/service/PolicySet;->cN:Z

    .line 414
    move/from16 v0, p36

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->mSimplePasswordEnabled:Z

    .line 416
    iput p10, p0, Lcom/android/emailcommon/service/PolicySet;->cO:I

    .line 420
    iput-boolean p11, p0, Lcom/android/emailcommon/service/PolicySet;->cP:Z

    .line 421
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cQ:Z

    .line 422
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cR:Z

    .line 423
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cS:Z

    .line 424
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cT:Z

    .line 425
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cU:Z

    .line 426
    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cV:Z

    .line 427
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cW:Z

    .line 428
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cX:Z

    .line 429
    move/from16 v0, p20

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cY:I

    .line 431
    move/from16 v0, p22

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cZ:I

    .line 432
    move/from16 v0, p23

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->da:I

    .line 433
    move/from16 v0, p24

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->db:I

    .line 434
    move/from16 v0, p25

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->dc:I

    .line 435
    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dd:Z

    .line 436
    move/from16 v0, p27

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->de:Z

    .line 437
    move/from16 v0, p28

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->df:I

    .line 438
    move/from16 v0, p29

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->dg:I

    .line 439
    move/from16 v0, p30

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->dh:I

    .line 440
    move/from16 v0, p31

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->di:Z

    .line 441
    move/from16 v0, p32

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dj:Z

    .line 442
    move/from16 v0, p33

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dk:Z

    .line 445
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dl:Ljava/lang/String;

    .line 446
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dm:Ljava/lang/String;

    .line 447
    move/from16 v0, p39

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dn:Z

    .line 448
    move/from16 v0, p40

    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->do:Z

    .line 450
    return-void

    .line 340
    :cond_1
    const/16 v1, 0x20

    if-eq p2, v1, :cond_2

    const/16 v1, 0x40

    if-eq p2, v1, :cond_2

    .line 341
    const/16 v1, 0x60

    if-eq p2, v1, :cond_2

    .line 342
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "password mode"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 347
    :cond_2
    const/16 v1, 0x20

    if-ne p2, v1, :cond_9

    .line 348
    const/4 v1, 0x0

    .line 352
    :goto_2
    const/16 v2, 0x1e

    if-le p1, v2, :cond_3

    .line 353
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "password length"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 355
    :cond_3
    const/16 v2, 0x3ff

    if-le p7, v2, :cond_4

    .line 356
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "password expiration"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 358
    :cond_4
    const/16 v2, 0xff

    if-le p8, v2, :cond_5

    .line 359
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "password history"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 361
    :cond_5
    const/16 v2, 0x1f

    if-le v1, v2, :cond_6

    .line 362
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "complex chars"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 366
    :cond_6
    const/16 v2, 0x1f

    if-le p3, v2, :cond_8

    .line 367
    const/16 v2, 0x1f

    .line 371
    :goto_3
    const/16 v3, 0x7ff

    if-le p4, v3, :cond_0

    .line 372
    const/16 p4, 0x7ff

    goto/16 :goto_0

    .line 388
    :cond_7
    iput p2, p0, Lcom/android/emailcommon/service/PolicySet;->cD:I

    goto/16 :goto_1

    :cond_8
    move v2, p3

    goto :goto_3

    :cond_9
    move/from16 v1, p21

    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const-string v0, ""

    iput-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dl:Ljava/lang/String;

    .line 249
    const-string v0, ""

    iput-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dm:Ljava/lang/String;

    .line 250
    iput-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->dn:Z

    .line 251
    iput-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->do:Z

    .line 674
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cC:I

    .line 675
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cD:I

    .line 676
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cE:I

    .line 677
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cF:I

    .line 678
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cG:Z

    .line 679
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cH:I

    .line 680
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cI:I

    .line 681
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cJ:I

    .line 682
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cM:Z

    .line 683
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cK:Z

    .line 684
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cL:Z

    .line 685
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->mSimplePasswordEnabled:Z

    .line 687
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cN:Z

    .line 688
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cO:I

    .line 689
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->db:I

    .line 690
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->dc:I

    .line 691
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cU:Z

    .line 692
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cZ:I

    .line 693
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->da:I

    .line 694
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cX:Z

    .line 695
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dd:Z

    .line 696
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->de:Z

    .line 697
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->df:I

    .line 698
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->dg:I

    .line 699
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->dh:I

    .line 702
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cP:Z

    .line 703
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cQ:Z

    .line 704
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cR:Z

    .line 705
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cS:Z

    .line 706
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cT:Z

    .line 707
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cV:Z

    .line 708
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_10

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cW:Z

    .line 709
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_11

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->di:Z

    .line 710
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_12

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dj:Z

    .line 711
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_13

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dk:Z

    .line 712
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/service/PolicySet;->cY:I

    .line 714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dl:Ljava/lang/String;

    .line 715
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dm:Ljava/lang/String;

    .line 716
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_14

    move v0, v1

    :goto_14
    iput-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dn:Z

    .line 717
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_15

    :goto_15
    iput-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->do:Z

    .line 720
    return-void

    :cond_0
    move v0, v2

    .line 678
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 682
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 683
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 684
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 685
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 687
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 691
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 694
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 695
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 696
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 702
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 703
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 704
    goto/16 :goto_c

    :cond_d
    move v0, v2

    .line 705
    goto/16 :goto_d

    :cond_e
    move v0, v2

    .line 706
    goto :goto_e

    :cond_f
    move v0, v2

    .line 707
    goto :goto_f

    :cond_10
    move v0, v2

    .line 708
    goto :goto_10

    :cond_11
    move v0, v2

    .line 709
    goto :goto_11

    :cond_12
    move v0, v2

    .line 710
    goto :goto_12

    :cond_13
    move v0, v2

    .line 711
    goto :goto_13

    :cond_14
    move v0, v2

    .line 716
    goto :goto_14

    :cond_15
    move v1, v2

    .line 717
    goto :goto_15
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 601
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 724
    invoke-virtual {p0}, Lcom/android/emailcommon/service/PolicySet;->w()J

    move-result-wide v0

    .line 725
    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 746
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ pw-len-min="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cC:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pw-mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cD:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 747
    const-string v1, ", pw-fails-max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cE:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenlock-max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cF:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 748
    const-string v1, ", remote-wipe-req="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cG:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pw-expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 749
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pw-history="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cI:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 750
    const-string v1, ", pw-complex-chars="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cJ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recoveryEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 751
    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cM:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", require-encryption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cK:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 752
    const-string v1, ", require-SD-encryption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cL:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", attachmentsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 753
    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cN:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxAttachmentsSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cO:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 754
    const-string v1, ", allowHTML="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cU:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requireManualSyncWhenRoaming="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 755
    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cX:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxCalendarAgeFilter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cZ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 756
    const-string v1, ", maxEmailAgeFilter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->da:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxEmailBodyTruncationSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 757
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->db:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxEmailHtmlBodyTruncationSize= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 758
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->dc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requireSignMessage= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 759
    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->dd:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requireEncryptMessage= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 760
    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->de:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowEncryptionNegotiation= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 761
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->dh:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", signAlgorithm= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 762
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->df:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", encryptAlgorithm= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 763
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->dg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowCamera= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cQ:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 764
    const-string v1, ", allowSDcard= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cP:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowWiFi= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cR:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 765
    const-string v1, ", allowSMS= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cS:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowPopImap= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cT:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 766
    const-string v1, ", allowIrDA= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->dk:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowBrowser= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cV:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 767
    const-string v1, ", allowInternetSharing= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->cW:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowBT= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 768
    iget v1, p0, Lcom/android/emailcommon/service/PolicySet;->cY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowKIES= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->dj:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 769
    const-string v1, ", allowSMIMEsoftCert="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->di:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 770
    const-string v1, ", mAllowAppList= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/service/PolicySet;->dl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBlockAppList= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 771
    iget-object v1, p0, Lcom/android/emailcommon/service/PolicySet;->dm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAllowUnsignedApp= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->dn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 772
    const-string v1, ", mAllowUnsignedInstallationPkg= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/service/PolicySet;->do:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 746
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()J
    .locals 5

    .prologue
    .line 729
    .line 730
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cC:I

    int-to-long v0, v0

    const/4 v2, 0x0

    shl-long/2addr v0, v2

    .line 731
    iget v2, p0, Lcom/android/emailcommon/service/PolicySet;->cD:I

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 732
    iget v2, p0, Lcom/android/emailcommon/service/PolicySet;->cE:I

    int-to-long v2, v2

    const/16 v4, 0x9

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 733
    iget v2, p0, Lcom/android/emailcommon/service/PolicySet;->cF:I

    int-to-long v2, v2

    const/16 v4, 0xe

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 734
    iget-boolean v2, p0, Lcom/android/emailcommon/service/PolicySet;->cG:Z

    if-eqz v2, :cond_0

    .line 735
    const-wide/32 v2, 0x2000000

    or-long/2addr v0, v2

    .line 736
    :cond_0
    iget v2, p0, Lcom/android/emailcommon/service/PolicySet;->cI:I

    int-to-long v2, v2

    const/16 v4, 0x24

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 737
    iget v2, p0, Lcom/android/emailcommon/service/PolicySet;->cH:I

    int-to-long v2, v2

    const/16 v4, 0x1a

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 738
    iget v2, p0, Lcom/android/emailcommon/service/PolicySet;->cJ:I

    int-to-long v2, v2

    const/16 v4, 0x2c

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 739
    iget-boolean v2, p0, Lcom/android/emailcommon/service/PolicySet;->cK:Z

    if-eqz v2, :cond_1

    .line 740
    const-wide/high16 v2, 0x2000000000000L

    or-long/2addr v0, v2

    .line 741
    :cond_1
    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 621
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cC:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 622
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cD:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 623
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cE:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 624
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cF:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cG:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 626
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cH:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 627
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 628
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cJ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 629
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cM:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 630
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cK:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 631
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cL:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 633
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->mSimplePasswordEnabled:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 636
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cN:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 637
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cO:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 638
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->db:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 639
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->dc:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 640
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cU:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 641
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cZ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 642
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->da:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 643
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cX:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 644
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dd:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 645
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->de:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 646
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->df:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 647
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->dg:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 648
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->dh:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 651
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cP:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 652
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cQ:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 653
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cR:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 654
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cS:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 655
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cT:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 656
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cV:Z

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 657
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->cW:Z

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 658
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->di:Z

    if-eqz v0, :cond_11

    move v0, v1

    :goto_11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 659
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dj:Z

    if-eqz v0, :cond_12

    move v0, v1

    :goto_12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 660
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dk:Z

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 661
    iget v0, p0, Lcom/android/emailcommon/service/PolicySet;->cY:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 663
    iget-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Lcom/android/emailcommon/service/PolicySet;->dm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 665
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->dn:Z

    if-eqz v0, :cond_14

    move v0, v1

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 666
    iget-boolean v0, p0, Lcom/android/emailcommon/service/PolicySet;->do:Z

    if-eqz v0, :cond_15

    :goto_15
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 668
    return-void

    :cond_0
    move v0, v2

    .line 625
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 629
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 630
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 631
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 633
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 636
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 640
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 643
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 644
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 645
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 651
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 652
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 653
    goto :goto_c

    :cond_d
    move v0, v2

    .line 654
    goto :goto_d

    :cond_e
    move v0, v2

    .line 655
    goto :goto_e

    :cond_f
    move v0, v2

    .line 656
    goto :goto_f

    :cond_10
    move v0, v2

    .line 657
    goto :goto_10

    :cond_11
    move v0, v2

    .line 658
    goto :goto_11

    :cond_12
    move v0, v2

    .line 659
    goto :goto_12

    :cond_13
    move v0, v2

    .line 660
    goto :goto_13

    :cond_14
    move v0, v2

    .line 665
    goto :goto_14

    :cond_15
    move v1, v2

    .line 666
    goto :goto_15
.end method
