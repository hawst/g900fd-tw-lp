.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;
.super Ljava/lang/Object;
.source "EmailCertificatePickerFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation


# instance fields
.field final synthetic rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

.field private final synthetic rZ:Landroid/widget/EditText;

.field private final synthetic sa:Landroid/widget/EditText;

.field private final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;ILandroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->val$id:I

    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rZ:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->sa:Landroid/widget/EditText;

    .line 436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 440
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->val$id:I

    if-ne v0, v4, :cond_2

    .line 441
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rZ:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->sa:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 443
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rZ:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 445
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 447
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rZ:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 448
    if-eqz v1, :cond_1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 449
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 453
    :cond_2
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->val$id:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->sa:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 455
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rZ:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->sa:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 457
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->sa:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 458
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->sa:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 460
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 462
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
