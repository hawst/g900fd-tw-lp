.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;
.super Ljava/lang/Object;
.source "PolicyClassHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field context:Landroid/content/Context;

.field contextInfo:Landroid/app/enterprise/ContextInfo;

.field gvpn:Lcom/sec/enterprise/knox/GenericVpnPolicy;

.field mPolicyCreationListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;

.field mReceivedIntent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

.field vendorPkgName:Ljava/lang/String;

.field final vpnBindReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)V
    .locals 4

    .prologue
    .line 1199
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    .line 1198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1250
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vpnBindReceiver:Landroid/content/BroadcastReceiver;

    .line 1200
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->context:Landroid/content/Context;

    .line 1201
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    .line 1202
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vendorPkgName:Ljava/lang/String;

    .line 1203
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->mPolicyCreationListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;

    .line 1205
    const-string v0, "com.samsung.android.mdm.VPN_BIND_RESULT"

    .line 1206
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vpnBindReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1209
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v1, 0x1

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKM(Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v1

    .line 1211
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->LEGACY_CONTAINER_ID:I

    if-eq v0, v2, :cond_0

    .line 1212
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->DEVICE_CONTAINER_ID:I

    if-eq v0, v2, :cond_0

    .line 1213
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 1216
    :goto_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vendorPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getGenericVpnPolicy(Ljava/lang/String;I)Lcom/sec/enterprise/knox/GenericVpnPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->gvpn:Lcom/sec/enterprise/knox/GenericVpnPolicy;

    .line 1218
    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    monitor-enter v1

    .line 1219
    :try_start_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vendorPkgName:Ljava/lang/String;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->gvpn:Lcom/sec/enterprise/knox/GenericVpnPolicy;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1218
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221
    return-void

    .line 1215
    :cond_0
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->DEVICE_CONTAINER_ID:I

    goto :goto_0

    .line 1218
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1229
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vpnBindReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1230
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->mReceivedIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.mdm.VPN_BIND_RESULT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1231
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->mReceivedIntent:Landroid/content/Intent;

    const-string v1, "vpn_bind_vendor"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1232
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->mReceivedIntent:Landroid/content/Intent;

    const-string v2, "vpn_bind_status"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1233
    if-eqz v1, :cond_1

    .line 1234
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " bind is successful"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mGenericVpnPolicyMap"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1236
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vendorPkgName:Ljava/lang/String;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->gvpn:Lcom/sec/enterprise/knox/GenericVpnPolicy;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1243
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    monitor-enter v2

    .line 1244
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->vendorPkgName:Ljava/lang/String;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1243
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1246
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->mPolicyCreationListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->gvpn:Lcom/sec/enterprise/knox/GenericVpnPolicy;

    invoke-interface {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;->onPolicyCreation(ZLjava/lang/Object;)V

    .line 1248
    :cond_0
    return-void

    .line 1239
    :cond_1
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " bind is failed"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->gvpn:Lcom/sec/enterprise/knox/GenericVpnPolicy;

    goto :goto_0

    .line 1243
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected schedule()V
    .locals 1

    .prologue
    .line 1224
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;->mPolicyCreationListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;

    invoke-interface {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;->schedule(Ljava/lang/Runnable;)V

    .line 1225
    return-void
.end method
