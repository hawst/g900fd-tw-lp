.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;
.super Landroid/content/BroadcastReceiver;
.source "AlarmHandler.java"


# static fields
.field private static oW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/a;

.field private static oX:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    .line 148
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)I
    .locals 7

    .prologue
    .line 325
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    monitor-enter v1

    :try_start_0
    const-string v0, "UMC:AlarmHandler"

    const-string v2, "Enter storeAlarmData"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->h(Landroid/content/Context;)I

    .line 329
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)J

    move-result-wide v4

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->i(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    const-string v0, "UMC:AlarmHandler"

    const-string v2, "UnLoad List failed"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    const/4 v0, -0x4

    .line 335
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 249
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 250
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 251
    invoke-virtual {v1, p3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    const-string v2, "message"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    const-string v2, "sourceId"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    const/high16 v2, 0x8000000

    .line 254
    invoke-static {p0, v6, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p1

    add-long/2addr v2, v4

    .line 257
    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 258
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    invoke-direct {v0, p3, v2, v3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 259
    const-string v1, "UMC:AlarmHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addAlarm :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-static {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)I

    .line 261
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 203
    const-string v0, "UMC:AlarmHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "resetAlarm :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    const-string v1, "sourceId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 206
    invoke-static {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v2

    .line 207
    if-eqz v2, :cond_0

    .line 208
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getPollingInterval()J

    move-result-wide v2

    .line 209
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 210
    invoke-static {p0, v2, v3, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 293
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 294
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 295
    const-string v2, "message"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    const-string v2, "sourceId"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    const/4 v2, 0x0

    .line 298
    const/high16 v3, 0x8000000

    .line 297
    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 299
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 300
    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    .line 301
    invoke-static {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    return-void
.end method

.method private static declared-synchronized b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 306
    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->h(Landroid/content/Context;)I

    .line 308
    const-string v0, "UMC:AlarmHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Enter removeAlarmData:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const/4 v0, 0x0

    .line 310
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 316
    if-eqz v1, :cond_1

    .line 317
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 320
    :cond_1
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->i(Landroid/content/Context;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    monitor-exit v2

    return-void

    .line 310
    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    .line 311
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v0

    .line 312
    goto :goto_0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static d(Landroid/content/Intent;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 151
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 153
    :try_start_0
    const-string v0, "message"

    const-string v2, "message"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 154
    const-string v0, "sourceId"

    const-string v2, "sourceId"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :goto_0
    return-object v1

    .line 155
    :catch_0
    move-exception v0

    .line 156
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private static declared-synchronized h(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 162
    const-class v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    monitor-enter v6

    :try_start_0
    const-string v3, "UMC:AlarmHandler"

    const-string v4, "Enter loadList"

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    const-string v0, "UMC:AlarmHandler"

    const-string v2, "Already Loaded"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 199
    :goto_0
    monitor-exit v6

    return v0

    .line 169
    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    :try_start_2
    const-string v3, "alarm.dat"

    invoke-virtual {p0, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 175
    :try_start_3
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v4}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 176
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v7

    .line 178
    const-string v0, "UMC:AlarmHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Load List: nitems="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move v5, v1

    .line 180
    :goto_1
    if-lt v5, v7, :cond_3

    .line 191
    if-eqz v4, :cond_1

    .line 192
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 193
    :cond_1
    if-eqz v3, :cond_2

    .line 194
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_2
    move v0, v1

    .line 199
    goto :goto_0

    .line 181
    :cond_3
    :try_start_6
    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 180
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 183
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 191
    :goto_3
    if-eqz v1, :cond_4

    .line 192
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 193
    :cond_4
    if-eqz v0, :cond_5

    .line 194
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_5
    :goto_4
    move v0, v2

    .line 185
    goto :goto_0

    .line 195
    :catch_1
    move-exception v0

    .line 196
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 186
    :catch_2
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v0, v1

    .line 187
    :goto_5
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 191
    if-eqz v4, :cond_6

    .line 192
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 193
    :cond_6
    if-eqz v3, :cond_7

    .line 194
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_7
    :goto_6
    move v0, v2

    .line 188
    goto :goto_0

    .line 195
    :catch_3
    move-exception v0

    .line 196
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    .line 189
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v0, v1

    .line 191
    :goto_7
    if-eqz v4, :cond_8

    .line 192
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 193
    :cond_8
    if-eqz v3, :cond_9

    .line 194
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 198
    :cond_9
    :goto_8
    :try_start_d
    throw v0

    .line 195
    :catch_4
    move-exception v1

    .line 196
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 195
    :catch_5
    move-exception v0

    .line 196
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_2

    .line 189
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_7

    :catchall_3
    move-exception v0

    goto :goto_7

    .line 186
    :catch_6
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_5

    .line 183
    :catch_8
    move-exception v1

    move-object v1, v4

    goto :goto_3

    :catch_9
    move-exception v0

    move-object v0, v3

    move-object v1, v4

    goto :goto_3
.end method

.method private static declared-synchronized i(Landroid/content/Context;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 215
    const-class v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    monitor-enter v4

    :try_start_0
    const-string v2, "UMC:AlarmHandler"

    const-string v3, "Enter unloadList"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    const-string v2, "UMC:AlarmHandler"

    const-string v3, "UnLoad List: empty"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_0
    :try_start_1
    const-string v2, "alarm.dat"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 225
    :try_start_2
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 227
    :try_start_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 228
    const-string v5, "UMC:AlarmHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UnLoad List: nitems="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 230
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v0

    if-nez v0, :cond_3

    .line 237
    if-eqz v3, :cond_1

    .line 238
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 239
    :cond_1
    if-eqz v2, :cond_2

    .line 240
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_1
    move v0, v1

    .line 245
    :goto_2
    monitor-exit v4

    return v0

    .line 230
    :cond_3
    :try_start_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    .line 231
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    move-object v0, v2

    move-object v1, v3

    .line 237
    :goto_3
    if-eqz v1, :cond_4

    .line 238
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 239
    :cond_4
    if-eqz v0, :cond_5

    .line 240
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 234
    :cond_5
    :goto_4
    const/4 v0, -0x1

    goto :goto_2

    .line 241
    :catch_1
    move-exception v0

    .line 242
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 235
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    .line 237
    :goto_5
    if-eqz v3, :cond_6

    .line 238
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 239
    :cond_6
    if-eqz v2, :cond_7

    .line 240
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 244
    :cond_7
    :goto_6
    :try_start_9
    throw v0

    .line 241
    :catch_2
    move-exception v1

    .line 242
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 241
    :catch_3
    move-exception v0

    .line 242
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 235
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catchall_3
    move-exception v0

    goto :goto_5

    .line 232
    :catch_4
    move-exception v1

    move-object v1, v0

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v1, v3

    goto :goto_3
.end method

.method public static init(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 268
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->j(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 269
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    return-void

    .line 269
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    .line 270
    const-string v1, "UMC:AlarmHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "init :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 272
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    const-string v4, "message"

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-string v4, "sourceId"

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const/high16 v4, 0x8000000

    .line 276
    invoke-static {p0, v6, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 278
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)J

    move-result-wide v4

    invoke-virtual {v1, v6, v4, v5, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static declared-synchronized j(Landroid/content/Context;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->h(Landroid/content/Context;)I

    .line 265
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oX:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 339
    const-string v0, "UMC:AlarmHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceive :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v0, "FP_DISABLE_PING"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->C(Landroid/content/Context;)V

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/a;

    if-eqz v0, :cond_0

    .line 345
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->oW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/a;

    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->d(Landroid/content/Intent;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/a;->a(Lorg/json/JSONObject;)V

    .line 346
    invoke-static {p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
