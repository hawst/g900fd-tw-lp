.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "ApplicationMethodInvoker.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;
.implements Ljava/lang/Runnable;


# static fields
.field private static final SPECIAL_HANDLE_METHOD_BACKUP:Ljava/lang/String; = "backupApplicationData"

.field private static final TAG:Ljava/lang/String; = "UMC:ApplicationMethodInvoker"


# instance fields
.field final mApplicationReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 56
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->mApplicationReceiver:Landroid/content/BroadcastReceiver;

    .line 74
    return-void
.end method


# virtual methods
.method protected isInvokeCompleted()Z
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "backupApplicationData"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->getReturnVal()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->getReturnVal()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    .line 83
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 84
    const-string v1, "edm.intent.action.backup.service.available"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->mApplicationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 86
    const-string v0, "UMC:ApplicationMethodInvoker"

    const-string v1, "isInvokeCompleted:false regsiter for ACTION_EDM_BACKUP_SERVICE_AVAILABLE intent"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method schedulex()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;->schedule(Ljava/lang/Runnable;)V

    .line 70
    return-void
.end method
