.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;
.super Ljava/lang/Object;
.source "PendingNetworkOperationProcessor.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->handleDeleteDevice()V
.end annotation


# instance fields
.field final synthetic qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 5

    .prologue
    .line 94
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 95
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gInstance:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$0()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    .line 96
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    .line 95
    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->removePendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gInstance:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$0()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->processPendingNetworkOperation()V

    .line 99
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$3()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v1

    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aE(Ljava/lang/String;)Z

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 103
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->q(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aF(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->retryCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->retryCount:I

    .line 89
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gInstance:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$0()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->storePendingNetworkOperation()V
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;)V

    .line 90
    return-void
.end method
