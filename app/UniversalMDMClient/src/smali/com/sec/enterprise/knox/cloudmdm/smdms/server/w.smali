.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;
.source "ProxyManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

.field private pk:Ljava/lang/String;

.field private pl:Ljava/lang/String;

.field private wU:Ljava/lang/String;

.field private wV:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;

.field private wW:Ljava/lang/String;

.field private wX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I
    .locals 3

    .prologue
    .line 52
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "UMC:ProxyManager"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->aZ(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    return-void
.end method

.method private aZ(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;
    .locals 4

    .prologue
    .line 166
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    const-string v1, "processCreateAccountResponse"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    .line 170
    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    .line 180
    :goto_0
    return-object v0

    .line 174
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    const-string v1, "CreateAccount response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processCreateAccountResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;-><init>()V

    .line 159
    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->setEmailAddress(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->setExchangeServerUrl(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->setProgram(Ljava/lang/String;)V

    .line 162
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 4

    .prologue
    .line 185
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network oper succeeded and called back to observer ProxyManager: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 199
    :goto_0
    return-void

    .line 190
    :pswitch_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 192
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    const-string v1, "Retrying create account after gslb..."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pk:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wU:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pl:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 4

    .prologue
    .line 204
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network operation failed and called back to observer ProxyManager:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 207
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    const-string v1, "https://us-segp-api.secb2b.com/v1"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 208
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    const-string v1, "Retrying create account after gslb..."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pk:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wU:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pl:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->mContext:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pk:Ljava/lang/String;

    .line 91
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wU:Ljava/lang/String;

    .line 92
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pl:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wW:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 95
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wW:Ljava/lang/String;

    .line 96
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "proxyUrl:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wW:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wW:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 100
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    const-string v1, "Need to do GSLB lookup again!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 102
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->k(Landroid/content/Context;Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 107
    :cond_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v0, p0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 144
    :try_start_0
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->g(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wV:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;

    .line 145
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wV:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 146
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Json request:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wW:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, p1, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 148
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Client"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v3, "X-Knox-Client"

    const-string v4, "UMC"

    invoke-virtual {v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v3, "/users"

    invoke-virtual {v2, v3, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CREATE_ACCOUNT failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public ie()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    return-object v0
.end method

.method public if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->wX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    return-object v0
.end method
