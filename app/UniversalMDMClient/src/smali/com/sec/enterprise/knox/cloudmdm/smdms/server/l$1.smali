.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;
.super Lorg/apache/commons/io/output/a;
.source "FtpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
.end annotation


# instance fields
.field final synthetic vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    .line 143
    invoke-direct {p0, p2}, Lorg/apache/commons/io/output/a;-><init>(Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method protected aW(I)V
    .locals 5

    .prologue
    .line 145
    invoke-super {p0, p1}, Lorg/apache/commons/io/output/a;->aW(I)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->getCount()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vL:I

    div-int/2addr v0, v1

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 148
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "downloading:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    iget-wide v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->ro:J

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->getCount()I

    move-result v1

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;->vN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    iget v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vL:I

    invoke-interface {v0, v2, v3, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onProgress(JII)V

    .line 153
    return-void
.end method
