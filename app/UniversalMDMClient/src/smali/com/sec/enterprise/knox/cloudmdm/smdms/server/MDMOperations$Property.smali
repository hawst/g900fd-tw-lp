.class public final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;
.super Ljava/lang/Enum;
.source "MDMOperations.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum wv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

.field public static final enum ww:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

.field public static final enum wx:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

.field public static final enum wy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

.field private static final synthetic wz:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    const-string v1, "DEVICE"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    const-string v1, "POLICY"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->ww:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    const-string v1, "GET_DEVICE"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wx:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    const-string v1, "UPDATE_DEVICE"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->ww:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wx:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wz:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wz:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
