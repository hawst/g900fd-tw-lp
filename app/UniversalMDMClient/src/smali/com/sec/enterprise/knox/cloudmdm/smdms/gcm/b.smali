.class Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;
.super Landroid/os/Handler;
.source "GcmInterface.java"


# instance fields
.field final synthetic rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    .line 87
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 88
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 96
    iget v0, p1, Landroid/os/Message;->what:I

    .line 97
    packed-switch v0, :pswitch_data_0

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 99
    :pswitch_0
    const-string v0, "UMC:GcmService"

    const-string v1, "GCM_REGISTER_DEVICE"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/google/android/gms/a/a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "951634295017"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/a/a;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    move-result-object v1

    .line 104
    const/4 v2, 0x3

    .line 103
    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 108
    :catch_0
    move-exception v0

    .line 109
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;->F(Ljava/lang/String;)V

    .line 111
    :cond_1
    const-string v1, "UMC:GcmService"

    .line 112
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device registration message. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :pswitch_1
    const-string v0, "UMC:GcmService"

    const-string v1, "GCM_ON_SERVER_PUSH_REQUEST"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lorg/json/JSONObject;

    .line 120
    if-nez v0, :cond_2

    .line 121
    const-string v0, "UMC:GcmService"

    .line 122
    const-string v1, "Push message from Gateway server is null or empty"

    .line 121
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 126
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;->b(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 128
    :cond_3
    const-string v0, "UMC:GcmService"

    const-string v1, "Core callback is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    :pswitch_2
    const-string v0, "UMC:GcmService"

    const-string v1, "GCM_ON_DEVICE_REGISTERED"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 137
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 138
    :cond_4
    const-string v0, "UMC:GcmService"

    const-string v1, "GCM Id from GCM is null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;->F(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 143
    :cond_5
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 145
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;->F(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 147
    :cond_6
    const-string v0, "UMC:GcmService"

    const-string v1, "Core callback is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    :pswitch_3
    const-string v0, "UMC:GcmService"

    const-string v1, "GCM_UNREGISTER_DEVICE"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :try_start_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->unregister()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 157
    :catch_1
    move-exception v0

    .line 158
    const-string v1, "UMC:GcmService"

    .line 159
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device Unregistration message. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    :pswitch_4
    const-string v0, "UMC:GcmService"

    const-string v1, "GCM_ON_DEVICE_UNREGISTERED"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->rl:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
