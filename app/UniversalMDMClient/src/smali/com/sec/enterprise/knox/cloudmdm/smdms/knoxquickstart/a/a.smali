.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;
.super Ljava/lang/Object;
.source "ActiveSyncManager.java"


# instance fields
.field private dO:Ljava/lang/String;

.field private dT:Ljava/lang/String;

.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mUseSSL:Z

.field private pk:Ljava/lang/String;

.field private tC:Ljava/lang/String;

.field private tD:Ljava/lang/String;

.field private tE:Ljava/lang/String;

.field private tF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;

.field private tG:Ljava/lang/String;

.field private tH:Z

.field private tI:Ljava/lang/String;

.field private tJ:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dT:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tI:Ljava/lang/String;

    .line 44
    const-string v0, "gs5"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dO:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dT:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tI:Ljava/lang/String;

    .line 44
    const-string v0, "gs5"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dO:Ljava/lang/String;

    .line 184
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tE:Ljava/lang/String;

    .line 185
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mDomain:Ljava/lang/String;

    .line 186
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tG:Ljava/lang/String;

    .line 187
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mPassword:Ljava/lang/String;

    .line 188
    iput-object p7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dT:Ljava/lang/String;

    .line 189
    iput-object p8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tI:Ljava/lang/String;

    .line 190
    iput-boolean p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mUseSSL:Z

    .line 191
    iput-boolean p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tH:Z

    .line 192
    iput-object p9, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dO:Ljava/lang/String;

    .line 194
    iput-object p10, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->pk:Ljava/lang/String;

    .line 196
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tJ:Ljava/lang/String;

    .line 197
    return-void
.end method

.method private a(Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->hl()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    .line 243
    new-instance v1, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v1}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 244
    invoke-interface {v0, p1, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/http/client/methods/HttpPost;
    .locals 4

    .prologue
    .line 368
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 369
    const-string v1, "User-Agent"

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v1, "Accept"

    const-string v2, "*/*"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v1, "Content-Type"

    const-string v2, "application/vnd.ms-sync.wbxml"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const-string v1, "MS-ASProtocolVersion"

    const-string v2, "12.1"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const-string v1, "Accept-Language"

    const-string v2, "en-us"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const-string v1, "Authorization"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tC:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    if-eqz p3, :cond_0

    .line 392
    const-string v1, "X-MS-PolicyKey"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_0
    if-eqz p2, :cond_1

    .line 398
    new-instance v1, Ljava/io/ByteArrayInputStream;

    .line 399
    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 398
    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 401
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 403
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;

    invoke-virtual {v3, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 404
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 406
    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v2, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 407
    const-string v1, "application/vnd.ms-sync.wbxml"

    invoke-virtual {v2, v1}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 408
    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 410
    :cond_1
    return-object v0
.end method

.method private b(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 207
    const-string v0, ""

    .line 209
    if-eqz p1, :cond_0

    .line 210
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 213
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 216
    const-string v3, "application/vnd.ms-sync.wbxml"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 217
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 218
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;

    invoke-virtual {v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 219
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    :cond_0
    :goto_0
    return-object v0

    .line 223
    :cond_1
    const-string v1, "text/html"

    invoke-virtual {v2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 224
    invoke-static {p1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private hi()V
    .locals 3

    .prologue
    const/16 v2, 0x3a

    .line 130
    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mDomain:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mDomain:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mDomain:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Basic "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tC:Ljava/lang/String;

    .line 141
    return-void

    .line 137
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private hl()Lorg/apache/http/client/HttpClient;
    .locals 3

    .prologue
    const v1, 0x1d4c0

    .line 345
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 346
    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 347
    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 348
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 349
    const/4 v1, 0x0

    const/16 v2, 0x1bb

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    .line 350
    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v2, v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 351
    return-object v2
.end method

.method private j(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 424
    const-string v0, "&"

    const-string v1, "&"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 427
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 427
    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 429
    invoke-static {}, Lorg/xml/sax/helpers/XMLReaderFactory;->createXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v0

    .line 430
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/k;

    invoke-direct {v2, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/k;-><init>(Ljava/lang/String;)V

    .line 431
    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 432
    new-instance v3, Lorg/xml/sax/InputSource;

    invoke-direct {v3, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v3}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 433
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/k;->hu()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public an(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 437
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tJ:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-object v0

    .line 442
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tJ:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->j(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 447
    :goto_1
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    .line 452
    const/4 v0, 0x0

    aget-object v0, v1, v0

    goto :goto_0

    .line 444
    :catch_0
    move-exception v1

    .line 445
    const-string v2, "ActiveSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getPolicyByName "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_1
.end method

.method public hj()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 149
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/i;

    .line 151
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->hi()V

    .line 155
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tE:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 174
    :goto_0
    return v0

    .line 160
    :cond_0
    :try_start_0
    new-instance v0, Ljava/net/URI;

    iget-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->mUseSSL:Z

    if-eqz v1, :cond_1

    const-string v1, "https"

    .line 161
    :goto_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tE:Ljava/lang/String;

    .line 162
    const-string v3, "/Microsoft-Server-ActiveSync"

    .line 163
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "User="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 164
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&DeviceId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->dO:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 165
    const-string v5, "&DeviceType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->getDeviceType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 166
    const-string v5, "&Cmd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 163
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 166
    const/4 v5, 0x0

    .line 160
    invoke-direct/range {v0 .. v5}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tD:Ljava/lang/String;

    .line 174
    const/4 v0, 0x1

    goto :goto_0

    .line 160
    :cond_1
    const-string v1, "http"
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 170
    :catch_0
    move-exception v0

    move v0, v6

    .line 171
    goto :goto_0
.end method

.method public hk()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tD:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Provision"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    const-string v1, "MS-EAS-Provisioning-WBXML"

    .line 260
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<Provision xmlns=\"Provision:\">\n\t<Policies>\n\t\t<Policy>\n\t\t\t<PolicyType>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 262
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</PolicyType>\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\t\t</Policy>\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 263
    const-string v2, "\t</Policies>\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</Provision>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 265
    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->a(Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 266
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 267
    const-string v2, "TAG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Validation (Provision) response: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->b(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    .line 272
    const-string v0, "PolicyKey"

    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->j(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 274
    if-nez v0, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 281
    :goto_0
    return-object v0

    .line 280
    :cond_0
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->tJ:Ljava/lang/String;

    goto :goto_0
.end method
