.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;
.super Ljava/lang/Object;
.source "EasValidator.java"


# instance fields
.field private uf:Z

.field final synthetic ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;


# direct methods
.method private constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)V
    .locals 1

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->uf:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)V

    return-void
.end method

.method private a(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 274
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 276
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dL:Ljava/lang/String;

    .line 277
    if-eqz p1, :cond_1

    .line 278
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    .line 279
    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    .line 280
    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    .line 283
    :cond_0
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    .line 284
    :goto_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 285
    const-string v2, "eas"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    if-ge v2, v1, :cond_1

    .line 286
    if-eqz v0, :cond_3

    const/16 v0, 0x1bb

    :goto_1
    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 290
    :cond_1
    return-void

    .line 283
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 286
    :cond_3
    const/16 v0, 0x50

    goto :goto_1
.end method

.method private a(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 335
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 336
    const/16 v0, 0x3a

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 337
    invoke-virtual {p2, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 338
    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "httpts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iput-boolean v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 340
    if-eqz p1, :cond_1

    .line 341
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 356
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 357
    if-eqz p3, :cond_5

    .line 358
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 364
    :cond_2
    :goto_1
    return-void

    .line 343
    :cond_3
    const-string v1, "tls"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 344
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iput-boolean v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 345
    if-eqz p1, :cond_1

    .line 347
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 348
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_0

    .line 351
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iput-boolean v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 352
    if-eqz p1, :cond_1

    .line 353
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v0, v0, -0xc

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_0

    .line 360
    :cond_5
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_1
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;)Z
    .locals 4

    .prologue
    .line 295
    const/4 v0, 0x0

    .line 296
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v1

    .line 297
    const-string v2, "X-MS-Location"

    invoke-virtual {p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->ay(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 298
    const/16 v3, 0x1c3

    if-ne v1, v3, :cond_0

    .line 299
    if-eqz v2, :cond_0

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 300
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 301
    const/4 v0, 0x1

    .line 303
    :cond_0
    return v0
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;Lorg/apache/http/client/methods/HttpRequestBase;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 310
    .line 311
    const-string v2, "X-MS-Location"

    invoke-virtual {p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->ay(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 313
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 314
    const-string v3, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Http status 451 recieved, Server redirected request to new URI : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 315
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 314
    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 317
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    .line 318
    const-string v4, "\\?"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 319
    const-string v5, "\\?"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 320
    aget-object v4, v4, v0

    aget-object v0, v2, v0

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 321
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    const-string v2, "https"

    const-string v3, "httpts"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 324
    :cond_0
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {p2, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 325
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->uf:Z

    move v0, v1

    .line 329
    :cond_1
    return v0
.end method

.method private ar(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 265
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 266
    if-eqz v0, :cond_0

    .line 267
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Z

    move-result v1

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;Z)V

    .line 268
    invoke-direct {p0, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;)V

    .line 271
    :cond_0
    return-void
.end method

.method private b(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 4

    .prologue
    .line 378
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    invoke-static {p3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 377
    invoke-static {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->a(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    const-string v1, "EasValidator"

    .line 381
    const-string v2, "sendHttpClientOptions(): IllegalStateException from HTTP Client handled"

    .line 380
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 383
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 384
    :catch_1
    move-exception v0

    .line 385
    new-instance v1, Ljavax/net/ssl/SSLException;

    invoke-direct {v1, v0}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 386
    :catch_2
    move-exception v0

    .line 387
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executePostWithTimeout(): Unexpected exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private ht()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->uf:Z

    return v0
.end method


# virtual methods
.method public a(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 235
    .line 237
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->uf:Z

    move v1, v0

    .line 240
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->b(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v3

    .line 241
    if-eqz v3, :cond_0

    .line 242
    invoke-direct {p0, v3, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;Lorg/apache/http/client/methods/HttpRequestBase;)Z

    move-result v0

    .line 245
    :cond_0
    add-int/lit8 v2, v1, 0x1

    const/16 v4, 0xa

    if-ge v1, v4, :cond_1

    if-nez v0, :cond_5

    .line 247
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_2

    .line 248
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ht()Z

    .line 251
    :cond_2
    if-eqz v3, :cond_4

    .line 252
    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v0

    .line 253
    const/16 v1, 0x191

    if-eq v0, v1, :cond_3

    const/16 v1, 0x193

    if-ne v0, v1, :cond_4

    .line 254
    :cond_3
    const-string v0, "ExchangeService"

    const-string v1, "Authentication Failed So aborting HTTP Session."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpOptions;->abort()V

    .line 259
    :cond_4
    return-object v3

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method protected a(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    invoke-static {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Lorg/apache/http/client/methods/HttpPost;)V

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tk:Ljava/lang/String;

    .line 194
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 193
    invoke-static {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 192
    invoke-static {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->a(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    .line 197
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ug:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 196
    invoke-static {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->a(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 210
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executePostWithTimeout(): IOException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 213
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Timeout waiting for connection"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    const-string v1, "EasValidator"

    .line 215
    const-string v2, "executePostWithTimeout(): Timeout Waiting for Connection. Shutting down ConnectionManager"

    .line 214
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_1
    throw v0

    .line 220
    :catch_1
    move-exception v0

    .line 221
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executePostWithTimeout(): IllegalStateException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 224
    :catch_2
    move-exception v0

    .line 225
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executePostWithTimeout(): Unexpected exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 227
    :catch_3
    move-exception v0

    .line 228
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executePostWithTimeout(): AssertionError "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/AssertionError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 156
    .line 158
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->uf:Z

    move v1, v0

    .line 161
    :goto_0
    invoke-virtual/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v3

    .line 163
    if-eqz v3, :cond_0

    .line 164
    invoke-direct {p0, v3, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;Lorg/apache/http/client/methods/HttpRequestBase;)Z

    move-result v0

    .line 167
    :cond_0
    add-int/lit8 v2, v1, 0x1

    const/16 v4, 0xa

    if-ge v1, v4, :cond_1

    if-nez v0, :cond_4

    .line 170
    :cond_1
    if-eqz v3, :cond_3

    .line 172
    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v0

    .line 174
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c1

    if-ne v0, v1, :cond_3

    .line 175
    :cond_2
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ht()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 176
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->ar(Ljava/lang/String;)V

    .line 179
    :cond_3
    return-object v3

    :cond_4
    move v1, v2

    goto :goto_0
.end method
