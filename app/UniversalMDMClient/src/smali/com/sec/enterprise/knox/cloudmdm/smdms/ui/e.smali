.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;
.super Ljava/lang/Object;
.source "EulaPromptFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private yD:Ljava/lang/String;

.field private yE:Ljava/lang/String;

.field private yF:I

.field private yG:Landroid/widget/CheckBox;

.field private yH:Ljava/lang/String;

.field final synthetic yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    const-string v0, "UMC:MyCustomOnClick"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->TAG:Ljava/lang/String;

    .line 443
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->context:Landroid/content/Context;

    .line 444
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yD:Ljava/lang/String;

    .line 445
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yE:Ljava/lang/String;

    .line 446
    iput p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yF:I

    .line 447
    iput-object p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yH:Ljava/lang/String;

    .line 448
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yG:Landroid/widget/CheckBox;

    .line 453
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iH()Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    const-string v0, "UMC:MyCustomOnClick"

    const-string v1, "No internet found so I am going back"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iI()V

    .line 490
    :goto_0
    return-void

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 473
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->context:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 474
    const-string v0, "UMC:MyCustomOnClick"

    const-string v1, "Context is not set, im going to stop it here."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 477
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 478
    const-string v2, "bundle.activityTitle"

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yE:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v2, "bundle.euladata"

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yD:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v2, "bundle.activitySubtitle"

    iget v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yF:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 484
    const-string v2, "bundle.eulaLocale"

    .line 485
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getLocale()Ljava/lang/String;

    move-result-object v3

    .line 484
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const-string v2, "bundle.eulaContent"

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yH:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 488
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 489
    const-string v0, "UMC:MyCustomOnClick"

    const-string v1, "Launching a webview for the eula"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
