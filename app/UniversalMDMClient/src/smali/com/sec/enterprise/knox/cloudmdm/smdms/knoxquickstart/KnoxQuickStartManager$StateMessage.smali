.class final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;
.super Ljava/lang/Enum;
.source "KnoxQuickStartManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum sI:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sK:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sL:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sM:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sO:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sP:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sR:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sU:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sV:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field public static final enum sY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

.field private static final synthetic sZ:[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 519
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_QUICKSTART_ENROLLMENT_LAUNCH"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sI:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_EAS_INFO_ENTERED"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_CREATE_ACCOUNT_RESPONSE"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sK:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_ENROLLMENT_RESPONSE"

    invoke-direct {v0, v1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sL:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_ENROLLMENT_SUCCESS"

    invoke-direct {v0, v1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sM:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_ENROLLMENT_FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_MANAGEMENT_PUSH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sO:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_APPLY_POLICIES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sP:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_POLICIES_APPLIED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_POLICIES_COMPLETE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sR:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_USER_CANCELLED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_NETWORK_ERROR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_USER_RETRY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sU:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_EULA_RESULT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sV:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_ENROLLMENT_COMPLETE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_CREDS_ENTERED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const-string v1, "MSG_DEVICE_VERSION_CHECK"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    .line 518
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sI:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sK:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sL:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sM:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sO:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sP:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sR:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sU:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sV:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sZ:[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sZ:[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
