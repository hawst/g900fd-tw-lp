.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;
.super Ljava/lang/Object;
.source "PolicyApplier.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:PolicyApplier"


# instance fields
.field bCancelled:Z

.field private mBackupFlag:Z

.field private final mInSequenceExec:Z

.field private mInSequenceExecBroken:Z

.field mMethodInvokers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;",
            ">;"
        }
    .end annotation
.end field

.field private mPolicyApplierThreadContext:Landroid/os/Handler;

.field private mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

.field private mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

.field private mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExec:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExecBroken:Z

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mMethodInvokers:Ljava/util/List;

    .line 72
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->bCancelled:Z

    .line 78
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 79
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mBackupFlag:Z

    .line 80
    return-void
.end method

.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;ZLandroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExec:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExecBroken:Z

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mMethodInvokers:Ljava/util/List;

    .line 72
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->bCancelled:Z

    .line 83
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 84
    iput-boolean p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mBackupFlag:Z

    .line 85
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    .line 86
    return-void
.end method

.method private declared-synchronized isCancelled()Z
    .locals 1

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->bCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private lookupCommandResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 240
    if-nez p1, :cond_1

    .line 249
    :cond_0
    return-object v1

    .line 244
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 245
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    .line 246
    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->id:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 244
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private lookupPolicyResponse(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->documentId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private selfschedule()V
    .locals 2

    .prologue
    .line 216
    const-string v0, "UMC:PolicyApplier"

    const-string v1, "selfschedule"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getThreadContext()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public applyPolicies(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Z
    .locals 1

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 100
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    .line 101
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->selfschedule()V

    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public backupPolicyResponse()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->backupPolicyResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;)V

    .line 290
    return-void
.end method

.method public declared-synchronized cancel()V
    .locals 2

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    const-string v0, "UMC:PolicyApplier"

    const-string v1, "cancel"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->bCancelled:Z

    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mMethodInvokers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    monitor-exit p0

    return-void

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    return-object v0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getBackupFlag()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mBackupFlag:Z

    return v0
.end method

.method public getMDMAdminUId()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    return v0
.end method

.method public getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    return-object v0
.end method

.method public getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    return-object v0
.end method

.method public getPolicyResponse()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    return-object v0
.end method

.method public jobComplete(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 179
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mMethodInvokers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->backupPolicyResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;)V

    .line 186
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->commandListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$CommandListener;

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->commandListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$CommandListener;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$CommandListener;->onCommandResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v0

    if-nez v0, :cond_3

    .line 191
    const-string v0, "UMC:PolicyApplier"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "All commands are procesed. No more commands to process for request id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->jobComplete(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;)V

    .line 193
    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 194
    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    goto :goto_0

    .line 198
    :cond_3
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExecBroken:Z

    if-eqz v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->selfschedule()V

    goto :goto_0
.end method

.method public lookupObject(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 253
    if-nez p1, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-object v0

    .line 257
    :cond_1
    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;->documentId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->lookupPolicyResponse(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v1

    .line 258
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    if-eqz v2, :cond_0

    .line 262
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;->commandId:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->lookupCommandResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v1

    .line 263
    if-eqz v1, :cond_0

    .line 266
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;->referredParam:Ljava/lang/String;

    .line 267
    if-eqz v2, :cond_0

    .line 269
    const-string v0, "."

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 271
    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->response:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 128
    const-string v0, "UMC:PolicyApplier"

    const-string v1, "Inside run()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    const-string v0, "UMC:PolicyApplier"

    const-string v1, "isCancelled = true"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExecBroken:Z

    .line 139
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v0

    if-nez v0, :cond_2

    .line 140
    const-string v0, "UMC:PolicyApplier"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "All commands are procesed. No more commands to process for request id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->jobComplete(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;)V

    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v0

    .line 146
    :goto_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 147
    const-string v1, "UMC:PolicyApplier"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "------------------ Request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getCommandResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v2

    .line 150
    if-eqz v2, :cond_3

    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->commandProcessState:I

    const/16 v3, 0xc8

    if-eq v1, v3, :cond_3

    .line 151
    invoke-static {p0, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/MethodInvokerFactory;->createInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mMethodInvokers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v1, "UMC:PolicyApplier"

    const-string v2, "Pending Command Response found and processing now "

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->continuePendingCommand()V

    .line 164
    :goto_2
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_4

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mInSequenceExecBroken:Z

    goto/16 :goto_0

    .line 156
    :cond_3
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/MethodInvokerFactory;->createInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;

    move-result-object v1

    .line 157
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mMethodInvokers:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 159
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->getCommandResponse()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;->INIT_STATE:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;)V

    .line 161
    const-string v2, "UMC:PolicyApplier"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Running Command id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->run()V

    move-object v0, v1

    goto :goto_2

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public schedule(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 204
    const-string v0, "UMC:PolicyApplier"

    const-string v1, "schedule"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 209
    const-string v0, "UMC:PolicyApplier"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-----------Runnable can not be scheduled.. \nPolicy Thread is not running for admin "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getMDMUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->mPolicyApplierThreadContext:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
