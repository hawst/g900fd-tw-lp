.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;
.super Ljava/lang/Object;
.source "KnoxQuickStartManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

.field private static pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

.field private static pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

.field private static pc:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

.field private static pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

.field private static pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

.field private static ph:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field private static pi:Ljava/lang/String;

.field private static pj:Ljava/lang/String;

.field private static pm:Ljava/lang/String;

.field private static pn:Ljava/lang/String;

.field private static po:Ljava/lang/String;

.field private static pp:Ljava/lang/String;

.field private static sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

.field private static ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

.field private static su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

.field private static sv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;

.field private static sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

.field private static sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

.field private static sy:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

.field private static sz:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I
    .locals 3

    .prologue
    .line 96
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    .line 551
    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "KnoxQuickStartManager()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    .line 561
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    .line 562
    sput-object p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    .line 563
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 564
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->init()Z

    .line 565
    sput-object p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    .line 566
    sput-object p4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    .line 567
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;

    .line 568
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    .line 569
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    .line 570
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iput-object p5, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    .line 571
    return-void
.end method

.method private I(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 723
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 724
    if-eqz p1, :cond_0

    .line 726
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->u(Landroid/content/Context;Ljava/lang/String;)V

    .line 729
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->J(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 730
    const/4 v4, 0x1

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V

    .line 732
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 733
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 734
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 738
    :goto_0
    return-void

    .line 736
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f080079

    const/16 v2, 0x52

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->g(II)V

    goto :goto_0
.end method

.method private J(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 741
    .line 744
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getLicenses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, v4

    move-object v2, v4

    move-object v1, v4

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 760
    if-nez v1, :cond_1

    if-eqz v2, :cond_8

    .line 761
    :cond_1
    const-string v0, ""

    .line 762
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;

    invoke-direct {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;-><init>()V

    .line 763
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 764
    if-eqz p1, :cond_6

    .line 765
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->s(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 766
    iput-boolean v7, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;->px:Z

    move-object v3, p1

    .line 780
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const/4 v6, -0x1

    invoke-static {v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 781
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    move v0, v7

    .line 785
    :goto_2
    return v0

    .line 744
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;

    .line 745
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    const-string v9, "KLM"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 746
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 747
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v2, v4

    .line 750
    :cond_3
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 751
    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ELM"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 752
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 753
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v1, v4

    .line 756
    :cond_5
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 768
    :cond_6
    if-nez v3, :cond_7

    .line 769
    iget-object v0, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 770
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 772
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 776
    :cond_7
    const-string v4, "1.0"

    .line 777
    iput-boolean v6, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;->px:Z

    goto :goto_1

    :cond_8
    move v0, v6

    .line 785
    goto :goto_2
.end method

.method static synthetic K(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 542
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pi:Ljava/lang/String;

    return-void
.end method

.method static synthetic M(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 545
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pm:Ljava/lang/String;

    return-void
.end method

.method static synthetic N(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 546
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pn:Ljava/lang/String;

    return-void
.end method

.method static synthetic O(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 547
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->po:Ljava/lang/String;

    return-void
.end method

.method static synthetic P(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 543
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pj:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I
    .locals 1

    .prologue
    .line 932
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V
    .locals 0

    .prologue
    .line 526
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 722
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->I(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;)V
    .locals 0

    .prologue
    .line 550
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sy:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;)V
    .locals 0

    .prologue
    .line 544
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;)V
    .locals 0

    .prologue
    .line 541
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ph:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    return-void
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZII)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 677
    if-nez p1, :cond_0

    .line 678
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "finalizeEnrollment: profile is null "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    :goto_0
    return-void

    .line 682
    :cond_0
    if-eqz p4, :cond_1

    .line 683
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "finalizeEnrollment: launchAgent "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aU(I)Ljava/lang/String;

    move-result-object v6

    .line 685
    invoke-direct {p0, p3, v6, p5, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 691
    :goto_1
    if-nez p3, :cond_2

    .line 692
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 693
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 694
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 697
    :goto_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 698
    invoke-virtual {v1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->setId(Ljava/lang/String;)V

    .line 699
    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    .line 700
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getProfileIcon()[B

    move-result-object v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;[BLjava/lang/String;Ljava/lang/String;)V

    .line 699
    invoke-static {v8, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)I

    .line 701
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v0, v1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 687
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const v1, 0x7f08007d

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->b(Landroid/content/Context;I)V

    .line 688
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "finalizeEnrollment: enrollment comlpete "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v7

    goto :goto_1

    :cond_2
    move-object v5, p3

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4

    .prologue
    .line 860
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.enterprise.knox.intent.action.LAUNCH_APP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 861
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 862
    const-string v1, "com.sec.enterprise.knox.intent.extra.USER_CREDENTIALS"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pn:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 863
    const-string v1, "com.sec.enterprise.knox.intent.extra.APP_PAYLOAD"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pp:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 864
    const-string v1, "com.sec.enterprise.knox.intent.extra.ELM_ERROR_CODE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 865
    const-string v1, "com.sec.enterprise.knox.intent.extra.KLM_ERROR_CODE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 866
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 868
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 869
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_EMAIL_ADDRESS"

    .line 870
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    .line 869
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_EMAIL_PASSWORD"

    .line 872
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->password:Ljava/lang/String;

    .line 871
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_SERVER_URL"

    .line 874
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->server:Ljava/lang/String;

    .line 873
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_DOMAIN"

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sC:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USERNAME"

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->username:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->cert:[B

    if-eqz v2, :cond_0

    .line 878
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_CERTIFICATE"

    .line 879
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->cert:[B

    .line 878
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 880
    :cond_0
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sD:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 881
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_CERTIFICATE_PASSWORD"

    .line 882
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sD:Ljava/lang/String;

    .line 881
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    :cond_1
    const-string v2, "com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_PASSWORD_POLICIES"

    .line 884
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sy:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->gB()Ljava/lang/String;

    move-result-object v3

    .line 883
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    const-string v2, "com.sec.enterprise.knox.intent.extra.EAS_DATA"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 886
    const-string v1, "com.sec.enterprise.knox.intent.extra.QUICKSTART_URL"

    .line 887
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 886
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 888
    const-string v1, "com.sec.enterprise.knox.intent.extra.UPDATE_URL"

    .line 889
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 888
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 891
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 892
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->sendBroadcast(Landroid/content/Intent;)V

    .line 893
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, " Launch Intent : com.sec.enterprise.knox.intent.action.LAUNCH_APP"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.USER_CREDENTIALS): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 896
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 895
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.APP_PAYLOAD): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 898
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 897
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.ELM_ERROR_CODE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 901
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 900
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.KLM_ERROR_CODE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 903
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 902
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_EMAIL_ADDRESS): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 907
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 905
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_EMAIL_PASSWORD): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 910
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->password:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 908
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_SERVER_URL): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 913
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->server:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 911
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_DOMAIN): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 915
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 914
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USERNAME): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 917
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 916
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_CERTIFICATE_PASSWORD): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 920
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 918
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_CERTIFICATE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 922
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->cert:[B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 921
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.bundle.EAS_USER_PASSWORD_POLICIES): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 925
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sy:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->gB()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 923
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.QUICKSTART_URL): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 927
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 926
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.UPDATE_URL): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 929
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 928
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Z
    .locals 1

    .prologue
    .line 1155
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gP()Z

    move-result v0

    return v0
.end method

.method static synthetic al(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 551
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sz:Ljava/lang/String;

    return-void
.end method

.method private b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I
    .locals 3

    .prologue
    const v0, 0x7f0800a1

    .line 933
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->K(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 934
    const v0, 0x7f08009e

    .line 954
    :goto_0
    :pswitch_0
    return v0

    .line 936
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 951
    :pswitch_2
    const v0, 0x7f0800a0

    goto :goto_0

    .line 936
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V
    .locals 0

    .prologue
    .line 533
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pc:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Z
    .locals 1

    .prologue
    .line 1164
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gQ()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Z
    .locals 1

    .prologue
    .line 1133
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gO()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 831
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fs()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fq()V

    return-void
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V
    .locals 0

    .prologue
    .line 671
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fr()V

    return-void
.end method

.method static synthetic fJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->po:Ljava/lang/String;

    return-object v0
.end method

.method private fq()V
    .locals 3

    .prologue
    .line 664
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 665
    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 666
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v1}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/c;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 669
    return-void
.end method

.method private fr()V
    .locals 2

    .prologue
    .line 672
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/c;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 673
    return-void
.end method

.method private fs()Ljava/lang/String;
    .locals 3

    .prologue
    .line 832
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v1

    .line 833
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pn:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 834
    const-string v0, ""

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pn:Ljava/lang/String;

    .line 836
    :cond_0
    const/4 v0, 0x0

    .line 837
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    .line 838
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getPayload()Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 839
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getPayload()Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fasterxml/jackson/databind/JsonNode;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pp:Ljava/lang/String;

    .line 841
    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 842
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 843
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    .line 844
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 845
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v0

    .line 855
    :cond_2
    :goto_0
    return-object v0

    .line 848
    :cond_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 849
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 850
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 851
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    .line 852
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 853
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final gL()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;
    .locals 1

    .prologue
    .line 554
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    return-object v0
.end method

.method private gO()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1134
    .line 1136
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getLicenses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    move-object v3, v2

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1148
    if-eqz v3, :cond_1

    if-nez v1, :cond_4

    .line 1149
    :cond_1
    const/4 v0, 0x0

    .line 1151
    :goto_1
    return v0

    .line 1136
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;

    .line 1137
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v5

    const-string v6, "KLM"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1138
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 1139
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    move-object v1, v2

    .line 1141
    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ELM"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1142
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 1143
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v3, v2

    .line 1144
    goto :goto_0

    .line 1151
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    move-object v3, v0

    goto :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_0
.end method

.method private gP()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1156
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const-string v2, "persona"

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 1157
    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v0

    .line 1158
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    move v0, v1

    .line 1161
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private gQ()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1165
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const-string v2, "persona"

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 1166
    invoke-virtual {v0, v1}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v0

    .line 1167
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1174
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1167
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PersonaInfo;

    .line 1168
    const-string v3, "UMC:KnoxQuickStartManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Persona Type:  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/pm/PersonaInfo;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    invoke-virtual {v0}, Landroid/content/pm/PersonaInfo;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v3, "centrify-pb-"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v2, "Found PB container"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1171
    goto :goto_0
.end method

.method static synthetic gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;
    .locals 1

    .prologue
    .line 530
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    return-object v0
.end method

.method static synthetic gS()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    return-object v0
.end method

.method static synthetic gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;
    .locals 1

    .prologue
    .line 549
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    return-object v0
.end method

.method static synthetic gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;
    .locals 1

    .prologue
    .line 528
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    return-object v0
.end method

.method static synthetic gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;
    .locals 1

    .prologue
    .line 544
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    return-object v0
.end method

.method static synthetic gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;
    .locals 1

    .prologue
    .line 539
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    return-object v0
.end method

.method static synthetic gX()Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    return-object v0
.end method

.method static synthetic gY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 551
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sz:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic gZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 543
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ha()Ljava/lang/String;
    .locals 1

    .prologue
    .line 542
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pi:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic hb()Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;
    .locals 1

    .prologue
    .line 532
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    return-object v0
.end method

.method static synthetic hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;
    .locals 1

    .prologue
    .line 550
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sy:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    return-object v0
.end method


# virtual methods
.method public A(Z)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 1064
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fO()I

    move-result v0

    .line 1063
    if-nez v0, :cond_1

    .line 1065
    if-nez p1, :cond_0

    .line 1066
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    .line 1101
    :goto_0
    return-void

    .line 1069
    :cond_0
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 1070
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->stopSettingsApp(Landroid/content/Context;I)V

    .line 1071
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1072
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1073
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1074
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1078
    :goto_1
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 1079
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    goto :goto_0

    .line 1075
    :catch_0
    move-exception v0

    .line 1076
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1084
    :cond_1
    if-eqz p1, :cond_3

    .line 1085
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    .line 1086
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sz:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1087
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 1088
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v1, v2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->f(II)V

    .line 1089
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fq()V

    .line 1090
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-direct {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V

    .line 1091
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sz:Ljava/lang/String;

    const-string v3, "vendor.apk"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 1090
    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "sent intent to start MDM agent download and install "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1094
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f08009a

    .line 1095
    const v2, 0x7f0800a0

    const v3, 0x7f080058

    const/4 v4, 0x0

    .line 1094
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 1096
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Invaild auth cred dialog shown"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1100
    :cond_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    goto/16 :goto_0
.end method

.method public F(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 637
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeviceRegistered: GCM Id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pi:Ljava/lang/String;

    .line 639
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 640
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pi:Ljava/lang/String;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->po:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    return-void
.end method

.method public H(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 644
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onRegisterSamsungPush: Push Id:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pj:Ljava/lang/String;

    .line 646
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 647
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pj:Ljava/lang/String;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sx:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->po:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    return-void
.end method

.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 612
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-nez v0, :cond_1

    .line 620
    :cond_0
    :goto_0
    return-object v1

    .line 615
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p1, v0}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 616
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    goto :goto_0

    .line 617
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wx:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p1, v0}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/16 v0, 0xc9

    .line 706
    if-eq p1, v0, :cond_0

    .line 707
    if-ne p2, v0, :cond_1

    .line 708
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 709
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f080099

    .line 710
    const v2, 0x7f08009f

    const v3, 0x7f080058

    const/4 v4, 0x1

    .line 709
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 711
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v7, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 720
    :goto_0
    return-void

    .line 715
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f080079

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->g(II)V

    .line 716
    check-cast p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;

    .line 717
    iget-object v1, p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    iget-boolean v4, p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/f;->px:Z

    move-object v0, p0

    move-object v2, p4

    move-object v3, p5

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V

    .line 718
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v7, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 719
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 575
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onNetworkOperationSuccess: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 594
    :goto_0
    return-void

    .line 579
    :sswitch_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sK:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 582
    :sswitch_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sL:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 585
    :sswitch_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 586
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fO()I

    move-result v0

    .line 587
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    .line 588
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 587
    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 577
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_1
        0xf -> :sswitch_0
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 3

    .prologue
    .line 599
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onNetworkOperationFailure: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 603
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 604
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 605
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 606
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V
    .locals 3

    .prologue
    .line 961
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;)V

    .line 962
    iput-object p1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->server:Ljava/lang/String;

    .line 963
    iput-object p2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    .line 964
    iput-object p3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->username:Ljava/lang/String;

    .line 965
    iput-object p5, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sC:Ljava/lang/String;

    .line 966
    iput-object p4, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->password:Ljava/lang/String;

    .line 967
    iput-object p6, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->cert:[B

    .line 968
    iput-object p7, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sD:Ljava/lang/String;

    .line 969
    const-string v1, "UMC:KnoxQuickStartManager"

    const-string v2, "onValidEmail"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const v2, 0x7f080078

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 971
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 973
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;

    invoke-direct {v1, p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;)V

    .line 1040
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->start()V

    .line 1042
    return-void
.end method

.method public c(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 655
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    .line 656
    return-void
.end method

.method public d(II)V
    .locals 3

    .prologue
    .line 1050
    mul-int/lit8 v0, p1, 0x64

    div-int/2addr v0, p2

    .line 1051
    div-int/lit8 v0, v0, 0x3

    .line 1052
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v2, 0x7f0800b9

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->g(II)V

    .line 1053
    return-void
.end method

.method public fm()V
    .locals 3

    .prologue
    .line 659
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "onUserCancelEnrollment"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    .line 661
    return-void
.end method

.method public fx()V
    .locals 2

    .prologue
    .line 1178
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "onErrorDialogUiClosed"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 1180
    return-void
.end method

.method public gM()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 624
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startQuickStartEnrollment: state  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 626
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->stopBrowserApps(Landroid/content/Context;I)V

    .line 627
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 629
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    if-eq v0, v1, :cond_0

    .line 630
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sI:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    .line 634
    :goto_0
    return-void

    .line 632
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->gw()V

    goto :goto_0
.end method

.method public gN()V
    .locals 3

    .prologue
    .line 1056
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f0800b5

    const/16 v2, 0x43

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->g(II)V

    .line 1057
    return-void
.end method

.method public s(Z)V
    .locals 3

    .prologue
    .line 651
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sr:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sV:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    .line 652
    return-void
.end method

.method public u(Z)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 1104
    if-eqz p1, :cond_1

    .line 1105
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    .line 1106
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sz:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1107
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 1108
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v1, v2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->f(II)V

    .line 1109
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fq()V

    .line 1110
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->su:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ss:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-direct {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V

    .line 1111
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->sz:Ljava/lang/String;

    const-string v3, "vendor.apk"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 1110
    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "sent intent to start MDM agent download and install "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    :goto_0
    return-void

    .line 1114
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f08009a

    .line 1115
    const v2, 0x7f0800a0

    const v3, 0x7f080058

    const/4 v4, 0x0

    .line 1114
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 1116
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Invaild auth cred dialog shown"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1120
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    goto :goto_0
.end method
