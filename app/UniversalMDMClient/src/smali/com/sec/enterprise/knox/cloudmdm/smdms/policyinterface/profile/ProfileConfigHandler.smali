.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;
.super Ljava/lang/Object;
.source "ProfileConfigHandler.java"


# static fields
.field private static final PROFILE_CONFIG_DIR:Ljava/lang/String; = "ProfileConfig"

.field private static final TAG:Ljava/lang/String; = "ProfileConfig"


# instance fields
.field mProfileConfigMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->mProfileConfigMap:Ljava/util/Map;

    .line 47
    return-void
.end method


# virtual methods
.method public get(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;
    .locals 1

    .prologue
    .line 155
    if-nez p2, :cond_1

    .line 156
    const/4 p2, 0x0

    .line 161
    :cond_0
    :goto_0
    return-object p2

    .line 158
    :cond_1
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->isBasicType(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->load(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object p2

    goto :goto_0
.end method

.method public load(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;
    .locals 5

    .prologue
    .line 165
    .line 166
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->mProfileConfigMap:Ljava/util/Map;

    monitor-enter v1

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->mProfileConfigMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 166
    monitor-exit v1

    .line 170
    if-eqz v0, :cond_0

    .line 191
    :goto_0
    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 173
    :cond_0
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 176
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 178
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ProfileConfig/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".config"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 179
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->readFile(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 185
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->fromJson(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->mProfileConfigMap:Ljava/util/Map;

    monitor-enter v1

    .line 188
    :try_start_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->mProfileConfigMap:Ljava/util/Map;

    invoke-interface {v2, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 180
    :catch_0
    move-exception v0

    .line 181
    const-string v1, "ProfileConfig"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v0, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " Cause = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v0, 0x0

    goto :goto_0

    .line 181
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
