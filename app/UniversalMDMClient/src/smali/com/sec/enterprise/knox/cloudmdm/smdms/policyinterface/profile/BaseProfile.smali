.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;
.super Ljava/lang/Object;
.source "BaseProfile.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:BaseProfile"


# instance fields
.field protected transient config:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

.field protected transient data:Ljava/lang/Object;

.field protected id:Ljava/lang/String;

.field protected transient name:Ljava/lang/String;

.field protected transient parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

.field protected transient policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

.field protected transient policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

.field protected transient profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

.field protected transient state:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;

.field protected userId:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->name:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    .line 76
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->initId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    .line 78
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->addProfileToCache(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;)V

    .line 79
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->config:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 80
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->data:Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getActiveProfileState()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;->getStateInfo(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->state:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;

    .line 82
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->initUserId()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    .line 83
    return-void
.end method

.method private findParentHolder(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 232
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 233
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private findValInMap(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 425
    invoke-virtual {p3, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-object v3

    .line 428
    :cond_1
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v3, p2

    .line 429
    goto :goto_0

    .line 431
    :cond_2
    const-string v0, "."

    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->dotSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 437
    const/4 v0, 0x1

    move v1, v0

    move-object v2, v3

    move-object v0, p2

    :goto_1
    array-length v5, v4

    if-lt v1, v5, :cond_3

    move-object v3, v2

    .line 451
    goto :goto_0

    .line 438
    :cond_3
    aget-object v2, v4, v1

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 440
    if-eqz v2, :cond_5

    instance-of v5, v2, Ljava/util/Map;

    if-eqz v5, :cond_5

    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 441
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 437
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 445
    :cond_5
    if-eqz v2, :cond_4

    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 446
    const-string v0, "UMC:BaseProfile"

    const-string v1, "Node Not found"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private formStateVal(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 415
    const-string v0, "response"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    const-string v0, "response"

    iget-object v1, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->response:Ljava/util/Map;

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->findValInMap(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 419
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initUserId()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    .line 171
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->state:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->state:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;

    const-string v1, "containerId"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;->getState(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->state:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;

    const-string v1, "containerId"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState$StateInfo;->getState(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-class v1, Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->translateObject(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    .line 176
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    return-object v0

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->getUserId()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static removeParentNodeInId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 371
    if-nez p0, :cond_0

    .line 377
    :goto_0
    return-object p1

    .line 374
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gt v0, v1, :cond_2

    .line 375
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 377
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected addCommandRequest(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    const-string v1, "highprio"

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 148
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->findUserId(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    .line 149
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 150
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    iput-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->addCommandRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)V

    .line 153
    return-void
.end method

.method public apply(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 97
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->getNodeFromDomTbl(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    move-result-object v1

    .line 98
    if-eqz v1, :cond_1

    .line 99
    const-string v2, "UMC:BaseProfile"

    const-string v3, "A Profile is already outstanding to be applied"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v2

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->reqId:Ljava/lang/String;

    invoke-virtual {v2, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->data:Ljava/lang/Object;

    if-nez v1, :cond_2

    if-eqz p1, :cond_0

    .line 107
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->applyInternal(Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getPolicyRequestId()Ljava/lang/String;

    move-result-object v1

    .line 109
    if-nez v1, :cond_3

    .line 110
    const-string v1, "UMC:BaseProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Nothing to apply for element id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_3
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v2

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-direct {v3, v4, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->addNodeToDomTbl(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;)V

    .line 116
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v1, v2, v3, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    move-result v1

    .line 117
    if-nez v1, :cond_0

    .line 118
    const-string v0, "UMC:BaseProfile"

    const-string v1, "profileHandler.policyHandler.applyPolicy failed"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected applyInternal(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method protected checkAndDoReverseIfNeeded(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 455
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->reverseInput:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->reverseInput:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    check-cast p2, Ljava/lang/Boolean;

    .line 457
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    .line 459
    :cond_0
    return-object p2

    .line 457
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected clearState(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getActiveProfileState()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;->clearState(Ljava/lang/String;)V

    .line 408
    return-void
.end method

.method protected findUserId(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->findProfile(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    .line 158
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected formObjectwithProperties(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 481
    if-nez p1, :cond_1

    .line 482
    const/4 p2, 0x0

    .line 495
    :cond_0
    return-object p2

    .line 484
    :cond_1
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    const-string v1, "DomProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 488
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 489
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 490
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    invoke-virtual {v3, v4, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->get(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v1

    .line 491
    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 492
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected getChildName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 364
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    .line 365
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 367
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getCurrentEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getCurrentlyProcessingChildName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 246
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->dotSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 248
    const-string v1, ".entry"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    const-string v1, "UMC:BaseProfile"

    const-string v2, "@getCurrentlyProcessingChildName "

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 254
    :goto_0
    return-object v0

    :cond_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x2

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method protected getMethodInfo(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getParamDetail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 206
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0, v2, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->load(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 207
    invoke-virtual {p1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 228
    :goto_0
    return-object v0

    .line 211
    :cond_0
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->isBasicType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 212
    const-string v0, "UMC:BaseProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Referred member not found (parent is a non class type) id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 213
    goto :goto_0

    .line 217
    :cond_1
    invoke-static {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->dotSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    .line 218
    invoke-static {p4, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->removeParentNodeInId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 219
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 220
    if-nez v0, :cond_2

    .line 221
    const-string v0, "UMC:BaseProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Referred member not found id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 222
    goto :goto_0

    .line 225
    :cond_2
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 226
    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->translateDataObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 228
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->platformName:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-direct {v1, v3, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected getParamsFromConfig(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->params:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 267
    const/4 v0, 0x0

    .line 359
    :goto_0
    return-object v0

    .line 269
    :cond_0
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 270
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->params:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 356
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 357
    const/4 v0, 0x0

    goto :goto_0

    .line 270
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 271
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;

    .line 272
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    iget-object v2, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->name:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getParamType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 275
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getCurrentlyProcessingChildName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 277
    if-eqz v2, :cond_1

    .line 280
    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->refer:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->refer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 281
    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->refer:Ljava/lang/String;

    .line 290
    const-string v6, "state."

    .line 291
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    if-le v7, v8, :cond_3

    .line 292
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 293
    invoke-virtual {p0, p1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getState(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 294
    if-eqz v6, :cond_3

    .line 295
    invoke-interface {v4, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 300
    :cond_3
    const-string v6, "parent.state."

    .line 301
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    if-le v7, v8, :cond_4

    .line 302
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 303
    const/4 v7, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 304
    invoke-virtual {p0, v7, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getState(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 305
    if-eqz v6, :cond_4

    .line 306
    invoke-interface {v4, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 311
    :cond_4
    const-string v6, "current.entry."

    .line 312
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    if-le v7, v8, :cond_5

    .line 313
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 314
    invoke-virtual {p0, v3, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getCurrentEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 315
    if-eqz v3, :cond_5

    .line 316
    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 321
    :cond_5
    const-string v3, "parent.entry."

    .line 322
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    if-le v6, v7, :cond_6

    .line 323
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 324
    invoke-virtual {p0, p1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getParentEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 325
    if-eqz v3, :cond_6

    .line 326
    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 332
    :cond_6
    invoke-interface {p5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 333
    if-nez v3, :cond_9

    .line 334
    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->refer:Ljava/lang/String;

    invoke-direct {p0, p5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->findParentHolder(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 336
    :goto_2
    invoke-interface {p5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 337
    invoke-interface {p4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 339
    if-eqz v6, :cond_7

    .line 341
    iget-object v7, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->refer:Ljava/lang/String;

    invoke-virtual {p0, v7, v2, v6, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getParamDetail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;

    move-result-object v2

    .line 342
    if-eqz v2, :cond_7

    .line 343
    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;->value:Ljava/lang/Object;

    invoke-virtual {p0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->checkAndDoReverseIfNeeded(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;->value:Ljava/lang/Object;

    .line 344
    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;->value:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 352
    :cond_7
    const-string v2, "UMC:BaseProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Param Not found - paramaname "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;->defaultVal:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_8
    move-object v0, v4

    .line 359
    goto/16 :goto_0

    :cond_9
    move-object v3, v2

    goto :goto_2
.end method

.method protected getParentEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPolicyRequestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->getCommandRequests()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->getCommandRequests()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    :cond_0
    const/4 v0, 0x0

    .line 183
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getState(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getActiveProfileState()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;->getState(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getUserId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->userId:Ljava/lang/Integer;

    return-object v0
.end method

.method protected hasAnyCommandsPending()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    :cond_0
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected init()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method protected initId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->name:Ljava/lang/String;

    .line 93
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->initId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected isEqualObject(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 499
    if-nez p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    if-nez p3, :cond_2

    if-eqz p2, :cond_2

    .line 543
    :cond_1
    :goto_0
    return v1

    .line 504
    :cond_2
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->isPrimitiveType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 505
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-static {v0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->isPrimitiveEqual(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 508
    :cond_3
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 509
    check-cast p3, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 512
    :cond_4
    instance-of v0, p3, Ljava/util/List;

    if-eqz v0, :cond_7

    .line 513
    check-cast p3, Ljava/util/List;

    .line 514
    check-cast p2, Ljava/util/List;

    move v0, v1

    .line 515
    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_5

    move v0, v1

    .line 520
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_6

    move v1, v2

    .line 525
    goto :goto_0

    .line 516
    :cond_5
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 515
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 521
    :cond_6
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 520
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 528
    :cond_7
    instance-of v0, p3, Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 529
    check-cast p3, Ljava/util/Map;

    .line 530
    check-cast p2, Ljava/util/Map;

    .line 531
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 532
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 534
    :cond_8
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 537
    :cond_9
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->isEqualObject(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v1, v2

    .line 540
    goto/16 :goto_0
.end method

.method public onAlert(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->addReport(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V

    .line 132
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->removeNodeFromDomTbl(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->onPoliciesAppliedUpdate()V

    .line 134
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->removePolicyReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V

    .line 135
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 136
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    .line 137
    return-void
.end method

.method protected translateDataObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 463
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->isBasicType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    :cond_0
    return-object p2

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->load(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 468
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    .line 469
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 470
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 471
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 472
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 473
    iget-object v4, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-virtual {p0, v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->translateDataObject(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 474
    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->platformName:Ljava/lang/String;

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected updateState(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 7

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getActiveProfileState()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    move-result-object v2

    .line 386
    invoke-virtual {p0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->getMethodInfo(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    move-result-object v0

    .line 387
    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->state:Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->state:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 404
    :cond_0
    return-void

    .line 390
    :cond_1
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->state:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 391
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 392
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 394
    const-string v4, "state."

    .line 395
    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    if-gt v5, v6, :cond_2

    .line 396
    const-string v0, "UMC:BaseProfile"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "State Name not understandable : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 400
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 401
    invoke-direct {p0, v0, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->formStateVal(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)Ljava/lang/Object;

    move-result-object v0

    .line 402
    invoke-virtual {v2, p1, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;->setState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
