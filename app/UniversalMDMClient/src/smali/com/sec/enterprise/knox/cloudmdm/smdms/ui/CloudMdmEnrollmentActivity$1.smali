.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;
.super Landroid/view/OrientationEventListener;
.source "CloudMdmEnrollmentActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation


# instance fields
.field final synthetic xU:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;->xU:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    .line 142
    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;->xU:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 147
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;->xU:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 148
    const/16 v1, 0x20

    .line 147
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;->xU:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;->xU:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 158
    const/16 v1, 0x10

    .line 157
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method
