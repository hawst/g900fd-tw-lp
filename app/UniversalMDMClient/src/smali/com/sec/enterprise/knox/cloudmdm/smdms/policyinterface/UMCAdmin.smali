.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;
.super Landroid/app/admin/DeviceAdminReceiver;
.source "UMCAdmin.java"


# static fields
.field private static final CHROME_PACKAGE_NAME:Ljava/lang/String; = "com.android.chrome"

.field private static final DOWNLOADER_PACKAGE_NAME:Ljava/lang/String; = "com.android.providers.downloads"

.field private static final MIN_PERSONA_ID:I = 0x64

.field private static final SBROWSER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field private static final SETTINGS_PACKAGE_NAME:Ljava/lang/String; = "com.android.settings"

.field private static final TAG:Ljava/lang/String; = "UMC:UMCAdmin"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/app/admin/DeviceAdminReceiver;-><init>()V

    return-void
.end method

.method private static activateAdminSliently(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 185
    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 187
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 186
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 188
    invoke-virtual {v0, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 189
    invoke-virtual {v0, v1, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->setActiveAdmin(Landroid/content/ComponentName;Z)V

    .line 190
    invoke-virtual {v0, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->setAdminRemovable(Z)Z

    .line 192
    :cond_0
    return-object v1
.end method

.method public static activateUMCAdmin(Landroid/content/Context;Ljava/lang/String;[BLjava/lang/String;[Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 2

    .prologue
    .line 126
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateAdminSliently(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    .line 127
    const/4 v1, -0x1

    invoke-static {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableClearAppData(Landroid/content/Context;I)V

    .line 131
    return-object v0
.end method

.method public static activateUMCAdmin(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 135
    const-string v0, "UMC:UMCAdmin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "activateUMCAdmin : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const/4 v0, -0x1

    .line 137
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->isContainer(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    :goto_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->isContainer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateAdminSliently(Landroid/content/Context;)Landroid/content/ComponentName;

    .line 143
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "activateAdminSliently - UMC App Admin activated"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableClearAppData(Landroid/content/Context;I)V

    .line 147
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableAppUninstall(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_1
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_1
    move p1, v0

    goto :goto_0
.end method

.method public static deactivateAdminSliently(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 395
    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 397
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 396
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 398
    invoke-virtual {v0, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 399
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->setAdminRemovable(Z)Z

    .line 400
    invoke-virtual {v0, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->removeActiveAdmin(Landroid/content/ComponentName;)V

    .line 402
    :cond_0
    return-void
.end method

.method public static deactivateUMCAdmin(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 154
    const-string v0, "UMC:UMCAdmin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deactivateUMCAdmin : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const/4 v0, -0x1

    .line 156
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->isContainer(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 160
    :goto_0
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->enableClearAppData(Landroid/content/Context;I)V

    .line 161
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->enableAppUninstall(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_1
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->isContainer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateAdminSliently(Landroid/content/Context;)V

    .line 167
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "deactivateUMCAdmin - UMC App Admin deactivated"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    const-string v1, "UMC:UMCAdmin"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move p1, v0

    goto :goto_0
.end method

.method public static deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 172
    const-string v0, "UMC:UMCAdmin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deactivateUMCAdminIfNotRequired : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdminCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 175
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    .line 174
    invoke-static {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->w(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 175
    if-nez v0, :cond_0

    .line 177
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdmin(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v0

    .line 179
    const-string v1, "UMC:UMCAdmin"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static disableAppStop(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 268
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableAppStop enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 271
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 272
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 271
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 277
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 278
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 279
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToForceStopBlackList(Ljava/util/List;)Z

    .line 281
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableAppStop exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    return-void

    .line 275
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 274
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static disableAppUninstall(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 300
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableAppUninstall enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 303
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 304
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 303
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 309
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 310
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationUninstallationDisabled(Ljava/lang/String;)V

    .line 311
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableAppUninstall exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    return-void

    .line 307
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 306
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method private static disableClearAppData(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 196
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableClearAppData enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 199
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 200
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 199
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 205
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 206
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 207
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToClearDataBlackList(Ljava/util/List;)Z

    .line 209
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableClearAppData exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    return-void

    .line 203
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 202
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static disableDownloadNotifications(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 328
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableDownloadNotifications enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 331
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 332
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 331
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 337
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 338
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 339
    const-string v2, "com.android.providers.downloads"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationNotificationMode(I)Z

    .line 341
    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToNotificationBlackList(Ljava/util/List;)Z

    .line 342
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableDownloadNotifications exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    return-void

    .line 335
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 334
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static disableFingerPrint(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 228
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableFingerPrint enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 231
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v1, p1, p2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 236
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getPasswordPolicy()Landroid/app/enterprise/PasswordPolicy;

    move-result-object v0

    .line 239
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238
    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/app/enterprise/PasswordPolicy;->setBiometricAuthenticationEnabled(IZ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :goto_1
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "disableFingerPrint exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return-void

    .line 234
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 233
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    .line 241
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static enableAppStop(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 286
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 287
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 288
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 287
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 293
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 294
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 295
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromForceStopBlackList(Ljava/util/List;)Z

    .line 297
    return-void

    .line 291
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 290
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static enableAppUninstall(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 316
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 317
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 318
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 317
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 323
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 324
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationUninstallationEnabled(Ljava/lang/String;)V

    .line 325
    return-void

    .line 321
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 320
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method private static enableClearAppData(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 214
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 215
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 216
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 215
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 221
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 222
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 223
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromClearDataBlackList(Ljava/util/List;)Z

    .line 225
    return-void

    .line 219
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 218
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static enableDownloadNotifications(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 346
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "enableDownloadNotifications enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 349
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 350
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 349
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 355
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 357
    const-string v2, "com.android.providers.downloads"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationNotificationMode(I)Z

    .line 359
    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromNotificationBlackList(Ljava/util/List;)Z

    .line 360
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "enableDownloadNotifications exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    return-void

    .line 353
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 352
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static isContainer(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 73
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v0

    .line 74
    const-string v1, "UMC:UMCAdmin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isContainer : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 76
    const/4 v0, 0x1

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFingerPrintEnabled(Landroid/content/Context;II)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 247
    const-string v0, "UMC:UMCAdmin"

    const-string v2, "isFingerPrintDisabled enter "

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 251
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v2, p1, p2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 256
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getPasswordPolicy()Landroid/app/enterprise/PasswordPolicy;

    move-result-object v0

    .line 259
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/app/enterprise/PasswordPolicy;->isBiometricAuthenticationEnabled(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 263
    :goto_1
    const-string v1, "UMC:UMCAdmin"

    const-string v2, "isFingerPrintDisabled exit "

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    return v0

    .line 254
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 253
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_1
.end method

.method public static setCCMProfile(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 82
    const-string v0, "UMC:UMCAdmin"

    const-string v2, "setCCMProfile"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->isContainer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-static {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    move v0, v1

    .line 112
    :goto_0
    if-eq v0, v1, :cond_4

    .line 113
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 114
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    .line 113
    invoke-direct {v1, v0, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-static {v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->createInstance(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v0

    .line 119
    :goto_1
    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getClientCertificateManagerPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    move-result-object v0

    .line 120
    new-instance v1, Lcom/sec/enterprise/knox/ccm/CCMProfile;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/ccm/CCMProfile;-><init>()V

    .line 121
    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->setCCMProfile(Lcom/sec/enterprise/knox/ccm/CCMProfile;)Z

    .line 122
    :goto_2
    return-void

    .line 87
    :cond_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->o(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 88
    if-nez v0, :cond_1

    .line 89
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "profiles is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 93
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 94
    if-nez v0, :cond_2

    .line 95
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "profiles iterator is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 99
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 100
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fU()Ljava/lang/String;

    move-result-object v0

    .line 101
    const-string v2, "UMC:UMCAdmin"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Admin of Container : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    if-nez v0, :cond_3

    .line 103
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 106
    :cond_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    .line 105
    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/d;->k(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 107
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 108
    const-string v2, "UMC:UMCAdmin"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "proxyAdminId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_4
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v0

    goto :goto_1
.end method

.method public static stopBrowserApps(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 364
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "stopApp enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 367
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 368
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 367
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 373
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 374
    const-string v1, "com.android.chrome"

    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->stopApp(Ljava/lang/String;)Z

    .line 375
    const-string v1, "com.sec.android.app.sbrowser"

    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->stopApp(Ljava/lang/String;)Z

    .line 376
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "stopApp exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    return-void

    .line 371
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 370
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method

.method public static stopSettingsApp(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 380
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "stoptSettingsApp enter "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 383
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    .line 384
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v2, 0x0

    .line 383
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 389
    :goto_0
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 390
    const-string v1, "com.android.settings"

    invoke-virtual {v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->stopApp(Ljava/lang/String;)Z

    .line 391
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "stoptSettingsApp exit "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    return-void

    .line 387
    :cond_0
    const-string v0, "enterprise_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 386
    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    goto :goto_0
.end method


# virtual methods
.method public onDisableRequested(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDisabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 421
    const-string v0, "UMC Device Admin: disabled"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 422
    return-void
.end method

.method public onEnabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 410
    const-string v0, "UMC:UMCAdmin"

    const-string v1, "onEnabled"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string v0, "UMC Device Admin: enabled"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 412
    return-void
.end method

.method public onPasswordChanged(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 426
    const-string v0, "UMC Device Admin: pw changed"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 427
    return-void
.end method

.method public onPasswordFailed(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 431
    const-string v0, "UMC Device Admin: pw failed"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 432
    return-void
.end method

.method public onPasswordSucceeded(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 436
    const-string v0, "UMC Device Admin: pw succeeded"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 437
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 441
    invoke-super {p0, p1, p2}, Landroid/app/admin/DeviceAdminReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 442
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onReceive():"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 443
    return-void
.end method

.method showToast(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 406
    return-void
.end method
