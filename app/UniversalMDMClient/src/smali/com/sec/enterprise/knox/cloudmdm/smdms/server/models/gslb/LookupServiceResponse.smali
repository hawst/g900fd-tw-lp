.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;
.super Ljava/lang/Object;
.source "LookupServiceResponse.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private endpoint:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;",
            ">;"
        }
    .end annotation
.end field

.field private service:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public getEndpoint()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->endpoint:Ljava/util/List;

    return-object v0
.end method

.method public getService()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->service:Ljava/lang/String;

    return-object v0
.end method

.method public setEndpoint(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->endpoint:Ljava/util/List;

    .line 65
    return-void
.end method

.method public setService(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->service:Ljava/lang/String;

    .line 52
    return-void
.end method
