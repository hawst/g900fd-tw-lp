.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;
.super Ljava/lang/Object;
.source "Account.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private emailAddress:Ljava/lang/String;

.field private exchangeServerUrl:Ljava/lang/String;

.field private program:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method


# virtual methods
.method public getEmailAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->emailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getExchangeServerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->exchangeServerUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProgram()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->program:Ljava/lang/String;

    return-object v0
.end method

.method public setEmailAddress(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->emailAddress:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setExchangeServerUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->exchangeServerUrl:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setProgram(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/Account;->program:Ljava/lang/String;

    .line 54
    return-void
.end method
