.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;
.super Ljava/lang/Object;
.source "PolicyHandler.java"


# static fields
.field static final ALERT_RECEIVED:I = 0x3

.field static final POLICY_APPLIED:I = 0x1

.field static final PROCESS_NEXT_PENDING_POLICY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "UMC:PolicyHandler"


# instance fields
.field private flushFlag:Z

.field private mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field private mAlertManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

.field private mContext:Landroid/content/Context;

.field private mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

.field private final mMDMUrl:Ljava/lang/String;

.field mPolicyAppliers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;",
            ">;"
        }
    .end annotation
.end field

.field private mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

.field private mPolicyHandlerAsync:Landroid/os/Handler;

.field mPolicyListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    .locals 2

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyListeners:Ljava/util/Map;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->flushFlag:Z

    .line 88
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    .line 147
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 148
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mContext:Landroid/content/Context;

    .line 149
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    .line 150
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    .line 151
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAlertManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    .line 152
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mProfileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    .line 153
    return-void
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->flushFlag:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->resetFlushFlag()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Z
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyNextPendingPolicy()Z

    move-result v0

    return v0
.end method

.method private applyNextPendingPolicy()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 288
    const-string v2, "UMC:PolicyHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "applyNextPendingPolicy for admin = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->hasAnyPendingPolicies()Z

    move-result v2

    if-nez v2, :cond_0

    .line 291
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->checkAndFlushPendingIntentsForAnAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V

    .line 292
    const-string v1, "UMC:PolicyHandler"

    const-string v2, "No Pending Policies to be applied for all admins"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :goto_0
    return v0

    .line 296
    :cond_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->restorePolicyRequest(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v2

    .line 297
    if-nez v2, :cond_1

    .line 298
    const-string v0, "UMC:PolicyHandler"

    const-string v2, "applyNextPendingPolicy - No Pending Policy to be applied"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 299
    goto :goto_0

    .line 302
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->checkForPendingPolicyAppliers(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)Z

    move-result v3

    .line 303
    if-eqz v3, :cond_2

    .line 304
    const-string v1, "UMC:PolicyHandler"

    const-string v2, "applyNextPendingPolicy() : There is a pending policy applier for the same policyRequest"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->restorePolicyResponse(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v3

    .line 309
    if-eqz v3, :cond_3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v4

    if-nez v4, :cond_3

    .line 311
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;

    invoke-direct {v4, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)V

    invoke-static {v1, v0, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 312
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 317
    :cond_3
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 318
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V

    .line 319
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-virtual {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->applyPolicies(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 323
    goto :goto_0
.end method

.method private checkForPendingPolicyAppliers(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 327
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 335
    :goto_1
    return v2

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    .line 329
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 331
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    const/4 v2, 0x1

    goto :goto_1

    .line 327
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 378
    .line 382
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v1, v5

    .line 399
    :cond_1
    :goto_0
    return-object v1

    :cond_2
    move v4, v3

    .line 385
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v4, v0, :cond_3

    move-object v1, v5

    .line 399
    goto :goto_0

    .line 386
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move v2, v3

    .line 388
    :goto_2
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_4

    move v0, v3

    .line 396
    :goto_3
    if-eqz v0, :cond_1

    .line 385
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 389
    :cond_4
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    .line 390
    iget-object v6, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    iget-object v7, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 391
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->commandProcessState:I

    const/16 v6, 0xc8

    if-ne v0, v6, :cond_5

    .line 392
    const/4 v0, 0x1

    .line 393
    goto :goto_3

    .line 388
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method

.method static getCommandResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 403
    move v1, v2

    move-object v0, v3

    .line 405
    :goto_0
    iget-object v4, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 412
    :goto_1
    if-eqz v2, :cond_0

    move-object v3, v0

    .line 415
    :cond_0
    return-object v3

    .line 406
    :cond_1
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    .line 407
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    iget-object v5, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 408
    const/4 v2, 0x1

    .line 409
    goto :goto_1

    .line 405
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static isAllCommandsProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Z
    .locals 1

    .prologue
    .line 374
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->findNextCommandNotProcessed(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetFlushFlag()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->flushFlag:Z

    .line 177
    return-void
.end method


# virtual methods
.method public applyPolicy(Ljava/lang/String;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 180
    invoke-virtual {p0, v0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    move-result v0

    return v0
.end method

.method public applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;ZLcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    move-result v0

    return v0
.end method

.method public applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;ZZLcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 202
    const-string v0, "UMC:PolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "applyPolicy for admin = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 207
    :cond_0
    const-string v0, "UMC:PolicyHandler"

    const-string v1, "PolicyHandler: Error - PolicyRequest Parse Failure"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const/4 v3, 0x0

    .line 236
    :goto_0
    return v3

    .line 211
    :cond_1
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->setTimeStamp()V

    .line 212
    iput-object p1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->user:Ljava/lang/String;

    .line 213
    if-eqz p4, :cond_4

    .line 214
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->getPriority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->isPendingPolicy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->backupPolicyRequest(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)V

    .line 216
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-virtual {p0, v0, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 217
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Waiting on Some Previous Policies to be applied"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->backupPolicyRequest(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)V

    .line 222
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->restorePolicyResponse(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v0

    .line 229
    :goto_1
    if-eqz p5, :cond_3

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 230
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getThreadContext()Landroid/os/Handler;

    move-result-object v4

    .line 232
    :cond_3
    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-virtual {p0, v1, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 233
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    invoke-direct {v1, p0, p4, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;ZLandroid/os/Handler;)V

    .line 234
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    invoke-virtual {v1, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->applyPolicies(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)Z

    goto :goto_0

    .line 224
    :cond_4
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    iget-object v2, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->policyVersion:Ljava/lang/String;

    .line 225
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 224
    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;)V

    goto :goto_1
.end method

.method public applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;ZLcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z
    .locals 7

    .prologue
    .line 197
    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;ZZLcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    move-result v0

    return v0
.end method

.method public applyPolicy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    move-result v0

    return v0
.end method

.method public applyPolicy(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z
    .locals 1

    .prologue
    .line 188
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->fromJson(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v0

    .line 189
    invoke-virtual {p0, p1, v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    move-result v0

    return v0
.end method

.method backupPolicyResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;)V
    .locals 3

    .prologue
    .line 350
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getBackupFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyResponse()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->backupPolicyResponse(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)V

    .line 352
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 3

    .prologue
    .line 271
    const-string v0, "UMC:PolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Policy Handler cleared for admin with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->deleteBackup(Ljava/lang/String;)V

    .line 273
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 277
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAlertManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stop()V

    .line 278
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mProfileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->cancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :goto_1
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->cancel()V

    .line 273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 279
    :catch_0
    move-exception v0

    .line 280
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public clearPendingAlerts()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->clearPendingAlerts(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    return-object v0
.end method

.method public getAlertManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAlertManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    return-object v0
.end method

.method public getCompletedPolicies()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->getCompletedPolicies(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMDMUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPendingAlerts()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->getAllPendingAlerts(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPendingPolicies()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->getPendingPolicyRequests(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPendingPolicyRequestFiles(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->getPendingPolicyRequestFiles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPolicyBackup()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    return-object v0
.end method

.method public getProfileHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mProfileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    return-object v0
.end method

.method public hasAnyPendingAlertRequests()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAlertManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->hasAnyPendingAlertRequests()Z

    move-result v0

    return v0
.end method

.method public hasAnyPendingPolicies()Z
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->hasAnyPendingPolicy(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method jobComplete(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;)V
    .locals 5

    .prologue
    .line 355
    const-string v0, "UMC:PolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jobComplete for admin = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getBackupFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->moveToCompletedPolicies(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyResponse()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 359
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 360
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 361
    return-void
.end method

.method public removeAlertReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->removeAlertReport(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V

    .line 262
    return-void
.end method

.method public removePolicyReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 3

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->removePolicyExecContext(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;)V

    .line 253
    return-void
.end method

.method public schedule(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getThreadContext()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 428
    return-void
.end method

.method public sendAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    .locals 3

    .prologue
    .line 364
    const-string v0, "UMC:PolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendAlert for admin = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->mdmUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->backupAlertReport(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 367
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 368
    return-void
.end method

.method public setFlushFlag(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->flushFlag:Z

    .line 173
    return-void
.end method

.method public setListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    .line 77
    return-void
.end method

.method public setListener(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyListeners:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method start()V
    .locals 3

    .prologue
    .line 342
    const-string v0, "UMC:PolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Policy Handler started for admin with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyNextPendingPolicy()Z

    .line 344
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mAlertManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->start()V

    .line 345
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mProfileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->start()V

    .line 346
    return-void
.end method
