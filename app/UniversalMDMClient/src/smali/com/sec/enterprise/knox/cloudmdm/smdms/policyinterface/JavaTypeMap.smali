.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;
.super Ljava/lang/Object;
.source "JavaTypeMap.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:JavaTypeMap"

.field static gGTypeTbl:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;


# instance fields
.field public gBasicTypeTbl:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    .line 45
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->initBasicTypeTbl()V

    .line 46
    return-void
.end method

.method public static get()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gGTypeTbl:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gGTypeTbl:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    .line 53
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gGTypeTbl:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    return-object v0
.end method

.method private getJavaTypeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;-><init>(Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->toJavaTypes()V

    .line 169
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTypeInfo(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getBasicJavaClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 180
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    invoke-direct {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;-><init>(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->toJavaTypes()V

    .line 182
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->toClassString()Ljava/lang/String;

    move-result-object v1

    .line 183
    if-nez v1, :cond_0

    .line 192
    :goto_0
    return-object v0

    .line 187
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v1

    .line 190
    const-string v2, "UMC:JavaTypeMap"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getBasicJavaType(Ljava/lang/String;)Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->getBasicJavaClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method initBasicTypeTbl()V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "String"

    const-string v2, "java.lang.String"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Boolean"

    const-string v2, "java.lang.Boolean"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Integer"

    const-string v2, "java.lang.Integer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Character"

    const-string v2, "java.lang.Character"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Byte"

    const-string v2, "java.lang.Byte"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Short"

    const-string v2, "java.lang.Short"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Double"

    const-string v2, "java.lang.Double"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Long"

    const-string v2, "java.lang.Long"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Float"

    const-string v2, "java.lang.Float"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "JsonObject"

    const-string v2, "com.google.gson.JsonObject"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "List"

    const-string v2, "java.util.List"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "ArrayList"

    const-string v2, "java.util.ArrayList"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Map"

    const-string v2, "java.util.Map"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "HashMap"

    const-string v2, "java.util.HashMap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    const-string v1, "Set"

    const-string v2, "java.util.Set"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method isBasicType(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPrimitive(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 203
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    const-class v0, Ljava/lang/Character;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    const-class v0, Ljava/lang/Byte;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    const-class v0, Ljava/lang/Short;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    const-class v0, Ljava/lang/Double;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    const-class v0, Ljava/lang/Float;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isPrimitive(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->getBasicJavaClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 215
    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 217
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->isPrimitive(Ljava/lang/Class;)Z

    move-result v0

    goto :goto_0
.end method
