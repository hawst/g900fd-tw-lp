.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "KnoxEnterpriseLicenseManagerMethodInvoker.java"


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 54
    return-void
.end method


# virtual methods
.method protected createContextInfo(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Landroid/app/enterprise/ContextInfo;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method protected preInvoke()V
    .locals 4

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deActivateLicense"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    const-string v0, "deActivateLicenseForUMC"

    .line 66
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 67
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgNames:Ljava/util/List;

    const-string v2, "pkgName"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    const-class v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgObjs:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 74
    :goto_1
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 79
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mPolicyClass:Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/reflect/Type;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    invoke-static {v2, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    .line 80
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    if-nez v0, :cond_5

    .line 81
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Method "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "activateLicense"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const-string v0, "activateLicenseForUMC"

    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 75
    :cond_3
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_4

    .line 76
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - null argument during preInvoke (KELM MI)"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 84
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgObjs:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->translateArgObjsToRequiredTypes(Ljava/lang/reflect/Method;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mArgObjs:Ljava/util/List;

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;->mPreparedForInvoke:Z

    .line 87
    return-void
.end method
