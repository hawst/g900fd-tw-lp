.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "SSOMethodInvoker.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:SSOMethodInvoker"


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mPolicyClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V

    return-void
.end method


# virtual methods
.method protected invoke()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mPolicyClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v1, "android.app.enterprise.SSOPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v1, "solutionPackageName"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mPolicyObject:Ljava/lang/Object;

    check-cast v1, Landroid/app/enterprise/SSOPolicy;

    .line 64
    invoke-virtual {v1, v0}, Landroid/app/enterprise/SSOPolicy;->isSSOReadyInUserSpace(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 65
    invoke-virtual {v1, v0}, Landroid/app/enterprise/SSOPolicy;->setupSSOInUserSpace(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 66
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "UserSpaceSSO setup failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;)V

    move v0, v2

    .line 91
    :goto_0
    return v0

    .line 74
    :cond_1
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->invoke()Z

    move-result v0

    goto :goto_0

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 79
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mPolicyObject:Ljava/lang/Object;

    check-cast v1, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .line 81
    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->isSSOReady(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 82
    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->setupSSO(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 83
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "EnterpriseSSO setup failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_3
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;)V

    move v0, v2

    .line 87
    goto :goto_0

    .line 91
    :cond_4
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->invoke()Z

    move-result v0

    goto :goto_0
.end method
