.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;
.super Landroid/os/Handler;
.source "QuickStartReceiver.java"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    .line 92
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x7d0

    const/4 v3, 0x1

    .line 95
    const-string v0, "QuickStartReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Msg Id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 98
    :pswitch_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->w(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    const-string v0, "QuickStartReceiver"

    .line 100
    const-string v1, "My KNOX Manager Admin is active, deactivating My KNOX Manager Admin"

    .line 99
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->v(Landroid/content/Context;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->r(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 105
    const-string v0, "QuickStartReceiver"

    const-string v1, "My KNOX Manager Installed, Unistall My KNOX Manager"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->f(Landroid/content/Context;Ljava/lang/String;)Z

    .line 107
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 109
    :cond_2
    const-string v0, "QuickStartReceiver"

    const-string v1, "Clean Up Complete"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    goto :goto_0

    .line 117
    :pswitch_1
    const/16 v0, 0x64

    .line 120
    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    const-string v2, "QuickStartReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "model : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "QuickStartReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "id : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->hg()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->jM()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    .line 128
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    .line 127
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->r(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 129
    if-eqz v1, :cond_0

    .line 130
    const-string v2, "QuickStartReceiver"

    const-string v3, "Apply Container FP Policy using My KNOX Manager App"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;->mContext:Landroid/content/Context;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableFingerPrint(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
