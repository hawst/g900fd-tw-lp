.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;
.super Ljava/lang/Object;
.source "EasValidator.java"


# static fields
.field static dP:Ljava/lang/String;

.field static mUserAgent:Ljava/lang/String;

.field public static final tS:I

.field public static tT:Lorg/apache/http/conn/params/ConnPerRoute;

.field public static tV:D

.field static tY:Ljava/lang/String;

.field static tZ:Ljava/lang/String;

.field static ua:Ljava/lang/String;


# instance fields
.field protected dJ:Z

.field private dK:Z

.field public dL:Ljava/lang/String;

.field public dM:Ljava/lang/String;

.field protected dO:Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field public mDomain:Ljava/lang/String;

.field public mPassword:Ljava/lang/String;

.field public mPath:Ljava/lang/String;

.field protected mPort:I

.field mProtocolVersion:Ljava/lang/String;

.field tC:Ljava/lang/String;

.field public tU:Ljava/lang/Double;

.field protected tW:Ljava/lang/String;

.field protected tX:Ljava/lang/String;

.field public tk:Ljava/lang/String;

.field ub:Ljava/lang/String;

.field private uc:Landroid/os/ConditionVariable;

.field private ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

.field private volatile ue:Lorg/apache/http/client/methods/HttpPost;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 122
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 123
    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    .line 122
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 124
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 125
    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    .line 124
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 122
    :goto_0
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tS:I

    .line 476
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tT:Lorg/apache/http/conn/params/ConnPerRoute;

    .line 865
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    sput-wide v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tV:D

    .line 872
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tY:Ljava/lang/String;

    .line 881
    return-void

    .line 125
    :cond_0
    const/16 v0, 0x3c

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLandroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 863
    const-string v0, "2.5"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    .line 864
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    .line 867
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    .line 869
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    .line 870
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    .line 885
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    .line 887
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ub:Ljava/lang/String;

    .line 891
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    .line 905
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 908
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dK:Z

    .line 911
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    .line 919
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    .line 944
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dL:Ljava/lang/String;

    .line 945
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dM:Ljava/lang/String;

    .line 946
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    .line 947
    iput p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    .line 948
    iput-boolean p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 949
    iput-boolean p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dK:Z

    .line 950
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tk:Ljava/lang/String;

    .line 951
    iput-object p7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 952
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->uc:Landroid/os/ConditionVariable;

    .line 953
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    .line 954
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    .line 955
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 863
    const-string v0, "2.5"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    .line 864
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    .line 867
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    .line 869
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    .line 870
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    .line 885
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    .line 887
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ub:Ljava/lang/String;

    .line 891
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    .line 905
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 908
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dK:Z

    .line 911
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    .line 919
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    .line 927
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dL:Ljava/lang/String;

    .line 928
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dM:Ljava/lang/String;

    .line 929
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mDomain:Ljava/lang/String;

    .line 930
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    .line 931
    iput p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    .line 932
    iput-boolean p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    .line 933
    iput-boolean p7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dK:Z

    .line 934
    iput-object p8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    .line 935
    iput-object p9, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tk:Ljava/lang/String;

    .line 936
    iput-object p10, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 937
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->uc:Landroid/os/ConditionVariable;

    .line 938
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    .line 939
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    .line 940
    return-void
.end method

.method private static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Lcom/android/exchange/adapter/d;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x388

    const/16 v7, 0x387

    const/16 v6, 0x386

    .line 1462
    .line 1464
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    .line 1466
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x402c333333333333L    # 14.1

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_3

    .line 1467
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Lcom/android/exchange/adapter/f;

    move-result-object v0

    .line 1468
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1542
    :cond_0
    :goto_0
    return-object v0

    .line 1471
    :cond_1
    invoke-virtual {v0, v6}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 1472
    invoke-virtual {v0, v7}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/android/exchange/adapter/f;->a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v2

    .line 1473
    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->done()V

    .line 1481
    :goto_1
    const-string v2, "Provision"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Ljava/lang/String;[B)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v2

    .line 1485
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 1486
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v0

    .line 1488
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->cj:Z

    if-eqz v4, :cond_2

    .line 1489
    const-string v4, "EasValidator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "canProvision(): Provision command response code:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    :cond_2
    const/16 v4, 0xc8

    if-ne v0, v4, :cond_7

    .line 1494
    new-instance v0, Lcom/android/exchange/adapter/d;

    invoke-direct {v0, v3, p0}, Lcom/android/exchange/adapter/d;-><init>(Ljava/io/InputStream;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)V

    .line 1495
    invoke-virtual {v0}, Lcom/android/exchange/adapter/d;->z()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1507
    invoke-virtual {v0}, Lcom/android/exchange/adapter/d;->I()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    .line 1537
    if-eqz v2, :cond_0

    .line 1538
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    goto :goto_0

    .line 1475
    :cond_3
    new-instance v0, Lcom/android/exchange/adapter/f;

    invoke-direct {v0}, Lcom/android/exchange/adapter/f;-><init>()V

    .line 1476
    const/16 v2, 0x385

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 1477
    invoke-virtual {v0, v7}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/android/exchange/adapter/f;->a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v2

    .line 1478
    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/f;->done()V

    goto :goto_1

    .line 1521
    :cond_4
    :try_start_1
    invoke-virtual {v0}, Lcom/android/exchange/adapter/d;->I()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1524
    const-string v1, "EasValidator"

    const-string v3, "PolicySet is NOT fully supportable"

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1525
    invoke-virtual {v0}, Lcom/android/exchange/adapter/d;->H()Ljava/lang/String;

    move-result-object v1

    .line 1526
    const-string v3, "2"

    .line 1525
    invoke-static {p0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1529
    if-eqz v1, :cond_5

    .line 1530
    invoke-virtual {v0}, Lcom/android/exchange/adapter/d;->J()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537
    :cond_5
    if-eqz v2, :cond_0

    .line 1538
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    goto/16 :goto_0

    .line 1536
    :catchall_0
    move-exception v0

    .line 1537
    if-eqz v2, :cond_6

    .line 1538
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1539
    :cond_6
    throw v0

    .line 1537
    :cond_7
    if-eqz v2, :cond_8

    .line 1538
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    :cond_8
    move-object v0, v1

    .line 1542
    goto/16 :goto_0
.end method

.method private static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1575
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v3, 0x38b

    .line 1581
    new-instance v0, Lcom/android/exchange/adapter/f;

    invoke-direct {v0}, Lcom/android/exchange/adapter/f;-><init>()V

    .line 1583
    const/16 v1, 0x385

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v1

    const/16 v2, 0x386

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 1585
    const/16 v1, 0x387

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 1589
    const/16 v1, 0x388

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/f;->a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;

    .line 1591
    const/16 v1, 0x389

    invoke-virtual {v0, v1, p1}, Lcom/android/exchange/adapter/f;->a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;

    .line 1593
    invoke-virtual {v0, v3, p2}, Lcom/android/exchange/adapter/f;->a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;

    .line 1595
    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    .line 1597
    if-eqz p3, :cond_0

    .line 1599
    const/16 v1, 0x38c

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    .line 1601
    const-string v1, "1"

    invoke-virtual {v0, v3, v1}, Lcom/android/exchange/adapter/f;->a(ILjava/lang/String;)Lcom/android/exchange/adapter/f;

    .line 1603
    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    .line 1607
    :cond_0
    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/f;->done()V

    .line 1609
    const-string v1, "Provision"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/f;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Ljava/lang/String;[B)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v1

    .line 1613
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 1614
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v2

    .line 1617
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->cj:Z

    if-eqz v3, :cond_1

    .line 1618
    const-string v3, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "acknowledgeProvisionImpl():Provision command response code:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    :cond_1
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_4

    .line 1623
    new-instance v2, Lcom/android/exchange/adapter/d;

    invoke-direct {v2, v0, p0}, Lcom/android/exchange/adapter/d;-><init>(Ljava/io/InputStream;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)V

    .line 1624
    invoke-virtual {v2}, Lcom/android/exchange/adapter/d;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1626
    invoke-virtual {v2}, Lcom/android/exchange/adapter/d;->H()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1630
    if-eqz v1, :cond_2

    .line 1631
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1635
    :cond_2
    :goto_0
    return-object v0

    .line 1629
    :catchall_0
    move-exception v0

    .line 1630
    if-eqz v1, :cond_3

    .line 1631
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1632
    :cond_3
    throw v0

    .line 1630
    :cond_4
    if-eqz v1, :cond_5

    .line 1631
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1635
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/client/HttpClient;
    .locals 4

    .prologue
    .line 428
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 430
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 432
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 434
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 438
    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 442
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 443
    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    invoke-static {v2, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    .line 442
    invoke-direct {v1, v2, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 445
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ho()Lorg/apache/http/impl/client/DefaultRedirectHandler;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 451
    return-object v1
.end method

.method public static declared-synchronized a(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 10

    .prologue
    const/16 v2, 0x50

    const/16 v1, 0x1bb

    .line 484
    const-class v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    monitor-enter v3

    :try_start_0
    const-string v0, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getClientConnectionManagerForSSLValidation ssl = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", port = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const/4 v0, 0x0

    .line 486
    if-nez v0, :cond_1

    .line 490
    new-instance v4, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 491
    new-instance v5, Lcom/android/emailcommon/utility/a;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/emailcommon/utility/b;->g(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/android/emailcommon/utility/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 493
    new-instance v6, Lcom/android/emailcommon/utility/a;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/emailcommon/utility/b;->g(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/android/emailcommon/utility/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 494
    sget-object v0, Lcom/android/emailcommon/utility/a;->dp:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v6, v0}, Lcom/android/emailcommon/utility/a;->a(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 496
    if-lez p1, :cond_0

    const v0, 0xffff

    if-le p1, v0, :cond_3

    .line 497
    :cond_0
    const-string v0, "EasValidator"

    const-string v1, "getClientConnectionManagerForSSLValidation 1"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v1, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v2

    const/16 v7, 0x50

    invoke-direct {v0, v1, v2, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v4, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 500
    if-eqz p0, :cond_2

    .line 501
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v1, "https"

    const/16 v2, 0x1bb

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v4, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 521
    :goto_0
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 522
    const-string v0, "http.conn-manager.max-total"

    const/16 v2, 0x19

    invoke-interface {v1, v0, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 523
    const-string v0, "http.conn-manager.max-per-route"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tT:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v1, v0, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 524
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v0, v1, v4}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    :cond_1
    monitor-exit v3

    return-object v0

    .line 503
    :cond_2
    :try_start_1
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v1, "https"

    const/16 v2, 0x1bb

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v4, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 507
    :cond_3
    :try_start_2
    const-string v0, "EasValidator"

    const-string v7, "getClientConnectionManagerForSSLValidation 2"

    invoke-static {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v9

    .line 509
    if-eqz p0, :cond_4

    move v0, v2

    :goto_1
    invoke-direct {v7, v8, v9, v0}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    .line 508
    invoke-virtual {v4, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 511
    if-eqz p0, :cond_6

    .line 512
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    if-eqz p0, :cond_5

    :goto_2
    invoke-direct {v0, v2, v5, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v4, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    goto :goto_0

    :cond_4
    move v0, p1

    .line 509
    goto :goto_1

    :cond_5
    move p1, v1

    .line 512
    goto :goto_2

    .line 514
    :cond_6
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    if-eqz p0, :cond_7

    :goto_3
    invoke-direct {v0, v2, v6, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v4, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_7
    move p1, v1

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1418
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 1419
    const-string v1, "CscFeature_Email_EasDoNotUseProxy"

    .line 1418
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 1419
    if-nez v0, :cond_0

    .line 1420
    invoke-interface {p1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 1422
    new-instance v1, Lorg/apache/http/HttpHost;

    invoke-direct {v1, p3, p4}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 1424
    invoke-virtual {p2, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 1426
    const-string v0, "PROXY4"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added proxy param: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Lorg/apache/http/client/methods/HttpPost;)V
    .locals 0

    .prologue
    .line 919
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    return-void
.end method

.method private aq(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 958
    const-string v1, "https:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 968
    :cond_0
    :goto_0
    return v0

    .line 960
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->az(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 962
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->hH()Ljava/security/cert/X509Certificate;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 963
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->hH()Ljava/security/cert/X509Certificate;

    move-result-object v2

    .line 964
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->uc:Landroid/os/ConditionVariable;

    .line 963
    invoke-static {v0, p1, v2, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/security/cert/X509Certificate;ILandroid/os/ConditionVariable;)V

    .line 965
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->uc:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 966
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hC()Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Lcom/android/exchange/adapter/f;
    .locals 4

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    .line 1550
    if-nez v0, :cond_0

    .line 1551
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1554
    :cond_0
    new-instance v1, Lcom/android/exchange/b;

    sget-wide v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tV:D

    invoke-direct {v1, v2, v3}, Lcom/android/exchange/b;-><init>(D)V

    .line 1557
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hr()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/exchange/b;->b(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1563
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/android/exchange/b;->h(Z)Lcom/android/exchange/adapter/f;

    move-result-object v0

    return-object v0

    .line 1558
    :catch_0
    move-exception v0

    .line 1559
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 532
    const-class v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    monitor-enter v3

    :try_start_0
    const-string v0, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "getClientConnectionManagerForSSLValidation port = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    :try_start_1
    const-string v0, "pkcs12"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 538
    :try_start_2
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 539
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 540
    invoke-virtual {v2, v4, v0}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 542
    invoke-virtual {v2}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v4

    .line 543
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 553
    :goto_1
    if-nez v2, :cond_1

    .line 554
    :try_start_3
    const-string v0, "EasValidator"

    const-string v2, "failed to create keyStore"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 607
    :goto_2
    monitor-exit v3

    return-object v1

    .line 545
    :cond_0
    :try_start_4
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 546
    const-string v5, "EasValidator"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "alias: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 549
    :catch_0
    move-exception v0

    .line 550
    :goto_3
    :try_start_5
    const-string v4, "EasValidator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to create keyStore:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 560
    :cond_1
    :try_start_6
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 561
    invoke-static {v0}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v0

    .line 563
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    .line 566
    const/4 v2, 0x1

    new-array v2, v2, [Ljavax/net/ssl/TrustManager;

    const/4 v4, 0x0

    .line 567
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c$3;

    invoke-direct {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c$3;-><init>()V

    aput-object v5, v2, v4

    .line 580
    const-string v4, "SSL"

    invoke-static {v4}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v4

    .line 581
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 582
    invoke-virtual {v0}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v2, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 590
    :goto_4
    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 591
    new-instance v0, Lcom/android/emailcommon/utility/a;

    invoke-virtual {v4}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/emailcommon/utility/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 593
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    invoke-direct {v4, v5, v0, p0}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 596
    new-instance v4, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v4}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 597
    const-string v0, "http.conn-manager.max-total"

    const/16 v5, 0x19

    invoke-interface {v4, v0, v5}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 598
    const-string v0, "http.conn-manager.max-per-route"

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tT:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v4, v0, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 599
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v0, v4, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    :goto_5
    move-object v1, v0

    .line 607
    goto/16 :goto_2

    .line 584
    :cond_2
    invoke-virtual {v0}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v2, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 601
    :catch_1
    move-exception v0

    .line 602
    :try_start_7
    const-string v2, "EasValidator"

    .line 603
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getClientConnectionManagerForSSLValidation(): Unexpected exception "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 604
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 603
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 602
    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-object v0, v1

    goto :goto_5

    .line 549
    :catch_2
    move-exception v0

    move-object v2, v1

    goto/16 :goto_3
.end method

.method private static c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1567
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    .line 1569
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    const-string v0, "MS-EAS-Provisioning-WBXML"

    :goto_0
    return-object v0

    .line 1570
    :cond_0
    const-string v0, "MS-WAP-Provisioning-XML"

    goto :goto_0
.end method

.method private c(IZ)Lorg/apache/http/client/HttpClient;
    .locals 4

    .prologue
    .line 399
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 401
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 403
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 405
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 409
    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 413
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 414
    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPort:I

    invoke-static {p2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    .line 413
    invoke-direct {v1, v2, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 415
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ho()Lorg/apache/http/impl/client/DefaultRedirectHandler;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 421
    return-object v1
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Z
    .locals 1

    .prologue
    .line 908
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dK:Z

    return v0
.end method

.method public static getDeviceType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 746
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 757
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tY:Ljava/lang/String;

    .line 758
    const-string v1, "[^\uac00-\ud7a3xfe0-9a-zA-Z\\s]"

    const-string v2, ""

    .line 757
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 758
    const-string v1, "\\p{Space}"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 757
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tZ:Ljava/lang/String;

    .line 759
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tY:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 761
    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 762
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SAMSUNG"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    .line 768
    :goto_0
    const-string v0, "EasValidator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "create and return deviceType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    .line 774
    :goto_1
    return-object v0

    .line 765
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tZ:Ljava/lang/String;

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    goto :goto_0

    .line 772
    :cond_1
    const-string v0, "EasValidator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDeviceType() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dP:Ljava/lang/String;

    goto :goto_1
.end method

.method public static ho()Lorg/apache/http/impl/client/DefaultRedirectHandler;
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c$2;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c$2;-><init>()V

    return-object v0
.end method

.method private hq()V
    .locals 4

    .prologue
    const/16 v3, 0x3a

    .line 718
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dM:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 724
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mDomain:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mDomain:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 726
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mDomain:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\\"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dM:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 736
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Basic "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 738
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 736
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    .line 740
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&User="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&DeviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&DeviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 741
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->getDeviceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 740
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ub:Ljava/lang/String;

    .line 743
    return-void

    .line 730
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dM:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static hr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 805
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mUserAgent:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 807
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tY:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 809
    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 810
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SAMSUNG-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ua:Ljava/lang/String;

    .line 814
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ua:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "101"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 815
    sget-object v1, Lcom/android/emailcommon/EasRefs;->cp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 814
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mUserAgent:Ljava/lang/String;

    .line 817
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mUserAgent:Ljava/lang/String;

    .line 821
    :goto_1
    return-object v0

    .line 812
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tY:Ljava/lang/String;

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ua:Ljava/lang/String;

    goto :goto_0

    .line 819
    :cond_1
    const-string v0, "EasValidator"

    const-string v1, "getUserAgent()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mUserAgent:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method protected a(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 1

    .prologue
    .line 1216
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Ljava/lang/String;Lorg/apache/http/HttpEntity;IZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;Lorg/apache/http/HttpEntity;IZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 14

    .prologue
    .line 1224
    const-string v2, "EasValidator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendHttpClientPost: isHBISyncCommand = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 1227
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tk:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move/from16 v0, p3

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    move-object v9, v2

    .line 1234
    :goto_0
    const/4 v2, 0x1

    .line 1238
    const/4 v5, 0x0

    .line 1244
    const-string v3, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "mProtocolVersion: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " - cmd: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    const-string v7, "0"

    .line 1254
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    const-wide v12, 0x402c333333333333L    # 14.1

    cmpg-double v3, v10, v12

    if-lez v3, :cond_0

    .line 1266
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x10

    if-gt v3, v4, :cond_0

    .line 1270
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x10

    if-le v3, v4, :cond_7

    .line 1276
    :cond_0
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    .line 1277
    iput-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    .line 1279
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hp()Ljava/lang/String;

    move-result-object v3

    .line 1283
    const-string v4, "EasValidator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sendHttpClientPost:URI:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {v3}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v3

    invoke-direct {v4, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 1289
    invoke-virtual {p0, v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 1335
    :goto_1
    if-eqz p2, :cond_1

    .line 1337
    const-string v2, "Content-Type"

    const-string v3, "application/vnd.ms-sync.wbxml"

    invoke-virtual {v4, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1353
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->cj:Z

    if-eqz v2, :cond_3

    .line 1355
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->headerIterator()Lorg/apache/http/HeaderIterator;

    move-result-object v2

    .line 1357
    :cond_2
    :goto_2
    invoke-interface {v2}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_c

    .line 1376
    :cond_3
    const/4 v7, 0x0

    .line 1380
    const-string v2, "Sync"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "FolderSync"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1382
    const-string v2, "Provision"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "Ping"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1384
    const/4 v7, 0x1

    .line 1392
    :cond_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/exchange/a/b;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1394
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/exchange/a/b;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 1396
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    if-ltz v3, :cond_5

    .line 1398
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v5, v9, v4, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V

    .line 1404
    :cond_5
    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, v9

    move/from16 v5, p3

    invoke-virtual/range {v2 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v2

    return-object v2

    .line 1229
    :cond_6
    const/4 v2, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->c(IZ)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    move-object v9, v2

    goto/16 :goto_0

    .line 1294
    :cond_7
    new-instance v2, Lcom/android/exchange/adapter/b;

    iget-boolean v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    iget-boolean v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dK:Z

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dL:Ljava/lang/String;

    .line 1296
    iget-object v8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dM:Ljava/lang/String;

    .line 1294
    invoke-direct {v2, v3, v4, v6, v8}, Lcom/android/exchange/adapter/b;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    .line 1298
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dO:Ljava/lang/String;

    .line 1300
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->getDeviceType()Ljava/lang/String;

    move-result-object v8

    move-object v4, p1

    .line 1298
    invoke-virtual/range {v2 .. v8}, Lcom/android/exchange/adapter/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1304
    const-string v3, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendHttpClientPost:URI:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    invoke-direct {v4, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 1311
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ub:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 1312
    :cond_8
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hq()V

    .line 1317
    :cond_9
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1318
    :cond_a
    const-string v2, "Authorization"

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    :cond_b
    const-string v2, "Connection"

    const-string v3, "keep-alive"

    invoke-virtual {v4, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1359
    :cond_c
    invoke-interface {v2}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v3

    .line 1361
    if-eqz v3, :cond_d

    const-string v5, "Authorization"

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1362
    const-string v5, "EasValidator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "***********"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1364
    :cond_d
    if-eqz v3, :cond_2

    .line 1365
    const-string v5, "EasValidator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/String;[B)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 2

    .prologue
    .line 1208
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v0, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tS:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 6

    .prologue
    .line 1446
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;)V

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 1448
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->b(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v0

    .line 1447
    return-object v0
.end method

.method public a(Lorg/apache/http/client/HttpClient;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 613
    const-string v0, "OPTIONS"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    .line 614
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    .line 616
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hp()Ljava/lang/String;

    move-result-object v0

    .line 618
    new-instance v1, Lorg/apache/http/client/methods/HttpOptions;

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpOptions;-><init>(Ljava/net/URI;)V

    .line 620
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 624
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->cj:Z

    if-eqz v2, :cond_1

    .line 626
    const-string v2, "EasValidator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendHttpClientOptions(): URI String:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpOptions;->headerIterator()Lorg/apache/http/HeaderIterator;

    move-result-object v0

    .line 630
    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 646
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/exchange/a/b;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 648
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/exchange/a/b;->getPort(Landroid/content/Context;)I

    move-result v2

    .line 650
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    if-ltz v2, :cond_2

    .line 652
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mContext:Landroid/content/Context;

    invoke-static {v3, p1, v1, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V

    .line 657
    :cond_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;

    invoke-direct {v0, p0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;)V

    .line 658
    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/d;->a(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v0

    return-object v0

    .line 632
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v2

    .line 634
    if-eqz v2, :cond_4

    const-string v3, "Authorization"

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 635
    const-string v3, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 637
    :cond_4
    if-eqz v2, :cond_0

    .line 638
    const-string v3, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method a(Lorg/apache/http/Header;)V
    .locals 7

    .prologue
    .line 829
    invoke-interface {p1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 830
    const-string v0, "EasValidator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Server supports versions: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 832
    const/4 v1, 0x0

    .line 835
    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    if-lt v2, v5, :cond_0

    .line 852
    if-nez v0, :cond_3

    .line 853
    const-string v0, "EasValidator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No supported EAS versions: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    new-instance v0, Lcom/android/emailcommon/mail/MessagingException;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v0

    .line 835
    :cond_0
    aget-object v1, v4, v2

    .line 836
    const-string v6, "2.5"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 837
    const-string v6, "12.0"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 839
    const-string v6, "12.1"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 841
    const-string v6, "14.0"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 843
    const-string v6, "14.1"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    move-object v0, v1

    .line 835
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 856
    :cond_3
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    .line 857
    invoke-static {v0}, Lcom/android/emailcommon/EasRefs;->c(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    .line 858
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tU:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    sput-wide v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tV:D

    .line 861
    return-void
.end method

.method a(Lorg/apache/http/client/methods/HttpRequestBase;Z)V
    .locals 2

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 790
    const-string v0, "Authorization"

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_0
    const-string v0, "MS-ASProtocolVersion"

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    const-string v0, "Connection"

    const-string v1, "keep-alive"

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    const-string v0, "User-Agent"

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    if-eqz p2, :cond_1

    .line 799
    const-string v0, "0"

    .line 800
    const-string v1, "X-MS-PolicyKey"

    invoke-virtual {p1, v1, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    :cond_1
    return-void
.end method

.method protected a(Ljava/io/InputStream;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1193
    const/16 v1, 0x1c1

    if-eq p2, v1, :cond_0

    const/16 v1, 0x193

    if-ne p2, v1, :cond_1

    .line 1204
    :cond_0
    :goto_0
    return v0

    .line 1197
    :cond_1
    if-eqz p1, :cond_2

    :try_start_0
    new-instance v1, Lcom/android/exchange/adapter/c;

    invoke-direct {v1, p1}, Lcom/android/exchange/adapter/c;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Lcom/android/exchange/adapter/c;->y()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    .line 1204
    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1200
    :catch_0
    move-exception v0

    .line 1201
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method hp()Ljava/lang/String;
    .locals 3

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ub:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 668
    :cond_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hq()V

    .line 685
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    if-eqz v0, :cond_4

    const-string v0, "https"

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 688
    const-string v0, "://"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    if-eqz v0, :cond_5

    const-string v0, ":443"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 689
    const-string v2, "/Microsoft-Server-ActiveSync"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 691
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 693
    const-string v0, "?Cmd="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ub:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 697
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 699
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 703
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 685
    :cond_4
    const-string v0, "http"

    goto :goto_0

    .line 688
    :cond_5
    const-string v0, ""

    goto :goto_1
.end method

.method public hs()I
    .locals 10

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v0, -0x2

    .line 974
    :try_start_0
    const-string v3, "OPTIONS"

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tW:Ljava/lang/String;

    .line 975
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tX:Ljava/lang/String;

    .line 976
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hp()Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v3

    .line 978
    const/4 v4, 0x0

    .line 982
    :try_start_1
    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 985
    const/16 v6, 0x3a98

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tk:Ljava/lang/String;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/client/HttpClient;

    move-result-object v6

    .line 987
    const/4 v7, 0x1

    invoke-virtual {p0, v6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/HttpClient;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v3

    move-object v6, v3

    .line 1010
    :goto_0
    :try_start_2
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v3

    .line 1012
    const-string v7, "EasValidator"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Validation (OPTIONS) response: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    const/16 v7, 0xc8

    if-ne v3, v7, :cond_1c

    .line 1015
    const-string v3, "MS-ASProtocolCommands"

    invoke-virtual {v6, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->ay(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    .line 1016
    const-string v7, "ms-asprotocolversions"

    invoke-virtual {v6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->ay(Ljava/lang/String;)Lorg/apache/http/Header;
    :try_end_2
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v7

    .line 1019
    if-eqz v3, :cond_0

    if-nez v7, :cond_7

    .line 1020
    :cond_0
    :try_start_3
    const-string v3, "EasValidator"

    const-string v7, "OPTIONS response without commands or versions"

    invoke-static {v3, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    new-instance v3, Lcom/android/emailcommon/mail/MessagingException;

    const/4 v7, 0x0

    invoke-direct {v3, v7}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v3
    :try_end_3
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1026
    :catch_0
    move-exception v1

    .line 1171
    if-eqz v6, :cond_1

    .line 1172
    :try_start_4
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_1
    if-eqz v5, :cond_2

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_4
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    .line 1029
    :cond_2
    const/4 v0, -0x7

    .line 1188
    :cond_3
    :goto_1
    return v0

    .line 989
    :cond_4
    const/16 v6, 0x3a98

    :try_start_5
    iget-boolean v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    invoke-direct {p0, v6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->c(IZ)Lorg/apache/http/client/HttpClient;

    move-result-object v6

    .line 990
    iget-boolean v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->dJ:Z

    invoke-virtual {p0, v6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/HttpClient;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    :try_end_5
    .catch Ljavax/net/ssl/SSLException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    move-result-object v3

    move-object v6, v3

    .line 992
    goto :goto_0

    :catch_1
    move-exception v6

    .line 994
    :try_start_6
    invoke-direct {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->aq(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 995
    const-string v1, "EasValidator"

    const-string v2, "User decided to not accept the certificate"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const/4 v0, -0x5

    goto :goto_1

    .line 999
    :cond_5
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 1001
    const/16 v3, 0x3a98

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->mPath:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->tk:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v3, v6, v7, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lorg/apache/http/client/HttpClient;

    move-result-object v3

    .line 1002
    const/4 v6, 0x1

    invoke-virtual {p0, v3, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/HttpClient;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;

    move-result-object v3

    move-object v6, v3

    .line 1003
    goto :goto_0

    .line 1004
    :cond_6
    const/16 v3, 0x3a98

    const/4 v6, 0x0

    invoke-direct {p0, v3, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->c(IZ)Lorg/apache/http/client/HttpClient;

    move-result-object v3

    .line 1005
    const/4 v6, 0x0

    invoke-virtual {p0, v3, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/client/HttpClient;Z)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    :try_end_6
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    move-result-object v3

    move-object v6, v3

    goto/16 :goto_0

    .line 1024
    :cond_7
    :try_start_7
    invoke-virtual {p0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lorg/apache/http/Header;)V
    :try_end_7
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1034
    :try_start_8
    const-string v3, "EasValidator"

    const-string v4, "Try folder sync"

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    const-string v3, "0"

    .line 1039
    new-instance v4, Lcom/android/exchange/adapter/f;

    invoke-direct {v4}, Lcom/android/exchange/adapter/f;-><init>()V

    .line 1041
    const/16 v7, 0x1d6

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v7

    const/16 v8, 0x1d2

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/f;->x(I)Lcom/android/exchange/adapter/f;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/adapter/f;->h(Ljava/lang/String;)Lcom/android/exchange/adapter/f;

    move-result-object v3

    .line 1042
    invoke-virtual {v3}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/f;->R()Lcom/android/exchange/adapter/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/f;->done()V
    :try_end_8
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1045
    :try_start_9
    const-string v3, "FolderSync"

    invoke-virtual {v4}, Lcom/android/exchange/adapter/f;->toByteArray()[B

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Ljava/lang/String;[B)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v4

    .line 1046
    :try_start_a
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getStatus()I

    move-result v3

    .line 1047
    const/16 v7, 0xc8

    if-ne v3, v7, :cond_8

    .line 1048
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getLength()I

    move-result v7

    .line 1049
    if-eqz v7, :cond_8

    .line 1050
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 1053
    :cond_8
    const-string v7, "EasValidator"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "validateAccount(): FolderSync response code:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 1059
    :try_start_b
    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v7, :cond_9

    .line 1060
    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v7}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 1061
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    .line 1066
    :cond_9
    invoke-virtual {p0, v5, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Ljava/io/InputStream;I)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1068
    new-instance v3, Lcom/android/exchange/CommandStatusException;

    const/16 v5, 0x8e

    invoke-direct {v3, v5}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v3
    :try_end_b
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1121
    :catch_2
    move-exception v3

    move-object v5, v4

    .line 1122
    :goto_2
    :try_start_c
    iget v3, v3, Lcom/android/exchange/CommandStatusException;->mStatus:I

    .line 1123
    invoke-static {v3}, Lcom/android/exchange/a;->q(I)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 1125
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;)Lcom/android/exchange/adapter/d;

    move-result-object v1

    .line 1126
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/android/exchange/adapter/d;->I()Z

    move-result v3

    if-nez v3, :cond_2c

    .line 1134
    :cond_a
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/android/exchange/adapter/d;->K()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 1135
    invoke-virtual {v1}, Lcom/android/exchange/adapter/d;->K()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-gtz v1, :cond_2c

    .line 1144
    :cond_b
    const-string v1, "EasValidator"

    .line 1145
    const-string v2, "We have no supported policy set, but unsupported policies list is null too. This is unusual and invalid situation. Return UNSPECIFIED_EXCEPTION"

    .line 1144
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 1171
    if-eqz v6, :cond_c

    .line 1172
    :try_start_d
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_c
    if-eqz v5, :cond_d

    .line 1174
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_d
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    .line 1148
    :cond_d
    const/4 v0, -0x3

    goto/16 :goto_1

    .line 1057
    :catchall_0
    move-exception v3

    move-object v4, v5

    .line 1059
    :goto_3
    :try_start_e
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v5, :cond_e

    .line 1060
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 1061
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->ue:Lorg/apache/http/client/methods/HttpPost;

    .line 1063
    :cond_e
    throw v3
    :try_end_e
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1170
    :catchall_1
    move-exception v1

    move-object v5, v4

    .line 1171
    :goto_4
    if-eqz v6, :cond_f

    .line 1172
    :try_start_f
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_f
    if-eqz v5, :cond_10

    .line 1174
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1175
    :cond_10
    throw v1
    :try_end_f
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5

    .line 1176
    :catch_3
    move-exception v1

    .line 1177
    const-string v2, "EasValidator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeviceAccessException caught: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/emailcommon/utility/DeviceAccessException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1082
    :cond_11
    const/16 v5, 0x194

    if-ne v3, v5, :cond_13

    .line 1171
    if-eqz v6, :cond_12

    .line 1172
    :try_start_10
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_12
    if-eqz v4, :cond_3

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_10
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5

    goto/16 :goto_1

    .line 1180
    :catch_4
    move-exception v1

    .line 1181
    const-string v2, "EasValidator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException caught: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1088
    :cond_13
    :try_start_11
    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->aT(I)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1089
    const-string v3, "EasValidator"

    const-string v5, "Authentication failed for foldersync in validateAccount"

    invoke-static {v3, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 1171
    if-eqz v6, :cond_14

    .line 1172
    :try_start_12
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_14
    if-eqz v4, :cond_15

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_12
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_12 .. :try_end_12} :catch_3
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_4
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5

    .line 1092
    :cond_15
    const/4 v0, -0x4

    goto/16 :goto_1

    .line 1093
    :cond_16
    const/16 v5, 0xc8

    if-eq v3, v5, :cond_19

    .line 1095
    :try_start_13
    const-string v5, "EasValidator"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unexpected response for FolderSync:  "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_13 .. :try_end_13} :catch_2
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 1171
    if-eqz v6, :cond_17

    .line 1172
    :try_start_14
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_17
    if-eqz v4, :cond_18

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_14
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_4
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_5

    :cond_18
    move v0, v1

    .line 1098
    goto/16 :goto_1

    .line 1100
    :cond_19
    :try_start_15
    const-string v3, "EasValidator"

    const-string v5, "Validation successful"

    invoke-static {v3, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_15 .. :try_end_15} :catch_2
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 1171
    if-eqz v6, :cond_1a

    .line 1172
    :try_start_16
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_1a
    if-eqz v4, :cond_1b

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_16
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_16 .. :try_end_16} :catch_3
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_4
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_5

    :cond_1b
    move v0, v2

    .line 1188
    goto/16 :goto_1

    .line 1102
    :cond_1c
    :try_start_17
    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->aT(I)Z

    move-result v7

    if-eqz v7, :cond_1f

    .line 1103
    const-string v3, "EasValidator"

    const-string v7, "Authentication failed"

    invoke-static {v3, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_17 .. :try_end_17} :catch_6
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    .line 1171
    if-eqz v6, :cond_1d

    .line 1172
    :try_start_18
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_1d
    if-eqz v5, :cond_1e

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_18
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_18 .. :try_end_18} :catch_3
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_4
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_5

    .line 1105
    :cond_1e
    const/4 v0, -0x4

    goto/16 :goto_1

    .line 1106
    :cond_1f
    const/16 v7, 0x1f4

    if-ne v3, v7, :cond_21

    .line 1109
    :try_start_19
    const-string v3, "EasValidator"

    const-string v7, "Internal server error"

    invoke-static {v3, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_19 .. :try_end_19} :catch_6
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    .line 1171
    if-eqz v6, :cond_20

    .line 1172
    :try_start_1a
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_20
    if-eqz v5, :cond_3

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_1a
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1a .. :try_end_1a} :catch_3
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_5

    goto/16 :goto_1

    .line 1184
    :catch_5
    move-exception v1

    .line 1185
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 1116
    :cond_21
    :try_start_1b
    const-string v7, "EasValidator"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Validation failed, reporting I/O error: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1b
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_1b .. :try_end_1b} :catch_6
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    .line 1171
    if-eqz v6, :cond_22

    .line 1172
    :try_start_1c
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_22
    if-eqz v5, :cond_23

    .line 1174
    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_1c
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1c .. :try_end_1c} :catch_3
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_5

    :cond_23
    move v0, v1

    .line 1118
    goto/16 :goto_1

    .line 1151
    :cond_24
    :try_start_1d
    invoke-static {v3}, Lcom/android/exchange/a;->r(I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1153
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Denied access: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/exchange/a;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    .line 1171
    if-eqz v6, :cond_25

    .line 1172
    :try_start_1e
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_25
    if-eqz v5, :cond_26

    .line 1174
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_1e
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1e .. :try_end_1e} :catch_3
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_5

    .line 1157
    :cond_26
    const/4 v0, -0x6

    goto/16 :goto_1

    .line 1158
    :cond_27
    :try_start_1f
    invoke-static {v3}, Lcom/android/exchange/a;->s(I)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 1159
    const-string v1, "EasValidator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Transient error: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/exchange/a;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_2

    .line 1171
    if-eqz v6, :cond_28

    .line 1172
    :try_start_20
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_28
    if-eqz v5, :cond_3

    .line 1174
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_20
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_20 .. :try_end_20} :catch_3
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_4
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_5

    goto/16 :goto_1

    .line 1163
    :cond_29
    :try_start_21
    const-string v2, "EasValidator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Unexpected response: "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/exchange/a;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_2

    .line 1171
    if-eqz v6, :cond_2a

    .line 1172
    :try_start_22
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_2a
    if-eqz v5, :cond_2b

    .line 1174
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    :cond_2b
    move v0, v1

    .line 1165
    goto/16 :goto_1

    .line 1171
    :cond_2c
    if-eqz v6, :cond_2d

    .line 1172
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V

    .line 1173
    :cond_2d
    if-eqz v5, :cond_2e

    .line 1174
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/i;->close()V
    :try_end_22
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_22 .. :try_end_22} :catch_3
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_4
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_5

    :cond_2e
    move v0, v2

    .line 1168
    goto/16 :goto_1

    .line 1170
    :catchall_2
    move-exception v1

    goto/16 :goto_4

    .line 1121
    :catch_6
    move-exception v3

    goto/16 :goto_2

    .line 1057
    :catchall_3
    move-exception v3

    goto/16 :goto_3
.end method
