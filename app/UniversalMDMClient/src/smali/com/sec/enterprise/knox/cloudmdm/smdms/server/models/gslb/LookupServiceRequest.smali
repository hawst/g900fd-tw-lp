.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest;
.super Ljava/lang/Object;
.source "LookupServiceRequest.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonInclude;
    value = .enum Lcom/fasterxml/jackson/annotation/JsonInclude$Include;->NON_NULL:Lcom/fasterxml/jackson/annotation/JsonInclude$Include;
.end annotation


# instance fields
.field private data:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;

.field private signature:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest;->data:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest;->signature:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;)V
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest;->data:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;

    .line 17
    return-void
.end method

.method public setSignature(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest;->signature:Ljava/lang/String;

    .line 25
    return-void
.end method
