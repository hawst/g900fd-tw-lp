.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;
.super Landroid/app/Activity;
.source "SelfUpdateActivity.java"


# static fields
.field private static aC:Landroid/app/Notification;

.field private static mAppContext:Landroid/content/Context;

.field private static mContext:Landroid/content/Context;

.field private static mHandler:Landroid/os/Handler;

.field private static ry:Ljava/lang/String;

.field private static yV:Landroid/app/Activity;

.field private static yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

.field private static yX:I

.field private static yY:Landroid/app/AlertDialog$Builder;

.field private static yZ:Landroid/app/ProgressDialog;

.field private static yd:Z

.field private static za:Landroid/app/ProgressDialog;

.field private static zb:Landroid/os/PowerManager$WakeLock;

.field private static zc:Landroid/widget/RemoteViews;

.field private static zd:I

.field private static ze:I

.field private static zf:I

.field private static zg:I

.field private static zn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 33
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    .line 35
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    .line 37
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 40
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    .line 41
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    .line 42
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    .line 43
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    .line 44
    sput-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yd:Z

    .line 45
    sput-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zn:Z

    .line 46
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    .line 57
    sput v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 59
    const-string v0, "UMC:SelfUpdateActivity"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    .line 67
    sput v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    .line 68
    const/16 v0, 0x1a0a

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zg:I

    .line 70
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mHandler:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic E(Z)V
    .locals 0

    .prologue
    .line 44
    sput-boolean p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yd:Z

    return-void
.end method

.method public static O(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 644
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "showProgressOnNotification"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    .line 646
    const/4 v0, 0x3

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 647
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jd()V

    .line 649
    return-void
.end method

.method public static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;)V
    .locals 0

    .prologue
    .line 187
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    .line 188
    return-void
.end method

.method static synthetic b(Landroid/app/AlertDialog$Builder;)V
    .locals 0

    .prologue
    .line 37
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method static synthetic b(Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 40
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    return-void
.end method

.method public static bf(I)V
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x4

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 179
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 180
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->bg(I)V

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "showRetryPopup() failed because mSelf is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static bg(I)V
    .locals 1

    .prologue
    .line 192
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    packed-switch v0, :pswitch_data_0

    .line 213
    :goto_0
    :pswitch_0
    return-void

    .line 194
    :pswitch_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iZ()V

    goto :goto_0

    .line 197
    :pswitch_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jo()V

    goto :goto_0

    .line 200
    :pswitch_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jp()V

    goto :goto_0

    .line 203
    :pswitch_4
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jb()V

    goto :goto_0

    .line 206
    :pswitch_5
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->bh(I)V

    goto :goto_0

    .line 209
    :pswitch_6
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jq()V

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private static bh(I)V
    .locals 5

    .prologue
    .line 402
    const v0, 0x7f080059

    .line 403
    const v1, 0x7f080053

    .line 404
    const v2, 0x7f080099

    .line 407
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-nez v3, :cond_0

    .line 408
    new-instance v3, Landroid/app/AlertDialog$Builder;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 410
    :cond_0
    if-nez p0, :cond_1

    .line 411
    const p0, 0x7f080070

    .line 413
    :cond_1
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 414
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 415
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 417
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$9;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$9;-><init>()V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 428
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$10;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$10;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 441
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 442
    return-void
.end method

.method public static bi(I)V
    .locals 0

    .prologue
    .line 698
    sput p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 699
    return-void
.end method

.method static synthetic c(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 33
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    return-void
.end method

.method public static c(ZI)V
    .locals 2

    .prologue
    .line 390
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 399
    :goto_0
    return-void

    .line 393
    :cond_0
    const/4 v0, 0x0

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    .line 395
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 396
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->e(ZI)V

    goto :goto_0

    .line 398
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->d(ZI)V

    goto :goto_0
.end method

.method public static close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 614
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "close() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iW()V

    .line 616
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iX()V

    .line 617
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iY()V

    .line 618
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    .line 619
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 620
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    .line 621
    sput v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    .line 622
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    .line 623
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    .line 624
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 625
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    .line 626
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    .line 629
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zn:Z

    .line 631
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 632
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 633
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    .line 634
    sput-boolean v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zn:Z

    .line 637
    :cond_0
    return-void
.end method

.method private static d(ZI)V
    .locals 2

    .prologue
    .line 470
    if-nez p0, :cond_0

    .line 471
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 472
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 473
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 474
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 481
    :goto_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iY()V

    .line 482
    return-void

    .line 477
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 478
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 479
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public static e(II)V
    .locals 2

    .prologue
    .line 367
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    sput p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zd:I

    .line 371
    sput p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ze:I

    .line 373
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zd:I

    mul-int/lit8 v0, v0, 0x64

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ze:I

    div-int/2addr v0, v1

    .line 374
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    if-le v0, v1, :cond_0

    .line 375
    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    .line 377
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 378
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->je()V

    goto :goto_0

    .line 380
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jc()V

    goto :goto_0
.end method

.method private static e(ZI)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f08006b

    const/4 v6, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 486
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    .line 487
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 486
    check-cast v0, Landroid/app/NotificationManager;

    .line 488
    new-instance v2, Landroid/support/v4/app/v;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, v1}, Landroid/support/v4/app/v;-><init>(Landroid/content/Context;)V

    .line 491
    if-eqz p0, :cond_0

    .line 492
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const v3, 0x7f080067

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 497
    :goto_0
    new-instance v3, Landroid/content/Intent;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 498
    const-string v4, "com.sec.enterprise.knox.cloudmdm.smdms.intent.UNHIDE_PROGRESS_BAR"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 499
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 500
    const-string v4, "showWindow"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 501
    const-string v4, "msg"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 502
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const/16 v5, 0x1a0a

    .line 503
    const/high16 v6, 0x8000000

    .line 502
    invoke-static {v4, v5, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 504
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/app/v;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v4

    .line 505
    const v5, 0x7f020049

    invoke-virtual {v4, v5}, Landroid/support/v4/app/v;->g(I)Landroid/support/v4/app/v;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/support/v4/app/v;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v4

    .line 506
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/support/v4/app/v;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v1

    .line 507
    invoke-virtual {v1, v7, v7, v7}, Landroid/support/v4/app/v;->a(IIZ)Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/support/v4/app/v;->d(Z)Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/support/v4/app/v;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/v;

    .line 508
    invoke-virtual {v2, v8}, Landroid/support/v4/app/v;->c(Z)Landroid/support/v4/app/v;

    .line 509
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zg:I

    invoke-virtual {v2}, Landroid/support/v4/app/v;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 510
    sput-object v10, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    .line 511
    sput-object v10, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    .line 513
    return-void

    .line 494
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 495
    sput v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    goto/16 :goto_0
.end method

.method static synthetic go()Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method public static iT()V
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x3

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 173
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->bg(I)V

    .line 175
    :cond_0
    return-void
.end method

.method public static iV()Z
    .locals 1

    .prologue
    .line 640
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yd:Z

    return v0
.end method

.method public static iW()V
    .locals 2

    .prologue
    .line 665
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 666
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "clearLoading() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 668
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    .line 671
    :cond_0
    return-void
.end method

.method public static iX()V
    .locals 2

    .prologue
    .line 674
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_0

    .line 675
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "clearUpdateAvailable() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 679
    :cond_0
    return-void
.end method

.method public static iY()V
    .locals 2

    .prologue
    .line 682
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 683
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "clearProgressUi() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 685
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    .line 687
    :cond_0
    return-void
.end method

.method private static iZ()V
    .locals 3

    .prologue
    .line 216
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 217
    new-instance v0, Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    .line 218
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    const v2, 0x7f08006d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 219
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 220
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->za:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 221
    return-void
.end method

.method private static jb()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 314
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    .line 317
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 318
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 319
    const/16 v1, 0xa

    .line 320
    const-string v2, "UMCSelfUpdate"

    .line 319
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    .line 322
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 324
    const v0, 0x7f080060

    .line 325
    const v1, 0x7f080063

    .line 326
    const v2, 0x7f080057

    .line 328
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 329
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 330
    const-string v1, "                         "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 329
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 331
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 332
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setProgressPercentFormat(Ljava/text/NumberFormat;)V

    .line 333
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 334
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 335
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 336
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 337
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    if-lez v0, :cond_2

    .line 338
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jc()V

    .line 340
    :cond_2
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    const/16 v1, 0x63

    if-gt v0, v1, :cond_3

    .line 341
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, -0x1

    .line 342
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$8;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$8;-><init>()V

    .line 341
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 361
    :cond_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 363
    return-void
.end method

.method private static jc()V
    .locals 4

    .prologue
    .line 587
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    const/16 v1, 0x63

    if-lt v0, v1, :cond_2

    .line 591
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    const v1, 0x7f0800b5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 592
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const v2, 0x7f080060

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 593
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 594
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 595
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 596
    if-eqz v0, :cond_0

    .line 597
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 601
    :cond_2
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    if-nez v0, :cond_3

    const-string v0, ""

    .line 602
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    const v3, 0x7f080063

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 603
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 604
    const-string v2, "                        "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 605
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 606
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 602
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 607
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 608
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yZ:Landroid/app/ProgressDialog;

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0

    .line 601
    :cond_3
    const-string v0, "   "

    goto :goto_1
.end method

.method private static jd()V
    .locals 4

    .prologue
    .line 652
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 653
    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.intent.UNHIDE_PROGRESS_BAR"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 654
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 655
    const-string v1, "showWindow"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 656
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const/16 v2, 0x4d2

    .line 657
    const/high16 v3, 0x8000000

    .line 656
    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 658
    new-instance v1, Landroid/support/v4/app/v;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/v;-><init>(Landroid/content/Context;)V

    .line 659
    const v2, 0x7f020049

    invoke-virtual {v1, v2}, Landroid/support/v4/app/v;->g(I)Landroid/support/v4/app/v;

    move-result-object v1

    .line 660
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const v3, 0x7f080061

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/v;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/v;

    move-result-object v0

    .line 661
    invoke-virtual {v0}, Landroid/support/v4/app/v;->build()Landroid/app/Notification;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    .line 662
    return-void
.end method

.method private static je()V
    .locals 10

    .prologue
    const/high16 v8, 0x44800000    # 1024.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 546
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jr()Z

    move-result v0

    if-nez v0, :cond_0

    .line 547
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "mAppContext is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :goto_0
    return-void

    .line 550
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    .line 551
    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 550
    check-cast v0, Landroid/app/NotificationManager;

    .line 552
    const-string v3, " "

    .line 554
    const-string v4, " "

    .line 556
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    if-nez v5, :cond_1

    .line 557
    new-instance v5, Landroid/widget/RemoteViews;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 558
    const v7, 0x7f03000f

    .line 557
    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    .line 560
    :cond_1
    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    const/16 v6, 0x63

    if-gt v5, v6, :cond_2

    .line 561
    new-instance v3, Ljava/lang/StringBuilder;

    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 562
    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zd:I

    int-to-float v5, v5

    div-float/2addr v5, v8

    .line 563
    sget v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ze:I

    int-to-float v6, v6

    div-float/2addr v6, v8

    .line 564
    const-string v7, "%.1f"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v8, v1

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 565
    const-string v7, "%.1f"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v2, v1

    invoke-static {v7, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 566
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "MB"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "MB"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 573
    :goto_1
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    const v6, 0x7f09002f

    .line 574
    sget-object v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const v8, 0x7f08006b

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 573
    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 575
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    const v6, 0x7f09002b

    invoke-virtual {v5, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 576
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    const v5, 0x7f090031

    invoke-virtual {v3, v5, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 577
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    const v4, 0x7f090032

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 579
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    const v3, 0x7f09002c

    const/16 v4, 0x64

    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zf:I

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 580
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zc:Landroid/widget/RemoteViews;

    iput-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 581
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 582
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zg:I

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 570
    :cond_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    const v5, 0x7f080065

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move v9, v2

    move-object v2, v1

    move v1, v9

    goto :goto_1
.end method

.method static synthetic jf()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    return-object v0
.end method

.method public static jn()V
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x2

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    .line 159
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->bg(I)V

    .line 161
    :cond_0
    return-void
.end method

.method private static jo()V
    .locals 6

    .prologue
    .line 224
    const v0, 0x7f080058

    .line 225
    const v1, 0x7f080054

    .line 226
    const v2, 0x7f080060

    .line 227
    const v3, 0x7f08005d

    .line 229
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-nez v4, :cond_0

    .line 230
    new-instance v4, Landroid/app/AlertDialog$Builder;

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 231
    :cond_0
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 233
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$3;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$3;-><init>()V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 247
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$4;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$4;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 261
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 262
    return-void
.end method

.method private static jp()V
    .locals 7

    .prologue
    .line 265
    const v1, 0x7f080058

    .line 266
    const v2, 0x7f080053

    .line 267
    const v3, 0x7f080061

    .line 268
    const v4, 0x7f08005e

    .line 270
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    const v5, 0x7f030002

    const/4 v6, 0x0

    invoke-static {v0, v5, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 271
    const v0, 0x7f090003

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 272
    new-instance v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$5;

    invoke-direct {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$5;-><init>()V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 281
    const v6, 0x7f08006a

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setText(I)V

    .line 283
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 285
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 286
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$6;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$6;-><init>()V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 297
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$7;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$7;-><init>()V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 310
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 311
    return-void
.end method

.method private static jq()V
    .locals 5

    .prologue
    .line 445
    const v0, 0x7f080058

    .line 446
    const v1, 0x7f08009a

    .line 447
    const v2, 0x7f08009e

    .line 449
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-nez v3, :cond_0

    .line 450
    new-instance v3, Landroid/app/AlertDialog$Builder;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 451
    :cond_0
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 452
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 453
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 455
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$2;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity$2;-><init>()V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 466
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 467
    return-void
.end method

.method private static jr()Z
    .locals 2

    .prologue
    .line 516
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 517
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "Serious error: mAppContext is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const/4 v0, 0x0

    .line 536
    :goto_0
    return v0

    .line 520
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    if-nez v0, :cond_1

    .line 534
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jd()V

    .line 536
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static js()V
    .locals 1

    .prologue
    .line 690
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zn:Z

    .line 691
    return-void
.end method

.method public static jt()I
    .locals 1

    .prologue
    .line 694
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    return v0
.end method

.method static synthetic ju()I
    .locals 1

    .prologue
    .line 57
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yX:I

    return v0
.end method

.method static synthetic jv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic jw()V
    .locals 0

    .prologue
    .line 651
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->jd()V

    return-void
.end method

.method static synthetic jx()V
    .locals 0

    .prologue
    .line 545
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->je()V

    return-void
.end method

.method static synthetic jy()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "onBackPressed() ignored"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    .line 87
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->aC:Landroid/app/Notification;

    .line 88
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mAppContext:Landroid/content/Context;

    .line 89
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->mContext:Landroid/content/Context;

    .line 90
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 130
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->yV:Landroid/app/Activity;

    .line 135
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 109
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iW()V

    .line 111
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iX()V

    .line 112
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->iY()V

    .line 113
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 95
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zn:Z

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "activity need to be closed so closing now"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->finish()V

    .line 99
    sput-boolean v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zn:Z

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "msg"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->bg(I)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 119
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 121
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->zb:Landroid/os/PowerManager$WakeLock;

    .line 123
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/SelfUpdateActivity;->ry:Ljava/lang/String;

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method
