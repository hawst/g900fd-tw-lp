.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "UMCPolicyMethodInvoker.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:UMCPolicyMethodInvoker"


# instance fields
.field private adminProfileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 48
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->adminProfileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    .line 52
    return-void
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->setAPIReturnParams(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V

    return-void
.end method


# virtual methods
.method protected createArgValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "applyProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "listener"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mArgNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mArgTypes:Ljava/util/List;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mArgObjs:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->adminProfileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->createArgValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected handleContinueFromAsyncResponse()Z
    .locals 3

    .prologue
    .line 73
    const-string v0, "UMC:UMCPolicyMethodInvoker"

    const-string v1, "UMCPolicyMethodInvoker: handleContinueFromAsyncResponse"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->handleContinueFromAsyncResponse()Z

    move-result v0

    .line 75
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mPolicyApplier:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getProfileHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->adminProfileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->setProfileListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;)V

    .line 76
    return v0
.end method

.method protected hasAPIResultIntentsToListen()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "activateELMLicense"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "activateKLMLicense"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->apiResultIntents:Ljava/util/Map;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected isInvokeCompleted()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 94
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->getReturnVal()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->getReturnVal()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 101
    :goto_0
    return v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "applyProfile"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 101
    goto :goto_0
.end method
