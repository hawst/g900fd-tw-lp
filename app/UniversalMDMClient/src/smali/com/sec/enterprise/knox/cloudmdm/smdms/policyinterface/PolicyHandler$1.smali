.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;
.super Landroid/os/Handler;
.source "PolicyHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 88
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 90
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 94
    :pswitch_0
    :try_start_0
    const-string v0, "UMC:PolicyHandler"

    const-string v1, "at Handler case : POLICY_APPLIED"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;

    .line 97
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyListeners:Ljava/util/Map;

    iget-object v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    .line 98
    if-eqz v1, :cond_3

    .line 99
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->user:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;->onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V

    .line 100
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyListeners:Ljava/util/Map;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->flushFlag:Z
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    const-string v0, "UMC:PolicyHandler"

    const-string v1, " - flushing pending policies"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->cancel()V

    .line 110
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->resetFlushFlag()V
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V

    .line 111
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyAppliers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$4(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyHandlerAsync:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$4(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "UMC:PolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/commons/a/a/a;->b(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->user:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;->onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 122
    :pswitch_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyNextPendingPolicy()Z
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$5(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Z

    goto/16 :goto_0

    .line 126
    :pswitch_2
    :try_start_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;

    .line 127
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mPolicyListeners:Ljava/util/Map;

    iget-object v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->documentId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    .line 128
    if-eqz v1, :cond_4

    .line 129
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->user:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;->onAlert(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 132
    :catch_1
    move-exception v0

    .line 133
    const-string v1, "UMC:PolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 130
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mGlobalPolicyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->mMDMUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->user:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;->onAlert(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
