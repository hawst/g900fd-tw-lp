.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;
.super Ljava/lang/Object;
.source "ObjectTranslator.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:ObjectTranslator"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static X509toPEMSTRING(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 942
    const/4 v1, 0x0

    .line 945
    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->convertToPem(Ljava/lang/Object;)[B

    move-result-object v2

    .line 946
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 952
    :goto_0
    if-eqz v0, :cond_0

    .line 953
    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 956
    :cond_0
    return-object v0

    .line 947
    :catch_0
    move-exception v0

    .line 949
    const-string v2, "UMC:ObjectTranslator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic access$0(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 941
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->X509toPEMSTRING(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static convertToPem(Ljava/lang/Object;)[B
    .locals 3

    .prologue
    .line 885
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 886
    new-instance v1, Ljava/io/OutputStreamWriter;

    const-string v2, "US-ASCII"

    invoke-direct {v1, v0, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 890
    new-instance v2, Lorg/spongycastle/openssl/r;

    invoke-direct {v2, v1}, Lorg/spongycastle/openssl/r;-><init>(Ljava/io/Writer;)V

    .line 891
    invoke-virtual {v2, p0}, Lorg/spongycastle/openssl/r;->writeObject(Ljava/lang/Object;)V

    .line 892
    invoke-virtual {v2}, Lorg/spongycastle/openssl/r;->close()V

    .line 895
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 3

    .prologue
    .line 242
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getJavaType(Ljava/lang/String;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->unwrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 243
    const/4 v0, 0x0

    .line 244
    if-eqz p0, :cond_0

    .line 245
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 246
    invoke-virtual {v0, p0}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 247
    invoke-virtual {v0, v2, v1}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 249
    :cond_0
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v2, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    return-object v2
.end method

.method public static createNormalObject(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 254
    const/4 v0, 0x0

    .line 255
    if-eqz p0, :cond_0

    .line 256
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 257
    invoke-virtual {v0, p0}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 258
    invoke-virtual {v0, v1, p1}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 260
    :cond_0
    return-object v0
.end method

.method private static extractLatLong(Ljava/util/Map;)Landroid/app/enterprise/geofencing/LatLongPoint;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;)",
            "Landroid/app/enterprise/geofencing/LatLongPoint;"
        }
    .end annotation

    .prologue
    .line 871
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 872
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - wrong number of arguments for Lat-Long (should be 2)"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :cond_0
    const-string v0, "latitude"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 875
    const-string v0, "longitude"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 877
    new-instance v4, Landroid/app/enterprise/geofencing/LatLongPoint;

    invoke-direct {v4, v2, v3, v0, v1}, Landroid/app/enterprise/geofencing/LatLongPoint;-><init>(DD)V

    return-object v4
.end method

.method public static getJavaType(Ljava/lang/String;)Ljava/lang/reflect/Type;
    .locals 2

    .prologue
    .line 227
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getOtherType(Ljava/lang/String;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_1

    .line 237
    :cond_0
    return-object v0

    .line 233
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->get()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->getBasicJavaType(Ljava/lang/String;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 234
    if-nez v0, :cond_0

    .line 235
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Param type is not supported"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getKnoxConfigurationTypeMap(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/container/KnoxConfigurationType;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 851
    .line 853
    instance-of v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 854
    const-string v1, "knoxConfigurationType"

    .line 860
    :goto_0
    if-nez v1, :cond_2

    .line 865
    :goto_1
    return-object v0

    .line 855
    :cond_0
    instance-of v1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    if-eqz v1, :cond_1

    .line 856
    const-string v1, "LightweightConfigurationType"

    goto :goto_0

    .line 857
    :cond_1
    instance-of v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    if-eqz v1, :cond_3

    .line 858
    const-string v1, "ContainerModeConfigurationType"

    goto :goto_0

    .line 863
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 864
    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method private static getListFromX509CertList(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 926
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 928
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 929
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 938
    :cond_0
    return-object v2

    .line 931
    :cond_1
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 935
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->X509toPEMSTRING(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 929
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static getOtherType(Ljava/lang/String;)Ljava/lang/reflect/Type;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 105
    :try_start_0
    const-string v1, "CCMProfile"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$1;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$1;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 223
    :cond_0
    :goto_0
    return-object v0

    .line 107
    :cond_1
    const-string v1, "CertificateProfile"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$2;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$2;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_2
    const-string v1, "CSRProfile"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 110
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$3;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$3;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 111
    :cond_3
    const-string v1, "KioskSetting"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 112
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$4;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$4;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$4;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 113
    :cond_4
    const-string v1, "ReferencedArg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 114
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$5;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$5;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$5;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_5
    const-string v1, "WifiAdminProfile"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 116
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$6;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$6;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$6;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_6
    const-string v1, "WifiControlInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 118
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$7;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$7;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$7;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 119
    :cond_7
    const-string v1, "CertificateInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 120
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$8;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$8;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$8;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_8
    const-string v1, "SimChangeInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 122
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$9;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$9;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$9;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 123
    :cond_9
    const-string v1, "LDAPAccount"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 124
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$10;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$10;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$10;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 125
    :cond_a
    const-string v1, "Account"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 126
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$11;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$11;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$11;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 127
    :cond_b
    const-string v1, "Account[]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 128
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$12;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$12;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$12;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 129
    :cond_c
    const-string v1, "ExchangeAccount"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 130
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$13;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$13;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$13;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 131
    :cond_d
    const-string v1, "EnterpriseDeviceManager.EnterpriseSdkVersion"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 132
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$14;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$14;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$14;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 133
    :cond_e
    const-string v1, "EmailAccount"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 134
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$15;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$15;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$15;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 135
    :cond_f
    const-string v1, "AccountControlInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 136
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$16;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$16;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$16;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 137
    :cond_10
    const-string v1, "Date"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 138
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$17;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$17;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$17;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 139
    :cond_11
    const-string v1, "BluetoothSecureModeConfig"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 140
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$18;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$18;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$18;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 141
    :cond_12
    const-string v1, "BluetoothSecureModeWhitelistConfig"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 142
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$19;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$19;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$19;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 143
    :cond_13
    const-string v1, "BluetoothControlInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 144
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$20;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$20;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$20;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 145
    :cond_14
    const-string v1, "AppInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 146
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$21;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$21;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$21;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 147
    :cond_15
    const-string v1, "AppInfoLastUsage[]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 148
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$22;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$22;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$22;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 149
    :cond_16
    const-string v1, "NetworkStats"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 150
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$23;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$23;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$23;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 151
    :cond_17
    const-string v1, "AppControlInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 152
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$24;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$24;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$24;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 157
    :cond_18
    const-string v1, "AppPermissionControlInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 158
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$25;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$25;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$25;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 159
    :cond_19
    const-string v1, "ApnSettings"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 160
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$26;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$26;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$26;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 163
    :cond_1a
    const-string v1, "LockscreenOverlay.LSOImage[]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 164
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$27;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$27;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$27;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 165
    :cond_1b
    const-string v1, "LockscreenOverlay.LSOEmergencyPhoneInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 166
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$28;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$28;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$28;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 167
    :cond_1c
    const-string v1, "X509Certificate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 168
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$29;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$29;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$29;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    :cond_1d
    const-string v1, "CertificateControlInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 170
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$30;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$30;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$30;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 171
    :cond_1e
    const-string v1, "PermissionApplicationPrivateKey"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 172
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$31;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$31;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$31;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 173
    :cond_1f
    const-string v1, "AdminProfile"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 174
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$32;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$32;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$32;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 175
    :cond_20
    const-string v1, "KnoxConfigurationType"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 176
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$33;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$33;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$33;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 177
    :cond_21
    const-string v1, "List<UsbDeviceConfig>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 178
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$34;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$34;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$34;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 179
    :cond_22
    const-string v1, "List<String>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 180
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$35;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$35;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$35;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 181
    :cond_23
    const-string v1, "List<WifiControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 182
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$36;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$36;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$36;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 183
    :cond_24
    const-string v1, "List<CertificateInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 184
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$37;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$37;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$37;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 185
    :cond_25
    const-string v1, "List<LDAPAccount>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 186
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$38;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$38;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$38;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 187
    :cond_26
    const-string v1, "List<AccountControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 188
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$39;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$39;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$39;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 189
    :cond_27
    const-string v1, "List<BluetoothSecureModeWhitelistConfig>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 190
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$40;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$40;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$40;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 191
    :cond_28
    const-string v1, "List<BluetoothControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 192
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$41;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$41;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$41;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 193
    :cond_29
    const-string v1, "List<AppInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 194
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$42;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$42;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$42;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 195
    :cond_2a
    const-string v1, "List<NetworkStats>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 196
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$43;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$43;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$43;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 197
    :cond_2b
    const-string v1, "List<AppControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 198
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$44;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$44;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$44;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 199
    :cond_2c
    const-string v1, "ArrayList<Integer>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 200
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$45;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$45;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$45;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 205
    :cond_2d
    const-string v1, "List<AppPermissionControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 206
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$46;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$46;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$46;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 207
    :cond_2e
    const-string v1, "List<ApnSettings>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 208
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$47;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$47;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$47;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 209
    :cond_2f
    const-string v1, "List<Integer>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 210
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$48;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$48;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$48;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 211
    :cond_30
    const-string v1, "List<X509Certificate>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 212
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$49;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$49;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$49;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 213
    :cond_31
    const-string v1, "List<CertificateControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 214
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$50;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$50;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$50;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 215
    :cond_32
    const-string v1, "List<PermissionApplicationPrivateKey>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 216
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$51;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$51;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$51;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    goto/16 :goto_0

    .line 217
    :cond_33
    const-string v1, "List<KnoxConfigurationType>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$52;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$52;-><init>()V

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$52;->getType()Ljava/lang/reflect/Type;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 219
    :catch_0
    move-exception v1

    .line 220
    const-string v2, "UMC:ObjectTranslator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Translating Type : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "thrown exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static getX509CertList(Ljava/lang/Object;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 900
    check-cast p0, Ljava/util/ArrayList;

    .line 901
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 902
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 903
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 921
    :cond_0
    return-object v3

    .line 904
    :cond_1
    const/4 v2, 0x0

    .line 906
    :try_start_0
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/a;->aB(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 914
    :goto_1
    if-nez v0, :cond_2

    .line 915
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - Certificate is null"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 907
    :catch_0
    move-exception v0

    .line 909
    const-string v4, "UMC:ObjectTranslator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_1

    .line 910
    :catch_1
    move-exception v0

    .line 912
    const-string v4, "UMC:ObjectTranslator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_1

    .line 918
    :cond_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 903
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static translateObject(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 8

    .prologue
    const/4 v6, 0x2

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 264
    .line 267
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Uri"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    if-eqz p1, :cond_0

    .line 272
    :try_start_0
    new-instance v0, Ljava/io/File;

    check-cast p1, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 275
    :cond_0
    const-class v1, Landroid/net/Uri;

    .line 276
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 844
    :cond_1
    :goto_0
    return-object v0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    throw v0

    .line 281
    :cond_2
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Byte[]"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 284
    if-eqz p1, :cond_58

    .line 285
    :try_start_1
    check-cast p1, Ljava/lang/String;

    .line 286
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v1, p1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    :try_start_2
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v4, v2, [B

    .line 288
    invoke-virtual {v1, v4}, Ljava/io/RandomAccessFile;->read([B)I

    .line 290
    array-length v2, v4

    new-array v2, v2, [Ljava/lang/Byte;

    .line 291
    :goto_1
    array-length v3, v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-lt v0, v3, :cond_3

    move-object v7, v1

    move-object v1, v4

    move-object v4, v7

    .line 297
    :goto_2
    :try_start_3
    const-class v2, [B

    .line 298
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 303
    if-eqz v4, :cond_1

    .line 304
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_0

    .line 293
    :cond_3
    :try_start_4
    aget-byte v3, v4, v0

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 291
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 299
    :catch_1
    move-exception v0

    .line 300
    :goto_3
    :try_start_5
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 302
    :catchall_0
    move-exception v0

    .line 303
    :goto_4
    if-eqz v4, :cond_4

    .line 304
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V

    .line 305
    :cond_4
    throw v0

    .line 306
    :cond_5
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "FileInputStream"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 308
    if-eqz p1, :cond_6

    .line 309
    :try_start_6
    check-cast p1, Ljava/lang/String;

    .line 310
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 314
    :try_start_7
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 315
    :try_start_8
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Total file size to read (in bytes) : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 316
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 315
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    :goto_5
    move-object v4, v0

    .line 322
    :cond_6
    :try_start_9
    const-class v1, Ljava/io/FileInputStream;

    .line 323
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_0

    .line 324
    :catch_2
    move-exception v0

    .line 325
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    throw v0

    .line 317
    :catch_3
    move-exception v0

    move-object v1, v0

    move-object v0, v4

    .line 318
    :goto_6
    :try_start_a
    const-string v2, "UMC:ObjectTranslator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_5

    .line 328
    :cond_7
    const-string v1, "WifiConfiguration"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "WifiConfigurationForUMC"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 334
    if-eqz p1, :cond_55

    .line 335
    :try_start_b
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_55

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WifiPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 336
    check-cast p1, Landroid/net/wifi/WifiConfiguration;

    .line 337
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;

    invoke-direct {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;-><init>(Landroid/net/wifi/WifiConfiguration;)V

    .line 338
    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;

    .line 339
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_0

    .line 342
    :catch_4
    move-exception v0

    .line 343
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    throw v0

    .line 347
    :cond_8
    const-string v1, "String[]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "List<String>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 348
    if-eqz p1, :cond_55

    .line 349
    check-cast p1, Ljava/util/ArrayList;

    .line 350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 351
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 353
    :cond_9
    const-string v1, "List<Geofence>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "List<Object>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 355
    if-eqz p1, :cond_55

    .line 356
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_55

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Geofencing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 357
    const-class v1, Ljava/util/List;

    .line 359
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 362
    :cond_a
    const-string v1, "Map<String, Object>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    const-string v1, "Geofence"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 364
    if-eqz p1, :cond_55

    .line 365
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_55

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Geofencing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 367
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 368
    const-string v0, "circulargeofence"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    const-string v0, "polygonalgeofence"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v0, "lineargeofence"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v5, "geofenceType"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 375
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 376
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - wrong value for GeofenceType"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 379
    :cond_b
    check-cast p1, Ljava/util/Map;

    .line 383
    const-string v1, "circulargeofence"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 386
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-eq v0, v6, :cond_c

    .line 387
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - wrong number of arguments for circular geofence"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 395
    :cond_c
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_d
    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_f

    .line 408
    new-instance v0, Landroid/app/enterprise/geofencing/CircularGeofence;

    invoke-direct {v0, v4, v2, v3}, Landroid/app/enterprise/geofencing/CircularGeofence;-><init>(Landroid/app/enterprise/geofencing/LatLongPoint;D)V

    move-object v4, v0

    .line 473
    :cond_e
    :goto_8
    const-class v1, Landroid/app/enterprise/geofencing/Geofence;

    .line 475
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 395
    :cond_f
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 396
    const-string v1, "radius"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 397
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 400
    :cond_10
    const-string v1, "center"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 401
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 403
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->extractLatLong(Ljava/util/Map;)Landroid/app/enterprise/geofencing/LatLongPoint;

    move-result-object v0

    move-object v4, v0

    goto :goto_7

    .line 410
    :cond_11
    const-string v1, "polygonalgeofence"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 413
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-eq v0, v6, :cond_12

    .line 414
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - wrong number of arguments for polygonal geofence"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :cond_12
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 422
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_13
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_14

    .line 438
    new-instance v4, Landroid/app/enterprise/geofencing/PolygonalGeofence;

    invoke-direct {v4, v5, v2, v3}, Landroid/app/enterprise/geofencing/PolygonalGeofence;-><init>(Ljava/util/List;D)V

    goto :goto_8

    .line 422
    :cond_14
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 423
    const-string v1, "graceDistance"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 424
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 427
    :cond_15
    const-string v1, "points"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 429
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 431
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->extractLatLong(Ljava/util/Map;)Landroid/app/enterprise/geofencing/LatLongPoint;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 440
    :cond_16
    const-string v1, "lineargeofence"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 443
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-eq v0, v6, :cond_17

    .line 444
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - wrong number of arguments for linear geofence"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :cond_17
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 452
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_18
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_19

    .line 468
    new-instance v4, Landroid/app/enterprise/geofencing/LinearGeofence;

    invoke-direct {v4, v5, v2, v3}, Landroid/app/enterprise/geofencing/LinearGeofence;-><init>(Ljava/util/List;D)V

    goto/16 :goto_8

    .line 452
    :cond_19
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 453
    const-string v1, "width"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 454
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 457
    :cond_1a
    const-string v1, "points"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 459
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 461
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->extractLatLong(Ljava/util/Map;)Landroid/app/enterprise/geofencing/LatLongPoint;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 479
    :cond_1b
    const-string v1, "String[]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    const-string v1, "Object"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 480
    if-eqz p1, :cond_55

    .line 481
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_55

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FirewallPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 483
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    const-string v1, "AddArgToJson"

    const-string v2, "firewallRuleType"

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isSpecialArgPresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 485
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 488
    const-string v0, "FirewallAllowRule"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    const-string v0, "FirewallDenyRule"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    const-string v0, "FirewallRerouteRule"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    const-string v0, "FirewallExceptionRule"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "firewallRuleType"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 495
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 496
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - incorrect value for firewallRuleType"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_1c
    check-cast p1, Ljava/util/ArrayList;

    .line 501
    const-string v1, "FirewallAllowRule"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 502
    new-instance v4, Landroid/app/enterprise/FirewallAllowRule;

    invoke-direct {v4}, Landroid/app/enterprise/FirewallAllowRule;-><init>()V

    .line 503
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_21

    .line 508
    :cond_1d
    const-string v1, "FirewallDenyRule"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 509
    new-instance v4, Landroid/app/enterprise/FirewallDenyRule;

    invoke-direct {v4}, Landroid/app/enterprise/FirewallDenyRule;-><init>()V

    .line 510
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_22

    .line 515
    :cond_1e
    const-string v1, "FirewallRerouteRule"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 516
    new-instance v4, Landroid/app/enterprise/FirewallRerouteRule;

    invoke-direct {v4}, Landroid/app/enterprise/FirewallRerouteRule;-><init>()V

    .line 517
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_23

    .line 522
    :cond_1f
    const-string v1, "FirewallExceptionRule"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 523
    new-instance v4, Landroid/app/enterprise/FirewallExceptionRule;

    invoke-direct {v4}, Landroid/app/enterprise/FirewallExceptionRule;-><init>()V

    .line 524
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_24

    .line 530
    :cond_20
    const-class v1, Ljava/lang/Object;

    .line 531
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 503
    :cond_21
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 504
    invoke-virtual {v4, v1}, Landroid/app/enterprise/FirewallAllowRule;->add(Ljava/lang/String;)V

    goto :goto_b

    .line 510
    :cond_22
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 511
    invoke-virtual {v4, v1}, Landroid/app/enterprise/FirewallDenyRule;->add(Ljava/lang/String;)V

    goto :goto_c

    .line 517
    :cond_23
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 518
    invoke-virtual {v4, v1}, Landroid/app/enterprise/FirewallRerouteRule;->add(Ljava/lang/String;)V

    goto :goto_d

    .line 524
    :cond_24
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 525
    invoke-virtual {v4, v0}, Landroid/app/enterprise/FirewallExceptionRule;->add(Ljava/lang/String;)V

    goto :goto_e

    .line 536
    :cond_25
    const-string v1, "Map<String, Object>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    const-string v1, "Bundle"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 537
    if-eqz p1, :cond_27

    .line 538
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_27

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SSOPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_26

    .line 539
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EnterpriseSSOPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 540
    :cond_26
    check-cast p1, Lcom/google/gson/internal/LinkedTreeMap;

    .line 541
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 542
    const-class v0, Landroid/os/Bundle;

    .line 543
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 547
    const-string v0, "SSO_CUSTOMER_LOGO"

    invoke-virtual {p1, v0}, Lcom/google/gson/internal/LinkedTreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 549
    if-nez v0, :cond_28

    .line 550
    const-string v0, "SSO_CUSTOMER_LOGO not supplied"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v4

    .line 558
    :goto_f
    const-string v0, "SSO_CUSTOMER_NAME"

    invoke-virtual {p1, v0}, Lcom/google/gson/internal/LinkedTreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 560
    if-nez v0, :cond_29

    .line 561
    const-string v0, "SSO_CUSTOMER_NAME not supplied"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v4

    .line 569
    :goto_10
    const-string v0, "SSO_IDP_CONF_DATA"

    invoke-virtual {p1, v0}, Lcom/google/gson/internal/LinkedTreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 571
    if-nez v0, :cond_2a

    .line 572
    const-string v0, "SSO_IDP_CONF_DATA not supplied"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, v4

    .line 580
    :goto_11
    const-string v0, "SSO_KRB_CONF_DATA"

    invoke-virtual {p1, v0}, Lcom/google/gson/internal/LinkedTreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 582
    if-nez v0, :cond_2b

    .line 583
    const-string v0, "SSO_KRB_CONF_DATA not supplied"

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    :goto_12
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 591
    const-string v0, "SSO_CUSTOMER_LOGO"

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 592
    const-string v0, "SSO_CUSTOMER_NAME"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v0, "SSO_IDP_CONF_DATA"

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 594
    const-string v0, "SSO_KRB_CONF_DATA"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    move-object v4, v5

    .line 601
    :cond_27
    const-class v1, Landroid/os/Bundle;

    .line 602
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 554
    :cond_28
    const-string v1, "Byte[]"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    check-cast v0, [B

    move-object v1, v0

    goto :goto_f

    .line 565
    :cond_29
    const-string v2, "String"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    goto :goto_10

    .line 576
    :cond_2a
    const-string v3, "Byte[]"

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    check-cast v0, [B

    move-object v3, v0

    goto :goto_11

    .line 587
    :cond_2b
    const-string v4, "Byte[]"

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    check-cast v0, [B

    move-object v4, v0

    goto :goto_12

    .line 597
    :cond_2c
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - Incomplete data supplied for SSO or EnterpriseSSO"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 603
    :cond_2d
    const-string v1, "Byte[]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_30

    const-string v1, "Bitmap"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 605
    if-eqz p1, :cond_2e

    .line 606
    :try_start_c
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2e

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiscPolicy"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 607
    check-cast p1, Ljava/util/ArrayList;

    .line 608
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [B

    move v1, v0

    .line 609
    :goto_13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2f

    .line 612
    const/4 v0, 0x0

    array-length v1, v2

    invoke-static {v2, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 615
    :cond_2e
    const-class v1, Landroid/graphics/Bitmap;

    .line 616
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_0

    .line 617
    :catch_5
    move-exception v0

    .line 618
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    throw v0

    .line 610
    :cond_2f
    :try_start_d
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->byteValue()B

    move-result v0

    aput-byte v0, v2, v1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    .line 609
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_13

    .line 621
    :cond_30
    const-string v1, "JsonObject"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_32

    const-string v1, "String"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 622
    if-eqz p1, :cond_31

    .line 623
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_31

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GenericVpnPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 624
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    invoke-virtual {v0, p1}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 627
    :cond_31
    const-class v1, Ljava/lang/String;

    .line 628
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 629
    :cond_32
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_34

    const-string v1, "JsonObject"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 630
    if-eqz p1, :cond_33

    .line 631
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_33

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GenericVpnPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 632
    check-cast p1, Ljava/lang/String;

    const-string v0, "\\"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 633
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    const-class v2, Lcom/google/gson/n;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/n;

    move-object v4, v0

    .line 636
    :cond_33
    const-class v1, Lcom/google/gson/n;

    .line 637
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 638
    :cond_34
    const-string v1, "List<String>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_37

    const-string v1, "List<JsonObject>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_37

    .line 639
    if-eqz p1, :cond_35

    .line 640
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_35

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GenericVpnPolicy"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 641
    check-cast p1, Ljava/util/ArrayList;

    .line 642
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 643
    if-eqz p1, :cond_35

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_35

    move v1, v0

    .line 644
    :goto_14
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_36

    .line 653
    :cond_35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 654
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 646
    :cond_36
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "\\"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 647
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    const-class v3, Lcom/google/gson/n;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/n;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    .line 655
    :cond_37
    const-string v1, "JSONArray"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    const-string v1, "List<String>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 656
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 657
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 658
    :cond_38
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3a

    const-string v1, "JSONArray"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 659
    if-eqz p1, :cond_39

    .line 660
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_39

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EnterpriseLicenseManager"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 661
    new-instance v4, Lorg/json/JSONArray;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 664
    :cond_39
    const-class v1, Lorg/json/JSONArray;

    .line 665
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 666
    :cond_3a
    const-string v1, "Date"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3c

    const-string v1, "String"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 667
    if-eqz p1, :cond_3b

    .line 669
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3b

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DateTimePolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 670
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 673
    :cond_3b
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 674
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 675
    :cond_3c
    const-string v1, "CertificateInfo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3e

    const-string v1, "String"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 676
    if-eqz p1, :cond_3d

    .line 677
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3d

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GenericVpnPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 679
    check-cast p1, Landroid/app/enterprise/CertificateInfo;

    .line 680
    invoke-virtual {p1}, Landroid/app/enterprise/CertificateInfo;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 685
    :try_start_e
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->convertToPem(Ljava/lang/Object;)[B

    move-result-object v1

    .line 686
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6

    .line 692
    :goto_15
    if-eqz v0, :cond_3d

    .line 693
    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 694
    const-string v1, "\r"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 699
    :cond_3d
    const-class v1, Ljava/lang/String;

    .line 700
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 687
    :catch_6
    move-exception v0

    .line 689
    const-string v1, "UMC:ObjectTranslator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    goto :goto_15

    .line 703
    :cond_3e
    const-string v1, "List<CertificateInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_41

    const-string v1, "List<String>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 704
    if-eqz p1, :cond_3f

    .line 705
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3f

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SecurityPolicy"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 706
    check-cast p1, Ljava/util/ArrayList;

    .line 707
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 708
    if-eqz p1, :cond_57

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_57

    .line 709
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 710
    :goto_16
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_40

    .line 716
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getListFromX509CertList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_17
    move-object v4, v0

    .line 722
    :cond_3f
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 711
    :cond_40
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    .line 713
    invoke-virtual {v0}, Landroid/app/enterprise/CertificateInfo;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 714
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 710
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 723
    :cond_41
    const-string v1, "List<String>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_43

    const-string v1, "List<X509Certificate>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 725
    if-eqz p1, :cond_42

    .line 726
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_42

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CertificatePolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 727
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getX509CertList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 731
    :cond_42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 732
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 734
    :cond_43
    const-string v1, "List<CertificateControlInfo>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    const-string v1, "List<CertificateControlInfoForUMC>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 736
    if-eqz p1, :cond_44

    .line 737
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_44

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CertificatePolicy"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_44

    .line 738
    check-cast p1, Ljava/util/ArrayList;

    .line 739
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 740
    if-eqz p1, :cond_44

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_44

    move v2, v0

    .line 741
    :goto_18
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_45

    .line 755
    :cond_44
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 742
    :cond_45
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;

    .line 743
    iget-object v1, v0, Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;->entries:Ljava/util/List;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getListFromX509CertList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 746
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;-><init>()V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 748
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;

    invoke-virtual {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->setEntries(Ljava/util/List;)V

    .line 749
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;

    iget-object v0, v0, Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;->adminPackageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->setAdminPackageName(Ljava/lang/String;)V

    .line 741
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_18

    .line 756
    :cond_46
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_48

    const-string v1, "ParcelFileDescriptor"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_48

    .line 757
    if-eqz p1, :cond_47

    .line 758
    new-instance v0, Ljava/io/File;

    check-cast p1, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 759
    const/high16 v1, 0x3c000000    # 0.0078125f

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 762
    :cond_47
    const-class v1, Landroid/os/ParcelFileDescriptor;

    .line 763
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 764
    :cond_48
    const-string v1, "String"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4a

    const-string v1, "CertificateInfo"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 765
    if-eqz p1, :cond_49

    .line 766
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_49

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SecurityPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 771
    const-string v0, "UMC:ObjectTranslator"

    const-string v1, "Object translator -- String to certificateInfo for securityPolicy"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    check-cast p1, Ljava/util/Map;

    .line 773
    const-string v0, "mCertificate"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 775
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/a;->aB(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 776
    new-instance v4, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v4, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    .line 780
    const-string v0, "mCertificate"

    invoke-interface {p1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    :cond_49
    const-class v1, Landroid/app/enterprise/CertificateInfo;

    .line 786
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 789
    :cond_4a
    const-string v1, "Map<String, Object>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4f

    const-string v1, "KnoxConfigurationType"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4f

    .line 790
    if-eqz p1, :cond_55

    .line 791
    check-cast p1, Ljava/util/Map;

    .line 793
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4b

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_4c

    .line 794
    :cond_4b
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - wrong value for KnoxConfigurationType"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 796
    :cond_4c
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_56

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 798
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 799
    const-string v2, "KnoxConfigurationType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 800
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 801
    const-class v4, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 811
    :goto_19
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v1, v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 802
    :cond_4d
    const-string v2, "LightweightConfigurationType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 803
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 804
    const-class v4, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    goto :goto_19

    .line 805
    :cond_4e
    const-string v2, "ContainerModeConfigurationType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 806
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 807
    const-class v4, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    goto :goto_19

    .line 814
    :cond_4f
    const-string v1, "KnoxConfigurationType"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_51

    const-string v1, "Map<String, Object>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_51

    .line 815
    if-eqz p1, :cond_55

    .line 817
    check-cast p1, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getKnoxConfigurationTypeMap(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)Ljava/util/Map;

    move-result-object v1

    .line 818
    if-nez v1, :cond_50

    .line 819
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - cannot translate from KnoxConfigurationType to Map"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 821
    :cond_50
    const-class v2, Ljava/util/HashMap;

    .line 822
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 825
    :cond_51
    const-string v1, "List<KnoxConfigurationType>"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_54

    const-string v1, "List<Map<String, Object>>"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 826
    if-eqz p1, :cond_55

    .line 827
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 829
    check-cast p1, Ljava/util/List;

    move v1, v0

    .line 830
    :goto_1a
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_52

    .line 839
    const-class v1, Ljava/util/List;

    .line 840
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 831
    :cond_52
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->getKnoxConfigurationTypeMap(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)Ljava/util/Map;

    move-result-object v0

    .line 833
    if-nez v0, :cond_53

    .line 834
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - cannot translate from (List)KnoxConfigurationType to (List)Map"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 836
    :cond_53
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 830
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    .line 843
    :cond_54
    invoke-virtual {p0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 844
    invoke-static {p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    goto/16 :goto_0

    .line 847
    :cond_55
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - unsupported translation"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :catch_7
    move-exception v1

    goto/16 :goto_6

    .line 302
    :catchall_1
    move-exception v0

    move-object v4, v1

    goto/16 :goto_4

    .line 299
    :catch_8
    move-exception v0

    move-object v4, v1

    goto/16 :goto_3

    :cond_56
    move-object v0, v4

    goto/16 :goto_19

    :cond_57
    move-object v0, v1

    goto/16 :goto_17

    :cond_58
    move-object v1, v4

    goto/16 :goto_2
.end method
