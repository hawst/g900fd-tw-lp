.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;
.super Ljava/lang/Object;
.source "Core.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->s(Ljava/lang/Object;)V
.end annotation


# instance fields
.field final synthetic pC:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;

.field private final synthetic pD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

.field private final synthetic pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;->pC:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;

    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;->pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;->pD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aE(I)V
    .locals 3

    .prologue
    .line 245
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ByodSetupStarter:onResult : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    if-eqz p1, :cond_0

    .line 247
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;->pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;->pD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    .line 252
    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 253
    return-void
.end method
