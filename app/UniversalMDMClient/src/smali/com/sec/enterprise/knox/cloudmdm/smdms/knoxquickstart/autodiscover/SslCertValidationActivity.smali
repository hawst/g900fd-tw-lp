.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;
.super Landroid/app/Activity;
.source "SslCertValidationActivity.java"


# static fields
.field private static ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

.field private static vg:Landroid/os/ConditionVariable;

.field public static vh:Z


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mUrl:Ljava/lang/String;

.field private tl:Landroid/net/http/SslCertificate;

.field private tm:Landroid/app/AlertDialog;

.field private vf:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    .line 48
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vg:Landroid/os/ConditionVariable;

    .line 49
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vh:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tl:Landroid/net/http/SslCertificate;

    .line 44
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tm:Landroid/app/AlertDialog;

    .line 45
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 35
    return-void
.end method

.method private a(Landroid/net/http/SslCertificate;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 250
    if-nez p1, :cond_0

    .line 251
    const-string v1, "SSLCertValidationActivity"

    const-string v2, "inflateCertificateView(): null certificate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :goto_0
    return-object v0

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03002a

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 258
    invoke-virtual {p1}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v2

    .line 259
    if-eqz v2, :cond_1

    .line 260
    const v0, 0x7f090094

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    const v0, 0x7f090096

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    const v0, 0x7f090098

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 263
    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    :cond_1
    invoke-virtual {p1}, Landroid/net/http/SslCertificate;->getIssuedBy()Landroid/net/http/SslCertificate$DName;

    move-result-object v2

    .line 268
    if-eqz v2, :cond_2

    .line 269
    const v0, 0x7f09009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const v0, 0x7f09009c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    const v0, 0x7f09009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    invoke-virtual {v2}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    :cond_2
    invoke-virtual {p1}, Landroid/net/http/SslCertificate;->getValidNotBeforeDate()Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 277
    const v0, 0x7f0900a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    invoke-virtual {p1}, Landroid/net/http/SslCertificate;->getValidNotAfterDate()Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 281
    const v0, 0x7f0900a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 283
    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    if-nez p1, :cond_1

    .line 294
    const-string v0, ""

    .line 300
    :cond_0
    :goto_0
    return-object v0

    .line 296
    :cond_1
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 297
    if-nez v0, :cond_0

    .line 298
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/security/cert/X509Certificate;ILandroid/os/ConditionVariable;)V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 66
    const-string v2, "certificate"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 67
    sput-object p4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vg:Landroid/os/ConditionVariable;

    .line 68
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 69
    const-string v1, "sslErrorType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 70
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 72
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tm:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tm:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hE()V

    return-void
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hD()V

    return-void
.end method

.method public static hC()Z
    .locals 1

    .prologue
    .line 58
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vh:Z

    return v0
.end method

.method private hD()V
    .locals 8

    .prologue
    const/4 v3, 0x3

    const/4 v7, 0x1

    const v6, 0x7f0900a4

    const/4 v5, 0x0

    const v4, 0x7f03002b

    .line 106
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showSSLCertificateOnError() url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tl:Landroid/net/http/SslCertificate;

    invoke-virtual {v0}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    .line 111
    iput v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    .line 112
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 113
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 117
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 118
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 119
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    if-ne v0, v3, :cond_0

    .line 120
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "showSSLCertificateOnError(): UNTRUSTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 122
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f080025

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 123
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 126
    :cond_0
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 127
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "showSSLCertificateOnError(): MISMATCH"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 129
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f080026

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 130
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 133
    :cond_1
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    if-ne v0, v7, :cond_2

    .line 134
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "showSSLCertificateOnError(): EXPIRED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 136
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f080027

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 137
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 140
    :cond_2
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    if-nez v0, :cond_3

    .line 141
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "showSSLCertificateOnError(): NOTYETVALID"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 143
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f080028

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 144
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 148
    :cond_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a0012

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 149
    const v1, 0x7f0800b1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 150
    const v1, 0x7f080056

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tl:Landroid/net/http/SslCertificate;

    if-eqz v1, :cond_4

    .line 175
    const v1, 0x7f080029

    .line 176
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V

    .line 175
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 192
    :cond_4
    const v1, 0x7f080053

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 208
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 191
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tm:Landroid/app/AlertDialog;

    .line 209
    return-void
.end method

.method private hE()V
    .locals 3

    .prologue
    .line 217
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "showCertificate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tl:Landroid/net/http/SslCertificate;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Landroid/net/http/SslCertificate;)Landroid/view/View;

    move-result-object v0

    .line 220
    if-nez v0, :cond_0

    .line 221
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "showCertificate(): null certificate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :goto_0
    return-void

    .line 225
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0a0012

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f08002a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 226
    const v2, 0x7f02001f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 227
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 228
    const v1, 0x7f080058

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$4;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 232
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$5;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public static hF()V
    .locals 3

    .prologue
    .line 318
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    if-eqz v0, :cond_0

    .line 319
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Close Activity from Exchange url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 320
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 319
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->finish()V

    .line 322
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    .line 324
    :cond_0
    return-void
.end method

.method static synthetic hG()Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vg:Landroid/os/ConditionVariable;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hF()V

    .line 79
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->requestWindowFeature(I)Z

    .line 81
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    .line 83
    :try_start_0
    new-instance v1, Landroid/net/http/SslCertificate;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 84
    const-string v2, "certificate"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-direct {v1, v0}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    .line 83
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->tl:Landroid/net/http/SslCertificate;

    .line 86
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sslErrorType"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vf:I

    .line 87
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mUrl:Ljava/lang/String;

    .line 88
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hD()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "Certificate was null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hF()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 328
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " OnDestroy  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->ve:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    .line 330
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 331
    return-void
.end method
