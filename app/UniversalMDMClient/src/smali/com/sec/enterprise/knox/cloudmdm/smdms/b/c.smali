.class public abstract Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;
.super Ljava/lang/Object;
.source "Logger.java"


# instance fields
.field protected level:I

.field protected loggerName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->level:I

    .line 57
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->loggerName:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public isLoggable(I)Z
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->level:I

    if-gt v0, p1, :cond_0

    .line 86
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract log(ILjava/lang/String;Ljava/lang/String;)V
.end method
