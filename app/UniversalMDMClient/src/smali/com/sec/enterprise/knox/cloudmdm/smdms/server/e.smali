.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;
.super Lcom/a/a/a/d;
.source "AsyncResponseHandler.java"


# static fields
.field private static final ry:Ljava/lang/String;


# instance fields
.field private vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field private vz:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->ry:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/a/a/a/d;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/a/a/a/d;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 33
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vz:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;

    .line 35
    return-void
.end method

.method private e(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processResponseAndNotifyObservers:<MESSAGE> "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</MESSAGE> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    if-nez p2, :cond_1

    const/16 v0, 0xcc

    if-ne p1, v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vz:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->aX(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    if-eqz p2, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vz:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->aX(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 55
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->onFinish()V

    .line 56
    const/4 v0, -0x1

    .line 57
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onFailure in ResponseHandler:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error from async client: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    instance-of v1, p1, Lorg/apache/http/conn/ConnectTimeoutException;

    if-eqz v1, :cond_0

    .line 62
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->ry:Ljava/lang/String;

    const-string v2, "onFailure:Connection timeout !"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    instance-of v1, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_1

    .line 66
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpResponseException !"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    .line 67
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    .line 68
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    .line 72
    :cond_1
    if-eqz p2, :cond_2

    const-string v1, ""

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 73
    :cond_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->ry:Ljava/lang/String;

    const-string v2, "Server did not return any response"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vz:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;

    if-eqz v1, :cond_3

    .line 79
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vz:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;->wS:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V

    .line 81
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/a/a/a/d;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 83
    return-void

    .line 75
    :cond_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Server message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess in AsyncResponseHandler:code:msg::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->e(ILjava/lang/String;)V

    .line 50
    invoke-super {p0, p1, p2}, Lcom/a/a/a/d;->c(ILjava/lang/String;)V

    .line 51
    return-void
.end method

.method public onFinish()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onFinish in ResponseHandler"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-super {p0}, Lcom/a/a/a/d;->onFinish()V

    .line 99
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->vy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStart in ResponseHandler"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-super {p0}, Lcom/a/a/a/d;->onStart()V

    .line 44
    return-void
.end method
