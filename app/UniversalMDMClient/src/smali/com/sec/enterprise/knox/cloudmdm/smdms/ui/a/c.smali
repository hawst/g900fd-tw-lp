.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;
.super Ljava/lang/Object;
.source "MsgBox.java"


# static fields
.field private static zN:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;

.field private static zR:I


# instance fields
.field private TAG:Ljava/lang/String;

.field private activity:Landroid/app/Activity;

.field private message:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field zL:Landroid/app/AlertDialog$Builder;

.field private zM:Z

.field private zO:Ljava/lang/String;

.field private zP:Ljava/lang/String;

.field private zQ:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, "UMC:MsgBox"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->TAG:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zM:Z

    .line 30
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->activity:Landroid/app/Activity;

    .line 31
    sput-object p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zN:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;

    .line 32
    sput p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zR:I

    .line 33
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    .line 34
    return-void
.end method

.method static synthetic jJ()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zN:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;

    return-object v0
.end method

.method static synthetic jK()I
    .locals 1

    .prologue
    .line 26
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zR:I

    return v0
.end method


# virtual methods
.method public bp(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zO:Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public bq(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->title:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public br(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->message:Ljava/lang/String;

    .line 53
    return-object p0
.end method

.method public show()V
    .locals 3

    .prologue
    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->activity:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->TAG:Ljava/lang/String;

    .line 71
    const-string v1, "No context to call this on, can only call this from CloudActivity"

    .line 70
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 75
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 78
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zO:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zO:Ljava/lang/String;

    .line 80
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c$1;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;)V

    .line 79
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zP:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zP:Ljava/lang/String;

    .line 93
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c$2;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;)V

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 102
    :cond_2
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zM:Z

    if-eqz v0, :cond_3

    .line 103
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->activity:Landroid/app/Activity;

    const v1, 0x7f030002

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 105
    const v0, 0x7f090003

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 104
    check-cast v0, Landroid/widget/CheckBox;

    .line 106
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c$3;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 115
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zQ:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->zL:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->TAG:Ljava/lang/String;

    const-string v2, "Showing the window failed"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
