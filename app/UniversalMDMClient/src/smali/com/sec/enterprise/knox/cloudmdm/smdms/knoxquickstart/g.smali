.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;
.super Landroid/content/BroadcastReceiver;
.source "KnoxQuickStartManager.java"


# instance fields
.field final synthetic sA:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V
    .locals 0

    .prologue
    .line 794
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;->sA:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 798
    if-nez p2, :cond_1

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 801
    :cond_1
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CoreLocalReceiver:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    if-ne v0, v1, :cond_0

    .line 804
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;->sA:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V

    .line 806
    const-string v0, "status"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 808
    if-eqz v1, :cond_3

    .line 809
    const v0, 0x7f08007b

    .line 810
    const/4 v2, -0x2

    if-ne v1, v2, :cond_2

    .line 811
    const v0, 0x7f08007c

    .line 816
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    const v2, 0x7f080099

    .line 817
    const v3, 0x7f080059

    const v4, 0x7f080053

    .line 816
    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->b(IIII)V

    goto :goto_0

    .line 820
    :cond_3
    const-string v0, "packageName"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 821
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/g;->sA:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;Ljava/lang/String;)V

    .line 822
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->q(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 823
    const v1, 0x7f0800ba

    .line 824
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 825
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
