.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils$InRange;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static check([Ljava/lang/Comparable;Ljava/lang/Comparable;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>([TT;TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 272
    array-length v2, p0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 273
    aget-object v2, p0, v1

    if-eqz v2, :cond_2

    aget-object v2, p0, v0

    if-eqz v2, :cond_2

    .line 274
    aget-object v2, p0, v1

    invoke-interface {p1, v2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_2

    aget-object v2, p0, v0

    invoke-interface {p1, v2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-gtz v2, :cond_2

    .line 284
    :cond_0
    :goto_0
    return v0

    .line 278
    :cond_1
    array-length v2, p0

    if-ne v2, v0, :cond_2

    .line 279
    aget-object v2, p0, v1

    invoke-interface {p1, v2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 284
    goto :goto_0
.end method
