.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;
.super Ljava/lang/Thread;
.source "AutoDiscoverHandler.java"


# instance fields
.field private mEmailAddress:Ljava/lang/String;

.field private mServerAddress:Ljava/lang/String;

.field private tC:Ljava/lang/String;

.field private tD:Ljava/lang/String;

.field private final uR:I

.field private uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

.field private uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

.field private uU:Lorg/apache/http/client/methods/HttpPost;

.field private uV:Lorg/apache/http/client/methods/HttpGet;

.field final synthetic uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 278
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 242
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tC:Ljava/lang/String;

    .line 244
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    .line 245
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    .line 248
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->mEmailAddress:Ljava/lang/String;

    .line 280
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uR:I

    .line 282
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tC:Ljava/lang/String;

    .line 283
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tD:Ljava/lang/String;

    .line 288
    new-instance v1, Ljava/lang/String;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 289
    new-instance v2, Ljava/lang/String;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 291
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 292
    new-instance v0, Ljava/lang/String;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 294
    :cond_0
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-direct {v3, v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    .line 295
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->init()V

    .line 297
    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 300
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    .line 299
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 242
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tC:Ljava/lang/String;

    .line 244
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    .line 245
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    .line 248
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->mEmailAddress:Ljava/lang/String;

    .line 301
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uR:I

    .line 302
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tC:Ljava/lang/String;

    .line 303
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->mServerAddress:Ljava/lang/String;

    .line 308
    new-instance v1, Ljava/lang/String;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 309
    new-instance v2, Ljava/lang/String;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 311
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 312
    new-instance v0, Ljava/lang/String;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 314
    :cond_0
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-direct {v3, v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    .line 315
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->init()V

    .line 317
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpPost;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V
    .locals 8

    .prologue
    const/16 v7, 0x1bb

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 407
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hy()I

    move-result v1

    invoke-static {v0, v1, v2, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;IZI)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    .line 412
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    const-string v1, "<?xml version=\'1.0\' encoding=\'UTF-8\' standalone=\'no\' ?><Autodiscover xmlns=\"http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006\"><Request><EMailAddress>%s</EMailAddress><AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006</AcceptableResponseSchema></Request></Autodiscover>"

    .line 416
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 417
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->mEmailAddress:Ljava/lang/String;

    .line 420
    :cond_2
    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->mEmailAddress:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 422
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v3, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    .line 424
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "Authorization"

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tC:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "MS-ASProtocolVersion"

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "Connection"

    const-string v5, "keep-alive"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "User-Agent"

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "Content-Type"

    const-string v5, "text/xml"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    const-string v4, "Accept"

    const-string v5, "text/xml, text/html"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    new-instance v4, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v4, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 432
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g$1;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 443
    :try_start_0
    const-string v1, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending HttpPost to URL : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 445
    const-string v0, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending HttpPost successfull : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljavax/net/ssl/SSLProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 484
    :goto_1
    if-eqz v1, :cond_0

    .line 485
    const-string v0, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Parsing response : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    .line 486
    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Lorg/apache/http/HttpResponse;ZLcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Ljava/lang/String;)V

    .line 487
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Parsing response successful: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 490
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpPost aborted forcefully: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 446
    :catch_0
    move-exception v0

    .line 447
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending HttpPost SSLProtocolException : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 449
    :catch_1
    move-exception v0

    .line 450
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Sending HttpPost got SSLError, trying to verifying ssl cert : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 451
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 450
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    monitor-enter v1

    .line 453
    :try_start_1
    const-string v0, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got the lock, verifying SSL Certificate : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 473
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Sending HttpPost ignoring SSL : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hy()I

    move-result v1

    invoke-static {v0, v1, v6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;IZI)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    .line 475
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uU:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 476
    const-string v0, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending HttpPost ignoring SSL successful : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 452
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 478
    :catch_2
    move-exception v0

    .line 479
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending HttpPost IOException : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(Lorg/apache/http/HttpResponse;ZLcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 515
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 519
    :cond_1
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 521
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_2

    .line 522
    invoke-static {p1, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->b(Lorg/apache/http/HttpResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lorg/apache/http/client/methods/HttpPost;

    goto :goto_0

    .line 530
    :cond_2
    const/16 v1, 0x1c3

    if-ne v0, v1, :cond_3

    .line 532
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    new-array v1, v2, [Ljava/lang/String;

    .line 536
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Posting autodiscover to redirect: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    .line 540
    invoke-direct {p0, v0, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V

    goto :goto_0

    .line 548
    :cond_3
    const/16 v1, 0x12e

    if-eq v0, v1, :cond_4

    .line 550
    const/16 v1, 0x12d

    if-ne v0, v1, :cond_9

    .line 552
    :cond_4
    const-string v0, "Location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 553
    const/4 v0, 0x0

    .line 554
    if-eqz v1, :cond_5

    .line 555
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 558
    :cond_5
    if-nez v0, :cond_6

    .line 560
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No location header"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 562
    :cond_6
    invoke-virtual {v0, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 564
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Same location from prev"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 568
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Prefer to html page"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_8
    invoke-direct {p0, v0, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V

    goto :goto_0

    .line 581
    :cond_9
    const/16 v1, 0x191

    if-ne v0, v1, :cond_b

    .line 583
    if-eqz p2, :cond_a

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 587
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 589
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 592
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tC:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 594
    const-string v1, "401 received; trying username: "

    aput-object v1, v0, v4

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    .line 599
    invoke-direct {p0, p5, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V

    goto/16 :goto_0

    .line 605
    :cond_a
    new-instance v0, Lcom/android/emailcommon/mail/MessagingException;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v0

    .line 609
    :cond_b
    new-array v1, v2, [Ljava/lang/String;

    .line 613
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", throwing IOException"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    .line 615
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpGet;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method

.method private b(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V
    .locals 4

    .prologue
    .line 631
    const-string v0, "/autodiscover/autodiscover.xml"

    .line 633
    :try_start_0
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending httpGet : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://autodiscover."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 635
    const-string v2, "/autodiscover/autodiscover.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 634
    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    .line 636
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-static {v0}, Lcom/android/exchange/a/a;->a(Lorg/apache/http/client/methods/HttpGet;)Ljava/net/URI;

    move-result-object v0

    .line 637
    if-eqz v0, :cond_1

    .line 638
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 639
    const-string v1, "AutoDiscoverHandler"

    .line 640
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending httpGet successfull, trying autodiscover with httpPost on new redirected URL : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 641
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 640
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 639
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    invoke-direct {p0, v0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 657
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 659
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpGet aborted forcefully: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :cond_0
    :goto_1
    return-void

    .line 644
    :cond_1
    :try_start_1
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 645
    const/4 v2, 0x1

    .line 644
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 647
    :catch_0
    move-exception v0

    .line 648
    :try_start_2
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 649
    const/4 v2, 0x1

    .line 648
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 657
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 659
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpGet aborted forcefully: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 650
    :catch_1
    move-exception v0

    .line 651
    :try_start_3
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 652
    const/4 v2, 0x5

    .line 651
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 657
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 659
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpGet aborted forcefully: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 653
    :catch_2
    move-exception v0

    .line 654
    :try_start_4
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 655
    const/4 v2, 0x0

    .line 654
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 657
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 659
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpGet aborted forcefully: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 656
    :catchall_0
    move-exception v0

    .line 657
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 658
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uV:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 659
    const-string v1, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpGet aborted forcefully: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    :cond_2
    throw v0
.end method

.method private hA()Z
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 331
    const-string v1, "autodiscover_host_auth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 332
    const/4 v0, 0x1

    .line 334
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ht()Z
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 275
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    .line 276
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 343
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Running: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :try_start_0
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uR:I

    if-ne v0, v3, :cond_2

    .line 346
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tD:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_c

    .line 364
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->hA()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Landroid/os/Bundle;)V

    .line 369
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    .line 364
    :cond_1
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_10

    .line 391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 392
    :try_start_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 393
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed from autodiscoverThreads "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_11

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 398
    :try_start_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 397
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_12

    .line 402
    :goto_2
    return-void

    .line 347
    :cond_2
    :try_start_4
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uR:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 348
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->mServerAddress:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->b(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_c

    goto/16 :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 354
    :try_start_5
    const-string v1, "AutoDiscoverHandler"

    const-string v2, "AutoDiscoverHandler: IOException"

    invoke-static {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 356
    const/4 v2, 0x1

    .line 355
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_c

    .line 364
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_6
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->hA()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 368
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Landroid/os/Bundle;)V

    .line 369
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    .line 371
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    .line 364
    :cond_4
    :goto_3
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 392
    :try_start_7
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 393
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed from autodiscoverThreads "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 398
    :try_start_8
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 397
    monitor-exit v1

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v0

    .line 364
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_9
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->hA()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 368
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Landroid/os/Bundle;)V

    .line 369
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    .line 371
    :cond_6
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    .line 364
    :cond_7
    :goto_4
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 392
    :try_start_a
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 393
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed from autodiscoverThreads "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 398
    :try_start_b
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 397
    monitor-exit v1

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v0

    .line 372
    :cond_8
    :try_start_c
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->ht()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 374
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    goto :goto_4

    .line 364
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v0

    .line 379
    :cond_9
    :try_start_d
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 381
    const-string v2, "autodiscover_error_code"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 380
    if-eq v0, v4, :cond_7

    .line 382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 383
    const-string v2, "autodiscover_error_code"

    .line 384
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v3

    .line 385
    const-string v4, "autodiscover_error_code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 383
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto/16 :goto_4

    .line 391
    :catchall_3
    move-exception v0

    :try_start_e
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    throw v0

    .line 372
    :cond_a
    :try_start_f
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->ht()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 374
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    goto/16 :goto_3

    .line 364
    :catchall_4
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    throw v0

    .line 379
    :cond_b
    :try_start_10
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 381
    const-string v2, "autodiscover_error_code"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 380
    if-eq v0, v4, :cond_4

    .line 382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 383
    const-string v2, "autodiscover_error_code"

    .line 384
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v3

    .line 385
    const-string v4, "autodiscover_error_code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 383
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    goto/16 :goto_3

    .line 391
    :catchall_5
    move-exception v0

    :try_start_11
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    throw v0

    .line 357
    :catch_1
    move-exception v0

    .line 358
    :try_start_12
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 359
    const/4 v2, 0x5

    .line 358
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_c

    .line 364
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_13
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->hA()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 368
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Landroid/os/Bundle;)V

    .line 369
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    .line 371
    :cond_c
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    .line 364
    :cond_d
    :goto_5
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    .line 391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 392
    :try_start_14
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 393
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed from autodiscoverThreads "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 398
    :try_start_15
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 397
    monitor-exit v1

    goto/16 :goto_2

    :catchall_6
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    throw v0

    .line 372
    :cond_e
    :try_start_16
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->ht()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 374
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    goto :goto_5

    .line 364
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_7

    throw v0

    .line 379
    :cond_f
    :try_start_17
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 381
    const-string v2, "autodiscover_error_code"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 380
    if-eq v0, v4, :cond_d

    .line 382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 383
    const-string v2, "autodiscover_error_code"

    .line 384
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v3

    .line 385
    const-string v4, "autodiscover_error_code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 383
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    goto/16 :goto_5

    .line 391
    :catchall_8
    move-exception v0

    :try_start_18
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_8

    throw v0

    .line 360
    :catch_2
    move-exception v0

    .line 361
    :try_start_19
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_error_code"

    .line 362
    const/4 v2, 0x0

    .line 361
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    .line 364
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_1a
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->hA()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 368
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Landroid/os/Bundle;)V

    .line 369
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    .line 371
    :cond_10
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    .line 364
    :cond_11
    :goto_6
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_a

    .line 391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 392
    :try_start_1b
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 393
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removed from autodiscoverThreads "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_b

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 398
    :try_start_1c
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 397
    monitor-exit v1

    goto/16 :goto_2

    :catchall_9
    move-exception v0

    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_9

    throw v0

    .line 372
    :cond_12
    :try_start_1d
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->ht()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 374
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    goto :goto_6

    .line 364
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_a

    throw v0

    .line 379
    :cond_13
    :try_start_1e
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 381
    const-string v2, "autodiscover_error_code"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 380
    if-eq v0, v4, :cond_11

    .line 382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 383
    const-string v2, "autodiscover_error_code"

    .line 384
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v3

    .line 385
    const-string v4, "autodiscover_error_code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 383
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_a

    goto/16 :goto_6

    .line 391
    :catchall_b
    move-exception v0

    :try_start_1f
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_b

    throw v0

    .line 363
    :catchall_c
    move-exception v0

    .line 364
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_20
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->hA()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 366
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 367
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 368
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Landroid/os/Bundle;)V

    .line 369
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    .line 371
    :cond_14
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    .line 364
    :cond_15
    :goto_7
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_d

    .line 391
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 392
    :try_start_21
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 393
    const-string v2, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "removed from autodiscoverThreads "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_e

    .line 397
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v1

    monitor-enter v1

    .line 398
    :try_start_22
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 397
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_f

    .line 401
    throw v0

    .line 372
    :cond_16
    :try_start_23
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->ht()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 373
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 374
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 375
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 376
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    goto :goto_7

    .line 364
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_d

    throw v0

    .line 379
    :cond_17
    :try_start_24
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 380
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    .line 381
    const-string v3, "autodiscover_error_code"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 380
    if-eq v2, v4, :cond_15

    .line 382
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v2

    .line 383
    const-string v3, "autodiscover_error_code"

    .line 384
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v4

    .line 385
    const-string v5, "autodiscover_error_code"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 383
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_d

    goto/16 :goto_7

    .line 391
    :catchall_e
    move-exception v0

    :try_start_25
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_e

    throw v0

    .line 397
    :catchall_f
    move-exception v0

    :try_start_26
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_f

    throw v0

    .line 372
    :cond_18
    :try_start_27
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->ht()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V

    .line 374
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V

    goto/16 :goto_1

    .line 364
    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_10

    throw v0

    .line 379
    :cond_19
    :try_start_28
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 381
    const-string v2, "autodiscover_error_code"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 380
    if-eq v0, v4, :cond_1

    .line 382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 383
    const-string v2, "autodiscover_error_code"

    .line 384
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v3

    .line 385
    const-string v4, "autodiscover_error_code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 383
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_10

    goto/16 :goto_1

    .line 391
    :catchall_11
    move-exception v0

    :try_start_29
    monitor-exit v1
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_11

    throw v0

    .line 397
    :catchall_12
    move-exception v0

    :try_start_2a
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_12

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 255
    const-string v1, "  AutoDiscoverThread [ Context "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 256
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    if-eqz v1, :cond_1

    .line 262
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tD:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 265
    const-string v1, " url "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->tD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_2
    const-string v1, " execution step "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->uR:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 269
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
