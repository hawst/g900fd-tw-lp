.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;
.super Ljava/lang/Object;
.source "Authentication.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonInclude;
    value = .enum Lcom/fasterxml/jackson/annotation/JsonInclude$Include;->NON_NULL:Lcom/fasterxml/jackson/annotation/JsonInclude$Include;
.end annotation


# instance fields
.field private fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication$Fields;",
            ">;"
        }
    .end annotation
.end field

.field private scheme:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public getFields()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication$Fields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;->fields:Ljava/util/List;

    return-object v0
.end method

.method public getScheme()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;->scheme:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    return-object v0
.end method

.method public setFields(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication$Fields;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;->fields:Ljava/util/List;

    .line 76
    return-void
.end method

.method public setScheme(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;->scheme:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    .line 68
    return-void
.end method
