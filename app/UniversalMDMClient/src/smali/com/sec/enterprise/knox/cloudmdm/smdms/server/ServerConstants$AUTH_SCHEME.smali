.class public final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;
.super Ljava/lang/Enum;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum wZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field public static final enum xa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field public static final enum xb:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field public static final enum xc:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field public static final enum xd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field private static final synthetic xe:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    const-string v1, "NO_AUTH"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->wZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    const-string v1, "BASIC"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    const-string v1, "TOKEN"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xb:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    const-string v1, "MDM"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xc:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    const-string v1, "SAML"

    invoke-direct {v0, v1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->wZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xb:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xc:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xe:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xe:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
