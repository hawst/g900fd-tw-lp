.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "SecurityPolicyMethodInvoker.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:SecurityPolicyMethodInvoker"


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected invoke()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 55
    const-string v0, "UMC:SecurityPolicyMethodInvoker"

    const-string v1, "Inside invoke()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mPreparedForInvoke:Z

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->preInvoke()V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->apiResultIntents:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 62
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v2

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v5, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->apiResultIntents:Ljava/util/Map;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;-><init>(Landroid/content/Context;ILcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->registerIntents()V

    .line 66
    :cond_1
    const-string v0, "UMC:SecurityPolicyMethodInvoker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The method.toString()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "powerOffDevice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iput-boolean v8, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->commandProcessed:Z

    .line 73
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    const/16 v1, 0xc8

    iput v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->commandProcessState:I

    .line 74
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mPolicyApplier:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->backupPolicyResponse()V

    .line 79
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mPolicyObject:Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mArgObjs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 86
    :goto_0
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mPolicyClass:Ljava/lang/Class;

    .line 87
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mPolicyObject:Ljava/lang/Object;

    .line 88
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    .line 89
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mArgNames:Ljava/util/List;

    .line 90
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mArgTypes:Ljava/util/List;

    .line 91
    iput-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mArgObjs:Ljava/util/List;

    .line 94
    :try_start_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->hasReturnType()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 95
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->setReturnVal(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->apiResultIntents:Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 102
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->getInvocationException()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->getReturnVal()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->isErrorReturnValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 105
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iput-boolean v8, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->commandProcessed:Z

    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    const/16 v1, 0x64

    iput v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->commandProcessState:I

    .line 107
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mPolicyApplier:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->backupPolicyResponse()V

    move v0, v7

    .line 119
    :goto_1
    return v0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->formErrorDescription(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->setInvocationException(Ljava/lang/String;)V

    .line 82
    const-string v0, "UMC:SecurityPolicyMethodInvoker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->getInvocationException()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    goto :goto_0

    .line 97
    :catch_1
    move-exception v0

    .line 98
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Fails in creating the return object"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->unRegisterIntents()V

    .line 114
    :cond_5
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->isInvokeCompleted()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v7

    .line 116
    goto :goto_1

    .line 119
    :cond_6
    invoke-virtual {p0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;->postInvoke(Z)Z

    move-result v0

    goto :goto_1
.end method
