.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;
.super Ljava/lang/Object;
.source "Enrollment.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private crypto:Z

.field private domain:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;

.field private option:Ljava/lang/String;

.field private region:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDomain()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->domain:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;

    return-object v0
.end method

.method public getOption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->option:Ljava/lang/String;

    return-object v0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->region:Ljava/lang/String;

    return-object v0
.end method

.method public isCrypto()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->crypto:Z

    return v0
.end method

.method public setCrypto(Z)V
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->crypto:Z

    .line 35
    return-void
.end method

.method public setDomain(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->domain:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;

    .line 41
    return-void
.end method

.method public setOption(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->option:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setRegion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->region:Ljava/lang/String;

    .line 29
    return-void
.end method
