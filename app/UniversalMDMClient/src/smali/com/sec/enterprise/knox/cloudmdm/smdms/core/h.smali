.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;
.super Ljava/lang/Object;
.source "PolicyManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# static fields
.field private static gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

.field private static gContext:Landroid/content/Context;

.field private static qr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

.field private static qs:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/DummyPermissions;->getAllPermissions()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->qs:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->init(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 81
    return-void
.end method

.method public static declared-synchronized n(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;
    .locals 2

    .prologue
    .line 71
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->qr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    if-nez v0, :cond_0

    .line 72
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    .line 73
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 74
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->qr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    .line 76
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->qr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 204
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
    .locals 4

    .prologue
    .line 117
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getEmail()Ljava/lang/String;

    move-result-object v1

    .line 119
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isUserExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->removeUser(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 122
    if-nez v1, :cond_1

    .line 123
    const-string v0, "UMC:PolicyManager"

    const-string v1, "Failure removing User"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->hasEnrolledUsers(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->removeAdmin(Ljava/lang/String;)Z

    move-result v1

    .line 129
    if-nez v1, :cond_0

    .line 130
    const-string v1, "UMC:PolicyManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not able to remove the admin "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;Ljava/lang/String;[B[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getName()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDescription()Ljava/lang/String;

    move-result-object v4

    .line 104
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 105
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getNextProxyUID()I

    move-result v0

    .line 106
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v3

    invoke-static {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUid(II)I

    .line 107
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 109
    invoke-static {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->g([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    move-object v3, p3

    move-object v5, p5

    move-object v7, p6

    .line 107
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addCloudMDMAdminShareOwnershipWithLocalAdmin(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 110
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isUserExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 188
    const-string v0, "UMC:PolicyManager"

    const-string v1, "Report successfully sent to server"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 2

    .prologue
    .line 196
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 197
    const-string v0, "UMC:PolicyManager"

    const-string v1, "Failed to send report to server"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;[B[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getDescription()Ljava/lang/String;

    move-result-object v4

    .line 87
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getNextProxyUID()I

    move-result v0

    .line 89
    new-instance v6, Landroid/content/ComponentName;

    const-string v3, "Admin"

    invoke-direct {v6, p5, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v3

    invoke-static {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUid(II)I

    move-result v5

    .line 91
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 92
    invoke-static {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->g([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    move-object v3, p3

    .line 91
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addCloudMDMAdmin(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ILandroid/content/ComponentName;[Ljava/lang/String;)Z

    .line 93
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isUserExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    return-object v0
.end method

.method public onAlert(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    .locals 8

    .prologue
    .line 170
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 171
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->toJson()Ljava/lang/String;

    move-result-object v7

    .line 172
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v3

    .line 174
    if-eqz v7, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    .line 177
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;->xr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    .line 178
    const-string v5, "Alert Report"

    iget-object v6, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->message:Ljava/lang/String;

    .line 177
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    .line 181
    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->removeAlertReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V

    .line 182
    return-void
.end method

.method public onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 7

    .prologue
    .line 145
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 146
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyResponseString()Ljava/lang/String;

    move-result-object v5

    .line 149
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v6

    .line 151
    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    .line 153
    const-string v4, "Policy Report"

    .line 152
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :goto_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->removePolicyReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V

    .line 162
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    .line 163
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->ra:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V

    .line 162
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V

    .line 164
    const/4 v0, 0x0

    .line 165
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->gContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->r(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V

    .line 166
    return-void

    .line 156
    :cond_0
    const-string v0, "UMC:PolicyManager"

    const-string v1, "unable to send report to server"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
