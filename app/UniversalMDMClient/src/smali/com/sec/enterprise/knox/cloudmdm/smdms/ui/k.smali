.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;
.super Landroid/app/DialogFragment;
.source "MdmServerSelectionFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static zh:[Ljava/lang/String;

.field public static zi:Ljava/lang/String;


# instance fields
.field private TAG:Ljava/lang/String;

.field private sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

.field private zj:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 49
    const-string v0, "UMC:MdmServerSelectionFragment"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->TAG:Ljava/lang/String;

    .line 48
    return-void
.end method

.method private jl()V
    .locals 4

    .prologue
    .line 91
    :try_start_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 92
    const v1, 0x7f0800c1

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 93
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zh:[Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 94
    const v2, 0x7f080053

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 95
    const v2, 0x7f080058

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 96
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 98
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->TAG:Ljava/lang/String;

    const-string v2, "Error in hiding the progress dialog with bar"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->TAG:Ljava/lang/String;

    const-string v1, "onAttach "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 110
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 112
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 118
    packed-switch p2, :pswitch_data_0

    .line 129
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 134
    :goto_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zh:[Ljava/lang/String;

    aget-object v0, v0, p2

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zi:Ljava/lang/String;

    .line 138
    :cond_0
    :goto_1
    return-void

    .line 120
    :pswitch_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_1

    .line 123
    :pswitch_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zi:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zi:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->G(Ljava/lang/String;)V

    .line 125
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_1

    .line 132
    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreateView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->setRetainInstance(Z)V

    .line 60
    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zi:Ljava/lang/String;

    .line 61
    const v0, 0x1030077

    invoke-virtual {p0, v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->setStyle(II)V

    .line 62
    const v0, 0x7f030001

    invoke-virtual {p1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 63
    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zi:Ljava/lang/String;

    .line 66
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_1

    .line 68
    const-string v2, "mdmservers"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zh:[Ljava/lang/String;

    .line 69
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zh:[Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zh:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "No mdm servers passed"

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->TAG:Ljava/lang/String;

    const-string v3, "Error in finding mdm server list"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 77
    :cond_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->jl()V

    .line 79
    return-object v1
.end method

.method public reload()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    if-nez v0, :cond_1

    .line 84
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->jl()V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->zj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
