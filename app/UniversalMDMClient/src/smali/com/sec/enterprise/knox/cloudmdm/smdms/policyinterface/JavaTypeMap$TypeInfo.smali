.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;
.super Ljava/lang/Object;
.source "JavaTypeMap.java"


# instance fields
.field public name:Ljava/lang/String;

.field public numdims:I

.field public ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    .line 80
    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    .line 81
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    .line 84
    if-nez p1, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 88
    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_3

    .line 89
    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    .line 90
    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const-string v3, ">"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 92
    array-length v3, v2

    new-array v3, v3, [Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    .line 93
    :goto_1
    array-length v3, v2

    if-lt v0, v3, :cond_2

    .line 95
    const-string v0, ">"

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 97
    const-string v1, "[]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 98
    :goto_2
    const-string v1, "[]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 99
    const-string v1, "[]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    goto :goto_2

    .line 94
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v0

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 103
    :cond_3
    const-string v2, "[]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_4

    .line 104
    const-string v2, "[]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    move-object v0, v1

    .line 105
    :goto_3
    const-string v1, "[]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 106
    const-string v1, "[]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 107
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    goto :goto_3

    .line 110
    :cond_4
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method isGenericType()Z
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isJavaType()Z
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toClassString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 146
    const-string v1, ""

    .line 148
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    if-gtz v0, :cond_1

    .line 149
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    .line 157
    :cond_0
    :goto_0
    return-object v1

    .line 150
    :cond_1
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    if-lez v0, :cond_0

    .line 151
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    if-lt v0, v2, :cond_2

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 152
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method toJavaTypes()V
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gGTypeTbl:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->gBasicTypeTbl:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 128
    :cond_0
    return-void

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->toJavaTypes()V

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    .line 132
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    if-eqz v2, :cond_4

    move-object v2, v0

    move v0, v1

    .line 133
    :goto_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    .line 139
    :goto_1
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->numdims:I

    if-lt v1, v0, :cond_3

    .line 142
    return-object v2

    .line 134
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v2, "<"

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_2

    const-string v2, ">"

    :goto_3
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_1
    const-string v2, ""

    goto :goto_2

    .line 136
    :cond_2
    const-string v2, ","

    goto :goto_3

    .line 140
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "[]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move-object v2, v0

    goto :goto_1
.end method
