.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;
.super Ljava/lang/Object;
.source "ProfileHandler.java"


# instance fields
.field activeProfile:Ljava/lang/Object;

.field reports:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;->activeProfile:Ljava/lang/Object;

    .line 66
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;->reports:Ljava/util/Map;

    .line 67
    return-void
.end method
