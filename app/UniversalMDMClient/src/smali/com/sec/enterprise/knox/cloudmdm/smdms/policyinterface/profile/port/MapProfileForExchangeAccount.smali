.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/MapProfile;
.source "MapProfileForExchangeAccount.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:MapProfileForExchangeAccount"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/MapProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    .line 18
    const-string v0, "UMC:MapProfileForExchangeAccount"

    const-string v1, "@MapProfileForExchangeAccount constructor"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method private mapToPlatform(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 49
    .line 51
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    .line 52
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 53
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    return-object v3

    .line 53
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 54
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->platformName:Ljava/lang/String;

    .line 55
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected getParamDetail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;
    .locals 2

    .prologue
    .line 64
    const-string v0, "UMC:MapProfileForExchangeAccount"

    const-string v1, "calling overriden getParamDetail @ MapProfileForExchangeAccount"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->load(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 70
    invoke-direct {p0, v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->mapToPlatform(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    .line 73
    :cond_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile$ParamDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected updateEntry(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 23
    const-string v0, "UMC:MapProfileForExchangeAccount"

    const-string v1, "calling overriden updateEntry @ MapProfileForExchangeAccount"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->config:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->domData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->isEqualObject(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->domData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 37
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 38
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-static {p1, p0, v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileFactory;->createInstance(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->apply(Ljava/lang/Object;)Z

    goto :goto_0
.end method
