.class public final Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;
.super Ljava/lang/Object;
.source "InstallStorage.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x6d1feb577c70888fL


# instance fields
.field public downloadUrl:Ljava/lang/String;

.field public fileName:Ljava/lang/String;

.field public launchParam:Ljava/lang/String;

.field public maxRetryCount:I

.field public operation:I

.field public retryCount:I

.field public state:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->fileName:Ljava/lang/String;

    .line 55
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->launchParam:Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->fileName:Ljava/lang/String;

    .line 67
    iput p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->maxRetryCount:I

    .line 68
    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    .line 69
    iput p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->operation:I

    .line 70
    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->state:I

    .line 71
    return-void
.end method
