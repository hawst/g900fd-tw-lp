.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;
.super Ljava/lang/Object;
.source "LookupServiceRequest.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonInclude;
    value = .enum Lcom/fasterxml/jackson/annotation/JsonInclude$Include;->NON_NULL:Lcom/fasterxml/jackson/annotation/JsonInclude$Include;
.end annotation


# instance fields
.field private country_iso:Ljava/lang/String;

.field private csc:Ljava/lang/String;

.field private deviceid:Ljava/lang/String;

.field private mcc:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method public getCountry_iso()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->country_iso:Ljava/lang/String;

    return-object v0
.end method

.method public getCsc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->csc:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->deviceid:Ljava/lang/String;

    return-object v0
.end method

.method public getMcc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->mcc:Ljava/lang/String;

    return-object v0
.end method

.method public setCountry_iso(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->country_iso:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setCsc(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->csc:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setDeviceid(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->deviceid:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setMcc(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceRequest$Data;->mcc:Ljava/lang/String;

    .line 65
    return-void
.end method
