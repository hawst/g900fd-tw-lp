.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;
.super Ljava/lang/Object;
.source "CommonEula.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private href:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHref()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->href:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setHref(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->href:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->id:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->title:Ljava/lang/String;

    .line 34
    return-void
.end method
