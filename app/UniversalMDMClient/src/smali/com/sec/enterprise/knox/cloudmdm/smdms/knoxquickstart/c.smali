.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;
.super Landroid/app/DialogFragment;
.source "EmailCertificatePickerFragment.java"


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->aO(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)V
    .locals 0

    .prologue
    .line 529
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gG()V

    return-void
.end method

.method public static aN(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;
    .locals 3

    .prologue
    .line 352
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;-><init>()V

    .line 353
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 354
    const-string v2, "ID"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 355
    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->setArguments(Landroid/os/Bundle;)V

    .line 356
    return-object v0
.end method

.method private aO(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aP(I)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mView:Landroid/view/View;

    const v1, 0x7f09007c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 535
    if-eqz v0, :cond_0

    .line 536
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 537
    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 538
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 540
    :cond_0
    return-object v0
.end method

.method private aQ(I)V
    .locals 3

    .prologue
    .line 553
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 555
    const v0, 0x7f09007d

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->aO(I)Ljava/lang/String;

    move-result-object v0

    .line 556
    const v1, 0x7f09007e

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->aO(I)Ljava/lang/String;

    .line 557
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$6;

    invoke-direct {v2, p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$6;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;ILjava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 576
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 600
    :goto_0
    return-void

    .line 577
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 578
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$7;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$7;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 595
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 598
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 533
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->aP(I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)V
    .locals 0

    .prologue
    .line 552
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->aQ(I)V

    return-void
.end method

.method private gG()V
    .locals 0

    .prologue
    .line 531
    return-void
.end method


# virtual methods
.method gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;
    .locals 1

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 365
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    .line 366
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 367
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f03001b

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mView:Landroid/view/View;

    .line 369
    const-string v3, ""

    .line 370
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mView:Landroid/view/View;

    const v2, 0x7f09007d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 372
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mView:Landroid/view/View;

    .line 373
    const v5, 0x7f09007e

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 372
    check-cast v2, Landroid/widget/EditText;

    .line 375
    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 376
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;Landroid/app/ProgressDialog;)V

    .line 377
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Landroid/app/ProgressDialog;

    move-result-object v0

    .line 378
    const v1, 0x7f080037

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 379
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 380
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 381
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Landroid/app/ProgressDialog;

    move-result-object v0

    .line 516
    :goto_0
    return-object v0

    .line 383
    :cond_0
    if-ne v4, v6, :cond_1

    .line 384
    const v0, 0x7f080036

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 393
    :goto_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 394
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 395
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 396
    const v3, 0x104000a

    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;

    invoke-direct {v5, p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)V

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 427
    const/high16 v3, 0x1040000

    .line 428
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$2;

    invoke-direct {v5, p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)V

    .line 427
    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 393
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mDialog:Landroid/app/AlertDialog;

    .line 436
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mDialog:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;

    invoke-direct {v3, p0, v4, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;ILandroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 468
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$4;

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$4;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;Landroid/widget/EditText;)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 492
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$5;

    invoke-direct {v0, p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$5;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;Landroid/widget/EditText;)V

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 516
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mDialog:Landroid/app/AlertDialog;

    goto :goto_0

    .line 387
    :cond_1
    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 389
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 391
    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setSelection(I)V

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 545
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 546
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 548
    const/4 v1, 0x5

    .line 547
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 550
    :cond_0
    return-void
.end method
