.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;
.source "GSLBManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->k(Landroid/content/Context;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic vU:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

.field private final synthetic vV:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;->vU:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;->vV:Landroid/content/Context;

    .line 112
    invoke-direct {p0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 129
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onFailure in GSLBlookup"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;->vU:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    invoke-static {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;Ljava/lang/String;)I

    move-result v0

    .line 133
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hS()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public c(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 117
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSuccess in GSLBlookup"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;->vU:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;->vV:Landroid/content/Context;

    invoke-static {v0, p2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 120
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 121
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    const-string v1, "Service name does not match"

    invoke-super {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->c(ILjava/lang/String;)V

    goto :goto_0
.end method
