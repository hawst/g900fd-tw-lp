.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;
.super Landroid/app/Application;
.source "Core.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/core/a;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

.field private static gContext:Landroid/content/Context;

.field private static oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

.field private static oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

.field private static pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

.field private static pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

.field private static pc:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

.field private static pd:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;

.field private static pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

.field private static pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

.field private static pg:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

.field private static ph:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

.field private static pi:Ljava/lang/String;

.field private static pj:Ljava/lang/String;

.field private static pk:Ljava/lang/String;

.field private static pl:Ljava/lang/String;

.field private static pm:Ljava/lang/String;

.field private static pn:Ljava/lang/String;

.field private static po:Ljava/lang/String;

.field private static pp:Ljava/lang/String;

.field private static pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

.field private static pr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

.field private static ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

.field private static pt:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I
    .locals 3

    .prologue
    .line 105
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 606
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    .line 632
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    .line 633
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    .line 634
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    .line 635
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pt:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 645
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 646
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    .line 647
    return-void
.end method

.method private I(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 1090
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 1091
    if-eqz p1, :cond_0

    .line 1093
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->u(Landroid/content/Context;Ljava/lang/String;)V

    .line 1096
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->J(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1097
    const/4 v4, 0x1

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V

    .line 1099
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 1100
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 1101
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 1105
    :goto_0
    return-void

    .line 1103
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    const v2, 0x7f080079

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private J(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1108
    .line 1111
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getLicenses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, v4

    move-object v2, v4

    move-object v1, v4

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1127
    if-nez v1, :cond_1

    if-eqz v2, :cond_8

    .line 1128
    :cond_1
    const-string v0, ""

    .line 1129
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;

    invoke-direct {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;-><init>()V

    .line 1130
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    iput-object v0, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 1131
    if-eqz p1, :cond_6

    .line 1132
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->s(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1133
    iput-boolean v7, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;->px:Z

    move-object v3, p1

    .line 1147
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    const/4 v6, -0x1

    invoke-static {v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 1148
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    move v0, v7

    .line 1152
    :goto_2
    return v0

    .line 1111
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;

    .line 1112
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    const-string v9, "KLM"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1113
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 1114
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v2, v4

    .line 1117
    :cond_3
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1118
    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ELM"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1119
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 1120
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v1, v4

    .line 1123
    :cond_5
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_0

    .line 1135
    :cond_6
    if-nez v3, :cond_7

    .line 1136
    iget-object v0, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 1137
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 1139
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 1143
    :cond_7
    const-string v4, "1.0"

    .line 1144
    iput-boolean v6, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;->px:Z

    goto :goto_1

    :cond_8
    move v0, v6

    .line 1152
    goto :goto_2
.end method

.method static synthetic K(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 624
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pi:Ljava/lang/String;

    return-void
.end method

.method static synthetic L(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 626
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    return-void
.end method

.method static synthetic M(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 628
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    return-void
.end method

.method static synthetic N(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 629
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pn:Ljava/lang/String;

    return-void
.end method

.method static synthetic O(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 630
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->po:Ljava/lang/String;

    return-void
.end method

.method static synthetic P(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 625
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pj:Ljava/lang/String;

    return-void
.end method

.method static synthetic Q(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 635
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pt:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I
    .locals 1

    .prologue
    .line 1253
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1199
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fs()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V
    .locals 0

    .prologue
    .line 606
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V
    .locals 0

    .prologue
    .line 934
    invoke-direct/range {p0 .. p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 0

    .prologue
    .line 1014
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 633
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;)V
    .locals 0

    .prologue
    .line 623
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ph:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    return-void
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZII)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 936
    if-nez p1, :cond_0

    .line 937
    const-string v0, "UMC:Core"

    const-string v1, "finalizeEnrollment: profile is null "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :goto_0
    return-void

    .line 941
    :cond_0
    if-eqz p4, :cond_2

    .line 942
    const-string v0, "UMC:Core"

    const-string v1, "finalizeEnrollment: launchAgent "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aU(I)Ljava/lang/String;

    move-result-object v6

    .line 944
    invoke-direct {p0, p3, v6, p5, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 950
    :goto_1
    if-nez p3, :cond_4

    .line 951
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 952
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 953
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 956
    :goto_2
    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    .line 957
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    .line 958
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getProfileIcon()[B

    move-result-object v4

    .line 957
    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;[BLjava/lang/String;Ljava/lang/String;)V

    .line 956
    invoke-static {v8, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)I

    .line 961
    if-eqz p2, :cond_3

    .line 962
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0

    .line 963
    :goto_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getProfileIcon()[B

    move-result-object v3

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;[B[Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    .line 968
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getPollInterval()I

    move-result v0

    .line 969
    if-lez v0, :cond_1

    .line 970
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    int-to-long v2, v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v4

    .line 971
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->ra:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->toString()Ljava/lang/String;

    move-result-object v5

    .line 970
    invoke-static {v1, v2, v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    .line 972
    const-string v1, "UMC:Core"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "finalizeEnrollment: created alarm for:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v0

    const-string v1, "ENROLLED"

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 946
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    const v1, 0x7f08007d

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->b(Landroid/content/Context;I)V

    .line 947
    const-string v0, "UMC:Core"

    const-string v1, "finalizeEnrollment: enrollment comlpete "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v7

    goto/16 :goto_1

    :cond_3
    move-object v4, v7

    goto :goto_3

    :cond_4
    move-object v5, p3

    goto/16 :goto_2
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 4

    .prologue
    .line 1016
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 1046
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    .line 1047
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v2

    .line 1048
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1046
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->addPendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 1050
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeEnrollment: ProfileId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    return-void
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 980
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 1005
    invoke-virtual {p1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setStatus(Ljava/lang/String;)V

    .line 1006
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    .line 1007
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v2

    .line 1008
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1006
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->addPendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 1010
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V

    .line 1011
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateEnrollmentStatus: ProfileId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V
    .locals 0

    .prologue
    .line 615
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pc:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 1233
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.enterprise.knox.intent.action.LAUNCH_APP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1234
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1235
    const-string v1, "com.sec.enterprise.knox.intent.extra.USER_CREDENTIALS"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pn:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1236
    const-string v1, "com.sec.enterprise.knox.intent.extra.APP_PAYLOAD"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pp:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1237
    const-string v1, "com.sec.enterprise.knox.intent.extra.ELM_ERROR_CODE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1238
    const-string v1, "com.sec.enterprise.knox.intent.extra.KLM_ERROR_CODE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1239
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1240
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1241
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1242
    const-string v0, "UMC:Core"

    const-string v1, " Launch Intent : com.sec.enterprise.knox.intent.action.LAUNCH_APP"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.USER_CREDENTIALS): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1245
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1244
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.APP_PAYLOAD): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1247
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1246
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.ELM_ERROR_CODE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.KLM_ERROR_CODE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1107
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->J(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I
    .locals 3

    .prologue
    const v0, 0x7f0800a1

    .line 1254
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1272
    :goto_0
    :pswitch_0
    return v0

    .line 1269
    :pswitch_1
    const v0, 0x7f0800a0

    goto :goto_0

    .line 1254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V
    .locals 0

    .prologue
    .line 922
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fq()V

    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1089
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->I(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fr()V

    return-void
.end method

.method static synthetic fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;
    .locals 1

    .prologue
    .line 612
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    return-object v0
.end method

.method static synthetic fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;
    .locals 1

    .prologue
    .line 608
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    return-object v0
.end method

.method static synthetic fC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic fD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 628
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic fE()Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;
    .locals 1

    .prologue
    .line 619
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    return-object v0
.end method

.method static synthetic fF()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;
    .locals 1

    .prologue
    .line 620
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    return-object v0
.end method

.method static synthetic fG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 629
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic fH()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;
    .locals 1

    .prologue
    .line 633
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    return-object v0
.end method

.method static synthetic fI()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;
    .locals 1

    .prologue
    .line 623
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ph:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    return-object v0
.end method

.method static synthetic fJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 625
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic fK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pi:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic fL()Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;
    .locals 1

    .prologue
    .line 614
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    return-object v0
.end method

.method static synthetic fM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->po:Ljava/lang/String;

    return-object v0
.end method

.method public static final fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;
    .locals 1

    .prologue
    .line 638
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    return-object v0
.end method

.method public static final fp()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 853
    const-string v0, "UMC:Core"

    const-string v1, "relaunch"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/CoreReceiver;->k(Landroid/content/Context;)V

    .line 855
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 856
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.enterprise.knox.intent.action.RELAUNCH_APP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 857
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    const-class v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/CoreReceiver;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 858
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    .line 859
    const/high16 v3, 0x8000000

    .line 858
    invoke-static {v2, v6, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 860
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    .line 861
    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 862
    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    .line 863
    return-void
.end method

.method private fq()V
    .locals 3

    .prologue
    .line 923
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 924
    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 925
    invoke-static {p0}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pd:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/c;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 928
    return-void
.end method

.method private fr()V
    .locals 2

    .prologue
    .line 931
    invoke-static {p0}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pd:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/c;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 932
    return-void
.end method

.method private fs()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1200
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v1

    .line 1201
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pn:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1202
    const-string v0, ""

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pn:Ljava/lang/String;

    .line 1204
    :cond_0
    const/4 v0, 0x0

    .line 1205
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    .line 1206
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getPayload()Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1207
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getPayload()Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fasterxml/jackson/databind/JsonNode;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pp:Ljava/lang/String;

    .line 1209
    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1210
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1211
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v2

    .line 1212
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1213
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v0

    .line 1223
    :cond_2
    :goto_0
    return-object v0

    .line 1216
    :cond_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 1217
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1218
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1219
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    .line 1220
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1221
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;->getHref()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic fy()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;
    .locals 1

    .prologue
    .line 606
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    return-object v0
.end method

.method static synthetic fz()Landroid/content/Context;
    .locals 1

    .prologue
    .line 610
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    return-object v0
.end method

.method public static final shutdown()V
    .locals 2

    .prologue
    .line 847
    const-string v0, "UMC:Core"

    const-string v1, "shutdown"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/CoreReceiver;->l(Landroid/content/Context;)V

    .line 849
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 850
    return-void
.end method


# virtual methods
.method public E(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 658
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pF:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    .line 659
    return-void
.end method

.method public F(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 672
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeviceRegistered: GCM Id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pi:Ljava/lang/String;

    .line 674
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 676
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pi:Ljava/lang/String;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->po:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 677
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 678
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->F(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public G(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 751
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The mdm server was selected with value:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pS:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    .line 753
    return-void
.end method

.method public H(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1056
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onRegisterSamsungPush: Push Id:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pj:Ljava/lang/String;

    .line 1058
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1059
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 1060
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pj:Ljava/lang/String;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->po:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    :cond_0
    :goto_0
    return-void

    .line 1061
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 1062
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->H(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 731
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-nez v0, :cond_1

    .line 741
    :cond_0
    :goto_0
    return-object v1

    .line 734
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;->wa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    invoke-virtual {p1, v0}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 735
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    goto :goto_0

    .line 736
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p1, v0}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 737
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    goto :goto_0

    .line 738
    :cond_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wx:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p1, v0}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 739
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/16 v0, 0xc9

    .line 1073
    if-eq p1, v0, :cond_0

    .line 1074
    if-ne p2, v0, :cond_1

    .line 1075
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 1076
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f080099

    .line 1077
    const v2, 0x7f08009f

    const v3, 0x7f080058

    const/4 v4, 0x1

    .line 1076
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 1078
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v7, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 1087
    :goto_0
    return-void

    .line 1082
    :cond_1
    check-cast p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;

    .line 1083
    iget-object v1, p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    iget-boolean v4, p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/b;->px:Z

    move-object v0, p0

    move-object v2, p4

    move-object v3, p5

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V

    .line 1084
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v7, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 1085
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 1086
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 693
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onNetworkOperationSuccess: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 712
    :goto_0
    return-void

    .line 697
    :sswitch_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pG:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 700
    :sswitch_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 703
    :sswitch_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 704
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fO()I

    move-result v0

    .line 705
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    .line 706
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 705
    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0

    .line 695
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x7 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 3

    .prologue
    .line 717
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onNetworkOperationFailure: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 719
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 720
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 721
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    .line 723
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 796
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    .line 797
    sput-object p4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pl:Ljava/lang/String;

    .line 799
    if-nez p2, :cond_0

    .line 800
    const-string p2, ""

    .line 802
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 804
    if-nez p3, :cond_1

    .line 805
    const-string p3, ""

    .line 807
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 809
    if-nez p5, :cond_2

    .line 810
    const-string p5, ""

    .line 812
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 813
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 881
    const-string v0, "UMC:Core"

    const-string v1, "onAlarmPushReceived: "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/PushReceiver;->a(Landroid/content/Context;Lorg/json/JSONObject;)V

    .line 883
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pg:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->d(Lorg/json/JSONObject;)V

    .line 884
    return-void
.end method

.method public b(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 867
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPushReceived: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/PushReceiver;->a(Landroid/content/Context;Lorg/json/JSONObject;)V

    .line 869
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pg:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->d(Lorg/json/JSONObject;)V

    .line 870
    return-void
.end method

.method public c(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 663
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 664
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->c(Ljava/util/HashMap;)V

    .line 668
    :goto_0
    return-void

    .line 666
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public c(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 874
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPushReceivedSpp: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/PushReceiver;->a(Landroid/content/Context;Lorg/json/JSONObject;)V

    .line 876
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pg:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->d(Lorg/json/JSONObject;)V

    .line 877
    return-void
.end method

.method public final fk()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;
    .locals 1

    .prologue
    .line 642
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    return-object v0
.end method

.method public fl()Z
    .locals 2

    .prologue
    .line 650
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->pz:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    if-ne v0, v1, :cond_0

    .line 651
    const/4 v0, 0x1

    .line 653
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fm()V
    .locals 3

    .prologue
    .line 756
    const-string v0, "UMC:Core"

    const-string v1, "onUserCancelEnrollment"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 758
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fm()V

    .line 762
    :goto_0
    return-void

    .line 760
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public fn()V
    .locals 6

    .prologue
    .line 776
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processLaunchParameters: state  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pl:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pl:Ljava/lang/String;

    const-string v1, "MMD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 778
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-nez v0, :cond_0

    .line 779
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    .line 780
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    .line 779
    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    .line 782
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gM()V

    .line 788
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->x(Landroid/content/Context;)Z

    move-result v0

    .line 789
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->v(Landroid/content/Context;)V

    .line 790
    const-string v1, "UMC:Core"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "selfUpdated : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    return-void

    .line 783
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->pz:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    if-eq v0, v1, :cond_1

    .line 784
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pE:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public fo()V
    .locals 3

    .prologue
    .line 816
    const-string v0, "UMC:Core"

    const-string v1, "startSelfUpdateCheck"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->gx()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 818
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->gx()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 819
    const-string v0, "UMC:Core"

    const-string v1, "Self update already in progress.Bring update ui foreground "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->gw()V

    .line 830
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$1;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/d;)V

    .line 844
    :cond_2
    :goto_0
    return-void

    .line 822
    :cond_3
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mState is not IDLE. So not doing self update check : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->gw()V

    .line 824
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pt:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 825
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pt:Ljava/lang/String;

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pk:Ljava/lang/String;

    goto :goto_0
.end method

.method public ft()V
    .locals 1

    .prologue
    .line 1284
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    if-eqz v0, :cond_0

    .line 1285
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pr:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->ft()V

    .line 1288
    :cond_0
    return-void
.end method

.method public fu()V
    .locals 1

    .prologue
    .line 1325
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    .line 1326
    return-void
.end method

.method public fv()Z
    .locals 1

    .prologue
    .line 1329
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1333
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pl:Ljava/lang/String;

    return-object v0
.end method

.method public fx()V
    .locals 2

    .prologue
    .line 1338
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 1339
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fx()V

    .line 1343
    :goto_0
    return-void

    .line 1341
    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 888
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 889
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;

    const-string v1, "UMC-Console-Logger"

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;)Z

    .line 890
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    .line 891
    const-string v0, "UMC:Core"

    const-string v1, "onCreate(): "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->V(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xy:Ljava/lang/String;

    .line 894
    const-string v0, "USER_AGENT"

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xy:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 898
    :goto_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    .line 900
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pb:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    .line 902
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 903
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->init()Z

    .line 905
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->u(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pq:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    .line 906
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->n(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pf:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    .line 907
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->r(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pg:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    .line 909
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->init(Landroid/content/Context;)V

    .line 912
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pd:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;

    .line 914
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/CoreReceiver;->k(Landroid/content/Context;)V

    .line 916
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->G(Landroid/content/Context;)V

    .line 918
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->gContext:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 920
    return-void

    .line 895
    :catch_0
    move-exception v0

    .line 896
    const-string v1, "Exception"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 1278
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 1279
    const-string v0, "UMC:Core"

    const-string v1, "onTerminate()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    return-void
.end method

.method public s(Z)V
    .locals 3

    .prologue
    .line 684
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 685
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->s(Z)V

    .line 689
    :goto_0
    return-void

    .line 687
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public t(Z)V
    .locals 2

    .prologue
    .line 1292
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_1

    .line 1293
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->A(Z)V

    .line 1315
    :cond_0
    :goto_0
    return-void

    .line 1296
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fO()I

    move-result v0

    .line 1295
    if-nez v0, :cond_0

    .line 1297
    if-nez p1, :cond_2

    .line 1298
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    goto :goto_0

    .line 1301
    :cond_2
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 1302
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->stopSettingsApp(Landroid/content/Context;I)V

    .line 1303
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1304
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1305
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1306
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->oZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1310
    :goto_1
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 1311
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->pa:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    goto :goto_0

    .line 1307
    :catch_0
    move-exception v0

    .line 1308
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public u(Z)V
    .locals 1

    .prologue
    .line 1319
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    if-eqz v0, :cond_0

    .line 1320
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->ps:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->u(Z)V

    .line 1322
    :cond_0
    return-void
.end method
