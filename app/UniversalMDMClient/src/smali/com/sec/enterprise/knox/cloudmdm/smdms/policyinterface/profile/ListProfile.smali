.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;
.source "ListProfile.java"


# instance fields
.field listObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->data:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->data:Ljava/lang/Object;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createListObject(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->data:Ljava/lang/Object;

    .line 63
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->config:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->operations:Ljava/util/Map;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->translateObject(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    .line 64
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->data:Ljava/lang/Object;

    goto :goto_0
.end method

.method private getListEntryType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 153
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->getTypeInfo(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    move-result-object v0

    .line 155
    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    const-string v2, "List"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->ptypes:[Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap$TypeInfo;->name:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected addEntry(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->getAddRequestParams(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->addMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->id:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->addCommandRequest(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)V

    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".entry"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-direct {v2, v3, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->addNodeToReqTbl(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;)V

    .line 72
    return-void
.end method

.method protected applyInternal(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 108
    check-cast p1, Ljava/util/List;

    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    :cond_1
    return-void

    .line 113
    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 114
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_6

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 123
    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->contains(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 124
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->removeEntry(Ljava/lang/Object;)V

    .line 122
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 115
    :cond_6
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->contains(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 116
    :cond_7
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->addEntry(Ljava/lang/Object;)V

    .line 114
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected contains(Ljava/util/List;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 162
    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected getAddRequestParams(Ljava/lang/Object;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 138
    :goto_0
    return-object v0

    .line 133
    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->config:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->getListEntryType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".entry"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->addMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->getParamsFromConfig(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method protected getRemoveRequestParams(Ljava/lang/Object;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 149
    :goto_0
    return-object v0

    .line 144
    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->config:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->dataType:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->getListEntryType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".entry"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->removeMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->getParamsFromConfig(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method protected onAddEntryResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 3

    .prologue
    .line 75
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->getObjectFromReqTbl(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->updateParent()V

    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->removeNodeFromReqTbl(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 5

    .prologue
    .line 168
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_0

    .line 178
    invoke-super {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V

    .line 179
    return-void

    .line 169
    :cond_0
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 170
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyResponse()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    .line 171
    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->addMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 172
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->onAddEntryResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 168
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 173
    :cond_2
    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->removeMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 174
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->onRemoveEntryResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_1
.end method

.method protected onRemoveEntryResponse(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->getObjectFromReqTbl(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->updateParent()V

    .line 96
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->removeNodeFromReqTbl(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method protected removeEntry(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->getRemoveRequestParams(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    .line 86
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listOperations:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->removeMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 87
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->id:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->addCommandRequest(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)V

    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".entry"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-direct {v2, v3, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->addNodeToReqTbl(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;)V

    .line 89
    return-void
.end method

.method updateParent()V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->listObjects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->data:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->onChildProfileApplied(Ljava/lang/String;Ljava/lang/Object;)V

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;->name:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->onChildProfileApplied(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
