.class Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;
.super Ljava/lang/Thread;
.source "InstallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->ga()V
.end annotation


# instance fields
.field final synthetic rs:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;->rs:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;

    .line 157
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->go()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;->rs:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;->rs:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;Ljava/lang/String;)Z

    move-result v1

    .line 164
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 165
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->gp()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file deleted: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 172
    :cond_0
    if-eqz v1, :cond_1

    .line 173
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;->rs:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->gm()V

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;->rs:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    goto :goto_0
.end method
