.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;
.super Ljava/lang/Object;
.source "PendingProfile.java"


# instance fields
.field nodeVsDomIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field nodeVsReqIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;

.field private transient profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

.field reports:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsDomIdMap:Ljava/util/Map;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->reports:Ljava/util/Map;

    .line 65
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    .line 66
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;

    .line 67
    return-void
.end method

.method private cleanupUnNeededFields(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 175
    move v1, v2

    :goto_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 178
    :goto_1
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_1

    .line 182
    return-void

    .line 176
    :cond_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->cmdRequests:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iput-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$StateData;

    .line 175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 179
    :cond_1
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->cmdResponses:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iput-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;
    .locals 2

    .prologue
    .line 150
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-object v0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 153
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static restore(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyBackup()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getMDMUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->restorePendingProfile(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    .line 192
    iput-object p0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    .line 193
    :cond_0
    return-object v0
.end method


# virtual methods
.method public addNodeToDomTbl(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsDomIdMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->domid:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->reqId:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->backup()V

    .line 85
    return-void
.end method

.method public addNodeToReqTbl(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->reqId:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->backup()V

    .line 90
    return-void
.end method

.method public addReport(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->reports:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->cleanupUnNeededFields(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V

    .line 171
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->backup()V

    .line 172
    return-void
.end method

.method public backup()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyBackup()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getMDMUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->backupPendingProfile(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;)V

    .line 187
    return-void
.end method

.method public delete()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyBackup()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getMDMUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;->removePendingProfile(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public getNodeFromDomTbl(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsDomIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    return-object v0
.end method

.method public getNodeFromReqTbl(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    return-object v0
.end method

.method public getObjectFromDomTbl(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 106
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->getNodeFromDomTbl(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    .line 108
    iget-object v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->object:Ljava/lang/Object;

    .line 110
    :cond_0
    return-object v0
.end method

.method public getObjectFromReqTbl(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 115
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->getNodeFromReqTbl(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    move-result-object v1

    .line 116
    if-eqz v1, :cond_0

    .line 117
    iget-object v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->object:Ljava/lang/Object;

    .line 119
    :cond_0
    return-object v0
.end method

.method public hasAnyPendingNodes()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isPendingNode(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeNodeFromDomTbl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->getNodeFromDomTbl(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile$NodeInfo;->reqId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsDomIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->backup()V

    .line 132
    return-void
.end method

.method public removeNodeFromReqTbl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->nodeVsReqIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->backup()V

    .line 137
    return-void
.end method

.method public toJson()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    invoke-virtual {v0, p0}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 162
    const/4 v0, 0x0

    goto :goto_0
.end method
