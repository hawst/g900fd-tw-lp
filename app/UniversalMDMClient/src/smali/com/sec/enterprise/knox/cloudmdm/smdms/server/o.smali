.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;
.source "GatewayManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

.field private static ry:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

.field private pk:Ljava/lang/String;

.field private vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I
    .locals 3

    .prologue
    .line 33
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;-><init>()V

    return-void
.end method

.method private E(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 60
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "httpclient: segurl:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    if-nez v0, :cond_0

    .line 65
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    const-string v1, "Need to do GSLB lookup again!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 67
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->k(Landroid/content/Context;Ljava/lang/String;)V

    .line 74
    :goto_0
    return-void

    .line 70
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Gateway_url "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-direct {v1, v0, p1, v4, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aQ(Ljava/lang/String;)V

    return-void
.end method

.method private aQ(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 121
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    const-string v1, "Converting profileinfo to json"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    .line 123
    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    .line 125
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Converted"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hW()V

    .line 138
    :goto_0
    return-void

    .line 132
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    const-string v1, "ProfileInfo response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processProfileInfoResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic hS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    return-object v0
.end method

.method public static hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/p;->hX()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    return-object v0
.end method

.method private hW()V
    .locals 7

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->getElements()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 268
    :cond_0
    return-void

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 227
    new-instance v2, Lcom/a/a/a/a;

    invoke-direct {v2}, Lcom/a/a/a/a;-><init>()V

    .line 228
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 229
    const-string v5, "image/png"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "image/jpeg"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "text/html"

    aput-object v5, v3, v4

    .line 231
    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o$2;

    invoke-direct {v4, p0, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;[Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    .line 261
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 262
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getIcon()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 263
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getIcon()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;->getHref()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 264
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "icon href is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getIcon()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;->getHref()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getIcon()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;->getHref()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, Lcom/a/a/a/a;->a(Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 3

    .prologue
    .line 170
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network oper succeeded and called back to observer Gatewaymanager: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 171
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 185
    :goto_0
    return-void

    .line 176
    :pswitch_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 178
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    const-string v1, "Retrying profile lookup after gslb..."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->pk:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->m(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 3

    .prologue
    .line 190
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network operation failed and called back to observer GatewayManager:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 191
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 190
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V

    .line 194
    return-void
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    .line 146
    return-void
.end method

.method public aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;
    .locals 5

    .prologue
    .line 149
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Getting profileinfo from container: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->getElements()Ljava/util/List;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_1

    .line 154
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "profilelist size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 164
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find matching profile for group:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 155
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 156
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "grpname: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method public hV()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->vX:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    return-object v0
.end method

.method public m(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 84
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    const-string v1, "Starting profilelookup: "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->mContext:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->pk:Ljava/lang/String;

    .line 87
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->E(Landroid/content/Context;)V

    .line 88
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 117
    :goto_0
    return-void

    .line 92
    :cond_0
    new-instance v0, Lcom/a/a/a/j;

    invoke-direct {v0}, Lcom/a/a/a/j;-><init>()V

    .line 93
    const-string v1, "emailAddress"

    invoke-virtual {v0, v1, p2}, Lcom/a/a/a/j;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "request_params:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/a/a/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o$1;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v1, p0, v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 108
    sget-boolean v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v2, :cond_1

    .line 109
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "profileContainer.json"

    .line 111
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    .line 110
    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    .line 112
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;)V

    .line 113
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;->wa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->b(Ljava/lang/Enum;)V

    goto :goto_0

    .line 115
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v3, "/profiles"

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    goto :goto_0
.end method
