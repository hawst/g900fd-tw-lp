.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;
.super Ljava/lang/Object;
.source "CloudMdmEnrollmentActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(IIII)V
.end annotation


# instance fields
.field private final synthetic xV:I

.field private final synthetic xW:I

.field private final synthetic xX:I

.field private final synthetic xY:I


# direct methods
.method constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 1
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xV:I

    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xW:I

    iput p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xX:I

    iput p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xY:I

    .line 680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const v2, 0x7f0800a6

    .line 684
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iC()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 685
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xV:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 686
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xW:I

    if-ne v1, v2, :cond_1

    .line 687
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 688
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xW:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 697
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xX:I

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2$1;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 704
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xY:I

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2$2;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 710
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 711
    return-void

    .line 690
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iC()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 691
    const-string v2, "My "

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 692
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 695
    :cond_1
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;->xW:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
