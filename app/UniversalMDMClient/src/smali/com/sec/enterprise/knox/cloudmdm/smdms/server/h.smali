.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;
.super Ljava/lang/Object;
.source "ContentTransferManagerResponseInfo.java"


# static fields
.field private static ry:Ljava/lang/String;


# instance fields
.field public status:I

.field public vF:J

.field public vG:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->ry:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JI)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vF:J

    .line 18
    iput p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->status:I

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    .line 20
    return-void
.end method


# virtual methods
.method public getResponseBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    return-object v0
.end method

.method public hQ()V
    .locals 4

    .prologue
    .line 31
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jobIdRef: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vF:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->status:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public setResponseBundle(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    .line 24
    return-void
.end method
