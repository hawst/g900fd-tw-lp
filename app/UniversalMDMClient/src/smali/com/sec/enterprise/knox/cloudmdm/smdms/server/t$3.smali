.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;
.source "MDMManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V
.end annotation


# instance fields
.field final synthetic wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

.field private final synthetic ws:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;->wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;->ws:Ljava/lang/String;

    .line 242
    invoke-direct {p0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    return-void
.end method


# virtual methods
.method public c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 247
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":process response on success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;->wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;->ws:Ljava/lang/String;

    invoke-static {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 249
    if-nez v0, :cond_0

    .line 250
    const/4 v0, 0x0

    const-string v1, "Enroll device error"

    invoke-super {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 256
    :goto_0
    return-void

    .line 252
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updatedevice result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;->wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 253
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;->wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getPushId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->c(ILjava/lang/String;)V

    goto :goto_0
.end method
