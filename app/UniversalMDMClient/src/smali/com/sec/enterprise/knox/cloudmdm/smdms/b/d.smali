.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/b/d;
.super Ljava/lang/Object;
.source "MultiUserUtils.java"


# direct methods
.method public static j(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    if-eqz p0, :cond_0

    .line 49
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->fh()Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;

    move-result-object v1

    .line 50
    const/4 v2, 0x0

    .line 49
    invoke-virtual {v1, p0, v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 51
    if-eqz v1, :cond_0

    .line 52
    const/4 v0, 0x1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 55
    :catch_0
    move-exception v1

    .line 56
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static k(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 2

    .prologue
    .line 63
    if-eqz p0, :cond_0

    .line 65
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->fh()Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;

    move-result-object v0

    .line 66
    const/4 v1, 0x0

    .line 65
    invoke-virtual {v0, p0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
