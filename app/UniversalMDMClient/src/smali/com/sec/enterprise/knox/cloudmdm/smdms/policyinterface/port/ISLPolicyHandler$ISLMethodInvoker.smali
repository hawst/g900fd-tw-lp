.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "ISLPolicyHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:ISLMethodInvoker"


# instance fields
.field holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    .line 61
    return-void
.end method


# virtual methods
.method protected handleDone()Z
    .locals 2

    .prologue
    .line 122
    const-string v0, "UMC:ISLMethodInvoker"

    const-string v1, "Inside handleDone @ ISLPOLICYmethodInvoker"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLMethodInvoker(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;)V

    .line 125
    :cond_0
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->handleDone()Z

    move-result v0

    return v0
.end method

.method public init()Z
    .locals 4

    .prologue
    .line 64
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->init()Z

    move-result v0

    .line 66
    const-string v1, "UMC:ISLMethodInvoker"

    const-string v2, "===inside Init() of ISLPolicyHandler"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mPolicyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLPolicyHolder(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    .line 68
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    if-eqz v1, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->prepareForISLCall()Z

    move-result v0

    .line 71
    :cond_0
    return v0
.end method

.method protected isInvokeCompleted()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 102
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "getISAList"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "startRuntimeWatch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "stopRuntimeWatch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 108
    :goto_0
    return v0

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->getReturnVal()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAPIResult(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    const-string v0, "UMC:ISLMethodInvoker"

    const-string v1, "onError is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "message"

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->setAPIReturnParams(Ljava/util/Map;)V

    .line 115
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;->INVOKE_SUCCESS_STATE:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V

    .line 116
    return-void
.end method

.method public onPolicyCreation(ZLjava/lang/Object;)V
    .locals 3

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mPolicyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLPolicyHolder(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    .line 77
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->prepareForISLCall()Z

    move-result p1

    .line 79
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->onPolicyCreation(ZLjava/lang/Object;)V

    .line 80
    return-void
.end method

.method prepareForISLCall()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 83
    const/4 v1, 0x0

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->isl:Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->isISAReady()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 90
    :goto_0
    if-nez v0, :cond_0

    .line 91
    const-string v0, "UMC:ISLMethodInvoker"

    const-string v1, "Enterprise ISL\'s thirdparty ISA Not Ready"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;->INVOKE_ERROR_STATE:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;

    invoke-virtual {p0, v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V

    .line 98
    :goto_1
    return v5

    .line 86
    :catch_0
    move-exception v0

    .line 87
    const-string v2, "UMC:ISLMethodInvoker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->holder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLMethodInvoker(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;)V

    goto :goto_1
.end method
