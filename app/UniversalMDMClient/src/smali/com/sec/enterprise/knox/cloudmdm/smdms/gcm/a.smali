.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;
.super Ljava/lang/Object;
.source "GcmInterface.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private rh:Landroid/os/HandlerThread;

.field private ri:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

.field private rj:Lcom/google/android/gms/a/a;

.field private rk:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rk:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    .line 32
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GcmServiceHandler"

    .line 33
    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rh:Landroid/os/HandlerThread;

    .line 34
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 35
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->ri:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    .line 37
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/a/a;->g(Landroid/content/Context;)Lcom/google/android/gms/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rj:Lcom/google/android/gms/a/a;

    .line 38
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->ri:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/GcmMessageReceiver;->a(Landroid/content/Context;Landroid/os/Handler;)V

    .line 39
    return-void
.end method

.method private U(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->mContext:Landroid/content/Context;

    .line 54
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 53
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 55
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 57
    :goto_0
    if-eqz v0, :cond_0

    .line 58
    const-string v1, "gcm_registration_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 59
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 62
    :cond_0
    return-void

    .line 56
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/google/android/gms/a/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rj:Lcom/google/android/gms/a/a;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->U(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->ri:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->rk:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/c;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->gj()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private gj()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 66
    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->mContext:Landroid/content/Context;

    .line 68
    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 67
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_1

    .line 70
    const-string v2, "gcm_registration_id"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    :cond_0
    :goto_0
    return-object v1

    :cond_1
    move-object v0, v1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public gi()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->gj()Ljava/lang/String;

    move-result-object v0

    .line 43
    if-nez v0, :cond_0

    .line 44
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->ri:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    .line 45
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 44
    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->ri:Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/b;->sendMessage(Landroid/os/Message;)Z

    .line 48
    :cond_0
    return-object v0
.end method
