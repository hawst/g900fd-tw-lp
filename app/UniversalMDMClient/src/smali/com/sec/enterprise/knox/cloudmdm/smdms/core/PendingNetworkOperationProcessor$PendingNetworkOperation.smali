.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;
.super Ljava/lang/Object;
.source "PendingNetworkOperationProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonInclude;
    value = .enum Lcom/fasterxml/jackson/annotation/JsonInclude$Include;->NON_NULL:Lcom/fasterxml/jackson/annotation/JsonInclude$Include;
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I


# instance fields
.field data:Ljava/lang/String;

.field profileId:Ljava/lang/String;

.field retryCount:I

.field type:Ljava/lang/String;

.field url:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I
    .locals 3

    .prologue
    .line 60
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    .line 78
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    .line 79
    return-void
.end method

.method private handleDeleteDevice()V
    .locals 4

    .prologue
    .line 82
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 117
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 118
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 119
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$3()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private handleUpdateDevice()V
    .locals 4

    .prologue
    .line 126
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 151
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$3()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 160
    if-ne p0, p1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 163
    goto :goto_0

    .line 164
    :cond_2
    instance-of v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    if-nez v2, :cond_3

    move v0, v1

    .line 165
    goto :goto_0

    .line 166
    :cond_3
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    .line 167
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 168
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 169
    goto :goto_0

    .line 170
    :cond_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 171
    goto :goto_0

    .line 172
    :cond_5
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 173
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 174
    goto :goto_0

    .line 175
    :cond_6
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 176
    goto :goto_0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    return-object v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->retryCount:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 217
    .line 219
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 220
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 221
    return v0

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public run()V
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    .line 227
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 235
    const-string v1, "UMC:PendingNetworkOperationProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled Network Operation Type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :goto_0
    return-void

    .line 229
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->handleUpdateDevice()V

    goto :goto_0

    .line 232
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->handleDeleteDevice()V

    goto :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    .line 244
    return-void
.end method

.method public setProfileId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    .line 251
    return-void
.end method

.method public setRetryCount(I)V
    .locals 0

    .prologue
    .line 257
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->retryCount:I

    .line 258
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    .line 272
    return-void
.end method
