.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;
.super Ljava/lang/Object;
.source "EasPasswordPolicies.java"


# instance fields
.field private rF:Ljava/lang/String;

.field private rG:Ljava/lang/String;

.field private rH:Ljava/lang/String;

.field private rI:Ljava/lang/String;

.field private rJ:Ljava/lang/String;

.field private rK:Ljava/lang/String;

.field private rL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getBoolean(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 141
    const-string v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x1

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getInt(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 149
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 153
    :goto_0
    return v0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 153
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public ac(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rF:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public ad(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rG:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public ae(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rH:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public af(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rI:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public ag(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rJ:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public ah(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rK:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public ai(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rL:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public gB()Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rF:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "DevicePasswordEnabled"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rF:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rG:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "AllowSimpleDevicePassword"

    .line 67
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rG:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 66
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rH:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 69
    const-string v0, "AlphanumericDevicePasswordRequired"

    .line 70
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rH:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 69
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rI:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 72
    const-string v0, "MinDevicePasswordComplexCharacters"

    .line 73
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rI:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 72
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rJ:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 75
    const-string v0, "MinDevicePasswordLength"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rJ:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rK:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 77
    const-string v0, "MaxDevicePasswordFailedAttempts"

    .line 78
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rK:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 77
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 79
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rL:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 80
    const-string v0, "MaxInactivityTimeDeviceLock"

    .line 81
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->rL:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 80
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_6
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
