.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;
.super Ljava/lang/Object;
.source "AsyncNetworkClient.java"


# static fields
.field private static final ry:Ljava/lang/String;


# instance fields
.field private vt:Lcom/a/a/a/a;

.field private vu:Ljava/lang/String;

.field private vv:I

.field private vw:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 53
    if-eqz p1, :cond_0

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Its https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :try_start_0
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SSL exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SSL connection failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 4

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/a/a/a/a;

    invoke-direct {v0}, Lcom/a/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    .line 47
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vv:I

    .line 48
    const/16 v0, 0x7530

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vw:I

    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    const-string v1, "Accept"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vv:I

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vw:I

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->b(II)V

    .line 68
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/a/a;->setUserAgent(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    invoke-virtual {v0}, Lcom/a/a/a/a;->eO()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UserAgent set to: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "http.useragent"

    invoke-interface {v0, v3}, Lorg/apache/http/params/HttpParams;->getParameter(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vu:Ljava/lang/String;

    .line 73
    return-void
.end method

.method private aM(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vu:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    return-object v0
.end method

.method private i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 129
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    const-string v1, "Start mutual auth"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "javax.net.debug"

    const-string v1, "ssl"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    const-string v0, "javax.net.ssl.debug"

    const-string v1, "all"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 147
    const/4 v0, 0x0

    .line 149
    invoke-static {p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->l(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Alias : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    if-eqz v1, :cond_1

    .line 152
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    const-string v3, "CSR"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->j(Landroid/content/Context;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 154
    if-eqz v1, :cond_0

    .line 155
    new-instance v0, Lorg/apache/a/a/a/a;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/a/a/a/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 172
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vw:I

    invoke-virtual {v1, v2}, Lcom/a/a/a/a;->setTimeout(I)V

    .line 175
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    invoke-virtual {v1, v0}, Lcom/a/a/a/a;->a(Lorg/apache/a/a/a/a;)V

    .line 177
    return-void

    .line 156
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hP()Z

    move-result v2

    if-nez v2, :cond_2

    .line 157
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    const-string v3, "Non-TIMA-CCM"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->j(Landroid/content/Context;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 159
    if-eqz v1, :cond_0

    .line 160
    new-instance v0, Lorg/apache/a/a/a/a;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/a/a/a/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    goto :goto_0

    .line 165
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    const-string v1, "TIMA-CCM"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->hK()V

    .line 168
    new-instance v0, Lorg/apache/a/a/a/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/a/a/a/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    goto :goto_0
.end method

.method static j(Landroid/content/Context;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 295
    const/4 v0, 0x1

    new-array v2, v0, [Ljavax/net/ssl/KeyManager;

    const/4 v0, 0x0

    .line 296
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;

    invoke-direct {v3, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    aput-object v3, v2, v0

    .line 301
    :try_start_0
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 303
    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v2, v1, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 305
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    const-string v2, "SSL Context initialization done!"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_2

    .line 311
    :goto_0
    return-object v0

    .line 306
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 307
    :goto_1
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 308
    :catch_1
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 309
    :goto_2
    invoke-virtual {v1}, Ljava/security/KeyManagementException;->printStackTrace()V

    goto :goto_0

    .line 308
    :catch_2
    move-exception v1

    goto :goto_2

    .line 306
    :catch_3
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->aM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;

    invoke-direct {v2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    invoke-virtual {v0, v1, p2, v2}, Lcom/a/a/a/a;->b(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    .line 77
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Get request sent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->aM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->aM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;

    invoke-direct {v2, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->b(Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    .line 116
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    .locals 6

    .prologue
    .line 93
    :try_start_0
    new-instance v3, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v3, p2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->aM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "application/json"

    .line 95
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;

    invoke-direct {v5, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    .line 94
    invoke-virtual/range {v0 .. v5}, Lcom/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    .line 96
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Post request sent with String Entity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->aM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 97
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "post failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 106
    :try_start_0
    new-instance v3, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v3, p2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->aM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "application/json"

    .line 108
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;

    invoke-direct {v5, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    move-object v1, p4

    .line 107
    invoke-virtual/range {v0 .. v5}, Lcom/a/a/a/a;->b(Landroid/content/Context;Ljava/lang/String;Lorg/apache/http/HttpEntity;Ljava/lang/String;Lcom/a/a/a/m;)Lcom/a/a/a/i;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "put failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->vt:Lcom/a/a/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method
