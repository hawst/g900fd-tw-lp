.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;
.super Ljava/lang/Object;
.source "LoginScreenFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->a(Landroid/widget/EditText;I)V
.end annotation


# instance fields
.field final synthetic yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

.field private final synthetic yU:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yU:Landroid/widget/EditText;

    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 270
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yU:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yU:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 275
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    move-result-object v1

    .line 283
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Ljava/util/ArrayList;

    move-result-object v2

    .line 282
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z

    move-result v0

    .line 283
    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 289
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;->yT:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method
