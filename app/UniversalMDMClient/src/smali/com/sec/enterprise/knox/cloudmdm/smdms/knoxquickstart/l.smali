.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;
.super Landroid/app/Fragment;
.source "QuickstartWorkspaceCreatingFragment.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/g;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

.field private final tA:J

.field private tu:Landroid/app/Activity;

.field private tv:Landroid/widget/ProgressBar;

.field private tw:Landroid/widget/TextView;

.field private tx:Landroid/widget/TextView;

.field public ty:Landroid/os/Handler;

.field private tz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 49
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tA:J

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tv:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tw:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tx:Landroid/widget/TextView;

    return-object v0
.end method

.method private gH()V
    .locals 12

    .prologue
    const/high16 v11, 0x43960000    # 300.0f

    const/high16 v10, 0x43480000    # 200.0f

    const/16 v9, 0x1e

    const/16 v8, 0x14

    const/4 v7, 0x5

    .line 72
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09005f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 73
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f090067

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 74
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090062

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 76
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f090061

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 78
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    .line 79
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;-><init>(Landroid/content/res/Resources;)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getView()Landroid/view/View;

    move-result-object v6

    .line 81
    if-nez v6, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->it()I

    move-result v6

    if-nez v6, :cond_3

    .line 87
    const/4 v6, 0x2

    if-ne v4, v6, :cond_2

    .line 88
    invoke-virtual {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 89
    invoke-virtual {v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 90
    invoke-virtual {v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 91
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 92
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 93
    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    goto :goto_0

    .line 95
    :cond_2
    const/4 v6, 0x1

    if-ne v4, v6, :cond_0

    .line 96
    invoke-virtual {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v4, 0x46

    invoke-virtual {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 97
    invoke-virtual {v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 98
    invoke-virtual {v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 99
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 100
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 101
    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    goto/16 :goto_0

    .line 105
    :cond_3
    const/4 v6, 0x2

    if-ne v4, v6, :cond_4

    .line 106
    invoke-virtual {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v4, 0x23

    invoke-virtual {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 107
    invoke-virtual {v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 108
    invoke-virtual {v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 109
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 110
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 111
    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    goto/16 :goto_0

    .line 113
    :cond_4
    const/4 v6, 0x1

    if-ne v4, v6, :cond_0

    .line 114
    invoke-virtual {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v4, 0x46

    invoke-virtual {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 115
    invoke-virtual {v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 116
    invoke-virtual {v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 117
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 118
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 119
    invoke-virtual {v3}, Landroid/widget/ImageView;->requestLayout()V

    goto/16 :goto_0
.end method

.method static synthetic gp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    if-nez p1, :cond_0

    .line 225
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->TAG:Ljava/lang/String;

    const-string v1, "Message is null in updateUi"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :goto_0
    return v2

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->ty:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 230
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->hh()V

    .line 232
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->ty:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public hh()V
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->ty:Landroid/os/Handler;

    .line 163
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 207
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->TAG:Ljava/lang/String;

    const-string v1, "onAttach "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 210
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_0
    return-void

    .line 211
    :catch_0
    move-exception v0

    .line 212
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 68
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->gH()V

    .line 69
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 58
    const v0, 0x7f030017

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 59
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->setRetainInstance(Z)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tu:Landroid/app/Activity;

    .line 61
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 219
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->stop()V

    .line 220
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x3

    .line 167
    invoke-super {p0, p1}, Landroid/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getView()Landroid/view/View;

    move-result-object v1

    .line 171
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->ty:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->hh()V

    .line 175
    :cond_0
    new-array v2, v4, [I

    fill-array-data v2, :array_0

    .line 176
    new-array v3, v4, [I

    fill-array-data v3, :array_1

    .line 177
    new-array v4, v4, [Landroid/widget/ImageView;

    const v0, 0x7f090063

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v4, v6

    const/4 v5, 0x1

    const v0, 0x7f090064

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v4, v5

    const/4 v5, 0x2

    const v0, 0x7f090065

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v4, v5

    .line 178
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;

    invoke-direct {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;-><init>()V

    .line 179
    const v0, 0x7f090061

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v5, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->a(Landroid/widget/ImageView;[I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;

    move-result-object v2

    .line 181
    const v0, 0x7f090066

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->a(Landroid/widget/TextView;[I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;

    move-result-object v0

    .line 182
    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->b(J)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;

    move-result-object v0

    .line 183
    const v2, 0x7f020016

    const v3, 0x7f02001a

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->a([Landroid/widget/ImageView;II)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->jI()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    move-result-object v0

    .line 179
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    .line 185
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->start()V

    .line 186
    const v0, 0x7f090068

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tv:Landroid/widget/ProgressBar;

    .line 187
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tv:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 188
    const v0, 0x7f09008f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tw:Landroid/widget/TextView;

    .line 189
    const v0, 0x7f090069

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->tx:Landroid/widget/TextView;

    .line 191
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_1

    .line 193
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 194
    const/16 v2, 0x64

    iput v2, v1, Landroid/os/Message;->what:I

    .line 195
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 196
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->handleMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->gH()V

    .line 203
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 199
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->TAG:Ljava/lang/String;

    const-string v2, "Error occured"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 175
    nop

    :array_0
    .array-data 4
        0x7f02002d
        0x7f020029
        0x7f02002c
    .end array-data

    .line 176
    :array_1
    .array-data 4
        0x7f08001c
        0x7f08001d
        0x7f08001e
    .end array-data
.end method
