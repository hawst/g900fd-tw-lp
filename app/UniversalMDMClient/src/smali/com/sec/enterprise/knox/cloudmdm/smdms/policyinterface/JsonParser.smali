.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;
.super Ljava/lang/Object;
.source "JsonParser.java"


# static fields
.field public static gAlertGson:Lcom/google/gson/e;

.field public static gCommandResponseExternalGson:Lcom/google/gson/e;

.field public static gCommandResponseGson:Lcom/google/gson/e;

.field public static gGson:Lcom/google/gson/e;

.field public static gPolicyResponseExternalGson:Lcom/google/gson/e;

.field public static gPolicyResponseGson:Lcom/google/gson/e;

.field private static mCommandResponseIgnoreItems:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "stateData"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->mCommandResponseIgnoreItems:[Ljava/lang/String;

    .line 57
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 58
    invoke-virtual {v0}, Lcom/google/gson/g;->dJ()Lcom/google/gson/g;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 57
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    .line 63
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 64
    invoke-virtual {v0}, Lcom/google/gson/g;->dJ()Lcom/google/gson/g;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;

    invoke-direct {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Lcom/google/gson/b;)Lcom/google/gson/g;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForAlertReportClass;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForAlertReportClass;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 63
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gAlertGson:Lcom/google/gson/e;

    .line 70
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 71
    invoke-virtual {v0}, Lcom/google/gson/g;->dJ()Lcom/google/gson/g;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;

    invoke-direct {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Lcom/google/gson/b;)Lcom/google/gson/g;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForPolicyResponseClass;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForPolicyResponseClass;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 70
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gPolicyResponseGson:Lcom/google/gson/e;

    .line 77
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 78
    invoke-virtual {v0}, Lcom/google/gson/g;->dJ()Lcom/google/gson/g;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;

    invoke-direct {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Lcom/google/gson/b;)Lcom/google/gson/g;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForCommandResponseClass;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForCommandResponseClass;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 77
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gCommandResponseGson:Lcom/google/gson/e;

    .line 84
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 85
    invoke-virtual {v0}, Lcom/google/gson/g;->dJ()Lcom/google/gson/g;

    move-result-object v0

    .line 86
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;

    invoke-direct {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Lcom/google/gson/b;)Lcom/google/gson/g;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForPolicyResponseExternalClass;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForPolicyResponseExternalClass;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 84
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gPolicyResponseExternalGson:Lcom/google/gson/e;

    .line 91
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 92
    invoke-virtual {v0}, Lcom/google/gson/g;->dJ()Lcom/google/gson/g;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;

    invoke-direct {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$serializationExclusionStrategy;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/g;->a(Lcom/google/gson/b;)Lcom/google/gson/g;

    move-result-object v0

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForCommandResponseExternalClass;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForCommandResponseExternalClass;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    .line 91
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gCommandResponseExternalGson:Lcom/google/gson/e;

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->mCommandResponseIgnoreItems:[Ljava/lang/String;

    return-object v0
.end method
