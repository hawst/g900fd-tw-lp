.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;
.super Ljava/lang/Object;
.source "AdminManager.java"


# static fields
.field public static final CLOUD_ADMIN_CLASSNAME:Ljava/lang/String; = "Admin"

.field private static final TAG:Ljava/lang/String; = "UMC:AdminManager"

.field public static gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;


# instance fields
.field private DEFAULT_DOCUMENTID:Ljava/lang/String;

.field final MAX_NUM_PROXY_ADMINS:I

.field private mContext:Landroid/content/Context;

.field private mDefaultListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

.field private mThreadContext:Landroid/os/Handler;

.field private mTypeConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->MAX_NUM_PROXY_ADMINS:I

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    .line 73
    const-string v0, "66666666-5555-4444-3333-2222222222"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->DEFAULT_DOCUMENTID:Ljava/lang/String;

    .line 133
    return-void
.end method

.method private addLicenseMonitorAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    .locals 5

    .prologue
    .line 511
    const-string v0, "UMC:AdminManager"

    const-string v1, "addLicenseMonitorAlert  "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const-string v0, "EnterpriseLicenseManager.activateLicense"

    .line 513
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 514
    const-string v2, "licenseKey"

    const-string v3, " "

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    const/4 v2, 0x0

    .line 517
    :try_start_0
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v3, v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/Map;)V

    .line 518
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->DEFAULT_DOCUMENTID:Ljava/lang/String;

    .line 519
    const/4 v1, 0x0

    .line 520
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v1, v0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/Boolean;)V

    .line 521
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAlertManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    move-result-object v0

    .line 522
    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->addAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    :goto_0
    return-void

    .line 523
    :catch_0
    move-exception v0

    .line 524
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 525
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error occured while adding license monitor alert   "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 80
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->gAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    return-object v0
.end method

.method private removeLicenseMonitorAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    .locals 4

    .prologue
    .line 531
    const-string v0, "UMC:AdminManager"

    const-string v1, "removeLicenseMonitorAlert  "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    const-string v0, "EnterpriseLicenseManager.activateLicense"

    .line 533
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 534
    const-string v2, "licenseKey"

    const-string v3, " "

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    const/4 v2, 0x0

    .line 537
    :try_start_0
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v3, v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/Map;)V

    .line 538
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAlertManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    move-result-object v0

    .line 539
    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->findAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    move-result-object v1

    .line 540
    if-nez v1, :cond_0

    .line 541
    const-string v0, "UMC:AdminManager"

    const-string v1, "unable to find alert and removeLicenseMonitorAlert failed"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :goto_0
    return-void

    .line 544
    :cond_0
    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->removeAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 545
    :catch_0
    move-exception v0

    .line 546
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error occured while remove license monitor alert   "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private startPolicyHandlers()V
    .locals 2

    .prologue
    .line 491
    const-string v0, "UMC:AdminManager"

    const-string v1, "startPolicyHandlers"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAdminMap()Ljava/util/Map;

    move-result-object v0

    .line 493
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    return-void

    .line 493
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 494
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 495
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    .line 496
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->start()V

    goto :goto_0
.end method


# virtual methods
.method public addCloudMDMAdmin(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ILandroid/content/ComponentName;[Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 137
    const-string v1, "UMC:AdminManager"

    const-string v2, "addCloudMDMAdmin"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    if-eqz p7, :cond_0

    array-length v1, p7

    if-nez v1, :cond_1

    .line 140
    :cond_0
    const-string v1, "UMC:AdminManager"

    const-string v2, "Warning: Admin Add Request received with out permissions"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 145
    if-eqz v1, :cond_2

    .line 146
    const-string v1, "UMC:AdminManager"

    const-string v2, "Admin Already Exists"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :goto_0
    return v0

    .line 150
    :cond_2
    invoke-static/range {p0 .. p7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->createCloudAdminInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ILandroid/content/ComponentName;[Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 152
    if-eqz v1, :cond_4

    .line 153
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v2, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->addAdmin(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V

    .line 154
    const-string v2, "UMC:AdminManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Enrolled Admin Count : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->adminCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->adminCount()I

    move-result v2

    if-ne v2, v0, :cond_3

    .line 156
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 160
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addLicenseMonitorAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 167
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addCloudMDMAdminShareOwnershipWithLocalAdmin(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 172
    const-string v1, "UMC:AdminManager"

    const-string v2, "addCloudMDMAdminShareOwnershipWithLocalAdmin"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 175
    if-eqz v1, :cond_0

    .line 176
    const-string v1, "UMC:AdminManager"

    const-string v2, "Admin Already Exists"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_0
    return v0

    .line 180
    :cond_0
    invoke-static/range {p0 .. p7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->createCloudAdminInstanceSharingOwnershipWithLocalAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 182
    if-eqz v1, :cond_2

    .line 183
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v2, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->addAdmin(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V

    .line 184
    const-string v2, "UMC:AdminManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Enrolled Admin Count : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->adminCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->adminCount()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 186
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v3

    iget v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 190
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addLicenseMonitorAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 197
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addCloudMDMAdminShareOwnershipWithLocalAdminWithOutProxyAdmin(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 201
    const-string v1, "UMC:AdminManager"

    const-string v2, "addCloudMDMAdminShareOwnershipWithLocalAdminWithOutProxyAdmin"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 204
    if-eqz v1, :cond_0

    .line 205
    const-string v1, "UMC:AdminManager"

    const-string v2, "Admin Already Exists"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :goto_0
    return v0

    .line 209
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->createCloudAdminInstanceSharingOwnershipWithLocalAdminWithoutProxyAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 210
    if-eqz v1, :cond_2

    .line 211
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v2, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->addAdmin(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V

    .line 212
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->adminCount()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 213
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 217
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addLicenseMonitorAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 224
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addUser(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 274
    const-string v1, "UMC:AdminManager"

    const-string v2, "addUser"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 277
    if-nez v1, :cond_0

    .line 286
    :goto_0
    return v0

    .line 279
    :cond_0
    invoke-virtual {v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->addUser(Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->saveAdminConfig()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    const/4 v0, 0x1

    goto :goto_0

    .line 282
    :catch_0
    move-exception v1

    .line 283
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public clearThreadContext()V
    .locals 2

    .prologue
    .line 553
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdminCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 554
    const-string v0, "UMC:AdminManager"

    const-string v1, "admin count is 0 and quit the thread"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 556
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    .line 558
    :cond_0
    return-void
.end method

.method public formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Landroid/content/ComponentName;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->formPackageNameFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Admin"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    return-object v0
.end method

.method public getAdminCount()I
    .locals 2

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    .line 266
    :cond_0
    return v0
.end method

.method public getAllAdminIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 341
    if-nez v0, :cond_0

    .line 342
    const/4 v0, 0x0

    .line 343
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDefaultPolicyListener()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mDefaultListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    return-object v0
.end method

.method public getDummyParentComponentName()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Admin"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    return-object v0
.end method

.method public getNextProxyUID()I
    .locals 6

    .prologue
    .line 84
    .line 85
    const/16 v1, 0x4e20

    .line 86
    const/16 v3, 0x5208

    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->fi()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->getProxyAdmins(I)Ljava/util/List;

    move-result-object v4

    .line 89
    :goto_0
    if-lt v1, v3, :cond_0

    .line 102
    const/4 v0, -0x1

    :goto_1
    return v0

    .line 90
    :cond_0
    const/4 v2, 0x0

    .line 91
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 98
    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 99
    goto :goto_1

    .line 91
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 92
    invoke-virtual {v0}, Landroid/app/admin/ProxyDeviceAdminInfo;->getReceiver()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 93
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getAppId(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 94
    const/4 v0, 0x1

    .line 95
    goto :goto_2

    .line 89
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 320
    if-nez v0, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 322
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    goto :goto_0
.end method

.method public getPolicyScriptHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 334
    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 336
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyScriptHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;

    move-result-object v0

    goto :goto_0
.end method

.method public getProfileHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 327
    if-nez v0, :cond_0

    .line 328
    const/4 v0, 0x0

    .line 329
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getProfileHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    move-result-object v0

    goto :goto_0
.end method

.method public getThreadContext()Landroid/os/Handler;
    .locals 3

    .prologue
    .line 501
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 502
    const-string v0, "UMC:AdminManager"

    const-string v1, "new theread is created"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PolicyApplierCommonThread"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 504
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 505
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mThreadContext:Landroid/os/Handler;

    return-object v0
.end method

.method public hasAnyPendingAlertRequests()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 359
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v2

    .line 360
    if-nez v2, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 363
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 364
    aget-object v3, v2, v0

    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->hasAnyPendingAlertRequests()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 365
    const/4 v1, 0x1

    goto :goto_0

    .line 363
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public hasAnyPendingPolicies()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 347
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v2

    .line 348
    if-nez v2, :cond_1

    .line 355
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 351
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 352
    aget-object v3, v2, v0

    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->hasAnyPendingPolicies()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 353
    const/4 v1, 0x1

    goto :goto_0

    .line 351
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public hasEnrolledUsers(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 312
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 313
    if-nez v0, :cond_0

    .line 314
    const/4 v0, 0x0

    .line 315
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->hasManagedUsers()Z

    move-result v0

    goto :goto_0
.end method

.method public init(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V
    .locals 2

    .prologue
    .line 114
    const-string v0, "UMC:AdminManager"

    const-string v1, "init - start"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    .line 117
    const-string v0, "enterprise_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 116
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 118
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    .line 120
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mTypeConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;

    .line 121
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mTypeConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;->init(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;)V

    .line 122
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mDefaultListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    .line 123
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->loadAdmins()Z

    move-result v0

    .line 124
    if-nez v0, :cond_0

    .line 125
    const-string v0, "UMC:AdminManager"

    const-string v1, "loadAdmins failed"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->startPolicyHandlers()V

    .line 128
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    .line 129
    const-string v0, "UMC:AdminManager"

    const-string v1, "init - end"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public isAdminExist(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUserExist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 305
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 306
    if-nez v0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 308
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->isUserExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method declared-synchronized loadAdmins()Z
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 393
    monitor-enter p0

    :try_start_0
    const-string v0, "UMC:AdminManager"

    const-string v1, "loadAdmins"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    :try_start_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 396
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    .line 397
    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 396
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 399
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getActiveAdmins()Ljava/util/List;

    move-result-object v0

    .line 398
    check-cast v0, Ljava/util/ArrayList;

    .line 401
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAdminMap()Ljava/util/Map;

    move-result-object v7

    .line 402
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->getProxyAdmins(I)Ljava/util/List;

    move-result-object v8

    .line 403
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->loadAdminConfig()Ljava/util/Map;

    move-result-object v1

    .line 404
    if-eqz v1, :cond_1

    .line 405
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 451
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->removeOutOfSyncProxyAdmins()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457
    :goto_0
    monitor-exit p0

    return v4

    .line 405
    :cond_2
    :try_start_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 406
    const-string v3, "UMC:AdminManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v2, "Loading Admin - UID = "

    invoke-direct {v10, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6

    move v3, v5

    .line 421
    :goto_1
    if-eqz v0, :cond_5

    .line 422
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7

    .line 443
    :cond_5
    :goto_3
    if-nez v3, :cond_0

    .line 444
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "loadAdmins - Serious Unrecoverable Error : Enrollment Repo went out of sync with EDM DB - UMC Admin got removed in the Platform Side"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 445
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 452
    :catch_0
    move-exception v0

    .line 453
    :try_start_3
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 408
    :cond_6
    :try_start_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 409
    invoke-virtual {v2}, Landroid/app/admin/ProxyDeviceAdminInfo;->getType()I

    move-result v3

    const/4 v11, 0x2

    if-ne v3, v11, :cond_3

    .line 410
    invoke-virtual {v2}, Landroid/app/admin/ProxyDeviceAdminInfo;->getReceiver()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 411
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    if-ne v11, v3, :cond_3

    .line 412
    new-instance v10, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    invoke-direct {v10, p0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Landroid/app/admin/ProxyDeviceAdminInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;)V

    .line 413
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v7, v2, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    const-string v2, "UMC:AdminManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, "loadAdmins - UMC Admin Added for Admin UID = "

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 416
    goto/16 :goto_1

    .line 422
    :cond_7
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 423
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 424
    const-string v2, "UMC:AdminManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Current admin loop :"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 425
    if-eqz v11, :cond_4

    if-eqz v6, :cond_4

    .line 427
    const/4 v2, 0x0

    :try_start_5
    invoke-virtual {v6, v11, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 428
    if-eqz v2, :cond_4

    iget v12, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    if-ne v12, v2, :cond_4

    .line 429
    new-instance v12, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    const/4 v13, 0x0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    invoke-direct {v12, p0, v13, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Landroid/app/admin/ProxyDeviceAdminInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;)V

    .line 430
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v7, v2, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 432
    :try_start_6
    const-string v3, "UMC:AdminManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v2, "loadAdmins - UMC Admin Added for Admin UID = "

    invoke-direct {v12, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v3, v4

    .line 433
    goto/16 :goto_3

    .line 435
    :catch_1
    move-exception v2

    move v2, v3

    .line 436
    :goto_4
    :try_start_7
    const-string v3, "UMC:AdminManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Admin not found in PackageManager : PackageName = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v3, v2

    goto/16 :goto_2

    .line 435
    :catch_2
    move-exception v2

    move v2, v4

    goto :goto_4
.end method

.method public removeAdmin(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 228
    const-string v0, "UMC:AdminManager"

    const-string v1, "removeAdmin"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_1

    .line 233
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v1

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    .line 234
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->deleteAdmin()V

    .line 235
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->removeAdmin(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->adminCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->clearThreadContext()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :cond_0
    const/4 v0, 0x1

    .line 248
    :goto_0
    return v0

    .line 243
    :catch_0
    move-exception v0

    .line 244
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 248
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method removeOutOfSyncProxyAdmins()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 461
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->getAdminMap()Ljava/util/Map;

    move-result-object v4

    .line 462
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getProxyAdmins(I)Ljava/util/List;

    move-result-object v0

    .line 463
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 488
    return-void

    .line 463
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 464
    invoke-virtual {v0}, Landroid/app/admin/ProxyDeviceAdminInfo;->getType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 465
    invoke-virtual {v0}, Landroid/app/admin/ProxyDeviceAdminInfo;->getReceiver()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 466
    invoke-static {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUserId(I)I

    move-result v1

    .line 467
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    if-eq v2, v1, :cond_2

    .line 468
    const-string v0, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "UMC in user "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ignoring uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :cond_2
    const/4 v1, 0x0

    .line 472
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v1

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    .line 477
    if-nez v2, :cond_0

    .line 478
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v1, v6}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 479
    new-instance v2, Landroid/app/enterprise/EnterpriseDeviceManager;

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mContext:Landroid/content/Context;

    .line 480
    const/4 v8, 0x0

    .line 479
    invoke-direct {v2, v7, v1, v8}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 481
    invoke-virtual {v2, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->setAdminRemovable(Z)Z

    .line 482
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/admin/ProxyDeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUserId(I)I

    move-result v7

    invoke-virtual {v1, v2, v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->deactivateAdminForUser(Landroid/content/ComponentName;I)V

    .line 483
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v1, v6}, Landroid/app/enterprise/EnterpriseDeviceManager;->removeProxyAdmin(I)V

    .line 484
    const-string v1, "UMC:AdminManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "EDM Proxy Admin Removed UID = "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "and package name = "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/admin/ProxyDeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 472
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 473
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v1

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    if-ne v6, v1, :cond_3

    move v2, v3

    .line 474
    goto :goto_1
.end method

.method public removeUser(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 290
    const-string v1, "UMC:AdminManager"

    const-string v2, "removeUser"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    .line 293
    if-nez v1, :cond_0

    .line 301
    :goto_0
    return v0

    .line 295
    :cond_0
    invoke-virtual {v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->removeUser(Ljava/lang/String;)V

    .line 296
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mEnrolledAdmins:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/EnrolledAdminRepo;->saveAdminConfig()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    const/4 v0, 0x1

    goto :goto_0

    .line 298
    :catch_0
    move-exception v1

    .line 299
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setDefaultPolicyListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->mDefaultListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    .line 373
    return-void
.end method

.method public updateAdminPermission(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 253
    if-nez v0, :cond_0

    .line 254
    const/4 v0, 0x0

    .line 255
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->updateAdminPermission([Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
