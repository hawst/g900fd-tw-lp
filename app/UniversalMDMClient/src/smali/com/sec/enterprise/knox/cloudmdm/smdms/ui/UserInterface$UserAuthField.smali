.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;
.super Ljava/lang/Object;
.source "UserInterface.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private authType:I

.field private zs:I

.field private zt:I

.field private zu:Ljava/lang/String;

.field private zv:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 215
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 223
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zs:I

    .line 144
    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zt:I

    .line 145
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zv:Ljava/lang/String;

    .line 146
    iput p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->authType:I

    .line 147
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zu:Ljava/lang/String;

    .line 148
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zs:I

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zt:I

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->authType:I

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zu:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zv:Ljava/lang/String;

    .line 231
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/content/Context;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/EditText;",
            ">;",
            "Landroid/widget/LinearLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 309
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-lt v1, v0, :cond_0

    .line 354
    return-void

    .line 312
    :cond_0
    :try_start_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 313
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f03001d

    .line 315
    :goto_1
    const/4 v3, 0x0

    .line 313
    invoke-virtual {v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 317
    const v0, 0x7f090013

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 316
    check-cast v0, Landroid/widget/TextView;

    .line 318
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->jA()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 320
    const v0, 0x7f090014

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 319
    check-cast v0, Landroid/widget/EditText;

    .line 321
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->getAuthType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 340
    :goto_2
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->jB()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 341
    const v3, 0x7f090001

    aget-object v4, p1, v1

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->jD()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/EditText;->setTag(ILjava/lang/Object;)V

    .line 342
    const/high16 v3, 0x7f090000

    aget-object v4, p1, v1

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->getAuthType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/EditText;->setTag(ILjava/lang/Object;)V

    .line 343
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->jC()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 344
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->jC()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 345
    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->jC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 347
    :cond_1
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    invoke-virtual {p3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 309
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 315
    :cond_2
    const v0, 0x7f030007

    goto :goto_1

    .line 323
    :pswitch_0
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 349
    :catch_0
    move-exception v0

    .line 350
    const-string v2, "UMC:UserInterface"

    const-string v3, "Error in inflating dynamic uaf Field"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 326
    :pswitch_1
    :try_start_1
    const-string v3, "UMC:UserInterface"

    const-string v4, "Going to set this for password"

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/high16 v3, 0x12000000

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 329
    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 330
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    goto :goto_2

    .line 333
    :pswitch_2
    const/16 v3, 0x21

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    goto/16 :goto_2

    .line 336
    :pswitch_3
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 337
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 321
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/EditText;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 244
    .line 245
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 246
    const-string v0, "UMC:UserInterface"

    const-string v1, "EditTexts not added properly"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_0
    return v3

    .line 249
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_2
    move v3, v1

    .line 296
    goto :goto_0

    .line 249
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 250
    if-nez v0, :cond_2

    .line 251
    const-string v0, "UMC:UserInterface"

    const-string v5, "Null field found but handling the error by ignoring the field"

    invoke-static {v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 254
    :cond_2
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    .line 259
    goto :goto_2

    .line 261
    :cond_3
    const/high16 v5, 0x7f090000

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 262
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 263
    packed-switch v5, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 285
    :pswitch_1
    const-string v1, "^[0-9]*$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    move v1, v0

    goto :goto_1

    .line 265
    :pswitch_2
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    move v1, v0

    .line 273
    goto :goto_1

    :pswitch_3
    move v1, v2

    .line 283
    goto :goto_1

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static final d(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 234
    if-nez p0, :cond_0

    .line 235
    const/4 v0, 0x0

    .line 237
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zp:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthType()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->authType:I

    return v0
.end method

.method public jA()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zs:I

    return v0
.end method

.method public jB()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zt:I

    return v0
.end method

.method public jC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zu:Ljava/lang/String;

    return-object v0
.end method

.method public jD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zv:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 209
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->authType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zu:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->zv:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    return-void
.end method
