.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PolicyIntentReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:PolicyIntentReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 14
    const-string v0, "UMC:PolicyIntentReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent received by UMC Action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    move-result-object v0

    .line 16
    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->dispatch(Landroid/content/Intent;)V

    .line 17
    return-void
.end method
