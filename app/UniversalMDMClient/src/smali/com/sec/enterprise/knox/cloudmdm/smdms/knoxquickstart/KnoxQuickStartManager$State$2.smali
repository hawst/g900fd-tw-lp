.class enum Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;
.source "KnoxQuickStartManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;
.end annotation


# static fields
.field private static synthetic sH:[I


# instance fields
.field private mEnrollmentRequested:Z

.field private mEulaAccepted:Z

.field private mShowPasswordScreen:Z


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;-><init>(Ljava/lang/String;ILcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 143
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mShowPasswordScreen:Z

    .line 144
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    .line 145
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEnrollmentRequested:Z

    .line 1
    return-void
.end method

.method static synthetic hd()[I
    .locals 3

    .prologue
    .line 141
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sH:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sP:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sK:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sW:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sL:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sM:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sV:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sO:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sT:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sR:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sI:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sS:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_10
    :try_start_10
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sU:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_11
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sH:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_11

    :catch_1
    move-exception v1

    goto :goto_10

    :catch_2
    move-exception v1

    goto :goto_f

    :catch_3
    move-exception v1

    goto :goto_e

    :catch_4
    move-exception v1

    goto :goto_d

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v1

    goto :goto_b

    :catch_7
    move-exception v1

    goto :goto_a

    :catch_8
    move-exception v1

    goto :goto_9

    :catch_9
    move-exception v1

    goto :goto_8

    :catch_a
    move-exception v1

    goto :goto_7

    :catch_b
    move-exception v1

    goto/16 :goto_6

    :catch_c
    move-exception v1

    goto/16 :goto_5

    :catch_d
    move-exception v1

    goto/16 :goto_4

    :catch_e
    move-exception v1

    goto/16 :goto_3

    :catch_f
    move-exception v1

    goto/16 :goto_2

    :catch_10
    move-exception v1

    goto/16 :goto_1
.end method


# virtual methods
.method A(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 371
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 372
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    .line 373
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    const v2, 0x7f080099

    .line 375
    const v3, 0x7f08009d

    const v4, 0x7f080058

    .line 374
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 376
    const-string v1, "UMC:KnoxQuickStartManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processMdmServerSelection: Already enroled:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 377
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 376
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 387
    :goto_0
    return-void

    .line 380
    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEulas()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 381
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEulas()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(Ljava/util/List;)V

    goto :goto_0

    .line 383
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 384
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->v(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 315
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 316
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processMessage In STATE_QUICKSTART_ENROLLMENT: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->hd()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 359
    :pswitch_0
    const-string v0, "UMC:KnoxQuickStartManager"

    .line 360
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled message in state, STATE_ENROLLMENT, Message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 360
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 359
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    const v2, 0x7f0800a2

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 363
    const/4 v2, 0x1

    .line 362
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 364
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 365
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    .line 368
    :goto_0
    return-void

    .line 319
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->r(Ljava/lang/Object;)V

    goto :goto_0

    .line 322
    :pswitch_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 323
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->z(Ljava/lang/Object;)V

    goto :goto_0

    .line 326
    :pswitch_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 327
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->A(Ljava/lang/Object;)V

    goto :goto_0

    .line 330
    :pswitch_4
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Msg: User Retry"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 332
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gM()V

    goto :goto_0

    .line 335
    :pswitch_5
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 336
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->v(Ljava/lang/Object;)V

    goto :goto_0

    .line 339
    :pswitch_6
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 340
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->y(Ljava/lang/Object;)V

    goto :goto_0

    .line 343
    :pswitch_7
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 344
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->s(Ljava/lang/Object;)V

    goto :goto_0

    .line 347
    :pswitch_8
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 348
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 349
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->x(Ljava/lang/Object;)V

    goto :goto_0

    .line 352
    :pswitch_9
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 353
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    goto/16 :goto_0

    .line 356
    :pswitch_a
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    goto/16 :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_4
        :pswitch_5
        :pswitch_a
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method

.method he()V
    .locals 8

    .prologue
    .line 421
    const/4 v0, 0x2

    new-array v6, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    .line 422
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v0

    iget-object v5, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    .line 423
    const v2, 0x7f080075

    .line 424
    const v1, 0x7f080077

    .line 425
    const/4 v7, 0x0

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    .line 426
    const-string v3, "username"

    .line 427
    const/4 v4, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;-><init>(IILjava/lang/String;ILjava/lang/String;)V

    .line 425
    aput-object v0, v6, v7

    .line 428
    const v2, 0x7f080074

    .line 429
    const v1, 0x7f080076

    .line 430
    const/4 v7, 0x1

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    const-string v3, "password"

    .line 431
    const/4 v4, 0x4

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;-><init>(IILjava/lang/String;ILjava/lang/String;)V

    .line 430
    aput-object v0, v6, v7

    .line 433
    if-eqz v6, :cond_0

    .line 434
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    .line 435
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 436
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(Ljava/lang/String;Ljava/lang/String;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V

    .line 438
    :cond_0
    return-void
.end method

.method p(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 149
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "In STATE_QUICKSTART_ENROLLMENT"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    const v2, 0x7f08006c

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 153
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->m(Landroid/content/Context;)V

    .line 154
    return-void
.end method

.method q(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 158
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mShowPasswordScreen:Z

    .line 159
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    .line 160
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEnrollmentRequested:Z

    .line 161
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 162
    return-object p1
.end method

.method public r(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const v6, 0x7f0800a4

    const v5, 0x7f080099

    const v4, 0x7f080058

    const/4 v3, 0x1

    .line 166
    check-cast p1, Ljava/lang/Integer;

    .line 167
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processDeviceVersionCheck: result  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 171
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 172
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 173
    const v1, 0x7f0800a6

    const v2, 0x7f0800a7

    .line 174
    const v3, 0x7f080053

    .line 172
    invoke-virtual {v0, v6, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->b(IIII)V

    .line 192
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 177
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 178
    const v1, 0x7f0800a9

    .line 177
    invoke-virtual {v0, v6, v1, v4, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0

    .line 182
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 184
    const v1, 0x7f080081

    .line 183
    invoke-virtual {v0, v5, v1, v4, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0

    .line 185
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 187
    const v1, 0x7f080015

    .line 186
    invoke-virtual {v0, v5, v1, v4, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0

    .line 189
    :cond_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method s(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x2

    const v6, 0x7f080099

    const/4 v4, -0x1

    const v5, 0x7f080058

    .line 196
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v0

    if-nez v0, :cond_0

    .line 197
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 198
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 199
    const v1, 0x7f08009c

    .line 198
    invoke-virtual {v0, v6, v1, v5, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 200
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Invaild auth cred dialog shown"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 206
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 207
    const v1, 0x7f08009f

    const/4 v2, 0x1

    .line 206
    invoke-virtual {v0, v6, v1, v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 208
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    goto :goto_0

    .line 212
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v2

    .line 214
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 215
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;->getPem()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 216
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;->getPem()Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gX()Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v1

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->l(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 218
    if-eq v0, v7, :cond_2

    .line 219
    if-eq v0, v4, :cond_2

    .line 220
    const/4 v1, -0x3

    if-ne v0, v1, :cond_4

    .line 223
    :cond_2
    const-string v1, ""

    .line 224
    if-ne v0, v7, :cond_3

    .line 225
    const v0, 0x7f080098

    .line 226
    const-string v7, "MDM Server Certificate Expired"

    .line 231
    :goto_1
    const-string v1, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processEnrollmentResponse: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v6, v0, v5, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 233
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    .line 234
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;->xq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    .line 235
    const-string v5, "Error Report"

    .line 236
    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->toString()Ljava/lang/String;

    move-result-object v6

    .line 233
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    goto/16 :goto_0

    .line 228
    :cond_3
    const v0, 0x7f080097

    .line 229
    const-string v7, "MDM Server Certificate Validation Failed"

    goto :goto_1

    .line 242
    :cond_4
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->al(Ljava/lang/String;)V

    .line 244
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gY()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 245
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 246
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->f(II)V

    .line 247
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V

    .line 248
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V

    .line 249
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gY()Ljava/lang/String;

    move-result-object v1

    const-string v3, "vendor.apk"

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v2

    .line 248
    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "sent intent to start MDM agent download and install "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 252
    :cond_5
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 253
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    const v1, 0x7f08009a

    .line 254
    const v2, 0x7f0800a0

    .line 253
    invoke-virtual {v0, v1, v2, v5, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 255
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Mdm Agent URL is Null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method v(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 286
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 287
    if-eqz v0, :cond_2

    .line 288
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    if-eqz v0, :cond_0

    .line 289
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "EULA has already been accepted"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :goto_0
    return-void

    .line 293
    :cond_0
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KNOX Container EULA Shown"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->T(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->c(Landroid/content/Context;Z)Z

    .line 295
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KLM EULA Shown"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->U(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->d(Landroid/content/Context;Z)Z

    .line 297
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mShowPasswordScreen:Z

    if-eqz v0, :cond_1

    .line 298
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    .line 299
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->he()V

    goto :goto_0

    .line 301
    :cond_1
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    .line 302
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 303
    const-string v1, "username"

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    const-string v1, "password"

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->ie()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;->getInitialPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->y(Ljava/lang/Object;)V

    goto :goto_0

    .line 308
    :cond_2
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Eula is not accepted and doing shutdown  "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    goto/16 :goto_0
.end method

.method x(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const v6, 0x7f080099

    const v5, 0x7f080058

    const/4 v4, 0x0

    .line 390
    iput-boolean v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    .line 391
    iput-boolean v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEnrollmentRequested:Z

    move-object v0, p1

    .line 392
    check-cast v0, Landroid/os/Message;

    .line 393
    iget v2, v0, Landroid/os/Message;->arg1:I

    .line 394
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 395
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 396
    const/16 v3, 0x194

    if-ne v2, v3, :cond_0

    .line 397
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    .line 398
    const v3, 0x7f08009b

    .line 397
    invoke-virtual {v1, v6, v3, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 416
    :goto_0
    const-string v1, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processNetworkError: errorcode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 418
    return-void

    .line 399
    :cond_0
    const/16 v3, 0x191

    if-ne v2, v3, :cond_1

    .line 400
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    .line 401
    const v3, 0x7f08009c

    .line 400
    invoke-virtual {v1, v6, v3, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0

    .line 402
    :cond_1
    const/16 v3, 0x199

    if-ne v2, v3, :cond_3

    .line 403
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 404
    iput-boolean v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mShowPasswordScreen:Z

    .line 405
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->A(Ljava/lang/Object;)V

    goto :goto_0

    .line 407
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    .line 408
    const v3, 0x7f08009d

    .line 407
    invoke-virtual {v1, v6, v3, v5, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 409
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    goto :goto_0

    .line 412
    :cond_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gU()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I

    move-result v1

    .line 413
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v3

    invoke-virtual {v3, v6, v1, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0
.end method

.method y(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEnrollmentRequested:Z

    if-eqz v0, :cond_1

    .line 442
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "Enrollment has already been requested"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEnrollmentRequested:Z

    .line 447
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gT()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    const v2, 0x7f080079

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 449
    check-cast p1, Ljava/util/HashMap;

    .line 450
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "username"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "password"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 451
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->O(Ljava/lang/String;)V

    .line 454
    :try_start_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->d(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v0

    .line 455
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->N(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :goto_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->if()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 462
    if-eqz v1, :cond_0

    .line 464
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getMessenger()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->xg:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 465
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gZ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gZ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 466
    :cond_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V

    .line 476
    :cond_3
    :goto_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 477
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getMessenger()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->xg:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 478
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gZ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gZ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendEnrollmentRequest:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gZ()Ljava/lang/String;

    move-result-object v3

    .line 481
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fJ()Ljava/lang/String;

    move-result-object v5

    .line 480
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 456
    :catch_0
    move-exception v0

    .line 457
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 469
    :cond_4
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ha()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ha()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 471
    :cond_5
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hb()Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->gi()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->K(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 484
    :cond_6
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ha()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ha()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendEnrollmentRequest:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->ha()Ljava/lang/String;

    move-result-object v3

    .line 487
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->fJ()Ljava/lang/String;

    move-result-object v5

    .line 486
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method z(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 263
    if-nez p1, :cond_0

    .line 264
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "obj is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 268
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gX()Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->init()Z

    .line 270
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "sendCreateUserRequest"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mShowPasswordScreen:Z

    .line 272
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEulaAccepted:Z

    .line 273
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;->mEnrollmentRequested:Z

    .line 274
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;)V

    .line 277
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gW()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gV()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->server:Ljava/lang/String;

    .line 278
    const-string v4, "MMD"

    .line 277
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 279
    :catch_0
    move-exception v0

    .line 280
    const-string v1, "UMC:KnoxQuickStartManager"

    const-string v2, "Profile Lookup failed in the core"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
