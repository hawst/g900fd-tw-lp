.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AlertHandlerFactory;
.super Ljava/lang/Object;
.source "AlertHandlerFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:AlertHandlerFactory"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;
    .locals 2

    .prologue
    .line 47
    .line 48
    const-string v0, "UMC:AlertHandlerFactory"

    const-string v1, "@ createInstance"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseISLPolicy.startRuntimeWatch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    .line 58
    :goto_0
    return-object v0

    .line 51
    :cond_0
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "UmcPolicy.registerForTimaViolationEvent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseLicenseManager.activateLicense"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerAlertHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerAlertHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    goto :goto_0

    .line 56
    :cond_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    goto :goto_0
.end method
