.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;
.super Ljava/lang/Object;
.source "Report.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private content:Lcom/fasterxml/jackson/databind/JsonNode;

.field private description:Ljava/lang/String;

.field private device:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;

.field private href:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private type:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Lcom/fasterxml/jackson/databind/JsonNode;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->content:Lcom/fasterxml/jackson/databind/JsonNode;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDevice()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->device:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;

    return-object v0
.end method

.method public getHref()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->href:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->type:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    return-object v0
.end method

.method public setContent(Lcom/fasterxml/jackson/databind/JsonNode;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->content:Lcom/fasterxml/jackson/databind/JsonNode;

    .line 88
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->description:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setDevice(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->device:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;

    .line 76
    return-void
.end method

.method public setHref(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->href:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->id:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->name:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setType(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->type:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    .line 84
    return-void
.end method
