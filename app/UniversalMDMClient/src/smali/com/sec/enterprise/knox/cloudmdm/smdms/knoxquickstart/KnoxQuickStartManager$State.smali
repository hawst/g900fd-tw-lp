.class abstract enum Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;
.super Ljava/lang/Enum;
.source "KnoxQuickStartManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

.field public static final enum sF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

.field private static final synthetic sG:[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 100
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$1;

    const-string v1, "STATE_QUICKSTART_IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    .line 141
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;

    const-string v1, "STATE_QUICKSTART_ENROLLMENT"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    .line 99
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sE:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sF:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sG:[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static declared-synchronized a(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V
    .locals 4

    .prologue
    .line 496
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    monitor-enter v1

    :try_start_0
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Old State : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gS()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const-string v0, "UMC:KnoxQuickStartManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "New State : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gS()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->q(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 500
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    .line 501
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gS()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->p(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    monitor-exit v1

    return-void

    .line 496
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V
    .locals 0

    .prologue
    .line 495
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->sG:[Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method abstract a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V
.end method

.method p(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method q(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 512
    return-object p1
.end method
