.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;
.super Ljava/lang/Object;
.source "LinearLayoutMarginBuilder.java"


# instance fields
.field private r:Landroid/content/res/Resources;

.field private yO:Landroid/widget/LinearLayout;

.field private yP:Landroid/widget/LinearLayout$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->r:Landroid/content/res/Resources;

    .line 27
    return-void
.end method

.method private iP()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yO:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;
    .locals 1

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yO:Landroid/widget/LinearLayout;

    .line 31
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    .line 32
    return-object p0
.end method

.method public ba(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    int-to-float v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 59
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->iP()V

    .line 60
    return-object p0
.end method

.method public bb(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    int-to-float v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 71
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->iP()V

    .line 72
    return-object p0
.end method

.method public bc(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;
    .locals 2

    .prologue
    .line 76
    if-gez p1, :cond_2

    .line 78
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 79
    const/4 v0, -0x2

    if-ne p1, v0, :cond_1

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 86
    :goto_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->iP()V

    .line 87
    return-object p0

    .line 82
    :cond_1
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Invalid parameter for setHeight"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    int-to-float v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->f(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    goto :goto_0
.end method

.method public e(F)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->yP:Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 42
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->iP()V

    .line 43
    return-object p0
.end method

.method public f(F)I
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x1

    .line 48
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->r:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 47
    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method
