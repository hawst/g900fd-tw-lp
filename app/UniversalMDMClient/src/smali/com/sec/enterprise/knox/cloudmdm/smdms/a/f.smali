.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "PackageManagerAdapter.java"


# instance fields
.field public oT:Z

.field public result:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 75
    monitor-enter p0

    .line 76
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;->oT:Z

    .line 77
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->DELETE_SUCCEEDED:I

    if-ne p2, v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;->result:Z

    .line 78
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 75
    monitor-exit p0

    .line 80
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
