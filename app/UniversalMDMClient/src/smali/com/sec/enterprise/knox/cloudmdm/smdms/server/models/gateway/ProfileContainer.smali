.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;
.super Ljava/lang/Object;
.source "ProfileContainer.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private href:Ljava/lang/String;

.field private limit:I

.field private next:Ljava/lang/String;

.field private prev:Ljava/lang/String;

.field private start:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->elements:Ljava/util/List;

    return-object v0
.end method

.method public getHref()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->href:Ljava/lang/String;

    return-object v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->limit:I

    return v0
.end method

.method public getNext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->next:Ljava/lang/String;

    return-object v0
.end method

.method public getPrev()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->prev:Ljava/lang/String;

    return-object v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->start:I

    return v0
.end method

.method public setElements(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->elements:Ljava/util/List;

    .line 46
    return-void
.end method

.method public setHref(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->href:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setLimit(I)V
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->limit:I

    .line 40
    return-void
.end method

.method public setNext(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->next:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setPrev(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->prev:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setStart(I)V
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->start:I

    .line 34
    return-void
.end method
