.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;
.super Ljava/lang/Object;
.source "UrlUploadHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "UrlUploadHandler"


# instance fields
.field private mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field private mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

.field private mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

.field private mContext:Landroid/content/Context;

.field private mUploadFile:Ljava/io/File;

.field private mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

.field private mUploadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 64
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    .line 70
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;

    .line 73
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mContext:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 75
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    .line 76
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 77
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    .line 78
    return-void
.end method

.method private Upload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 139
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 142
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;

    invoke-direct {v4, p0, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Ljava/lang/String;)V

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;-><init>(Landroid/content/Context;JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    .line 191
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    const-string v1, "Content-Type"

    const-string v2, "multipart/form-data"

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;

    .line 193
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 194
    :cond_0
    const-string v0, "UrlUploadHandler"

    const-string v1, "No content to upload"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 196
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "PostInvokeException: No content to upload"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;->onUrlUploadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 208
    :goto_0
    return-void

    .line 199
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    invoke-virtual {v0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->m(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    const-string v1, "UrlUploadHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cant handle the url, so aborting:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v1, "UrlUploadHandler"

    const-string v2, "download()onFailure "

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v1, "UrlUploadHandler"

    const-string v2, "upload()onFailure "

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 205
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 206
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    const/4 v2, 0x1

    invoke-interface {v1, p1, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;->onUrlUploadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Ljava/io/File;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->Upload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private formLocaluploadFile()Ljava/lang/String;
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->getHostName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    :cond_0
    const-string v0, "upload"

    .line 217
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/admins/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "upload"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 220
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 221
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/upd_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".temp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private uploadUrl(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 123
    :try_start_0
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->Upload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return v2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 128
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    invoke-interface {v1, p1, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;->onUrlUploadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 129
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto :goto_0
.end method


# virtual methods
.method addUploadRequestFromLocalFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->formLocaluploadFile()Ljava/lang/String;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    return-void
.end method

.method getInputArgument(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 100
    const-class v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-static {v1, v0, p2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->translateObject(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    return-object v0
.end method

.method hasUrlsToUpload()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 110
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 111
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->uploadUrl(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    .line 112
    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public startUpload()V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->run()V

    .line 106
    return-void
.end method
