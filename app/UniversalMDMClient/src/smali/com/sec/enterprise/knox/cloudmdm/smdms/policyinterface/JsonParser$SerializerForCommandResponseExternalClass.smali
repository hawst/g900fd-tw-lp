.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForCommandResponseExternalClass;
.super Ljava/lang/Object;
.source "JsonParser.java"

# interfaces
.implements Lcom/google/gson/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/q",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serialize(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;
    .locals 3

    .prologue
    .line 139
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    invoke-virtual {v0, p1}, Lcom/google/gson/e;->i(Ljava/lang/Object;)Lcom/google/gson/l;

    move-result-object v0

    check-cast v0, Lcom/google/gson/n;

    .line 141
    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->errorDescription:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 142
    const-string v1, "errorDescription"

    invoke-virtual {v0, v1}, Lcom/google/gson/n;->t(Ljava/lang/String;)Lcom/google/gson/l;

    .line 145
    :cond_0
    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-boolean v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->hasReturn:Z

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->response:Ljava/util/Map;

    if-nez v1, :cond_1

    .line 146
    const-string v1, "response"

    invoke-virtual {v0, v1}, Lcom/google/gson/n;->t(Ljava/lang/String;)Lcom/google/gson/l;

    .line 149
    :cond_1
    const/4 v1, 0x0

    :goto_0
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->mCommandResponseIgnoreItems:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->access$0()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_2

    .line 152
    return-object v0

    .line 150
    :cond_2
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->mCommandResponseIgnoreItems:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->access$0()[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/google/gson/n;->t(Ljava/lang/String;)Lcom/google/gson/l;

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForCommandResponseExternalClass;->serialize(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;

    move-result-object v0

    return-object v0
.end method
