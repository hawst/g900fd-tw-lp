.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACTION_BIND_RESULT:Ljava/lang/String; = "com.samsung.android.mdm.VPN_BIND_RESULT"

.field public static final BIND_STATUS:Ljava/lang/String; = "vpn_bind_status"

.field public static final BIND_VENDOR:Ljava/lang/String; = "vpn_bind_vendor"

.field public static DEVICE_CONTAINER_ID:I

.field public static LEGACY_CONTAINER_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->DEVICE_CONTAINER_ID:I

    .line 41
    const/4 v0, 0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->LEGACY_CONTAINER_ID:I

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
