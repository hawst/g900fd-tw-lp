.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;
.super Landroid/app/Activity;
.source "CloudMdmEnrollmentActivity.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;


# static fields
.field private static mContext:Landroid/content/Context;

.field static xI:Z

.field private static xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

.field private static xK:Landroid/app/ProgressDialog;

.field private static xL:Landroid/app/FragmentManager;

.field private static xM:Landroid/app/Activity;

.field private static xO:Landroid/app/AlertDialog;

.field private static xP:I

.field private static xQ:Landroid/os/Bundle;

.field private static xR:I

.field static xS:Landroid/widget/Button;

.field private static xT:Ljava/lang/String;


# instance fields
.field private xN:Landroid/view/OrientationEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 90
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    .line 91
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    .line 235
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    .line 357
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static final L(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 841
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Launching exclude from recents"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 843
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 844
    const-string v1, "Finish"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 845
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 846
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 847
    return-void
.end method

.method static synthetic L(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 357
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xT:Ljava/lang/String;

    return-void
.end method

.method private static a(IIIZ)V
    .locals 2

    .prologue
    .line 721
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Show error Dialog"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 724
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$3;-><init>(IIIZ)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 775
    :goto_0
    return-void

    .line 773
    :cond_0
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "addFragmentOnUiThread : Activity is NULL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(IILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 433
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    if-eqz v0, :cond_0

    .line 434
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$11;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$11;-><init>(ILandroid/os/Bundle;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_0
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "addFragmentOnUiThread : Activity is NULL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 93
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xO:Landroid/app/AlertDialog;

    return-void
.end method

.method private static a(Landroid/app/Fragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 480
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 481
    const v1, 0x7f090002

    invoke-virtual {v0, v1, p0, p1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 482
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 483
    return-void
.end method

.method static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;)V
    .locals 0

    .prologue
    .line 392
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    .line 393
    return-void
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 360
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 362
    const v1, 0x7f0800c1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 363
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$8;

    invoke-direct {v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$8;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1, p1, v4, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 370
    const v2, 0x7f080053

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$9;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$9;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 376
    const v2, 0x7f080058

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$10;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$10;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 385
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 386
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 387
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xS:Landroid/widget/Button;

    .line 388
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xS:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 389
    return-void
.end method

.method private aY(I)Z
    .locals 1

    .prologue
    .line 196
    const/16 v0, 0x68

    if-eq p1, v0, :cond_0

    .line 197
    const/16 v0, 0x6a

    if-eq p1, v0, :cond_0

    .line 198
    const/16 v0, 0x6f

    if-eq p1, v0, :cond_0

    .line 199
    const/16 v0, 0x70

    if-ne p1, v0, :cond_1

    .line 200
    :cond_0
    const/4 v0, 0x1

    .line 202
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aZ(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 396
    const/4 v0, 0x0

    .line 398
    packed-switch p0, :pswitch_data_0

    .line 424
    :pswitch_0
    const-string v1, "UMC:CloudMdmEnrollmentActivity"

    const-string v2, "Unknown Fragment integer id"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :goto_0
    return-object v0

    .line 400
    :pswitch_1
    const-string v0, "CLOUD_MDM_ACTIVITY_SELECT_MDM_SERVER_SCREEN"

    goto :goto_0

    .line 403
    :pswitch_2
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_LOGIN_FRAGMENT"

    goto :goto_0

    .line 406
    :pswitch_3
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_DYNAMIC_FRAGMENT"

    goto :goto_0

    .line 409
    :pswitch_4
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_EULA_FRAGMENT"

    goto :goto_0

    .line 412
    :pswitch_5
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_FREEMIUM_EMAIL_SIMPLE"

    goto :goto_0

    .line 415
    :pswitch_6
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_FREEMIUM_EMAIL_ADVANCED"

    goto :goto_0

    .line 418
    :pswitch_7
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_FREEMIUM_CERTIFICATE_PICKER"

    goto :goto_0

    .line 421
    :pswitch_8
    const-string v0, "CLOUD_MDM_ACTIVITY_ADD_QUICKSTART_PROGRESS_SCREEN"

    goto :goto_0

    .line 398
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static b(IIII)V
    .locals 2

    .prologue
    .line 677
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "showRetryDialog"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 680
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$2;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 716
    :goto_0
    return-void

    .line 714
    :cond_0
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "addFragmentOnUiThread : Activity is NULL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(ILandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 486
    sput p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xP:I

    .line 487
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xQ:Landroid/os/Bundle;

    .line 488
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 489
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$12;

    invoke-direct {v1, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$12;-><init>(ILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 500
    :goto_0
    return-void

    .line 496
    :cond_0
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    .line 497
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addFragmentOnUiThread : Activity is NULL, Serious Error, failed to do UI operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->aZ(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 497
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 496
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static c(ILandroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 503
    const/4 v0, 0x0

    .line 504
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->aZ(I)Ljava/lang/String;

    move-result-object v1

    .line 505
    const-string v2, "UMC:CloudMdmEnrollmentActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "adding fragment. id:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "str:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    packed-switch p0, :pswitch_data_0

    .line 573
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "unidentified fragment value in CloudMdmEnrollment"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 509
    :pswitch_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;-><init>()V

    .line 577
    :goto_1
    if-eqz v0, :cond_0

    .line 578
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 579
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->a(Landroid/app/Fragment;Ljava/lang/String;)V

    goto :goto_0

    .line 512
    :pswitch_1
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;-><init>()V

    goto :goto_1

    .line 515
    :pswitch_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;-><init>()V

    goto :goto_1

    .line 519
    :pswitch_3
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;-><init>()V

    goto :goto_1

    .line 523
    :pswitch_4
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/e;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/e;-><init>()V

    goto :goto_1

    .line 527
    :pswitch_5
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;-><init>()V

    goto :goto_1

    .line 530
    :pswitch_6
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;-><init>()V

    goto :goto_1

    .line 534
    :pswitch_7
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;-><init>()V

    goto :goto_1

    .line 539
    :pswitch_8
    new-instance v2, Landroid/app/ProgressDialog;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 540
    const-string v3, "message"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 541
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 542
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    .line 543
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    goto :goto_1

    .line 547
    :pswitch_9
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 548
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_1

    .line 550
    :cond_1
    const-string v2, "UMC:CloudMdmEnrollmentActivity"

    const-string v3, "DIALOG WAS NULL"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 555
    :pswitch_a
    const-string v2, "error.title"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 556
    const-string v3, "error.message"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 557
    const-string v4, "error.buttonText"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 558
    const-string v5, "error.shouldClose"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 555
    invoke-static {v2, v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->a(IIIZ)V

    goto/16 :goto_1

    .line 562
    :pswitch_b
    const-string v2, "error.title"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 563
    const-string v3, "error.message"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 564
    const-string v4, "error.positiveButtonText"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 565
    const-string v5, "error.negativeButtonText"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 562
    invoke-static {v2, v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(IIII)V

    goto/16 :goto_1

    .line 569
    :pswitch_c
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iy()V

    goto/16 :goto_1

    .line 507
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method static synthetic d(ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 502
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->c(ILandroid/os/Bundle;)V

    return-void
.end method

.method static synthetic iA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 357
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic iB()Landroid/app/FragmentManager;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    return-object v0
.end method

.method static synthetic iC()Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic iD()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xO:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic iE()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    return-object v0
.end method

.method public static it()I
    .locals 2

    .prologue
    .line 239
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    const/4 v1, -0x1

    if-gt v0, v1, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 240
    :cond_0
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    .line 249
    :goto_0
    return v0

    .line 242
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 243
    const/16 v1, 0x2d0

    if-lt v0, v1, :cond_2

    .line 244
    const/4 v0, 0x2

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    .line 249
    :goto_1
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    goto :goto_0

    .line 245
    :cond_2
    const/16 v1, 0x258

    if-lt v0, v1, :cond_3

    .line 246
    const/4 v0, 0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    goto :goto_1

    .line 248
    :cond_3
    const/4 v0, 0x0

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xR:I

    goto :goto_1
.end method

.method private iu()V
    .locals 5

    .prologue
    const v4, 0x7f080053

    .line 309
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .line 310
    const/4 v1, 0x4

    .line 309
    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 311
    const v1, 0x7f080061

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 312
    const v2, 0x7f0800c2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 313
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 314
    const v2, 0x7f080058

    .line 315
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$5;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;)V

    .line 314
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 325
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    :cond_0
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$6;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;)V

    .line 324
    invoke-virtual {v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 331
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xO:Landroid/app/AlertDialog;

    .line 332
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 334
    return-void
.end method

.method public static iv()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 585
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Clean up called"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 587
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->L(Landroid/content/Context;)V

    .line 588
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 589
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    .line 591
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 592
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 593
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    .line 595
    :cond_1
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    .line 596
    sput-boolean v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 597
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    .line 598
    sput v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xP:I

    .line 599
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xQ:Landroid/os/Bundle;

    .line 600
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zq:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 601
    sput-boolean v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zr:Z

    .line 602
    return-void
.end method

.method public static iw()V
    .locals 2

    .prologue
    .line 661
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 662
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f050030

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 664
    :cond_0
    return-void
.end method

.method private ix()V
    .locals 1

    .prologue
    .line 667
    const v0, 0x7f0a0010

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->setTheme(I)V

    .line 668
    return-void
.end method

.method private static iy()V
    .locals 2

    .prologue
    .line 779
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "showWifiNotAvailableDialog"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 782
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$4;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$4;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 831
    :goto_0
    return-void

    .line 829
    :cond_0
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "addFragmentOnUiThread : Activity is NULL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic iz()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    return-object v0
.end method


# virtual methods
.method public B(Z)Z
    .locals 1

    .prologue
    .line 271
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    if-eqz v0, :cond_0

    .line 272
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;->s(Z)V

    .line 273
    :cond_0
    return p1
.end method

.method public C(Z)V
    .locals 1

    .prologue
    .line 295
    if-nez p1, :cond_1

    .line 296
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    if-eqz v0, :cond_0

    .line 298
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-interface {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;->fm()V

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iu()V

    goto :goto_0
.end method

.method public E(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    if-eqz v0, :cond_0

    .line 281
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;->E(Ljava/lang/String;)V

    .line 282
    :cond_0
    return-void
.end method

.method public G(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 263
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Activity: onSelectMDMServer(String mdmServerInfo)"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;->G(Ljava/lang/String;)V

    .line 267
    :cond_0
    return-void
.end method

.method public c(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 288
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    if-eqz v0, :cond_0

    .line 289
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;->c(Ljava/util/HashMap;)V

    .line 290
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 608
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 609
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 611
    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 612
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 613
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 614
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 615
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    aget v4, v2, v6

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 616
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aget v2, v2, v7

    int-to-float v2, v2

    sub-float v2, v4, v2

    .line 620
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v7, :cond_1

    .line 621
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v3, v4

    if-ltz v4, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-gez v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-ltz v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_1

    .line 623
    :cond_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 624
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 627
    :cond_1
    return v1
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 633
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "On back pressed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 635
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->C(Z)V

    .line 658
    :goto_0
    return-void

    .line 637
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 638
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 639
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v1

    .line 640
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 641
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v2

    .line 642
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 643
    invoke-interface {v2}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 642
    invoke-virtual {v0, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    .line 644
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-interface {v1}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 645
    const-string v4, "UMC:CloudMdmEnrollmentActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Backstack retrieving Fragment:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    const-string v1, "UMC:CloudMdmEnrollmentActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Current Fragment:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    instance-of v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;

    if-eqz v1, :cond_1

    .line 649
    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/k;->reload()V

    .line 651
    :cond_1
    instance-of v0, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    if-eqz v0, :cond_2

    .line 652
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Back button disabled for this fragment"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 656
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CloudMdmEnrollmentActivity onCreate() flags "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/c;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getIntent()Landroid/content/Intent;

    .line 109
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Finish"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Need to finish this activity"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->finish()V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fw()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 115
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->L(Landroid/content/Context;)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->finish()V

    goto :goto_0

    .line 120
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 121
    invoke-virtual {p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->requestWindowFeature(I)Z

    .line 123
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 128
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->ix()V

    .line 129
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "QS Mode"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :goto_1
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->setContentView(I)V

    .line 137
    sput-boolean v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 138
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    .line 139
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xM:Landroid/app/Activity;

    .line 141
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->it()I

    move-result v0

    if-lez v0, :cond_3

    .line 142
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;

    invoke-direct {v0, p0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xN:Landroid/view/OrientationEventListener;

    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xN:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 168
    :cond_3
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    .line 170
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xK:Landroid/app/ProgressDialog;

    .line 172
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fl()Z

    move-result v0

    if-nez v0, :cond_4

    .line 173
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Not in Enrollment State"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xL:Landroid/app/FragmentManager;

    const-string v1, "CLOUD_MDM_ACTIVITY_ADD_DYNAMIC_FRAGMENT"

    invoke-virtual {v0, v1, v3}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 177
    :cond_4
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 178
    const-string v1, "cloudmdm.ui.intent.extra.bundle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 179
    const-string v2, "cloudmdm.ui.intent.extra"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 180
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    if-eqz v1, :cond_5

    .line 182
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 184
    :cond_5
    if-nez p1, :cond_7

    .line 185
    const-string v1, "UMC:CloudMdmEnrollmentActivity"

    const-string v3, "savedInstanceState is null"

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 131
    :cond_6
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 132
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 133
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "Normal Mode"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 187
    :cond_7
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xP:I

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->aY(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xP:I

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xQ:Landroid/os/Bundle;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(ILandroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 224
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "MDM OnDestroy, clearing all fragments"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 227
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xO:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 228
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xO:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 230
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 208
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 209
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onResume"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zq:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zq:Ljava/util/LinkedHashMap;

    .line 212
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 213
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zq:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 218
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zr:Z

    .line 219
    return-void

    .line 213
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 214
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 835
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 836
    const-string v0, "UMC:CloudMdmEnrollmentActivity"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    return-void
.end method

.method public x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 338
    .line 340
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 345
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->mContext:Landroid/content/Context;

    .line 346
    const/4 v2, 0x4

    .line 345
    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 347
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 348
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 349
    const v2, 0x7f080058

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity$7;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 353
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 354
    return-void

    .line 343
    :cond_0
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method
