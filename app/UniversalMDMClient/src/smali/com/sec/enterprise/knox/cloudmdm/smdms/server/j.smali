.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;
.source "FtpService.java"


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V
    .locals 1

    .prologue
    .line 206
    invoke-direct/range {p0 .. p8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V

    .line 207
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 212
    new-instance v4, Lorg/apache/commons/net/ftp/c;

    invoke-direct {v4}, Lorg/apache/commons/net/ftp/c;-><init>()V

    .line 213
    new-instance v2, Lorg/apache/commons/net/ftp/f;

    invoke-direct {v2}, Lorg/apache/commons/net/ftp/f;-><init>()V

    .line 218
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->server:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lorg/apache/commons/net/ftp/c;->connect(Ljava/lang/String;)V

    .line 219
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jZ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jW()I

    move-result v2

    .line 226
    invoke-static {v2}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 227
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V

    .line 228
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->TAG:Ljava/lang/String;

    const-string v3, "FTP server refused connection."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 271
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 281
    :cond_0
    :goto_0
    return-object v0

    .line 232
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->user:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->vI:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lorg/apache/commons/net/ftp/c;->E(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 233
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jZ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->vJ:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v3, v6

    iput v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->vL:I

    .line 236
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 238
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j$1;

    invoke-direct {v2, p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;Ljava/io/InputStream;)V

    .line 251
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lorg/apache/commons/net/ftp/c;->br(I)Z

    .line 253
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->kj()V

    .line 255
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->vK:Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Lorg/apache/commons/net/ftp/c;->a(Ljava/lang/String;Ljava/io/InputStream;)Z

    .line 257
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 260
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->ki()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 271
    :goto_1
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 273
    :try_start_3
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move v2, v1

    .line 279
    :goto_2
    if-nez v2, :cond_2

    .line 280
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    iget-wide v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->ro:J

    const/16 v5, 0xc8

    invoke-direct {v4, v6, v7, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 281
    :cond_2
    if-eqz v2, :cond_5

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 262
    :cond_3
    :try_start_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->TAG:Ljava/lang/String;

    const-string v3, "Wrong user/pass to login"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 264
    :catch_0
    move-exception v2

    .line 265
    const/4 v3, 0x1

    .line 266
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 271
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 273
    :try_start_6
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    move v2, v3

    .line 274
    goto :goto_2

    :catch_1
    move-exception v2

    move v2, v3

    goto :goto_2

    .line 267
    :catch_2
    move-exception v2

    .line 268
    :try_start_7
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 269
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    iget-wide v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->ro:J

    const/16 v5, 0x1f4

    invoke-direct {v3, v6, v7, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    invoke-interface {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 271
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 273
    :try_start_8
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    move v2, v1

    .line 274
    goto :goto_2

    :catch_3
    move-exception v2

    move v2, v1

    goto :goto_2

    .line 270
    :catchall_0
    move-exception v0

    .line 271
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 273
    :try_start_9
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 278
    :cond_4
    :goto_4
    throw v0

    .line 274
    :catch_4
    move-exception v2

    move v2, v1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 281
    goto :goto_3

    .line 274
    :catch_5
    move-exception v1

    goto/16 :goto_0

    :catch_6
    move-exception v1

    goto :goto_4

    :cond_6
    move v2, v1

    goto :goto_2

    :cond_7
    move v2, v3

    goto :goto_2
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
