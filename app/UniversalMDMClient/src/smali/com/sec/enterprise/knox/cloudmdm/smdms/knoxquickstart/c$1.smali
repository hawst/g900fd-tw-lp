.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;
.super Ljava/lang/Object;
.source "EmailCertificatePickerFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation


# instance fields
.field final synthetic rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

.field private final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->val$id:I

    .line 396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 399
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    const v1, 0x7f09007d

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)Ljava/lang/String;

    move-result-object v0

    .line 400
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    const v2, 0x7f09007e

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)Ljava/lang/String;

    move-result-object v1

    .line 401
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;)V

    .line 403
    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->val$id:I

    packed-switch v2, :pswitch_data_0

    .line 419
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid Dialog Id"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :pswitch_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;I)V

    .line 424
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->val$id:I

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)V

    .line 425
    return-void

    .line 412
    :pswitch_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    const v1, 0x7f080035

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;I)Landroid/widget/TextView;

    goto :goto_0

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c$1;->rY:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->gF()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;I)V

    goto :goto_0

    .line 403
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
