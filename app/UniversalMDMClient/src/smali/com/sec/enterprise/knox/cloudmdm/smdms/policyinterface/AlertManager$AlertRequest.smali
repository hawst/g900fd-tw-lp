.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;
.super Ljava/lang/Object;
.source "AlertManager.java"


# instance fields
.field public alerts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

.field transient notpersistant:Ljava/lang/Boolean;

.field public policyDocumentId:Ljava/lang/String;

.field public user:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->user:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->policyDocumentId:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 28
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->notpersistant:Ljava/lang/Boolean;

    .line 29
    return-void
.end method
