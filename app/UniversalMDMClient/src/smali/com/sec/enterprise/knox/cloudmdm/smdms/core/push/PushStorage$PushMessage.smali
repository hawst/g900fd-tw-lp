.class public final Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;
.super Ljava/lang/Object;
.source "PushStorage.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x4ab863565ee81281L


# instance fields
.field private final deviceId:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    .line 68
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    .line 69
    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    if-ne p0, p1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 76
    goto :goto_0

    .line 77
    :cond_2
    instance-of v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    if-nez v2, :cond_3

    move v0, v1

    .line 78
    goto :goto_0

    .line 79
    :cond_3
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    .line 80
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 81
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 82
    goto :goto_0

    .line 83
    :cond_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 84
    goto :goto_0

    .line 85
    :cond_5
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 86
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 87
    goto :goto_0

    .line 88
    :cond_6
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 89
    goto :goto_0

    .line 90
    :cond_7
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 91
    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    return-object v0
.end method

.method public gg()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 118
    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 121
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 122
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 123
    return v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 122
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    const-string v1, "PushMessage [id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", deviceId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 130
    const-string v2, ", messageType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->messageType:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
