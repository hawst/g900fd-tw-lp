.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;
.super Ljava/lang/Object;
.source "CarouselUi.java"


# instance fields
.field private mDuration:J

.field private zA:I

.field private zB:I

.field private zH:I

.field private zI:I

.field private zJ:[Landroid/widget/ImageView;

.field private zw:[I

.field private zx:[I

.field private zy:Landroid/widget/ImageView;

.field private zz:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zy:Landroid/widget/ImageView;

    .line 53
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zz:Landroid/widget/TextView;

    .line 55
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->mDuration:J

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zA:I

    return v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)[I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)[I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zz:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zy:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->mDuration:J

    return-wide v0
.end method

.method static synthetic g(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zJ:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zH:I

    return v0
.end method

.method static synthetic i(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zI:I

    return v0
.end method

.method static synthetic j(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zB:I

    return v0
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;[I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zy:Landroid/widget/ImageView;

    .line 63
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    .line 64
    return-object p0
.end method

.method public a(Landroid/widget/TextView;[I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zz:Landroid/widget/TextView;

    .line 69
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    .line 70
    return-object p0
.end method

.method public a([Landroid/widget/ImageView;II)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zJ:[Landroid/widget/ImageView;

    .line 75
    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zH:I

    .line 76
    iput p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zI:I

    .line 77
    return-object p0
.end method

.method public b(J)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;
    .locals 1

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->mDuration:J

    .line 82
    return-object p0
.end method

.method public jI()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 89
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Carousel must have the same number of titles("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 90
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") as images("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 91
    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 89
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :goto_0
    return-object v0

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    if-eqz v1, :cond_2

    .line 95
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zw:[I

    array-length v1, v1

    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zB:I

    .line 98
    :cond_1
    :goto_1
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-direct {v1, p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V

    move-object v0, v1

    goto :goto_0

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    if-eqz v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zx:[I

    array-length v1, v1

    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->zB:I

    goto :goto_1
.end method
