.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;
.super Ljava/lang/Object;
.source "TestPolicyHandler.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:TestPolicyHandler"

.field static gTestPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

.field private static permissions:[Ljava/lang/String;


# instance fields
.field private final POLICY_FILE_EXTENSION:Ljava/lang/String;

.field public final defaultpolicyFolderpath:Ljava/lang/String;

.field mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

.field final mContext:Landroid/content/Context;

.field private mKnoxEnterpriseLicenseManager:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/DummyPermissions;->getAllPermissions()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->permissions:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-string v0, "/storage/sdcard0/Policies/"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->defaultpolicyFolderpath:Ljava/lang/String;

    .line 63
    const-string v0, ".req"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->POLICY_FILE_EXTENSION:Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mContext:Landroid/content/Context;

    .line 69
    return-void
.end method

.method private findRequestFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 313
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->findAllFilesInFolder()Ljava/util/List;

    move-result-object v3

    .line 314
    if-nez v3, :cond_0

    move-object v0, v2

    .line 322
    :goto_0
    return-object v0

    .line 316
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    .line 322
    goto :goto_0

    .line 317
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getPolicyRequestID(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 318
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 319
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 316
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->gTestPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->gTestPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    .line 74
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->gTestPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->setup()V

    .line 76
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->gTestPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    return-object v0
.end method

.method private getListFiles(Ljava/io/File;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 327
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 328
    if-nez v2, :cond_1

    .line 329
    const/4 v0, 0x0

    .line 340
    :cond_0
    return-object v0

    .line 331
    :cond_1
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 332
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 333
    invoke-direct {p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getListFiles(Ljava/io/File;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 331
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 335
    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ".req"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 336
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private initListenersAfterReboot()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v4

    .line 90
    if-nez v4, :cond_1

    .line 104
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 93
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 94
    aget-object v0, v4, v1

    .line 95
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v5

    .line 96
    invoke-virtual {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getPendingPolicyRequestFiles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    move v3, v2

    .line 97
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lt v3, v0, :cond_2

    .line 93
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 98
    :cond_2
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getPolicyRequestID(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->isPolicyAppliedThruDemoApp(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 100
    invoke-virtual {v5, v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 97
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method private isPolicyAppliedThruDemoApp(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->findRequestFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadDefaultIcon()[B
    .locals 4

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 272
    :try_start_0
    const-string v1, "proxyadmindefault.png"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 273
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->read2array(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 277
    :goto_0
    return-object v0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private read2array(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    .line 344
    const/4 v0, 0x0

    .line 345
    if-eqz p1, :cond_4

    .line 347
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v0, v0

    .line 348
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 349
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The file is too big"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_0
    long-to-int v0, v0

    new-array v1, v0, [B

    .line 354
    const/4 v0, 0x0

    .line 356
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 357
    array-length v2, v1

    sub-int/2addr v2, v0

    invoke-virtual {p1, v1, v0, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 356
    if-gez v2, :cond_2

    .line 361
    :cond_1
    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 362
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The icon was not completely read: "

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_2
    add-int/2addr v0, v2

    goto :goto_0

    .line 364
    :cond_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    move-object v0, v1

    .line 366
    :cond_4
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytes = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    return-object v0
.end method

.method private setup()V
    .locals 2

    .prologue
    .line 81
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "Setup is called in testpolicyhandler"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mKnoxEnterpriseLicenseManager:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    .line 84
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->initListenersAfterReboot()V

    .line 85
    return-void
.end method


# virtual methods
.method public activateKlm(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 205
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "activateKlm called "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The key is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mKnoxEnterpriseLicenseManager:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    if-nez v0, :cond_0

    .line 209
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "KnoxLicenseManagerIsNull"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mKnoxEnterpriseLicenseManager:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mKnoxEnterpriseLicenseManager:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->activateLicense(Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public applyPolicyforTestEnterprise(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 217
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "applyPolicyforTestEnterprise"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    array-length v1, p2

    if-lt v0, v1, :cond_0

    .line 230
    :goto_1
    return-void

    .line 221
    :cond_0
    aget-object v1, p2, v0

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 222
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 224
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v2

    const-string v3, "DemoAppUser"

    invoke-virtual {v2, v3, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->applyPolicy(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)Z

    .line 225
    const-string v2, "UMC:TestPolicyHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "applyPolicyforTestEnterprise - Admin = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " PolicyRequest = \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 228
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public enroll(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 111
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to Enroll : User : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to MDM Server : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->loadDefaultIcon()[B

    move-result-object v3

    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getNextProxyUID()I

    move-result v5

    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v6

    .line 116
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    const-string v2, "Cloud Admin - "

    .line 117
    const-string v4, "Samsung Cloud Admin"

    sget-object v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->permissions:[Ljava/lang/String;

    move-object v1, p1

    .line 116
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addCloudMDMAdmin(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ILandroid/content/ComponentName;[Ljava/lang/String;)Z

    move-result v0

    .line 118
    if-nez v0, :cond_0

    .line 119
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "Failure adding Admin"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 130
    :goto_0
    return v0

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 124
    if-nez v0, :cond_1

    .line 125
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "Failure adding User"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 126
    goto :goto_0

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 129
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Successfully Enrolled to MDM Server : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public enroll_withoutproxyadmin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 135
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to Enroll : User : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to MDM Server : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 136
    const-string v3, " And also share ownership with Local Package : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 135
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addCloudMDMAdminShareOwnershipWithLocalAdminWithOutProxyAdmin(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 140
    if-nez v1, :cond_0

    .line 141
    const-string v1, "UMC:TestPolicyHandler"

    const-string v2, "Failure adding Admin"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_0
    return v0

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 145
    if-nez v1, :cond_1

    .line 146
    const-string v1, "UMC:TestPolicyHandler"

    const-string v2, "Failure adding User"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 152
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Successfully Enrolled to MDM Server : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 153
    const-string v2, " with Local Package : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 152
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public enroll_withproxyadmin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 158
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to Enroll : User : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to MDM Server : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->loadDefaultIcon()[B

    move-result-object v3

    .line 161
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    const-string v2, "Cloud Admin - "

    .line 162
    const-string v4, "Samsung Cloud Admin"

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->permissions:[Ljava/lang/String;

    move-object v1, p1

    move-object v5, p3

    move-object v7, p3

    .line 161
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addCloudMDMAdminShareOwnershipWithLocalAdmin(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 163
    if-nez v0, :cond_0

    .line 164
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "Failure adding Admin"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 175
    :goto_0
    return v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->addUser(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 168
    if-nez v0, :cond_1

    .line 169
    const-string v0, "UMC:TestPolicyHandler"

    const-string v1, "Failure adding User"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 170
    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 174
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Successfully Enrolled to MDM Server : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public findAllFilesInFolder()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    const-string v0, "/storage/sdcard0/Policies/"

    .line 282
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getListFiles(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    .line 283
    return-object v0
.end method

.method public getAdminIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAllAdminIds()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPolicyRequestID(Ljava/io/File;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->readFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler$TestPolicyRequest;->fromJson(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler$TestPolicyRequest;

    move-result-object v0

    .line 305
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler$TestPolicyRequest;->documentId:Ljava/lang/String;

    return-object v0
.end method

.method getPolicyRequestID(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 298
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler$TestPolicyRequest;->fromJson(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler$TestPolicyRequest;

    move-result-object v0

    .line 299
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler$TestPolicyRequest;->documentId:Ljava/lang/String;

    return-object v0
.end method

.method public isAdminExist(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isInsideContainer()Z
    .locals 1

    .prologue
    .line 375
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getCallingUserId()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAlert(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    .locals 4

    .prologue
    .line 253
    const-string v0, "UMC:TestPolicyHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAlert - Admin = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " user = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "alertReport = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->toJson()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :try_start_0
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->documentId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->findRequestFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->timestamp:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".alert"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->toJson()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->removeAlertReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_0
    return-void

    .line 259
    :catch_0
    move-exception v0

    .line 260
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPoliciesApplied(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 233
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPoliciesApplied - Admin = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " user = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Report : Request= \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyRequestString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPoliciesApplied - Admin = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " user = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Report : Response= \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyResponseString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :try_start_0
    iget-object v1, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->documentId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->findRequestFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".resp"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 239
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->getPolicyResponseString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    .line 241
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getCompletedPolicies()Ljava/util/List;

    move-result-object v2

    move v1, v0

    .line 242
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 249
    :goto_1
    return-void

    .line 243
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;

    .line 244
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v3, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->removePolicyReport(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public unenroll(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 179
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unenroll admin = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " user = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isAdminExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->isUserExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->removeUser(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 183
    if-nez v1, :cond_0

    .line 184
    const-string v1, "UMC:TestPolicyHandler"

    const-string v2, "Failure removing User"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_0
    return v0

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->hasEnrolledUsers(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 189
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->mAdminManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->removeAdmin(Ljava/lang/String;)Z

    move-result v1

    .line 190
    if-nez v1, :cond_1

    .line 191
    const-string v1, "UMC:TestPolicyHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not able to remove the admin "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
