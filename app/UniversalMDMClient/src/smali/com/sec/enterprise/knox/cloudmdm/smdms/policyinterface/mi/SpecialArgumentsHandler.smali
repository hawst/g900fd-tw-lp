.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;
.super Ljava/lang/Object;
.source "SpecialArgumentsHandler.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;


# instance fields
.field private mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field private mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

.field private mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

.field private mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

.field private mUrlDownLoadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

.field private mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;)V
    .locals 6

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 79
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mContext:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 81
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    .line 82
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    .line 83
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

    .line 84
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-direct {v0, v1, p1, v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlDownLoadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    .line 85
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    .line 86
    return-void
.end method

.method private isNotLocalPath(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 213
    const/4 v0, 0x0

    .line 214
    const-string v1, "http"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ftp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    const-string v1, "HTTP"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FTP"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 217
    :cond_0
    const/4 v0, 0x1

    .line 220
    :cond_1
    return v0
.end method


# virtual methods
.method createIgnoredObject(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 100
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isParamRemovedInJson(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-object v0

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 106
    if-eqz v1, :cond_0

    .line 107
    const-string v2, "Boolean"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :cond_2
    const-string v2, "ComponentName"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 110
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    const-class v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :cond_3
    const-string v2, "ContextInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 112
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    const-class v1, Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getContextInfo(Ljava/lang/Integer;)Landroid/app/enterprise/ContextInfo;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto :goto_0

    .line 113
    :cond_4
    const-string v2, "Context"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    const-class v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method createReferencedObject(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Ref"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 91
    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    .line 94
    :cond_0
    const-string v1, "ReferencedArg"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;->lookupObject(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;)Ljava/lang/Object;

    move-result-object v0

    .line 96
    invoke-static {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    goto :goto_0
.end method

.method createTranslatedInputArgObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialRequestParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isDownloadURLArg(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p3

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->isNotLocalPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlDownLoadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {v0, p1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->addDownloadRequest(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    invoke-direct {v0, v2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 152
    :cond_0
    :goto_0
    return-object v0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isUploadURLArg(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {v0, p1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->addUploadRequestFromLocalFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->getInputArgument(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-static {v1, p3, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->translateObject(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->type:Ljava/lang/reflect/Type;

    if-nez v1, :cond_0

    .line 150
    :cond_3
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - unable to translate input object"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method createTranslatedOutputObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialResponseParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-static {p2, p3, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->translateObject(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->type:Ljava/lang/reflect/Type;

    if-nez v1, :cond_1

    .line 159
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - unable to translate output object"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_1
    return-object v0
.end method

.method public isInputParamReady()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlDownLoadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->hasUrlsToDownload()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isOutputParamReady()Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->hasUrlsToUpload()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onUrlDownloadError(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 1

    .prologue
    .line 166
    if-nez p2, :cond_0

    .line 167
    new-instance p2, Ljava/lang/Exception;

    const-string v0, "Download Error"

    invoke-direct {p2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;->onInputParamError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 170
    return-void
.end method

.method public onUrlDownloadSuccess(Ljava/util/Map;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;->onInputParamReady(Ljava/util/Map;Z)V

    .line 181
    return-void
.end method

.method public onUrlUploadError(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 1

    .prologue
    .line 173
    if-nez p2, :cond_0

    .line 174
    new-instance p2, Ljava/lang/Exception;

    const-string v0, "Upload Error"

    invoke-direct {p2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;->onOutputParamError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 177
    return-void
.end method

.method public onUrlUploadSuccess(Z)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;

    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;->onOutputParamReady(Z)V

    .line 185
    return-void
.end method

.method prepareInputArgs()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlDownLoadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->hasUrlsToDownload()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlDownLoadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->startDownload()V

    .line 193
    :cond_0
    return-void
.end method

.method public prepareOutputArgs()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->hasUrlsToUpload()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler;->mUrlUploadHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->startUpload()V

    .line 205
    :cond_0
    return-void
.end method
