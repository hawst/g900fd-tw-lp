.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$Downloader;
.source "UrlDownloadHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->download(Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

.field private final synthetic val$argName:Ljava/lang/String;

.field private final synthetic val$localDownloadFile:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->val$localDownloadFile:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->val$argName:Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$Downloader;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$Downloader;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 4

    .prologue
    .line 159
    const-string v0, "UrlDownloadHandler"

    const-string v1, "download()onFailure "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 161
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->val$argName:Ljava/lang/String;

    const/4 v2, 0x0

    .line 162
    const/4 v3, 0x1

    .line 161
    invoke-interface {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;->onUrlDownloadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 164
    return-void
.end method

.method public onProgress(JII)V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public onStart(J)V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    const-string v0, "UrlDownloadHandler"

    const-string v1, "download()onSuccess "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->val$localDownloadFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 149
    invoke-virtual {v0, v3, v2}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 150
    invoke-virtual {v0, v3, v2}, Ljava/io/File;->setReadable(ZZ)Z

    .line 152
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->val$argName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;->val$localDownloadFile:Ljava/lang/String;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->handleDownloadSuccess(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method
