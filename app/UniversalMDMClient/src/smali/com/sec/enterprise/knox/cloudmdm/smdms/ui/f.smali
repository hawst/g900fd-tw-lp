.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;
.super Landroid/os/AsyncTask;
.source "EulaWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private yI:Landroid/webkit/WebView;

.field final synthetic yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

.field private yM:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 327
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yI:Landroid/webkit/WebView;

    .line 328
    return-void
.end method


# virtual methods
.method protected bk(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 394
    const-string v0, "fail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 398
    :catch_0
    move-exception v0

    .line 399
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yM:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 403
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yM:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yI:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->f([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs f([Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 332
    array-length v6, p1

    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-lt v5, v6, :cond_0

    .line 388
    const-string v0, "fail"

    :goto_1
    return-object v0

    .line 332
    :cond_0
    aget-object v0, p1, v5

    .line 333
    const-string v4, ""

    .line 334
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Testing this:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    const/4 v2, 0x0

    .line 342
    const/4 v1, 0x0

    .line 344
    :try_start_1
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 345
    new-instance v7, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v7}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 346
    new-instance v8, Ljava/net/URI;

    invoke-direct {v8, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 347
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 348
    invoke-virtual {v0, v8}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 350
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v8

    const/16 v9, 0x4e20

    invoke-static {v8, v9}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 351
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v8

    const/16 v9, 0x4e20

    invoke-static {v8, v9}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 353
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 354
    invoke-interface {v3, v0, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 355
    :try_start_2
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "Code recieived: "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 357
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 358
    const/16 v1, 0x1fa0

    new-array v1, v1, [C

    .line 360
    :goto_2
    const/4 v7, 0x0

    const/16 v10, 0x1fa0

    invoke-virtual {v2, v1, v7, v10}, Ljava/io/InputStreamReader;->read([CII)I

    move-result v7

    const/4 v10, -0x1

    if-ne v7, v10, :cond_3

    .line 363
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    .line 364
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 365
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Time Taken : "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v8, v10, v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 370
    if-eqz v2, :cond_1

    .line 372
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 377
    :cond_1
    :goto_3
    if-nez v3, :cond_9

    .line 378
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Response is null, connection timed out"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_2
    :goto_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_0

    .line 337
    :catch_0
    move-exception v1

    .line 338
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This is not a url so im returning data"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 361
    :cond_3
    const/4 v10, 0x0

    :try_start_6
    invoke-virtual {v0, v1, v10, v7}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_2

    .line 366
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v4

    .line 367
    :goto_5
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 370
    if-eqz v2, :cond_4

    .line 372
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 377
    :cond_4
    :goto_6
    if-nez v3, :cond_5

    .line 378
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Response is null, connection timed out"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 373
    :catch_2
    move-exception v1

    .line 374
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 380
    :cond_5
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "This is the response"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_2

    goto/16 :goto_1

    .line 369
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v4

    .line 370
    :goto_7
    if-eqz v2, :cond_6

    .line 372
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 377
    :cond_6
    :goto_8
    if-nez v3, :cond_8

    .line 378
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Response is null, connection timed out"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_7
    throw v1

    .line 373
    :catch_3
    move-exception v2

    .line 374
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 380
    :cond_8
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "This is the response"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_7

    goto/16 :goto_1

    .line 373
    :catch_4
    move-exception v1

    .line 374
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 380
    :cond_9
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->access$0()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "This is the response"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_2

    goto/16 :goto_1

    .line 369
    :catchall_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v1, v0

    move-object v0, v4

    goto :goto_7

    :catchall_3
    move-exception v1

    goto :goto_7

    .line 366
    :catch_5
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v4

    goto/16 :goto_5

    :catch_6
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v4

    goto/16 :goto_5

    :catch_7
    move-exception v1

    goto/16 :goto_5
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->bk(Ljava/lang/String;)V

    return-void
.end method
