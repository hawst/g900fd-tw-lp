.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;
.super Ljava/lang/Object;
.source "PolicyIntentManager.java"


# static fields
.field private static final INTENTREPO:Ljava/lang/String; = "IntentOfInterest.jason"

.field private static final TAG:Ljava/lang/String; = "UMC:PolicyIntentManager"

.field static gPolicyIntentManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;


# instance fields
.field private final gGson:Lcom/google/gson/e;

.field private mIntentOfInterestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;",
            ">;"
        }
    .end annotation
.end field

.field mListenerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;",
            ">;>;"
        }
    .end annotation
.end field

.field pendingIntentMap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gPolicyIntentManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    .line 22
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gGson:Lcom/google/gson/e;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    .line 17
    return-void
.end method

.method private addToInterestOfIntent(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    monitor-enter v1

    .line 146
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;Ljava/lang/String;I)V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    monitor-exit v1

    .line 149
    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private deleteFromInterestOfIntent(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 151
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    monitor-enter v2

    .line 152
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    :goto_0
    monitor-exit v2

    .line 160
    return-void

    .line 152
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 153
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;

    .line 154
    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;->intentAction:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;->admin:I

    if-ne v1, p2, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gPolicyIntentManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gPolicyIntentManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    .line 44
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gPolicyIntentManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    return-object v0
.end method

.method private loadInterestOfIntentRepo()V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    const-string v0, "UMC:PolicyIntentManager"

    const-string v1, "loadInterestOfIntentRepo"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "IntentOfInterest.jason"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    const-string v1, "UMC:PolicyIntentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loadInterestOfIntentRepo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    if-eqz v0, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gGson:Lcom/google/gson/e;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$1;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;)V

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    goto :goto_0
.end method

.method private saveInterestOfIntentRepo()V
    .locals 4

    .prologue
    .line 133
    const-string v0, "UMC:PolicyIntentManager"

    const-string v1, "saveInterestOfIntentRepo"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "IntentOfInterest.jason"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    monitor-enter v1

    .line 137
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->gGson:Lcom/google/gson/e;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    monitor-exit v1

    .line 141
    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public checkAndFlushAllPendingIntents()V
    .locals 2

    .prologue
    .line 206
    const-string v0, "UMC:PolicyIntentManager"

    const-string v1, "checkAndFlushAllPendingIntents"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->hasAnyPendingPolicies()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->hasAnyPendingAlertRequests()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    monitor-enter v1

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 209
    monitor-exit v1

    .line 213
    :cond_0
    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public checkAndFlushPendingIntentsForAnAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V
    .locals 4

    .prologue
    .line 216
    const-string v0, "UMC:PolicyIntentManager"

    const-string v1, "flushPendingIntents"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->hasAnyPendingPolicies()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->hasAnyPendingAlertRequests()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    monitor-enter v1

    .line 220
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 219
    monitor-exit v1

    .line 229
    :cond_0
    return-void

    .line 221
    :cond_1
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v2

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    .line 223
    if-ne v2, v2, :cond_2

    .line 224
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 220
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method dispatch(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    monitor-enter v3

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 55
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 56
    if-eqz v0, :cond_1

    .line 57
    const-string v2, "UMC:PolicyIntentManager"

    const-string v4, "intent dispatched to listner"

    invoke-static {v2, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 58
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lt v2, v1, :cond_0

    .line 64
    :goto_1
    monitor-exit v3

    .line 96
    :goto_2
    return-void

    .line 59
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;->onIntentReceived(Landroid/content/Intent;)V

    .line 58
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 62
    :cond_1
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No one is interested on this intent and ignoring : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->loadInterestOfIntentRepo()V

    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 72
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mIntentOfInterestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v1

    .line 82
    :goto_3
    if-nez v0, :cond_6

    .line 83
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Umc is not interested in this intent and Ignoring it: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 72
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 73
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;

    .line 74
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$IntentOfInterest;->intentAction:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    const/4 v0, 0x1

    .line 76
    goto :goto_3

    .line 91
    :cond_6
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    monitor-enter v1

    .line 92
    :try_start_2
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "added into pending intent quene : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    monitor-exit v1

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public getPendingIntents(Ljava/lang/String;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getPendingIntent  - intentaction = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v1, 0x0

    .line 101
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    monitor-enter v3

    .line 102
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_0

    .line 101
    monitor-exit v3

    .line 113
    return-object v1

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne p2, p2, :cond_2

    .line 105
    if-nez v1, :cond_1

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->pendingIntentMap:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_2
    move-object v0, v1

    .line 102
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerForIntent([Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 163
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "registerForIntent  - intentactions = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    monitor-enter v4

    move v3, v2

    .line 165
    :goto_0
    :try_start_0
    array-length v0, p1

    if-lt v3, v0, :cond_0

    .line 164
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->saveInterestOfIntentRepo()V

    .line 186
    return-void

    .line 167
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    aget-object v1, p1, v3

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 168
    if-nez v0, :cond_1

    .line 169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 172
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    aget-object v5, p1, v3

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    aget-object v0, p1, v3

    invoke-interface {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;->getAdminUID()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->addToInterestOfIntent(Ljava/lang/String;I)V

    .line 175
    aget-object v0, p1, v3

    invoke-interface {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;->getAdminUID()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->getPendingIntents(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v5

    .line 176
    if-eqz v5, :cond_2

    move v1, v2

    .line 177
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_3

    .line 165
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 178
    :cond_3
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "pending intent found and calling listener: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, p1, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-interface {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;->onIntentReceived(Landroid/content/Intent;)V

    .line 177
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public unregisterForIntent([Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;)V
    .locals 5

    .prologue
    .line 189
    const-string v0, "UMC:PolicyIntentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unregisterForIntent  - intentactions = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    monitor-enter v2

    .line 191
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    array-length v0, p1

    if-lt v1, v0, :cond_0

    .line 190
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->saveInterestOfIntentRepo()V

    .line 201
    return-void

    .line 192
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->mListenerMap:Ljava/util/Map;

    aget-object v3, p1, v1

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 193
    aget-object v3, p1, v1

    invoke-interface {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;->getAdminUID()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->deleteFromInterestOfIntent(Ljava/lang/String;I)V

    .line 194
    if-eqz v0, :cond_1

    .line 195
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 191
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
