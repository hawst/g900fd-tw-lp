.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;
.super Lcom/a/a/a/d;
.source "HttpService.java"


# instance fields
.field final synthetic wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;


# direct methods
.method private constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-direct {p0}, Lcom/a/a/a/d;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;)V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)V

    return-void
.end method

.method private b([Lorg/apache/http/Header;)V
    .locals 4

    .prologue
    .line 415
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 423
    :cond_0
    return-void

    .line 419
    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 421
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p1, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(I[Lorg/apache/http/Header;[B)V
    .locals 4

    .prologue
    .line 389
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess:statusCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    .line 391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;I)V

    .line 392
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Z)V

    .line 394
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    .line 395
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSuccess:caller not intersted any tags!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_0
    return-void

    .line 399
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->b([Lorg/apache/http/Header;)V

    .line 400
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/util/List;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 403
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v1

    .line 405
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p2, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 404
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 428
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFailure: statusCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->b([Lorg/apache/http/Header;)V

    .line 431
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    .line 432
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;I)V

    .line 433
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Z)V

    .line 435
    if-eqz p4, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "errorMsg"

    invoke-virtual {p4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFailure: file deleted:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 441
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    .line 444
    :cond_1
    return-void
.end method

.method public onFinish()V
    .locals 4

    .prologue
    .line 448
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onFinish ResponseHandler"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->onFinish()V

    .line 451
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 452
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    .line 453
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)I

    move-result v1

    .line 452
    invoke-direct {v0, v2, v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    .line 455
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->setResponseBundle(Landroid/os/Bundle;)V

    .line 457
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 460
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 465
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/a/a/a/d;->onFinish()V

    .line 467
    return-void

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 4

    .prologue
    .line 379
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onProgress:  bytesWritten:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " totalSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 380
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 379
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onProgress(JII)V

    .line 384
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/a/a/a/d;->onProgress(II)V

    .line 385
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 371
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStart in ResponseHandlerUpload"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onStart(J)V

    .line 374
    :cond_0
    invoke-super {p0}, Lcom/a/a/a/d;->onStart()V

    .line 375
    return-void
.end method
