.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;
.super Ljava/lang/Thread;
.source "ActiveSync.java"


# static fields
.field public static uw:Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPassword:Ljava/lang/String;

.field private pk:Ljava/lang/String;

.field private final uA:Ljava/lang/String;

.field private final uB:Ljava/lang/String;

.field private final uC:Ljava/lang/String;

.field private uu:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;

.field private uv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;

.field private ux:Ljava/lang/String;

.field private uy:Ljava/lang/String;

.field private uz:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uw:Ljava/lang/Object;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uz:Ljava/util/HashMap;

    .line 37
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uu:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;

    .line 38
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->mContext:Landroid/content/Context;

    .line 39
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<Autodiscover xmlns=\"http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006\">   <Request>     <EMailAddress>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 42
    const-string v1, "</EMailAddress>     <AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006</AcceptableResponseSchema>   </Request></Autodiscover>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uy:Ljava/lang/String;

    .line 44
    :try_start_0
    const-string v0, "@"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->ux:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->pk:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->mPassword:Ljava/lang/String;

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Basic "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uz:Ljava/util/HashMap;

    const-string v2, "Authorization"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uz:Ljava/util/HashMap;

    const-string v1, "Content-Type"

    const-string v2, "text/xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->ux:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/autodiscover/autodiscover.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uA:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://autodiscover."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->ux:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/autodiscover/autodiscover.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uB:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://autodiscover."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->ux:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/autodiscover/autodiscover.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uC:Ljava/lang/String;

    .line 59
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 46
    const-string v0, "ActiveSync"

    const-string v1, "Invalid email no @ symbol"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method public hv()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->start()V

    .line 63
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 70
    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->pk:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->mPassword:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->ux:Ljava/lang/String;

    .line 74
    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->mContext:Landroid/content/Context;

    .line 73
    invoke-static/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 76
    const-string v1, "autodiscover_host_auth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 77
    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;

    invoke-interface {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;->gK()V

    .line 86
    :goto_1
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v6

    goto :goto_0

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/a;->uv:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;->ak(Ljava/lang/String;)V

    goto :goto_1
.end method
