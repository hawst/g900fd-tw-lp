.class public final Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;
.source "DeviceVersionChecker.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

.field private pZ:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 98
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->pZ:Ljava/lang/String;

    return-void
.end method

.method public static fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/e;->fQ()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public fO()I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 60
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->pZ:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    :try_start_0
    const-string v3, "ro.build.PDA"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    const-string v4, "DeviceVersionChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PDA : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 70
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->pZ:Ljava/lang/String;

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 71
    const/4 v5, 0x0

    aget-object v5, v4, v5

    .line 72
    const-string v6, "DeviceVersionChecker"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "compareVersion : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 76
    invoke-virtual {v3, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    .line 78
    if-gez v3, :cond_0

    .line 81
    const/4 v0, 0x1

    aget-object v0, v4, v0

    const-string v3, "X"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 82
    goto :goto_0

    :cond_2
    move v0, v2

    .line 84
    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 89
    goto :goto_0
.end method

.method public m(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 109
    const-string v0, "DeviceVersionChecker"

    const-string v1, "Starting versionLookup: "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->mContext:Landroid/content/Context;

    .line 112
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d$1;

    .line 113
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 112
    invoke-direct {v0, p0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 132
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 133
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->mContext:Landroid/content/Context;

    invoke-direct {v2, v1, v3, v5, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 134
    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/my-knox-whitelist/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-virtual {v2, v1, v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    .line 139
    return-void
.end method
