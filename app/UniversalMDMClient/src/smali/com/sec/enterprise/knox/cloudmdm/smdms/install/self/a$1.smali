.class Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/b;
.source "SelfUpdateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->g(Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;)V
    .locals 1

    .prologue
    .line 1
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    .line 205
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/b;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/b;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 2

    .prologue
    .line 262
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onFailure: "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const/4 v0, -0x1

    .line 265
    if-eqz p1, :cond_2

    .line 266
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->hQ()V

    .line 269
    iget v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->status:I

    const/16 v1, 0x130

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->status:I

    const/16 v1, 0x193

    if-eq v0, v1, :cond_0

    .line 270
    iget v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->status:I

    const/16 v1, 0x194

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->status:I

    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    .line 271
    :cond_0
    const/16 v0, 0xa

    .line 272
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;I)V

    .line 282
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;I)V

    goto :goto_0

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;I)V

    goto :goto_0
.end method

.method public onProgress(JII)V
    .locals 0

    .prologue
    .line 288
    return-void
.end method

.method public onStart(J)V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 215
    const/4 v0, 0x0

    .line 217
    if-eqz p1, :cond_0

    .line 218
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->hQ()V

    .line 219
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->getResponseBundle()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 220
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    const-string v3, "ETag"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 221
    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    const-string v4, "Content-Length"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 222
    iget-object v4, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    const-string v5, "x-amz-meta-apk-version"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 224
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->access$0()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "latestVersion: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->access$0()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "New eTagValue: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->access$0()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TotalSize: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 229
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->gd()Landroid/content/Context;

    move-result-object v2

    .line 230
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->gd()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 229
    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->s(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 231
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "currentVersion: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-static {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->y(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 249
    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    .line 253
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;I)V

    .line 258
    :goto_1
    return-void

    .line 235
    :cond_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 236
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v4, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;I)V

    .line 237
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;)I

    move-result v3

    if-lez v3, :cond_0

    .line 238
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;)Ljava/lang/String;

    move-result-object v3

    .line 239
    if-eqz v3, :cond_2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 240
    :cond_2
    if-nez v3, :cond_4

    :cond_3
    move v0, v1

    .line 242
    :cond_4
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->access$0()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "stored eTagValue: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a$1;->rD:Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;I)V

    goto :goto_1
.end method
