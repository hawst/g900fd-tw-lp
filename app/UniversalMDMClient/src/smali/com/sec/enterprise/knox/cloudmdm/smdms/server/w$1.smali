.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;
.source "ProxyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;->wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    .line 107
    invoke-direct {p0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 124
    .line 125
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error from async client: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    .line 128
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpResponseException !"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    .line 129
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    .line 130
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    .line 132
    const/16 v1, 0x199

    if-ne v0, v1, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;->wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;->wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-static {v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;)V

    .line 137
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 113
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":process response on success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;->wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;->wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-static {v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;)V

    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w$1;->wY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/w;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    const-string v1, "Create Account Error"

    invoke-super {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 120
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->c(ILjava/lang/String;)V

    goto :goto_0
.end method
