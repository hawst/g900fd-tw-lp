.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;
.super Ljava/lang/Object;
.source "ObjectTranslator.java"


# instance fields
.field caCertificate:Ljava/lang/String;

.field clientCertificate:Ljava/lang/String;

.field theWifiConfiguration:Landroid/net/wifi/WifiConfiguration;


# direct methods
.method constructor <init>(Landroid/net/wifi/WifiConfiguration;)V
    .locals 1

    .prologue
    .line 983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 984
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;->theWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    .line 985
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificate()Ljava/security/cert/X509Certificate;

    move-result-object v0

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->X509toPEMSTRING(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->access$0(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;->caCertificate:Ljava/lang/String;

    .line 986
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0}, Landroid/net/wifi/WifiEnterpriseConfig;->getClientCertificate()Ljava/security/cert/X509Certificate;

    move-result-object v0

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->X509toPEMSTRING(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->access$0(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$WifiConfigurationForUMC;->clientCertificate:Ljava/lang/String;

    .line 987
    return-void
.end method
