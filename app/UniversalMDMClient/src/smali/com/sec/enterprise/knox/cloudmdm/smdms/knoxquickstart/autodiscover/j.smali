.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;
.super Ljava/lang/Thread;
.source "EmailValidator.java"


# instance fields
.field private dJ:Z

.field private dK:Z

.field private mContext:Landroid/content/Context;

.field private mPassword:Ljava/lang/String;

.field private pk:Ljava/lang/String;

.field private tG:Ljava/lang/String;

.field private uh:Ljava/lang/String;

.field private ui:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 17
    iput-object p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->ui:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;

    .line 18
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->pk:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->mPassword:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->tG:Ljava/lang/String;

    .line 21
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->uh:Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->mContext:Landroid/content/Context;

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->dJ:Z

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->dK:Z

    .line 25
    return-void
.end method


# virtual methods
.method public hB()V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->run()V

    .line 38
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    .line 31
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->uh:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->tG:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->mPassword:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->dJ:Z

    if-eqz v4, :cond_0

    const/16 v4, 0x1bb

    :goto_0
    iget-boolean v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->dJ:Z

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->mContext:Landroid/content/Context;

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLandroid/content/Context;)V

    .line 32
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hs()I

    move-result v1

    .line 33
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->ui:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->pk:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->mPassword:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->uh:Ljava/lang/String;

    const-string v5, ""

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/j;->pk:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-void

    .line 31
    :cond_0
    const/16 v4, 0x50

    goto :goto_0
.end method
