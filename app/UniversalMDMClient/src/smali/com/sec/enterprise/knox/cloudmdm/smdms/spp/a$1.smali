.class Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$1;
.super Ljava/lang/Object;
.source "SppInterface.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;
.end annotation


# instance fields
.field final synthetic xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$1;->xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 181
    const-string v0, "UMC:SppInterface"

    const-string v1, "[ServiceConnection]onServiceConnected"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-static {p2}, Lcom/sec/a/a/b;->d(Landroid/os/IBinder;)Lcom/sec/a/a/a;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->a(Lcom/sec/a/a/a;)V

    .line 184
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->is()Lcom/sec/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 185
    const-string v0, "UMC:SppInterface"

    const-string v1, "[ServiceConnection]ServiceConnection-mService is not null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    const-string v0, "UMC:SppInterface"

    const-string v1, "[ServiceConnection]ServiceConnection-mService is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 193
    const-string v0, "UMC:SppInterface"

    const-string v1, "[ServiceConnection]onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->a(Lcom/sec/a/a/a;)V

    .line 196
    return-void
.end method
