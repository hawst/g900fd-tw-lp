.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;
.super Ljava/lang/Object;
.source "AsyncNetworkClient.java"

# interfaces
.implements Ljavax/net/ssl/X509KeyManager;


# instance fields
.field private pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

.field private vp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    const-string v0, "hash"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->vp:Ljava/lang/String;

    .line 211
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->vp:Ljava/lang/String;

    .line 212
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 213
    return-void
.end method

.method private a([Ljava/security/cert/Certificate;)[Ljava/security/cert/X509Certificate;
    .locals 3

    .prologue
    .line 276
    array-length v0, p1

    new-array v2, v0, [Ljava/security/cert/X509Certificate;

    .line 278
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-lt v1, v0, :cond_0

    .line 288
    return-object v2

    .line 285
    :cond_0
    aget-object v0, p1, v1

    check-cast v0, Ljava/security/cert/X509Certificate;

    aput-object v0, v2, v1

    .line 278
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 217
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "chooseClientAlias : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->vp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->vp:Ljava/lang/String;

    return-object v0
.end method

.method public chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 224
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "chooseServerAlias"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 3

    .prologue
    .line 230
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCertificateChain : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/4 v0, 0x0

    .line 233
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->hI()Ljava/security/KeyStore;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v1

    .line 234
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->a([Ljava/security/cert/Certificate;)[Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 235
    :catch_0
    move-exception v1

    .line 236
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_0

    .line 237
    :catch_1
    move-exception v1

    .line 238
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0
.end method

.method public getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 245
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getClientAliases"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 247
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->vp:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 246
    return-object v0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 3

    .prologue
    .line 253
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getPrivateKey : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const/4 v1, 0x0

    .line 256
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->hI()Ljava/security/KeyStore;

    move-result-object v0

    .line 257
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->hJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 256
    invoke-virtual {v0, p1, v2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v0

    check-cast v0, Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2

    .line 265
    :goto_0
    return-object v0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    invoke-virtual {v0}, Ljava/security/UnrecoverableKeyException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 260
    :catch_1
    move-exception v0

    .line 261
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 262
    :catch_2
    move-exception v0

    .line 263
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0
.end method

.method public getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->ry:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getServerAliases"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v0, 0x0

    return-object v0
.end method
