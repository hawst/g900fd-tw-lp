.class public final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;
.super Ljava/lang/Enum;
.source "GatewayOperations.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum wa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

.field private static final synthetic wb:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;->wa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;->wa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;->wb:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;->wb:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/GatewayOperations$Property;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
