.class public Lcom/sec/enterprise/knox/Attestation;
.super Ljava/lang/Object;
.source "Attestation.java"


# static fields
.field public static final ACTION_KNOX_ATTESTATION_RESULT:Ljava/lang/String; = "com.sec.enterprise.knox.intent.action.KNOX_ATTESTATION_RESULT"

.field public static final EXTRA_ATTESTATION_DATA:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.ATTESTATION_DATA"

.field public static final EXTRA_ERROR_MSG:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.ERROR_MSG"

.field public static final EXTRA_NETWORK_ERROR:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.NETWORK_ERROR"

.field public static final EXTRA_RESULT:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.RESULT"

.field public static final RESULT_ATTESTATION_SUCCESSFUL:I = 0x0

.field public static final RESULT_ERROR_DEVICE_NOT_SUPPORTED:I = -0x3

.field public static final RESULT_ERROR_INVALID_NONCE:I = -0x5

.field public static final RESULT_ERROR_MDM_PERMISSION:I = -0x1

.field public static final RESULT_ERROR_TIMA_INTERNAL:I = -0x2

.field public static final RESULT_ERROR_UNKNOWN:I = -0x4

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Lcom/sec/enterprise/knox/IAttestation;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private nonce:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "UMC:Attestation"

    sput-object v0, Lcom/sec/enterprise/knox/Attestation;->TAG:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 352
    new-instance v0, Lcom/sec/enterprise/knox/Attestation$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/Attestation$1;-><init>(Lcom/sec/enterprise/knox/Attestation;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 372
    iput-object p2, p0, Lcom/sec/enterprise/knox/Attestation;->mContext:Landroid/content/Context;

    .line 373
    iput-object p1, p0, Lcom/sec/enterprise/knox/Attestation;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 374
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/enterprise/knox/Attestation;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/Attestation;Lcom/sec/enterprise/knox/IAttestation;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/enterprise/knox/Attestation;)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/sec/enterprise/knox/Attestation;->doAttestation()V

    return-void
.end method

.method private doAttestation()V
    .locals 4

    .prologue
    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->nonce:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->nonce:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    :cond_0
    const-string v0, "Attestation"

    const-string v1, "Invalid Nonce"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :goto_0
    return-void

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    if-eqz v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    iget-object v1, p0, Lcom/sec/enterprise/knox/Attestation;->nonce:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/IAttestation;->startAttestation_nonce(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    const-string v1, "Attestation"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "doAttestation exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_2
    :try_start_1
    const-string v0, "Attestation"

    const-string v1, "Attestation Agent Service Not Available"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private getService()Lcom/sec/enterprise/knox/IAttestation;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    if-nez v0, :cond_0

    .line 378
    const-string v0, "attestation"

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/h;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/a;->c(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IAttestation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    return-object v0
.end method


# virtual methods
.method public getDeviceKnoxId()I
    .locals 1

    .prologue
    .line 349
    const/4 v0, 0x0

    return v0
.end method

.method public setAttestationServerUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x0

    return v0
.end method

.method public startAttestation()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public startAttestation_nonce(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/enterprise/knox/Attestation;->nonce:Ljava/lang/String;

    .line 252
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mService:Lcom/sec/enterprise/knox/IAttestation;

    if-nez v0, :cond_0

    .line 253
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 254
    const-string v1, "com.sec.enterprise.knox.intent.action.BIND_KNOX_ATTESTATION_SERVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    sget-object v1, Lcom/sec/enterprise/knox/Attestation;->TAG:Ljava/lang/String;

    const-string v2, "bind service:"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v1, p0, Lcom/sec/enterprise/knox/Attestation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/Attestation;->doAttestation()V

    goto :goto_0
.end method
