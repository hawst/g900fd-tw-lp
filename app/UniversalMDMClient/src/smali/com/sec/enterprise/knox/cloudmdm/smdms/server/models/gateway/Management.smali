.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;
.super Ljava/lang/Object;
.source "Management.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private agent:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

.field private certificate:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

.field private icon:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

.field private service:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->agent:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    return-object v0
.end method

.method public getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
        ignoreUnknown = true
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->certificate:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    return-object v0
.end method

.method public getIcon()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->icon:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

    return-object v0
.end method

.method public getService()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->service:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;

    return-object v0
.end method

.method public setAgent(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->agent:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Agent;

    .line 106
    return-void
.end method

.method public setCertificate(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->certificate:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    .line 75
    return-void
.end method

.method public setIcon(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->icon:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Icon;

    .line 114
    return-void
.end method

.method public setService(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->service:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;

    .line 98
    return-void
.end method
