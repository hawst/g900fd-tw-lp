.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;
.source "GSLBManager.java"


# static fields
.field private static ry:Ljava/lang/String;

.field private static vO:Ljava/lang/String;

.field private static vP:Ljava/lang/String;

.field public static vQ:Ljava/lang/String;

.field public static vR:Ljava/lang/String;

.field public static vS:Ljava/lang/String;

.field private static vT:[Ljava/lang/String;


# instance fields
.field private pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const-string v0, "GSLBManager"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    .line 30
    const-string v0, ""

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vO:Ljava/lang/String;

    .line 31
    const-string v0, "/KnoxGSLB/lookup/"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vP:Ljava/lang/String;

    .line 35
    const-string v0, "segd-api"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    .line 36
    const-string v0, "umc-cdn"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    .line 37
    const-string v0, "segp-api"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 39
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 38
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vT:[Ljava/lang/String;

    .line 40
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;-><init>()V

    return-void
.end method

.method private E(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 58
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->I(Landroid/content/Context;)Ljava/util/Properties;

    move-result-object v0

    .line 60
    const-string v1, "ro.csc.country_code"

    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/d;->p(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GSLB country: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    if-eqz v1, :cond_0

    const-string v2, "China"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const-string v1, "PROD_CHINA_GSLB_URL"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vO:Ljava/lang/String;

    .line 73
    :goto_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GSLB_URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vO:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vO:Ljava/lang/String;

    invoke-direct {v0, v1, p1, v5, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 75
    return-void

    .line 66
    :cond_0
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xv:Z

    if-eqz v1, :cond_1

    .line 67
    const-string v1, "PROD_GLOBAL_GSLB_URL"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vO:Ljava/lang/String;

    goto :goto_0

    .line 69
    :cond_1
    const-string v1, "DEV_GLOBAL_GSLB_URL"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vO:Ljava/lang/String;

    goto :goto_0
.end method

.method public static F(Landroid/content/Context;)Lorg/json/JSONObject;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 156
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;-><init>(Landroid/content/Context;)V

    .line 157
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 159
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 163
    :try_start_0
    const-string v1, "deviceid"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->ik()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 165
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->im()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    const-string v1, "country_iso"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->im()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 169
    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->il()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 170
    const-string v1, "csc"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->il()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 173
    :cond_1
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->ij()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 174
    const-string v1, "mcc"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->ij()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 177
    :cond_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;-><init>()V

    .line 178
    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    const-string v2, "GSLB"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 178
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 181
    const-string v1, "data"

    invoke-virtual {v6, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 182
    const-string v1, "signature"

    invoke-virtual {v6, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v6

    .line 198
    :goto_0
    return-object v0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "compriseRedirectionRequestJSON JSONException occurred : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 185
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v7

    .line 189
    goto :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    .line 192
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "compriseRedirectionRequestJSON Exception occurred : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v7

    .line 195
    goto :goto_0
.end method

.method public static G(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 312
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "migrateStoredUrlFromOldPref"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 314
    const/4 v1, 0x0

    .line 313
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 315
    if-eqz v2, :cond_2

    .line 316
    invoke-interface {v2}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 318
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 322
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "migrateStoredUrlFromOldPref : Finished Migrating"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :goto_1
    return-void

    .line 318
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 319
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 324
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "migrateStoredUrlFromOldPref : Migration Not Needed"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 327
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "migrateStoredUrlFromOldPref : PREF IS NULL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->aP(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;Ljava/lang/String;Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 225
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method private aP(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 206
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "Converting lookupservice error to json"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;

    .line 208
    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;

    .line 210
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Converted error code:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->getError()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->getError_code()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 211
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->getError()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->getError_description()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->getError()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->getError_code()I

    move-result v0

    .line 219
    :goto_0
    return v0

    .line 214
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "lookupservice response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :goto_1
    const/4 v0, -0x1

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processGslbLookupError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Landroid/content/Context;)I
    .locals 7

    .prologue
    .line 228
    if-eqz p1, :cond_4

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4

    .line 229
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "Converting lookupservice response to object"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;

    .line 230
    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;

    .line 232
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    .line 233
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Converted"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getEndpoint()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getService()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 232
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getEndpoint()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 235
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;

    .line 236
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getProtocol()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 240
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getPort()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 241
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Setting GSLB returned https url to : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getProtocol()Ljava/lang/String;

    move-result-object v4

    const-string v5, "https"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 244
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getService()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 246
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "seg_url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vQ:Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 270
    :catch_0
    move-exception v0

    .line 271
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processGslbLookupResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 251
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getProtocol()Ljava/lang/String;

    move-result-object v4

    const-string v5, "http"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 252
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getService()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 254
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "umcUrl: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-static {v0, v3, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 259
    :cond_3
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->getProtocol()Ljava/lang/String;

    move-result-object v1

    const-string v4, "https"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getService()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 262
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "proxy_url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 268
    :cond_4
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "lookupservice response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 302
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 303
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 305
    :goto_0
    if-eqz v0, :cond_0

    .line 306
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 307
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 309
    :cond_0
    return-void

    .line 303
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method

.method public static hR()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/n;->hT()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic hS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    return-object v0
.end method

.method public static l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 278
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_2

    .line 280
    const-string v2, ""

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 287
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public k(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 100
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lookup for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->E(Landroid/content/Context;)V

    .line 103
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 104
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 111
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting operation to be lookedup as: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;

    invoke-direct {v1, p0, v0, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Landroid/content/Context;)V

    .line 138
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v0, :cond_2

    .line 139
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v0, "gslbresponse.json"

    .line 141
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;

    .line 140
    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;

    .line 142
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "service name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse;->getService()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :goto_1
    return-void

    .line 107
    :cond_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vS:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    goto :goto_0

    .line 144
    :cond_2
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->F(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 145
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GSLB Request : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GSLB Request TimeZone: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GSLB Request Time: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vP:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    goto :goto_1
.end method
