.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private applicationId:Ljava/lang/String;

.field private key:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    return-void
.end method


# virtual methods
.method public getApplicationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->applicationId:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setApplicationId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->applicationId:Ljava/lang/String;

    .line 215
    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->key:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->title:Ljava/lang/String;

    .line 226
    return-void
.end method
