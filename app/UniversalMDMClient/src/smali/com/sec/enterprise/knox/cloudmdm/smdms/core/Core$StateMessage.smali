.class final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;
.super Ljava/lang/Enum;
.source "Core.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum pE:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pF:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pG:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pH:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pL:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pN:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pO:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pP:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pS:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pU:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field public static final enum pW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

.field private static final synthetic pX:[Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 597
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_MANUAL_ENROLLMENT_LAUNCH"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pE:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_EMAIL_ENTERED"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pF:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_PROFILE_DISCOVERY_RESPONSE"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pG:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_SHOW_MDM_SERVER_SELECTION"

    invoke-direct {v0, v1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pH:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_CREDS_ENTERED"

    invoke-direct {v0, v1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_ENROLLMENT_RESPONSE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_ENROLLMENT_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_ENROLLMENT_FAILURE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pL:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_MANAGEMENT_PUSH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_APPLY_POLICIES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pN:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_POLICIES_APPLIED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pO:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_POLICIES_COMPLETE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pP:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_USER_CANCELLED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_NETWORK_ERROR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_MDM_SERVER_SELECTED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pS:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_EULA_RESULT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_EULAS_DOWNLOADED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pU:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_ENROLLMENT_COMPLETE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    const-string v1, "MSG_DEVICE_VERSION_CHECK"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    .line 596
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pE:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pF:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pG:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pH:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pL:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pN:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pO:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pP:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pS:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pU:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pX:[Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 596
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pX:[Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
