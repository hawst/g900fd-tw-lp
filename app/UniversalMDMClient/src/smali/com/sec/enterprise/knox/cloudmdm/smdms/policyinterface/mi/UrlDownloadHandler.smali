.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;
.super Ljava/lang/Object;
.source "UrlDownloadHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "UrlDownloadHandler"


# instance fields
.field private mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field private mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

.field private mContext:Landroid/content/Context;

.field mDownloadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;

.field private mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

.field mInputObjects:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;",
            ">;"
        }
    .end annotation
.end field

.field mInputUrls:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPendingUrlArgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputObjects:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputUrls:Ljava/util/Map;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    .line 67
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    .line 69
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 76
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mContext:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 78
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;

    .line 79
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 80
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->handleDownloadSuccess(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private download(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 117
    const-string v0, "UrlDownloadHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "download()ArgName: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 120
    const-string v0, "."

    invoke-virtual {p2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->extensionHasSpecialCaharacters(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->formLocalDownloadFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    :goto_0
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mContext:Landroid/content/Context;

    .line 130
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;

    invoke-direct {v5, p0, p0, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;-><init>(Landroid/content/Context;JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V

    .line 129
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    invoke-virtual {v1, v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->download(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_1
    return-void

    .line 125
    :cond_0
    invoke-direct {p0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->formLocalDownloadFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    const-string v0, "UrlDownloadHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cant handle the url, so aborting:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v0, "UrlDownloadHandler"

    const-string v1, "download()onFailure "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 180
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;

    .line 181
    const/4 v1, 0x1

    .line 180
    invoke-interface {v0, p1, v6, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;->onUrlDownloadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_1
.end method

.method private extensionHasSpecialCaharacters(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 107
    const-string v1, "[/$&+,:;=?@#|-]"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 108
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 109
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    const/4 v0, 0x1

    .line 112
    :cond_0
    return v0
.end method

.method private formLocalDownloadFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 218
    if-nez p1, :cond_0

    .line 219
    const-string p1, ".temp"

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->getHostName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    :cond_1
    const-string v0, "download"

    .line 226
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 228
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 229
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 230
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 234
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/dl_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleDownloadSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 190
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialRequestParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 195
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 194
    invoke-static {v1, p2, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->translateObject(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputObjects:Ljava/util/Map;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->type:Ljava/lang/reflect/Type;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    invoke-direct {v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 201
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputObjects:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;->onUrlDownloadSuccess(Ljava/util/Map;Z)V

    .line 214
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 205
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputUrls:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->download(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 210
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 211
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mDownloadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler$DownloadListener;->onUrlDownloadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0
.end method


# virtual methods
.method addDownloadRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputUrls:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method hasUrlsToDownload()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mPendingUrlArgs:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->mInputUrls:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->download(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    return-void
.end method

.method public startDownload()V
    .locals 0

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlDownloadHandler;->run()V

    .line 95
    return-void
.end method
