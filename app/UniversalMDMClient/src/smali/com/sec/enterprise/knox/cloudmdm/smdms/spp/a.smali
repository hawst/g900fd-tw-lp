.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;
.super Ljava/lang/Object;
.source "SppInterface.java"


# static fields
.field private static xB:Lcom/sec/a/a/a;


# instance fields
.field private mContext:Landroid/content/Context;

.field private xC:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;

.field public xD:Landroid/content/BroadcastReceiver;

.field private xE:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/SppMessageReceiver;

.field private xF:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    .line 177
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xF:Landroid/content/ServiceConnection;

    .line 30
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xC:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;

    .line 32
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xD:Landroid/content/BroadcastReceiver;

    .line 129
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 130
    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xD:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 133
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    const-string v1, "reqType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 135
    const-string v1, "appId"

    const-string v2, "485c57ce73420eed"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string v1, "userdata"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 143
    const-string v0, "UMC:SppInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registered the SPP Receiver isPushAvailable?:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->ir()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/SppMessageReceiver;

    invoke-direct {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/SppMessageReceiver;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xE:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/SppMessageReceiver;

    .line 147
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 148
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 149
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xE:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/SppMessageReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 153
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 154
    const-string v1, "485c57ce73420eed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    const-string v1, "com.sec.spp.NotificationAckResultAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xE:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/SppMessageReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 160
    const-string v0, "485c57ce73420eed"

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->u(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->iq()V

    .line 162
    const-string v0, "UMC:SppInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SPP regID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "485c57ce73420eed"

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->be(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method static synthetic a(Lcom/sec/a/a/a;)V
    .locals 0

    .prologue
    .line 21
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xC:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;

    return-object v0
.end method

.method static synthetic is()Lcom/sec/a/a/a;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    return-object v0
.end method


# virtual methods
.method public be(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 260
    const-string v0, "UMC:SppInterface"

    const-string v1, "getRegId()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    .line 264
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    invoke-interface {v0, p1}, Lcom/sec/a/a/a;->be(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.getRegId("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") requested. regid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :goto_0
    return-object v0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 269
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.getRegId() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 272
    :cond_0
    const-string v0, "UMC:SppInterface"

    const-string v1, "mService is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public finalize()V
    .locals 4

    .prologue
    .line 169
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->onPause()V

    .line 175
    return-void

    .line 170
    :catch_0
    move-exception v0

    .line 171
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 172
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "finalize() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public iq()V
    .locals 4

    .prologue
    .line 202
    const-string v0, "UMC:SppInterface"

    const-string v1, "setUpSpp"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    .line 205
    const-string v2, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 210
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xF:Landroid/content/ServiceConnection;

    .line 211
    const/4 v3, 0x1

    .line 210
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 212
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindService(mPushClientConnection) requested. bResult="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    const-string v0, "UMC:SppInterface"

    const-string v1, "Component name com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION was not found"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public ir()Z
    .locals 4

    .prologue
    .line 278
    const-string v0, "UMC:SppInterface"

    const-string v1, "isPushAvailable()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    .line 282
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    invoke-interface {v0}, Lcom/sec/a/a/a;->ir()Z

    move-result v0

    .line 283
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.isPushAvailable() requested. bPushAvailable : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 284
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 283
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :goto_0
    return v0

    .line 286
    :catch_0
    move-exception v0

    .line 287
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 288
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.isPushAvailable() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 291
    :cond_0
    const-string v0, "UMC:SppInterface"

    const-string v1, "mService is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 219
    const-string v0, "UMC:SppInterface"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xF:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 222
    const-string v0, "UMC:SppInterface"

    const-string v1, "unbindService(mPushClientConnection) requested."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    return-void
.end method

.method public u(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 228
    const-string v0, "UMC:SppInterface"

    const-string v1, "registration()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    if-eqz v0, :cond_0

    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 233
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->xB:Lcom/sec/a/a/a;

    invoke-interface {v1, p1, v0}, Lcom/sec/a/a/a;->u(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v0, "UMC:SppInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mService.registration("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") requested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 237
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.registration() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :cond_0
    const-string v0, "UMC:SppInterface"

    const-string v1, "mService is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
