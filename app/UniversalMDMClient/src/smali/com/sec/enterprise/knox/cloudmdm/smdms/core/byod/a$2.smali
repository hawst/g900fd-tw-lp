.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;
.super Ljava/lang/Object;
.source "ByodSetupStarter.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;
.end annotation


# instance fields
.field final synthetic qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 394
    if-nez p1, :cond_0

    .line 395
    if-eqz p2, :cond_1

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    const v1, 0x7f080099

    .line 398
    const v2, 0x7f08009f

    const v3, 0x7f080058

    .line 397
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 399
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    const/4 v1, -0x4

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 442
    :goto_0
    return-void

    .line 403
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->i(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    .line 404
    check-cast p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/b;

    .line 408
    iget-boolean v0, p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/b;->px:Z

    if-eqz v0, :cond_2

    .line 409
    const-string v0, "UMC:ByodSetupStarter"

    const-string v1, "Create Container with MDM Agent"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->j(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 413
    invoke-virtual {v1, v4, v2}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 414
    invoke-virtual {v1, v4, v2}, Ljava/io/File;->setReadable(ZZ)Z

    .line 421
    :goto_1
    const-string v1, "UMC:ByodSetupStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 423
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    .line 424
    const-string v2, "knox-b2b"

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Ljava/lang/String;Landroid/net/Uri;)I

    move-result v0

    .line 423
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 425
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "containerRequestId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v0

    if-gez v0, :cond_4

    .line 427
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    const/4 v1, -0x3

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 428
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 429
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 430
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v0

    const/16 v1, -0x3f8

    if-ne v0, v1, :cond_3

    .line 431
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080080

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->b(Landroid/content/Context;I)V

    .line 441
    :goto_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableAppStop(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 416
    :cond_2
    const-string v0, "UMC:ByodSetupStarter"

    const-string v1, "Create Container with UMC Admin"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    goto/16 :goto_1

    .line 433
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08007f

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->b(Landroid/content/Context;I)V

    goto :goto_2

    .line 437
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 438
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080079

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    goto :goto_2
.end method
