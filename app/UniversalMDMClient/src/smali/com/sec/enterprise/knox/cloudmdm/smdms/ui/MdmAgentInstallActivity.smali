.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;
.super Landroid/app/Activity;
.source "MdmAgentInstallActivity.java"


# static fields
.field private static aC:Landroid/app/Notification;

.field private static mAppContext:Landroid/content/Context;

.field private static mHandler:Landroid/os/Handler;

.field private static ry:Ljava/lang/String;

.field private static yV:Landroid/app/Activity;

.field private static yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

.field static yX:I

.field private static yY:Landroid/app/AlertDialog$Builder;

.field private static yZ:Landroid/app/ProgressDialog;

.field private static yd:Z

.field private static za:Landroid/app/ProgressDialog;

.field private static zb:Landroid/os/PowerManager$WakeLock;

.field private static zc:Landroid/widget/RemoteViews;

.field private static zd:I

.field private static ze:I

.field private static zf:I

.field private static zg:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    .line 37
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    .line 38
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 39
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 41
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    .line 42
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    .line 43
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    .line 44
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    .line 45
    sput-boolean v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yd:Z

    .line 46
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    .line 53
    const-string v0, "UMC:MdmAgentInstallActivity"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    .line 61
    sput v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    .line 62
    const/16 v0, 0x1e61

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zg:I

    .line 64
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mHandler:Landroid/os/Handler;

    .line 71
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic D(Z)V
    .locals 0

    .prologue
    .line 45
    sput-boolean p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yd:Z

    return-void
.end method

.method public static O(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 239
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "showProgressOnNotification"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    .line 241
    const/4 v0, 0x3

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 242
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jd()V

    .line 244
    return-void
.end method

.method static synthetic a(Landroid/app/AlertDialog$Builder;)V
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method static synthetic a(Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 41
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    return-void
.end method

.method public static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;)V
    .locals 0

    .prologue
    .line 164
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    .line 165
    return-void
.end method

.method static synthetic b(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 36
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    return-void
.end method

.method public static bf(I)V
    .locals 2

    .prologue
    .line 204
    const/4 v0, 0x4

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 205
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 206
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->bg(I)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "showRetryPopup() failed because mSelf is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static bg(I)V
    .locals 1

    .prologue
    .line 273
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    packed-switch v0, :pswitch_data_0

    .line 289
    :goto_0
    :pswitch_0
    return-void

    .line 275
    :pswitch_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iZ()V

    goto :goto_0

    .line 278
    :pswitch_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ja()V

    goto :goto_0

    .line 281
    :pswitch_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jb()V

    goto :goto_0

    .line 284
    :pswitch_4
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->bh(I)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static bh(I)V
    .locals 5

    .prologue
    .line 447
    const v0, 0x7f080059

    .line 448
    const v1, 0x7f080053

    .line 449
    const v2, 0x7f080099

    .line 452
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-nez v3, :cond_0

    .line 453
    new-instance v3, Landroid/app/AlertDialog$Builder;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 455
    :cond_0
    if-nez p0, :cond_1

    .line 456
    const p0, 0x7f08007b

    .line 458
    :cond_1
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 459
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 460
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 462
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$7;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$7;-><init>()V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 473
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$8;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$8;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 486
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 487
    return-void
.end method

.method public static c(ZI)V
    .locals 2

    .prologue
    .line 190
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 201
    :goto_0
    return-void

    .line 192
    :cond_0
    const/4 v0, 0x0

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    .line 194
    if-nez p0, :cond_1

    .line 195
    const/4 v0, 0x4

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 197
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 198
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->e(ZI)V

    goto :goto_0

    .line 200
    :cond_2
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->d(ZI)V

    goto :goto_0
.end method

.method public static close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 213
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "close"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iW()V

    .line 215
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iX()V

    .line 216
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iY()V

    .line 217
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iU()V

    .line 218
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    .line 219
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 220
    const/4 v0, 0x0

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    .line 221
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 222
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 223
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 224
    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    .line 226
    :cond_0
    return-void
.end method

.method private static d(ZI)V
    .locals 2

    .prologue
    .line 437
    if-nez p0, :cond_0

    .line 438
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 439
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 440
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 441
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 443
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iY()V

    .line 444
    return-void
.end method

.method public static e(II)V
    .locals 2

    .prologue
    .line 169
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    sput p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zd:I

    .line 173
    sput p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ze:I

    .line 175
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zd:I

    mul-int/lit8 v0, v0, 0x64

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ze:I

    div-int/2addr v0, v1

    .line 176
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    if-le v0, v1, :cond_0

    .line 177
    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    .line 179
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 180
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->je()V

    goto :goto_0

    .line 182
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jc()V

    goto :goto_0
.end method

.method private static e(ZI)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0800b6

    const/high16 v6, 0x8000000

    const/4 v7, 0x0

    .line 541
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "updateAppInstallStatusNotificationbar()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 577
    :goto_0
    return-void

    .line 546
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    .line 547
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 546
    check-cast v0, Landroid/app/NotificationManager;

    .line 548
    new-instance v3, Landroid/support/v4/app/v;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v1}, Landroid/support/v4/app/v;-><init>(Landroid/content/Context;)V

    .line 551
    if-eqz p0, :cond_1

    .line 552
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const v2, 0x7f080069

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 554
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {v1, v7, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 568
    :goto_1
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/v;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v4

    .line 569
    const v5, 0x7f020049

    invoke-virtual {v4, v5}, Landroid/support/v4/app/v;->g(I)Landroid/support/v4/app/v;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/support/v4/app/v;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v4

    .line 570
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/support/v4/app/v;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v2

    .line 571
    invoke-virtual {v2, v7, v7, v7}, Landroid/support/v4/app/v;->a(IIZ)Landroid/support/v4/app/v;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/support/v4/app/v;->d(Z)Landroid/support/v4/app/v;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/v;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/v;

    .line 573
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zg:I

    invoke-virtual {v3}, Landroid/support/v4/app/v;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 574
    sput-object v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    .line 575
    sput-object v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    goto :goto_0

    .line 558
    :cond_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 559
    new-instance v1, Landroid/content/Intent;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 560
    const-string v4, "com.sec.enterprise.knox.cloudmdm.smdms.ui.MdmAgentInstallActivity.intent.UNHIDE_PROGRESS_BAR"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 561
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 562
    const-string v4, "showWindow"

    const/4 v5, 0x4

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 563
    const-string v4, "msg"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 564
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const/16 v5, 0x4d2

    invoke-static {v4, v5, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_1
.end method

.method static synthetic gp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    return-object v0
.end method

.method public static iT()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x3

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 158
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->bg(I)V

    .line 161
    :cond_0
    return-void
.end method

.method public static iU()V
    .locals 2

    .prologue
    .line 229
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    .line 230
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 229
    check-cast v0, Landroid/app/NotificationManager;

    .line 231
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zg:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 232
    return-void
.end method

.method public static iV()Z
    .locals 1

    .prologue
    .line 235
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yd:Z

    return v0
.end method

.method public static iW()V
    .locals 2

    .prologue
    .line 247
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "clearLoading() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 250
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    .line 253
    :cond_0
    return-void
.end method

.method public static iX()V
    .locals 2

    .prologue
    .line 256
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_0

    .line 257
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "clearUpdateAvailable() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 261
    :cond_0
    return-void
.end method

.method public static iY()V
    .locals 2

    .prologue
    .line 264
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "clearProgressUi() "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 267
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    .line 269
    :cond_0
    return-void
.end method

.method private static iZ()V
    .locals 3

    .prologue
    const v2, 0x7f08006c

    .line 292
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    .line 294
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 295
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 296
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-static {v2, v0, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    .line 297
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 298
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 299
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->za:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 300
    return-void
.end method

.method private static ja()V
    .locals 3

    .prologue
    .line 304
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    const v1, 0x7f030002

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 305
    const v0, 0x7f090003

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 306
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$2;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$2;-><init>()V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 315
    const v2, 0x7f08006a

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(I)V

    .line 317
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    if-nez v0, :cond_0

    .line 318
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    .line 319
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f080061

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 320
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f08005f

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 321
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 322
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 323
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080058

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$3;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$3;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 335
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f080053

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$4;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$4;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 348
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yY:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 349
    return-void
.end method

.method private static jb()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 353
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 354
    new-instance v0, Landroid/app/ProgressDialog;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    .line 356
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 357
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 358
    const/16 v1, 0xa

    .line 359
    const-string v2, "UMCSelfUpdate"

    .line 358
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    .line 361
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 363
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 364
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    const v3, 0x7f080064

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 365
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "                         "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 364
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 366
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 367
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressPercentFormat(Ljava/text/NumberFormat;)V

    .line 368
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 369
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 370
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 371
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 372
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    if-lez v0, :cond_2

    .line 373
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jc()V

    .line 375
    :cond_2
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    const/16 v1, 0x63

    if-gt v0, v1, :cond_3

    .line 376
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    const/4 v1, -0x1

    .line 377
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    const v3, 0x7f080057

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$5;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$5;-><init>()V

    .line 376
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 392
    :cond_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 394
    return-void
.end method

.method private static jc()V
    .locals 2

    .prologue
    .line 398
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 402
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$6;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$6;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private static jd()V
    .locals 4

    .prologue
    .line 490
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 491
    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.ui.MdmAgentInstallActivity.intent.UNHIDE_PROGRESS_BAR"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 492
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 493
    const-string v1, "showWindow"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 494
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const/16 v2, 0x4d2

    .line 495
    const/high16 v3, 0x8000000

    .line 494
    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 496
    new-instance v1, Landroid/support/v4/app/v;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/v;-><init>(Landroid/content/Context;)V

    .line 497
    const v2, 0x7f020049

    invoke-virtual {v1, v2}, Landroid/support/v4/app/v;->g(I)Landroid/support/v4/app/v;

    move-result-object v1

    .line 498
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const v3, 0x7f080061

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/v;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/v;

    move-result-object v0

    .line 499
    invoke-virtual {v0}, Landroid/support/v4/app/v;->build()Landroid/app/Notification;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    .line 500
    return-void
.end method

.method private static je()V
    .locals 10

    .prologue
    const/high16 v8, 0x44800000    # 1024.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 504
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    .line 505
    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 504
    check-cast v0, Landroid/app/NotificationManager;

    .line 506
    const-string v3, " "

    .line 508
    const-string v4, " "

    .line 510
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    if-nez v5, :cond_0

    .line 511
    new-instance v5, Landroid/widget/RemoteViews;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 512
    const v7, 0x7f03000f

    .line 511
    invoke-direct {v5, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    .line 514
    :cond_0
    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    const/16 v6, 0x63

    if-gt v5, v6, :cond_1

    .line 515
    new-instance v3, Ljava/lang/StringBuilder;

    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 516
    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zd:I

    int-to-float v5, v5

    div-float/2addr v5, v8

    .line 517
    sget v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ze:I

    int-to-float v6, v6

    div-float/2addr v6, v8

    .line 518
    const-string v7, "%.1f"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v8, v1

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 519
    const-string v7, "%.1f"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v2, v1

    invoke-static {v7, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 520
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "MB"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "MB"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 527
    :goto_0
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    const v6, 0x7f09002f

    .line 528
    sget-object v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const v8, 0x7f0800b6

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 527
    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 529
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    const v6, 0x7f09002b

    invoke-virtual {v5, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 530
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    const v5, 0x7f090031

    invoke-virtual {v3, v5, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 531
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    const v4, 0x7f090032

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 533
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    const v3, 0x7f09002c

    const/16 v4, 0x64

    sget v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 534
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zc:Landroid/widget/RemoteViews;

    iput-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 535
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 536
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zg:I

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 538
    return-void

    .line 524
    :cond_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    const v5, 0x7f080065

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move v9, v2

    move-object v2, v1

    move v1, v9

    goto :goto_0
.end method

.method static synthetic jf()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yW:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;

    return-object v0
.end method

.method static synthetic jg()V
    .locals 0

    .prologue
    .line 489
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jd()V

    return-void
.end method

.method static synthetic jh()V
    .locals 0

    .prologue
    .line 502
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->je()V

    return-void
.end method

.method static synthetic ji()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic jj()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zf:I

    return v0
.end method

.method static synthetic jk()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yZ:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "onBackPressed() ignored"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/16 v2, 0x400

    const/4 v3, -0x1

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->setRequestedOrientation(I)V

    .line 84
    :cond_0
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    .line 85
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->aC:Landroid/app/Notification;

    .line 86
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->mAppContext:Landroid/content/Context;

    .line 87
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_2

    .line 89
    const-string v1, "ignore"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->finish()V

    .line 92
    :cond_1
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    if-ne v1, v3, :cond_2

    .line 93
    const-string v1, "showWindow"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    .line 96
    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 129
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 132
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yV:Landroid/app/Activity;

    .line 135
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 108
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iW()V

    .line 110
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iX()V

    .line 111
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iY()V

    .line 112
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 101
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "msg"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->bg(I)V

    .line 103
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 118
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 119
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 120
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->zb:Landroid/os/PowerManager$WakeLock;

    .line 122
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ry:Ljava/lang/String;

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method
