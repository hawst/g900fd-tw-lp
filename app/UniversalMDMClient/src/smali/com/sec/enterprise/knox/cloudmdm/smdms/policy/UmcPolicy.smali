.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;
.super Ljava/lang/Object;
.source "UmcPolicy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:UmcPolicy"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mMDMUrl:Ljava/lang/String;

.field private mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

.field private mPushManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->r(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mPushManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->n(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    .line 98
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    .line 99
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 100
    return-void
.end method

.method private checkOperationAllowed(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->o(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Operation not allowed"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 76
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createJson(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 84
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 86
    :try_start_0
    const-string v0, "message"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    const-string v0, "sourceId"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return-object v1

    .line 88
    :catch_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public applyPolicyScipt(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;)V
    .locals 2

    .prologue
    .line 187
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getPolicyScriptHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;->applyPolicyScipt(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;)Z

    .line 191
    :cond_0
    return-void
.end method

.method public applyProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;)V
    .locals 2

    .prologue
    .line 155
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getProfileHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->applyProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;)Z

    .line 159
    :cond_0
    return-void
.end method

.method public flushPendingPolicies()V
    .locals 6

    .prologue
    .line 222
    const-string v0, "UMC:UmcPolicy"

    const-string v1, " - inside flushPendingPolicies()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setFlushFlag(Z)V

    .line 226
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAllManagedUsers()Ljava/util/List;

    move-result-object v0

    .line 227
    if-nez v0, :cond_1

    .line 237
    :cond_0
    return-void

    .line 231
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 232
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v0

    .line 233
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 234
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->ra:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-direct {v3, v4, v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V

    .line 233
    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V

    goto :goto_0
.end method

.method public getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getProfileHandler(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    move-result-object v0

    .line 171
    if-nez v0, :cond_0

    .line 172
    const/4 v0, 0x0

    .line 174
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getActiveProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    move-result-object v0

    goto :goto_0
.end method

.method public registerForTimaViolationEvent()Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    return v0
.end method

.method public unenroll(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->checkOperationAllowed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_0

    .line 111
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mPushManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 112
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rb:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-direct {v3, v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V

    .line 111
    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V

    .line 113
    :cond_0
    return-void
.end method

.method public unregisterForTimaViolationEvent()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method public updateClientCertificate(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->checkOperationAllowed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_0

    .line 135
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mPushManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 136
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rd:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-direct {v3, v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V

    .line 135
    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V

    .line 137
    :cond_0
    return-void
.end method

.method public updateServerCertificate(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mMDMUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->checkOperationAllowed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_0

    .line 123
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;->mPushManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 124
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rc:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-direct {v3, v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V

    .line 123
    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V

    .line 125
    :cond_0
    return-void
.end method
