.class public abstract Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;
.super Ljava/lang/Object;
.source "AbstractManager.java"


# instance fields
.field protected observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;",
            ">;"
        }
    .end annotation
.end field

.field protected vs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->vs:Ljava/util/List;

    .line 11
    return-void
.end method


# virtual methods
.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    :cond_0
    return-void
.end method

.method public b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    return-void

    .line 62
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;

    .line 63
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;->wR:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;

    if-ne p2, v2, :cond_1

    .line 64
    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    goto :goto_0

    .line 66
    :cond_1
    invoke-interface {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V

    goto :goto_0
.end method

.method public b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 35
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Enum;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    return-void

    .line 53
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;

    .line 54
    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;->a(Ljava/lang/Enum;)Ljava/lang/Enum;

    goto :goto_0
.end method
