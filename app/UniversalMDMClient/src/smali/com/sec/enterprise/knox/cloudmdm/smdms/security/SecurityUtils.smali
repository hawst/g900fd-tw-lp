.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;
.super Ljava/lang/Object;
.source "SecurityUtils.java"


# static fields
.field private static mContext:Landroid/content/Context;

.field private static vq:Ljava/security/KeyStore;

.field private static vr:Ljava/security/KeyStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 89
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    .line 90
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    .line 130
    new-instance v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v0}, Lorg/spongycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v0}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 138
    const-string v0, "OpensslReg"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    .line 150
    const-string v0, "PRVT_KEYSTORE"

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 151
    const-string v1, "test_verify"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 154
    const-string v2, "keystore.p12"

    const/4 v3, 0x1

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;[CZ)Ljava/security/KeyStore;

    move-result-object v2

    sput-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    .line 160
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    .line 159
    invoke-static {v2}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v2

    .line 161
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    invoke-virtual {v2, v3, v0}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_2

    .line 171
    :goto_0
    const-string v0, "truststore.bks"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;[CZ)Ljava/security/KeyStore;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    .line 177
    :try_start_1
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 178
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    invoke-virtual {v0, v1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_4

    .line 184
    :goto_1
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :catch_1
    move-exception v0

    .line 165
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/UnrecoverableKeyException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :catch_2
    move-exception v0

    .line 167
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :catch_3
    move-exception v0

    .line 180
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 181
    :catch_4
    move-exception v0

    .line 182
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static a(Ljava/security/KeyPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 400
    const-string v0, "CSR inputs "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Values "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    .line 408
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CN="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", OU="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", O="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", C="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 416
    :goto_0
    const-string v0, "CSR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "principal: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    new-instance v0, Lorg/spongycastle/asn1/q/v;

    new-instance v1, Lorg/spongycastle/asn1/q/u;

    const/4 v2, 0x1

    .line 420
    invoke-direct {v1, v2, p1}, Lorg/spongycastle/asn1/q/u;-><init>(ILjava/lang/String;)V

    .line 419
    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/q/v;-><init>(Lorg/spongycastle/asn1/q/u;)V

    .line 423
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 424
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 425
    sget-object v4, Lorg/spongycastle/asn1/q/av;->Tq:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 426
    new-instance v4, Lorg/spongycastle/asn1/q/au;

    const/4 v5, 0x0

    new-instance v6, Lorg/spongycastle/asn1/bd;

    invoke-direct {v6, v0}, Lorg/spongycastle/asn1/bd;-><init>(Lorg/spongycastle/asn1/d;)V

    invoke-direct {v4, v5, v6}, Lorg/spongycastle/asn1/q/au;-><init>(ZLorg/spongycastle/asn1/m;)V

    invoke-virtual {v2, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 427
    new-instance v0, Lorg/spongycastle/asn1/q/av;

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/q/av;-><init>(Ljava/util/Vector;Ljava/util/Vector;)V

    .line 428
    new-instance v5, Lorg/spongycastle/asn1/l/a;

    sget-object v1, Lorg/spongycastle/asn1/l/q;->LM:Lorg/spongycastle/asn1/l;

    .line 429
    new-instance v2, Lorg/spongycastle/asn1/bj;

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/bj;-><init>(Lorg/spongycastle/asn1/d;)V

    .line 428
    invoke-direct {v5, v1, v2}, Lorg/spongycastle/asn1/l/a;-><init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/t;)V

    .line 431
    const/4 v6, 0x0

    .line 433
    :try_start_0
    new-instance v0, Lorg/spongycastle/jce/d;

    const-string v1, "SHA256withRSA"

    new-instance v2, Ljavax/security/auth/x500/X500Principal;

    invoke-direct {v2, v3}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    .line 434
    invoke-virtual {p0}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v3

    new-instance v4, Lorg/spongycastle/asn1/bj;

    invoke-direct {v4, v5}, Lorg/spongycastle/asn1/bj;-><init>(Lorg/spongycastle/asn1/d;)V

    invoke-virtual {p0}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v5

    .line 433
    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/jce/d;-><init>(Ljava/lang/String;Ljavax/security/auth/x500/X500Principal;Ljava/security/PublicKey;Lorg/spongycastle/asn1/t;Ljava/security/PrivateKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_3

    .line 449
    :goto_1
    new-instance v1, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->convertToPem(Ljava/lang/Object;)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1

    .line 409
    :cond_0
    if-nez p3, :cond_1

    if-eqz p4, :cond_1

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CN="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", OU="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", O="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", C="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 411
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 410
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 411
    goto/16 :goto_0

    .line 413
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CN="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", OU="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", O="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", C="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 414
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 413
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_0

    .line 435
    :catch_0
    move-exception v0

    .line 437
    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->printStackTrace()V

    move-object v0, v6

    goto :goto_1

    .line 438
    :catch_1
    move-exception v0

    .line 440
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    move-object v0, v6

    goto :goto_1

    .line 441
    :catch_2
    move-exception v0

    .line 443
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    move-object v0, v6

    goto :goto_1

    .line 444
    :catch_3
    move-exception v0

    .line 446
    invoke-virtual {v0}, Ljava/security/SignatureException;->printStackTrace()V

    move-object v0, v6

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;[CZ)Ljava/security/KeyStore;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 235
    .line 239
    if-eqz p3, :cond_1

    .line 240
    :try_start_0
    const-string v0, "PKCS12"

    const-string v2, "SC"

    invoke-static {v0, v2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    .line 245
    :goto_0
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 246
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 256
    :cond_0
    :goto_1
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loaded server certificates: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStore;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/security/NoSuchProviderException; {:try_start_1 .. :try_end_1} :catch_5

    .line 271
    :goto_2
    return-object v0

    .line 242
    :cond_1
    :try_start_2
    const-string v0, "BKS"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;
    :try_end_2
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/NoSuchProviderException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v0

    goto :goto_0

    .line 249
    :cond_2
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 250
    invoke-virtual {v0, v1, p2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 251
    if-eqz v1, :cond_0

    .line 252
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/security/KeyStoreException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/security/NoSuchProviderException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_1

    .line 260
    :catch_0
    move-exception v1

    .line 261
    :goto_3
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 262
    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 263
    :goto_4
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 264
    :catch_2
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 265
    :goto_5
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 266
    :catch_3
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 267
    :goto_6
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 268
    :catch_4
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 269
    :goto_7
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/security/NoSuchProviderException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 268
    :catch_5
    move-exception v1

    goto :goto_7

    .line 266
    :catch_6
    move-exception v1

    goto :goto_6

    .line 264
    :catch_7
    move-exception v1

    goto :goto_5

    .line 262
    :catch_8
    move-exception v1

    goto :goto_4

    .line 260
    :catch_9
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto/16 :goto_3
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 835
    const-string v0, "TZ_CCM_DATA"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 836
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 838
    :goto_0
    if-eqz v0, :cond_0

    .line 839
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 840
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 842
    :cond_0
    return-void

    .line 836
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyStore;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 618
    .line 620
    if-eqz p1, :cond_5

    .line 621
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 626
    :goto_0
    :try_start_0
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 627
    :try_start_1
    invoke-virtual {p3, v3, v0}, Ljava/security/KeyStore;->store(Ljava/io/OutputStream;[C)V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_1b
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1a
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_19
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_18
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 637
    if-eqz v3, :cond_0

    .line 639
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9

    .line 647
    :cond_0
    :goto_1
    const-string v2, "UMC:SecurityUtils"

    const-string v3, "Store written!"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :try_start_3
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 655
    if-eqz p4, :cond_3

    .line 656
    const-string v2, "PKCS12"

    const-string v3, "SC"

    invoke-static {v2, v3}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v2

    .line 661
    :goto_2
    invoke-virtual {v2, v1, v0}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 662
    const-string v0, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loaded server certificates: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/security/KeyStore;->size()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/security/KeyStoreException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_12
    .catch Ljava/security/NoSuchProviderException; {:try_start_3 .. :try_end_3} :catch_14
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_17

    .line 685
    :cond_1
    :goto_3
    return-void

    .line 628
    :catch_0
    move-exception v2

    move-object v3, v1

    .line 629
    :goto_4
    :try_start_5
    const-string v4, "UMC:SecurityUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception occured: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 637
    if-eqz v3, :cond_0

    .line 639
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 640
    :catch_1
    move-exception v2

    .line 642
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 630
    :catch_2
    move-exception v2

    move-object v3, v1

    .line 631
    :goto_5
    :try_start_7
    const-string v4, "UMC:SecurityUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception occured: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 637
    if-eqz v3, :cond_0

    .line 639
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_1

    .line 640
    :catch_3
    move-exception v2

    .line 642
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 632
    :catch_4
    move-exception v2

    move-object v3, v1

    .line 633
    :goto_6
    :try_start_9
    const-string v4, "UMC:SecurityUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception occured: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 637
    if-eqz v3, :cond_0

    .line 639
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_1

    .line 640
    :catch_5
    move-exception v2

    .line 642
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 634
    :catch_6
    move-exception v2

    move-object v3, v1

    .line 635
    :goto_7
    :try_start_b
    const-string v4, "UMC:SecurityUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception occured: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 637
    if-eqz v3, :cond_0

    .line 639
    :try_start_c
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto/16 :goto_1

    .line 640
    :catch_7
    move-exception v2

    .line 642
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 636
    :catchall_0
    move-exception v0

    move-object v3, v1

    .line 637
    :goto_8
    if-eqz v3, :cond_2

    .line 639
    :try_start_d
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    .line 645
    :cond_2
    :goto_9
    throw v0

    .line 640
    :catch_8
    move-exception v1

    .line 642
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 640
    :catch_9
    move-exception v2

    .line 642
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 658
    :cond_3
    :try_start_e
    const-string v2, "BKS"

    invoke-static {v2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_a
    .catch Ljava/security/KeyStoreException; {:try_start_e .. :try_end_e} :catch_c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_e .. :try_end_e} :catch_e
    .catch Ljava/security/cert/CertificateException; {:try_start_e .. :try_end_e} :catch_10
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_12
    .catch Ljava/security/NoSuchProviderException; {:try_start_e .. :try_end_e} :catch_14
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    move-result-object v2

    goto/16 :goto_2

    .line 663
    :catch_a
    move-exception v0

    .line 664
    :try_start_f
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_10
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    goto/16 :goto_3

    .line 679
    :catch_b
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 665
    :catch_c
    move-exception v0

    .line 666
    :try_start_11
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_12
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_d

    goto/16 :goto_3

    .line 679
    :catch_d
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 667
    :catch_e
    move-exception v0

    .line 668
    :try_start_13
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_14
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_f

    goto/16 :goto_3

    .line 679
    :catch_f
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 669
    :catch_10
    move-exception v0

    .line 670
    :try_start_15
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_16
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_11

    goto/16 :goto_3

    .line 679
    :catch_11
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 671
    :catch_12
    move-exception v0

    .line 672
    :try_start_17
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_18
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_13

    goto/16 :goto_3

    .line 679
    :catch_13
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 673
    :catch_14
    move-exception v0

    .line 674
    :try_start_19
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception occured: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    .line 676
    if-eqz v1, :cond_1

    .line 678
    :try_start_1a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_15

    goto/16 :goto_3

    .line 679
    :catch_15
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 675
    :catchall_1
    move-exception v0

    .line 676
    if-eqz v1, :cond_4

    .line 678
    :try_start_1b
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_16

    .line 684
    :cond_4
    :goto_a
    throw v0

    .line 679
    :catch_16
    move-exception v1

    .line 681
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 679
    :catch_17
    move-exception v0

    .line 681
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 636
    :catchall_2
    move-exception v0

    goto/16 :goto_8

    .line 634
    :catch_18
    move-exception v2

    goto/16 :goto_7

    .line 632
    :catch_19
    move-exception v2

    goto/16 :goto_6

    .line 630
    :catch_1a
    move-exception v2

    goto/16 :goto_5

    .line 628
    :catch_1b
    move-exception v2

    goto/16 :goto_4

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static aG(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 191
    if-nez p0, :cond_0

    .line 192
    const/4 v0, 0x0

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->registerEngine(Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static convertToPem(Ljava/lang/Object;)[B
    .locals 3

    .prologue
    .line 288
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 289
    new-instance v1, Ljava/io/OutputStreamWriter;

    const-string v2, "US-ASCII"

    invoke-direct {v1, v0, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 290
    new-instance v2, Lorg/spongycastle/openssl/r;

    invoke-direct {v2, v1}, Lorg/spongycastle/openssl/r;-><init>(Ljava/io/Writer;)V

    .line 291
    invoke-virtual {v2, p0}, Lorg/spongycastle/openssl/r;->writeObject(Ljava/lang/Object;)V

    .line 292
    invoke-virtual {v2}, Lorg/spongycastle/openssl/r;->close()V

    .line 293
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private static native deregisterEngine()V
.end method

.method public static e([B)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 306
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 307
    new-instance v1, Ljava/io/InputStreamReader;

    const-string v2, "US-ASCII"

    invoke-direct {v1, v0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 309
    new-instance v0, Lorg/spongycastle/openssl/b;

    invoke-direct {v0, v1}, Lorg/spongycastle/openssl/b;-><init>(Ljava/io/Reader;)V

    .line 310
    invoke-virtual {v0}, Lorg/spongycastle/openssl/b;->readObject()Ljava/lang/Object;

    move-result-object v1

    .line 311
    invoke-virtual {v0}, Lorg/spongycastle/openssl/b;->close()V

    .line 312
    return-object v1
.end method

.method private g(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p1, p2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private static h(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 845
    const-string v0, "TZ_CCM_DATA"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 846
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 848
    :goto_0
    if-eqz v0, :cond_0

    .line 849
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 850
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 852
    :cond_0
    return-void

    .line 846
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method

.method public static hL()Ljavax/net/ssl/SSLContext;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 771
    const/4 v0, 0x1

    new-array v2, v0, [Ljavax/net/ssl/KeyManager;

    const/4 v0, 0x0

    .line 772
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/b;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/b;-><init>()V

    aput-object v3, v2, v0

    .line 777
    :try_start_0
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 779
    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v2, v1, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 782
    const-string v1, "UMC:SecurityUtils"

    const-string v2, "SSL Context initialization done!"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_2

    .line 791
    :goto_0
    return-object v0

    .line 783
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 785
    :goto_1
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 786
    :catch_1
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 788
    :goto_2
    invoke-virtual {v1}, Ljava/security/KeyManagementException;->printStackTrace()V

    goto :goto_0

    .line 786
    :catch_2
    move-exception v1

    goto :goto_2

    .line 783
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public static hM()V
    .locals 0

    .prologue
    .line 199
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->deregisterEngine()V

    .line 200
    return-void
.end method

.method public static hN()[Ljava/security/cert/X509Certificate;
    .locals 7

    .prologue
    .line 212
    const-string v0, "X.509"

    invoke-static {v0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 213
    new-instance v1, Ljava/io/ByteArrayInputStream;

    .line 214
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;-><init>()V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->x509Cert()[B

    move-result-object v2

    .line 213
    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 216
    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificates(Ljava/io/InputStream;)Ljava/util/Collection;

    move-result-object v2

    .line 218
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v3, v0, [Ljava/security/cert/X509Certificate;

    .line 220
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 221
    const/4 v0, 0x0

    move v1, v0

    .line 222
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    const-string v0, "UMC:SecurityUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "X509 Chain length: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    return-object v3

    .line 223
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    aput-object v0, v3, v1

    .line 224
    const-string v0, "UMC:SecurityUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cert "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "received!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static hO()Ljava/security/KeyPair;
    .locals 2

    .prologue
    .line 352
    const-string v0, "RSA"

    invoke-static {v0}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v0

    .line 353
    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Ljava/security/KeyPairGenerator;->initialize(I)V

    .line 354
    invoke-virtual {v0}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v0

    .line 356
    return-object v0
.end method

.method public static hP()Z
    .locals 5

    .prologue
    .line 824
    const/4 v0, 0x1

    .line 825
    const-string v1, "ro.config.timaversion"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/i;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 826
    const-string v2, "UMC:SecurityUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "timaVersion on the device: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 828
    const-string v2, "3.0"

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->z(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 829
    :cond_0
    const/4 v0, 0x0

    .line 831
    :cond_1
    return v0
.end method

.method private hexStringToByteArray(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 760
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/codec/a/a;->a([C)[B

    move-result-object v0

    return-object v0
.end method

.method private static i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 855
    const-string v0, "TZ_CCM_DATA"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 856
    if-eqz v0, :cond_2

    .line 857
    const-string v2, ""

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 858
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 864
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private static native registerEngine(Ljava/lang/String;)V
.end method

.method private static native x509Cert()[B
.end method


# virtual methods
.method public aE(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 585
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CERTIFICATE_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 587
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    invoke-virtual {v2, v1}, Ljava/security/KeyStore;->containsAlias(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 596
    :goto_0
    return v0

    .line 591
    :cond_0
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    invoke-virtual {v2, v1}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 594
    const-string v1, "test_verify"

    const-string v2, "truststore.bks"

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyStore;Z)V

    .line 596
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public aF(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 602
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    invoke-virtual {v1, p1}, Ljava/security/KeyStore;->containsAlias(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 603
    const/4 v0, 0x0

    .line 612
    :goto_0
    return v0

    .line 605
    :cond_0
    const-string v1, "PRVT_KEYSTORE"

    const/4 v2, 0x6

    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 607
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    invoke-virtual {v2, p1}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 610
    const-string v2, "keystore.p12"

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyStore;Z)V

    goto :goto_0
.end method

.method public aH(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    .locals 3

    .prologue
    .line 708
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CERTIFICATE_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public aI(Ljava/lang/String;)Ljava/security/PublicKey;
    .locals 1

    .prologue
    .line 714
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aH(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    .line 716
    return-object v0
.end method

.method public aJ(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 724
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/a;->aD(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public aK(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 811
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->h(Landroid/content/Context;Ljava/lang/String;)V

    .line 812
    return-void
.end method

.method public aL(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 819
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/security/KeyPair;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 465
    new-array v1, v5, [Ljava/security/cert/X509Certificate;

    .line 467
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->e([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    aput-object v0, v1, v2

    .line 468
    const-string v0, "UMC:SecurityUtils"

    const-string v2, "Certificate entry for storage received!"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 475
    :goto_0
    const-string v0, "PRVT_KEYSTORE"

    const/4 v2, 0x6

    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 477
    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 485
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    invoke-virtual {p2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v4

    invoke-virtual {v3, p1, v4, v2, v1}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    .line 488
    const-string v1, "keystore.p12"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    invoke-direct {p0, v0, v1, v2, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyStore;Z)V

    .line 489
    return-void

    .line 469
    :catch_0
    move-exception v0

    .line 471
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 480
    :cond_0
    const-string v0, "UMC:SecurityUtils"

    const-string v1, "Password for store null!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 731
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    .line 732
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 734
    const-string v2, "SHA256withRSAandMGF1"

    const-string v3, "SC"

    invoke-static {v2, v3}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v2

    .line 735
    const-string v3, "UMC:SecurityUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "using provider : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/security/Signature;->getProvider()Ljava/security/Provider;

    move-result-object v5

    invoke-virtual {v5}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    invoke-virtual {p0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aI(Ljava/lang/String;)Ljava/security/PublicKey;

    move-result-object v3

    .line 739
    invoke-virtual {v2, v3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 740
    invoke-virtual {v2, v0}, Ljava/security/Signature;->update([B)V

    .line 742
    invoke-virtual {v2, v1}, Ljava/security/Signature;->verify([B)Z
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/commons/codec/DecoderException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v0

    .line 756
    :goto_0
    return v0

    .line 743
    :catch_0
    move-exception v0

    .line 744
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 745
    :catch_1
    move-exception v0

    .line 746
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/commons/codec/DecoderException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 747
    :catch_2
    move-exception v0

    .line 748
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/SignatureException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 749
    :catch_3
    move-exception v0

    .line 750
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 751
    :catch_4
    move-exception v0

    .line 752
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 753
    :catch_5
    move-exception v0

    .line 754
    const-string v1, "UMC:SecurityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public hI()Ljava/security/KeyStore;
    .locals 1

    .prologue
    .line 519
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vq:Ljava/security/KeyStore;

    return-object v0
.end method

.method public hJ()Ljava/lang/String;
    .locals 2

    .prologue
    .line 492
    const-string v0, "PRVT_KEYSTORE"

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 800
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 802
    if-nez v0, :cond_0

    .line 803
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aU(I)Ljava/lang/String;

    move-result-object v0

    .line 804
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->mContext:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 807
    :cond_0
    return-object v0
.end method

.method public l(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "CERTIFICATE_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 542
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aJ(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/a;->aC(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 547
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->checkValidity()V
    :try_end_0
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_0 .. :try_end_0} :catch_1

    .line 554
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/Certificate;

    invoke-virtual {v4, v2, v0}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V

    .line 556
    const-string v0, "test_verify"

    const-string v2, "truststore.bks"

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->vr:Ljava/security/KeyStore;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyStore;Z)V

    move v0, v1

    .line 561
    :goto_0
    return v0

    .line 548
    :catch_0
    move-exception v0

    .line 549
    const/4 v0, -0x2

    goto :goto_0

    .line 550
    :catch_1
    move-exception v0

    .line 551
    const/4 v0, -0x1

    goto :goto_0

    .line 561
    :cond_0
    const/4 v0, -0x3

    goto :goto_0
.end method
