.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;
.super Ljava/lang/Object;
.source "PushManager.java"


# static fields
.field private static gContext:Landroid/content/Context;

.field private static qR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

.field private static final qS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;",
            ">;"
        }
    .end annotation
.end field

.field private static qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;


# instance fields
.field private final qU:Landroid/os/Handler;

.field private final qV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qS:Ljava/util/Map;

    .line 66
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qU:Landroid/os/Handler;

    .line 99
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a$2;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gb()V

    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qU:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V
    .locals 0

    .prologue
    .line 66
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    return-void
.end method

.method private gb()V
    .locals 3

    .prologue
    .line 114
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->s(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    move-result-object v0

    .line 116
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    if-nez v1, :cond_0

    .line 117
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    .line 123
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    if-eqz v0, :cond_1

    .line 124
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qS:Ljava/util/Map;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->gg()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    .line 125
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

    .line 124
    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;)V

    .line 129
    :goto_0
    return-void

    .line 119
    :cond_0
    const-string v0, "UMC:PushManager"

    const-string v1, "Push Message Processing already in progress"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_1
    const-string v0, "UMC:PushManager"

    const-string v1, "No Push Messages Left"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic gd()Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic ge()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    return-object v0
.end method

.method public static final declared-synchronized r(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;
    .locals 5

    .prologue
    .line 69
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    if-nez v0, :cond_0

    .line 70
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    .line 71
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qS:Ljava/util/Map;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->ra:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    .line 72
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/f;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/f;-><init>(Landroid/content/Context;)V

    .line 71
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qS:Ljava/util/Map;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rc:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    .line 74
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/g;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/g;-><init>(Landroid/content/Context;)V

    .line 73
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qS:Ljava/util/Map;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rb:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qS:Ljava/util/Map;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rd:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    .line 77
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/e;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/e;-><init>(Landroid/content/Context;)V

    .line 76
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    .line 80
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V
    .locals 2

    .prologue
    .line 154
    if-eqz p1, :cond_0

    .line 155
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->b(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)I

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qU:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 158
    return-void
.end method

.method public d(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 133
    :try_start_0
    const-string v0, "message"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    move-result-object v1

    .line 135
    const-string v0, "sourceId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 136
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->o(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_1

    .line 138
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->qU:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 151
    :cond_1
    :goto_1
    return-void

    .line 138
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 139
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 141
    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->gContext:Landroid/content/Context;

    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-direct {v5, v2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;)V

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->b(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
