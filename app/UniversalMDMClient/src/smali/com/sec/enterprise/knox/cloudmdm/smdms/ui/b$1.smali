.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;
.super Ljava/lang/Object;
.source "EulaPromptFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation


# instance fields
.field final synthetic yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 207
    .line 208
    instance-of v0, p1, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 209
    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 214
    :goto_0
    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V

    .line 216
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 221
    :goto_1
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 212
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V

    .line 219
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;->yz:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_1
.end method
