.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;
.super Ljava/lang/Object;
.source "AutoDiscoverHandler.java"


# static fields
.field private static final TIMEOUT:I

.field private static final uF:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;",
            ">;"
        }
    .end annotation
.end field

.field private static final uK:I

.field private static uM:Lorg/apache/http/conn/ClientConnectionManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mProtocolVersion:Ljava/lang/String;

.field private mUserAgent:Ljava/lang/String;

.field private tY:Ljava/lang/String;

.field private uE:I

.field private final uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

.field private uH:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

.field private uI:Z

.field private uJ:Z

.field private final uL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;",
            ">;"
        }
    .end annotation
.end field

.field private ua:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 103
    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    .line 102
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 104
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 105
    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    .line 104
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 102
    :goto_0
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->TIMEOUT:I

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    .line 131
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 132
    const-string v1, "CscFeature_Email_EasSyncServiceConnectionTimeoutValue"

    .line 131
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    .line 133
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 134
    const-string v1, "CscFeature_Email_EasSyncServiceConnectionTimeoutValue"

    .line 133
    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 131
    :goto_1
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uK:I

    .line 1762
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;

    return-void

    .line 105
    :cond_0
    const/16 v0, 0x50

    goto :goto_0

    .line 135
    :cond_1
    const/16 v0, 0x28

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uE:I

    .line 113
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    .line 118
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->tY:Ljava/lang/String;

    .line 121
    const-string v0, "2.5"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mProtocolVersion:Ljava/lang/String;

    .line 125
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uI:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uJ:Z

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    .line 667
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mContext:Landroid/content/Context;

    .line 668
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 835
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uH:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    .line 838
    const/4 v0, 0x0

    move-object v3, v0

    move v4, v2

    move v0, v1

    .line 841
    :cond_0
    :goto_0
    iget v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uE:I

    const/16 v6, 0xa

    if-ge v5, v6, :cond_1

    if-nez v4, :cond_2

    .line 874
    :cond_1
    const-string v0, "AutoDiscoverHandler"

    const-string v1, "AutoDiscovery finished"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0

    .line 842
    :cond_2
    const-string v5, "AutoDiscoverHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " Starting threads for autodiscover for user  "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 843
    const-string v7, " redirect count "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uE:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 842
    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uH:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-direct {p0, v5, v6, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Ljava/lang/String;)V

    .line 845
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 846
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v4

    .line 847
    const-string v5, "autodiscover_error_code"

    .line 848
    const/4 v6, -0x1

    .line 847
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v4, v1

    .line 850
    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 851
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V

    .line 852
    iget v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uE:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uE:I

    move v4, v2

    .line 855
    goto :goto_0

    :cond_4
    if-nez v0, :cond_6

    .line 857
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uH:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 858
    if-lez v5, :cond_0

    .line 859
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uH:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v3, v5, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 860
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->aw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Ljava/lang/String;)V

    .line 861
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Ljava/lang/String;

    move-result-object v0

    .line 862
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V

    .line 864
    if-eqz v0, :cond_5

    move-object v3, v0

    move v4, v2

    move v0, v2

    .line 865
    goto/16 :goto_0

    :cond_5
    move-object v3, v0

    move v4, v1

    move v0, v2

    .line 870
    goto/16 :goto_0

    :cond_6
    move v4, v1

    .line 871
    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 771
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 772
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    move-result-object v1

    .line 773
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "trying autodisocover waiting for lock for user "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    monitor-enter v1

    .line 777
    :try_start_0
    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hx()V

    .line 778
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "autodiscover lock acquired for user "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v0

    .line 785
    invoke-static {v0, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Landroid/os/Bundle;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798
    :try_start_1
    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hx()V

    .line 799
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->av(Ljava/lang/String;)V

    .line 800
    const-string v2, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "autodiscover lock cleared for user "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 804
    const-string v2, "autodiscover_host_auth"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 805
    const-string v2, "AutoDiscoverHandler"

    .line 806
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "returning autodiscover result [ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 807
    const-string v4, "autodiscover_host_auth"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 808
    const-string v4, " ] for user "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " result "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 806
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 805
    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :goto_0
    iget-boolean v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uJ:Z

    if-eqz v1, :cond_0

    .line 814
    const-string v1, "autodiscover_error_code"

    .line 815
    const/16 v2, 0x74

    .line 814
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 819
    :cond_0
    return-object v0

    .line 787
    :catchall_0
    move-exception v0

    .line 798
    :try_start_2
    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hx()V

    .line 799
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->av(Ljava/lang/String;)V

    .line 800
    const-string v2, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "autodiscover lock cleared for user "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    throw v0

    .line 774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 810
    :cond_1
    const-string v2, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " autodiscover failed for user "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;
    .locals 3

    .prologue
    .line 699
    .line 700
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    monitor-enter v1

    .line 701
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    .line 702
    if-nez v0, :cond_0

    .line 703
    const-string v0, "AutoDiscoverHandler"

    const-string v2, "creating autodiscover instance lock"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;-><init>(Landroid/content/Context;)V

    .line 705
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    :cond_0
    monitor-exit v1

    .line 708
    return-object v0

    .line 700
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1069
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x3a

    .line 672
    .line 674
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 676
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 684
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Basic "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 685
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 684
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 687
    return-object v0

    .line 680
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized a(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 9

    .prologue
    const/16 v1, 0x50

    const/4 v2, 0x0

    const/16 v0, 0x1bb

    .line 1766
    const-class v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;

    monitor-enter v3

    :try_start_0
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getClientConnectionManager ssl = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", port = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    if-eqz p0, :cond_0

    const/high16 v2, 0x10000

    :cond_0
    add-int/2addr v2, p1

    .line 1769
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;

    if-nez v2, :cond_2

    .line 1773
    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 1774
    new-instance v4, Lcom/android/emailcommon/utility/a;

    const/4 v5, 0x0

    invoke-static {v5}, Lcom/android/emailcommon/utility/b;->g(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/emailcommon/utility/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 1776
    new-instance v5, Lcom/android/emailcommon/utility/a;

    const/4 v6, 0x1

    invoke-static {v6}, Lcom/android/emailcommon/utility/b;->g(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/emailcommon/utility/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 1777
    sget-object v6, Lcom/android/emailcommon/utility/a;->dp:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v5, v6}, Lcom/android/emailcommon/utility/a;->a(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 1779
    if-lez p1, :cond_1

    const v6, 0xffff

    if-le p1, v6, :cond_4

    .line 1780
    :cond_1
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v1, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    const/16 v7, 0x50

    invoke-direct {v0, v1, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 1782
    if-eqz p0, :cond_3

    .line 1783
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v1, "https"

    const/16 v5, 0x1bb

    invoke-direct {v0, v1, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 1802
    :goto_0
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 1803
    const-string v1, "http.conn-manager.max-total"

    const/16 v4, 0x19

    invoke-interface {v0, v1, v4}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 1806
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v1, v0, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;

    .line 1809
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v0

    .line 1785
    :cond_3
    :try_start_1
    new-instance v0, Lorg/apache/http/conn/scheme/Scheme;

    const-string v1, "https"

    const/16 v4, 0x1bb

    invoke-direct {v0, v1, v5, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1766
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1789
    :cond_4
    :try_start_2
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    .line 1790
    if-eqz p0, :cond_5

    :goto_1
    invoke-direct {v6, v7, v8, v1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    .line 1789
    invoke-virtual {v2, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 1792
    if-eqz p0, :cond_7

    .line 1793
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    if-eqz p0, :cond_6

    :goto_2
    invoke-direct {v1, v5, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    goto :goto_0

    :cond_5
    move v1, p1

    .line 1790
    goto :goto_1

    :cond_6
    move p1, v0

    .line 1793
    goto :goto_2

    .line 1795
    :cond_7
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "https"

    if-eqz p0, :cond_8

    :goto_3
    invoke-direct {v1, v4, v5, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_8
    move p1, v0

    goto :goto_3
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;IZI)Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 1

    .prologue
    .line 1182
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->b(IZI)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 971
    .line 972
    const-string v0, "autodiscover_host_auth"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 973
    const-string v1, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Address : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Z)V
    .locals 0

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uI:Z

    return-void
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 888
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Starting threads for autodiscover for user  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 889
    const-string v2, " , autodiscoverInfo  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 888
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    if-eqz p3, :cond_5

    move-object v2, p3

    .line 903
    :goto_0
    const-string v0, "AutoDiscoverHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, " autodiscover server domain : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    const-string v0, "/autodiscover/autodiscover.xml"

    .line 907
    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->ax(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 915
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v4

    .line 917
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "https://autodiscover."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/autodiscover/autodiscover.xml"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 919
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;

    invoke-direct {v3, p0, v1, p1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 920
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 921
    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->start()V

    .line 923
    if-eqz v0, :cond_0

    .line 924
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "https://autodiscover."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/autodiscover/autodiscover.xml"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 925
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;

    invoke-direct {v3, p0, v1, p1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 926
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->start()V

    .line 930
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/autodiscover/autodiscover.xml"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 931
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;

    invoke-direct {v3, p0, v1, p1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 932
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 933
    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->start()V

    .line 935
    if-eqz v0, :cond_1

    .line 936
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/autodiscover/autodiscover.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 937
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;

    invoke-direct {v1, p0, v0, p1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 938
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 939
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->start()V

    .line 942
    :cond_1
    const/4 v5, 0x1

    .line 943
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;Z)V

    .line 944
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 945
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->start()V

    .line 947
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    monitor-enter v1

    .line 948
    :cond_2
    :goto_1
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uI:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 947
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 960
    const-string v0, "AutoDiscoverHandler"

    const-string v1, "All the autoDiscovery threads finished"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    :cond_4
    :goto_2
    return-void

    .line 896
    :cond_5
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 897
    if-lez v0, :cond_4

    .line 898
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 950
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 951
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uG:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 952
    :try_start_2
    monitor-exit v1

    goto :goto_2

    .line 947
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 954
    :catch_0
    move-exception v0

    .line 955
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private static av(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 723
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    monitor-enter v1

    .line 724
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 725
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uF:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 723
    :cond_0
    monitor-exit v1

    .line 728
    return-void

    .line 723
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private aw(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v1, -0x1

    .line 1019
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "_autodiscover._tcp."

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1020
    const-string v2, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending dnsQuery : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    const/4 v4, 0x0

    .line 1026
    :try_start_0
    new-instance v2, Lorg/xbill/DNS/Lookup;

    const/16 v3, 0x21

    invoke-direct {v2, v0, v3}, Lorg/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Lorg/xbill/DNS/Lookup;->run()[Lorg/xbill/DNS/Record;

    move-result-object v8

    .line 1027
    if-eqz v8, :cond_4

    .line 1028
    array-length v9, v8
    :try_end_0
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_2

    move v6, v7

    move v2, v1

    :goto_0
    if-lt v6, v9, :cond_1

    move-object v0, v4

    .line 1051
    :goto_1
    if-eqz v0, :cond_0

    .line 1052
    const-string v1, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending dnsQuery successfull, host : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1054
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1057
    :cond_0
    return-object v0

    .line 1028
    :cond_1
    :try_start_1
    aget-object v0, v8, v6

    .line 1029
    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    .line 1030
    invoke-virtual {v0}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v3

    .line 1031
    if-eqz v3, :cond_3

    .line 1032
    invoke-virtual {v3}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1033
    invoke-virtual {v0}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v10

    .line 1034
    invoke-virtual {v0}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v3

    .line 1035
    invoke-virtual {v0}, Lorg/xbill/DNS/SRVRecord;->getWeight()I
    :try_end_1
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    .line 1036
    const/16 v11, 0x1bb

    if-ne v10, v11, :cond_3

    if-le v2, v3, :cond_2

    if-gt v1, v0, :cond_3

    :cond_2
    move v1, v3

    move-object v2, v5

    .line 1028
    :goto_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-object v4, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1044
    :catch_0
    move-exception v0

    .line 1045
    invoke-virtual {v0}, Lorg/xbill/DNS/TextParseException;->printStackTrace()V

    move-object v0, v4

    goto :goto_1

    .line 1046
    :catch_1
    move-exception v0

    .line 1047
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v4

    goto :goto_1

    .line 1048
    :catch_2
    move-exception v0

    .line 1049
    invoke-virtual {v0}, Ljava/lang/ExceptionInInitializerError;->printStackTrace()V

    move-object v0, v4

    goto :goto_1

    :cond_3
    move v0, v1

    move v1, v2

    move-object v2, v4

    goto :goto_2

    :cond_4
    move-object v0, v4

    goto :goto_1
.end method

.method private ax(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1161
    const/4 v0, 0x0

    .line 1162
    const-string v1, "\\."

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1163
    if-eqz v1, :cond_0

    array-length v1, v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1171
    :cond_0
    :goto_0
    return-object v0

    .line 1166
    :cond_1
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1167
    if-lez v1, :cond_0

    .line 1168
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1070
    const-string v0, "X-MS-Location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 1071
    const/4 v0, 0x0

    .line 1072
    if-eqz v1, :cond_0

    .line 1073
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1075
    :cond_0
    return-object v0
.end method

.method private b(IZI)Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 6

    .prologue
    .line 1184
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 1186
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uK:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 1188
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 1190
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 1194
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uK:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 1202
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    .line 1203
    const-string v2, "CscFeature_Email_EasDoNotUseProxy"

    .line 1202
    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    .line 1203
    if-nez v1, :cond_0

    .line 1204
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/exchange/a/b;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1206
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/exchange/a/b;->getPort(Landroid/content/Context;)I

    move-result v2

    .line 1208
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    if-ltz v2, :cond_0

    .line 1210
    new-instance v3, Lorg/apache/http/HttpHost;

    invoke-direct {v3, v1, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, v3}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 1212
    const-string v3, "PROXY"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Added proxy param host: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " port: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    :cond_0
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 1220
    invoke-static {p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->a(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    .line 1219
    invoke-direct {v1, v2, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 1227
    return-object v1
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)V
    .locals 0

    .prologue
    .line 990
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hw()V

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uH:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static varargs d([Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1086
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->cj:Z

    if-eqz v1, :cond_0

    .line 1087
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    .line 1090
    array-length v1, p0

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 1091
    aget-object v0, p0, v0

    .line 1100
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "AutoDiscoverHandler<"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    :cond_0
    return-void

    .line 1093
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x40

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1094
    array-length v4, p0

    :goto_1
    if-lt v0, v4, :cond_2

    .line 1097
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1094
    :cond_2
    aget-object v5, p0, v0

    .line 1095
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1094
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mProtocolVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1085
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->d([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1134
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->hr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mUserAgent:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1137
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->tY:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 1139
    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1140
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SAMSUNG-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->tY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->ua:Ljava/lang/String;

    .line 1144
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->ua:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "101"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1145
    sget-object v1, Lcom/android/emailcommon/EasRefs;->cp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1144
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mUserAgent:Ljava/lang/String;

    .line 1147
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mUserAgent:Ljava/lang/String;

    .line 1151
    :goto_1
    return-object v0

    .line 1142
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->tY:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->ua:Ljava/lang/String;

    goto :goto_0

    .line 1150
    :cond_1
    const-string v0, "AutoDiscoverHandler"

    const-string v1, "getUserAgent()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->mUserAgent:Ljava/lang/String;

    goto :goto_1
.end method

.method private hw()V
    .locals 6

    .prologue
    .line 991
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    monitor-enter v1

    .line 993
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 991
    :goto_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1009
    return-void

    .line 993
    :cond_0
    :try_start_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;

    .line 994
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpPost;->isAborted()Z

    move-result v3

    if-nez v3, :cond_1

    .line 995
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 996
    const-string v3, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "httpPost aborted : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    :cond_1
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v3

    if-nez v3, :cond_2

    .line 999
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 1000
    const-string v3, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "httpGet aborted : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_2
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->interrupt()V

    .line 1003
    const-string v3, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Interrupted : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/g;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1006
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 991
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method private hx()V
    .locals 1

    .prologue
    .line 1236
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;

    if-eqz v0, :cond_0

    .line 1237
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1238
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->uM:Lorg/apache/http/conn/ClientConnectionManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1243
    :cond_0
    :goto_0
    return-void

    .line 1240
    :catch_0
    move-exception v0

    .line 1241
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic hy()I
    .locals 1

    .prologue
    .line 102
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->TIMEOUT:I

    return v0
.end method
