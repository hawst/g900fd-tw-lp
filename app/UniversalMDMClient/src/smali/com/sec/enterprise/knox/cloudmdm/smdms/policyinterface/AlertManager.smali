.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;
.super Ljava/lang/Object;
.source "AlertManager.java"


# static fields
.field static final ALERTREPO:Ljava/lang/String; = "AlertRequestRepo.json"

.field private static final TAG:Ljava/lang/String; = "UMC:AlertManager"


# instance fields
.field admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field alertHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final gGson:Lcom/google/gson/e;

.field private mContext:Landroid/content/Context;

.field private policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

.field stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    .line 51
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->gGson:Lcom/google/gson/e;

    .line 57
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->mContext:Landroid/content/Context;

    .line 60
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->loadRepo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    .line 61
    return-void
.end method

.method private getPersistentStateData(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;
    .locals 4

    .prologue
    .line 175
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;-><init>()V

    .line 176
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 181
    return-object v2

    .line 177
    :cond_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    .line 178
    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->notpersistant:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->notpersistant:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    .line 179
    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public addAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V
    .locals 3

    .prologue
    .line 93
    const-string v0, "UMC:AlertManager"

    const-string v1, "@AlertManager - addAlertRequest"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-static {p0, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AlertHandlerFactory;->createInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->saveRepo()V

    .line 98
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->start()V

    .line 99
    return-void
.end method

.method public findAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 64
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiInActiveIntents:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiInActiveIntents:Ljava/util/Map;

    move-object v8, v0

    .line 65
    :goto_0
    if-nez v8, :cond_2

    move-object v0, v6

    .line 89
    :cond_0
    :goto_1
    return-object v0

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiInActiveCallbacks:Ljava/util/Map;

    move-object v8, v0

    goto :goto_0

    :cond_2
    move v2, v3

    .line 68
    :goto_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_3

    move-object v0, v6

    .line 89
    goto :goto_1

    .line 69
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    .line 70
    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v9

    .line 71
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 72
    const/4 v7, 0x1

    move v4, v3

    .line 73
    :goto_3
    array-length v5, v1

    if-lt v4, v5, :cond_4

    move v1, v7

    .line 84
    :goto_4
    if-nez v1, :cond_0

    .line 68
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 74
    :cond_4
    iget-object v5, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveIntents:Ljava/util/Map;

    if-eqz v5, :cond_6

    iget-object v5, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveIntents:Ljava/util/Map;

    .line 75
    :goto_5
    if-nez v5, :cond_5

    .line 76
    iget-object v5, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeIntents:Ljava/util/Map;

    if-eqz v5, :cond_7

    iget-object v5, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeIntents:Ljava/util/Map;

    .line 78
    :cond_5
    :goto_6
    aget-object v10, v1, v2

    invoke-interface {v5, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    move v1, v3

    .line 80
    goto :goto_4

    .line 74
    :cond_6
    iget-object v5, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveCallbacks:Ljava/util/Map;

    goto :goto_5

    .line 76
    :cond_7
    iget-object v5, v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeCallbacks:Ljava/util/Map;

    goto :goto_6

    .line 73
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method public getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    return-object v0
.end method

.method getAlertFolder()Ljava/lang/String;
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->getHostName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/admins/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 142
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 144
    :cond_0
    return-object v0
.end method

.method public handleAlertRequests(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)V
    .locals 4

    .prologue
    .line 194
    const-string v0, "UMC:AlertManager"

    const-string v1, "entering handleAlertRequests()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v2

    .line 196
    iget-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveIntents:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveIntents:Ljava/util/Map;

    .line 197
    :goto_0
    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeIntents:Ljava/util/Map;

    if-eqz v1, :cond_2

    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeIntents:Ljava/util/Map;

    .line 198
    :goto_1
    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiInActiveIntents:Ljava/util/Map;

    if-eqz v3, :cond_3

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiInActiveIntents:Ljava/util/Map;

    .line 199
    :goto_2
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->findAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    move-result-object v3

    .line 200
    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 201
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->user:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, p2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/Boolean;)V

    .line 202
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->addAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    .line 209
    :cond_0
    :goto_3
    return-void

    .line 196
    :cond_1
    iget-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveCallbacks:Ljava/util/Map;

    goto :goto_0

    .line 197
    :cond_2
    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeCallbacks:Ljava/util/Map;

    goto :goto_1

    .line 198
    :cond_3
    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiInActiveCallbacks:Ljava/util/Map;

    goto :goto_2

    .line 203
    :cond_4
    if-nez v3, :cond_5

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 204
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->user:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;->getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->id()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v0, v1, v2, p2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/Boolean;)V

    .line 205
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->addAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    goto :goto_3

    .line 206
    :cond_5
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    if-eqz v3, :cond_0

    .line 207
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->removeAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    goto :goto_3
.end method

.method public hasAnyPendingAlertRequests()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method loadRepo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;
    .locals 6

    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->getAlertFolder()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "AlertRequestRepo.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->readFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->gGson:Lcom/google/gson/e;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$1;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;)V

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    .line 151
    if-nez v0, :cond_1

    .line 152
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;-><init>()V

    move-object v1, v0

    .line 154
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_0

    .line 164
    return-object v1

    .line 155
    :cond_0
    iget-object v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    .line 157
    :try_start_0
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->initSignature()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    const-string v3, "UMC:AlertManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public removeAlertRequest(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V
    .locals 2

    .prologue
    .line 102
    const-string v0, "UMC:AlertManager"

    const-string v1, " removeAlertRequest"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;

    .line 104
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->stop()V

    .line 105
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 107
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->saveRepo()V

    .line 108
    return-void
.end method

.method saveRepo()V
    .locals 3

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->getAlertFolder()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "AlertRequestRepo.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->getPersistentStateData(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->gGson:Lcom/google/gson/e;

    invoke-virtual {v2, v1}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public schedule(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getThreadContext()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 186
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    .line 111
    const-string v0, "UMC:AlertManager"

    const-string v1, "start"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "UMC:AlertManager"

    const-string v1, "No Pending Alert Requests to be applied for all admins"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 127
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertState;->alertRequests:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    .line 119
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-static {p0, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AlertHandlerFactory;->createInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;

    move-result-object v2

    .line 120
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->id:Ljava/lang/String;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    const-string v2, "UMC:AlertManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 130
    const-string v0, "UMC:AlertManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alert Manager stopped for admin with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->alertHandlers:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 135
    return-void

    .line 131
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->stop()V

    goto :goto_0
.end method
