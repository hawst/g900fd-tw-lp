.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;
.super Ljava/lang/Object;
.source "PolicyClassHolder.java"


# static fields
.field protected static TAG:Ljava/lang/String;


# instance fields
.field gPolicyClassTbl:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field globalPolicyClassHolder:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

.field mPendingCreations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field theadmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-string v0, "UMC:PolicyClassHolder"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    .line 1259
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    .line 1352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    .line 96
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->theadmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 97
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 98
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->initPolicyClassTbl()V

    .line 99
    return-void
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1334
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1328
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .locals 1

    .prologue
    .line 945
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKM(Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1322
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private activateSSO(Ljava/lang/String;Landroid/app/enterprise/SSOPolicy;)V
    .locals 4

    .prologue
    .line 1046
    .line 1049
    :try_start_0
    invoke-virtual {p2, p1}, Landroid/app/enterprise/SSOPolicy;->setupSSOInUserSpace(Ljava/lang/String;)I

    move-result v0

    .line 1050
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1051
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "UserSpaceSSO setup failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1053
    :catch_0
    move-exception v0

    .line 1054
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activateSSO SecurityException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1055
    :catch_1
    move-exception v0

    .line 1056
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "activateSSO UnsupportedOperation !!!!!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1057
    :catch_2
    move-exception v0

    .line 1058
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "activateSSO NullPointerException"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1060
    :cond_0
    return-void
.end method

.method private getEKM(Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .locals 5

    .prologue
    .line 946
    .line 947
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mEnterpriseKnoxManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 948
    if-nez v0, :cond_3

    .line 949
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 950
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mEnterpriseKnoxManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    .line 958
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "YES"

    move-object v1, v0

    .line 961
    :goto_1
    monitor-enter v2

    .line 962
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getEKMId value = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 964
    if-nez v0, :cond_0

    .line 966
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "getEKM - calling createInstance"

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    invoke-static {p1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->createInstance(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 980
    :goto_2
    :try_start_2
    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 961
    :cond_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 983
    return-object v0

    .line 958
    :cond_1
    const-string v0, "NO"

    move-object v1, v0

    goto :goto_1

    .line 968
    :catch_0
    move-exception v0

    .line 971
    :try_start_3
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v4, "getEKM - This throwable is handled correctly"

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 973
    if-eqz p2, :cond_2

    .line 974
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v1, "getEKM - requiresCreateInstance but not supported. Throwing exception (will go into cmd resp)"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "This API is not supported for this version of binary ~ EKM"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 961
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 977
    :cond_2
    :try_start_4
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "getEKM - calling getInstance instead of createInstance"

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    invoke-static {p1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v2, v0

    goto :goto_0
.end method

.method private getEKMId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1347
    if-nez p1, :cond_0

    .line 1348
    const/4 v0, 0x0

    .line 1349
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1323
    if-nez p1, :cond_0

    .line 1324
    const/4 v0, 0x0

    .line 1325
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1335
    if-nez p1, :cond_0

    .line 1336
    const/4 v0, 0x0

    .line 1337
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1341
    if-nez p1, :cond_0

    .line 1342
    const/4 v0, 0x0

    .line 1343
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1329
    if-nez p1, :cond_0

    .line 1330
    const/4 v0, 0x0

    .line 1331
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getUMCPolicy(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 231
    .line 232
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mUMCPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 233
    if-nez v0, :cond_1

    .line 234
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 235
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mUMCPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 238
    :goto_0
    monitor-enter v1

    .line 239
    :try_start_0
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    if-nez v0, :cond_0

    .line 242
    :try_start_1
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v3, v4, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policy/UmcPolicy;-><init>(Ljava/lang/String;Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 246
    :goto_1
    :try_start_2
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    :cond_0
    monitor-exit v1

    return-object v0

    .line 243
    :catch_0
    move-exception v2

    .line 244
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private isInstalled(Ljava/lang/String;Landroid/app/enterprise/ApplicationPolicy;)Z
    .locals 1

    .prologue
    .line 1041
    invoke-virtual {p2, p1}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v0

    .line 1042
    return v0
.end method


# virtual methods
.method public clearPendingCreation(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1294
    const/4 v0, 0x0

    .line 1295
    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    invoke-virtual {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getContextInfo(Ljava/lang/Integer;)Landroid/app/enterprise/ContextInfo;

    move-result-object v1

    .line 1297
    const-string v2, "GenericVpnPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1298
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "vendorpackage"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1299
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1302
    :cond_0
    const-string v2, "SSOPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1303
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "solutionPackageName"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1304
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1307
    :cond_1
    const-string v2, "EnterpriseSSOPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1308
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "type"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1309
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1312
    :cond_2
    const-string v2, "EnterpriseISLPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1313
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "vendorpackage"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1314
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1317
    :cond_3
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    monitor-enter v1

    .line 1318
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1317
    monitor-exit v1

    .line 1320
    return-void

    .line 1317
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAdvancedRestrictionPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 900
    .line 901
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mAdvRestrictionPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 902
    if-nez v0, :cond_2

    .line 903
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 904
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mAdvRestrictionPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 907
    :goto_0
    monitor-enter v1

    .line 908
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached getAdvancedRestrictionPolicy"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    .line 910
    if-nez v0, :cond_0

    .line 912
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 913
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached getAdvancedRestrictionPolicy - container not null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    .line 915
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getAdvancedRestrictionPolicy()Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    move-result-object v0

    .line 921
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 923
    :cond_0
    monitor-exit v1

    return-object v0

    .line 918
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached getAdvancedRestrictionPolicy - container null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    const-string v0, "AdvancedRestriction"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    goto :goto_1

    .line 907
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public getCCMPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 663
    .line 664
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mClientCertificateManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 665
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached CCM"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    if-nez v0, :cond_2

    .line 667
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v1, "reached CCM 2"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 669
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mClientCertificateManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 672
    :goto_0
    monitor-enter v1

    .line 673
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached CCM 3"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    .line 675
    if-nez v0, :cond_0

    .line 677
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 678
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached CCM - container not null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    .line 680
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getClientCertificateManagerPolicy()Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    move-result-object v0

    .line 686
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    :cond_0
    monitor-exit v1

    return-object v0

    .line 683
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "reached CCM - container null"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const-string v0, "ClientCertificateManager"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    goto :goto_1

    .line 672
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public getCertificatePolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 779
    .line 780
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mCertificatePolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 781
    if-nez v0, :cond_1

    .line 782
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 783
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mCertificatePolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 786
    :goto_0
    monitor-enter v1

    .line 787
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 788
    if-nez v0, :cond_0

    .line 790
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "CertificatePolicy using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 798
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    :cond_0
    monitor-exit v1

    return-object v0

    .line 792
    :catch_0
    move-exception v0

    .line 793
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "CertificatePolicy - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 795
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "CertificatePolicy using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    goto :goto_1

    .line 786
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getContainerPolicyObject(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 652
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    .line 653
    if-nez v0, :cond_0

    .line 654
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No such container of ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 656
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "get"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 657
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "@getContainerPolicyObject - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Class;

    invoke-virtual {v2, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceSettingsPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 436
    .line 437
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mDeviceSettingsPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 438
    if-nez v0, :cond_1

    .line 439
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 440
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mDeviceSettingsPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 443
    :goto_0
    monitor-enter v1

    .line 444
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    if-nez v0, :cond_0

    .line 447
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "DeviceSettingsPolicy using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 455
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    :cond_0
    monitor-exit v1

    return-object v0

    .line 449
    :catch_0
    move-exception v0

    .line 450
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "DeviceSettingsPolicy - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 452
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "DeviceSettingsPolicy using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    move-result-object v0

    goto :goto_1

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getDualSimPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 409
    .line 410
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mDualSimPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 411
    if-nez v0, :cond_1

    .line 412
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 413
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mDualSimPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 416
    :goto_0
    monitor-enter v1

    .line 417
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/dualsim/DualSimPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    if-nez v0, :cond_0

    .line 420
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "DualSimPolicy using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/dualsim/DualSimPolicy;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/dualsim/DualSimPolicy;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 428
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    :cond_0
    monitor-exit v1

    return-object v0

    .line 422
    :catch_0
    move-exception v0

    .line 423
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "DualSimPolicy - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 425
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "DualSimPolicy using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/dualsim/DualSimPolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/dualsim/DualSimPolicy;

    move-result-object v0

    goto :goto_1

    .line 416
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getEDMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 521
    .line 522
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mEnterpriseDeviceManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 523
    if-nez v0, :cond_3

    .line 524
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 525
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mEnterpriseDeviceManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 528
    :goto_0
    monitor-enter v1

    .line 529
    :try_start_0
    iget v0, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->LEGACY_CONTAINER_ID:I

    if-eq v0, v2, :cond_0

    .line 530
    iget v0, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Constants;->DEVICE_CONTAINER_ID:I

    if-eq v0, v2, :cond_0

    .line 532
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "@getEDMPolicy, getting policy object from KCM"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-virtual {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getContainerPolicyObject(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    .line 548
    :goto_1
    return-object v0

    .line 536
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 537
    if-nez v0, :cond_1

    .line 538
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, p1, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 539
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    :cond_1
    const-string v2, "EnterpriseDeviceManager"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 544
    monitor-exit v1

    goto :goto_1

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 547
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 548
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public getEKMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 928
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKM(Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v0

    .line 929
    const-string v1, "AuditLog"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 930
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getEnterpriseAuditLogPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/auditlog/AuditLog;

    move-result-object v0

    .line 941
    :goto_0
    return-object v0

    .line 931
    :cond_0
    const-string v1, "AdvancedRestriction"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 932
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getAdvancedRestrictionPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    move-result-object v0

    goto :goto_0

    .line 933
    :cond_1
    const-string v1, "ClientCertificateManager"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 934
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getClientCertificateManagerPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    move-result-object v0

    goto :goto_0

    .line 935
    :cond_2
    const-string v1, "TimaKeyStorePolicy"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 936
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getTimaKeystorePolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    move-result-object v0

    goto :goto_0

    .line 940
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "get"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 941
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getEnterpriseLicenseManagerPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 301
    .line 302
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mEnterpriseLicenseManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 303
    if-nez v0, :cond_1

    .line 304
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 305
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mEnterpriseLicenseManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 308
    :goto_0
    monitor-enter v1

    .line 309
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    if-nez v0, :cond_0

    .line 312
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "EnterpriseLicenseManager using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 320
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    :cond_0
    monitor-exit v1

    return-object v0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "EnterpriseLicenseManager - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 317
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "EnterpriseLicenseManager using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v0

    goto :goto_1

    .line 308
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getEnterpriseSSOPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 554
    .line 555
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mEnterpriseSSOPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 556
    if-nez v0, :cond_5

    .line 557
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 558
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mEnterpriseSSOPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v11, v0

    .line 561
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    .line 562
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v3

    .line 564
    iget-object v1, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "type"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 567
    if-nez v5, :cond_0

    .line 568
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Type field is empty"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 570
    :cond_0
    const-string v1, "samsung"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 571
    const-string v1, "com.sec.android.service.singlesignon"

    move-object v2, v1

    .line 578
    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->isInstalled(Ljava/lang/String;Landroid/app/enterprise/ApplicationPolicy;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 579
    monitor-enter v11

    .line 580
    :try_start_0
    invoke-direct {p0, p1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    if-nez v1, :cond_3

    .line 584
    :try_start_1
    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getEnterpriseSSOPolicy(Ljava/lang/String;)Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 596
    :try_start_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$SSOCreationHandler;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    move-object v9, p3

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$SSOCreationHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;ZLandroid/content/Context;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;)V

    .line 598
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 600
    :goto_2
    return-object v7

    .line 572
    :cond_1
    const-string v1, "centrify"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 573
    const-string v1, "com.centrify.sso.samsung"

    move-object v2, v1

    .line 574
    goto :goto_1

    .line 575
    :cond_2
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Type is not supplied correctly"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585
    :catch_0
    move-exception v0

    .line 586
    :try_start_3
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getEnterpriseSSOPolicy SecurityException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 579
    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 587
    :catch_1
    move-exception v0

    .line 588
    :try_start_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getEnterpriseSSOPolicy UnsupportedOperation !!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 589
    :catch_2
    move-exception v0

    .line 590
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "getEnterpriseSSOPolicy NullPointerException"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 600
    :cond_3
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v7, v1

    goto :goto_2

    .line 604
    :cond_4
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "The package "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not yet installed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v11, v0

    goto/16 :goto_0
.end method

.method public getGenericVPNPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 749
    .line 750
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mGenericVpnPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 751
    if-nez v0, :cond_2

    .line 752
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 753
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mGenericVpnPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 756
    :goto_0
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "vendorpackage"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 759
    const-string v0, "ApplicationPolicy"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEDMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ApplicationPolicy;

    .line 762
    invoke-direct {p0, v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->isInstalled(Ljava/lang/String;Landroid/app/enterprise/ApplicationPolicy;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 763
    monitor-enter v1

    .line 764
    :try_start_0
    invoke-direct {p0, p1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    .line 765
    if-eqz v0, :cond_0

    .line 766
    monitor-exit v1

    .line 770
    :goto_1
    return-object v0

    .line 763
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 769
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$GenericVPNCreationHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)V

    .line 770
    const/4 v0, 0x0

    goto :goto_1

    .line 763
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 773
    :cond_1
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not yet installed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public getGeofencingPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 254
    .line 255
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mGeofencingMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 256
    if-nez v0, :cond_1

    .line 257
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 258
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mGeofencingMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 261
    :goto_0
    monitor-enter v1

    .line 262
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/geofencing/Geofencing;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    if-nez v0, :cond_0

    .line 265
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "Geofencing using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/geofencing/Geofencing;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/geofencing/Geofencing;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 273
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    :cond_0
    monitor-exit v1

    return-object v0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "Geofencing - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 270
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "Geofencing using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/geofencing/Geofencing;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/geofencing/Geofencing;

    move-result-object v0

    goto :goto_1

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getISLPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 833
    .line 834
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mISLPolicy"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 835
    if-nez v0, :cond_3

    .line 836
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 837
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mISLPolicy"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 840
    :goto_0
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "vendorpackage"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 841
    if-nez v7, :cond_0

    .line 872
    :goto_1
    return-object v6

    .line 846
    :cond_0
    monitor-enter v1

    .line 847
    :try_start_0
    invoke-direct {p0, p1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    .line 846
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 849
    if-eqz v0, :cond_1

    .line 850
    iget-object v6, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->isl:Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    goto :goto_1

    .line 846
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 853
    :cond_1
    new-instance v8, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    invoke-direct {v8, p1}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    .line 854
    new-instance v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    invoke-direct {v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;-><init>()V

    .line 855
    invoke-virtual {v8, v9}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->setIntegrityResultSubscriber(Lcom/sec/enterprise/knox/IntegrityResultSubscriber;)V

    .line 857
    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    invoke-direct {v4, v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;-><init>(Lcom/sec/enterprise/knox/EnterpriseISLPolicy;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;)V

    .line 858
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;)V

    .line 859
    invoke-virtual {v9, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLBindListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;)V

    .line 862
    invoke-virtual {v8, v7}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->requestBindISA(Ljava/lang/String;)Z

    move-result v0

    .line 863
    if-nez v0, :cond_2

    .line 864
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " bind is failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 868
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    monitor-enter v1

    .line 869
    :try_start_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    invoke-direct {p0, p1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public getISLPolicyHolder(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;
    .locals 3

    .prologue
    .line 1029
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v1, "vendorpackage"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1030
    if-nez v0, :cond_0

    .line 1031
    const/4 v0, 0x0

    .line 1036
    :goto_0
    return-object v0

    .line 1034
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mISLPolicy"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 1035
    monitor-enter v1

    .line 1036
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    monitor-exit v1

    goto :goto_0

    .line 1035
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getKioskPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 281
    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mKioskModeMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 283
    if-nez v0, :cond_1

    .line 284
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 285
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mKioskModeMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 288
    :goto_0
    monitor-enter v1

    .line 289
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/kioskmode/KioskMode;

    .line 290
    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/kioskmode/KioskMode;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/kioskmode/KioskMode;

    move-result-object v0

    .line 292
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    :cond_0
    monitor-exit v1

    return-object v0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 631
    .line 632
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mKnoxContainerManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 633
    if-nez v0, :cond_2

    .line 634
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 635
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mKnoxContainerManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 638
    :goto_0
    monitor-enter v1

    .line 639
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    .line 640
    if-nez v0, :cond_1

    .line 641
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKM(Landroid/app/enterprise/ContextInfo;Z)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v0

    .line 642
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    move-result-object v0

    .line 643
    if-nez v0, :cond_0

    .line 644
    new-instance v0, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No such container of ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 638
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 645
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public getKnoxEnterpriseLicenseManagerPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 328
    .line 329
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mKnoxEnterpriseLicenseManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 330
    if-nez v0, :cond_1

    .line 331
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 332
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mKnoxEnterpriseLicenseManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 335
    :goto_0
    monitor-enter v1

    .line 336
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    if-nez v0, :cond_0

    .line 339
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "KnoxEnterpriseLicenseManager using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 347
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    :cond_0
    monitor-exit v1

    return-object v0

    .line 341
    :catch_0
    move-exception v0

    .line 342
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "KnoxEnterpriseLicenseManager - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 344
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "KnoxEnterpriseLicenseManager using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v0

    goto :goto_1

    .line 335
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getLockscreenOverlayPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 355
    .line 356
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mLockscreenOverlayMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 357
    if-nez v0, :cond_1

    .line 358
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 359
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mLockscreenOverlayMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 362
    :goto_0
    monitor-enter v1

    .line 363
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/lso/LockscreenOverlay;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    if-nez v0, :cond_0

    .line 366
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "LockscreenOverlay using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/lso/LockscreenOverlay;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/lso/LockscreenOverlay;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 374
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    :cond_0
    monitor-exit v1

    return-object v0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "LockscreenOverlay - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 371
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "LockscreenOverlay using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/lso/LockscreenOverlay;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/lso/LockscreenOverlay;

    move-result-object v0

    goto :goto_1

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getMultiUserManagerPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 382
    .line 383
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mMultiUserManagerMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 384
    if-nez v0, :cond_1

    .line 385
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 386
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mMultiUserManagerMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 389
    :goto_0
    monitor-enter v1

    .line 390
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/multiuser/MultiUserManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    if-nez v0, :cond_0

    .line 393
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "MultiUserManager using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/multiuser/MultiUserManager;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/multiuser/MultiUserManager;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 401
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    :cond_0
    monitor-exit v1

    return-object v0

    .line 395
    :catch_0
    move-exception v0

    .line 396
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "MultiUserManager - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 398
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "MultiUserManager using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/app/enterprise/multiuser/MultiUserManager;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/multiuser/MultiUserManager;

    move-result-object v0

    goto :goto_1

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getPolicyClass(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getPolicyName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1410
    if-nez v0, :cond_0

    .line 1411
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Policy class name is not found : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1413
    :cond_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public getPolicyObject(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 102
    const-string v0, "."

    invoke-virtual {p5, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    const-string v0, "com.sec.enterprise.knox.cloudmdm.smdms.policy.UmcPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-direct {p0, p3, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getUMCPolicy(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    .line 226
    :goto_0
    return-object v0

    .line 116
    :cond_0
    const-string v0, "android.app.enterprise.geofencing.Geofencing"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGeofencingPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_1
    const-string v0, "android.app.enterprise.kioskmode.KioskMode"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 122
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKioskPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_2
    const-string v0, "android.app.enterprise.license.EnterpriseLicenseManager"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEnterpriseLicenseManagerPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 128
    :cond_3
    const-string v0, "com.sec.enterprise.knox.license.KnoxEnterpriseLicenseManager"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxEnterpriseLicenseManagerPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 132
    :cond_4
    const-string v0, "android.app.enterprise.lso.LockscreenOverlay"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 134
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getLockscreenOverlayPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 136
    :cond_5
    const-string v0, "android.app.enterprise.multiuser.MultiUserManager"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 138
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getMultiUserManagerPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 140
    :cond_6
    const-string v0, "android.app.enterprise.dualsim.DualSimPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 142
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getDualSimPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 144
    :cond_7
    const-string v0, "android.app.enterprise.devicesettings.DeviceSettingsPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 146
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getDeviceSettingsPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_8
    const-string v0, "android.app.enterprise.SSOPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v4, p6

    move-object v5, p3

    .line 150
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 152
    :cond_9
    const-string v0, "android.app.enterprise"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 156
    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEDMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 160
    :cond_a
    const-string v0, "com.sec.enterprise.knox.EnterpriseSSOPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v4, p6

    move-object v5, p3

    .line 162
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEnterpriseSSOPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 164
    :cond_b
    const-string v0, "com.sec.enterprise.knox.container.SmartCardAccessPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 166
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSmartCardAccessPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 168
    :cond_c
    const-string v0, "com.sec.enterprise.knox.container.KnoxContainerManager"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 170
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 172
    :cond_d
    const-string v0, "com.sec.enterprise.knox.container"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 176
    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getContainerPolicyObject(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 178
    :cond_e
    const-string v0, "com.sec.enterprise.knox.ccm.ClientCertificateManager"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 180
    invoke-virtual {p0, p1, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getCCMPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 182
    :cond_f
    const-string v0, "com.sec.enterprise.knox.smartcard.policy.SmartCardBrowserPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 184
    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSmartCardBrowserPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 186
    :cond_10
    const-string v0, "com.sec.enterprise.knox.smartcard.policy.SmartCardEmailPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 188
    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSmartCardEmailPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 190
    :cond_11
    const-string v0, "com.sec.enterprise.knox.GenericVpnPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 192
    invoke-virtual {p0, p1, v2, p4, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGenericVPNPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 194
    :cond_12
    const-string v0, "com.sec.enterprise.knox.certificate.CertificatePolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 196
    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getCertificatePolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 198
    :cond_13
    const-string v0, "com.sec.enterprise.knox.Attestation"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 200
    new-instance v0, Lcom/sec/enterprise/knox/Attestation;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/sec/enterprise/knox/Attestation;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 202
    :cond_14
    const-string v0, "com.sec.enterprise.knox.seams"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 204
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSEAMSPolicy(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 206
    :cond_15
    const-string v0, "com.sec.enterprise.knox.EnterpriseISLPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 208
    invoke-virtual {p0, p1, p4, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 210
    :cond_16
    const-string v0, "com.sec.enterprise.knox.keystore.TimaKeystore"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 212
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getTimaKeyStorePolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 214
    :cond_17
    const-string v0, "com.sec.enterprise.knox.AdvancedRestrictionPolicy"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 216
    invoke-virtual {p0, p1, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getAdvancedRestrictionPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 218
    :cond_18
    const-string v0, "com.sec.enterprise.knox"

    invoke-virtual {p5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 222
    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 226
    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public getSEAMSPolicy(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 806
    .line 807
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mSEAMSPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 808
    if-nez v0, :cond_1

    .line 809
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 810
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mSEAMSPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 813
    :goto_0
    monitor-enter v1

    .line 814
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/seams/SEAMS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 815
    if-nez v0, :cond_0

    .line 817
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "SEAMS using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    invoke-static {p1, p2}, Lcom/sec/enterprise/knox/seams/SEAMS;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 825
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    :cond_0
    monitor-exit v1

    return-object v0

    .line 819
    :catch_0
    move-exception v0

    .line 820
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "SEAMS - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 822
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "SEAMS using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    invoke-static {p1, p2}, Lcom/sec/enterprise/knox/seams/SEAMS;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;

    move-result-object v0

    goto :goto_1

    .line 813
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getSSOPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 463
    .line 464
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mSSOPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 465
    if-nez v0, :cond_4

    .line 466
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 467
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mSSOPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v11, v0

    .line 470
    :goto_0
    const-string v0, "EnterpriseDeviceManager"

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEDMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 471
    const-string v1, "ApplicationPolicy"

    invoke-virtual {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEDMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ApplicationPolicy;

    .line 473
    iget-object v2, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v3, "solutionPackageName"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 475
    if-nez v5, :cond_0

    .line 476
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "solutionPackageName field is empty"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :cond_0
    invoke-direct {p0, v5, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->isInstalled(Ljava/lang/String;Landroid/app/enterprise/ApplicationPolicy;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 480
    monitor-enter v11

    .line 481
    :try_start_0
    invoke-direct {p0, p1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/SSOPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 482
    if-nez v1, :cond_2

    .line 485
    :try_start_1
    invoke-virtual {v0, v5}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSSOPolicy(Ljava/lang/String;)Landroid/app/enterprise/SSOPolicy;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 496
    :try_start_2
    invoke-virtual {v7, v5}, Landroid/app/enterprise/SSOPolicy;->isSSOReadyInUserSpace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    invoke-direct {p0, p1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$SSOCreationHandler;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    move-object v9, p3

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$SSOCreationHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;ZLandroid/content/Context;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;)V

    .line 499
    monitor-exit v11

    .line 510
    :goto_1
    return-object v7

    .line 486
    :catch_0
    move-exception v0

    .line 487
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getSSOPolicy SecurityException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 480
    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 488
    :catch_1
    move-exception v0

    .line 489
    :try_start_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getSSOPolicy UnsupportedOperation !!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 490
    :catch_2
    move-exception v0

    .line 491
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "getSSOPolicy NullPointerException"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$SSOCreationHandler;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    move-object v9, p3

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$SSOCreationHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;ZLandroid/content/Context;Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;)V

    .line 506
    invoke-direct {p0, v5, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->activateSSO(Ljava/lang/String;Landroid/app/enterprise/SSOPolicy;)V

    .line 508
    monitor-exit v11

    const/4 v7, 0x0

    goto :goto_1

    .line 510
    :cond_2
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v7, v1

    goto :goto_1

    .line 514
    :cond_3
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not yet installed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v11, v0

    goto/16 :goto_0
.end method

.method public getSmartCardAccessPolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 611
    .line 612
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mSmartCardAccessPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 613
    if-nez v0, :cond_1

    .line 614
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 615
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mSmartCardAccessPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 618
    :goto_0
    monitor-enter v1

    .line 619
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    .line 620
    if-nez v0, :cond_0

    .line 621
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getKnoxContainerManager(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    .line 622
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getSmartCardAccessPolicy()Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    move-result-object v0

    .line 623
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    :cond_0
    monitor-exit v1

    return-object v0

    .line 618
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getSmartCardBrowserPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 694
    .line 695
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mSmartCardBrowserPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 696
    if-nez v0, :cond_1

    .line 697
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 698
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mSmartCardBrowserPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 701
    :goto_0
    monitor-enter v1

    .line 702
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    if-nez v0, :cond_0

    .line 705
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "SmartCardBrowserPolicy using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 713
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    :cond_0
    monitor-exit v1

    return-object v0

    .line 707
    :catch_0
    move-exception v0

    .line 708
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "SmartCardBrowserPolicy - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 710
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "SmartCardBrowserPolicy using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    move-result-object v0

    goto :goto_1

    .line 701
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getSmartCardEmailPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 721
    .line 722
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v1, "mSmartCardEmailPolicyMap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 723
    if-nez v0, :cond_1

    .line 724
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 725
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mSmartCardEmailPolicyMap"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 728
    :goto_0
    monitor-enter v1

    .line 729
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 730
    if-nez v0, :cond_0

    .line 732
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "SmartCardEmailPolicy using createInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 740
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    :cond_0
    monitor-exit v1

    return-object v0

    .line 734
    :catch_0
    move-exception v0

    .line 735
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v3, "SmartCardEmailPolicy - This throwable is handled correctly"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 737
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->TAG:Ljava/lang/String;

    const-string v2, "SmartCardEmailPolicy using getInstance"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    move-result-object v0

    goto :goto_1

    .line 728
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public getTimaKeyStorePolicy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 877
    .line 878
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v2, "mTimaKeystore"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 879
    if-nez v1, :cond_1

    .line 880
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 881
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->globalPolicyClassHolder:Ljava/util/Map;

    const-string v3, "mTimaKeystore"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v1

    .line 884
    :goto_0
    monitor-enter v3

    .line 885
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 886
    if-nez v1, :cond_0

    .line 888
    :try_start_1
    const-string v2, "TimaKeyStorePolicy"

    invoke-virtual {p0, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getEKMPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    move-object v1, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 892
    :goto_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    :cond_0
    monitor-exit v3

    return-object v1

    .line 889
    :catch_0
    move-exception v2

    .line 890
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 884
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method initPolicyClassTbl()V
    .locals 3

    .prologue
    .line 1354
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "WifiPolicy"

    const-string v2, "android.app.enterprise.WifiPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1355
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SecurityPolicy"

    const-string v2, "android.app.enterprise.SecurityPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "RoamingPolicy"

    const-string v2, "android.app.enterprise.RoamingPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1357
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "RestrictionPolicy"

    const-string v2, "android.app.enterprise.RestrictionPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1358
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "PhoneRestrictionPolicy"

    const-string v2, "android.app.enterprise.PhoneRestrictionPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1359
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "PasswordPolicy"

    const-string v2, "android.app.enterprise.PasswordPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1360
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "MiscPolicy"

    const-string v2, "android.app.enterprise.MiscPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1361
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "LocationPolicy"

    const-string v2, "android.app.enterprise.LocationPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1362
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "LDAPAccountPolicy"

    const-string v2, "android.app.enterprise.LDAPAccountPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1363
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "FirewallPolicy"

    const-string v2, "android.app.enterprise.FirewallPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1364
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "ExchangeAccountPolicy"

    const-string v2, "android.app.enterprise.ExchangeAccountPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1365
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "EnterpriseDeviceManager"

    const-string v2, "android.app.enterprise.EnterpriseDeviceManager"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1366
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "EmailPolicy"

    const-string v2, "android.app.enterprise.EmailPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1367
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "EmailAccountPolicy"

    const-string v2, "android.app.enterprise.EmailAccountPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1368
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SSOPolicy"

    const-string v2, "android.app.enterprise.SSOPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1369
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "DeviceInventory"

    const-string v2, "android.app.enterprise.DeviceInventory"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1370
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "DeviceAccountPolicy"

    const-string v2, "android.app.enterprise.DeviceAccountPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1371
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "DateTimePolicy"

    const-string v2, "android.app.enterprise.DateTimePolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1372
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "BrowserPolicy"

    const-string v2, "android.app.enterprise.BrowserPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1373
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "BluetoothSecureModePolicy"

    const-string v2, "android.app.enterprise.BluetoothSecureModePolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "BluetoothPolicy"

    const-string v2, "android.app.enterprise.BluetoothPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1375
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "BasePasswordPolicy"

    const-string v2, "android.app.enterprise.BasePasswordPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1376
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "ApplicationPolicy"

    const-string v2, "android.app.enterprise.ApplicationPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1377
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "ApplicationPermissionControlPolicy"

    const-string v2, "android.app.enterprise.ApplicationPermissionControlPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1378
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "ApnSettingsPolicy"

    const-string v2, "android.app.enterprise.ApnSettingsPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1379
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "DeviceSettingsPolicy"

    const-string v2, "android.app.enterprise.devicesettings.DeviceSettingsPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "DualSimPolicy"

    const-string v2, "android.app.enterprise.dualsim.DualSimPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1381
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "Geofencing"

    const-string v2, "android.app.enterprise.geofencing.Geofencing"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1382
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "KioskMode"

    const-string v2, "android.app.enterprise.kioskmode.KioskMode"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1383
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "android.app.enterprise.license.EnterpriseLicenseManager"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1384
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "KnoxEnterpriseLicenseManager"

    const-string v2, "com.sec.enterprise.knox.license.KnoxEnterpriseLicenseManager"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1385
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "LockscreenOverlay"

    const-string v2, "android.app.enterprise.lso.LockscreenOverlay"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1386
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "MultiUserManager"

    const-string v2, "android.app.enterprise.multiuser.MultiUserManager"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1387
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "RemoteInjection"

    const-string v2, "android.app.enterprise.remotecontrol.RemoteInjection"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1388
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "GenericVpnPolicy"

    const-string v2, "com.sec.enterprise.knox.GenericVpnPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1389
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "com.sec.enterprise.knox.EnterpriseSSOPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1390
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "EnterpriseISLPolicy"

    const-string v2, "com.sec.enterprise.knox.EnterpriseISLPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1391
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "Attestation"

    const-string v2, "com.sec.enterprise.knox.Attestation"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "AdvancedRestrictionPolicy"

    const-string v2, "com.sec.enterprise.knox.AdvancedRestrictionPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1393
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "AuditLog"

    const-string v2, "com.sec.enterprise.knox.auditlog.AuditLog"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1394
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "ClientCertificateManager"

    const-string v2, "com.sec.enterprise.knox.ccm.ClientCertificateManager"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1395
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "CertificatePolicy"

    const-string v2, "com.sec.enterprise.knox.certificate.CertificatePolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1396
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "UmcPolicy"

    const-string v2, "com.sec.enterprise.knox.cloudmdm.smdms.policy.UmcPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1397
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SmartCardAccessPolicy"

    const-string v2, "com.sec.enterprise.knox.container.SmartCardAccessPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1398
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "RCPPolicy"

    const-string v2, "com.sec.enterprise.knox.container.RCPPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "KnoxContainerManager"

    const-string v2, "com.sec.enterprise.knox.container.KnoxContainerManager"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "TimaKeystore"

    const-string v2, "com.sec.enterprise.knox.keystore.TimaKeystore"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1401
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SEAMS"

    const-string v2, "com.sec.enterprise.knox.seams.SEAMS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1402
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SmartCardPolicy"

    const-string v2, "com.sec.enterprise.knox.smartcard.policy.SmartCardPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1403
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SmartCardEmailPolicy"

    const-string v2, "com.sec.enterprise.knox.smartcard.policy.SmartCardEmailPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1404
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "SmartCardBrowserPolicy"

    const-string v2, "com.sec.enterprise.knox.smartcard.policy.SmartCardBrowserPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1405
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->gPolicyClassTbl:Ljava/util/Map;

    const-string v1, "ContainerConfigurationPolicy"

    const-string v2, "com.sec.enterprise.knox.container.ContainerConfigurationPolicy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1406
    return-void
.end method

.method public isPendingCreation(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1262
    const/4 v0, 0x0

    .line 1263
    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    invoke-virtual {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getContextInfo(Ljava/lang/Integer;)Landroid/app/enterprise/ContextInfo;

    move-result-object v1

    .line 1265
    const-string v2, "com.sec.enterprise.knox.GenericVpnPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1266
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "vendorpackage"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1267
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getGVPNId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1270
    :cond_0
    const-string v2, "android.app.enterprise.SSOPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1271
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "solutionPackageName"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1272
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1275
    :cond_1
    const-string v2, "com.sec.enterprise.knox.EnterpriseSSOPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1276
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "type"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1277
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getSSOId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1280
    :cond_2
    const-string v2, "com.sec.enterprise.knox.EnterpriseISLPolicy"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1281
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    const-string v2, "vendorpackage"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1282
    invoke-direct {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1285
    :cond_3
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    monitor-enter v1

    .line 1286
    if-eqz v0, :cond_4

    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->mPendingCreations:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1287
    monitor-exit v1

    const/4 v0, 0x1

    .line 1290
    :goto_0
    return v0

    .line 1285
    :cond_4
    monitor-exit v1

    .line 1290
    const/4 v0, 0x0

    goto :goto_0

    .line 1285
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
