.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;
.super Ljava/lang/Object;
.source "ExchangeLoginHandler.java"


# static fields
.field public static tk:Ljava/lang/String;

.field public static uj:Ljava/lang/String;


# instance fields
.field private dJ:Z

.field private dK:Z

.field private mContext:Landroid/content/Context;

.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private pk:Ljava/lang/String;

.field private tG:Ljava/lang/String;

.field private uh:Ljava/lang/String;

.field private ui:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->ui:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;

    .line 27
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->uh:Ljava/lang/String;

    return-object v0
.end method

.method public static as(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    if-nez p0, :cond_1

    .line 48
    const/4 p0, 0x0

    .line 55
    :cond_0
    :goto_0
    return-object p0

    .line 50
    :cond_1
    :try_start_0
    const-string v0, "\\\\"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 51
    const-string v0, "\\\\"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object p0, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    const-string v0, "ExchangeLoginActivity"

    const-string v1, "No domain"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static at(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59
    if-nez p0, :cond_1

    .line 60
    const/4 p0, 0x0

    .line 66
    :cond_0
    :goto_0
    return-object p0

    .line 62
    :cond_1
    :try_start_0
    const-string v0, "\\\\"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-le v0, v1, :cond_0

    .line 63
    const-string v0, "\\\\"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object p0, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->tG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->dJ:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->ui:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->pk:Ljava/lang/String;

    return-object v0
.end method

.method public static k(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 32
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 35
    const-string v1, "pkcs12"

    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 37
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 38
    invoke-virtual {v1, v0, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    const-string v1, "ExchangeLoginActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "validateClientCert:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->as(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->mDomain:Ljava/lang/String;

    .line 73
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->pk:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->mPassword:Ljava/lang/String;

    .line 75
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->at(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->tG:Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->uh:Ljava/lang/String;

    .line 78
    iput-boolean p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->dJ:Z

    .line 79
    iput-boolean p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->dK:Z

    .line 81
    sput-object p7, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->uj:Ljava/lang/String;

    .line 82
    sput-object p8, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->tk:Ljava/lang/String;

    .line 83
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 84
    return-void
.end method
