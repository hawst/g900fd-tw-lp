.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;
.super Ljava/lang/Object;
.source "CarouselUi.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mDuration:J

.field private zA:I

.field private zB:I

.field private zC:Landroid/os/Handler;

.field private zD:Ljava/lang/Runnable;

.field private zE:Z

.field private zF:Z

.field private zG:Z

.field private zH:I

.field private zI:I

.field private zJ:[Landroid/widget/ImageView;

.field private zw:[I

.field private zx:[I

.field private zy:Landroid/widget/ImageView;

.field private zz:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zy:Landroid/widget/ImageView;

    .line 104
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zz:Landroid/widget/TextView;

    .line 106
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    .line 117
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    .line 118
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zw:[I

    .line 119
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zx:[I

    .line 120
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zz:Landroid/widget/TextView;

    .line 121
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zy:Landroid/widget/ImageView;

    .line 122
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    .line 123
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)[Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zJ:[Landroid/widget/ImageView;

    .line 124
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zH:I

    .line 125
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->i(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zI:I

    .line 126
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;->j(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zB:I

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zC:Landroid/os/Handler;

    .line 129
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zD:Ljava/lang/Runnable;

    .line 146
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zF:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zC:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    return-wide v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zG:Z

    return v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->jH()V

    return-void
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->jG()V

    return-void
.end method

.method private jE()Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 149
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v2, -0x40800000    # -1.0f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 152
    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    const-wide/16 v4, 0x4

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 153
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 154
    return-object v0
.end method

.method private jF()Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const-wide/16 v10, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 158
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 159
    const/high16 v4, 0x3f800000    # 1.0f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    .line 158
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 161
    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    div-long/2addr v2, v10

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 162
    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    iget-wide v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    div-long/2addr v4, v10

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 163
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 164
    return-object v0
.end method

.method private jG()V
    .locals 2

    .prologue
    .line 271
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zB:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    .line 272
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->update()V

    .line 273
    return-void
.end method

.method private jH()V
    .locals 2

    .prologue
    .line 279
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zB:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    .line 280
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->update()V

    .line 281
    return-void
.end method

.method private update()V
    .locals 12

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const-wide/16 v10, 0x4

    const/4 v0, 0x0

    .line 168
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Updating UI, index="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zy:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zw:[I

    if-eqz v1, :cond_3

    .line 171
    new-instance v1, Landroid/view/animation/AnimationSet;

    invoke-direct {v1, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 172
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->jE()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 173
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->jF()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 174
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zy:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 175
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zy:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zw:[I

    iget v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 180
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zz:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zx:[I

    if-eqz v1, :cond_4

    .line 182
    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    const-wide/16 v4, 0x8

    div-long/2addr v2, v4

    .line 183
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 184
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 186
    iget-wide v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    div-long/2addr v4, v10

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 187
    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 188
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v4, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 189
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 191
    iget-wide v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    iget-wide v8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    div-long/2addr v8, v10

    sub-long/2addr v6, v8

    add-long/2addr v2, v6

    invoke-virtual {v4, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 192
    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    div-long/2addr v2, v10

    invoke-virtual {v4, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 193
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 195
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 196
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 197
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zz:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 198
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zz:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zx:[I

    iget v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 203
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zJ:[Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 204
    :goto_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zJ:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_5

    .line 213
    :cond_2
    return-void

    .line 177
    :cond_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    const-string v2, "No image resources specified for Carousel R.drawable.foo_bar"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 200
    :cond_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    const-string v2, "No title resources specified for Carousel R.string.foo_bar"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 205
    :cond_5
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zA:I

    if-ne v1, v0, :cond_6

    .line 206
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zJ:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zH:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 204
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 208
    :cond_6
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zJ:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zI:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3
.end method


# virtual methods
.method public start()V
    .locals 4

    .prologue
    .line 219
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "start(), mIsStarted:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zE:Z

    if-nez v0, :cond_0

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zE:Z

    .line 222
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zC:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zD:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->mDuration:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 224
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zC:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->zD:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 265
    return-void
.end method
