.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;
.super Ljava/lang/Object;
.source "ProfileHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    .line 132
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->restore(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;)V

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->applyProfileInternal()Z
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    return-void

    .line 138
    :catch_0
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public schedule()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->schedule(Ljava/lang/Runnable;)V

    .line 146
    return-void
.end method
