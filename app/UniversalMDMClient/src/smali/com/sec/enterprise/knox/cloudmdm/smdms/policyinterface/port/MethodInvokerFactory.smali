.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/MethodInvokerFactory;
.super Ljava/lang/Object;
.source "MethodInvokerFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
    .locals 2

    .prologue
    .line 46
    .line 48
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseISLPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 69
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "SSOPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseSSOPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "Attestation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AttestationMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AttestationMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 56
    :cond_3
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "UmcPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 58
    :cond_4
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "ApplicationPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 59
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ApplicationMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 60
    :cond_5
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "SecurityPolicy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 61
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SecurityPolicyMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 62
    :cond_6
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseLicenseManager"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 63
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 64
    :cond_7
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "KnoxEnterpriseLicenseManager"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 65
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/KnoxEnterpriseLicenseManagerMethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto :goto_0

    .line 67
    :cond_8
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    goto/16 :goto_0
.end method
