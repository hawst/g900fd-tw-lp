.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;
.super Ljava/lang/Object;
.source "UMCPolicyMethodInvoker.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;


# instance fields
.field params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;


# direct methods
.method private constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;)V
    .locals 1

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->params:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;)V

    return-void
.end method


# virtual methods
.method public onProfileApplied(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;)V
    .locals 3

    .prologue
    .line 84
    const-string v0, "UMC:UMCPolicyMethodInvoker"

    const-string v1, "onProfileApplied is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->params:Ljava/util/Map;

    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->params:Ljava/util/Map;

    const-string v1, "result"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->params:Ljava/util/Map;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->setAPIReturnParams(Ljava/util/Map;)V
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;Ljava/util/Map;)V

    .line 88
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker$AdminProfileApplyListenerImpl;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;->INVOKE_SUCCESS_STATE:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;

    const/4 v2, 0x1

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/UMCPolicyMethodInvoker;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V

    .line 89
    return-void
.end method
