.class Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;
.super Landroid/content/BroadcastReceiver;
.source "SppInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;)V
.end annotation


# instance fields
.field final synthetic xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;->xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v5, 0x3e8

    .line 35
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] onReceive an intent"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[BroadcastReceiver] action:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 54
    const-string v0, "appId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[BroadcastReceiver] appId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    if-nez v0, :cond_1

    .line 60
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] appId==null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-string v1, "com.sec.spp.RegistrationFail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 64
    const-string v1, "Error"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 65
    const-string v2, "UMC:SppInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[BroadcastReceiver] setText:PUSH_REGISTRATION_FAIL. appid is default :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 67
    const-string v3, " with error code:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_2
    const-string v1, "com.sec.spp.DeRegistrationFail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 71
    const-string v1, "Error"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 72
    const-string v2, "UMC:SppInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[BroadcastReceiver] setText:PUSH_DEREGISTRATION_FAIL. appid is default :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 74
    const-string v3, " with error code:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_3
    const-string v1, "485c57ce73420eed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string v1, "com.sec.spp.Status"

    .line 79
    const/4 v2, 0x1

    .line 78
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 80
    const-string v2, "UMC:SppInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[BroadcastReceiver] registrationState:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 84
    :pswitch_0
    const-string v1, "RegistrationID"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    if-nez p0, :cond_4

    .line 86
    const-string v0, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUSH_REGISTRATION_SUCCESS \n intent:regId=>\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n mPushService.getRegId()=>\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 88
    const-string v2, "mPushService==null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :goto_1
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] setText:PUSH_REGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 90
    :cond_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;->xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    invoke-virtual {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->be(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string v2, "UMC:SppInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PUSH_REGISTRATION_SUCCESS \n intent:regId=>\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n mPushService.getRegId()=>\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 93
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;->xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;->H(Ljava/lang/String;)V

    goto :goto_1

    .line 100
    :pswitch_1
    const-string v0, "Error"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 101
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUSH_REGISTRATION_FAIL. errorCode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] setText:PUSH_REGISTRATION_FAIL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a$2;->xG:Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;->H(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 107
    :pswitch_2
    const-string v0, "UMC:SppInterface"

    const-string v1, "PUSH_DEREGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] setText:PUSH_DEREGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 112
    :pswitch_3
    const-string v0, "Error"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 113
    const-string v1, "UMC:SppInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUSH_DEREGISTRATION_FAIL. errorCode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] setText:PUSH_DEREGISTRATION_FAIL"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    :cond_5
    const-string v1, "com.sec.spp.ServiceAbnormallyStoppedAction"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 120
    const-string v0, "UMC:SppInterface"

    .line 121
    const-string v1, "[BroadcastReceiver] received : SERVICE_ABNORMALLY_STOPPED_ACTION, Server stopped abnormally!"

    .line 120
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 123
    :cond_6
    const-string v0, "UMC:SppInterface"

    const-string v1, "[BroadcastReceiver] action!=com.sec.spp.RegistrationChangedAction"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
