.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/DummyPermissions;
.super Ljava/lang/Object;
.source "DummyPermissions.java"


# static fields
.field static final mAllPermisionsForTesting:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/16 v0, 0x69

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 36
    const-string v2, "android.permission.BIND_DEVICE_ADMIN"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 37
    const-string v2, "android.permission.sec.ENTERPRISE_DEVICE_ADMIN"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 38
    const-string v2, "android.permission.sec.MDM_APP_MGMT"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 39
    const-string v2, "android.permission.sec.MDM_APP_PERMISSION_MGMT"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 40
    const-string v2, "android.permission.sec.MDM_BLUETOOTH"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 41
    const-string v2, "android.permission.sec.MDM_INVENTORY"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 42
    const-string v2, "android.permission.sec.MDM_EXCHANGE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 43
    const-string v2, "android.permission.sec.MDM_ROAMING"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 44
    const-string v2, "android.permission.sec.MDM_WIFI"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 45
    const-string v2, "android.permission.sec.MDM_SECURITY"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 46
    const-string v2, "android.permission.sec.MDM_HW_CONTROL"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 47
    const-string v2, "android.permission.sec.MDM_RESTRICTION"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 48
    const-string v2, "android.permission.sec.MDM_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 49
    const-string v2, "android.permission.sec.MDM_CALLING"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 50
    const-string v2, "android.permission.sec.MDM_EMAIL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 51
    const-string v2, "android.permission.sec.MDM_VPN"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 52
    const-string v2, "android.permission.sec.MDM_APN"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 53
    const-string v2, "android.permission.sec.MDM_PHONE_RESTRICTION"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 54
    const-string v2, "android.permission.sec.MDM_BROWSER_SETTINGS"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 55
    const-string v2, "android.permission.sec.MDM_DATE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 56
    const-string v2, "android.permission.sec.MDM_ENTERPRISE_VPN"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 57
    const-string v2, "android.permission.sec.MDM_FIREWALL"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 58
    const-string v2, "android.permission.sec.MDM_REMOTE_CONTROL"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 59
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 60
    const-string v2, "android.permission.BLUETOOTH"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 61
    const-string v2, "android.permission.ACCESS_WIFI_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 62
    const-string v2, "android.permission.SET_WALLPAPER"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 63
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 64
    const-string v2, "android.permission.WRITE_SETTINGS"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 65
    const-string v2, "android.permission.WAKE_LOCK"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 66
    const-string v2, "android.permission.NFC"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 67
    const-string v2, "android.permission.sec.MDM_KIOSK_MODE"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 69
    const-string v2, "com.android.launcher.permission.INSTALL_SHORTCUT"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 70
    const-string v2, "com.android.launcher.permission.UNINSTALL_SHORTCUT"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 72
    const-string v2, "android.permission.sec.MDM_AUDIT_LOG"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 73
    const-string v2, "android.permission.sec.MDM_CERTIFICATE"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 74
    const-string v2, "android.permission.INTERNET"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 75
    const-string v2, "android.permission.sec.MDM_SMARTCARD"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 76
    const-string v2, "android.permission.sec.MDM_SEANDROID"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 79
    const-string v2, "android.permission.sec.MDM_LDAP"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 80
    const-string v2, "android.permission.sec.MDM_LOCKSCREEN"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 81
    const-string v2, "android.permission.CALL_PHONE"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 82
    const-string v2, "android.permission.READ_PHONE_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 83
    const-string v2, "android.permission.sec.MDM_APP_BACKUP"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 84
    const-string v2, "android.permission.sec.MDM_GEOFENCING"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 85
    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 86
    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    .line 87
    const-string v2, "android.permission.ACCESS_MOCK_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 88
    const-string v2, "android.permission.GET_TASKS"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 89
    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 90
    const-string v2, "com.mocana.vpn.android.permission.MOCANA_VPN_SERVICE"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 91
    const-string v2, "android.permission.sec.MDM_BLUETOOTH_SECUREMODE"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 92
    const-string v2, "android.permission.sec.MDM_MULTI_USER_MGMT"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 94
    const-string v2, "android.permission.sec.MDM_LICENSE_LOG"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 95
    const-string v2, "android.permission.sec.MDM_DUAL_SIM"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 99
    const-string v2, "com.sec.enterprise.mdm.permission.BROWSER_PROXY"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 100
    const-string v2, "android.permission.sec.MDM_SSO"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    .line 101
    const-string v2, "android.Manifest.permission.MDM_SSO"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    .line 102
    const-string v2, "android.Manifest.permission.MDM_ENTERPRISE_SSO"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 103
    const-string v2, "com.sec.enterprise.mdm.permission.MDM_SSO"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    .line 107
    const-string v2, "android.permission.sec.MDM_ENTERPRISE_SSO"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    .line 108
    const-string v2, "android.permission.sec.MDM_ENTERPRISE_ISL"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    .line 109
    const-string v2, "android.permission.sec.MDM_ENTERPRISE_CONTAINER"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    .line 110
    const-string v2, "android.permission.sec.ENTERPRISE_MOUNT_UNMOUNT_ENCRYPT"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    .line 111
    const-string v2, "android.permission.sec.ENTERPRISE_CONTAINER"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    .line 112
    const-string v2, "android.permission.INSTALL_PACKAGES"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    .line 113
    const-string v2, "com.sec.enterprise.knox.KNOX_GENERIC_VPN"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    .line 117
    const-string v2, "com.sec.enterprise.knox.permission.KNOX_RCP_SYNC_MGMT"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 119
    const-string v2, "android.permission.INTERACT_ACROSS_USERS_FULL"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    .line 120
    const-string v2, "android.permission.android.permission.INTERACT_ACROSS_USERS"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    .line 124
    const-string v2, "android.permission.INTERNET"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    .line 125
    const-string v2, "android.permission.GET_ACCOUNTS"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 126
    const-string v2, "android.permission.WAKE_LOCK"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    .line 127
    const-string v2, "com.google.android.c2dm.permission.RECEIVE"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    .line 129
    const-string v2, "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    .line 130
    const-string v2, "com.sec.enterprise.knox.permission.KNOX_RESTRICTION"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    .line 131
    const-string v2, "com.sec.enterprise.knox.permission.KNOX_SEAMS"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    .line 132
    const-string v2, "com.sec.enterprise.knox.permission.KNOX_SEAMS_SEPOLICY"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    .line 133
    const-string v2, "com.sec.enterprise.knox.permission.KNOX_CCM"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    .line 136
    const-string v2, "wipe-data"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    .line 137
    const-string v2, "reset-password"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    .line 138
    const-string v2, "limit-password"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    .line 139
    const-string v2, "watch-login"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    .line 140
    const-string v2, "force-lock"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    .line 141
    const-string v2, "set-global-proxy"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    .line 142
    const-string v2, "expire-password"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    .line 143
    const-string v2, "encrypted-storage"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    .line 144
    const-string v2, "disable-camera"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    .line 145
    const-string v2, "disable-keyguard-features"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    .line 146
    const-string v2, "require-storagecard-encryption"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    .line 147
    const-string v2, "recover-password"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    .line 148
    const-string v2, "allow-popimapemail"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    .line 149
    const-string v2, "allow-storagecard"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    .line 150
    const-string v2, "allow-wifi"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    .line 151
    const-string v2, "allow-textmessaging"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    .line 152
    const-string v2, "allow-browser"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    .line 153
    const-string v2, "allow-internetsharing"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    .line 154
    const-string v2, "allow-bluetoothmode"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    .line 155
    const-string v2, "allow-desktopsync"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    .line 156
    const-string v2, "allow-irda"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    .line 157
    const-string v2, "allow-list-thirdparty"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    .line 158
    const-string v2, "block-list-InRom"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    .line 159
    const-string v2, "allow-unsignedapp"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    .line 160
    const-string v2, "allow-unsignedinstallationpkg"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    .line 161
    const-string v2, "disable-lock-password"

    aput-object v2, v0, v1

    .line 35
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/DummyPermissions;->mAllPermisionsForTesting:[Ljava/lang/String;

    .line 162
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAllPermissions()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/DummyPermissions;->mAllPermisionsForTesting:[Ljava/lang/String;

    return-object v0
.end method
