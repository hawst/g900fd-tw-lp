.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;
.source "DomProfile.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:DomProfile"


# instance fields
.field protected transient childProfiles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;",
            ">;"
        }
    .end annotation
.end field

.field protected domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

.field protected domData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->childProfiles:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    .line 54
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->data:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->data:Ljava/lang/Object;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->data:Ljava/lang/Object;

    .line 59
    :goto_0
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 60
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->data:Ljava/lang/Object;

    goto :goto_0
.end method

.method private isProfile(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 87
    const-string v0, "UMC:DomProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@isProfile DOMPROFILE +  childname = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->getChildConfig(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 89
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static realizeDomTree(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 136
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->dotSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 138
    :goto_0
    array-length v0, v3

    if-lt v1, v0, :cond_0

    .line 153
    const/4 v2, 0x1

    :goto_1
    return v2

    .line 139
    :cond_0
    aget-object v4, v3, v1

    .line 140
    invoke-virtual {p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->getChild(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    move-result-object v0

    .line 141
    if-nez v0, :cond_2

    .line 142
    invoke-direct {p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->isProfile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    const-string v0, "UMC:DomProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "One of the member in the Path is not a profile Member Name = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 147
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->addChild(Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    .line 150
    :cond_2
    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    .line 138
    add-int/lit8 v1, v1, 0x1

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public addChild(Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->getChildConfig(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-static {p1, p0, v1, v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileFactory;->createInstance(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->childProfiles:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    return-object v0
.end method

.method public applyDomProfile(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    check-cast p1, Ljava/util/Map;

    .line 163
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 164
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 165
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 168
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 169
    if-nez p1, :cond_5

    const/4 v1, 0x0

    move-object v2, v1

    .line 171
    :goto_2
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->isProfile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    if-nez v4, :cond_3

    if-eqz v2, :cond_2

    .line 179
    :cond_3
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->childProfiles:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    .line 180
    if-nez v1, :cond_4

    .line 182
    invoke-virtual {p0, v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->addChild(Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v1

    .line 185
    :cond_4
    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->apply(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 186
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 187
    const-string v1, "UMC:DomProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Profile Apply for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " failed"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 193
    :catch_0
    move-exception v0

    .line 195
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 169
    :cond_5
    :try_start_1
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    move-object v2, v1

    goto :goto_2
.end method

.method protected applyInternal(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->applyDomProfile(Ljava/lang/Object;)V

    .line 96
    return-void
.end method

.method public getChild(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->childProfiles:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    return-object v0
.end method

.method public getChildConfig(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;
    .locals 3

    .prologue
    .line 119
    const-string v0, "UMC:DomProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Calling getChildConfig @ DOMPROFILE with childname = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domConfig:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->childs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    .line 121
    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 124
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->get(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    goto :goto_0
.end method

.method protected getCurrentEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getParentEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 68
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->dotSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x2

    aget-object v0, v0, v1

    .line 69
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->createMapObject(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected init()Z
    .locals 6

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    const/4 v0, 0x1

    return v0

    .line 73
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 74
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 76
    const-string v3, "UMC:DomProfile"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "@DOM init - childnName = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->isProfile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 78
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->addChild(Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->init()Z

    goto :goto_0
.end method

.method public onChildProfileApplied(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 99
    if-nez p2, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->data:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->onChildProfileApplied(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    :goto_1
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->domData:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->parent:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->name:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->onChildProfileApplied(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->profileHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileUpdated()V

    goto :goto_1
.end method
