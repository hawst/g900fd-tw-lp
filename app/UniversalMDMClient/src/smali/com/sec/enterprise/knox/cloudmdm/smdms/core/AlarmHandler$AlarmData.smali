.class final Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;
.super Ljava/lang/Object;
.source "AlarmHandler.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x653da3beffcd816cL


# instance fields
.field private final id:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field private final nextAlarmTime:J


# direct methods
.method constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    .line 73
    iput-wide p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->nextAlarmTime:J

    .line 74
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;)J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->nextAlarmTime:J

    return-wide v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 93
    if-ne p0, p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 95
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 96
    goto :goto_0

    .line 97
    :cond_2
    instance-of v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    if-nez v2, :cond_3

    move v0, v1

    .line 98
    goto :goto_0

    .line 99
    :cond_3
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;

    .line 100
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 101
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 102
    goto :goto_0

    .line 103
    :cond_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 104
    goto :goto_0

    .line 105
    :cond_5
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 106
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 107
    goto :goto_0

    .line 108
    :cond_6
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 109
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 122
    .line 124
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 125
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 126
    return v0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    const-string v1, "AlarmData [id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", nextAlarmTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 133
    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->nextAlarmTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler$AlarmData;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
