.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;
.super Ljava/lang/Object;
.source "AdminProfile.java"


# instance fields
.field profiles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;
    .locals 2

    .prologue
    .line 12
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    :goto_0
    return-object v0

    .line 13
    :catch_0
    move-exception v0

    .line 14
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 15
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public toJson()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    invoke-virtual {v0, p0}, Lcom/google/gson/e;->j(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 24
    :goto_0
    return-object v0

    .line 22
    :catch_0
    move-exception v0

    .line 23
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 24
    const/4 v0, 0x0

    goto :goto_0
.end method
