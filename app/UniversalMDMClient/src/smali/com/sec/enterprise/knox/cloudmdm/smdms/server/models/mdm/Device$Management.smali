.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private agent:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

.field private byodEnabled:Z

.field private payload:Lcom/fasterxml/jackson/databind/JsonNode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAgent()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->agent:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    return-object v0
.end method

.method public getByodEnabled()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->byodEnabled:Z

    return v0
.end method

.method public getPayload()Lcom/fasterxml/jackson/databind/JsonNode;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->payload:Lcom/fasterxml/jackson/databind/JsonNode;

    return-object v0
.end method

.method public setAgent(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->agent:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management$Agent;

    .line 183
    return-void
.end method

.method public setByodEnabled(Z)V
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->byodEnabled:Z

    .line 199
    return-void
.end method

.method public setPayload(Lcom/fasterxml/jackson/databind/JsonNode;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->payload:Lcom/fasterxml/jackson/databind/JsonNode;

    .line 191
    return-void
.end method
