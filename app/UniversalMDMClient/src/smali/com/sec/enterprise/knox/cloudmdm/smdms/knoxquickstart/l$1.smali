.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;
.super Landroid/os/Handler;
.source "QuickstartWorkspaceCreatingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->hh()V
.end annotation


# instance fields
.field final synthetic tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    .line 125
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 128
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 159
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->gp()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled message type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 133
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    .line 135
    const-string v1, "step"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    .line 136
    const-string v1, "headerMessage"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 137
    const-string v2, "actionMessage"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 138
    const-string v3, "progress"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 141
    if-eq v0, v4, :cond_1

    .line 142
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/ProgressBar;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 143
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 146
    :cond_1
    if-eq v1, v4, :cond_2

    .line 147
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 151
    :cond_2
    if-eq v2, v4, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l$1;->tB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/l;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
