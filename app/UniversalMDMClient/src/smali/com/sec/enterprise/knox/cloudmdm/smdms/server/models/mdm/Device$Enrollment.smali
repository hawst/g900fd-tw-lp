.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private emailAddress:Ljava/lang/String;

.field private group:Ljava/lang/String;

.field private profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    return-void
.end method


# virtual methods
.method public getEmailAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->emailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->group:Ljava/lang/String;

    return-object v0
.end method

.method public getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;

    return-object v0
.end method

.method public setEmailAddress(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->emailAddress:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setGroup(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->group:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;

    .line 127
    return-void
.end method
