.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;
.super Ljava/lang/Object;
.source "EnrollmentInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field device:Ljava/lang/String;

.field keyPair:Ljava/security/KeyPair;

.field permissions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

.field qN:I

.field qO:I

.field qP:Ljava/lang/String;

.field qQ:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qN:I

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qO:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qP:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->device:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qQ:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/security/KeyPair;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->keyPair:Ljava/security/KeyPair;

    .line 75
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;Ljava/util/ArrayList;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/security/KeyPair;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 80
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    .line 81
    iput p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qN:I

    .line 82
    iput p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qO:I

    .line 83
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qP:Ljava/lang/String;

    .line 84
    iput-object p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->device:Ljava/lang/String;

    .line 85
    iput-object p7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qQ:Ljava/lang/String;

    .line 86
    iput-object p8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->keyPair:Ljava/security/KeyPair;

    .line 87
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    const-string v1, "EnrollmentInfo [profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", permissions="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 110
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", klm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qN:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", elm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qO:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 111
    const-string v2, ", userCred="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->device:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 112
    const-string v2, ", mdmCerts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qQ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 98
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qO:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->device:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qQ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->keyPair:Ljava/security/KeyPair;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 104
    return-void
.end method
