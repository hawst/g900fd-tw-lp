.class public abstract Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;
.super Ljava/lang/Object;
.source "PushMessageProcessor.java"


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected qX:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

.field protected qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;)V
    .locals 3

    .prologue
    .line 58
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->qX:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->init()Z

    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    if-nez v0, :cond_0

    .line 63
    const-string v0, "UMC:PushMessageProcessor"

    const-string v1, "No Profile for Message"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->qX:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

    invoke-interface {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;->gf()V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V

    goto :goto_0
.end method

.method public abstract g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
.end method
