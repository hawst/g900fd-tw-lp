.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;
.super Ljava/lang/Object;
.source "ProfileStorage.java"


# static fields
.field private static qt:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 262
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    .line 263
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)I
    .locals 3

    .prologue
    .line 463
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    const-string v0, "ProfileStorage"

    const-string v2, "Enter storeProfile"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 466
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 467
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-direct {v2, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 469
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->i(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    const-string v0, "ProfileStorage"

    const-string v2, "UnLoad List failed"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    const/4 v0, -0x4

    .line 473
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;
    .locals 4

    .prologue
    .line 360
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 362
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter getProfile:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 368
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    .line 363
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 364
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;
    .locals 4

    .prologue
    .line 387
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 389
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter Device Id :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter Profile Id :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 396
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    .line 391
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 392
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized d(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;
    .locals 4

    .prologue
    .line 373
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 375
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter App Secret:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 381
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    .line 376
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 377
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;
    .locals 4

    .prologue
    .line 402
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 404
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter Email :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter Profile Id :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 411
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    .line 406
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 407
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 422
    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 424
    const-string v0, "ProfileStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Enter removeProfile:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const/4 v0, 0x0

    .line 426
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 432
    if-eqz v1, :cond_1

    .line 433
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 436
    :cond_1
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->i(Landroid/content/Context;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    monitor-exit v2

    return-void

    .line 426
    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 427
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v0

    .line 428
    goto :goto_0

    .line 422
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 443
    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v2

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 445
    const-string v0, "ProfileStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Enter removeProfile:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const/4 v0, 0x0

    .line 447
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 454
    if-eqz v1, :cond_1

    .line 455
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 458
    :cond_1
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->i(Landroid/content/Context;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    monitor-exit v2

    return-void

    .line 447
    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 448
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 449
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v0

    .line 450
    goto :goto_0

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;
    .locals 4

    .prologue
    .line 491
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 493
    const-string v0, "ProfileStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getProfileByManagementService:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 500
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    .line 494
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 495
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 496
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 491
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized h(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 278
    const-class v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v6

    :try_start_0
    const-string v3, "ProfileStorage"

    const-string v4, "Enter loadList"

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 281
    const-string v0, "ProfileStorage"

    const-string v2, "Already Loaded"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 315
    :goto_0
    monitor-exit v6

    return v0

    .line 285
    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    sput-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 290
    :try_start_2
    const-string v3, "profile.dat"

    invoke-virtual {p0, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 291
    :try_start_3
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v4}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 292
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v7

    .line 294
    const-string v0, "ProfileStorage"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Load List: nitems="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move v5, v1

    .line 296
    :goto_1
    if-lt v5, v7, :cond_3

    .line 307
    if-eqz v3, :cond_1

    .line 308
    :try_start_5
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 309
    :cond_1
    if-eqz v4, :cond_2

    .line 310
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_2
    move v0, v1

    .line 315
    goto :goto_0

    .line 297
    :cond_3
    :try_start_6
    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 296
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 299
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 307
    :goto_3
    if-eqz v0, :cond_4

    .line 308
    :try_start_7
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->close()V

    .line 309
    :cond_4
    if-eqz v1, :cond_5

    .line 310
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_5
    :goto_4
    move v0, v2

    .line 301
    goto :goto_0

    .line 311
    :catch_1
    move-exception v0

    .line 312
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 302
    :catch_2
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v0, v1

    .line 303
    :goto_5
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 307
    if-eqz v3, :cond_6

    .line 308
    :try_start_a
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 309
    :cond_6
    if-eqz v4, :cond_7

    .line 310
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_7
    :goto_6
    move v0, v2

    .line 304
    goto :goto_0

    .line 311
    :catch_3
    move-exception v0

    .line 312
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    .line 305
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v4, v0

    move-object v0, v1

    .line 307
    :goto_7
    if-eqz v3, :cond_8

    .line 308
    :try_start_c
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 309
    :cond_8
    if-eqz v4, :cond_9

    .line 310
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 314
    :cond_9
    :goto_8
    :try_start_d
    throw v0

    .line 311
    :catch_4
    move-exception v1

    .line 312
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 311
    :catch_5
    move-exception v0

    .line 312
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_2

    .line 305
    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_7

    :catchall_3
    move-exception v0

    goto :goto_7

    .line 302
    :catch_6
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_5

    .line 299
    :catch_8
    move-exception v1

    move-object v1, v4

    goto :goto_3

    :catch_9
    move-exception v0

    move-object v0, v3

    move-object v1, v4

    goto :goto_3
.end method

.method public static declared-synchronized h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 504
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    const-string v0, "ProfileStorage"

    const-string v2, "isEnrolled"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 508
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 514
    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return v0

    .line 508
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 509
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 510
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    const/4 v0, 0x1

    goto :goto_0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized i(Landroid/content/Context;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 319
    const-class v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v4

    :try_start_0
    const-string v2, "ProfileStorage"

    const-string v3, "Enter unloadList"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    const-string v2, "ProfileStorage"

    const-string v3, "UnLoad List: empty"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    :cond_0
    :try_start_1
    const-string v2, "profile.dat"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 329
    :try_start_2
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 331
    :try_start_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 332
    const-string v5, "ProfileStorage"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UnLoad List: nitems="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 334
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v0

    if-nez v0, :cond_3

    .line 341
    if-eqz v2, :cond_1

    .line 342
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 343
    :cond_1
    if-eqz v3, :cond_2

    .line 344
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_1
    move v0, v1

    .line 349
    :goto_2
    monitor-exit v4

    return v0

    .line 334
    :cond_3
    :try_start_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 335
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_0

    .line 336
    :catch_0
    move-exception v0

    move-object v0, v2

    move-object v1, v3

    .line 341
    :goto_3
    if-eqz v0, :cond_4

    .line 342
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V

    .line 343
    :cond_4
    if-eqz v1, :cond_5

    .line 344
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 338
    :cond_5
    :goto_4
    const/4 v0, -0x1

    goto :goto_2

    .line 345
    :catch_1
    move-exception v0

    .line 346
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 339
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    .line 341
    :goto_5
    if-eqz v2, :cond_6

    .line 342
    :try_start_8
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 343
    :cond_6
    if-eqz v3, :cond_7

    .line 344
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 348
    :cond_7
    :goto_6
    :try_start_9
    throw v0

    .line 345
    :catch_2
    move-exception v1

    .line 346
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 345
    :catch_3
    move-exception v0

    .line 346
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 339
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catchall_3
    move-exception v0

    goto :goto_5

    .line 336
    :catch_4
    move-exception v1

    move-object v1, v0

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v1, v3

    goto :goto_3
.end method

.method public static declared-synchronized o(Landroid/content/Context;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;)I

    .line 354
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->qt:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
