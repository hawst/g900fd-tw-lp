.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;
.super Ljava/lang/Object;
.source "LookupServiceError.java"


# instance fields
.field private error_code:I

.field private error_description:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getError_code()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->error_code:I

    return v0
.end method

.method public getError_description()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->error_description:Ljava/lang/String;

    return-object v0
.end method

.method public setError_code(I)V
    .locals 0

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->error_code:I

    .line 23
    return-void
.end method

.method public setError_description(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;->error_description:Ljava/lang/String;

    .line 31
    return-void
.end method
