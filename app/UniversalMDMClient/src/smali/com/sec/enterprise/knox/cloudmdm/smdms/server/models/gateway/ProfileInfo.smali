.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;
.super Ljava/lang/Object;
.source "ProfileInfo.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private authentication:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;

.field private bootstrap:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Bootstrap;

.field private description:Ljava/lang/String;

.field private enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

.field private eulas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;"
        }
    .end annotation
.end field

.field private href:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

.field private name:Ljava/lang/String;

.field private notification:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

.field private profileIcon:[B

.field private tenant:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Tenant;

.field private tenantId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthentication()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->authentication:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;

    return-object v0
.end method

.method public getBootstrap()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Bootstrap;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->bootstrap:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Bootstrap;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    return-object v0
.end method

.method public getEulas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->eulas:Ljava/util/List;

    return-object v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    const-string v0, ""

    .line 250
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->getDomain()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->getDomain()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;->getGroup()Ljava/lang/String;

    move-result-object v0

    .line 253
    :cond_0
    return-object v0
.end method

.method public getHref()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->href:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    return-object v0
.end method

.method public getMdmUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 259
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getService()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getService()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;->getHref()Ljava/lang/String;

    move-result-object v0

    .line 262
    :cond_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->notification:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    return-object v0
.end method

.method public getProfileIcon()[B
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->profileIcon:[B

    return-object v0
.end method

.method public getTenant()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Tenant;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->tenant:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Tenant;

    return-object v0
.end method

.method public getTenantId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->tenantId:Ljava/lang/String;

    return-object v0
.end method

.method public setAuthentication(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->authentication:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;

    .line 229
    return-void
.end method

.method public setBootstrap(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Bootstrap;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->bootstrap:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Bootstrap;

    .line 246
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->description:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setEnrollment(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    .line 209
    return-void
.end method

.method public setEulas(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->eulas:Ljava/util/List;

    .line 273
    return-void
.end method

.method public setHref(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->href:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->id:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public setManagement(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    .line 219
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->name:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setNotification(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->notification:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    .line 238
    return-void
.end method

.method public setProfileIcon([B)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->profileIcon:[B

    .line 283
    return-void
.end method

.method public setTenant(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Tenant;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->tenant:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Tenant;

    .line 159
    return-void
.end method

.method public setTenantId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->tenantId:Ljava/lang/String;

    .line 199
    return-void
.end method
