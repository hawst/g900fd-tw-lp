.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;
.super Lcom/sec/enterprise/knox/IntegrityResultSubscriber;
.source "ISLPolicyHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:ISLSubscriber"


# instance fields
.field islBindListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;

.field islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

.field islRunTimeAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

.field islTimaAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

.field violations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/sec/enterprise/knox/IntegrityResultSubscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(IILjava/lang/String;I)V
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    if-nez v0, :cond_0

    .line 203
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "IntegrityResultSubscriber.onError - No Listener"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :goto_0
    return-void

    .line 207
    :cond_0
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "onError is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 210
    const-string v1, "version"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    const-string v1, "scanType"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    const-string v1, "packageName"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    const-string v1, "opType"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    const-string v2, "IntegrityResultSubscriber.onError"

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->onAPIResult(Ljava/lang/String;Ljava/util/Map;)V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 2

    .prologue
    .line 255
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "onProgress Notification Ignored"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    return-void
.end method

.method public onReady()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islBindListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islBindListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islBindListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;->schedule(Ljava/lang/Runnable;)V

    .line 196
    :cond_0
    return-void
.end method

.method public onRuntimeViolation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islRunTimeAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    if-nez v0, :cond_0

    .line 261
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "IntegrityResultSubscriber.onRuntimeViolation - No Listener"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :goto_0
    return-void

    .line 265
    :cond_0
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "onRuntimeViolation is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 267
    const-string v1, "event"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    const-string v1, "path"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islRunTimeAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    const-string v2, "IntegrityResultSubscriber.onRuntimeViolation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->OnAlert(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onSuccess(I)V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    if-nez v0, :cond_0

    .line 222
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "IntegrityResultSubscriber.onSuccess - No Listener"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :goto_0
    return-void

    .line 226
    :cond_0
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "onSuccess is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 228
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 229
    const-string v1, "opType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 231
    const-string v1, "violations"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    const-string v2, "IntegrityResultSubscriber.onSuccess"

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;->onAPIResult(Ljava/lang/String;Ljava/util/Map;)V

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    goto :goto_0
.end method

.method public onTimaViolation(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islTimaAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    if-nez v0, :cond_0

    .line 276
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "IntegrityResultSubscriber.onTimaViolation - No Listener"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :goto_0
    return-void

    .line 280
    :cond_0
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "onTimaViolation is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 283
    const-string v1, "violation"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islTimaAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    const-string v2, "IntegrityResultSubscriber.onTimaViolation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->OnAlert(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onViolation(IILjava/lang/String;[B[BI)V
    .locals 3

    .prologue
    .line 240
    const-string v0, "UMC:ISLSubscriber"

    const-string v1, "onViolation is being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 242
    const-string v1, "version"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const-string v1, "scanType"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string v1, "packageName"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    const-string v1, "baselineFingerPrint"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    const-string v1, "currentFingerPrint"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    const-string v1, "opType"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    if-nez v1, :cond_0

    .line 250
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    return-void
.end method

.method public setISLBindListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islBindListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$ISLBindListener;

    .line 177
    return-void
.end method

.method public setISLMethodInvoker(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;)V
    .locals 1

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    .line 189
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islMethodInvoker:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLMethodInvoker;

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->violations:Ljava/util/List;

    .line 191
    :cond_0
    return-void
.end method

.method public setISLRuntimeAlertHandler(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islRunTimeAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    .line 181
    return-void
.end method

.method public setISLTimaAlertHandler(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->islTimaAlertHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;

    .line 185
    return-void
.end method
