.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;
.super Ljava/lang/Object;
.source "MethodSignature.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:MethodSignature"


# instance fields
.field public apiResultCallbacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public apiResultError:Ljava/lang/String;

.field public apiResultIntents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private isStatic:Ljava/lang/Boolean;

.field public method:Ljava/lang/String;

.field public params:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;",
            ">;"
        }
    .end annotation
.end field

.field public postApiActiveCallbacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public postApiActiveIntents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public postApiActiveRunTimeCallbacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public postApiActiveRunTimeIntents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public postApiInActiveCallbacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public postApiInActiveIntents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public response:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scope:Ljava/lang/String;

.field private specialParams:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFirstParam(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;
    .locals 2

    .prologue
    .line 280
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;->getPolicyMethodSignature(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    .line 281
    if-nez v0, :cond_0

    .line 282
    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;

    goto :goto_0
.end method

.method public static getParamType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 288
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TypeConfig;->getPolicyMethodSignature(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v3

    .line 289
    if-nez v3, :cond_0

    move-object v0, v2

    .line 297
    :goto_0
    return-object v0

    .line 292
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    .line 297
    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;->type:Ljava/lang/String;

    goto :goto_0

    .line 292
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public getAPIResultErrorRange()[Ljava/lang/Comparable;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 263
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->apiResultError:Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 276
    :goto_0
    return-object v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->response:Ljava/util/Map;

    const-string v2, "retval"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 267
    if-nez v0, :cond_1

    move-object v0, v1

    .line 268
    goto :goto_0

    .line 271
    :cond_1
    :try_start_0
    new-instance v2, Lcom/google/gson/g;

    invoke-direct {v2}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v2}, Lcom/google/gson/g;->dK()Lcom/google/gson/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/g;->dL()Lcom/google/gson/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/g;->dM()Lcom/google/gson/e;

    move-result-object v2

    .line 272
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->apiResultError:Ljava/lang/String;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->get()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "[]"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JavaTypeMap;->getBasicJavaType(Ljava/lang/String;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/gson/e;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Comparable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 273
    :catch_0
    move-exception v0

    .line 274
    const-string v2, "UMC:MethodSignature"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 276
    goto :goto_0
.end method

.method public getDownloadUrlParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    const-string v0, "Download"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getUrlParams(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getNewParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    const-string v0, "AddArgToJson"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialParams(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getNoDownloadUrlParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    const-string v0, "NoDownload"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getUrlParams(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getParamType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, v2

    .line 85
    :goto_0
    return-object v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    .line 85
    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->params:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature$ParamConf;->type:Ljava/lang/String;

    goto :goto_0

    .line 81
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public getParamsRemovedInJson()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 153
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 169
    :goto_0
    return-object v0

    .line 156
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 157
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 166
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v2

    .line 167
    goto :goto_0

    .line 158
    :cond_2
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->collanSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 159
    aget-object v5, v4, v1

    .line 160
    const/4 v6, 0x1

    aget-object v4, v4, v6

    .line 161
    const-string v6, "RemoveArgInJson"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 162
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 169
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getSpecialParams(Ljava/lang/String;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 210
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 227
    :goto_0
    return-object v0

    .line 213
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move v0, v1

    .line 214
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 224
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v2

    .line 225
    goto :goto_0

    .line 215
    :cond_2
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->collanSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 216
    aget-object v5, v4, v1

    .line 217
    const/4 v6, 0x1

    aget-object v6, v4, v6

    .line 218
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 219
    const/4 v6, 0x2

    aget-object v4, v4, v6

    .line 220
    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move-object v0, v3

    .line 227
    goto :goto_0
.end method

.method public getSpecialRequestParamType(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 235
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 247
    :goto_0
    return-object v0

    :cond_1
    move v0, v1

    .line 238
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_2

    move-object v0, v2

    .line 247
    goto :goto_0

    .line 239
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->collanSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 240
    aget-object v4, v3, v1

    .line 241
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    array-length v4, v3

    if-le v4, v5, :cond_3

    .line 242
    aget-object v0, v3, v5

    goto :goto_0

    .line 238
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getSpecialResponseParamType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialRequestParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpecialReturnType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    const-string v0, "retval"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialRequestParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUploadUrlParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    const-string v0, "Upload"

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getUrlParams(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getUrlParams(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 174
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 194
    :goto_0
    return-object v0

    .line 177
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move v0, v1

    .line 178
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 191
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v2

    .line 192
    goto :goto_0

    .line 179
    :cond_2
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->collanSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 180
    aget-object v5, v4, v1

    .line 181
    const/4 v6, 0x1

    aget-object v6, v4, v6

    .line 182
    const-string v7, "URLArg"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 183
    const/4 v6, 0x2

    aget-object v6, v4, v6

    .line 184
    const/4 v7, 0x3

    aget-object v4, v4, v7

    .line 185
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 186
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move-object v0, v3

    .line 194
    goto :goto_0
.end method

.method public isDownloadURLArg(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 145
    const-string v0, "Download"

    invoke-virtual {p0, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isUrlArgPresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isNoDownloadURLArg(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 141
    const-string v0, "NoDownload"

    invoke-virtual {p0, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isUrlArgPresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isParamRemovedInJson(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getParamsRemovedInJson()[Ljava/lang/String;

    move-result-object v2

    .line 104
    if-nez v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 107
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 108
    aget-object v3, v2, v0

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 109
    const/4 v1, 0x1

    goto :goto_0

    .line 107
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public isSpecialArg(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 92
    :goto_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->specialParams:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Utils;->collanSeparatedToArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 94
    aget-object v2, v2, v1

    .line 95
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    const/4 v1, 0x1

    goto :goto_0

    .line 92
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public isSpecialArgPresent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 129
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialParams(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 130
    if-nez v1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v0

    .line 133
    :cond_1
    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isStatic()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isStatic:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isStatic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUploadURLArg(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 149
    const-string v0, "Upload"

    invoke-virtual {p0, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isUrlArgPresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUrlArgPresent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getUrlParams(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 118
    if-nez v1, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 121
    :cond_1
    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    const/4 v0, 0x1

    goto :goto_0
.end method
