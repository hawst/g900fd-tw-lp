.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;
.super Landroid/content/BroadcastReceiver;
.source "ByodSetupStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Ljava/util/ArrayList;II)V
.end annotation


# instance fields
.field final synthetic qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    .line 475
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 479
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->getResultCode()I

    move-result v0

    .line 480
    const-string v1, "UMC:ByodSetupStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Result Code : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string v0, "UMC:ByodSetupStarter"

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 482
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 483
    const-string v1, "knox-b2b"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 484
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 485
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 486
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 487
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 488
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 489
    return-void
.end method
