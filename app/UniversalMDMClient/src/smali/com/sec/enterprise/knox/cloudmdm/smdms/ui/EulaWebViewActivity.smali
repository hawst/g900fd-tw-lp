.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;
.super Landroid/app/Activity;
.source "EulaWebViewActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private yD:Ljava/lang/String;

.field private yI:Landroid/webkit/WebView;

.field private yJ:Ljava/lang/String;

.field private yw:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(Landroid/webkit/WebView;)F
    .locals 3

    .prologue
    .line 457
    invoke-virtual {p1}, Landroid/webkit/WebView;->getTop()I

    move-result v0

    int-to-float v0, v0

    .line 458
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/webkit/WebView;->getScale()F

    move-result v2

    mul-float/2addr v1, v2

    .line 459
    invoke-virtual {p1}, Landroid/webkit/WebView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    .line 460
    sub-float v0, v2, v0

    div-float/2addr v0, v1

    .line 461
    return v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->iM()V

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->iO()V

    return-void
.end method

.method private bj(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 269
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;

    const-string v1, "A url is loaded"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 270
    :catch_0
    move-exception v0

    .line 272
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    return-object v0
.end method

.method private iM()V
    .locals 3

    .prologue
    .line 295
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->iO()V

    .line 296
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 297
    const v1, 0x7f080099

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 298
    const v1, 0x7f08009e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 299
    const v1, 0x7f080058

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 307
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$4;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 315
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 316
    return-void
.end method

.method private iN()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 135
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yw:Landroid/app/ProgressDialog;

    .line 136
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yw:Landroid/app/ProgressDialog;

    const v1, 0x7f08006c

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yw:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 145
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 146
    const v0, 0x7f03000b

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->setContentView(I)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 148
    const-string v0, "bundle.euladata"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    .line 150
    const-string v0, "bundle.activitySubtitle"

    .line 151
    const v2, 0x7f080082

    .line 150
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    .line 157
    const v0, 0x7f09001e

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    .line 160
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 162
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 209
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->bj(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getLocale()Ljava/lang/String;

    move-result-object v0

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".html"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".html"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 215
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/default.html"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 217
    const-string v5, "bundle.eulaLocale"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 218
    if-eqz v5, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    const-string v0, "bundle.eulaContent"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 221
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 222
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->iO()V

    .line 244
    :goto_1
    return-void

    .line 160
    :cond_0
    const/high16 v0, -0x1000000

    goto/16 :goto_0

    .line 227
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;Landroid/webkit/WebView;)V

    .line 228
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    .line 229
    aput-object v2, v1, v6

    .line 230
    aput-object v3, v1, v7

    .line 231
    aput-object v4, v1, v8

    .line 233
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    .line 232
    invoke-static {p0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 234
    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 236
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a url, loaded : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yD:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->x(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    if-nez v0, :cond_3

    .line 240
    const-string v0, ""

    .line 241
    :cond_3
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {p0, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 242
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->iO()V

    goto :goto_1
.end method

.method private iO()V
    .locals 1

    .prologue
    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yw:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 250
    :catch_0
    move-exception v0

    .line 251
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 256
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const-string v0, " EMM"

    const-string v2, ""

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 259
    :goto_0
    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yJ:Ljava/lang/String;

    .line 260
    const-string v3, "text/html"

    .line 261
    const-string v4, "utf-8"

    .line 262
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    move-object v0, p1

    move-object v5, v1

    .line 263
    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    return-void

    :cond_0
    move-object v2, p2

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    .line 435
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 437
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;)F

    move-result v0

    .line 439
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yJ:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 441
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;

    invoke-direct {v2, p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;F)V

    .line 451
    const-wide/16 v4, 0x12c

    .line 441
    invoke-virtual {v1, v2, v4, v5}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 453
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 92
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 98
    const-string v1, "bundle.activityTitle"

    .line 99
    const v2, 0x7f080082

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    const-string v1, " EMM"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 104
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->iN()V

    .line 105
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 413
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 414
    packed-switch p1, :pswitch_data_0

    .line 430
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 416
    :pswitch_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 417
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 418
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yJ:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 427
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 425
    :cond_2
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->finish()V

    goto :goto_1

    .line 414
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yI:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->yJ:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 291
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 289
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->finish()V

    goto :goto_0
.end method
