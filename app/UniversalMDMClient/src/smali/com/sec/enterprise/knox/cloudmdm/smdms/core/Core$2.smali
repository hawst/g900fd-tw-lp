.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;
.super Ljava/lang/Object;
.source "Core.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic pu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

.field private final synthetic pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

.field private final synthetic pw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;->pu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;->pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;->pw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 984
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 5

    .prologue
    .line 989
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateEnrollmentStatus: onNetworkOperationSuccess: operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 990
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 989
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 992
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    .line 993
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;->pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 994
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;->pw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$2;->pw:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v4

    .line 993
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->removePendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    return-void
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 3

    .prologue
    .line 1000
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateEnrollmentStatus: operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1001
    const-string v2, "returnCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1000
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 1003
    return-void
.end method
