.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;
.super Ljava/lang/Object;
.source "Reflect.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/reflect/Method;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->types([Ljava/lang/Object;)[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 73
    invoke-static {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method public static getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/Method;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/reflect/Type;",
            ")",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    .line 51
    if-eqz p2, :cond_4

    .line 52
    :try_start_0
    array-length v2, p2

    new-array v2, v2, [Ljava/lang/Class;

    move v3, v0

    .line 53
    :goto_0
    array-length v4, p2

    if-lt v3, v4, :cond_1

    .line 56
    :goto_1
    invoke-virtual {p0, p1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 68
    :cond_0
    :goto_2
    return-object v0

    .line 54
    :cond_1
    aget-object v4, p2, v3

    invoke-static {v4}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gson/b/a;->getRawType()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 61
    :catch_0
    move-exception v2

    .line 62
    invoke-virtual {p0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    move v2, v0

    :goto_3
    if-lt v2, v4, :cond_2

    move-object v0, v1

    .line 68
    goto :goto_2

    .line 62
    :cond_2
    aget-object v0, v3, v2

    .line 63
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->match([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 62
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method

.method public static getReturnTypeName(Ljava/lang/reflect/Method;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->wrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/b/a;->getRawType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->getTypeName(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTypeName(Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 220
    const-string v1, ""

    .line 221
    instance-of v0, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_3

    .line 222
    check-cast p0, Ljava/lang/reflect/ParameterizedType;

    .line 223
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v3

    .line 224
    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    array-length v2, v3

    if-lt v1, v2, :cond_0

    .line 231
    :goto_1
    return-object v0

    .line 225
    :cond_0
    aget-object v2, v3, v1

    .line 226
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_1

    const-string v0, "<"

    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->wrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/b/a;->getRawType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_2

    const-string v0, ">"

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 224
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 226
    :cond_1
    const-string v0, ","

    goto :goto_2

    .line 228
    :cond_2
    const-string v0, ""

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private static match([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 77
    array-length v0, p0

    array-length v2, p1

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 78
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_1

    .line 86
    const/4 v1, 0x1

    .line 89
    :cond_0
    return v1

    .line 79
    :cond_1
    aget-object v2, p0, v0

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->wrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v2}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v2

    .line 80
    aget-object v3, p1, v0

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->wrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v3}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v3

    .line 82
    invoke-virtual {v2, v3}, Lcom/google/gson/b/a;->isAssignableFrom(Lcom/google/gson/b/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static translateArgObjsToRequiredTypes(Ljava/lang/reflect/Method;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 235
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 237
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    .line 241
    array-length v5, v4

    move v1, v0

    move v2, v0

    :goto_0
    if-lt v1, v5, :cond_0

    .line 301
    return-object v3

    .line 241
    :cond_0
    aget-object v6, v4, v1

    .line 242
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/b/a;->getRawType()Ljava/lang/Class;

    move-result-object v0

    .line 244
    if-eq v6, v0, :cond_1

    .line 245
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 246
    const-class v7, [B

    if-ne v6, v7, :cond_2

    .line 248
    check-cast v0, [Ljava/lang/Byte;

    .line 249
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Byte;)[B

    move-result-object v0

    .line 250
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 299
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 252
    :cond_2
    const-class v7, [I

    if-ne v6, v7, :cond_3

    .line 254
    check-cast v0, [Ljava/lang/Integer;

    .line 255
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Integer;)[I

    move-result-object v0

    .line 256
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 258
    :cond_3
    const-class v7, [F

    if-ne v6, v7, :cond_4

    .line 260
    check-cast v0, [Ljava/lang/Float;

    .line 261
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Float;)[F

    move-result-object v0

    .line 262
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 264
    :cond_4
    const-class v7, [Z

    if-ne v6, v7, :cond_5

    .line 266
    check-cast v0, [Ljava/lang/Boolean;

    .line 267
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Boolean;)[Z

    move-result-object v0

    .line 268
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 270
    :cond_5
    const-class v7, [J

    if-ne v6, v7, :cond_6

    .line 272
    check-cast v0, [Ljava/lang/Long;

    .line 273
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Long;)[J

    move-result-object v0

    .line 274
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 276
    :cond_6
    const-class v7, [C

    if-ne v6, v7, :cond_7

    .line 278
    check-cast v0, [Ljava/lang/Character;

    .line 279
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Character;)[C

    move-result-object v0

    .line 280
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 282
    :cond_7
    const-class v7, [S

    if-ne v6, v7, :cond_8

    .line 284
    check-cast v0, [Ljava/lang/Short;

    .line 285
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Short;)[S

    move-result-object v0

    .line 286
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 288
    :cond_8
    const-class v7, [D

    if-ne v6, v7, :cond_1

    .line 290
    check-cast v0, [Ljava/lang/Double;

    .line 291
    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Double;)[D

    move-result-object v0

    .line 292
    invoke-interface {v3, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static varargs types([Ljava/lang/Object;)[Ljava/lang/reflect/Type;
    .locals 3

    .prologue
    .line 202
    if-nez p0, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 211
    :goto_0
    return-object v0

    .line 206
    :cond_0
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/reflect/Type;

    .line 208
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-lt v0, v2, :cond_1

    move-object v0, v1

    .line 211
    goto :goto_0

    .line 209
    :cond_1
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v1, v0

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static unwrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 148
    const-class v0, Ljava/lang/Boolean;

    if-ne v0, p0, :cond_1

    .line 149
    sget-object p0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    .line 197
    :cond_0
    :goto_0
    return-object p0

    .line 151
    :cond_1
    const-class v0, Ljava/lang/Integer;

    if-ne v0, p0, :cond_2

    .line 152
    sget-object p0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 154
    :cond_2
    const-class v0, Ljava/lang/Long;

    if-ne v0, p0, :cond_3

    .line 155
    sget-object p0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 157
    :cond_3
    const-class v0, Ljava/lang/Short;

    if-ne v0, p0, :cond_4

    .line 158
    sget-object p0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 160
    :cond_4
    const-class v0, Ljava/lang/Byte;

    if-ne v0, p0, :cond_5

    .line 161
    sget-object p0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 163
    :cond_5
    const-class v0, Ljava/lang/Double;

    if-ne v0, p0, :cond_6

    .line 164
    sget-object p0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 166
    :cond_6
    const-class v0, Ljava/lang/Float;

    if-ne v0, p0, :cond_7

    .line 167
    sget-object p0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 169
    :cond_7
    const-class v0, Ljava/lang/Character;

    if-ne v0, p0, :cond_8

    .line 170
    sget-object p0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 172
    :cond_8
    const-class v0, [Ljava/lang/Boolean;

    if-ne v0, p0, :cond_9

    .line 173
    const-class p0, [Z

    goto :goto_0

    .line 175
    :cond_9
    const-class v0, [Ljava/lang/Integer;

    if-ne v0, p0, :cond_a

    .line 176
    const-class p0, [I

    goto :goto_0

    .line 178
    :cond_a
    const-class v0, [Ljava/lang/Long;

    if-ne v0, p0, :cond_b

    .line 179
    const-class p0, [J

    goto :goto_0

    .line 181
    :cond_b
    const-class v0, [Ljava/lang/Short;

    if-ne v0, p0, :cond_c

    .line 182
    const-class p0, [S

    goto :goto_0

    .line 184
    :cond_c
    const-class v0, [Ljava/lang/Byte;

    if-ne v0, p0, :cond_d

    .line 185
    const-class p0, [B

    goto :goto_0

    .line 187
    :cond_d
    const-class v0, [Ljava/lang/Double;

    if-ne v0, p0, :cond_e

    .line 188
    const-class p0, [D

    goto :goto_0

    .line 190
    :cond_e
    const-class v0, [Ljava/lang/Float;

    if-ne v0, p0, :cond_f

    .line 191
    const-class p0, [F

    goto :goto_0

    .line 193
    :cond_f
    const-class v0, [Ljava/lang/Character;

    if-ne v0, p0, :cond_0

    .line 194
    const-class p0, [C

    goto :goto_0
.end method

.method public static wrapPrimitives(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_1

    .line 95
    const-class p0, Ljava/lang/Boolean;

    .line 143
    :cond_0
    :goto_0
    return-object p0

    .line 97
    :cond_1
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_2

    .line 98
    const-class p0, Ljava/lang/Integer;

    goto :goto_0

    .line 100
    :cond_2
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_3

    .line 101
    const-class p0, Ljava/lang/Long;

    goto :goto_0

    .line 103
    :cond_3
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_4

    .line 104
    const-class p0, Ljava/lang/Short;

    goto :goto_0

    .line 106
    :cond_4
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_5

    .line 107
    const-class p0, Ljava/lang/Byte;

    goto :goto_0

    .line 109
    :cond_5
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_6

    .line 110
    const-class p0, Ljava/lang/Double;

    goto :goto_0

    .line 112
    :cond_6
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_7

    .line 113
    const-class p0, Ljava/lang/Float;

    goto :goto_0

    .line 115
    :cond_7
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    if-ne v0, p0, :cond_8

    .line 116
    const-class p0, Ljava/lang/Character;

    goto :goto_0

    .line 118
    :cond_8
    const-class v0, [Z

    if-ne v0, p0, :cond_9

    .line 119
    const-class p0, [Ljava/lang/Boolean;

    goto :goto_0

    .line 121
    :cond_9
    const-class v0, [I

    if-ne v0, p0, :cond_a

    .line 122
    const-class p0, [Ljava/lang/Integer;

    goto :goto_0

    .line 124
    :cond_a
    const-class v0, [J

    if-ne v0, p0, :cond_b

    .line 125
    const-class p0, [Ljava/lang/Long;

    goto :goto_0

    .line 127
    :cond_b
    const-class v0, [S

    if-ne v0, p0, :cond_c

    .line 128
    const-class p0, [Ljava/lang/Short;

    goto :goto_0

    .line 130
    :cond_c
    const-class v0, [B

    if-ne v0, p0, :cond_d

    .line 131
    const-class p0, [Ljava/lang/Byte;

    goto :goto_0

    .line 133
    :cond_d
    const-class v0, [D

    if-ne v0, p0, :cond_e

    .line 134
    const-class p0, [Ljava/lang/Double;

    goto :goto_0

    .line 136
    :cond_e
    const-class v0, [F

    if-ne v0, p0, :cond_f

    .line 137
    const-class p0, [Ljava/lang/Float;

    goto :goto_0

    .line 139
    :cond_f
    const-class v0, [C

    if-ne v0, p0, :cond_0

    .line 140
    const-class p0, [Ljava/lang/Character;

    goto :goto_0
.end method
