.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/security/b;
.super Ljava/lang/Object;
.source "CustomKeyManager.java"

# interfaces
.implements Ljavax/net/ssl/X509KeyManager;


# instance fields
.field private vp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const-string v0, "Digital Signature"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/b;->vp:Ljava/lang/String;

    .line 28
    const-string v0, "Digital Signature"

    return-object v0
.end method

.method public chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 4

    .prologue
    .line 39
    const/4 v1, 0x0

    .line 41
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hN()[Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 43
    :try_start_1
    const-string v1, "UMC:CustomKeyManager"

    const-string v2, "Cert chain received!"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    .line 52
    :goto_0
    return-object v0

    .line 44
    :catch_0
    move-exception v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 46
    :goto_1
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0

    .line 47
    :catch_1
    move-exception v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 49
    :goto_2
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 47
    :catch_2
    move-exception v1

    goto :goto_2

    .line 44
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 58
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/b;->vp:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 57
    return-object v0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 2

    .prologue
    .line 64
    const-string v0, "secpkcs11"

    invoke-static {v0}, Lcom/android/org/conscrypt/OpenSSLEngine;->getInstance(Ljava/lang/String;)Lcom/android/org/conscrypt/OpenSSLEngine;

    move-result-object v0

    .line 66
    :try_start_0
    const-string v1, "Digital Signature"

    invoke-virtual {v0, v1}, Lcom/android/org/conscrypt/OpenSSLEngine;->getPrivateKeyById(Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    .line 68
    :catch_0
    move-exception v0

    .line 70
    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 73
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method
