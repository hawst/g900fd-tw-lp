.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;
.super Ljava/lang/Object;
.source "ContentTransferManager.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

.field private vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

.field private vC:Ljava/lang/String;

.field private vD:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private vE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->TAG:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vD:Ljava/util/Map;

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mHeaders:Ljava/util/Map;

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vE:Ljava/util/List;

    .line 28
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;-><init>(Landroid/content/Context;JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    .line 29
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    invoke-direct {v0, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;-><init>(JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    .line 30
    if-eqz p1, :cond_0

    if-nez p4, :cond_1

    .line 31
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->TAG:Ljava/lang/String;

    const-string v1, "ContentTransferManager: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 34
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_2

    .line 35
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->TAG:Ljava/lang/String;

    const-string v1, "job id cannot be less than 0"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    :cond_2
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vC:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public aN(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->aN(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 46
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->TAG:Ljava/lang/String;

    const-string v1, "addHeader: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mHeaders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 54
    const-string v0, "ftp"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ftps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vD:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mHeaders:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 56
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    invoke-virtual {v0, v3, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->o(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vC:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->download(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :goto_0
    return-void

    .line 58
    :cond_1
    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "https"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vD:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mHeaders:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 60
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->download(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_3
    new-instance v0, Ljava/net/MalformedURLException;

    invoke-direct {v0}, Ljava/net/MalformedURLException;-><init>()V

    throw v0
.end method

.method public m(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 80
    const-string v0, "ftp"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ftps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vD:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mHeaders:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 82
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    invoke-virtual {v0, v3, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->o(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->m(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :goto_0
    return-void

    .line 84
    :cond_1
    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "https"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vD:Ljava/util/Map;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->mHeaders:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->m(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_3
    new-instance v0, Ljava/net/MalformedURLException;

    invoke-direct {v0}, Ljava/net/MalformedURLException;-><init>()V

    throw v0
.end method

.method public n(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->vA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->n(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method
