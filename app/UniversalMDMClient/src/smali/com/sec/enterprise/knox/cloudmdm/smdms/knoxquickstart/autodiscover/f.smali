.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;
.super Ljava/lang/Object;
.source "AutoDiscoverHandler.java"


# direct methods
.method private static a(Lorg/apache/http/HttpResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lorg/apache/http/client/methods/HttpPost;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1254
    .line 1263
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 1265
    if-eqz v0, :cond_6

    .line 1266
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1270
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 1272
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 1274
    const-string v3, "UTF-8"

    invoke-interface {v0, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1276
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 1278
    if-nez v3, :cond_0

    .line 1280
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 1282
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1284
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1286
    const-string v4, "Autodiscover"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1289
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v3

    .line 1291
    invoke-static {v0, v3, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    .line 1296
    iget-object v0, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1308
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "\\"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1311
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1310
    iput-object v0, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    .line 1321
    :goto_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    .line 1325
    const-string v0, "eas"

    iput-object v0, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    .line 1338
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 1340
    const-string v4, "autodiscover_host_auth"

    .line 1338
    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1369
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 1370
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1373
    :cond_1
    :goto_2
    return-object v2

    .line 1315
    :cond_2
    :try_start_2
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1364
    :catch_0
    move-exception v0

    .line 1366
    :goto_3
    :try_start_3
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1369
    if-eqz v1, :cond_1

    .line 1370
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_2

    .line 1342
    :cond_3
    :try_start_4
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1344
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 1345
    const-string v3, "autodiscover_error_code"

    .line 1347
    const/4 v4, 0x5

    .line 1344
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1368
    :catchall_0
    move-exception v0

    .line 1369
    :goto_4
    if-eqz v1, :cond_4

    .line 1370
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1371
    :cond_4
    throw v0

    .line 1350
    :cond_5
    :try_start_5
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Landroid/os/Bundle;

    move-result-object v0

    .line 1351
    const-string v3, "autodiscover_error_code"

    .line 1352
    const/4 v4, 0x0

    .line 1350
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1368
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 1364
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_1
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1468
    :cond_0
    :goto_0
    if-nez p0, :cond_1

    goto :goto_0

    .line 1469
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 1472
    if-ne v0, v4, :cond_3

    .line 1502
    :cond_2
    return-void

    .line 1476
    :cond_3
    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "User"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1480
    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1482
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1484
    if-eqz v0, :cond_5

    const-string v1, "EMailAddress"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1486
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    .line 1488
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Autodiscover, email: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    goto :goto_0

    .line 1490
    :cond_5
    if-eqz v0, :cond_0

    const-string v1, "DisplayName"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1492
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    .line 1494
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Autodiscover, user: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1591
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 1595
    if-ne v0, v4, :cond_2

    .line 1635
    :cond_1
    return-void

    .line 1601
    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1605
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1607
    invoke-static {p2, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    .line 1609
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1611
    const-string v1, "Status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1613
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    .line 1615
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Autodiscover, Error Status : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    goto :goto_0

    .line 1617
    :cond_4
    const-string v1, "Message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1619
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    .line 1621
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Autodiscover, Error Message : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    goto :goto_0

    .line 1623
    :cond_5
    const-string v1, "DebugData"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    .line 1627
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Autodiscover, Error DebugData : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1385
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v1

    .line 1389
    if-ne v1, v0, :cond_2

    .line 1408
    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1395
    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Autodiscover"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1399
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Response"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1401
    invoke-static {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->b(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method static synthetic b(Lorg/apache/http/HttpResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lorg/apache/http/client/methods/HttpPost;
    .locals 1

    .prologue
    .line 1249
    invoke-static {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->a(Lorg/apache/http/HttpResponse;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V
    .locals 3

    .prologue
    .line 1644
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 1648
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1672
    :cond_1
    return-void

    .line 1654
    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Settings"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1658
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1660
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1662
    const-string v1, "Server"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->c(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1419
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 1423
    if-ne v1, v0, :cond_2

    .line 1452
    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 1429
    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Response"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1433
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1435
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1437
    const-string v2, "User"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1439
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    goto :goto_0

    .line 1441
    :cond_4
    const-string v2, "Action"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1443
    invoke-static {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->c(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1
.end method

.method private static c(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1679
    move v0, v1

    .line 1683
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 1687
    if-ne v3, v2, :cond_2

    .line 1759
    :cond_1
    return-void

    .line 1693
    :cond_2
    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Server"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1697
    :cond_3
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1699
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1701
    const-string v4, "Type"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1703
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MobileSync"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    .line 1713
    goto :goto_0

    :cond_4
    if-eqz v0, :cond_0

    const-string v4, "Url"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1715
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 1717
    const-string v3, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Autodiscover, server URL :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1731
    const-string v3, "https://"

    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1732
    const/16 v3, 0x8

    .line 1733
    const/16 v5, 0x1bb

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 1734
    const/4 v5, 0x5

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 1742
    :goto_1
    const-string v5, "/microsoft-server-activesync"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1744
    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 1745
    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    .line 1748
    :goto_2
    new-array v3, v2, [Ljava/lang/String;

    .line 1751
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Autodiscover, hostAuth.mAddress : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/d;->e([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1737
    :cond_5
    const/4 v3, 0x7

    .line 1738
    const/4 v5, -0x1

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 1739
    const/4 v5, 0x4

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_1

    .line 1748
    :cond_6
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    goto :goto_2
.end method

.method private static c(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1512
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 1516
    if-ne v1, v0, :cond_2

    .line 1580
    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 1522
    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Action"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1526
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1528
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1530
    const-string v2, "Error"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1534
    invoke-static {p0, p1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;)V

    goto :goto_0

    .line 1538
    :cond_4
    const-string v2, "Redirect"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1540
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 1542
    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1547
    invoke-static {p2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/h;Ljava/lang/String;)V

    .line 1560
    invoke-static {p3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/e;Z)V

    goto :goto_1

    .line 1571
    :cond_5
    const-string v2, "Settings"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1573
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/f;->b(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    goto :goto_0
.end method
