.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/CustomPolicyIntentReceiver;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentReceiver;
.source "CustomPolicyIntentReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:CustomPolicyIntentReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 14
    const-string v0, "UMC:CustomPolicyIntentReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent received by UMC Action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.intent.action.KNOX_ATTESTATION_RESULT"

    if-ne v0, v1, :cond_0

    .line 16
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 19
    :cond_0
    return-void
.end method
