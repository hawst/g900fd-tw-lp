.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/PushReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PushReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static final a(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 76
    const-string v0, "UMC:PushReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "broadcastToAllUmcs : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.sec.enterprise.knox.cloudmdm.smdms.core.intent.PUSH_MESSAGE"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    const-string v0, "com.sec.enterprise.knox.cloudmdm.smdms.core.extra.PUSH_MESSAGE"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const/4 v1, 0x0

    .line 81
    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 82
    if-eqz v0, :cond_3

    .line 83
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/l;->a(Landroid/os/UserManager;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 86
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 87
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    :cond_1
    return-void

    .line 87
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;

    .line 88
    const-string v3, "UMC:PushReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "broadcastToAllUmcs : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;->id:I

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 90
    const-string v3, "UMC:PushReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "broadcastToAllUmcs : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 92
    const-string v3, "com.sec.enterprise.knox.cloudmdm.smdms.permission.SAMSUNG_MDM_SERVICE"

    .line 91
    invoke-virtual {p0, v2, v0, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 61
    const-string v0, "UMC:PushReceiver"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.core.intent.PUSH_MESSAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "com.sec.enterprise.knox.cloudmdm.smdms.core.extra.PUSH_MESSAGE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 67
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->r(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/a;->d(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
