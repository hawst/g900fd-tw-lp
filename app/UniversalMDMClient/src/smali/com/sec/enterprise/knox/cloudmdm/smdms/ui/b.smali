.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;
.super Landroid/app/Fragment;
.source "EulaPromptFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

.field private yj:Landroid/widget/LinearLayout;

.field private yk:Landroid/widget/LinearLayout;

.field private yl:Landroid/widget/LinearLayout;

.field private ym:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/CheckBox;",
            ">;"
        }
    .end annotation
.end field

.field private yn:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/CheckBox;",
            ">;"
        }
    .end annotation
.end field

.field private yo:[Z

.field private yp:Landroid/widget/CheckBox;

.field private yq:[Ljava/lang/String;

.field private yr:[Ljava/lang/String;

.field private ys:Landroid/widget/LinearLayout;

.field private yt:Landroid/widget/LinearLayout;

.field private yu:Z

.field private yv:Landroid/widget/LinearLayout;

.field private yw:Landroid/app/ProgressDialog;

.field private yx:Landroid/view/LayoutInflater;

.field private yy:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 87
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    .line 88
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yk:Landroid/widget/LinearLayout;

    .line 89
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yl:Landroid/widget/LinearLayout;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    .line 92
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yo:[Z

    .line 93
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    .line 96
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ys:Landroid/widget/LinearLayout;

    .line 97
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yt:Landroid/widget/LinearLayout;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yu:Z

    .line 83
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;ILjava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Landroid/widget/LinearLayout;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 323
    const/4 v1, 0x0

    move v12, v1

    :goto_0
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v12, v1, :cond_0

    .line 405
    return-void

    .line 324
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    .line 325
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x7f03001f

    .line 326
    :goto_1
    const/4 v2, 0x0

    .line 324
    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/LinearLayout;

    .line 327
    const v1, 0x7f09001b

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 328
    const-string v2, "checkOne"

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Landroid/widget/CheckBox;

    .line 329
    if-eqz v1, :cond_1

    .line 330
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 331
    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    :cond_1
    invoke-virtual {v9, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 338
    const v1, 0x7f090083

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 339
    const v2, 0x7f090084

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 340
    if-eqz v12, :cond_8

    .line 341
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 342
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 350
    :goto_2
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v12, v3, :cond_2

    .line 352
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 353
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 357
    :cond_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-boolean v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yB:Z

    if-eqz v1, :cond_9

    .line 358
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yC:Ljava/lang/String;

    invoke-virtual {v2, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    :goto_3
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 364
    const v1, 0x7f090017

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    .line 363
    check-cast v10, Landroid/widget/LinearLayout;

    .line 366
    const v1, 0x7f09001a

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v11, v1

    .line 365
    check-cast v11, Landroid/widget/ImageView;

    .line 367
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;

    .line 368
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v4, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->href:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v5, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    .line 369
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v7, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    move-object v2, p0

    move/from16 v6, p3

    .line 367
    invoke-direct/range {v1 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 370
    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    invoke-virtual {v11, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    invoke-virtual {v1, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/e;->a(Landroid/widget/CheckBox;)V

    .line 373
    const v1, 0x7f090019

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 375
    :try_start_0
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yA:Ljava/lang/String;

    .line 376
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 377
    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->bg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 379
    :cond_4
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\s+"

    const-string v4, " "

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 380
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :cond_5
    :goto_4
    const v1, 0x7f090018

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 389
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 390
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v12, v2, :cond_a

    .line 391
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    :goto_5
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->it()I

    move-result v1

    if-nez v1, :cond_6

    .line 396
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 397
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 398
    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 397
    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 403
    :cond_6
    :goto_6
    invoke-virtual {p2, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 323
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto/16 :goto_0

    .line 326
    :cond_7
    const v1, 0x7f030009

    goto/16 :goto_1

    .line 344
    :cond_8
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 345
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 360
    :cond_9
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 382
    :catch_0
    move-exception v1

    .line 383
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    .line 393
    :cond_a
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 401
    :cond_b
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Z
    .locals 1

    .prologue
    .line 493
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iJ()Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 828
    .line 830
    :try_start_0
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/i;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "KOREA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 831
    if-eqz v0, :cond_0

    .line 832
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getLocale()Ljava/lang/String;

    move-result-object v0

    .line 833
    const-string v1, "ko_KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 835
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/korea_ko.html"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 836
    array-length v1, p2

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 837
    const/4 v2, 0x0

    :try_start_1
    aput-object v0, v1, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 845
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_2
    array-length v3, p2

    invoke-static {p2, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-object p2, v0

    .line 850
    :cond_0
    :goto_1
    return-object p2

    .line 841
    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/korea_en.html"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 842
    array-length v1, p2

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 843
    const/4 v2, 0x0

    :try_start_4
    aput-object v0, v1, v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 847
    :catch_0
    move-exception v0

    .line 848
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 847
    :catch_1
    move-exception v0

    move-object p2, v1

    goto :goto_2

    :catch_2
    move-exception v1

    move-object p2, v0

    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private bf(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 117
    if-nez p1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-object v0

    .line 120
    :cond_1
    const-string v1, "local://KNOXTNC"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080089

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_2
    const-string v1, "local://KLM"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 123
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_3
    const-string v1, "local://ELM"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08008d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private bg(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const v1, 0x7f080090

    .line 131
    if-nez p1, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    .line 134
    :cond_0
    const-string v0, "local://KNOXTNC"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08008a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 136
    :cond_1
    const-string v0, "local://KLM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08008c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 138
    :cond_2
    const-string v0, "local://ELM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 139
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08008e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_3
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private bh(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 638
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getLocale()Ljava/lang/String;

    move-result-object v1

    .line 641
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".html"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 642
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".html"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 643
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/default.html"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 645
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    .line 646
    aput-object v2, v4, v0

    .line 647
    const/4 v2, 0x1

    aput-object v1, v4, v2

    .line 648
    aput-object v3, v4, v5

    .line 650
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 652
    const/4 v1, 0x0

    .line 654
    array-length v4, v3

    move v2, v0

    move-object v0, v1

    :goto_0
    if-lt v2, v4, :cond_1

    .line 669
    :cond_0
    return-object v0

    .line 654
    :cond_1
    aget-object v1, v3, v2

    .line 656
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 657
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->bi(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    move-result-object v0

    .line 658
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 659
    const-string v1, "UMC:EulaPromptFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v10, "Time Taken : "

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v6, v8, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    const-string v1, "UMC:EulaPromptFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Result : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    :cond_2
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 664
    :catch_0
    move-exception v1

    .line 665
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Z
    .locals 1

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iL()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V
    .locals 0

    .prologue
    .line 691
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iM()V

    return-void
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iF()V

    return-void
.end method

.method static synthetic g(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iG()V

    return-void
.end method

.method static synthetic h(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    return-object v0
.end method

.method private iF()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 285
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 294
    return-void

    .line 285
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 286
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 287
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 289
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 290
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 291
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method private iG()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 306
    return-void

    .line 297
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 298
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 299
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 301
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 302
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 303
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method private iJ()Z
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 501
    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 495
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 496
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private iK()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 505
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 506
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 513
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 519
    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 506
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 507
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 508
    goto :goto_0

    .line 513
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 514
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 515
    goto :goto_0
.end method

.method private iL()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 598
    const-string v0, "UMC:EulaPromptFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "downloadEulas"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    if-nez v0, :cond_1

    .line 600
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 634
    :cond_0
    :goto_0
    return v1

    .line 606
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yy:Ljava/util/ArrayList;

    move v0, v1

    .line 608
    :goto_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 634
    const/4 v1, 0x1

    goto :goto_0

    .line 609
    :cond_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->bf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 610
    if-eqz v2, :cond_3

    .line 612
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;)V

    .line 613
    iput-object v2, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    .line 614
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    aget-object v2, v2, v0

    iput-object v2, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->href:Ljava/lang/String;

    .line 615
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 616
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 615
    invoke-static {v2, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->x(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    .line 617
    iput-boolean v1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yB:Z

    .line 618
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yy:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 621
    :cond_3
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->bh(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    move-result-object v2

    .line 623
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 627
    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 628
    :cond_4
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yr:[Ljava/lang/String;

    aget-object v3, v3, v0

    iput-object v3, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    .line 630
    :cond_5
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yy:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private iM()V
    .locals 3

    .prologue
    .line 692
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 695
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 696
    const v1, 0x7f080099

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 697
    const v1, 0x7f08009e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 698
    const v1, 0x7f080058

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 699
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 700
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$2;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 708
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 709
    return-void
.end method


# virtual methods
.method public final bi(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 742
    .line 748
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 749
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 750
    const/16 v1, 0x2710

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 751
    const/16 v1, 0x4e20

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 753
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 754
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 755
    const/16 v3, 0x1fa0

    new-array v3, v3, [C

    .line 757
    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x1fa0

    invoke-virtual {v4, v3, v5, v6}, Ljava/io/InputStreamReader;->read([CII)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_4

    .line 760
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 762
    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 764
    new-instance v1, Lorg/htmlcleaner/m;

    invoke-direct {v1}, Lorg/htmlcleaner/m;-><init>()V

    .line 765
    invoke-virtual {v1}, Lorg/htmlcleaner/m;->ll()Lorg/htmlcleaner/f;

    move-result-object v3

    .line 766
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lorg/htmlcleaner/f;->K(Z)V

    .line 767
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/htmlcleaner/f;->J(Z)V

    .line 768
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/htmlcleaner/f;->H(Z)V

    .line 769
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/htmlcleaner/f;->I(Z)V

    .line 772
    invoke-virtual {v1, v5}, Lorg/htmlcleaner/m;->cd(Ljava/lang/String;)Lorg/htmlcleaner/y;

    move-result-object v6

    .line 774
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;

    const/4 v1, 0x0

    invoke-direct {v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 776
    :try_start_3
    const-string v1, "/head/title"

    invoke-virtual {v6, v1}, Lorg/htmlcleaner/y;->cw(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    .line 778
    array-length v2, v1

    if-lez v2, :cond_0

    .line 780
    const/4 v2, 0x0

    aget-object v1, v1, v2

    check-cast v1, Lorg/htmlcleaner/y;

    .line 782
    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->title:Ljava/lang/String;

    .line 785
    :cond_0
    const-string v1, "/body"

    invoke-virtual {v6, v1}, Lorg/htmlcleaner/y;->cw(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    .line 787
    array-length v2, v1

    if-lez v2, :cond_1

    .line 789
    const/4 v2, 0x0

    aget-object v1, v1, v2

    check-cast v1, Lorg/htmlcleaner/y;

    .line 791
    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yA:Ljava/lang/String;

    .line 793
    :cond_1
    iput-object v5, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->content:Ljava/lang/String;

    .line 794
    iput-object p1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->href:Ljava/lang/String;

    .line 796
    const-string v1, "/head[@id=\'optional\']"

    invoke-virtual {v6, v1}, Lorg/htmlcleaner/y;->cw(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    .line 798
    array-length v2, v1

    if-lez v2, :cond_6

    .line 800
    const/4 v2, 0x0

    aget-object v1, v1, v2

    check-cast v1, Lorg/htmlcleaner/y;

    .line 802
    const-string v2, "content"

    invoke-virtual {v1, v2}, Lorg/htmlcleaner/y;->cv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yC:Ljava/lang/String;

    .line 803
    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yB:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v1, v3

    .line 813
    :goto_1
    if-eqz v0, :cond_2

    .line 814
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 815
    :cond_2
    if-eqz v4, :cond_9

    .line 817
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-object v0, v1

    .line 823
    :cond_3
    :goto_2
    return-object v0

    .line 758
    :cond_4
    const/4 v6, 0x0

    :try_start_5
    invoke-virtual {v1, v3, v6, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_0

    .line 810
    :catch_0
    move-exception v1

    move-object v3, v4

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    .line 811
    :goto_3
    :try_start_6
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GetTitle.GetTitle - error opening or reading URL: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 813
    if-eqz v2, :cond_5

    .line 814
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 815
    :cond_5
    if-eqz v3, :cond_3

    .line 817
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 818
    :catch_1
    move-exception v1

    .line 819
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 805
    :cond_6
    const/4 v1, 0x0

    :try_start_8
    iput-boolean v1, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/c;->yB:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-object v1, v3

    .line 810
    goto :goto_1

    .line 812
    :catchall_0
    move-exception v0

    move-object v4, v2

    .line 813
    :goto_4
    if-eqz v2, :cond_7

    .line 814
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 815
    :cond_7
    if-eqz v4, :cond_8

    .line 817
    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 821
    :cond_8
    :goto_5
    throw v0

    .line 818
    :catch_2
    move-exception v1

    .line 819
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 818
    :catch_3
    move-exception v0

    .line 819
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_9
    move-object v0, v1

    goto :goto_2

    .line 812
    :catchall_1
    move-exception v1

    move-object v4, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v4, v3

    goto :goto_4

    .line 810
    :catch_4
    move-exception v0

    move-object v1, v0

    move-object v3, v2

    move-object v0, v2

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v3, v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto :goto_3

    :catch_6
    move-exception v1

    move-object v2, v0

    move-object v0, v3

    move-object v3, v4

    goto :goto_3

    :cond_a
    move-object v1, v2

    goto :goto_1
.end method

.method public iH()Z
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->K(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public iI()V
    .locals 6

    .prologue
    .line 426
    const v0, 0x7f08009a

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 427
    const v1, 0x7f08009e

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 428
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;-><init>(Landroid/app/Activity;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;I)V

    .line 429
    invoke-virtual {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->bq(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->br(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;

    move-result-object v0

    .line 430
    const v1, 0x7f080058

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->bp(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/c;->show()V

    .line 431
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 409
    const-string v0, "UMC:EulaPromptFragment"

    const-string v1, "onAttach"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 412
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 419
    :goto_0
    return-void

    .line 413
    :catch_0
    move-exception v0

    .line 414
    const-string v1, "UMC:EulaPromptFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 415
    :catch_1
    move-exception v0

    .line 416
    const-string v1, "UMC:EulaPromptFragment"

    const-string v2, "uhoh history manager error"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 524
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 571
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iJ()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 572
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 576
    :goto_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iK()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 577
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 583
    :cond_0
    :goto_1
    return-void

    .line 528
    :sswitch_0
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 529
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    :goto_2
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 539
    :goto_3
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iK()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 540
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 530
    :catch_0
    move-exception v0

    .line 531
    const-string v2, "UMC:EulaPromptFragment"

    const-string v3, "Agree container caused error!"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 537
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_3

    .line 542
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 546
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iJ()Z

    move-result v0

    .line 547
    if-eqz v0, :cond_0

    .line 548
    iput-boolean v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yu:Z

    .line 549
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yo:[Z

    .line 551
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :cond_3
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 557
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    invoke-virtual {v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->B(Z)Z

    goto :goto_1

    .line 551
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 552
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 553
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yo:[Z

    add-int/lit8 v1, v2, 0x1

    aput-boolean v5, v4, v2

    .line 554
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move v2, v1

    goto :goto_4

    .line 563
    :sswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->C(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 564
    :catch_1
    move-exception v0

    .line 565
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 574
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    .line 579
    :cond_6
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 524
    :sswitch_data_0
    .sparse-switch
        0x7f09001b -> :sswitch_0
        0x7f090024 -> :sswitch_2
        0x7f090025 -> :sswitch_2
        0x7f090026 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 146
    const-string v0, "UMC:EulaPromptFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreateView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "UMC:EulaPromptFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreateView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->setRetainInstance(Z)V

    .line 150
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yx:Landroid/view/LayoutInflater;

    .line 152
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f030021

    .line 153
    :goto_0
    const/4 v1, 0x0

    .line 151
    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 154
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const v0, 0x7f090087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 156
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 157
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setLinksClickable(Z)V

    .line 159
    :cond_0
    const v0, 0x7f090021

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yv:Landroid/widget/LinearLayout;

    .line 162
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    const v2, 0x7f08006c

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 165
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_3

    .line 169
    const-string v2, "ui.eula.datas"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yq:[Ljava/lang/String;

    .line 170
    const-string v2, "ui.eula.titles"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yr:[Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ym:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 177
    :cond_2
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/d;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/d;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V

    new-array v2, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 182
    :cond_3
    const v0, 0x7f090026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    .line 183
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 184
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    const v0, 0x7f090025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 188
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 191
    new-instance v2, Landroid/text/SpannableString;

    const v3, 0x7f080053

    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 192
    new-instance v3, Landroid/text/style/UnderlineSpan;

    invoke-direct {v3}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v4

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 193
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    :cond_4
    const v0, 0x7f090024

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yk:Landroid/widget/LinearLayout;

    .line 198
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yk:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 199
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yk:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    const-string v0, "checkall"

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    .line 204
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;)V

    .line 225
    const v0, 0x7f090088

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 224
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yl:Landroid/widget/LinearLayout;

    .line 226
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yl:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 227
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yl:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    const v0, 0x7f090022

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ys:Landroid/widget/LinearLayout;

    .line 232
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ys:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 233
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 234
    const v0, 0x7f090085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yt:Landroid/widget/LinearLayout;

    .line 235
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yt:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 238
    :cond_6
    return-object v1

    .line 153
    :cond_7
    const v0, 0x7f03000c

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 310
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 311
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    const v1, 0x7f080087

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 312
    return-void
.end method

.method public setUp()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 243
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yy:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yx:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yv:Landroid/widget/LinearLayout;

    const v3, 0x7f080093

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yy:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->a(Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;ILjava/util/ArrayList;)V

    .line 247
    :cond_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yu:Z

    if-eqz v0, :cond_2

    .line 248
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iF()V

    .line 249
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yo:[Z

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yn:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 255
    :cond_1
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yu:Z

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yw:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 262
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->ys:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 264
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 265
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yt:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 268
    :cond_4
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iJ()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 269
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 273
    :goto_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->iK()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 274
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 278
    :goto_2
    return-void

    .line 251
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 252
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yo:[Z

    add-int/lit8 v3, v1, 0x1

    aget-boolean v1, v5, v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    move v1, v3

    goto :goto_0

    .line 271
    :cond_6
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yj:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_1

    .line 276
    :cond_7
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/b;->yp:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2
.end method
