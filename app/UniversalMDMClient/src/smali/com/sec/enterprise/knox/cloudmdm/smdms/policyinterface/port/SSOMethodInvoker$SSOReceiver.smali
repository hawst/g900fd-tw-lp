.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;
.super Ljava/lang/Object;
.source "SSOMethodInvoker.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

.field final xreceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;)V
    .locals 3

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;->xreceiver:Landroid/content/BroadcastReceiver;

    .line 100
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mPolicyClass:Ljava/lang/Class;
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 101
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 102
    const-string v2, "android.app.enterprise.SSOPolicy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "sso.enterprise.userspace.setup.success"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    :goto_0
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;->xreceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107
    return-void

    .line 105
    :cond_0
    const-string v0, "sso.enterprise.container.setup.success"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;->INVOKE_STATE:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;

    const/4 v2, 0x1

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->changeState(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker$State;Z)V

    .line 129
    return-void
.end method

.method schedulex()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker$SSOReceiver;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/SSOMethodInvoker;->schedule(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method
