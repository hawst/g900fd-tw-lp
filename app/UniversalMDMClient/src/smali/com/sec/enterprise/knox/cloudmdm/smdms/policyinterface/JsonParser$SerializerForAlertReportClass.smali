.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForAlertReportClass;
.super Ljava/lang/Object;
.source "JsonParser.java"

# interfaces
.implements Lcom/google/gson/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/q",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serialize(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;
    .locals 2

    .prologue
    .line 172
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser;->gGson:Lcom/google/gson/e;

    invoke-virtual {v0, p1}, Lcom/google/gson/e;->i(Ljava/lang/Object;)Lcom/google/gson/l;

    move-result-object v0

    check-cast v0, Lcom/google/gson/n;

    .line 173
    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;->exception:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 174
    const-string v1, "exception"

    invoke-virtual {v0, v1}, Lcom/google/gson/n;->t(Ljava/lang/String;)Lcom/google/gson/l;

    .line 176
    :cond_0
    return-object v0
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;

    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/JsonParser$SerializerForAlertReportClass;->serialize(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;

    move-result-object v0

    return-object v0
.end method
