.class abstract Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;
.super Landroid/os/AsyncTask;
.source "FtpService.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field protected TAG:Ljava/lang/String;

.field protected final ro:J

.field protected server:Ljava/lang/String;

.field protected user:Ljava/lang/String;

.field protected final vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

.field protected vI:Ljava/lang/String;

.field protected vJ:Ljava/lang/String;

.field protected vK:Ljava/lang/String;

.field protected vL:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V
    .locals 5

    .prologue
    .line 297
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 292
    const-string v0, "UMC:FTPOperation"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->TAG:Ljava/lang/String;

    .line 299
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->server:Ljava/lang/String;

    .line 300
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->user:Ljava/lang/String;

    .line 301
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->vI:Ljava/lang/String;

    .line 302
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->vJ:Ljava/lang/String;

    .line 303
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->vK:Ljava/lang/String;

    .line 304
    iput-object p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    .line 305
    iput-wide p7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->ro:J

    .line 306
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->ro:J

    invoke-interface {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onStart(J)V

    .line 307
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 315
    return-void
.end method

.method protected varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->a([Ljava/lang/Void;)V

    return-void
.end method
