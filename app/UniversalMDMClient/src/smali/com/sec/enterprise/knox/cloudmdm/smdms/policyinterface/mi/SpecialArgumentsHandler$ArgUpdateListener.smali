.class interface abstract Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/SpecialArgumentsHandler$ArgUpdateListener;
.super Ljava/lang/Object;
.source "SpecialArgumentsHandler.java"


# virtual methods
.method public abstract lookupObject(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest$ReferencedArg;)Ljava/lang/Object;
.end method

.method public abstract onInputParamError(Ljava/lang/String;Ljava/lang/Exception;Z)V
.end method

.method public abstract onInputParamReady(Ljava/util/Map;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract onOutputParamError(Ljava/lang/String;Ljava/lang/Exception;Z)V
.end method

.method public abstract onOutputParamReady(Z)V
.end method
