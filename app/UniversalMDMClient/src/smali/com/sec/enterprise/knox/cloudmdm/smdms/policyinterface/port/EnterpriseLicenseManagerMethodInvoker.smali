.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "EnterpriseLicenseManagerMethodInvoker.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:EnterpriseLicenseManagerMethodInvoker"


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 59
    return-void
.end method


# virtual methods
.method public onIntentOccurence(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;Landroid/content/Intent;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Exception;",
            "Landroid/content/Intent;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 96
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onIntentOccurence - msg = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, "edm.intent.action.license.status"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    if-eqz p2, :cond_0

    if-eqz p3, :cond_2

    .line 101
    :cond_0
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    const-string v1, "Error : Exception while receiving intent = edm.intent.action.license.status"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    const-string v0, "edm.intent.extra.license.status"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 107
    if-nez v1, :cond_3

    .line 108
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 109
    const-string v1, "EXTRA_LICENSE_STATUS is not mention in the intent and ignoring "

    .line 108
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_3
    check-cast v1, Ljava/lang/String;

    .line 116
    const-string v0, "edm.intent.extra.license.errorcode"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 118
    if-nez v0, :cond_4

    .line 119
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 120
    const-string v1, "EXTRA_LICENSE_ERROR_CODE is not mention in the intent and ignoring "

    .line 119
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_4
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 126
    const-string v0, "edm.intent.extra.license.result_type"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 127
    if-nez v0, :cond_5

    .line 128
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 129
    const-string v1, "EXTRA_LICENSE_RESULT_TYPE is not mention in the intent and ignoring "

    .line 128
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_5
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 134
    const-string v0, "edm.intent.extra.license.data.pkgname"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 136
    if-nez v0, :cond_6

    .line 137
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 138
    const-string v1, "EXTRA_LICENSE_DATA_PACKAGENAME is not mention in the intent and ignoring "

    .line 137
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_6
    check-cast v0, Ljava/lang/String;

    .line 143
    const-string v4, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ELM License result type : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v4, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ELM License Activation Status : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v4, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ELM License Activation ErrorCode : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v4, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ELM License Activation Pkg : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const/16 v4, 0x320

    if-ne v3, v4, :cond_b

    .line 149
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 150
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 151
    const-string v1, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Intent admin pkgName: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " umc admin PkgName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 154
    const-string v1, "Intent admin pkgName differ from umc admin pkgName and ignoring "

    .line 153
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 158
    :cond_7
    const-string v0, "success"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    if-nez v2, :cond_a

    .line 161
    const-string v0, "edm.intent.extra.license.data.license_permissions"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 162
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 164
    const-string v2, "Permissions"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8

    .line 170
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "permissions size : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 172
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 174
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->g([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v2

    .line 177
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mMDMUrl:Ljava/lang/String;

    .line 176
    invoke-virtual {v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->updateAdminPermission(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    .line 178
    const-string v2, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateAdminPermission : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 191
    invoke-super/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->onIntentOccurence(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 167
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 168
    const-string v4, "UMC:EnterpriseLicenseManagerMethodInvoker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "permissions : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 182
    :cond_9
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 183
    const-string v2, "ELM License Activation : No permissions are returned from ELM "

    .line 182
    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 187
    :cond_a
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 188
    const-string v2, "ELM License Activation : status is not success so not updating the admin permission "

    .line 187
    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 192
    :cond_b
    const/16 v0, 0x321

    if-ne v3, v0, :cond_1

    .line 193
    const-string v0, "UMC:EnterpriseLicenseManagerMethodInvoker"

    .line 194
    const-string v1, "Only interested on ACTIVATION but received VALIDATION type and ignoring "

    .line 193
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected preInvoke()V
    .locals 4

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "activateLicense"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const-string v0, "activateLicenseForUMC"

    .line 68
    :cond_0
    if-eqz v0, :cond_1

    .line 69
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgNames:Ljava/util/List;

    const-string v2, "pkgName"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    const-class v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgObjs:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 76
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 81
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mPolicyClass:Ljava/lang/Class;

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/reflect/Type;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    invoke-static {v2, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    .line 82
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    if-nez v0, :cond_4

    .line 83
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Method "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 77
    :cond_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    .line 78
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - null argument during preInvoke (ELM MI)"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 86
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->theMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgTypes:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgObjs:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Reflect;->translateArgObjsToRequiredTypes(Ljava/lang/reflect/Method;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mArgObjs:Ljava/util/List;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/EnterpriseLicenseManagerMethodInvoker;->mPreparedForInvoke:Z

    .line 89
    return-void
.end method
