.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;
.super Landroid/content/BroadcastReceiver;
.source "LicenseManager.java"


# static fields
.field private static mCookie:Ljava/lang/Object;

.field private static mPackageName:Ljava/lang/String;

.field private static qf:Ljava/lang/String;

.field private static qg:Ljava/lang/String;

.field private static qh:Ljava/lang/String;

.field private static qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

.field private static qj:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/g;

.field private static qk:Z

.field private static ql:Z

.field private static qm:Z

.field private static qn:I

.field private static qo:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 49
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qf:Ljava/lang/String;

    .line 50
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qg:Ljava/lang/String;

    .line 51
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    .line 52
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 53
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qh:Ljava/lang/String;

    .line 56
    sput-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qk:Z

    .line 57
    sput-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->ql:Z

    .line 58
    sput-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qm:Z

    .line 61
    sput v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    .line 62
    sput v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 86
    if-eqz p0, :cond_1

    if-eqz p6, :cond_1

    .line 87
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 91
    :cond_2
    sput-object p6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    .line 92
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qf:Ljava/lang/String;

    .line 93
    sput-object p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qg:Ljava/lang/String;

    .line 94
    sput-object p5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 95
    sput-object p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    .line 96
    sput-object p4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qh:Ljava/lang/String;

    .line 98
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 100
    invoke-static {v1, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(ZZZ)V

    .line 102
    invoke-static {p0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qg:Ljava/lang/String;

    .line 103
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->activateLicenseForUMC(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_3
    :goto_0
    return-void

    .line 104
    :cond_4
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 106
    invoke-static {v2, v1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(ZZZ)V

    .line 108
    invoke-static {p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qf:Ljava/lang/String;

    .line 109
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qh:Ljava/lang/String;

    .line 108
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->activateLicenseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(ZZZ)V
    .locals 0

    .prologue
    .line 148
    sput-boolean p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qk:Z

    .line 149
    sput-boolean p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->ql:Z

    .line 150
    sput-boolean p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qm:Z

    .line 151
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 156
    const-string v0, "UMC:LicenseManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v0, "fail"

    .line 158
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->ql:Z

    if-eqz v0, :cond_2

    .line 159
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "edm.intent.action.knox_license.status"

    if-ne v0, v1, :cond_2

    .line 161
    const-string v0, "edm.intent.extra.knox_license.status"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 163
    const-string v0, "edm.intent.extra.knox_license.errorcode"

    .line 164
    const/4 v1, 0x0

    .line 162
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 166
    const-string v1, "UMC:LicenseManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Status : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v1, "UMC:LicenseManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "errorCode : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    .line 170
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(ZZZ)V

    .line 172
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qf:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qh:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 174
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(ZZZ)V

    .line 175
    invoke-static {p1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qf:Ljava/lang/String;

    .line 176
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qh:Ljava/lang/String;

    .line 175
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->activateLicenseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    const/4 v4, 0x0

    .line 179
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 178
    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;->a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 181
    :cond_2
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qk:Z

    if-eqz v0, :cond_6

    .line 182
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "edm.intent.action.license.status"

    if-ne v0, v1, :cond_6

    .line 183
    const-string v0, "edm.intent.extra.license.status"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 184
    const-string v0, "edm.intent.extra.license.errorcode"

    .line 185
    const/4 v1, 0x0

    .line 184
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 187
    const-string v1, "UMC:LicenseManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Status : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v1, "UMC:LicenseManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "errorCode : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    .line 192
    const-string v0, "edm.intent.extra.license.data.license_permissions"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 196
    const-string v1, "Permissions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 198
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 199
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 201
    const-string v0, "UMC:LicenseManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "permissions size : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    .line 203
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    .line 204
    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    .line 205
    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 203
    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;->a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    :goto_2
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    .line 215
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qf:Ljava/lang/String;

    .line 216
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qg:Ljava/lang/String;

    .line 217
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    .line 218
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qh:Ljava/lang/String;

    .line 219
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 220
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    .line 221
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    .line 222
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(ZZZ)V

    goto/16 :goto_0

    .line 199
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    const-string v2, "UMC:LicenseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "permissions : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 207
    :cond_4
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    const/4 v4, 0x0

    .line 208
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 207
    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;->a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 211
    :cond_5
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qi:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qn:I

    const/4 v4, 0x0

    .line 212
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 211
    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;->a(IILjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 225
    :cond_6
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qm:Z

    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "edm.intent.action.knox_license.status"

    if-ne v0, v1, :cond_0

    .line 228
    const-string v0, "edm.intent.extra.knox_license.status"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    const-string v1, "edm.intent.extra.knox_license.errorcode"

    .line 231
    const/4 v2, 0x0

    .line 229
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 233
    const-string v2, "UMC:LicenseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Status : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v2, "UMC:LicenseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "errorCode : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sput v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    .line 237
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(ZZZ)V

    .line 238
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qj:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/g;

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    .line 239
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 238
    invoke-interface {v1, v2, v0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/g;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qj:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/g;

    .line 242
    const/4 v0, -0x1

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->qo:I

    .line 243
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mCookie:Ljava/lang/Object;

    .line 244
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->mPackageName:Ljava/lang/String;

    goto/16 :goto_0
.end method
