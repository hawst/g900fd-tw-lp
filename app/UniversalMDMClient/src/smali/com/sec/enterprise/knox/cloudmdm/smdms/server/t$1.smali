.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;
.source "MDMManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

.field private final synthetic wr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;->wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;->wr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 83
    invoke-direct {p0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    return-void
.end method


# virtual methods
.method public c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 89
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":process response on success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;->wq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;->wr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 91
    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    const-string v1, "Enroll device error"

    invoke-super {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    const-string v0, "Sensitive Info. Intentionally Not Logged"

    invoke-super {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->c(ILjava/lang/String;)V

    goto :goto_0
.end method
