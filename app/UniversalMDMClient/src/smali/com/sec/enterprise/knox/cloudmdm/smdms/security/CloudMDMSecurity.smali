.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;
.super Ljava/lang/Object;
.source "CloudMDMSecurity.java"


# static fields
.field private static mContext:Landroid/content/Context;

.field private static mPassword:Ljava/lang/String;

.field private static vm:Ljavax/net/ssl/SSLContext;

.field private static vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

.field private static vo:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vm:Ljavax/net/ssl/SSLContext;

    .line 51
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->mPassword:Ljava/lang/String;

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->mContext:Landroid/content/Context;

    .line 73
    return-void
.end method

.method public static declared-synchronized D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vo:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    .line 66
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vo:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vo:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static aU(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 418
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 420
    new-array v1, p0, [B

    .line 421
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 423
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/c;->toHexString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 403
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->hL()Ljavax/net/ssl/SSLContext;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    return-object v0
.end method

.method public static hK()V
    .locals 4

    .prologue
    .line 373
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "onBindInitiate called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :try_start_0
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "CCM Profile Flow"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->setCCMProfile(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    :goto_0
    const-string v0, "secpkcs11"

    invoke-static {v0}, Lcom/android/org/conscrypt/NativeCrypto;->ENGINE_by_id(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 382
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->mPassword:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aG(Ljava/lang/String;)Z

    .line 383
    :cond_0
    return-void

    .line 378
    :catch_0
    move-exception v0

    .line 379
    const-string v1, "UMC:CloudMDMSecurity"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized hL()Ljavax/net/ssl/SSLContext;
    .locals 2

    .prologue
    .line 410
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vm:Ljavax/net/ssl/SSLContext;

    if-nez v0, :cond_0

    .line 411
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hL()Ljavax/net/ssl/SSLContext;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vm:Ljavax/net/ssl/SSLContext;

    .line 413
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vm:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static onFinish()V
    .locals 4

    .prologue
    .line 392
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "Deregister Engine being called!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :goto_0
    const-string v0, "secpkcs11"

    invoke-static {v0}, Lcom/android/org/conscrypt/NativeCrypto;->ENGINE_by_id(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 400
    return-void

    .line 397
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hM()V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/security/KeyPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 172
    :try_start_0
    invoke-static {p1, p2, p3, p4, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->a(Ljava/security/KeyPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/spongycastle/operator/OperatorCreationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/spongycastle/operator/OperatorCreationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :catch_1
    move-exception v0

    .line 176
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/security/KeyPair;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 190
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->b(Ljava/lang/String;Ljava/security/KeyPair;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public aE(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 293
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aE(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 297
    :goto_0
    return v0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aF(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 303
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aF(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 307
    :goto_0
    return v0

    .line 304
    :catch_0
    move-exception v0

    .line 305
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 328
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public generateKeyPair()Ljava/security/KeyPair;
    .locals 4

    .prologue
    .line 152
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hO()Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hI()Ljava/security/KeyStore;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hI()Ljava/security/KeyStore;

    move-result-object v0

    return-object v0
.end method

.method public hJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hJ()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public init()Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 77
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hP()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    const-string v1, "UMC:CloudMDMSecurity"

    const-string v2, "TIMA not available"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :goto_0
    return v0

    .line 82
    :cond_0
    const/4 v1, 0x0

    .line 84
    const-string v2, "tima"

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/h;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 83
    invoke-static {v2}, Landroid/service/tima/ITimaService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/tima/ITimaService;

    move-result-object v2

    .line 85
    if-nez v2, :cond_1

    .line 86
    const-string v1, "UMC:CloudMDMSecurity"

    const-string v2, "TIMA Service not found"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_1
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    const-string v3, "RND_TZ_CCM_VAL"

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aL(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 92
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v3, "New Flow"

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x0

    .line 95
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    .line 97
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4, v0, v1}, Landroid/service/tima/ITimaService;->ccmRegisterForDefaultCertificate(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 99
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ccmRegisterForDefaultCertificate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    if-ne v0, v5, :cond_2

    .line 101
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "Already using new password"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->mPassword:Ljava/lang/String;

    .line 129
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 103
    :cond_2
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "TIMA service call for password change success!!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 122
    :catch_0
    move-exception v0

    .line 123
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    .line 107
    :cond_3
    :try_start_1
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v3, "Old Flow"

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    const-string v3, "RND_TZ_CCM_VAL"

    const/16 v4, 0x20

    invoke-virtual {v0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->i(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 111
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 110
    invoke-interface {v2, v3, v4, v0, v1}, Landroid/service/tima/ITimaService;->ccmRegisterForDefaultCertificate(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 112
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ccmRegisterForDefaultCertificate:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    if-ne v0, v5, :cond_4

    .line 114
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "Password already changed to new"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :goto_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    const-string v1, "RND_TZ_CCM_VAL"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->aK(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->mPassword:Ljava/lang/String;

    goto :goto_2

    .line 116
    :cond_4
    const-string v0, "UMC:CloudMDMSecurity"

    const-string v1, "TIMA service call for password change success!!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public l(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 353
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->vn:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;

    invoke-virtual {v0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->l(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result v0

    .line 367
    :goto_0
    return v0

    .line 354
    :catch_0
    move-exception v0

    .line 356
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/KeyStoreException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :goto_1
    const/4 v0, -0x3

    goto :goto_0

    .line 357
    :catch_1
    move-exception v0

    .line 359
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 360
    :catch_2
    move-exception v0

    .line 362
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 363
    :catch_3
    move-exception v0

    .line 365
    const-string v1, "UMC:CloudMDMSecurity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
