.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;
.super Landroid/app/Fragment;
.source "LoginScreenFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;


# instance fields
.field private sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

.field private yQ:Landroid/view/View;

.field private yR:Landroid/view/View;

.field private yS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/EditText;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    return-object v0
.end method

.method private a(Landroid/widget/EditText;I)V
    .locals 1

    .prologue
    .line 267
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;Landroid/widget/EditText;)V

    .line 291
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 292
    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;)Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yQ:Landroid/view/View;

    return-object v0
.end method

.method private iQ()V
    .locals 4

    .prologue
    .line 229
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 230
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->c(Ljava/util/HashMap;)V

    .line 234
    return-void

    .line 230
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 231
    const v3, 0x7f090001

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private iR()Z
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    check-cast v0, Landroid/content/Context;

    .line 238
    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 237
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 239
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 240
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x1

    .line 244
    :goto_0
    return v0

    .line 243
    :cond_0
    const-string v0, "UMC:LoginScreenFragment"

    const-string v1, "Network is not available"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private iS()V
    .locals 3

    .prologue
    .line 249
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 250
    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 249
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 251
    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 254
    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 257
    if-eqz v1, :cond_0

    .line 258
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 261
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public b(ZI)V
    .locals 3

    .prologue
    .line 308
    const-string v0, "UMC:LoginScreenFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "never show: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->a(Landroid/content/Context;Z)Z

    .line 310
    return-void
.end method

.method public bd(I)V
    .locals 2

    .prologue
    .line 296
    const-string v0, "UMC:LoginScreenFragment"

    const-string v1, "Yes occured!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->iQ()V

    .line 298
    return-void
.end method

.method public be(I)V
    .locals 2

    .prologue
    .line 302
    const-string v0, "UMC:LoginScreenFragment"

    const-string v1, "No occured!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->C(Z)V

    .line 304
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 190
    const-string v0, "UMC:LoginScreenFragment"

    const-string v1, "onAttach "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 193
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    const-string v1, "UMC:LoginScreenFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 201
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 202
    packed-switch v0, :pswitch_data_0

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 206
    :pswitch_0
    const-string v0, "UMC:LoginScreenFragment"

    const-string v1, "enroll button clicked"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    .line 208
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    .line 207
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z

    move-result v0

    .line 208
    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->iS()V

    .line 211
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->iR()Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    const v1, 0x7f08009e

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    const v2, 0x7f08009a

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 212
    invoke-interface {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->x(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :cond_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->iQ()V

    goto :goto_0

    .line 220
    :pswitch_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->C(Z)V

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x7f090010
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 81
    const-string v0, "UMC:LoginScreenFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreateView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bundle passed into: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->setRetainInstance(Z)V

    .line 84
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f03001c

    .line 85
    :goto_0
    const/4 v1, 0x0

    .line 83
    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 87
    return-object v0

    .line 85
    :cond_0
    const v0, 0x7f030006

    goto :goto_0
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-super {p0, p1}, Landroid/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getView()Landroid/view/View;

    move-result-object v2

    .line 95
    const v0, 0x7f090011

    :try_start_0
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yQ:Landroid/view/View;

    .line 96
    const v0, 0x7f090010

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yR:Landroid/view/View;

    .line 98
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 99
    if-eqz v5, :cond_1

    .line 101
    const v0, 0x7f09000d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 103
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 104
    const v1, 0x7f080016

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 105
    const v1, 0x7f09000e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 106
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :goto_0
    const-string v1, "btnEnrollText"

    const/4 v4, 0x0

    invoke-virtual {v5, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 114
    if-eqz v4, :cond_0

    .line 117
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 118
    const v1, 0x7f090027

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 125
    :cond_0
    :goto_1
    const v1, 0x7f090081

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 126
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 136
    const-string v2, "ui.uaf"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    .line 137
    array-length v2, v6

    new-array v7, v2, [Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    move v4, v3

    .line 138
    :goto_2
    array-length v2, v6

    if-lt v4, v2, :cond_4

    .line 143
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    .line 144
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 145
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    .line 144
    invoke-static {v2, v7, v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    .line 147
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 151
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fv()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    const-string v2, "email"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 153
    const-string v3, "mdmUrl"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 154
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 155
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/my?username="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 157
    const v3, 0x7f0800ab

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 158
    const v3, 0x7f0800ad

    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 159
    const-string v4, "Forgot password?"

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 161
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 163
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLinksClickable(Z)V

    .line 165
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yQ:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yQ:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yR:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    .line 176
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yS:Ljava/util/ArrayList;

    .line 175
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z

    move-result v0

    .line 176
    if-eqz v0, :cond_6

    .line 177
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yQ:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 186
    :goto_4
    return-void

    .line 108
    :cond_2
    const-string v1, "email"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    const v4, 0x7f080073

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {p0, v4, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 110
    const v1, 0x7f09000e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 111
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 182
    :catch_0
    move-exception v0

    .line 183
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 184
    const-string v0, "UMC:LoginScreenFragment"

    const-string v1, "Error!!! Email Fragment messed up"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 120
    :cond_3
    const v1, 0x7f090011

    :try_start_1
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 139
    :cond_4
    aget-object v2, v6, v4

    check-cast v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    aput-object v2, v7, v4

    .line 138
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 147
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 148
    add-int/lit8 v4, v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->a(Landroid/widget/EditText;I)V

    move v3, v4

    goto/16 :goto_3

    .line 179
    :cond_6
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/j;->yQ:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method
