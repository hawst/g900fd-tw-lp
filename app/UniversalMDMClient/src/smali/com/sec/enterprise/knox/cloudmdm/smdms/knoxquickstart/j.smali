.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;
.source "QuickStartAgentInstaller.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static rv:Ljava/lang/String;

.field private static tq:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;


# instance fields
.field private qC:Ljava/lang/String;

.field private rm:Ljava/lang/String;

.field private rt:Ljava/lang/String;

.field private ru:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    .line 61
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->tq:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    .line 66
    const-string v0, "UMC:QuickStartAgentInstaller"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    .line 69
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rv:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;-><init>(Landroid/content/Context;)V

    .line 56
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rm:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->qC:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    .line 64
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->ru:Ljava/lang/String;

    .line 103
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    .line 104
    sput-object p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->tq:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    .line 105
    return-void
.end method

.method private aS(I)V
    .locals 3

    .prologue
    .line 258
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 259
    const-string v1, "url"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rm:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 261
    const-string v1, "error_description"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rv:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    const-string v1, "packageName"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/c;->a(Landroid/content/Intent;)Z

    .line 264
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InstallMdmAgent status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " broadcast sent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    return-void
.end method

.method private gq()V
    .locals 6

    .prologue
    .line 271
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->K(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->gs()V

    .line 273
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->aS(I)V

    .line 278
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rm:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->qC:Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->a(Ljava/lang/String;Ljava/lang/String;IIZ)V

    goto :goto_0
.end method

.method private gs()V
    .locals 3

    .prologue
    .line 298
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    const v2, 0x7f08009e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 299
    const/4 v2, 0x0

    .line 298
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 300
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 301
    return-void
.end method

.method private t(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->qC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 290
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleted:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 295
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 2

    .prologue
    .line 140
    if-nez p5, :cond_0

    .line 141
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    const-string v1, "showing mdm agent download and install status on notification bar"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->a(Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 149
    return-void
.end method

.method public aH(I)V
    .locals 1

    .prologue
    .line 199
    .line 200
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 201
    const-string v0, "Download error"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rv:Ljava/lang/String;

    .line 209
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->aS(I)V

    .line 210
    invoke-super {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    .line 211
    return-void

    .line 202
    :cond_0
    const/4 v0, -0x2

    if-ne p1, v0, :cond_1

    .line 203
    const-string v0, "Install error"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rv:Ljava/lang/String;

    goto :goto_0

    .line 206
    :cond_1
    const-string v0, "Internal error"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rv:Ljava/lang/String;

    goto :goto_0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 108
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 109
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 112
    :cond_1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rm:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->qC:Ljava/lang/String;

    .line 114
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->ru:Ljava/lang/String;

    .line 129
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->gq()V

    .line 131
    return-void
.end method

.method public fZ()V
    .locals 3

    .prologue
    .line 154
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mMdmUrl :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->ru:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->qC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 157
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_1

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 161
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    .line 162
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mMdmAgentPkgNamee:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->ru:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 168
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->ru:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_2

    .line 171
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    .line 173
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->clearProxyAdminInstallBlock(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPreInstall : Remove Install Block for : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_0
    :goto_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->tq:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gN()V

    .line 184
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->fZ()V

    .line 185
    return-void

    .line 164
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    const-string v1, "unable to get MDM agent package name "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 179
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Admin is null for :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->ru:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public gm()V
    .locals 1

    .prologue
    .line 190
    const-string v0, "success"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rv:Ljava/lang/String;

    .line 192
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->aS(I)V

    .line 193
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->gm()V

    .line 194
    return-void
.end method

.method public onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 2

    .prologue
    .line 248
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    const-string v1, "mdmAgent download fail"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->gs()V

    .line 250
    invoke-super {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 254
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->t(Landroid/content/Context;)V

    .line 255
    return-void
.end method

.method public onProgress(JII)V
    .locals 1

    .prologue
    .line 224
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->tq:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    invoke-virtual {v0, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->d(II)V

    .line 225
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onProgress(JII)V

    .line 226
    return-void
.end method

.method public onStart(J)V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onStart(J)V

    .line 218
    return-void
.end method

.method public onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 3

    .prologue
    .line 230
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    const-string v1, "mdmAgent download success "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 233
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->qC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 234
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 238
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    .line 239
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mMdmAgentPkgNamee:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->rt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 244
    return-void

    .line 241
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/j;->TAG:Ljava/lang/String;

    const-string v1, "unable to get MDM agent package name "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
