.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$1;
.super Landroid/content/BroadcastReceiver;
.source "IntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    .line 85
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 88
    const-string v0, "UMC:IntentHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent received by UMC - Action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->onIntentReceived(Landroid/content/Intent;)V

    .line 90
    return-void
.end method
