.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field private static final PLATFORM_VER_SYS_PROP_KEY:Ljava/lang/String; = "ro.build.version.release"

.field private static final TAG:Ljava/lang/String; = "UMC:DeviceInfo"

.field private static final UNKNOWN:Ljava/lang/String; = "unknown"

.field public static final UTF_8:Ljava/nio/charset/Charset;


# instance fields
.field private androidOsVersion:Ljava/lang/String;

.field private deviceId:Ljava/lang/String;

.field private deviceUserId:Ljava/lang/String;

.field private knoxSdkVersion:Ljava/lang/String;

.field private locale:Ljava/lang/String;

.field private meidNumber:Ljava/lang/String;

.field private modelNumber:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private pushId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->UTF_8:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    return-void
.end method

.method private static encode(Ljava/nio/charset/Charset;Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 293
    if-nez p1, :cond_0

    .line 294
    const/4 v0, 0x0

    .line 299
    :goto_0
    return-object v0

    .line 296
    :cond_0
    invoke-static {p1}, Ljava/nio/CharBuffer;->wrap(Ljava/lang/CharSequence;)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/nio/charset/Charset;->encode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 297
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-array v0, v0, [B

    .line 298
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method private static getConsistentDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xe

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 215
    .line 218
    :try_start_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 217
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 219
    if-nez v0, :cond_0

    .line 220
    const-string v0, "UMC:DeviceInfo"

    const-string v1, "TelephonyManager is null in getConsistentDeviceId"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SEC"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 251
    :goto_0
    return-object v0

    .line 223
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    .line 224
    if-nez v4, :cond_1

    .line 225
    const-string v0, "UMC:DeviceInfo"

    const-string v1, "tm.getDeviceId() is null in getConsistentDeviceId"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SEC"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 228
    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 229
    if-ne v0, v1, :cond_2

    move v0, v1

    .line 249
    :goto_1
    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getSmallHashForDeviceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 250
    const-string v2, "UMC:DeviceInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "return unique deviceID : SEC"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SEC"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 231
    :cond_2
    if-ne v0, v3, :cond_4

    .line 232
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v6, :cond_3

    move v0, v2

    .line 233
    goto :goto_1

    :cond_3
    move v0, v3

    .line 236
    goto :goto_1

    .line 238
    :cond_4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v5, 0xf

    if-ne v0, v5, :cond_5

    move v0, v1

    .line 239
    goto :goto_1

    .line 240
    :cond_5
    invoke-virtual {v4}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    if-ne v0, v6, :cond_6

    move v0, v2

    .line 241
    goto :goto_1

    :cond_6
    move v0, v3

    .line 245
    goto :goto_1

    :catch_0
    move-exception v0

    .line 246
    const-string v1, "UMC:DeviceInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception in getConsistentDeviceId"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SEC"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;
    .locals 6

    .prologue
    .line 130
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;-><init>()V

    .line 131
    const-string v0, "ro.build.version.release"

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/i;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->androidOsVersion:Ljava/lang/String;

    .line 133
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 132
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 134
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    .line 136
    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDeviceIdInternal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    :cond_0
    iget-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceId:Ljava/lang/String;

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    .line 146
    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->phoneNumber:Ljava/lang/String;

    .line 147
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getVersion()Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->knoxSdkVersion:Ljava/lang/String;

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->locale:Ljava/lang/String;

    .line 149
    const-string v1, "AJ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 150
    const-string v4, "=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v0, "ro.product.model"

    const-string v1, "N/A"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/i;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->modelNumber:Ljava/lang/String;

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceUserId:Ljava/lang/String;

    .line 153
    return-object v2

    .line 137
    :catch_0
    move-exception v1

    .line 138
    const-string v3, "UMC:DeviceInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getDefaultDeviceInfo failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SEC"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceId:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private static getDeviceIdInternal(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x80

    .line 162
    if-nez p0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getDeviceId requires a Context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    const-string v0, "deviceName"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 167
    const/4 v2, 0x0

    .line 169
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 172
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    const/16 v4, 0x80

    invoke-direct {v1, v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 175
    if-eqz v1, :cond_1

    .line 176
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 179
    :cond_1
    if-nez v0, :cond_3

    .line 182
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 183
    const-string v0, "UMC:DeviceInfo"

    const-string v1, "Can\'t delete null deviceName file; try overwrite."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_2
    :goto_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/FileWriter;

    invoke-direct {v0, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 199
    :try_start_2
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getConsistentDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 201
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 203
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    .line 205
    :cond_3
    return-object v0

    .line 174
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 175
    :goto_1
    if-eqz v1, :cond_4

    .line 176
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 178
    :cond_4
    throw v0

    .line 189
    :cond_5
    const-string v0, "UMC:DeviceInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ": File exists, but can\'t read?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 190
    const-string v2, "  Trying to remove."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 192
    const-string v0, "UMC:DeviceInfo"

    const-string v1, "Remove failed. Tring to overwrite."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :catchall_1
    move-exception v0

    .line 203
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    .line 204
    throw v0

    .line 174
    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method private static getHexString([B)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 279
    const-string v0, ""

    move-object v2, v0

    move v0, v1

    .line 280
    :goto_0
    const/4 v3, 0x6

    if-lt v0, v3, :cond_0

    .line 284
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 282
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%02x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aget-byte v5, p0, v0

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static getSmallHashForDeviceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 256
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v0

    .line 257
    const-string v1, "UMC:DeviceInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "myUserId : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    if-eqz v0, :cond_0

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 265
    :cond_0
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 270
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->toUtf8(Ljava/lang/String;)[B

    move-result-object v1

    .line 271
    if-eqz v1, :cond_1

    .line 272
    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 274
    :cond_1
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getHexString([B)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static toUtf8(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 289
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->encode(Ljava/nio/charset/Charset;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAndroidOsVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->androidOsVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceUserId:Ljava/lang/String;

    return-object v0
.end method

.method public getKnoxSdkVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->knoxSdkVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->locale:Ljava/lang/String;

    return-object v0
.end method

.method public getMeidNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->modelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPushId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->pushId:Ljava/lang/String;

    return-object v0
.end method

.method public printDeviceInfo()V
    .locals 3

    .prologue
    .line 157
    const-string v0, "AJ"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deviceinfo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->androidOsVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 158
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->locale:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public setAndroidOsVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->androidOsVersion:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceId:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setDeviceUserId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->deviceUserId:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setKnoxSdkVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->knoxSdkVersion:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setLocale(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->locale:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setMeidNumber(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->meidNumber:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setModelNumber(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->modelNumber:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->phoneNumber:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setPushId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->pushId:Ljava/lang/String;

    .line 119
    return-void
.end method
