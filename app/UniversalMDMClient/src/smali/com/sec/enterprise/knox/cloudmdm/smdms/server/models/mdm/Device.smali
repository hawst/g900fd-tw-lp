.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonInclude;
    value = .enum Lcom/fasterxml/jackson/annotation/JsonInclude$Include;->NON_NULL:Lcom/fasterxml/jackson/annotation/JsonInclude$Include;
.end annotation


# static fields
.field public static final ENROLLED:Ljava/lang/String; = "ENROLLED"

.field public static final ENROLLING:Ljava/lang/String; = "ENROLLING"

.field public static final UNENROLLED:Ljava/lang/String; = "UNENROLLED"


# instance fields
.field private authentication:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;

.field private certificate:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

.field private data:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Data;

.field private description:Ljava/lang/String;

.field private enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;

.field private eulas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;"
        }
    .end annotation
.end field

.field private id:Ljava/lang/String;

.field private keyPair:Ljava/security/KeyPair;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field private licenses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;",
            ">;"
        }
    .end annotation
.end field

.field private management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

.field private name:Ljava/lang/String;

.field private profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    return-void
.end method


# virtual methods
.method public getAuthentication()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->authentication:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;

    return-object v0
.end method

.method public getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->certificate:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    return-object v0
.end method

.method public getData()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Data;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->data:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Data;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;

    return-object v0
.end method

.method public getEulas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->eulas:Ljava/util/List;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyPair()Ljava/security/KeyPair;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->keyPair:Ljava/security/KeyPair;

    return-object v0
.end method

.method public getLicenses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->licenses:Ljava/util/List;

    return-object v0
.end method

.method public getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->status:Ljava/lang/String;

    return-object v0
.end method

.method public printDevice()V
    .locals 3

    .prologue
    .line 338
    const-string v0, "AJ"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method public setAuthentication(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->authentication:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;

    .line 303
    return-void
.end method

.method public setCertificate(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->certificate:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    .line 279
    return-void
.end method

.method public setData(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Data;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->data:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Data;

    .line 287
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->description:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public setEnrollment(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->enrollment:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;

    .line 295
    return-void
.end method

.method public setEulas(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->eulas:Ljava/util/List;

    .line 327
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->id:Ljava/lang/String;

    .line 247
    return-void
.end method

.method public setKeyPair(Ljava/security/KeyPair;)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->keyPair:Ljava/security/KeyPair;

    .line 48
    return-void
.end method

.method public setLicenses(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->licenses:Ljava/util/List;

    .line 319
    return-void
.end method

.method public setManagement(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->management:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    .line 311
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->name:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public setProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    .line 271
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->status:Ljava/lang/String;

    .line 335
    return-void
.end method
