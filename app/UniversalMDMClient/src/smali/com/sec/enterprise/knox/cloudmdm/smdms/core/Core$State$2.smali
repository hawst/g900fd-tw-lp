.class enum Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;
.source "Core.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;
.end annotation


# static fields
.field private static synthetic pB:[I


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;-><init>(Ljava/lang/String;ILcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 1
    return-void
.end method

.method static synthetic fN()[I
    .locals 3

    .prologue
    .line 152
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->pB:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pN:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_12

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_11

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pW:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_10

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pF:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_f

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pV:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_e

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pL:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_d

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_c

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_b

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pU:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_a

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pT:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_8

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pE:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_7

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pS:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_6

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pR:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_5

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pO:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_4

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pP:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_3

    :goto_10
    :try_start_10
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pG:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_2

    :goto_11
    :try_start_11
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pH:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_1

    :goto_12
    :try_start_12
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pQ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_0

    :goto_13
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->pB:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_13

    :catch_1
    move-exception v1

    goto :goto_12

    :catch_2
    move-exception v1

    goto :goto_11

    :catch_3
    move-exception v1

    goto :goto_10

    :catch_4
    move-exception v1

    goto :goto_f

    :catch_5
    move-exception v1

    goto :goto_e

    :catch_6
    move-exception v1

    goto :goto_d

    :catch_7
    move-exception v1

    goto :goto_c

    :catch_8
    move-exception v1

    goto :goto_b

    :catch_9
    move-exception v1

    goto :goto_a

    :catch_a
    move-exception v1

    goto :goto_9

    :catch_b
    move-exception v1

    goto/16 :goto_8

    :catch_c
    move-exception v1

    goto/16 :goto_7

    :catch_d
    move-exception v1

    goto/16 :goto_6

    :catch_e
    move-exception v1

    goto/16 :goto_5

    :catch_f
    move-exception v1

    goto/16 :goto_4

    :catch_10
    move-exception v1

    goto/16 :goto_3

    :catch_11
    move-exception v1

    goto/16 :goto_2

    :catch_12
    move-exception v1

    goto/16 :goto_1
.end method


# virtual methods
.method R(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 361
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hV()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->getElements()Ljava/util/List;

    move-result-object v1

    .line 363
    const/4 v0, 0x0

    .line 364
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 369
    if-eqz v1, :cond_1

    .line 370
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->M(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 373
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    const v2, 0x7f080099

    .line 374
    const v3, 0x7f08009d

    const v4, 0x7f080058

    const/4 v5, 0x1

    .line 373
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 375
    const-string v0, "UMC:Core"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processMdmServerSelection: Already enroled:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 376
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 375
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_1
    :goto_1
    return-void

    .line 364
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 365
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 366
    goto :goto_0

    .line 379
    :cond_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEulas()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(Ljava/util/List;)V

    goto :goto_1
.end method

.method a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 386
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 387
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processMessage In STATE_ENROLLMENT: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->fN()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 435
    :pswitch_0
    const-string v0, "UMC:Core"

    .line 436
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled message in state, STATE_ENROLLMENT, Message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 437
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 436
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0800a2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 439
    const/4 v2, 0x1

    .line 438
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 439
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 440
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 441
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    .line 444
    :goto_0
    return-void

    .line 390
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->r(Ljava/lang/Object;)V

    goto :goto_0

    .line 393
    :pswitch_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 394
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->t(Ljava/lang/Object;)V

    goto :goto_0

    .line 397
    :pswitch_3
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 398
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->w(Ljava/lang/Object;)V

    goto :goto_0

    .line 401
    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->u(Ljava/lang/Object;)V

    goto :goto_0

    .line 404
    :pswitch_5
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->R(Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :pswitch_6
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 408
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->y(Ljava/lang/Object;)V

    goto :goto_0

    .line 411
    :pswitch_7
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 412
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->s(Ljava/lang/Object;)V

    goto :goto_0

    .line 418
    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->v(Ljava/lang/Object;)V

    goto :goto_0

    .line 421
    :pswitch_9
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 422
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 423
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->x(Ljava/lang/Object;)V

    .line 424
    const-string v0, "UMC:Core"

    const-string v1, "Changing state should be handled here"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    :pswitch_a
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 429
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    goto :goto_0

    .line 432
    :pswitch_b
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    goto/16 :goto_0

    .line 388
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_b
        :pswitch_1
    .end packed-switch
.end method

.method a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 484
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->M(Ljava/lang/String;)V

    .line 485
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getAuthentication()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;->getScheme()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;)V

    .line 493
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fI()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->wZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fy()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 495
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fI()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 496
    const/4 v0, 0x2

    new-array v6, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    .line 497
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v5

    .line 498
    const v2, 0x7f080075

    .line 499
    const v1, 0x7f080077

    .line 500
    const/4 v7, 0x0

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    .line 501
    const-string v3, "username"

    .line 502
    const/4 v4, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;-><init>(IILjava/lang/String;ILjava/lang/String;)V

    .line 500
    aput-object v0, v6, v7

    .line 503
    const v2, 0x7f080074

    .line 504
    const v1, 0x7f080076

    .line 505
    const/4 v7, 0x1

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    const-string v3, "password"

    .line 506
    const/4 v4, 0x4

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;-><init>(IILjava/lang/String;ILjava/lang/String;)V

    .line 505
    aput-object v0, v6, v7

    .line 508
    if-eqz v6, :cond_0

    .line 509
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(Ljava/lang/String;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V

    goto :goto_0

    .line 512
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fy()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pI:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method p(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 156
    const-string v0, "UMC:Core"

    const-string v1, "In STATE_ENROLLMENT"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    const v2, 0x7f08006c

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 158
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 159
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->fP()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->m(Landroid/content/Context;)V

    .line 160
    return-void
.end method

.method q(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 165
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->Q(Ljava/lang/String;)V

    .line 166
    return-object p1
.end method

.method public r(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const v4, 0x7f0800a4

    .line 170
    check-cast p1, Ljava/lang/Integer;

    .line 171
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processDeviceVersionCheck: result  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 174
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 175
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 176
    const v1, 0x7f0800a6

    const v2, 0x7f0800a7

    .line 177
    const v3, 0x7f080053

    .line 175
    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->b(IIII)V

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 180
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    .line 181
    const v1, 0x7f0800a9

    const v2, 0x7f080058

    const/4 v3, 0x1

    .line 180
    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0

    .line 184
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bm(Ljava/lang/String;)V

    goto :goto_0
.end method

.method s(Ljava/lang/Object;)V
    .locals 11

    .prologue
    const v6, 0x7f080058

    const/4 v10, 0x0

    const/4 v3, -0x2

    const/4 v9, -0x1

    const/4 v0, 0x0

    .line 189
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v1

    if-nez v1, :cond_1

    .line 190
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    const v1, 0x7f080099

    .line 191
    const v2, 0x7f08009c

    .line 190
    invoke-virtual {v0, v1, v2, v6, v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 198
    const-string v0, "UMC:Core"

    const-string v1, "Invaild auth cred dialog shown"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v5

    .line 204
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 205
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;->getPem()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 206
    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;->getPem()Ljava/lang/String;

    move-result-object v8

    .line 207
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fE()Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v1

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->l(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 208
    if-eq v1, v3, :cond_2

    .line 209
    if-eq v1, v9, :cond_2

    .line 210
    const/4 v2, -0x3

    if-ne v1, v2, :cond_5

    .line 211
    :cond_2
    const v2, 0x7f080099

    .line 213
    const-string v0, ""

    .line 214
    if-ne v1, v3, :cond_3

    .line 215
    const v0, 0x7f080098

    .line 216
    const-string v7, "MDM Server Certificate Expired"

    .line 221
    :goto_1
    const-string v1, "UMC:Core"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processEnrollmentResponse: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v6, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 224
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v2

    .line 225
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;->xq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    .line 226
    const-string v5, "Error Report"

    .line 227
    sget-object v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->toString()Ljava/lang/String;

    move-result-object v6

    .line 224
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    goto/16 :goto_0

    .line 218
    :cond_3
    const v0, 0x7f080097

    .line 219
    const-string v7, "MDM Server Certificate Validation Failed"

    goto :goto_1

    :cond_4
    move-object v8, v0

    .line 233
    :cond_5
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)Ljava/lang/String;

    move-result-object v4

    .line 235
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v6

    .line 236
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 237
    invoke-virtual {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getByodEnabled()Z

    move-result v1

    .line 238
    if-eqz v1, :cond_6

    .line 239
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v2

    .line 240
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fF()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    move-result-object v3

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fG()Ljava/lang/String;

    move-result-object v7

    .line 241
    new-instance v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;

    invoke-direct {v9, p0, v6, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    .line 239
    invoke-direct/range {v0 .. v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    .line 255
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fH()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->execute()V

    goto/16 :goto_0

    .line 260
    :cond_6
    if-eqz v4, :cond_7

    .line 261
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 262
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 263
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V

    .line 264
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;-><init>(Landroid/content/Context;)V

    .line 265
    const-string v1, "vendor.apk"

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v2

    .line 264
    invoke-virtual {v0, v4, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v0, "UMC:Core"

    const-string v1, "sent intent to start MDM agent download and install "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    :cond_7
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v4

    move-object v6, v0

    move-object v7, v0

    move v8, v10

    move v10, v9

    invoke-static/range {v4 .. v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/util/ArrayList;Ljava/lang/String;ZII)V

    .line 271
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    .line 272
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 273
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    goto/16 :goto_0
.end method

.method t(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 281
    if-nez p1, :cond_0

    .line 282
    const-string v0, "UMC:Core"

    const-string v1, "obj is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :goto_0
    return-void

    .line 286
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fE()Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->init()Z

    .line 288
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendProfileDiscoveryRequest: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080078

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 291
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->L(Ljava/lang/String;)V

    .line 292
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->Q(Ljava/lang/String;)V

    .line 295
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->m(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 296
    :catch_0
    move-exception v0

    .line 297
    const-string v1, "UMC:Core"

    const-string v2, "Profile Lookup failed in the core"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method u(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/16 v3, 0x194

    const/4 v2, 0x1

    .line 303
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hV()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;

    move-result-object v0

    .line 304
    if-nez v0, :cond_0

    .line 305
    const-string v0, "UMC:Core"

    const-string v1, "sendMdmServerSelectionRequest: profContainer is null "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 307
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 308
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 309
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->x(Ljava/lang/Object;)V

    .line 342
    :goto_0
    return-void

    .line 312
    :cond_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileContainer;->getElements()Ljava/util/List;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 314
    :cond_1
    const-string v0, "UMC:Core"

    const-string v1, "sendMdmServerSelectionRequest: profList is null or empty "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 316
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 317
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 318
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->x(Ljava/lang/Object;)V

    goto :goto_0

    .line 321
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v2, :cond_4

    .line 322
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 323
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 327
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v0

    const-string v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v2, v0, v2

    .line 329
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 328
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 330
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v3

    .line 331
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 330
    invoke-virtual {v3, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 332
    :catch_0
    move-exception v0

    .line 333
    const-string v1, "UMC:Core"

    const-string v2, "Email doesn\'t have the @"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 335
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    goto :goto_0

    .line 323
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 324
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 339
    :cond_4
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 340
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->R(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method v(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 345
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 346
    if-eqz v0, :cond_0

    .line 347
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v0

    .line 348
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    .line 349
    const-string v0, "UMC:Core"

    .line 350
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KNOX Container EULA Shown"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->T(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 349
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->c(Landroid/content/Context;Z)Z

    .line 352
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KLM EULA Shown"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->U(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->d(Landroid/content/Context;Z)Z

    .line 358
    :goto_0
    return-void

    .line 355
    :cond_0
    const-string v0, "UMC:Core"

    const-string v1, "Eula is not accepted and doing shutdown  "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->shutdown()V

    goto :goto_0
.end method

.method w(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 447
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 448
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fy()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;->pH:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;

    invoke-virtual {v0, v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$StateMessage;Ljava/lang/Object;)V

    .line 449
    return-void
.end method

.method x(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const v6, 0x7f080099

    const v5, 0x7f080058

    .line 452
    move-object v0, p1

    check-cast v0, Landroid/os/Message;

    .line 453
    iget v2, v0, Landroid/os/Message;->arg1:I

    .line 454
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 455
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 456
    const/16 v3, 0x194

    if-ne v2, v3, :cond_0

    .line 459
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    .line 460
    const v3, 0x7f08009b

    .line 459
    invoke-virtual {v1, v6, v3, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 479
    :goto_0
    const-string v1, "UMC:Core"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processNetworkError: errorcode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 481
    return-void

    .line 461
    :cond_0
    const/16 v3, 0x191

    if-ne v2, v3, :cond_1

    .line 464
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    .line 465
    const v3, 0x7f08009c

    .line 464
    invoke-virtual {v1, v6, v3, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0

    .line 466
    :cond_1
    const/16 v3, 0x199

    if-ne v2, v3, :cond_2

    .line 469
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v1

    .line 470
    const v3, 0x7f08009d

    const/4 v4, 0x1

    .line 469
    invoke-virtual {v1, v6, v3, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 471
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State$2;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    goto :goto_0

    .line 475
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)I

    move-result v1

    .line 476
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v3

    invoke-virtual {v3, v6, v1, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    goto :goto_0
.end method

.method y(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 517
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fA()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080079

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 518
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fI()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->xa:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 519
    check-cast p1, Ljava/util/HashMap;

    .line 520
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "username"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "password"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->O(Ljava/lang/String;)V

    .line 524
    :try_start_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->d(Ljava/util/HashMap;)Lorg/json/JSONObject;

    move-result-object v0

    .line 525
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->N(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 538
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->hU()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/o;->aR(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    move-result-object v1

    .line 540
    if-eqz v1, :cond_3

    .line 542
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getMessenger()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->xg:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 543
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fJ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 544
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fB()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/b;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/spp/a;)V

    .line 554
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 555
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getMessenger()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->xg:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 556
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fJ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 557
    const-string v0, "UMC:Core"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendEnrollmentRequest:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fJ()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v4

    .line 559
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fM()Ljava/lang/String;

    move-result-object v5

    .line 558
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    :cond_3
    :goto_2
    return-void

    .line 526
    :catch_0
    move-exception v0

    .line 527
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 529
    :cond_4
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fI()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->wZ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    const-string v0, ""

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->O(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 547
    :cond_5
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fK()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fK()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 549
    :cond_6
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fL()Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/gcm/a;->gi()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->K(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 562
    :cond_7
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fK()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fK()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 563
    const-string v0, "UMC:Core"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendEnrollmentRequest:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fK()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fC()Ljava/lang/String;

    move-result-object v4

    .line 565
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fM()Ljava/lang/String;

    move-result-object v5

    .line 564
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
