.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;
.super Landroid/os/AsyncTask;
.source "ExchangeLoginHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 11

    .prologue
    .line 90
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v5, 0x1bb

    :goto_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Z

    move-result v6

    const/4 v7, 0x0

    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->uj:Ljava/lang/String;

    sget-object v9, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->tk:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Landroid/content/Context;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 91
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/c;->hs()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 90
    :cond_0
    const/16 v5, 0x50

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 7

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v3

    .line 97
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->uk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    invoke-static {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;)Ljava/lang/String;

    move-result-object v6

    .line 96
    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/f;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
