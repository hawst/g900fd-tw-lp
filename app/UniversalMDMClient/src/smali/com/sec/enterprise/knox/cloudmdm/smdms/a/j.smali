.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;
.super Ljava/lang/Object;
.source "UserHandle.java"


# direct methods
.method public static aC(I)Landroid/os/UserHandle;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Landroid/os/UserHandle;

    invoke-direct {v0, p0}, Landroid/os/UserHandle;-><init>(I)V

    return-object v0
.end method

.method public static fi()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, -0x1

    return v0
.end method

.method public static getAppId(I)I
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    return v0
.end method

.method public static getCallingUserId()I
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    return v0
.end method

.method public static getUid(II)I
    .locals 1

    .prologue
    .line 44
    invoke-static {p0, p1}, Landroid/os/UserHandle;->getUid(II)I

    move-result v0

    return v0
.end method

.method public static getUserId(I)I
    .locals 1

    .prologue
    .line 47
    invoke-static {p0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    return v0
.end method

.method public static myUserId()I
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    return v0
.end method
