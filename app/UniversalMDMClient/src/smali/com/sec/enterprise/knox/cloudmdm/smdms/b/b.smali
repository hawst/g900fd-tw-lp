.class public final Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field private static zT:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    .line 58
    return-void
.end method

.method public static declared-synchronized a(Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;

    monitor-enter v1

    if-nez p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    monitor-exit v1

    return v0

    .line 75
    :cond_1
    :try_start_0
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 76
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 136
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    return-void

    .line 136
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 137
    const/4 v2, 0x3

    invoke-virtual {v0, v2, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 181
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    return-void

    .line 181
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 182
    const/4 v2, 0x6

    invoke-virtual {v0, v2, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 281
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    return-void

    .line 281
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 282
    const/4 v2, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 310
    if-nez p0, :cond_0

    .line 311
    const-string v0, ""

    .line 328
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    .line 318
    :goto_1
    if-nez v0, :cond_1

    .line 325
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 326
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 327
    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 328
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 319
    :cond_1
    instance-of v1, v0, Ljava/net/UnknownHostException;

    if-eqz v1, :cond_2

    .line 320
    const-string v0, ""

    goto :goto_0

    .line 322
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_1
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 151
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    return-void

    .line 151
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 152
    const/4 v2, 0x4

    invoke-virtual {v0, v2, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static s(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 196
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    return-void

    .line 196
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 197
    const/4 v2, 0x1

    invoke-virtual {v0, v2, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 121
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    return-void

    .line 121
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 122
    const/4 v2, 0x2

    invoke-virtual {v0, v2, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 166
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->zT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    return-void

    .line 166
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;

    .line 167
    const/4 v2, 0x5

    invoke-virtual {v0, v2, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
