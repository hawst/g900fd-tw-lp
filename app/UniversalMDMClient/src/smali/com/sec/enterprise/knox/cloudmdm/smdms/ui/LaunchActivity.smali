.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;
.super Landroid/app/Activity;
.source "LaunchActivity.java"


# static fields
.field private static final yN:[Ljava/lang/String;


# instance fields
.field private xK:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 34
    const-string v2, "b2b-sanjose.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "secb2b.com"

    aput-object v2, v0, v1

    .line 33
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->yN:[Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static final M(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 131
    .line 132
    const-string v0, "LaunchParamters"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 133
    const-string v1, "email"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    const-string v2, "seg_url"

    invoke-interface {v0, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 135
    const-string v3, "update_url"

    invoke-interface {v0, v3, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 136
    const-string v4, "mdm_token"

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 137
    const-string v4, "program"

    const-string v5, "NORMAL"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 138
    const-string v5, "quickstart_url"

    invoke-interface {v0, v5, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 139
    const-string v6, "all"

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 140
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public static final N(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 158
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->gA()Z

    .line 159
    return-void
.end method

.method public static final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 118
    const-string v0, "LaunchParamters"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    const-string v1, "email"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 121
    const-string v1, "seg_url"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 122
    const-string v1, "update_url"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 123
    const-string v1, "mdm_token"

    invoke-interface {v0, v1, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 124
    const-string v1, "program"

    invoke-interface {v0, v1, p5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 125
    const-string v1, "quickstart_url"

    invoke-interface {v0, v1, p6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 126
    const-string v1, "all"

    invoke-interface {v0, v1, p7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 127
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 128
    return-void
.end method

.method public static bl(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 162
    if-nez p0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 166
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 167
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->yN:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 168
    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 169
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static final m(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 144
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    const-string v0, "quickstart_url"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->bl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    :cond_2
    const-string v0, "LaunchParamters"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 151
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 153
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private showDialog()V
    .locals 2

    .prologue
    .line 103
    new-instance v0, Landroid/app/ProgressDialog;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->xK:Landroid/app/ProgressDialog;

    .line 104
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->xK:Landroid/app/ProgressDialog;

    const v1, 0x7f08006d

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->xK:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->xK:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 107
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const/4 v0, -0x1

    :try_start_0
    invoke-static {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateUMCAdminIfNotRequired(Landroid/content/Context;I)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 49
    const-string v0, "UMC:LaunchActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "seg_url"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "update_url"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "email"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v4, "mdm_token"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 55
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v5, "program"

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 56
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v8, "quickstart_url"

    invoke-virtual {v0, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-virtual {v7}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v7

    .line 59
    const-string v8, "UMC:LaunchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onCreate :seg_url: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v8, "UMC:LaunchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onCreate :update_url: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v8, "UMC:LaunchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onCreate :email: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v8, "UMC:LaunchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onCreate :mdm_token: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v8, "UMC:LaunchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onCreate :program: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v8, "UMC:LaunchActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onCreate :quickstart_url: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "SEG_URL_OVERRIDE_ALLOWED"

    invoke-static {v8, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->t(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    move-object v2, v6

    .line 69
    :cond_0
    if-nez v5, :cond_1

    .line 70
    const-string v5, "NORMAL"

    .line 72
    :cond_1
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->bl(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 75
    :goto_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fw()Ljava/lang/String;

    move-result-object v0

    .line 79
    const-string v4, "UMC:LaunchActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Current Program : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v4, "UMC:LaunchActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Launch Program : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    if-eqz v0, :cond_2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 82
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fk()Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 83
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fp()V

    .line 88
    :goto_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v7

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v5

    move-object v12, v6

    invoke-virtual/range {v7 .. v12}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->showDialog()V

    .line 98
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fo()V

    .line 100
    :goto_2
    return-void

    .line 85
    :cond_2
    :try_start_1
    const-string v0, "UMC:LaunchActivity"

    const-string v4, "First Launch"

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v0, "UMC:LaunchActivity"

    const-string v1, "INVALID PARAMS finish Activity."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->finish()V

    goto :goto_2

    :cond_3
    move-object v6, v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->xK:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->xK:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 113
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 114
    return-void
.end method
