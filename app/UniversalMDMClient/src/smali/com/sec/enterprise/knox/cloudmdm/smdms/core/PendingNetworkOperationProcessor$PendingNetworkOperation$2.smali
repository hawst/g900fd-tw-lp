.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;
.super Ljava/lang/Object;
.source "PendingNetworkOperationProcessor.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->handleUpdateDevice()V
.end annotation


# instance fields
.field final synthetic qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 5

    .prologue
    .line 138
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 139
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gInstance:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$0()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->type:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->data:Ljava/lang/String;

    .line 140
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->url:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->profileId:Ljava/lang/String;

    .line 139
    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->removePendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gInstance:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$0()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->processPendingNetworkOperation()V

    .line 142
    return-void
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 2

    .prologue
    .line 131
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 132
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation$2;->qq:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;

    iget v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->retryCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor$PendingNetworkOperation;->retryCount:I

    .line 133
    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->gInstance:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$0()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->storePendingNetworkOperation()V
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;)V

    .line 134
    return-void
.end method
