.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;
.super Ljava/lang/Object;
.source "PushStorage.java"


# static fields
.field private static qZ:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    .line 137
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)V
    .locals 4

    .prologue
    .line 243
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->h(Landroid/content/Context;)I

    .line 245
    const-string v0, "PushStorage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter removePush:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 247
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->i(Landroid/content/Context;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    monitor-exit v1

    return-void

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)I
    .locals 5

    .prologue
    .line 252
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;

    monitor-enter v1

    :try_start_0
    const-string v0, "PushStorage"

    const-string v2, "Enter storeProfile"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->h(Landroid/content/Context;)I

    .line 256
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    move-result-object v0

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->rb:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    const-string v0, "PushStorage"

    .line 258
    const-string v2, "Uneroll Push : Remove related messages and put unenroll as first message"

    .line 257
    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 260
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 271
    :goto_1
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->i(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    const-string v0, "PushStorage"

    const-string v2, "UnLoad List failed"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    const/4 v0, -0x4

    .line 275
    :goto_2
    monitor-exit v1

    return v0

    .line 261
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    .line 262
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 268
    :cond_2
    :try_start_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 275
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static declared-synchronized h(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 152
    const-class v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;

    monitor-enter v6

    :try_start_0
    const-string v0, "PushStorage"

    const-string v3, "Enter loadList"

    invoke-static {v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const-string v0, "PushStorage"

    const-string v2, "Already Loaded"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 189
    :goto_0
    monitor-exit v6

    return v0

    .line 159
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    :try_start_2
    const-string v0, "push.dat"

    invoke-virtual {p0, v0}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v5

    .line 165
    :try_start_3
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 166
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v7

    .line 168
    const-string v0, "PushStorage"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Load List: nitems="

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move v4, v1

    .line 170
    :goto_1
    if-lt v4, v7, :cond_3

    .line 181
    if-eqz v3, :cond_1

    .line 182
    :try_start_5
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 183
    :cond_1
    if-eqz v5, :cond_2

    .line 184
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_2
    move v0, v1

    .line 189
    goto :goto_0

    .line 171
    :cond_3
    :try_start_6
    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    invoke-virtual {v8, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 170
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 173
    :catch_0
    move-exception v0

    move-object v1, v4

    .line 174
    :goto_3
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 181
    if-eqz v1, :cond_4

    .line 182
    :try_start_8
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 183
    :cond_4
    if-eqz v4, :cond_5

    .line 184
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_5
    :goto_4
    move v0, v2

    .line 175
    goto :goto_0

    .line 185
    :catch_1
    move-exception v0

    .line 186
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 176
    :catch_2
    move-exception v0

    move-object v3, v4

    move-object v5, v4

    .line 177
    :goto_5
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 181
    if-eqz v3, :cond_6

    .line 182
    :try_start_b
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 183
    :cond_6
    if-eqz v5, :cond_7

    .line 184
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_7
    :goto_6
    move v0, v2

    .line 178
    goto :goto_0

    .line 185
    :catch_3
    move-exception v0

    .line 186
    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_6

    .line 179
    :catchall_1
    move-exception v0

    move-object v3, v4

    move-object v5, v4

    .line 181
    :goto_7
    if-eqz v3, :cond_8

    .line 182
    :try_start_d
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 183
    :cond_8
    if-eqz v5, :cond_9

    .line 184
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 188
    :cond_9
    :goto_8
    :try_start_e
    throw v0

    .line 185
    :catch_4
    move-exception v1

    .line 186
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 185
    :catch_5
    move-exception v0

    .line 186
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_2

    .line 179
    :catchall_2
    move-exception v0

    move-object v3, v4

    goto :goto_7

    :catchall_3
    move-exception v0

    goto :goto_7

    :catchall_4
    move-exception v0

    move-object v3, v1

    move-object v5, v4

    goto :goto_7

    .line 176
    :catch_6
    move-exception v0

    move-object v3, v4

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_5

    .line 173
    :catch_8
    move-exception v0

    move-object v1, v4

    move-object v4, v5

    goto :goto_3

    :catch_9
    move-exception v0

    move-object v1, v3

    move-object v4, v5

    goto :goto_3
.end method

.method private static declared-synchronized i(Landroid/content/Context;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 193
    const-class v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;

    monitor-enter v4

    :try_start_0
    const-string v2, "PushStorage"

    const-string v3, "Enter unloadList"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    const-string v2, "PushStorage"

    const-string v3, "UnLoad List: empty"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_0
    :try_start_1
    const-string v2, "push.dat"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 203
    :try_start_2
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 205
    :try_start_3
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    .line 206
    const-string v5, "PushStorage"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UnLoad List: nitems="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 208
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v0

    if-nez v0, :cond_3

    .line 215
    if-eqz v2, :cond_1

    .line 216
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 217
    :cond_1
    if-eqz v3, :cond_2

    .line 218
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_1
    move v0, v1

    .line 223
    :goto_2
    monitor-exit v4

    return v0

    .line 208
    :cond_3
    :try_start_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;

    .line 209
    invoke-virtual {v2, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    move-object v0, v2

    move-object v1, v3

    .line 215
    :goto_3
    if-eqz v0, :cond_4

    .line 216
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V

    .line 217
    :cond_4
    if-eqz v1, :cond_5

    .line 218
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 212
    :cond_5
    :goto_4
    const/4 v0, -0x1

    goto :goto_2

    .line 219
    :catch_1
    move-exception v0

    .line 220
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 213
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    .line 215
    :goto_5
    if-eqz v2, :cond_6

    .line 216
    :try_start_8
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 217
    :cond_6
    if-eqz v3, :cond_7

    .line 218
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 222
    :cond_7
    :goto_6
    :try_start_9
    throw v0

    .line 219
    :catch_2
    move-exception v1

    .line 220
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 219
    :catch_3
    move-exception v0

    .line 220
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 213
    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catchall_3
    move-exception v0

    goto :goto_5

    .line 210
    :catch_4
    move-exception v1

    move-object v1, v0

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v1, v3

    goto :goto_3
.end method

.method public static declared-synchronized s(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;
    .locals 3

    .prologue
    .line 228
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->h(Landroid/content/Context;)I

    .line 229
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage;->qZ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
