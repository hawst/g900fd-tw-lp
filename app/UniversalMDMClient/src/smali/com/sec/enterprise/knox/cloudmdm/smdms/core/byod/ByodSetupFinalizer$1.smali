.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;
.super Ljava/lang/Object;
.source "ByodSetupFinalizer.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
.end annotation


# instance fields
.field private final synthetic pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

.field final synthetic qv:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;

.field private final synthetic qw:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->qv:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;

    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->qw:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 5

    .prologue
    .line 158
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 159
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    .line 160
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->pv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 161
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->qw:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;->qw:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v4

    .line 160
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->removePendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 168
    return-void
.end method
