.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;
.super Ljava/lang/Object;
.source "FtpService.java"


# instance fields
.field private final ro:J

.field private server:Ljava/lang/String;

.field private user:Ljava/lang/String;

.field private final vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

.field private vI:Ljava/lang/String;


# direct methods
.method constructor <init>(JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    .line 32
    iput-wide p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->ro:J

    .line 33
    return-void
.end method

.method private static aO(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 46
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v1, "www."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 52
    :cond_0
    :goto_0
    return-object v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    const-string v1, "FtpService"

    const-string v2, "Url exception in getDomainName"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 52
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198
    return-void
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 70
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->aO(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->setServer(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 76
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 77
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->user:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->vI:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    .line 78
    iget-wide v8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->ro:J

    move-object v5, p1

    .line 77
    invoke-direct/range {v1 .. v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V

    .line 79
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public m(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 57
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->aO(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->setServer(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 63
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 64
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->user:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->vI:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    .line 65
    iget-wide v8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->ro:J

    move-object v5, p1

    .line 64
    invoke-direct/range {v1 .. v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V

    .line 66
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method o(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->user:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->vI:Ljava/lang/String;

    .line 38
    return-void
.end method

.method setServer(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/k;->server:Ljava/lang/String;

    .line 42
    return-void
.end method
