.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;
.super Ljava/lang/Object;
.source "DeviceInfoCollector.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private xz:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    .line 22
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->xz:Landroid/telephony/TelephonyManager;

    .line 23
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 26
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private ih()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->xz:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->xz:Landroid/telephony/TelephonyManager;

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->xz:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private ii()Landroid/net/wifi/WifiManager;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method


# virtual methods
.method public ij()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    .line 80
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    const-string v1, "gsm.sim.operator.numeric"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/d;->p(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ik()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    .line 152
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->ig()Ljava/lang/String;

    move-result-object v0

    .line 154
    if-nez v0, :cond_0

    .line 155
    const-string v0, "ro.serial"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    :cond_0
    return-object v0
.end method

.method public il()Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    const-string v1, "ro.csc.sales_code"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/d;->p(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 168
    :cond_0
    const-string v0, "DeviceInfoCollector"

    const-string v1, "Failed to get the SC"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v0, 0x0

    .line 172
    :cond_1
    return-object v0
.end method

.method public im()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    .line 178
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    const-string v1, "ro.csc.countryiso_code"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/d;->p(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 180
    :cond_0
    const-string v0, "DeviceInfoCollector"

    const-string v1, "Failed to get the CISO"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const/4 v0, 0x0

    .line 184
    :cond_1
    return-object v0
.end method

.method public in()Ljava/lang/String;
    .locals 2

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->ih()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->bb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public io()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    .line 203
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->mContext:Landroid/content/Context;

    const-string v1, "ril.serialnumber"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/d;->p(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 207
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    const-string v1, "0000000000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    const-string v1, "00000000000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 211
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->bb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    :goto_0
    return-object v0

    .line 213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ip()Ljava/lang/String;
    .locals 3

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->ii()Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    .line 224
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->io()Ljava/lang/String;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_0

    .line 234
    :goto_0
    return-object v0

    .line 229
    :cond_0
    if-eqz v1, :cond_1

    .line 230
    const-string v0, ":"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->bb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
