.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;
.super Ljava/lang/Object;
.source "Admin.java"


# instance fields
.field public mAdminUId:I

.field public mCN:Landroid/content/ComponentName;

.field public mMDMUrl:Ljava/lang/String;

.field public mManagedUsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;ILandroid/content/ComponentName;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Landroid/content/ComponentName;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    .line 80
    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    .line 81
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mMDMUrl:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    .line 83
    if-eqz p4, :cond_0

    .line 84
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    .line 85
    :cond_0
    return-void
.end method
