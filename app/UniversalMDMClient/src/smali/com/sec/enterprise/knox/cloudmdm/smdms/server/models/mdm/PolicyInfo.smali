.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;
.super Ljava/lang/Object;
.source "PolicyInfo.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private content:Lcom/fasterxml/jackson/databind/JsonNode;

.field private description:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private signature:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->content:Lcom/fasterxml/jackson/databind/JsonNode;

    invoke-virtual {v0}, Lcom/fasterxml/jackson/databind/JsonNode;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->signature:Ljava/lang/String;

    return-object v0
.end method

.method public setContent(Lcom/fasterxml/jackson/databind/JsonNode;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->content:Lcom/fasterxml/jackson/databind/JsonNode;

    .line 56
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->description:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->id:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->name:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setSignature(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->signature:Ljava/lang/String;

    .line 81
    return-void
.end method
