.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;
.super Ljava/lang/Object;
.source "SslValidator.java"


# instance fields
.field private vj:Ljava/security/cert/X509Certificate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->vj:Ljava/security/cert/X509Certificate;

    .line 25
    return-void
.end method

.method private static a([Ljava/security/cert/X509Certificate;Ljava/lang/String;)Landroid/net/http/SslError;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 118
    aget-object v3, p0, v2

    .line 119
    if-nez v3, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 121
    const-string v1, "certificate for this site is null"

    .line 120
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getSigAlgName()Ljava/lang/String;

    move-result-object v4

    .line 125
    new-instance v0, Ljavax/net/ssl/DefaultHostnameVerifier;

    invoke-direct {v0}, Ljavax/net/ssl/DefaultHostnameVerifier;-><init>()V

    .line 126
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 127
    invoke-virtual {v0, p1, v3}, Ljavax/net/ssl/DefaultHostnameVerifier;->verify(Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x1

    .line 128
    :goto_0
    if-nez v0, :cond_2

    .line 129
    const-string v0, "SslValidator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "certificate not for this host: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    new-instance v0, Landroid/net/http/SslError;

    const/4 v1, 0x2

    const-string v2, ""

    invoke-direct {v0, v1, v3, v2}, Landroid/net/http/SslError;-><init>(ILjava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 155
    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    .line 126
    goto :goto_0

    .line 132
    :cond_2
    const-string v0, "X509"

    .line 133
    const-string v0, "HarmonyJSSE"

    .line 134
    const/4 v5, 0x0

    .line 137
    :try_start_0
    const-string v0, "X509"

    const-string v6, "HarmonyJSSE"

    invoke-static {v0, v6}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 144
    :goto_2
    :try_start_1
    invoke-virtual {v0, v5}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_2

    .line 149
    :goto_3
    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v0

    .line 148
    aget-object v0, v0, v2

    check-cast v0, Ljavax/net/ssl/X509TrustManager;

    .line 151
    :try_start_2
    invoke-interface {v0, p0, v4}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v0, v1

    .line 152
    goto :goto_1

    .line 138
    :catch_0
    move-exception v0

    .line 139
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    move-object v0, v1

    goto :goto_2

    .line 140
    :catch_1
    move-exception v0

    .line 141
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    move-object v0, v1

    goto :goto_2

    .line 145
    :catch_2
    move-exception v5

    .line 146
    invoke-virtual {v5}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_3

    .line 153
    :catch_3
    move-exception v0

    .line 155
    new-instance v0, Landroid/net/http/SslError;

    const/4 v1, 0x3

    const-string v2, ""

    invoke-direct {v0, v1, v3, v2}, Landroid/net/http/SslError;-><init>(ILjava/security/cert/X509Certificate;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public az(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 59
    .line 63
    const/4 v0, 0x1

    new-array v1, v0, [Ljavax/net/ssl/TrustManager;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;)V

    aput-object v0, v1, v3

    .line 77
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;)V

    invoke-static {v0}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 79
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    const/16 v4, 0x2710

    :try_start_1
    invoke-virtual {v0, v4}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 81
    const-string v4, "TLS"

    invoke-static {v4}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v4

    .line 82
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v1, v6}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 83
    invoke-virtual {v4}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 84
    const-string v1, "SslValidator"

    const-string v4, "connect"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 86
    const-string v1, "SslValidator"

    const-string v4, "getServerCertificates"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getServerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v1

    .line 87
    check-cast v1, [Ljava/security/cert/X509Certificate;

    .line 89
    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->a([Ljava/security/cert/X509Certificate;Ljava/lang/String;)Landroid/net/http/SslError;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 92
    const/4 v4, 0x0

    :try_start_2
    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->vj:Ljava/security/cert/X509Certificate;

    .line 94
    if-nez v3, :cond_0

    .line 95
    const-string v1, "Test"

    const-string v4, "No error"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 101
    :cond_0
    if-eqz v0, :cond_1

    .line 102
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 105
    :cond_1
    :goto_0
    if-nez v3, :cond_3

    move-object v0, v2

    :goto_1
    return-object v0

    .line 97
    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 98
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 101
    if-eqz v3, :cond_4

    .line 102
    invoke-virtual {v3}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    move-object v3, v1

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    .line 101
    :goto_3
    if-eqz v2, :cond_2

    .line 102
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 104
    :cond_2
    throw v0

    .line 105
    :cond_3
    invoke-virtual {v3}, Landroid/net/http/SslError;->getPrimaryError()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 100
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_3

    .line 97
    :catch_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v7, v1

    move-object v1, v3

    move-object v3, v0

    move-object v0, v7

    goto :goto_2

    :cond_4
    move-object v3, v1

    goto :goto_0
.end method

.method public hH()Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;->vj:Ljava/security/cert/X509Certificate;

    return-object v0
.end method
