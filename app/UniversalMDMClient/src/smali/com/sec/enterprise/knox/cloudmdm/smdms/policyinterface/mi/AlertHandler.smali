.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;
.super Ljava/lang/Object;
.source "AlertHandler.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:AlertHandler"


# instance fields
.field protected final admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field protected final alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

.field protected final contextInfo:Landroid/app/enterprise/ContextInfo;

.field protected final mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

.field protected final mContext:Landroid/content/Context;

.field private final mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

.field protected final mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

.field protected final mgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

.field protected final policyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V
    .locals 6

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    .line 34
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 35
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    .line 36
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 37
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    .line 38
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mContext:Landroid/content/Context;

    .line 39
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->container:Ljava/lang/Integer;

    invoke-virtual {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getContextInfo(Ljava/lang/Integer;)Landroid/app/enterprise/ContextInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    .line 40
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyClassHolder()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->policyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    .line 41
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveIntents:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveIntents:Ljava/util/Map;

    .line 42
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeIntents:Ljava/util/Map;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v5, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeIntents:Ljava/util/Map;

    .line 43
    :goto_1
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 45
    :cond_0
    if-eqz v0, :cond_3

    .line 46
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v2

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;-><init>(Landroid/content/Context;ILcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    .line 50
    :goto_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->initSignature()V

    .line 51
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveCallbacks:Ljava/util/Map;

    goto :goto_0

    .line 42
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    iget-object v5, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->postApiActiveRunTimeCallbacks:Ljava/util/Map;

    goto :goto_1

    .line 48
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    goto :goto_2
.end method

.method private formErrorDescription(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", Cause : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private sendAlert(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    const-string v0, "UMC:AlertHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendAlert action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->formErrorDescription(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v7

    .line 78
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->admin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->user:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->policyDocumentId:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->sendAlert(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/AlertReport;)V

    .line 81
    return-void
.end method


# virtual methods
.method public OnAlert(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->sendAlert(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;)V

    .line 74
    return-void
.end method

.method public getAlertRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    return-object v0
.end method

.method public onIntentOccurence(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;Landroid/content/Intent;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Exception;",
            "Landroid/content/Intent;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 94
    const-string v0, "UMC:AlertHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onIntentOccurence action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->sendAlert(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;)V

    .line 96
    return-void
.end method

.method public schedule(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 100
    const-string v0, "UMC:AlertHandler"

    const-string v1, "schedule"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;->schedule(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 54
    const-string v0, "UMC:AlertHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "start Alert Session : Method = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->registerIntents()V

    .line 58
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "UMC:AlertHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stop Alert Session : Method = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;->mIntentHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->unRegisterIntents()V

    .line 65
    :cond_0
    return-void
.end method
