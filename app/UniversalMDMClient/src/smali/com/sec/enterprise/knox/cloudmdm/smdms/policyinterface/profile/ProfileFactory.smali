.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileFactory;
.super Ljava/lang/Object;
.source "ProfileFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createInstance(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;
    .locals 6

    .prologue
    .line 44
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    const-string v1, "DomProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    .line 57
    :goto_0
    return-object v0

    .line 46
    :cond_0
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    const-string v1, "MapProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "exchangeAccounts"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/port/MapProfileForExchangeAccount;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    goto :goto_0

    .line 48
    :cond_1
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    const-string v1, "MapProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/MapProfile;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/MapProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    const-string v1, "ListProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ListProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :cond_3
    iget-object v0, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;->profileType:Ljava/lang/String;

    const-string v1, "ObjectProfile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ObjectProfile;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ObjectProfile;-><init>(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
