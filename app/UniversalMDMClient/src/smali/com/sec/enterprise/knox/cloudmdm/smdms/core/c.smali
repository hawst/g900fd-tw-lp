.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;
.super Landroid/content/BroadcastReceiver;
.source "Core.java"


# instance fields
.field final synthetic pu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V
    .locals 0

    .prologue
    .line 1161
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;->pu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1165
    if-nez p2, :cond_1

    .line 1196
    :cond_0
    :goto_0
    return-void

    .line 1168
    :cond_1
    const-string v0, "UMC:Core"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CoreLocalReceiver:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    if-ne v0, v1, :cond_0

    .line 1171
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;->pu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;)V

    .line 1173
    const-string v0, "status"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1185
    const/4 v0, 0x0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->py:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;->b(Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core$State;)V

    goto :goto_0

    .line 1188
    :cond_2
    const-string v0, "packageName"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1189
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/c;->pu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;Ljava/lang/String;)V

    .line 1190
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->q(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1191
    const v1, 0x7f0800ba

    .line 1192
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1193
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fz()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
