.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;
.super Ljava/lang/Object;
.source "CarouselUi.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/b;)V
.end annotation


# instance fields
.field final synthetic zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 144
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V

    .line 142
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Posting delay again"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a$1;->zK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/a;)V

    goto :goto_1
.end method
