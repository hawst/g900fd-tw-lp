.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;
.super Ljava/lang/Object;
.source "AccountInfo.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private initialPassword:Ljava/lang/String;

.field private profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    return-void
.end method


# virtual methods
.method public getInitialPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;->initialPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    return-object v0
.end method

.method public setInitialPassword(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;->initialPassword:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/proxy/AccountInfo;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 62
    return-void
.end method
