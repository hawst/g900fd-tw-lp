.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;
.super Ljava/lang/Thread;
.source "KnoxQuickStartManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V
.end annotation


# instance fields
.field final synthetic sA:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

.field private final synthetic sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sA:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    .line 973
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v5, 0x1

    .line 983
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gR()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDeviceId()Ljava/lang/String;

    move-result-object v9

    .line 985
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->server:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->sC:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->username:Ljava/lang/String;

    .line 986
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v4, v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->password:Ljava/lang/String;

    const-string v7, "0"

    const-string v8, "14.0"

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    iget-object v10, v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;->email:Ljava/lang/String;

    move v6, v5

    .line 985
    invoke-direct/range {v0 .. v10}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->hj()Z

    move-result v1

    if-nez v1, :cond_0

    .line 989
    const-string v0, "UMC:KnoxQuickStartManager"

    const-string v1, "error: ActiveSyncManager not initialized"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    :goto_0
    return-void

    .line 993
    :cond_0
    const/4 v1, 0x0

    .line 996
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->hk()[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1001
    :goto_1
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;-><init>()V

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;)V

    .line 1002
    if-eqz v1, :cond_1

    .line 1004
    const-string v1, "AllowSimpleDevicePassword"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1005
    const-string v2, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EasPasswordPolicies.ALLOW_SIMPLE_DEVICE_PASSWORD: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->ad(Ljava/lang/String;)V

    .line 1009
    const-string v1, "AlphanumericDevicePasswordRequired"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1010
    const-string v2, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EasPasswordPolicies.ALPHANUMERIC_DEVICE_PASSWORD_REQUIRED: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1011
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1010
    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->ae(Ljava/lang/String;)V

    .line 1014
    const-string v1, "DevicePasswordEnabled"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1015
    const-string v2, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EasPasswordPolicies.DEVICE_PASSWORD_ENABLED: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->ac(Ljava/lang/String;)V

    .line 1019
    const-string v1, "MinDevicePasswordComplexCharacters"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1020
    const-string v2, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EasPasswordPolicies.MIN_DEVICE_PASSWORD_COMPLEX_CHARS: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->af(Ljava/lang/String;)V

    .line 1023
    const-string v1, "MinDevicePasswordLength"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1024
    const-string v2, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EasPasswordPolicies.MIN_DEVICE_PASSWORD_LENGTH: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->ag(Ljava/lang/String;)V

    .line 1028
    const-string v1, "MaxDevicePasswordFailedAttempts"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1029
    const-string v2, "UMC:KnoxQuickStartManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EasPasswordPolicies.MAX_DEVICE_PASSWORD_FAILED_ATTEMPTS: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->ah(Ljava/lang/String;)V

    .line 1033
    const-string v1, "MaxInactivityTimeDeviceLock"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/a;->an(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1034
    const-string v1, "UMC:KnoxQuickStartManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EasPasswordPolicies.MAX_INACTIVITY_TIME_DEVICE_LOCK: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->hc()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a;->ai(Ljava/lang/String;)V

    .line 1038
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gS()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;->sJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$1;->sB:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$State;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager$StateMessage;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 997
    :catch_0
    move-exception v2

    .line 998
    const-string v3, "UMC:KnoxQuickStartManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "error"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
