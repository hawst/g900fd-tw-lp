.class public final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;
.super Ljava/lang/Enum;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum xl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

.field public static final enum xm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

.field public static final enum xn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

.field public static final enum xo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

.field private static final synthetic xp:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    const-string v1, "ERROR_10000"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    const-string v1, "ERROR_20000"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    const-string v1, "ERROR_30000"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    const-string v1, "ERROR_40000"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    .line 46
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xp:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;->xp:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_ERROR_CODES;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
