.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity$6;
.super Ljava/lang/Object;
.source "MdmAgentInstallActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jc()V
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 407
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jj()I

    move-result v0

    const/16 v1, 0x63

    if-lt v0, v1, :cond_1

    .line 408
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 409
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 410
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 411
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ji()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0800b5

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 412
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 411
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 413
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 414
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 415
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 416
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x7f0800b4

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 429
    :goto_0
    return-void

    .line 419
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->ji()Landroid/app/Activity;

    move-result-object v2

    .line 420
    const v3, 0x7f080064

    .line 419
    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 420
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 421
    const-string v2, "                         "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 422
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jj()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 423
    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 419
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 424
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jk()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->jj()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
