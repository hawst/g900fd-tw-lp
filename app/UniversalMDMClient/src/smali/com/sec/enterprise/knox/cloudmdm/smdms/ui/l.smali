.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;
.super Landroid/app/Fragment;
.source "NewEnrollmentFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a/d;


# instance fields
.field private sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

.field private yS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/EditText;",
            ">;"
        }
    .end annotation
.end field

.field private zk:Landroid/widget/Button;

.field private zl:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    return-object v0
.end method

.method private a(Landroid/widget/EditText;I)V
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;Landroid/widget/EditText;)V

    .line 256
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 257
    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    return-object v0
.end method

.method private iQ()V
    .locals 7

    .prologue
    const v6, 0x7f090001

    .line 193
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 194
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->c(Ljava/util/HashMap;)V

    .line 200
    return-void

    .line 194
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 195
    const-string v3, "UMC:NewEnrollmentFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Storing Form Data {"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 196
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 195
    invoke-static {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private iR()Z
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    check-cast v0, Landroid/content/Context;

    .line 211
    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 210
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 212
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const/4 v0, 0x1

    .line 217
    :goto_0
    return v0

    .line 216
    :cond_0
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "Network is not available"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private iS()V
    .locals 3

    .prologue
    .line 222
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 223
    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 222
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 224
    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 226
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    .line 225
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private jm()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->E(Ljava/lang/String;)V

    .line 207
    :goto_0
    return-void

    .line 206
    :cond_0
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "Null email"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public b(ZI)V
    .locals 3

    .prologue
    .line 273
    const-string v0, "UMC:NewEnrollmentFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "never show: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->a(Landroid/content/Context;Z)Z

    .line 275
    return-void
.end method

.method public bd(I)V
    .locals 2

    .prologue
    .line 261
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "Yes occured!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->iQ()V

    .line 263
    return-void
.end method

.method public be(I)V
    .locals 2

    .prologue
    .line 267
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "No occured!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->C(Z)V

    .line 269
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 140
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "onAttach"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 143
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    const-string v1, "UMC:NewEnrollmentFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 152
    packed-switch v0, :pswitch_data_0

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 156
    :pswitch_0
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "enroll button clicked"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    .line 158
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    .line 157
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z

    move-result v0

    .line 158
    if-eqz v0, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->iS()V

    .line 163
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->iR()Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    const v1, 0x7f08009e

    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 165
    const v2, 0x7f08009a

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-interface {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->x(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->jm()V

    goto :goto_0

    .line 186
    :pswitch_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;->C(Z)V

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x7f090010
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 75
    const v0, 0x7f030006

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 77
    const-string v0, "UMC:NewEnrollmentFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateView "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v0, "UMC:NewEnrollmentFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateView "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->setRetainInstance(Z)V

    .line 81
    const v0, 0x7f090011

    :try_start_0
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    .line 82
    const v0, 0x7f090010

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zl:Landroid/widget/Button;

    .line 84
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 85
    if-eqz v3, :cond_0

    .line 87
    const v0, 0x7f09000d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 89
    const-string v1, "fragmentTitleMessage"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 90
    const v1, 0x7f09000e

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 91
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 92
    const-string v1, "btnEnrollText"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 93
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    invoke-virtual {v5, v1}, Landroid/widget/Button;->setText(I)V

    .line 94
    const-string v1, "fragmentFooterText"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 95
    const v1, 0x7f09000f

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 98
    const-string v1, "ui.uaf"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v5

    .line 99
    array-length v1, v5

    new-array v6, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    move v3, v2

    .line 100
    :goto_0
    array-length v1, v5

    if-lt v3, v1, :cond_1

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    .line 106
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 107
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    .line 106
    invoke-static {v1, v6, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    .line 111
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 118
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zl:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :goto_2
    return-object v4

    .line 101
    :cond_1
    aget-object v1, v5, v3

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    aput-object v1, v6, v3

    .line 100
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 111
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 112
    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->a(Landroid/widget/EditText;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    goto :goto_1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 122
    const-string v0, "UMC:NewEnrollmentFragment"

    const-string v1, "Error!!! Email Fragment messed up"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 129
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 131
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->yS:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->zk:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
