.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
.super Ljava/lang/Object;
.source "Admin.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:Admin"


# instance fields
.field private bShareCommonThreadAllAdmins:Z

.field private mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

.field mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

.field private mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

.field private mPolicyApplierThreadContext:Landroid/os/Handler;

.field private mPolicyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

.field private mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

.field private mPolicyScriptHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Landroid/app/admin/ProxyDeviceAdminInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->bShareCommonThreadAllAdmins:Z

    .line 95
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    .line 96
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 97
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 98
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    .line 99
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getDefaultPolicyListener()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->setListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler$PolicyListener;)V

    .line 101
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    .line 102
    return-void
.end method

.method private static addAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;[BLjava/lang/String;[Ljava/lang/String;)Landroid/app/admin/ProxyDeviceAdminInfo;
    .locals 8

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v6

    .line 408
    new-instance v1, Landroid/content/pm/ResolveInfo;

    invoke-direct {v1}, Landroid/content/pm/ResolveInfo;-><init>()V

    .line 409
    new-instance v7, Landroid/content/pm/ActivityInfo;

    invoke-direct {v7}, Landroid/content/pm/ActivityInfo;-><init>()V

    .line 410
    new-instance v0, Landroid/content/pm/ApplicationInfo;

    invoke-direct {v0}, Landroid/content/pm/ApplicationInfo;-><init>()V

    .line 411
    invoke-virtual {p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    .line 412
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 414
    iput p1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 415
    iput-object v0, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 416
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 417
    invoke-virtual {p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 418
    iput-object v7, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 421
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 422
    if-eqz p6, :cond_0

    .line 423
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 425
    :cond_0
    new-instance v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    move-object v2, p3

    move-object v3, p5

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Landroid/app/admin/ProxyDeviceAdminInfo;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Ljava/lang/String;[BLjava/util/List;)V

    .line 426
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getDummyParentComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v6, v0, p1, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->addProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V

    .line 427
    const-string v1, "UMC:Admin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EDM Proxy Admin Added UID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "and package name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-virtual {v6, p2}, Landroid/app/enterprise/EnterpriseDeviceManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 431
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUserId(I)I

    move-result v2

    invoke-virtual {v6, p2, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->activateAdminForUser(Landroid/content/ComponentName;ZI)V

    .line 432
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->clearProxyAdminInstallBlock(Landroid/content/Context;ILjava/lang/String;)V

    .line 433
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v1, p1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 434
    new-instance v2, Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 435
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->setAdminRemovable(Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 436
    const-string v0, "UMC:Admin"

    const-string v1, "setAdminRemovable failed - Removing the Admin"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-virtual {v2, p2}, Landroid/app/enterprise/EnterpriseDeviceManager;->removeActiveAdmin(Landroid/content/ComponentName;)V

    .line 438
    invoke-virtual {v2, p1}, Landroid/app/enterprise/EnterpriseDeviceManager;->removeProxyAdmin(I)V

    .line 439
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EDM Proxy Admin Removed UID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "and package name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    const/4 v0, 0x0

    .line 449
    :cond_1
    :goto_0
    return-object v0

    .line 442
    :catch_0
    move-exception v0

    .line 443
    const-string v1, "UMC:Admin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activateAdmin Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-virtual {v6, p1}, Landroid/app/enterprise/EnterpriseDeviceManager;->removeProxyAdmin(I)V

    .line 445
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static attachToPlatformAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 298
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v3

    .line 299
    invoke-static {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPackageAdminInfo(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;

    move-result-object v1

    .line 300
    if-nez v1, :cond_0

    .line 315
    :goto_0
    return-object v0

    .line 303
    :cond_0
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    iget v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    invoke-direct {v4, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 304
    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    invoke-virtual {v3, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 306
    :try_start_0
    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    const/4 v5, 0x0

    iget v6, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    invoke-static {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUserId(I)I

    move-result v6

    invoke-virtual {v3, v2, v5, v6}, Landroid/app/enterprise/EnterpriseDeviceManager;->activateAdminForUser(Landroid/content/ComponentName;ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    :cond_1
    :goto_1
    new-instance v2, Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v4, v0}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 313
    invoke-virtual {v2, v8}, Landroid/app/enterprise/EnterpriseDeviceManager;->setAdminRemovable(Z)Z

    .line 314
    const-string v0, "UMC:Admin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attached to EDM Proxy Admin UID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "and package name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 315
    goto :goto_0

    .line 307
    :catch_0
    move-exception v2

    .line 308
    const-string v5, "UMC:Admin"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "activateAdmin Exception = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v2, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    iget v5, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    invoke-static {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->getUserId(I)I

    move-result v5

    invoke-virtual {v3, v2, v5}, Landroid/app/enterprise/EnterpriseDeviceManager;->deactivateAdminForUser(Landroid/content/ComponentName;I)V

    goto :goto_1
.end method

.method public static clearProxyAdminInstallBlock(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 477
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 478
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v1, p1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const/4 v2, 0x0

    .line 477
    invoke-direct {v0, p0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 479
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 480
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationInstallationDisabled(Ljava/lang/String;Z)V

    .line 482
    return-void
.end method

.method static createCloudAdminInstance(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ILandroid/content/ComponentName;[Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 111
    move-object v0, p0

    move v1, p5

    move-object v2, p6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p7

    invoke-static/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->addAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;[BLjava/lang/String;[Ljava/lang/String;)Landroid/app/admin/ProxyDeviceAdminInfo;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_0

    .line 113
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    invoke-direct {v2, p1, p5, p6, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;-><init>(Ljava/lang/String;ILandroid/content/ComponentName;Ljava/util/List;)V

    .line 114
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Landroid/app/admin/ProxyDeviceAdminInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;)V

    .line 115
    const-string v1, "UMC:Admin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "createCloudAdminInstance - UMC Admin Added for Admin UID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v7

    goto :goto_0
.end method

.method static createCloudAdminInstanceSharingOwnershipWithLocalAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 8

    .prologue
    .line 124
    invoke-static {p0, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getPackageAdminInfo(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;

    move-result-object v7

    .line 126
    if-eqz v7, :cond_0

    .line 127
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p7, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    .line 129
    iget v1, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    iget-object v2, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->addAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;[BLjava/lang/String;[Ljava/lang/String;)Landroid/app/admin/ProxyDeviceAdminInfo;

    move-result-object v1

    .line 130
    if-eqz v1, :cond_0

    .line 131
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v0, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    iget-object v3, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    const/4 v4, 0x0

    invoke-direct {v2, p1, v0, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;-><init>(Ljava/lang/String;ILandroid/content/ComponentName;Ljava/util/List;)V

    .line 132
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Landroid/app/admin/ProxyDeviceAdminInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;)V

    .line 133
    const-string v1, "UMC:Admin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "createCloudAdminInstanceSharingOwnershipWithLocalAdmin - UMC Admin Added for Admin UID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static createCloudAdminInstanceSharingOwnershipWithLocalAdminWithoutProxyAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 142
    invoke-static {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->attachToPlatformAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;

    move-result-object v2

    .line 143
    if-eqz v2, :cond_0

    .line 144
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    iget-object v4, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    invoke-direct {v3, p1, v0, v4, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;-><init>(Ljava/lang/String;ILandroid/content/ComponentName;Ljava/util/List;)V

    .line 145
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-direct {v0, p0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Landroid/app/admin/ProxyDeviceAdminInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;)V

    .line 146
    const-string v1, "UMC:Admin"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createCloudAdminInstanceSharingOwnershipWithLocalAdminWithoutProxyAdmin - UMC Admin Added for Admin UID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private static getPackageAdminInfo(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 319
    .line 320
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 321
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 322
    if-nez v3, :cond_0

    move-object v0, v1

    .line 346
    :goto_0
    return-object v0

    .line 325
    :cond_0
    const-string v2, "device_policy"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 326
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getActiveAdmins()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 327
    if-nez v0, :cond_1

    move-object v0, v1

    .line 328
    goto :goto_0

    .line 329
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 346
    goto :goto_0

    .line 329
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 330
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 331
    const-string v5, "UMC:Admin"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Current admin loop :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 334
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 335
    if-eqz v5, :cond_2

    .line 336
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;-><init>()V

    .line 337
    iput-object v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminCN:Landroid/content/ComponentName;

    .line 338
    iget v0, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$PackageAdminInfo;->adminUId:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 339
    goto :goto_0

    .line 341
    :catch_0
    move-exception v0

    .line 342
    const-string v2, "UMC:Admin"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private removeAdmin(ILandroid/content/ComponentName;I)V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    if-eqz v0, :cond_0

    .line 283
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, p1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 284
    new-instance v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 285
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->setAdminRemovable(Z)Z

    .line 286
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0, p2, p3}, Landroid/app/enterprise/EnterpriseDeviceManager;->deactivateAdminForUser(Landroid/content/ComponentName;I)V

    .line 287
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0, p1}, Landroid/app/enterprise/EnterpriseDeviceManager;->removeProxyAdmin(I)V

    .line 288
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EDM Proxy Admin Removed UID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "and package name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_0
    return-void
.end method

.method private static updateAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;[Ljava/lang/String;)Landroid/app/admin/ProxyDeviceAdminInfo;
    .locals 14

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v6

    .line 356
    new-instance v1, Landroid/content/pm/ResolveInfo;

    invoke-direct {v1}, Landroid/content/pm/ResolveInfo;-><init>()V

    .line 357
    new-instance v0, Landroid/content/pm/ActivityInfo;

    invoke-direct {v0}, Landroid/content/pm/ActivityInfo;-><init>()V

    .line 358
    new-instance v2, Landroid/content/pm/ApplicationInfo;

    invoke-direct {v2}, Landroid/content/pm/ApplicationInfo;-><init>()V

    .line 359
    invoke-virtual/range {p2 .. p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    .line 360
    invoke-virtual/range {p2 .. p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 362
    iput p1, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 363
    iput-object v2, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 364
    invoke-virtual/range {p2 .. p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 365
    invoke-virtual/range {p2 .. p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 366
    iput-object v0, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 369
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 370
    if-eqz p7, :cond_0

    .line 371
    new-instance v5, Ljava/util/ArrayList;

    invoke-static/range {p7 .. p7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 373
    :cond_0
    new-instance v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    move-object/from16 v2, p4

    move-object/from16 v3, p6

    move-object/from16 v4, p5

    invoke-direct/range {v0 .. v5}, Landroid/app/admin/ProxyDeviceAdminInfo;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Ljava/lang/String;[BLjava/util/List;)V

    .line 375
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getDummyParentComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v6, v0, p1, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->updateProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V

    .line 376
    const-string v1, "UMC:Admin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EDM Proxy Admin Updated UID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "and package name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, p0

    move v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object v13, v5

    .line 377
    invoke-static/range {v6 .. v13}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->updateChildAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/util/List;)V

    .line 378
    return-object v0
.end method

.method private static updateChildAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;",
            "I",
            "Landroid/content/ComponentName;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v6

    .line 388
    const/4 v0, -0x1

    invoke-virtual {v6, v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getProxyAdmins(I)Ljava/util/List;

    move-result-object v0

    .line 389
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 403
    return-void

    .line 389
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 391
    invoke-virtual {v1}, Landroid/app/admin/ProxyDeviceAdminInfo;->getType()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 392
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/admin/ProxyDeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {v1}, Landroid/app/admin/ProxyDeviceAdminInfo;->getReceiver()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 394
    iget v0, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v0, p1, :cond_0

    .line 395
    new-instance v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    invoke-virtual {v1}, Landroid/app/admin/ProxyDeviceAdminInfo;->getReceiver()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    move-object v2, p4

    move-object v3, p6

    move-object v4, p5

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v5}, Landroid/app/admin/ProxyDeviceAdminInfo;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Ljava/lang/String;[BLjava/util/List;)V

    .line 396
    iget v1, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->updateProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V

    .line 397
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EDM Proxy Admin Updated UID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 398
    const-string v2, "and package name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addUser(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User Added for Admin UID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " user = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_0
    return-void
.end method

.method public checkPermission(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 258
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    if-eqz v1, :cond_1

    .line 259
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    invoke-virtual {v1}, Landroid/app/admin/ProxyDeviceAdminInfo;->getRequestedPermissions()Ljava/util/List;

    move-result-object v1

    .line 260
    if-eqz v1, :cond_2

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 274
    :cond_0
    :goto_0
    return v0

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 265
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 266
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 267
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    :cond_2
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "checkPermission in UMC failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "and package name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearThreadContext()V
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->bShareCommonThreadAllAdmins:Z

    if-nez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyApplierThreadContext:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 474
    :cond_0
    return-void
.end method

.method public deleteAdmin()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Admin "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Delete - Start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->clearThreadContext()V

    .line 156
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->removeAllUsers()V

    .line 157
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->cancel()V

    .line 158
    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 159
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->removeAdmin(ILandroid/content/ComponentName;I)V

    .line 160
    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    .line 161
    iput-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 162
    const-string v0, "UMC:Admin"

    const-string v1, "Admin  Delete - Complete"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public getAllManagedUsers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    if-nez v0, :cond_0

    .line 212
    const/4 v0, 0x0

    .line 214
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    goto :goto_0
.end method

.method public getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    return-object v0
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getContextInfo(Ljava/lang/Integer;)Landroid/app/enterprise/ContextInfo;
    .locals 3

    .prologue
    .line 246
    .line 247
    if-eqz p1, :cond_0

    .line 248
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    .line 252
    :goto_0
    return-object v0

    .line 250
    :cond_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    goto :goto_0
.end method

.method public getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    return-object v0
.end method

.method public getPlatformAdminInfo()Landroid/app/admin/ProxyDeviceAdminInfo;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    return-object v0
.end method

.method public getPolicyClassHolder()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    return-object v0
.end method

.method public getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    return-object v0
.end method

.method public getPolicyScriptHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyScriptHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/script/PolicyScriptHandler;

    return-object v0
.end method

.method public getThreadContext()Landroid/os/Handler;
    .locals 3

    .prologue
    .line 454
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->bShareCommonThreadAllAdmins:Z

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getThreadContext()Landroid/os/Handler;

    move-result-object v0

    .line 465
    :goto_0
    return-object v0

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyApplierThreadContext:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 461
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdminThread - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 462
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 463
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyApplierThreadContext:Landroid/os/Handler;

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPolicyApplierThreadContext:Landroid/os/Handler;

    goto :goto_0
.end method

.method public hasManagedUsers()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mMDMUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isUserExist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x1

    .line 191
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAllUsers()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    if-nez v0, :cond_1

    .line 208
    :cond_0
    return-void

    .line 205
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->removeUser(Ljava/lang/String;)V

    .line 205
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public removeUser(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mManagedUsers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 183
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User Removed for Admin UID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " user = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_0
    return-void
.end method

.method public updateAdminPermission([Ljava/lang/String;)Z
    .locals 8

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 167
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mCN:Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mMDMUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    invoke-virtual {v4, v6}, Landroid/app/admin/ProxyDeviceAdminInfo;->getLabel(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    invoke-virtual {v5}, Landroid/app/admin/ProxyDeviceAdminInfo;->getIcon()[B

    move-result-object v5

    .line 168
    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    invoke-virtual {v7, v6}, Landroid/app/admin/ProxyDeviceAdminInfo;->getDescription(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v6

    move-object v7, p1

    .line 167
    invoke-static/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->updateAdmin(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;ILandroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;[Ljava/lang/String;)Landroid/app/admin/ProxyDeviceAdminInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mPlatformAdminInfo:Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 169
    const-string v0, "UMC:Admin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateAdminPermission for UID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->mAppAdminInfo:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    iget v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Permissions = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v0, 0x1

    return v0
.end method
