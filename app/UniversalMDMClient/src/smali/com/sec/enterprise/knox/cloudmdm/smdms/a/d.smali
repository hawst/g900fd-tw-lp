.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;
.super Ljava/lang/Object;
.source "PackageManagerAdapter.java"


# static fields
.field public static DELETE_SUCCEEDED:I

.field public static INSTALL_FAILED_INVALID_URI:I

.field public static INSTALL_INTERNAL:I

.field public static INSTALL_REPLACE_EXISTING:I

.field public static INSTALL_SUCCEEDED:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 48
    const/4 v0, -0x3

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_FAILED_INVALID_URI:I

    .line 49
    const/16 v0, 0x10

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_INTERNAL:I

    .line 50
    const/4 v0, 0x2

    sput v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_REPLACE_EXISTING:I

    .line 51
    sput v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_SUCCEEDED:I

    .line 52
    sput v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->DELETE_SUCCEEDED:I

    return-void
.end method

.method public static a(Landroid/content/pm/PackageManager;Landroid/net/Uri;Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    .line 89
    return-void
.end method

.method public static a(Landroid/content/pm/PackageManager;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;I)V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    .line 85
    return-void
.end method

.method public static fh()Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;

    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;-><init>(Landroid/content/pm/IPackageManager;)V

    return-object v0
.end method
