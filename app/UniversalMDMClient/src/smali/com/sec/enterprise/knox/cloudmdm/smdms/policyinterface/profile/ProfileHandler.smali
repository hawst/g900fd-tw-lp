.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;
.super Ljava/lang/Object;
.source "ProfileHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:ProfileHandler"


# instance fields
.field private activeProfileCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;",
            ">;"
        }
    .end annotation
.end field

.field private activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

.field private activeProfileState:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

.field private mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

.field private pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

.field private policyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

.field private policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

.field private profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

.field private profileConfigHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

.field private rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;)V
    .locals 4

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileCache:Ljava/util/Map;

    .line 87
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileConfigHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    .line 94
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getAdmin()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    .line 95
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    .line 96
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;->getPolicyBackup()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->policyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    .line 98
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;->restore(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    .line 99
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    .line 103
    :cond_0
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;->restore(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileState:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    .line 104
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileState:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    if-nez v0, :cond_1

    .line 105
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileState:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    .line 108
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    move-result-object v0

    const-string v1, "RootProfile"

    invoke-virtual {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;->load(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;

    move-result-object v0

    .line 109
    const-string v1, "rootProfile"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;->activeProfile:Ljava/lang/Object;

    invoke-static {v1, v2, p0, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileFactory;->createInstance(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    .line 110
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->init()Z

    .line 111
    return-void
.end method

.method static synthetic access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)Z
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->applyProfileInternal()Z

    move-result v0

    return v0
.end method

.method private applyProfileInternal()Z
    .locals 8

    .prologue
    const/16 v7, 0x2e

    const/4 v6, 0x1

    .line 152
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    if-nez v0, :cond_0

    .line 201
    :goto_0
    return v6

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;->profiles:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->apply(Ljava/lang/Object;)Z

    move-result v0

    .line 158
    if-nez v0, :cond_1

    .line 159
    const-string v0, "UMC:ProfileHandler"

    const-string v1, "profile.apply : failed "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->onPoliciesAppliedUpdate()V

    goto :goto_0

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->profile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;->profiles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 200
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->onPoliciesAppliedUpdate()V

    goto :goto_0

    .line 165
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 166
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 167
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 168
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->findProfile(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_5

    .line 170
    invoke-virtual {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->apply(Ljava/lang/Object;)Z

    move-result v0

    .line 171
    if-nez v0, :cond_3

    .line 172
    const-string v0, "UMC:ProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "profile.apply : failed for id = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 177
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    .line 179
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 180
    const/4 v0, 0x0

    invoke-virtual {v1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-virtual {v1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 183
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    invoke-static {v5, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->realizeDomTree(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;Ljava/lang/String;)Z

    move-result v5

    .line 184
    if-nez v5, :cond_6

    .line 185
    const-string v0, "UMC:ProfileHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Error : Dom not found "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 188
    :cond_6
    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->findProfile(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    move-result-object v2

    .line 191
    :goto_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 192
    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    invoke-virtual {v2, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->apply(Ljava/lang/Object;)Z

    move-result v0

    .line 195
    if-nez v0, :cond_3

    .line 196
    const-string v0, "UMC:ProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "profile.apply : failed for id = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    move-object v2, v0

    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public addProfileToCache(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;)V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileCache:Ljava/util/Map;

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    return-void
.end method

.method public applyProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;)Z
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->hasAnyPendingNodes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "UMC:ProfileHandler"

    const-string v1, "Busy Applying Previous Profile. Apply aftr sometime"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x0

    .line 122
    :goto_0
    return v0

    .line 119
    :cond_0
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    .line 120
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-direct {v0, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/AdminProfile;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    .line 121
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->backup()V

    .line 122
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->applyProfileInternal()Z

    move-result v0

    goto :goto_0
.end method

.method public cancel()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public findProfile(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/BaseProfile;

    return-object v0
.end method

.method public getActiveProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    return-object v0
.end method

.method public getActiveProfileState()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileState:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfileState;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getManager()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getPendingProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    return-object v0
.end method

.method public getPolicyBackup()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->policyBackup:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyBackup;

    return-object v0
.end method

.method public getPolicyHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->policyHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyHandler;

    return-object v0
.end method

.method public getProfileConfigHandler()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileConfigHandler:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler;

    return-object v0
.end method

.method public onPoliciesAppliedUpdate()V
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->hasAnyPendingNodes()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->saveProfile()V

    .line 262
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;->activeProfile:Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->reports:Ljava/util/Map;

    invoke-direct {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;-><init>(Ljava/lang/Object;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;->onProfileApplied(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;)V

    .line 264
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->delete()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    .line 268
    :cond_0
    return-void
.end method

.method public profileUpdated()V
    .locals 0

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->saveProfile()V

    .line 222
    return-void
.end method

.method public saveProfile()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->rootProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/DomProfile;->data:Ljava/lang/Object;

    iput-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;->activeProfile:Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;->backup(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)V

    .line 248
    return-void
.end method

.method public setProfileListener(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;)V
    .locals 4

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    .line 273
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->hasAnyPendingNodes()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->profileApplyListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->activeProfileData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ActiveProfile;->activeProfile:Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->reports:Ljava/util/Map;

    invoke-direct {v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;-><init>(Ljava/lang/Object;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileListener;->onProfileApplied(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$ProfileApplyResult;)V

    .line 277
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;->delete()V

    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;->pendingProfile:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/PendingProfile;

    .line 281
    :cond_0
    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    .line 206
    :try_start_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;

    invoke-direct {v0, p0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler;)V

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileHandler$PendingProfileHandler;->schedule()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v0

    .line 208
    const-string v1, "UMC:ProfileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception in start() - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
