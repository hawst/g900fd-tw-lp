.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;
.super Landroid/app/Fragment;
.source "ManualSetupFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/b;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/b;


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static tj:Ljava/lang/String;

.field public static tk:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mInflater:Landroid/view/LayoutInflater;

.field sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

.field sk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

.field sl:Landroid/app/ProgressDialog;

.field ta:Landroid/widget/Button;

.field tb:Landroid/widget/CheckBox;

.field private tc:Landroid/widget/EditText;

.field private td:Landroid/widget/EditText;

.field private te:Landroid/widget/EditText;

.field private tf:Landroid/widget/EditText;

.field private tg:Landroid/widget/LinearLayout;

.field th:Landroid/widget/Button;

.field private ti:Landroid/widget/CheckBox;

.field private tl:Landroid/net/http/SslCertificate;

.field private tm:Landroid/app/AlertDialog;

.field private tn:Z

.field private to:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->TAG:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 70
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/b;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

    .line 82
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tl:Landroid/net/http/SslCertificate;

    .line 83
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tm:Landroid/app/AlertDialog;

    .line 84
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mInflater:Landroid/view/LayoutInflater;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->to:Z

    .line 49
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tn:Z

    return-void
.end method

.method private a(Landroid/widget/TextView;)Z
    .locals 2

    .prologue
    .line 382
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    :cond_0
    const/4 v0, 0x0

    .line 386
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static am(Ljava/lang/String;)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 482
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 486
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v1

    .line 490
    :goto_0
    if-eqz v2, :cond_4

    .line 492
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 493
    const-wide/32 v4, 0x7fffffff

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 494
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The file is too big"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 487
    :catch_0
    move-exception v1

    .line 488
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v2, v0

    goto :goto_0

    .line 498
    :cond_0
    long-to-int v0, v0

    new-array v1, v0, [B

    .line 500
    const/4 v0, 0x0

    .line 502
    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 503
    array-length v4, v1

    sub-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    .line 502
    if-gez v4, :cond_2

    .line 507
    :cond_1
    array-length v4, v1

    if-ge v0, v4, :cond_3

    .line 508
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The file was not completely read: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    :cond_2
    add-int/2addr v0, v4

    goto :goto_1

    .line 512
    :cond_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    move-object v0, v1

    .line 514
    :cond_4
    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private b(Landroid/widget/TextView;)Z
    .locals 2

    .prologue
    .line 390
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/widget/TextView;)Z
    .locals 2

    .prologue
    .line 395
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 396
    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    :cond_0
    const/4 v0, 0x0

    .line 399
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private gH()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 108
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getView()Landroid/view/View;

    move-result-object v2

    .line 109
    if-nez v2, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    .line 113
    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;-><init>(Landroid/content/res/Resources;)V

    .line 114
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->it()I

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const v0, 0x7f09005b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 115
    check-cast v0, Landroid/widget/LinearLayout;

    .line 118
    const v1, 0x7f09005d

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 117
    check-cast v1, Landroid/widget/LinearLayout;

    .line 120
    const v5, 0x7f09005c

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 119
    check-cast v2, Landroid/widget/LinearLayout;

    .line 122
    if-eqz v0, :cond_0

    .line 125
    const/4 v5, 0x2

    if-ne v3, v5, :cond_2

    .line 126
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 127
    invoke-virtual {v4, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bc(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->e(F)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 128
    invoke-virtual {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bc(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->e(F)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    goto :goto_0

    .line 130
    :cond_2
    if-ne v3, v10, :cond_0

    .line 131
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 132
    invoke-virtual {v4, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bc(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->e(F)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    .line 133
    invoke-virtual {v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->a(Landroid/widget/LinearLayout;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->bc(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;->e(F)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/i;

    goto :goto_0
.end method

.method private gI()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 363
    const/4 v0, 0x1

    .line 364
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tc:Landroid/widget/EditText;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->a(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 368
    :cond_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->c(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 372
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->td:Landroid/widget/EditText;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->b(Landroid/widget/TextView;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 373
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->te:Landroid/widget/EditText;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->b(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 377
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->y(Z)V

    .line 378
    return v0
.end method

.method private y(Z)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->th:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 406
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 435
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 437
    if-nez p1, :cond_3

    .line 438
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->TAG:Ljava/lang/String;

    const-string v2, "Validator: VALID EMAIL"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tn:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tj:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 442
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tj:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->am(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 447
    :goto_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->gL()Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;

    move-result-object v0

    .line 448
    iget-boolean v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tn:Z

    if-eqz v2, :cond_2

    sget-object v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tk:Ljava/lang/String;

    :goto_1
    move-object v1, p4

    move-object v2, p2

    move-object v3, p6

    move-object v4, p3

    move-object v5, p5

    .line 447
    invoke-virtual/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/KnoxQuickStartManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V

    .line 457
    :cond_0
    :goto_2
    return-void

    .line 443
    :catch_0
    move-exception v0

    .line 444
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    move-object v6, v1

    goto :goto_0

    :cond_2
    move-object v7, v1

    .line 448
    goto :goto_1

    .line 449
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->aR(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 451
    const v3, 0x7f0800b1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 452
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 453
    const v0, 0x7f080058

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 454
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_2
.end method

.method aR(I)I
    .locals 2

    .prologue
    const v0, 0x7f08001a

    .line 460
    packed-switch p1, :pswitch_data_0

    .line 477
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 462
    :pswitch_1
    const v0, 0x7f080007

    goto :goto_0

    .line 464
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->K(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 465
    const v0, 0x7f08009e

    goto :goto_0

    .line 469
    :pswitch_3
    const v0, 0x7f080008

    goto :goto_0

    .line 471
    :pswitch_4
    const v0, 0x7f080009

    goto :goto_0

    .line 473
    :pswitch_5
    const v0, 0x7f080019

    goto :goto_0

    .line 475
    :pswitch_6
    const v0, 0x7f08001b

    goto :goto_0

    .line 460
    nop

    :pswitch_data_0
    .packed-switch -0x7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public ak(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 417
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 418
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 419
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 420
    const-string v2, "server"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 422
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 424
    return-void
.end method

.method public gJ()V
    .locals 0

    .prologue
    .line 410
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->gI()Z

    .line 412
    return-void
.end method

.method public gK()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 430
    return-void
.end method

.method public hf()V
    .locals 2

    .prologue
    .line 357
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->TAG:Ljava/lang/String;

    const-string v1, "startFragmentEmailCertificate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    const/16 v0, 0x6d

    const/4 v1, 0x0

    .line 358
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(ILandroid/os/Bundle;)V

    .line 360
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 297
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->TAG:Ljava/lang/String;

    const-string v1, "onAttach"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 300
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sf:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 302
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 316
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 354
    :goto_0
    return-void

    .line 319
    :sswitch_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 320
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 324
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->hf()V

    goto :goto_0

    .line 327
    :sswitch_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 328
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 337
    :sswitch_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tc:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 338
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->td:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 339
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->te:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 340
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 341
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    .line 342
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    .line 343
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 344
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;

    iget-object v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mContext:Landroid/content/Context;

    invoke-direct {v0, v7, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;-><init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/k;)V

    .line 346
    iget-boolean v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tn:Z

    if-eqz v7, :cond_2

    sget-object v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tj:Ljava/lang/String;

    .line 347
    :goto_1
    iget-boolean v9, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tn:Z

    if-eqz v9, :cond_1

    sget-object v8, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tk:Ljava/lang/String;

    .line 345
    :cond_1
    invoke-virtual/range {v0 .. v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move-object v7, v8

    .line 346
    goto :goto_1

    .line 317
    :sswitch_data_0
    .sparse-switch
        0x7f090049 -> :sswitch_3
        0x7f090054 -> :sswitch_2
        0x7f090057 -> :sswitch_0
        0x7f09005a -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 103
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->gH()V

    .line 104
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->setRetainInstance(Z)V

    .line 93
    const v0, 0x7f030015

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 94
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mInflater:Landroid/view/LayoutInflater;

    .line 95
    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 289
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 291
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 292
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->gI()Z

    .line 293
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const v8, 0x7f090053

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 141
    invoke-super {p0, p1}, Landroid/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 143
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iw()V

    .line 144
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mHandler:Landroid/os/Handler;

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getView()Landroid/view/View;

    move-result-object v1

    .line 162
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mContext:Landroid/content/Context;

    .line 163
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    .line 164
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 165
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    const v2, 0x7f080006

    invoke-virtual {p0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sl:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 167
    const v0, 0x7f090054

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v0, 0x7f090049

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->th:Landroid/widget/Button;

    .line 170
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->th:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    .line 176
    const v0, 0x7f09003f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tc:Landroid/widget/EditText;

    .line 177
    const v0, 0x7f09004e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mContext:Landroid/content/Context;

    .line 178
    const v4, 0x7f080047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 179
    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 180
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->mContext:Landroid/content/Context;

    const v4, 0x7f080046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    const v0, 0x7f09004f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->td:Landroid/widget/EditText;

    .line 182
    const v0, 0x7f090040

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->te:Landroid/widget/EditText;

    .line 183
    const v0, 0x7f090052

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    .line 184
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    .line 186
    const v0, 0x7f090058

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    .line 187
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i$2;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 199
    const v0, 0x7f090057

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 198
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    .line 200
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    const v0, 0x7f09005a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    .line 202
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ti:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 205
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i$3;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 219
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tc:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 220
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->td:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->te:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 222
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->sk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b/a;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 227
    if-eqz v2, :cond_1

    .line 228
    const-string v0, "bundle.ad.failed"

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 229
    if-eqz v0, :cond_3

    .line 231
    const v0, 0x7f09004c

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 232
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->to:Z

    if-nez v0, :cond_2

    .line 250
    const-string v0, "bundle.email"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tc:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->te:Landroid/widget/EditText;

    const-string v3, "bundle.password"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 253
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\\"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 254
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->td:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 257
    :try_start_1
    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 258
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 263
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tb:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 273
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 279
    :goto_2
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->gI()Z

    .line 281
    iput-boolean v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->to:Z

    .line 283
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->gH()V

    .line 285
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 238
    :cond_3
    const v0, 0x7f09004b

    :try_start_2
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 239
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 240
    :catch_1
    move-exception v0

    .line 241
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 259
    :catch_2
    move-exception v0

    .line 260
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 261
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tf:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tg:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->ta:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2
.end method
