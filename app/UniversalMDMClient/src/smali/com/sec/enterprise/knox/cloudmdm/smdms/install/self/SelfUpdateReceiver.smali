.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SelfUpdateReceiver.java"


# static fields
.field private static final rE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    .line 29
    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 29
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->rE:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static final gA()Z
    .locals 3

    .prologue
    .line 125
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->rE:Ljava/lang/String;

    const-string v2, "Knox_Express_Registration_Info.txt"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 127
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->rE:Ljava/lang/String;

    const-string v2, "Samsung_My_KNOX_Registration_Info.txt"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    return v0
.end method

.method public static gz()Ljava/lang/String;
    .locals 8

    .prologue
    .line 98
    new-instance v2, Ljava/io/File;

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->rE:Ljava/lang/String;

    const-string v1, "Knox_Express_Registration_Info.txt"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    new-instance v3, Ljava/io/File;

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->rE:Ljava/lang/String;

    const-string v1, "Samsung_My_KNOX_Registration_Info.txt"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v1, 0x0

    .line 102
    :try_start_0
    new-instance v4, Ljava/util/Scanner;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v0}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V

    .line 103
    const-string v0, "\\z"

    invoke-virtual {v4, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Scanner;->next()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 104
    :try_start_1
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 106
    invoke-virtual {v4}, Ljava/util/Scanner;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 121
    :goto_0
    return-object v0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :goto_1
    const-string v4, "UMC:SelfUpdateReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "file not found "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    :try_start_2
    new-instance v4, Ljava/util/Scanner;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v0}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V

    .line 112
    const-string v0, "\\z"

    invoke-virtual {v4, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Scanner;->next()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 113
    :try_start_3
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 115
    invoke-virtual {v4}, Ljava/util/Scanner;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 116
    :catch_1
    move-exception v1

    .line 117
    :goto_2
    const-string v3, "UMC:SelfUpdateReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file not found "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 116
    :catch_2
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_2

    .line 107
    :catch_3
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1
.end method

.method private y(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    const/4 v1, 0x1

    .line 80
    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 82
    return-void
.end method

.method public static final z(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 86
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 88
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v2, "/data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    const-string v2, "UMC:SelfUpdateReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onReceive "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v2, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    const-string v0, "UMC:SelfUpdateReceiver"

    const-string v2, "starting MDM service "

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->x(Landroid/content/Context;)Z

    move-result v0

    .line 45
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/a;->v(Landroid/content/Context;)V

    .line 46
    const-string v2, "UMC:SelfUpdateReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "selfUpdated : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->z(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->y(Landroid/content/Context;)V

    .line 52
    :cond_0
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/self/SelfUpdateReceiver;->gz()Ljava/lang/String;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_1

    .line 55
    const-string v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 56
    if-eqz v2, :cond_1

    .line 57
    array-length v3, v2

    move v0, v1

    :goto_0
    if-lt v0, v3, :cond_4

    .line 67
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->M(Landroid/content/Context;)V

    .line 69
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->vR:Ljava/lang/String;

    invoke-static {v1, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/m;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 70
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fn()V

    .line 73
    :cond_2
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->A(Landroid/content/Context;)V

    .line 76
    :cond_3
    return-void

    .line 57
    :cond_4
    aget-object v4, v2, v0

    .line 58
    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 59
    if-eqz v4, :cond_5

    array-length v5, v4

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 60
    aget-object v5, v4, v1

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-static {p1, v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;->m(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
