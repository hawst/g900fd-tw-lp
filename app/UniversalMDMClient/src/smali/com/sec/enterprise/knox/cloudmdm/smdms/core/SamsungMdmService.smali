.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/SamsungMdmService;
.super Landroid/app/Service;
.source "SamsungMdmService.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private qu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "UMC:SamsungMdmService"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/SamsungMdmService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 59
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/SamsungMdmService;->qu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    .line 60
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 65
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.intent.action.START_SAMSUNG_MDM_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/SamsungMdmService;->TAG:Ljava/lang/String;

    const-string v1, "calling startSelfUpdateCheck"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/SamsungMdmService;->qu:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fo()V

    .line 71
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
