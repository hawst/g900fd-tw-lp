.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;
.super Ljava/lang/Object;
.source "UserInterface.java"


# static fields
.field private static zo:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

.field public static final zp:Ljava/util/regex/Pattern;

.field public static zq:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public static zr:Z


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const-string v0, "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 111
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zp:Ljava/util/regex/Pattern;

    .line 114
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zq:Ljava/util/LinkedHashMap;

    .line 115
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zr:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->mContext:Landroid/content/Context;

    .line 119
    sput-object p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zo:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    .line 120
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zo:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;)V

    .line 121
    return-void
.end method

.method private a(ILandroid/os/Bundle;ZI)V
    .locals 4

    .prologue
    .line 536
    if-nez p3, :cond_2

    .line 537
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zr:Z

    if-eqz v0, :cond_0

    .line 538
    const-string v0, "UMC:UserInterface"

    const-string v1, "Activity Started but not yet running"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zq:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    :goto_0
    return-void

    .line 543
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms"

    .line 544
    const-string v2, "com.sec.enterprise.knox.cloudmdm.smdms.ui.CloudMdmEnrollmentActivity"

    .line 543
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 546
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 547
    const-string v0, "cloudmdm.ui.intent.extra"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 548
    if-eqz p2, :cond_1

    .line 549
    const-string v0, "cloudmdm.ui.intent.extra.bundle"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 551
    :cond_1
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 552
    invoke-virtual {v1, p4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 553
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 554
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zr:Z

    .line 555
    const-string v0, "UMC:UserInterface"

    const-string v1, "Activity Started"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 556
    :catch_0
    move-exception v0

    .line 557
    const-string v1, "UMC:UserInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Manual Enrollment Activity Not found. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 560
    :cond_2
    const-string v0, "UMC:UserInterface"

    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting new fragment: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 562
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->aZ(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 561
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 560
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    invoke-static {p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->b(ILandroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public a(IIIZ)V
    .locals 4

    .prologue
    const/16 v3, 0x6a

    const/4 v2, 0x0

    .line 520
    const-string v0, "UMC:UserInterface"

    const-string v1, "showErrorDialog()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 522
    const-string v1, "error.title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 523
    const-string v1, "error.message"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 524
    const-string v1, "error.buttonText"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 525
    const-string v1, "error.shouldClose"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 526
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    if-nez v1, :cond_0

    .line 527
    const/high16 v1, 0x800000

    .line 528
    invoke-direct {p0, v3, v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 532
    :goto_0
    return-void

    .line 530
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method

.method public a(III[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V
    .locals 4

    .prologue
    .line 401
    const-string v0, "UMC:UserInterface"

    const-string v1, "showDynamicUI()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    if-nez p4, :cond_0

    .line 403
    const-string v0, "UMC:UserInterface"

    const-string v1, "UAF is null!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    :goto_0
    return-void

    .line 406
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 407
    const-string v1, "fragmentTitleMessage"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 408
    const-string v1, "btnEnrollText"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 409
    const-string v1, "fragmentFooterText"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 410
    const-string v1, "ui.uaf"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 411
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 412
    const/16 v2, 0x64

    .line 413
    const/4 v3, 0x0

    .line 412
    invoke-direct {p0, v2, v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V
    .locals 4

    .prologue
    .line 431
    const-string v0, "UMC:UserInterface"

    const-string v1, "showLoginScreen()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    if-nez p3, :cond_0

    .line 433
    const-string v0, "UMC:UserInterface"

    const-string v1, "UAF is null!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :goto_0
    return-void

    .line 436
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 437
    const-string v1, "email"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v1, "mdmUrl"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v1, "ui.uaf"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 440
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 441
    const/16 v2, 0x67

    .line 442
    const/4 v3, 0x0

    .line 441
    invoke-direct {p0, v2, v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V
    .locals 4

    .prologue
    .line 417
    const-string v0, "UMC:UserInterface"

    const-string v1, "showLoginScreen()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    if-nez p2, :cond_0

    .line 419
    const-string v0, "UMC:UserInterface"

    const-string v1, "UAF is null!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :goto_0
    return-void

    .line 422
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 423
    const-string v1, "email"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string v1, "ui.uaf"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 425
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 426
    const/16 v2, 0x67

    .line 427
    const/4 v3, 0x0

    .line 426
    invoke-direct {p0, v2, v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 361
    invoke-static {p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 364
    const-string v0, "UMC:UserInterface"

    const-string v1, "showSelectMDMServer()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 377
    const-string v0, "UMC:UserInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "showEula() : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 379
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 380
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    move v1, v2

    .line 381
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 387
    const-string v0, "ui.eula.titles"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 388
    const-string v0, "ui.eula.datas"

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 389
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    .line 390
    const/16 v1, 0x66

    invoke-direct {p0, v1, v3, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 392
    return-void

    .line 382
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 383
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->getHref()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    .line 384
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/CommonEula;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 381
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public b(IIII)V
    .locals 4

    .prologue
    const/16 v3, 0x6f

    const/4 v2, 0x0

    .line 593
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 594
    const-string v1, "error.title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 595
    const-string v1, "error.message"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 596
    const-string v1, "error.positiveButtonText"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 597
    const-string v1, "error.negativeButtonText"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 598
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    if-nez v1, :cond_0

    .line 599
    const/high16 v1, 0x800000

    .line 600
    invoke-direct {p0, v3, v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 604
    :goto_0
    return-void

    .line 602
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method

.method public bm(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 450
    if-nez p1, :cond_0

    .line 451
    const-string v5, ""

    .line 453
    :goto_0
    const/4 v0, 0x1

    new-array v6, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    .line 454
    const v2, 0x7f080075

    .line 455
    const v1, 0x7f080077

    .line 456
    const/4 v7, 0x0

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;

    .line 457
    const-string v3, "username"

    .line 458
    const/4 v4, 0x5

    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;-><init>(IILjava/lang/String;ILjava/lang/String;)V

    .line 456
    aput-object v0, v6, v7

    .line 459
    const v0, 0x7f0800b2

    .line 460
    const v1, 0x7f080072

    .line 461
    const v2, 0x7f080055

    .line 462
    invoke-virtual {p0, v1, v2, v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(III[Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;)V

    .line 463
    return-void

    :cond_0
    move-object v5, p1

    goto :goto_0
.end method

.method public bn(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 466
    const-string v0, "UMC:UserInterface"

    const-string v1, "showQuickstartEmail()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 468
    const-string v1, "email"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const/16 v1, 0x6b

    .line 470
    sget-boolean v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    const/4 v3, 0x0

    .line 469
    invoke-direct {p0, v1, v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 471
    return-void
.end method

.method public bo(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x68

    const/4 v2, 0x0

    .line 568
    const-string v0, "UMC:UserInterface"

    const-string v1, "showWaitBoxDialog()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 570
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    if-nez v1, :cond_0

    .line 572
    const/high16 v1, 0x800000

    .line 573
    invoke-direct {p0, v3, v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 577
    :goto_0
    return-void

    .line 575
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method

.method public f(II)V
    .locals 4

    .prologue
    .line 477
    const-string v0, "UMC:UserInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showQuickstartProgress : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 483
    const-string v1, "actionMessage"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 484
    const-string v1, "progress"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 485
    const/16 v1, 0x6e

    .line 486
    sget-boolean v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    const/4 v3, 0x0

    .line 485
    invoke-direct {p0, v1, v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 487
    return-void
.end method

.method public g(II)V
    .locals 3

    .prologue
    .line 491
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 492
    const-string v1, "actionMessage"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 493
    const-string v1, "progress"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 495
    const/16 v1, 0x6e

    .line 496
    const/16 v2, 0x64

    .line 494
    invoke-static {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->a(IILandroid/os/Bundle;)V

    .line 497
    return-void
.end method

.method public gw()V
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    .line 500
    const-string v0, "UMC:UserInterface"

    const-string v1, "bringUiToForeground"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 502
    sget v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 503
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 504
    const-string v2, "showWindow"

    sget v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->yX:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 505
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 506
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 516
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 517
    return-void

    .line 508
    :cond_1
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 509
    const-string v3, "com.sec.enterprise.knox.cloudmdm.smdms.ui.CloudMdmEnrollmentActivity"

    .line 508
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 511
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 512
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zo:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    if-eqz v1, :cond_0

    .line 513
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->zo:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/m;->ft()V

    goto :goto_0
.end method

.method public iv()V
    .locals 0

    .prologue
    .line 589
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iv()V

    .line 590
    return-void
.end method

.method public jz()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x69

    const/4 v2, 0x0

    .line 580
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->xI:Z

    if-nez v0, :cond_0

    .line 581
    const-string v0, "UMC:UserInterface"

    const-string v1, "CloudMdmEnrollmentActivity is not running"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    invoke-direct {p0, v3, v4, v2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    .line 586
    :goto_0
    return-void

    .line 584
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v3, v4, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(ILandroid/os/Bundle;ZI)V

    goto :goto_0
.end method
