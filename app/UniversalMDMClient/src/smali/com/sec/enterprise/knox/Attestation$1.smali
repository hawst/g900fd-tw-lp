.class Lcom/sec/enterprise/knox/Attestation$1;
.super Ljava/lang/Object;
.source "Attestation.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/Attestation;
.end annotation


# instance fields
.field final synthetic oQ:Lcom/sec/enterprise/knox/Attestation;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/Attestation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/Attestation$1;->oQ:Lcom/sec/enterprise/knox/Attestation;

    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 363
    # getter for: Lcom/sec/enterprise/knox/Attestation;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/Attestation;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "On onServiceConnected"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation$1;->oQ:Lcom/sec/enterprise/knox/Attestation;

    invoke-static {p2}, Lcom/sec/enterprise/knox/a;->c(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IAttestation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/Attestation;->access$1(Lcom/sec/enterprise/knox/Attestation;Lcom/sec/enterprise/knox/IAttestation;)V

    .line 365
    const-string v0, "IAttestation"

    const-string v1, "Binding done - service connected"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation$1;->oQ:Lcom/sec/enterprise/knox/Attestation;

    # invokes: Lcom/sec/enterprise/knox/Attestation;->doAttestation()V
    invoke-static {v0}, Lcom/sec/enterprise/knox/Attestation;->access$2(Lcom/sec/enterprise/knox/Attestation;)V

    .line 367
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 356
    # getter for: Lcom/sec/enterprise/knox/Attestation;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/Attestation;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "On onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation$1;->oQ:Lcom/sec/enterprise/knox/Attestation;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/Attestation;->access$1(Lcom/sec/enterprise/knox/Attestation;Lcom/sec/enterprise/knox/IAttestation;)V

    .line 358
    const-string v0, "IAttestation"

    const-string v1, "Binding - Service disconnected"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void
.end method
