.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;
.super Ljava/lang/Object;
.source "LookupServiceResponse.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private port:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public getPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->port:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setPort(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->port:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->protocol:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceResponse$EndPoint;->url:Ljava/lang/String;

    .line 36
    return-void
.end method
