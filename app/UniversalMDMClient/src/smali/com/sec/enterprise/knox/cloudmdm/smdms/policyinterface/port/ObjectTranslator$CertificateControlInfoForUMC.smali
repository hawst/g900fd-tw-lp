.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;
.super Ljava/lang/Object;
.source "ObjectTranslator.java"


# instance fields
.field private adminPackageName:Ljava/lang/String;

.field private entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 961
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->adminPackageName:Ljava/lang/String;

    .line 962
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->entries:Ljava/util/List;

    .line 959
    return-void
.end method


# virtual methods
.method public getAdminPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->adminPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 971
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->entries:Ljava/util/List;

    return-object v0
.end method

.method public setAdminPackageName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 968
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->adminPackageName:Ljava/lang/String;

    .line 969
    return-void
.end method

.method public setEntries(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 974
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$CertificateControlInfoForUMC;->entries:Ljava/util/List;

    .line 975
    return-void
.end method
