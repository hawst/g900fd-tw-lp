.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;
.source "MDMManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations;


# static fields
.field private static ry:Ljava/lang/String;

.field private static vu:Ljava/lang/String;


# instance fields
.field private csr:Ljava/lang/String;

.field private pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

.field private rg:Ljava/security/KeyPair;

.field private wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

.field private wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

.field private wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

.field private wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

.field private wp:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "UMC:MDMManager"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;-><init>()V

    return-void
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonNode;
    .locals 4

    .prologue
    .line 484
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 485
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss.s\'TZD\'"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 486
    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 487
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 489
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;->xq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    if-ne p1, v1, :cond_0

    .line 490
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent;-><init>()V

    .line 491
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent$Error;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent$Error;-><init>()V

    .line 492
    invoke-virtual {v2, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent$Error;->setCode(Ljava/lang/String;)V

    .line 493
    invoke-virtual {v2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent$Error;->setMessage(Ljava/lang/String;)V

    .line 494
    invoke-virtual {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent$Error;->setTimestamp(Ljava/lang/String;)V

    .line 495
    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent;->setError(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/ErrorContent$Error;)V

    .line 498
    :try_start_0
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 499
    new-instance v1, Lcom/fasterxml/jackson/databind/ObjectMapper;

    invoke-direct {v1}, Lcom/fasterxml/jackson/databind/ObjectMapper;-><init>()V

    .line 500
    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/ObjectMapper;->readTree(Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonNode;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 524
    :goto_0
    return-object v0

    .line 503
    :catch_0
    move-exception v0

    .line 504
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getReport failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 507
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent;-><init>()V

    .line 508
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent$Event;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent$Event;-><init>()V

    .line 509
    invoke-virtual {v2, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent$Event;->setCode(Ljava/lang/String;)V

    .line 510
    invoke-virtual {v2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent$Event;->setMessage(Ljava/lang/String;)V

    .line 511
    invoke-virtual {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent$Event;->setTimestamp(Ljava/lang/String;)V

    .line 512
    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent;->setEvent(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/EventContent$Event;)V

    .line 515
    :try_start_1
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 516
    new-instance v1, Lcom/fasterxml/jackson/databind/ObjectMapper;

    invoke-direct {v1}, Lcom/fasterxml/jackson/databind/ObjectMapper;-><init>()V

    .line 517
    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/ObjectMapper;->readTree(Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonNode;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 520
    :catch_1
    move-exception v0

    .line 521
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getReport failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    return-object v0
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 471
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    .line 472
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setName(Ljava/lang/String;)V

    .line 473
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;-><init>()V

    .line 474
    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;->setId(Ljava/lang/String;)V

    .line 475
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setDevice(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;)V

    .line 476
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setType(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;)V

    .line 478
    invoke-direct {p0, p1, p4, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v0

    .line 479
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setContent(Lcom/fasterxml/jackson/databind/JsonNode;)V

    .line 480
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->aV(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->p(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private aV(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 196
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "processQueryDeviceResponse"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 207
    :goto_0
    return-void

    .line 202
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "QueryDevice response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processQueryDeviceResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aW(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 340
    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Converting policyinfo to json"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    .line 342
    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    .line 344
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Converted"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Converted"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;->getElements()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;->getElements()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Policy content: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;->getElements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 348
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;->getElements()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyInfo;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "PolicyInfo response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 354
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processPolicyInfoResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aX(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 445
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "processReportResponse"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    .line 457
    :goto_0
    return-void

    .line 451
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Report response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 453
    :catch_0
    move-exception v0

    .line 454
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processReportResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aY(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->getPem()Ljava/lang/String;

    move-result-object v0

    .line 608
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->getCsr()Ljava/lang/String;

    move-result-object v1

    .line 609
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "server response pem:csr:: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 615
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 616
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error csr mismatch: orig:new:: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 620
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wp:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    if-eqz v1, :cond_0

    .line 622
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wp:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 623
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->q(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->rg:Ljava/security/KeyPair;

    .line 622
    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->a(Ljava/lang/String;Ljava/security/KeyPair;Ljava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->rg:Ljava/security/KeyPair;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setKeyPair(Ljava/security/KeyPair;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 626
    :catch_0
    move-exception v0

    .line 627
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 122
    if-nez p1, :cond_0

    .line 123
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Please do Profile lookup to get profile before enrolling device"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :goto_0
    return-void

    .line 127
    :cond_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 128
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    const-string v1, "Samsung Device"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setName(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    const-string v1, "Samsung Device Description"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setDescription(Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;-><init>()V

    .line 134
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->getOption()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 135
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->getOption()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CSR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 136
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hP()Z

    move-result v1

    if-nez v1, :cond_3

    .line 137
    :cond_2
    invoke-direct {p0, p2, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->n(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    .line 138
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Csr "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 140
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->setCsr(Ljava/lang/String;)V

    .line 143
    :cond_3
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setCertificate(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;)V

    .line 145
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;-><init>()V

    .line 146
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getAuthentication()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 147
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getAuthentication()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Authentication;->getScheme()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;->setScheme(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$AUTH_SCHEME;)V

    .line 149
    :cond_4
    invoke-virtual {v0, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;->setCredential(Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setAuthentication(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Authentication;)V

    .line 152
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;-><init>()V

    .line 153
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;-><init>()V

    .line 154
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getHref()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;->setHref(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->setEmailAddress(Ljava/lang/String;)V

    .line 156
    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->setProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment$Profile;)V

    .line 157
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->setGroup(Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setEnrollment(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;)V

    .line 160
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->getDefaultDeviceInfo(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->setPushId(Ljava/lang/String;)V

    .line 162
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setProfile(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;)V

    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    const-string v1, "ENROLLING"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setStatus(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 338
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->aW(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->aX(Ljava/lang/String;)V

    return-void
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 460
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    .line 461
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setName(Ljava/lang/String;)V

    .line 462
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;-><init>()V

    .line 463
    invoke-virtual {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;->setId(Ljava/lang/String;)V

    .line 464
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setDevice(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report$ReportDevice;)V

    .line 465
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;->xs:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setType(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;)V

    .line 466
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->ba(Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;->setContent(Lcom/fasterxml/jackson/databind/JsonNode;)V

    .line 467
    return-void
.end method

.method static synthetic hS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    return-object v0
.end method

.method public static hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/u;->ic()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    return-object v0
.end method

.method private n(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 593
    :try_start_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wp:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 594
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wp:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->rg:Ljava/security/KeyPair;

    .line 595
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Send inputs for CSR:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->rg:Ljava/security/KeyPair;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wp:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->rg:Ljava/security/KeyPair;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    move-object v4, p2

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->a(Ljava/security/KeyPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 602
    :goto_0
    return-object v0

    .line 599
    :catch_0
    move-exception v0

    .line 600
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 602
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 167
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "processEnrollDeviceResponse"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    if-eqz p2, :cond_1

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 172
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {p2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 173
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->printDevice()V

    .line 174
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->csr:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 175
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->aY(Ljava/lang/String;)V

    .line 180
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "1 processEnrollDeviceResponse:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 181
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Finished processEnrollDeviceResponse:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 191
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 190
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v0, 0x1

    return v0

    .line 183
    :cond_1
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "EnrollDevice response null or empty"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 185
    :catch_0
    move-exception v0

    .line 186
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processEnrollDeviceResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 241
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 242
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v0, p0, v1, p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Ljava/lang/String;)V

    .line 259
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v1, :cond_0

    .line 260
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v0, "deviceContainer.json"

    .line 262
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    .line 261
    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    .line 263
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "mock device:"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 264
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wy:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Ljava/lang/Enum;)V

    .line 276
    :goto_0
    return-void

    .line 268
    :cond_0
    :try_start_0
    invoke-static {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 269
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->bc(Ljava/lang/String;)V

    .line 270
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Json request:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/devices/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updatedevice failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 408
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 411
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;->xq:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;

    if-ne p4, v0, :cond_0

    .line 412
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-object v6, v0

    .line 417
    :goto_0
    new-instance v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$7;

    invoke-direct {v7, p0, v6, p0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$7;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V

    .line 429
    sget-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v0, :cond_1

    .line 430
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :goto_1
    return-void

    .line 414
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-object v6, v0

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v1, p4

    move-object v2, p5

    move-object v3, p3

    move-object v4, p6

    move-object v5, p7

    .line 433
    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$REPORT_TYPE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 435
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":Json request:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v2, "/reports"

    invoke-virtual {v1, v2, v0, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 438
    :catch_0
    move-exception v0

    .line 439
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "postreport failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 308
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 310
    new-instance v0, Lcom/a/a/a/j;

    invoke-direct {v0}, Lcom/a/a/a/j;-><init>()V

    .line 311
    const-string v1, "device"

    invoke-virtual {v0, v1, p3}, Lcom/a/a/a/j;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v1, "emailAddress"

    invoke-virtual {v0, v1, p4}, Lcom/a/a/a/j;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "request_params:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/a/a/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$5;

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v1, p0, v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$5;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 326
    sget-boolean v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v2, :cond_0

    .line 327
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v0, "policiesForDevice.json"

    .line 329
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    .line 328
    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    .line 330
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;)V

    .line 331
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->ww:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Ljava/lang/Enum;)V

    .line 336
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v3, "/policies"

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 371
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$6;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v0, p0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$6;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 384
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v1, :cond_0

    .line 385
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :goto_0
    return-void

    .line 392
    :cond_0
    :try_start_0
    invoke-direct {p0, p4, p3, p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wo:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Report;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 394
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Json request:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    invoke-direct {v2, v3, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 396
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Client"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v3, "/reports"

    invoke-virtual {v2, v3, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 398
    :catch_0
    move-exception v0

    .line 399
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "postreport failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 83
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v0, p0, v1, p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;)V

    .line 100
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v1, :cond_0

    .line 101
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v0, "deviceresponse.json"

    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {p2, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 103
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v1

    iput-object v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 104
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wv:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Ljava/lang/Enum;)V

    .line 117
    :goto_0
    return-void

    .line 107
    :cond_0
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 109
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Json request:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, p2, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 111
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Client"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    const-string v3, "/devices"

    invoke-virtual {v2, v3, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "enrolldevice failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    .line 364
    return-void
.end method

.method public ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wl:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    return-object v0
.end method

.method public ib()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wm:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/PolicyContainer;

    return-object v0
.end method

.method public j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 221
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 222
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$2;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v0, p0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 233
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": sending delete request"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/devices/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    .line 235
    return-void
.end method

.method public k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 282
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    .line 283
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$4;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-direct {v0, p0, v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t$4;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    .line 293
    sget-boolean v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->xu:Z

    if-eqz v1, :cond_0

    .line 294
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    const-string v1, "Using MOCK DATA"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v0, "deviceContainer.json"

    .line 296
    const-class v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    .line 295
    invoke-static {p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    .line 297
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ry:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "mock device:"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 298
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->wn:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceContainer;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297
    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;->wx:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/MDMOperations$Property;

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Ljava/lang/Enum;)V

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->pY:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/devices/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;)V

    goto :goto_0
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 214
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->vu:Ljava/lang/String;

    .line 215
    return-void
.end method
