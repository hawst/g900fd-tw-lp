.class public final Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;
.super Ljava/lang/Object;
.source "ProfileStorage.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x160e49ebde0be847L


# instance fields
.field private final appSecret:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final deviceId:Ljava/lang/String;

.field private final domainGroup:Ljava/lang/String;

.field private final domainName:Ljava/lang/String;

.field private final email:Ljava/lang/String;

.field private final icon:[B

.field private final id:Ljava/lang/String;

.field private final managementService:Ljava/lang/String;

.field private final mdmAgentPackageName:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final notificationMessenger:Ljava/lang/String;

.field private final notificationMethod:Ljava/lang/String;

.field private final pollingInterval:I


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    .line 75
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->name:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->description:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainName:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainGroup:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainGroup:Ljava/lang/String;

    .line 79
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->icon:[B

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->icon:[B

    .line 80
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->managementService:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->managementService:Ljava/lang/String;

    .line 81
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMessenger:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMessenger:Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMethod:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMethod:Ljava/lang/String;

    .line 83
    iget v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->pollingInterval:I

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->pollingInterval:I

    .line 84
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    .line 85
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    .line 86
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->mdmAgentPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->mdmAgentPackageName:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->appSecret:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->appSecret:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->name:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->description:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getGroupName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainGroup:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment;->getDomain()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Enrollment$Domain;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainName:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management;->getService()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Service;->getHref()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->managementService:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getMessenger()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMessenger:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getMethod()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMethod:Ljava/lang/String;

    .line 100
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->icon:[B

    .line 101
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    .line 102
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getNotification()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->getPollInterval()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->pollingInterval:I

    .line 104
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->mdmAgentPackageName:Ljava/lang/String;

    .line 105
    iput-object p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->appSecret:Ljava/lang/String;

    .line 106
    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->appSecret:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->managementService:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 110
    if-ne p0, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 113
    goto :goto_0

    .line 114
    :cond_2
    instance-of v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    if-nez v2, :cond_3

    move v0, v1

    .line 115
    goto :goto_0

    .line 116
    :cond_3
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    .line 117
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 118
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 119
    goto :goto_0

    .line 120
    :cond_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 121
    goto :goto_0

    .line 122
    :cond_5
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 123
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 124
    goto :goto_0

    .line 125
    :cond_6
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 126
    goto :goto_0

    .line 127
    :cond_7
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 128
    iget-object v2, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 129
    goto :goto_0

    .line 130
    :cond_8
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public fS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainGroup:Ljava/lang/String;

    return-object v0
.end method

.method public fT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->managementService:Ljava/lang/String;

    return-object v0
.end method

.method public fU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->mdmAgentPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public fV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->appSecret:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()[B
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->icon:[B

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPollingInterval()J
    .locals 2

    .prologue
    .line 230
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->pollingInterval:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 235
    .line 237
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 238
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 239
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 240
    return v0

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 239
    :cond_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    const-string v1, "Profile [id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 247
    const-string v2, ", description="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", domainName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 248
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", domainGroup="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->domainGroup:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 249
    const-string v2, ", icon="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->icon:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", managementService="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 250
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->managementService:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", notificationMessenger="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 251
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMessenger:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", notificationMethod="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 252
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->notificationMethod:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pollingInterval="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 253
    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->pollingInterval:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", email="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", deviceId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 254
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mdmAgentPackageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->mdmAgentPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 255
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
