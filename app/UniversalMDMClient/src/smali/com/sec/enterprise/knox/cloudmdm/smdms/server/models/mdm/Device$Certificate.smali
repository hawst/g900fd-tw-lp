.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;
.super Ljava/lang/Object;
.source "Device.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private csr:Ljava/lang/String;

.field private pem:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCsr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->csr:Ljava/lang/String;

    return-object v0
.end method

.method public getPem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->pem:Ljava/lang/String;

    return-object v0
.end method

.method public setCsr(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->csr:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setPem(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->pem:Ljava/lang/String;

    .line 76
    return-void
.end method
