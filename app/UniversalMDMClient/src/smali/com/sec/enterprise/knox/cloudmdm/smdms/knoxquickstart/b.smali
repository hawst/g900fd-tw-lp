.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;
.super Landroid/app/Fragment;
.source "EmailCertificatePickerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static rP:Ljava/io/File;

.field private static rR:Landroid/widget/Toast;


# instance fields
.field private certType:I

.field private final mHandler:Landroid/os/Handler;

.field rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

.field rN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;

.field private rO:I

.field private rQ:Ljava/lang/String;

.field private rS:Landroid/widget/ListView;

.field private rT:Landroid/app/ProgressDialog;

.field private rU:Landroid/widget/ImageView;

.field private rV:Landroid/view/View;

.field private rW:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->TAG:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rR:Landroid/widget/Toast;

    .line 78
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rO:I

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->certType:I

    .line 267
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->mHandler:Landroid/os/Handler;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;I)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->showDialog(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rT:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Z
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->gC()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->aj(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private aj(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 322
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rP:Ljava/io/File;

    if-nez v1, :cond_0

    .line 343
    :goto_0
    return v0

    .line 324
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rP:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/a/e;->k(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rP:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tj:Ljava/lang/String;

    .line 328
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/i;->tk:Ljava/lang/String;

    .line 343
    const/4 v0, 0x1

    goto :goto_0

    .line 330
    :cond_1
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b$3;

    invoke-direct {v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rT:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;I)V
    .locals 0

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rO:I

    return-void
.end method

.method static synthetic b(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 63
    sput-object p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rP:Ljava/io/File;

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rQ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rO:I

    return v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->gE()V

    return-void
.end method

.method private gC()Z
    .locals 14

    .prologue
    const v13, 0x7f08003c

    const/16 v12, 0xbb8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 148
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v3, "mounted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f080039

    invoke-static {v0, v2, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 151
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->TAG:Ljava/lang/String;

    const-string v2, "Failed createFileList"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :goto_0
    return v1

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 156
    const-string v3, "storage"

    .line 155
    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 157
    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    .line 158
    array-length v4, v3

    .line 159
    const-string v0, ""

    .line 162
    const/4 v0, 0x3

    new-array v5, v0, [Ljava/io/File;

    .line 163
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    aput-object v0, v5, v1

    .line 165
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/Download"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v0, v5, v2

    move v0, v1

    .line 166
    :goto_1
    if-lt v0, v4, :cond_1

    .line 177
    :goto_2
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 179
    array-length v6, v5

    move v3, v1

    :goto_3
    if-lt v3, v6, :cond_3

    .line 192
    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 194
    if-eqz v0, :cond_8

    array-length v3, v0

    if-lez v3, :cond_8

    .line 195
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "files: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rS:Landroid/widget/ListView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 198
    const v5, 0x1090003

    invoke-direct {v3, v4, v5, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 197
    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move v1, v2

    .line 199
    goto :goto_0

    .line 167
    :cond_1
    aget-object v6, v3, v0

    .line 168
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v7

    .line 169
    if-eqz v7, :cond_2

    .line 170
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 171
    const-string v8, "sd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "/storage/extSdCard"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 172
    const/4 v0, 0x2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v3, v5, v0

    goto :goto_2

    .line 166
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    :cond_3
    aget-object v7, v5, v3

    .line 180
    if-eqz v7, :cond_4

    .line 181
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 182
    if-eqz v8, :cond_4

    move v0, v1

    .line 183
    :goto_4
    array-length v9, v8

    if-lt v0, v9, :cond_5

    .line 179
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 184
    :cond_5
    aget-object v9, v8, v0

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 185
    const-string v10, ".p12"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    const-string v10, ".pfx"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 186
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 202
    :cond_8
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rR:Landroid/widget/Toast;

    if-nez v0, :cond_9

    .line 203
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v13, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rR:Landroid/widget/Toast;

    .line 208
    :goto_5
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rR:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 206
    :cond_9
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rR:Landroid/widget/Toast;

    invoke-virtual {v0, v13}, Landroid/widget/Toast;->setText(I)V

    goto :goto_5
.end method

.method private gD()V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->stopWatching()V

    .line 265
    :cond_0
    return-void
.end method

.method private gE()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 306
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rO:I

    if-ne v0, v2, :cond_1

    .line 308
    const/4 v0, 0x2

    :try_start_0
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 309
    :catch_0
    move-exception v0

    .line 310
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 312
    :cond_1
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rO:I

    if-ne v0, v1, :cond_0

    .line 314
    const/4 v0, 0x1

    :try_start_1
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->showDialog(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 315
    :catch_1
    move-exception v0

    .line 316
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private showDialog(I)V
    .locals 4

    .prologue
    .line 142
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;->aN(I)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/c;

    move-result-object v0

    .line 143
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dialog "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 145
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 131
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->TAG:Ljava/lang/String;

    const-string v1, "onAttach"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 134
    :try_start_0
    check-cast p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;

    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rM:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 136
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity must implement Listener interface. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 605
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090077

    if-ne v0, v1, :cond_0

    .line 606
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->gC()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rU:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rV:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rW:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rV:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 612
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rW:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/CloudMdmEnrollmentActivity;->iw()V

    .line 92
    const v0, 0x7f03001a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 93
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 258
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 259
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->gD()V

    .line 260
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x7f09007b

    const v4, 0x7f090077

    const/4 v3, 0x1

    .line 99
    invoke-super {p0, p1}, Landroid/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getView()Landroid/view/View;

    move-result-object v1

    .line 101
    const v0, 0x7f09006a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rV:Landroid/view/View;

    .line 102
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rW:Landroid/view/View;

    .line 103
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rV:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 104
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->setRetainInstance(Z)V

    .line 105
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->setHasOptionsMenu(Z)V

    .line 107
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rU:Landroid/widget/ImageView;

    .line 108
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rS:Landroid/widget/ListView;

    .line 112
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rS:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b$2;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 124
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;

    .line 125
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->rN:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->startWatching()V

    .line 126
    return-void
.end method
