.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;
.source "ConsoleLogger.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/c;-><init>(Ljava/lang/String;)V

    .line 43
    return-void
.end method


# virtual methods
.method public bk(I)I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 57
    const/4 p1, 0x2

    .line 59
    :cond_0
    return p1
.end method

.method public isLoggable(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 49
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;->level:I

    if-gt v1, p1, :cond_0

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;->level:I

    if-eq v1, v0, :cond_0

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public log(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/a;->bk(I)I

    move-result v0

    invoke-static {v0, p2, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    return-void
.end method
