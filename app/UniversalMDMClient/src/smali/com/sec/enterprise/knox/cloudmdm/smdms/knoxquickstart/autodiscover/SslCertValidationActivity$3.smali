.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;
.super Ljava/lang/Object;
.source "SslCertValidationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hD()V
.end annotation


# instance fields
.field final synthetic vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;Landroid/app/AlertDialog;)V

    .line 195
    const-string v0, "SSLCertValidationActivity"

    const-string v1, "CertificateErrorDialog: NegativeButton pressed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " finishing Activity  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " finishing Activity  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vh:Z

    .line 202
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hG()Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 203
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$3;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->finish()V

    .line 207
    :cond_0
    return-void
.end method
