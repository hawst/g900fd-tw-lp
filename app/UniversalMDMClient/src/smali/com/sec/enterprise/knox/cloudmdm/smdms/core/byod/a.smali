.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;
.super Ljava/lang/Object;
.source "ByodSetupStarter.java"


# instance fields
.field private mContainerId:I

.field private mContext:Landroid/content/Context;

.field private mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private qA:Ljava/lang/String;

.field private qB:Ljava/lang/String;

.field private qC:Ljava/lang/String;

.field private qD:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;

.field private qE:I

.field private qF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

.field private qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

.field private qH:Ljava/lang/String;

.field private qI:Ljava/lang/String;

.field private qJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

.field private qf:Ljava/lang/String;

.field private qg:Ljava/lang/String;

.field private qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

.field private qy:Ljava/lang/String;

.field private qz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const-string v0, "byodVendor.apk"

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qC:Ljava/lang/String;

    .line 111
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 389
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    .line 205
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    .line 206
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    .line 207
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    .line 208
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qy:Ljava/lang/String;

    .line 209
    iput-object p9, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qD:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;

    .line 210
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    .line 211
    iput-object p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    .line 212
    iput-object p7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qH:Ljava/lang/String;

    .line 213
    iput-object p8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qI:Ljava/lang/String;

    .line 214
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->ia()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getLicenses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    return-void

    .line 214
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;

    .line 215
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v2

    const-string v3, "KLM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 216
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qg:Ljava/lang/String;

    .line 217
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qg:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 218
    iput-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qg:Ljava/lang/String;

    .line 220
    :cond_2
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qA:Ljava/lang/String;

    goto :goto_0

    .line 221
    :cond_3
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getTitle()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ELM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getKey()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qf:Ljava/lang/String;

    .line 223
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qf:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 224
    iput-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qf:Ljava/lang/String;

    .line 226
    :cond_4
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$DeviceLicense;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qA:Ljava/lang/String;

    goto :goto_0
.end method

.method private S(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    const-string v1, "UMC:ByodSetupStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    .line 281
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_0

    .line 283
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qB:Ljava/lang/String;

    .line 284
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    .line 286
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->aF(I)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->S(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;Ljava/util/ArrayList;II)V
    .locals 0

    .prologue
    .line 445
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Ljava/util/ArrayList;II)V

    return-void
.end method

.method private a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V
    .locals 7

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    const v2, 0x7f080079

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->bo(Ljava/lang/String;)V

    .line 294
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/b;

    invoke-direct {v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/b;-><init>()V

    .line 297
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 298
    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/b;->px:Z

    .line 299
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    .line 300
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qB:Ljava/lang/String;

    .line 313
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->activateUMCAdmin(Landroid/content/Context;I)V

    .line 314
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qf:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qg:Ljava/lang/String;

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/LicenseManager;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    .line 316
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qA:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v0

    .line 304
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->fR()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->formProxyComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 309
    :goto_1
    const-string v4, "1.0"

    .line 310
    const/4 v1, 0x0

    iput-boolean v1, v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/b;->px:Z

    move-object v3, v0

    goto :goto_0

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qA:Ljava/lang/String;

    goto :goto_1
.end method

.method private a(Ljava/util/ArrayList;II)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 447
    .line 450
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 451
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    .line 452
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aU(I)Ljava/lang/String;

    move-result-object v6

    .line 455
    :goto_0
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getEnrollment()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Enrollment;->getEmailAddress()Ljava/lang/String;

    move-result-object v2

    .line 456
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getProfileIcon()[B

    move-result-object v4

    .line 455
    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;[BLjava/lang/String;Ljava/lang/String;)V

    .line 458
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    const-string v2, "ENROLLED"

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setStatus(Ljava/lang/String;)V

    .line 461
    :try_start_0
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qH:Ljava/lang/String;

    .line 462
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qI:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v9

    move-object v2, v0

    move-object v3, p1

    move v4, p2

    move v5, p3

    .line 461
    invoke-direct/range {v1 .. v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;Ljava/util/ArrayList;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 467
    :goto_1
    if-eqz v0, :cond_0

    .line 468
    const-string v1, "UMC:ByodSetupStarter"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.enterprise.knox.cloudmdm.smdms.core.intent.ENROLLMENT_INFO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 470
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 471
    const-string v2, "com.sec.enterprise.knox.cloudmdm.smdms.core.extra.ENROLLMENT_INFO"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 472
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Container Id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->aC(I)Landroid/os/UserHandle;

    move-result-object v2

    .line 474
    const-string v3, "com.sec.enterprise.knox.cloudmdm.smdms.permission.SAMSUNG_MDM_SERVICE"

    .line 475
    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;

    invoke-direct {v4, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$3;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    .line 490
    const/4 v6, 0x0

    move-object v5, v10

    move-object v7, v10

    move-object v8, v10

    .line 473
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 492
    :cond_0
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 464
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v10

    goto :goto_1

    :cond_1
    move-object v5, v10

    move-object v6, v10

    goto/16 :goto_0
.end method

.method private aF(I)V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qD:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qD:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;

    invoke-interface {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/d;->aE(I)V

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->enableAppStop(Landroid/content/Context;I)V

    .line 323
    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->fr()V

    return-void
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V
    .locals 0

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->fX()V

    return-void
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V
    .locals 0

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qE:I

    return-void
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qE:I

    return v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    return v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Z
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->fY()Z

    move-result v0

    return v0
.end method

.method private fW()V
    .locals 3

    .prologue
    .line 267
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 268
    const-string v1, "com.samsung.knox.container.creation.status"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 269
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    return-void
.end method

.method private fX()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 274
    return-void
.end method

.method private fY()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 326
    .line 328
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 329
    const-string v1, "UMC:ByodSetupStarter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "installUmcInsideContainer:  apk path: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "installUmcInsideContainer:  Thread: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 337
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    const/4 v4, 0x1

    .line 336
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 338
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 339
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    const/4 v4, 0x0

    .line 338
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v3

    .line 343
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->fh()Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 346
    :try_start_2
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    invoke-virtual {v0, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/e;->installExistingPackageAsUser(Ljava/lang/String;I)I

    move-result v0

    .line 347
    const-string v4, "UMC:ByodSetupStarter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Install Result : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    sget v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_SUCCEEDED:I

    if-eq v0, v4, :cond_0

    .line 349
    const-string v0, "UMC:ByodSetupStarter"

    const-string v4, "Fallback to EDM API Package "

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qz:Ljava/lang/String;

    .line 357
    :goto_1
    iget v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    .line 356
    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/d;->k(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 358
    if-eqz v4, :cond_3

    .line 359
    const-string v5, "UMC:ByodSetupStarter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Package:UID = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ":"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    .line 361
    new-instance v6, Landroid/app/enterprise/ContextInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v7, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContainerId:I

    invoke-direct {v6, v4, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v4, 0x0

    .line 360
    invoke-direct {v0, v5, v6, v4}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    .line 362
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/enterprise/ApplicationPolicy;->installApplication(Ljava/lang/String;)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 379
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 381
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    .line 380
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 385
    :cond_1
    :goto_3
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "_installApplication:  Thread: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    return v2

    .line 354
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 364
    :cond_3
    const-string v4, "UMC:ByodSetupStarter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Application Info for package "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " is null"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 368
    :catch_0
    move-exception v0

    .line 370
    :try_start_4
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Package "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " doesn\'t exist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 373
    :catch_1
    move-exception v0

    .line 374
    :goto_4
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 379
    if-eqz v1, :cond_1

    .line 380
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 381
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    .line 380
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_3

    .line 376
    :catchall_0
    move-exception v0

    move v1, v2

    .line 379
    :goto_5
    if-eqz v1, :cond_4

    .line 380
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 381
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    .line 380
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 384
    :cond_4
    throw v0

    .line 376
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 373
    :catch_2
    move-exception v0

    move v1, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto/16 :goto_0
.end method

.method private fq()V
    .locals 3

    .prologue
    .line 255
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 256
    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v1

    .line 258
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/c;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 260
    return-void
.end method

.method private fr()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/c;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 264
    return-void
.end method

.method static synthetic g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->fW()V

    return-void
.end method

.method static synthetic j(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qC:Ljava/lang/String;

    return-object v0
.end method

.method public static q(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 495
    const-string v1, "UMC:ByodSetupStarter"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 496
    const-string v2, "knox-b2b"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 497
    const-string v2, "UMC:ByodSetupStarter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Container ID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    if-lez v1, :cond_0

    .line 499
    invoke-static {v1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->removeContainer(I)I

    move-result v2

    .line 500
    const-string v3, "UMC:ByodSetupStarter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Container Removal Result : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_0
    const-string v2, "UMC:ByodSetupStarter"

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 504
    const-string v3, "knox-b2b"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 505
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 507
    if-lez v1, :cond_1

    .line 508
    const/4 v0, 0x1

    .line 510
    :cond_1
    return v0
.end method


# virtual methods
.method public execute()V
    .locals 5

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qf:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qg:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 234
    :cond_0
    const-string v0, "UMC:ByodSetupStarter"

    const-string v1, "ELM or KLM key is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 236
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    const v1, 0x7f080099

    .line 237
    const v2, 0x7f08009f

    const v3, 0x7f080058

    const/4 v4, 0x1

    .line 236
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->a(IIIZ)V

    .line 238
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->aF(I)V

    .line 252
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qy:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 244
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qx:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 245
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->fq()V

    .line 246
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/c;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/c;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qy:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qC:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo;->getMdmUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/c;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    goto :goto_0
.end method

.method public ft()V
    .locals 2

    .prologue
    .line 514
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->qE:I

    if-lez v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->mContext:Landroid/content/Context;

    const v1, 0x7f080080

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->c(Landroid/content/Context;I)V

    .line 517
    :cond_0
    return-void
.end method
