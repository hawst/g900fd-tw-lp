.class public abstract Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;
.super Ljava/lang/Object;
.source "InstallManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static rr:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

.field private rm:Ljava/lang/String;

.field private rn:Ljava/lang/String;

.field private ro:J

.field private rp:I

.field private rq:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rm:Ljava/lang/String;

    .line 59
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    .line 82
    if-nez p1, :cond_0

    .line 83
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "InstallManager: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 87
    :cond_0
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    .line 89
    return-void
.end method

.method private V(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 277
    .line 279
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_installApplication:  apk path: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->W(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".apk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 284
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v2, "_installApplication invalied Input"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 345
    :cond_1
    :goto_0
    return v0

    .line 288
    :cond_2
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 289
    sget v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_REPLACE_EXISTING:I

    .line 291
    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_INTERNAL:I

    or-int/2addr v0, v2

    .line 294
    new-instance v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;

    invoke-direct {v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;-><init>()V

    .line 297
    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v5, v2, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 298
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Ljava/io/File;->setReadable(ZZ)Z

    .line 302
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 303
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    const/4 v7, 0x1

    .line 302
    invoke-static {v2, v3, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v4, :cond_6

    .line 304
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 305
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    const/4 v7, 0x0

    .line 304
    invoke-static {v2, v3, v7}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v3, v4

    .line 310
    :goto_1
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 312
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v2, v7, v6, v0, v8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->a(Landroid/content/pm/PackageManager;Landroid/net/Uri;Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;ILjava/lang/String;)V

    .line 314
    monitor-enter v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 315
    :goto_2
    :try_start_2
    iget-boolean v0, v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->oT:Z

    if-eqz v0, :cond_3

    .line 323
    iget v0, v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->result:I

    sget v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->INSTALL_SUCCEEDED:I

    if-ne v0, v2, :cond_4

    .line 324
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v2, "_installApplication: success"

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v4

    .line 314
    :goto_3
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 336
    invoke-virtual {v5, v1, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 337
    invoke-virtual {v5, v1, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 339
    if-eqz v3, :cond_1

    .line 340
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 341
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    .line 340
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 317
    :cond_3
    :try_start_4
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v2, "_installApplication:app install inprogress..."

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    invoke-virtual {v6}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 319
    :catch_0
    move-exception v0

    .line 320
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 314
    :catchall_0
    move-exception v0

    move v2, v1

    :goto_4
    :try_start_6
    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 331
    :catch_1
    move-exception v0

    move-object v9, v0

    move v0, v2

    move-object v2, v9

    .line 332
    :goto_5
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 336
    invoke-virtual {v5, v1, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 337
    invoke-virtual {v5, v1, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 339
    if-eqz v3, :cond_1

    .line 340
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 341
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    .line 340
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 327
    :cond_4
    :try_start_9
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "_installApplication:failed "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v6, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->result:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 328
    goto :goto_3

    .line 334
    :catchall_1
    move-exception v0

    move v3, v1

    .line 336
    :goto_6
    invoke-virtual {v5, v1, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 337
    invoke-virtual {v5, v1, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 339
    if-eqz v3, :cond_5

    .line 340
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 341
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/b;->PACKAGE_VERIFIER_ENABLE:Ljava/lang/String;

    .line 340
    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 344
    :cond_5
    throw v0

    .line 334
    :catchall_2
    move-exception v0

    goto :goto_6

    .line 331
    :catch_2
    move-exception v0

    move-object v2, v0

    move v3, v1

    move v0, v1

    goto :goto_5

    :catch_3
    move-exception v0

    move-object v2, v0

    move v0, v1

    goto :goto_5

    .line 314
    :catchall_3
    move-exception v2

    move-object v9, v2

    move v2, v0

    move-object v0, v9

    goto :goto_4

    :catchall_4
    move-exception v0

    goto :goto_4

    :cond_6
    move v3, v1

    goto/16 :goto_1
.end method

.method private static W(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 352
    if-nez p0, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-object v0

    .line 352
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-lez v2, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 353
    :catch_0
    move-exception v1

    .line 354
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static X(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;
    .locals 3

    .prologue
    .line 398
    const/4 v2, 0x0

    .line 400
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 410
    :goto_0
    return-object v0

    .line 403
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    move-object v0, v2

    goto :goto_0

    .line 404
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 405
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    goto :goto_0

    .line 403
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private static Y(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 436
    .line 438
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 441
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 442
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 443
    const/4 v2, 0x1

    goto :goto_0

    .line 441
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;)Z
    .locals 4

    .prologue
    .line 360
    const/4 v0, 0x0

    .line 362
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    if-nez v1, :cond_0

    .line 363
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    .line 365
    :cond_0
    if-nez p0, :cond_1

    .line 376
    :goto_0
    return v0

    .line 368
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->Y(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 369
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "url already in queue "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 373
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "entry added in queue "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage;->a(Landroid/content/Context;Ljava/util/List;)Z

    .line 376
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->V(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 414
    .line 416
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p0, :cond_1

    .line 432
    :cond_0
    :goto_0
    return v2

    .line 419
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    move v1, v2

    .line 420
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 421
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 423
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    iput v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    .line 424
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->state:I

    iput v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->state:I

    .line 425
    const/4 v2, 0x1

    .line 426
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "update "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rr:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage;->a(Landroid/content/Context;Ljava/util/List;)Z

    goto :goto_0

    .line 420
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method static synthetic go()Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic gp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 6

    .prologue
    const/4 v5, -0x3

    .line 93
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "InstallManager: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    .line 120
    :goto_0
    return-void

    .line 98
    :cond_1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rm:Ljava/lang/String;

    .line 99
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    .line 100
    iput p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rp:I

    .line 101
    iput p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rq:I

    .line 103
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rm:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    .line 104
    iget v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rp:I

    iget v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rq:I

    .line 103
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 105
    if-eqz p5, :cond_2

    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rq:I

    if-lez v1, :cond_2

    .line 107
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already url is present on the queue"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    goto :goto_0

    .line 114
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->ro:J

    .line 115
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->ro:J

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;-><init>(Landroid/content/Context;JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    .line 117
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->gk()V

    .line 118
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->gl()V

    goto :goto_0
.end method

.method public aH(I)V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public fZ()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public ga()V
    .locals 2

    .prologue
    .line 146
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "begin update process..."

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 148
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "File is not available for install/update"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    .line 181
    :goto_0
    return-void

    .line 157
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;)V

    .line 179
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public gk()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public gl()V
    .locals 3

    .prologue
    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rm:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;->download(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 136
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 137
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    goto :goto_0
.end method

.method public gm()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method protected gn()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->mDownloadMgr:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/f;

    return-object v0
.end method

.method public onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 5

    .prologue
    .line 224
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "onFailure: "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->rm:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->X(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_0

    .line 227
    iget v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    iget v2, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->maxRetryCount:I

    if-ge v1, v2, :cond_0

    .line 228
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Download will retry for : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->maxRetryCount:I

    iget v4, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    sub-int/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 229
    const-string v3, " times"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 228
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;->retryCount:I

    .line 231
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/install/InstallStorage$InstallAppInfo;)Z

    .line 238
    :goto_0
    return-void

    .line 235
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "No more retry. Informing operation failed "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    goto :goto_0
.end method

.method public onProgress(JII)V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public onStart(J)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 2

    .prologue
    .line 207
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->TAG:Ljava/lang/String;

    const-string v1, "onSuccess: apk successfully downloaded "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->fZ()V

    .line 218
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->ga()V

    .line 220
    return-void
.end method
