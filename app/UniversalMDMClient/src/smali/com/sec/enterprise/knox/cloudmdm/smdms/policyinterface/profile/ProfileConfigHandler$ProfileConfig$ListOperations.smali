.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;
.super Ljava/lang/Object;
.source "ProfileConfigHandler.java"


# instance fields
.field addMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

.field removeMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ParamConfig;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->addMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    .line 99
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    invoke-direct {v0, p4, p5, p6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$ListOperations;->removeMethod:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/profile/ProfileConfigHandler$ProfileConfig$MethodConfig;

    .line 100
    return-void
.end method
