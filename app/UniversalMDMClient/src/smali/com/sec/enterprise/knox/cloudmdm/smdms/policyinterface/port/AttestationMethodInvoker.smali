.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AttestationMethodInvoker;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;
.source "AttestationMethodInvoker.java"

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyApplier;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;)V

    .line 11
    return-void
.end method


# virtual methods
.method protected invoke()Z
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AttestationMethodInvoker;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getMethodName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startAttestation_nonce"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/AttestationMethodInvoker;->mAdmin:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    const-string v1, "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->checkPermission(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Error : Security Exception - Permission Denied - com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/MethodInvoker;->invoke()Z

    move-result v0

    return v0
.end method
