.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;
.super Ljava/lang/Object;
.source "IntentHandler.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:IntentHandler"


# instance fields
.field private mAdminUID:I

.field private mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

.field mContext:Landroid/content/Context;

.field mIntentListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;

.field mIntentReceived:Landroid/content/Intent;

.field mIntentToListenSignature:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

.field theIntentReceiver:Landroid/content/BroadcastReceiver;

.field thruManifest:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 106
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;-><init>(Landroid/content/Context;ILcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/util/Map;Z)V

    .line 107
    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Ljava/util/Map;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->thruManifest:Z

    .line 85
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->theIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 95
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mContext:Landroid/content/Context;

    .line 96
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;

    .line 97
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    .line 98
    invoke-virtual {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->getSignature()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    .line 99
    iput-object p5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    .line 100
    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mAdminUID:I

    .line 101
    iput-boolean p6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->thruManifest:Z

    .line 102
    return-void
.end method

.method private getParam(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 205
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-object v0

    .line 208
    :cond_1
    const-string v1, "Boolean"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 209
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_2
    const-string v1, "Boolean[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 211
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->a([Z)[Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 212
    :cond_3
    const-string v1, "Byte"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 213
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_4
    const-string v1, "Byte[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 215
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->f([B)[Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_5
    const-string v1, "Char"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 217
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getChar(Ljava/lang/String;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_6
    const-string v1, "Char[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 219
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getCharArray(Ljava/lang/String;)[C

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->b([C)[Ljava/lang/Character;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_7
    const-string v1, "Integer"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 221
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 222
    :cond_8
    const-string v1, "Integer[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 223
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->a([I)[Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 224
    :cond_9
    const-string v1, "Short"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 225
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getShort(Ljava/lang/String;)S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    goto/16 :goto_0

    .line 226
    :cond_a
    const-string v1, "Short[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 227
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getShortArray(Ljava/lang/String;)[S

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->a([S)[Ljava/lang/Short;

    move-result-object v0

    goto/16 :goto_0

    .line 228
    :cond_b
    const-string v1, "Long"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 229
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 230
    :cond_c
    const-string v1, "Long[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 231
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->a([J)[Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 232
    :cond_d
    const-string v1, "Float"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 233
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    .line 234
    :cond_e
    const-string v1, "Float[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 235
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->a([F)[Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    .line 236
    :cond_f
    const-string v1, "Double"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 237
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto/16 :goto_0

    .line 238
    :cond_10
    const-string v1, "Double[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 239
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getDoubleArray(Ljava/lang/String;)[D

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/a/a;->a([D)[Ljava/lang/Double;

    move-result-object v0

    goto/16 :goto_0

    .line 240
    :cond_11
    const-string v1, "String"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 241
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 242
    :cond_12
    const-string v1, "String[]"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private isIntentOfInterest(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    const/4 v0, 0x1

    .line 256
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method createTranslatedOutputObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-virtual {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->getSpecialResponseParamType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-static {p2, p3, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->translateObject(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->type:Ljava/lang/reflect/Type;

    if-nez v1, :cond_1

    .line 151
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Argument Translation Error - unable to translate incoming intent values"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_1
    return-object v0
.end method

.method public getAdminUID()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mAdminUID:I

    return v0
.end method

.method public onIntentReceived(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    .line 82
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->schedule()V

    .line 83
    return-void
.end method

.method public registerIntents()V
    .locals 6

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    const-string v0, "UMC:IntentHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerIntents() Intents = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 169
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->thruManifest:Z

    if-eqz v0, :cond_3

    .line 170
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    move-result-object v1

    .line 171
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 172
    invoke-virtual {v1, v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->registerForIntent([Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;)V

    goto :goto_0

    .line 165
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 166
    const-string v2, "UMC:IntentHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UMC Intent Listen Start - Action = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 174
    :cond_3
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 175
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 179
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->theIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 175
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 176
    const-string v4, "UMC:IntentHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "UMC Intent Listen Start - Action = "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public run()V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 113
    const-string v0, "UMC:IntentHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "run() Intent = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    .line 116
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->isIntentOfInterest(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 122
    if-nez v6, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    move-object v3, v2

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;->onIntentOccurence(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 128
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 144
    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;

    iget-object v10, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    move-object v7, v1

    move-object v9, v2

    move v11, v5

    invoke-interface/range {v6 .. v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;->onIntentOccurence(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 128
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 130
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v6, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->getParam(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 133
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mSignature:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/MethodSignature;->isSpecialArg(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 134
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p0, v3, v4, v9}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->createTranslatedOutputObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;

    move-result-object v3

    .line 138
    :goto_2
    iget-object v4, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 139
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;->obj:Ljava/lang/Object;

    invoke-interface {v8, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 140
    :catch_0
    move-exception v3

    .line 141
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;

    iget-object v4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentReceived:Landroid/content/Intent;

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;->onIntentOccurence(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Exception;Landroid/content/Intent;Z)V

    goto :goto_1

    .line 136
    :cond_4
    :try_start_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v9, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator;->createNormalObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ObjectTranslator$Arg;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_2
.end method

.method protected schedule()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;

    invoke-interface {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler$IntentListener;->schedule(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method

.method public unRegisterIntents()V
    .locals 5

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    const-string v0, "UMC:IntentHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unRegisterIntents() Intents = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 194
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->thruManifest:Z

    if-eqz v0, :cond_3

    .line 195
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;

    move-result-object v1

    .line 196
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mIntentToListenSignature:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 197
    invoke-virtual {v1, v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager;->unregisterForIntent([Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyIntentManager$PolicyIntentListener;)V

    goto :goto_0

    .line 190
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 191
    const-string v2, "UMC:IntentHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UMC Intent Listen Stop - Action = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/IntentHandler;->theIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method
