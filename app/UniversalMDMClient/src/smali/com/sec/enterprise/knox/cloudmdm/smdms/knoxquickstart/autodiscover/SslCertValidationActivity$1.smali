.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;
.super Ljava/lang/Object;
.source "SslCertValidationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hD()V
.end annotation


# instance fields
.field final synthetic vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 152
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;Landroid/app/AlertDialog;)V

    .line 154
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " handleReceivedSslError  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    :try_start_0
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " CertificateErrorDialog: Continue pressed  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 158
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " finishing Activity  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    sput-boolean v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vh:Z

    .line 165
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hG()Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->finish()V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    const-string v0, "SSLCertValidationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " finishing Activity  url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    sput-boolean v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vh:Z

    .line 165
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hG()Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->finish()V

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    .line 163
    const-string v1, "SSLCertValidationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " finishing Activity  url: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    sput-boolean v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->vh:Z

    .line 165
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->hG()Landroid/os/ConditionVariable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    .line 166
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity$1;->vi:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/SslCertValidationActivity;->finish()V

    .line 167
    throw v0
.end method
