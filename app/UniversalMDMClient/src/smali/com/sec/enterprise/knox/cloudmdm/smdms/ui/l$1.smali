.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;
.super Ljava/lang/Object;
.source "NewEnrollmentFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->a(Landroid/widget/EditText;I)V
.end annotation


# instance fields
.field private final synthetic yU:Landroid/widget/EditText;

.field final synthetic zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->yU:Landroid/widget/EditText;

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 235
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 237
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->yU:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->yU:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 240
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;

    move-result-object v1

    .line 248
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Ljava/util/ArrayList;

    move-result-object v2

    .line 247
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface$UserAuthField;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/a;Ljava/util/ArrayList;)Z

    move-result v0

    .line 248
    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 254
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l$1;->zm:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/l;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
