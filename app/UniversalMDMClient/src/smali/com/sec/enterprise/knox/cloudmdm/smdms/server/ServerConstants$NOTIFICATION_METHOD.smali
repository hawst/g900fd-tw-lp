.class public final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;
.super Ljava/lang/Enum;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum xi:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

.field public static final enum xj:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

.field private static final synthetic xk:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    const-string v1, "PUSH"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->xi:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    const-string v1, "POLL"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->xj:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->xi:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->xj:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->xk:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;->xk:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
