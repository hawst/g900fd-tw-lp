.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;
.super Landroid/content/BroadcastReceiver;
.source "ByodSetupStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;
.end annotation


# instance fields
.field final synthetic qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    .line 111
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    if-nez p2, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const-string v0, "UMC:ByodSetupStarter"

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    .line 122
    const-string v0, "status"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    const/4 v1, -0x2

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    goto :goto_0

    .line 126
    :cond_2
    const-string v0, "packageName"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 130
    const-string v1, "com.samsung.knox.container.creation.status"

    .line 129
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)V

    .line 133
    const-string v0, "requestId"

    .line 132
    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 134
    const-string v1, "UMC:ByodSetupStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestId : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 136
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    .line 137
    const-string v1, "code"

    .line 136
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 138
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "containerId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v0

    if-lez v0, :cond_4

    .line 141
    const-string v0, "UMC:ByodSetupStarter"

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 142
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 143
    const-string v1, "knox-b2b"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 144
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1$1;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;)V

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/f;)V

    goto/16 :goto_0

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    const/4 v1, -0x3

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;I)V

    .line 160
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->jz()V

    .line 161
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/UserInterface;->iv()V

    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v0

    const/16 v1, -0x3f8

    if-ne v0, v1, :cond_5

    .line 164
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080080

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->b(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 166
    :cond_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->h(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08007f

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->b(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 171
    :cond_6
    const-string v1, "UMC:ByodSetupStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong request Id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "UMC:ByodSetupStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expecting request Id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a$1;->qK:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
