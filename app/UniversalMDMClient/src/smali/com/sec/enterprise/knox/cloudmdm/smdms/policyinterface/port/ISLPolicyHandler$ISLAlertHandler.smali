.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;
.source "ISLPolicyHandler.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UMC:ISLAlertHandler"


# instance fields
.field islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V
    .locals 3

    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/AlertHandler;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;)V

    .line 135
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->policyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLPolicyHolder(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    .line 136
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->policyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p3, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0, v1, v2, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLPolicy(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$PolicyCreationListener;)Ljava/lang/Object;

    .line 138
    :cond_0
    return-void
.end method


# virtual methods
.method public onPolicyCreation(ZLjava/lang/Object;)V
    .locals 4

    .prologue
    .line 156
    if-eqz p1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->policyClassHolder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->contextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    invoke-virtual {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder;->getISLPolicyHolder(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    .line 159
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    const-string v1, "UMC:ISLAlertHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ISLAlertHandler failed to start - Exception = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseISLPolicy.startRuntimeWatch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLRuntimeAlertHandler(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "UmcPolicy.registerForTimaViolationEvent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLTimaAlertHandler(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 148
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "EnterpriseISLPolicy.startRuntimeWatch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLRuntimeAlertHandler(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;)V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->alertRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AlertManager$AlertRequest;->commandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->method:Ljava/lang/String;

    const-string v1, "UmcPolicy.registerForTimaViolationEvent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;->islholder:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/PolicyClassHolder$EnterpriseISLPolicyHolder;->islSubscriber:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;

    invoke-virtual {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLSubscriber;->setISLTimaAlertHandler(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/port/ISLPolicyHandler$ISLAlertHandler;)V

    goto :goto_0
.end method
