.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;
.source "UnenrollProcessor.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

.field private static rf:Ljava/lang/String;


# instance fields
.field mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

.field pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I
    .locals 3

    .prologue
    .line 57
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_f

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_e

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_d

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_c

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_b

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_a

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_9

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_8

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_7

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_6

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_5

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_4

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_3

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_2

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_0

    :goto_10
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_10

    :catch_1
    move-exception v1

    goto :goto_f

    :catch_2
    move-exception v1

    goto :goto_e

    :catch_3
    move-exception v1

    goto :goto_d

    :catch_4
    move-exception v1

    goto :goto_c

    :catch_5
    move-exception v1

    goto :goto_b

    :catch_6
    move-exception v1

    goto :goto_a

    :catch_7
    move-exception v1

    goto :goto_9

    :catch_8
    move-exception v1

    goto :goto_8

    :catch_9
    move-exception v1

    goto :goto_7

    :catch_a
    move-exception v1

    goto :goto_6

    :catch_b
    move-exception v1

    goto/16 :goto_5

    :catch_c
    move-exception v1

    goto/16 :goto_4

    :catch_d
    move-exception v1

    goto/16 :goto_3

    :catch_e
    move-exception v1

    goto/16 :goto_2

    :catch_f
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "com.sec.enterprise.knox.intent.action.UNENROLL_FROM_UMC"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->rf:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/b;-><init>(Landroid/content/Context;)V

    .line 66
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->n(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    .line 68
    return-void
.end method

.method private gh()V
    .locals 3

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->rf:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/EnrollUnenrollReceiver;->qe:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 121
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 122
    const-string v0, "UMC:UnenrollProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Unenroll Intent : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->rf:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "UMC:UnenrollProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fU()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v0, "UMC:UnenrollProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v0, "UMC:UnenrollProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/EnrollUnenrollReceiver;->qe:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 126
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;)V
    .locals 3

    .prologue
    .line 72
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 73
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->$SWITCH_TABLE$com$sec$enterprise$knox$cloudmdm$smdms$server$NetworkOperation()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    const-string v0, "UMC:UnenrollProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled Operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_0
    return-void

    .line 75
    :pswitch_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mPolicyManager:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V

    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v1

    .line 77
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->ra:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->toString()Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aE(Ljava/lang/String;)Z

    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->q(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->pe:Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    invoke-virtual {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->aF(Ljava/lang/String;)Z

    .line 82
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->isContainer(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "UMC:UnenrollProcessor"

    const-string v1, "Deactivating Admin"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->deactivateAdminSliently(Landroid/content/Context;)V

    .line 88
    :cond_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->gh()V

    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qX:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

    invoke-interface {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;->gf()V

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation$ReturnCode;I)V
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 100
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qX:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;

    invoke-interface {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/c;->gf()V

    .line 101
    return-void
.end method

.method public g(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
    .locals 4

    .prologue
    .line 110
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 111
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/d;->qY:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    return-void
.end method
