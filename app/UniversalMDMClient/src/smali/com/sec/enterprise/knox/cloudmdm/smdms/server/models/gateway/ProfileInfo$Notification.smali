.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;
.super Ljava/lang/Object;
.source "ProfileInfo.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private messenger:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

.field private method:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

.field private pollInterval:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMessenger()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->messenger:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    return-object v0
.end method

.method public getMethod()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->method:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    return-object v0
.end method

.method public getPollInterval()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->pollInterval:I

    return v0
.end method

.method public setMessenger(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->messenger:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$MESSENGER_TYPE;

    .line 78
    return-void
.end method

.method public setMethod(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->method:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/ServerConstants$NOTIFICATION_METHOD;

    .line 86
    return-void
.end method

.method public setPollInterval(I)V
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ProfileInfo$Notification;->pollInterval:I

    .line 94
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
