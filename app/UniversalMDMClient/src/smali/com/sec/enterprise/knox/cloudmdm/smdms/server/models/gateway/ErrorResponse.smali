.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ErrorResponse;
.super Ljava/lang/Object;
.source "ErrorResponse.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private code:Ljava/lang/String;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ErrorResponse;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ErrorResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ErrorResponse;->code:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/ErrorResponse;->message:Ljava/lang/String;

    .line 29
    return-void
.end method
