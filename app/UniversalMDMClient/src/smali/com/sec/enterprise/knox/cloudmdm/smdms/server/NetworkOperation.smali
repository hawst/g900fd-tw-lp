.class public final enum Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;
.super Ljava/lang/Enum;
.source "NetworkOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field public static final enum wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

.field private static final synthetic wQ:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "GSLB_SEG_LOOKUP"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    .line 12
    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "GSLB_UMC_LOOKUP"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "GSLB_PROXY_LOOKUP"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "GET_PROFILE"

    invoke-direct {v0, v1, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "DOWNLOAD_RESOURCE"

    invoke-direct {v0, v1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "GET_DEVICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "ENROLL_DEVICE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "DELETE_DEVICE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "UPDATE_DEVICE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "GET_POLICIES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "POLICY_REPORT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "ALERT_REPORT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "ERROR_REPORT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "EVENT_REPORT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "CREATE_ACCOUNT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    const-string v1, "DEVICE_VERSION_LOOKUP"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    .line 7
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wA:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wB:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wC:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wD:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wE:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wF:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wG:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wJ:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wK:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wL:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wM:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wN:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wO:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wP:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wQ:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wQ:[Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
