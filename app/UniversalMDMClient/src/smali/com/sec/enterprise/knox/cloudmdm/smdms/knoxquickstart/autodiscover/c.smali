.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;
.super Ljava/lang/Object;
.source "ActiveSyncClient.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field nW:Lorg/apache/http/impl/client/DefaultHttpClient;

.field uD:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/l;

.field private uc:Landroid/os/ConditionVariable;

.field private ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v2, 0x3a98

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->uc:Landroid/os/ConditionVariable;

    .line 38
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->ud:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    .line 39
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/l;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/l;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->uD:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/l;

    .line 40
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 41
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->uD:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/l;

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 42
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 43
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 44
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 45
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/c;->nW:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 46
    return-void
.end method
