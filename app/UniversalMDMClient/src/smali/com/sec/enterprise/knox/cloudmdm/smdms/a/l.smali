.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/a/l;
.super Ljava/lang/Object;
.source "UserManagerAdapter.java"


# direct methods
.method public static a(Landroid/os/UserManager;Z)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/UserManager;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 14
    invoke-virtual {p0, p1}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    move-result-object v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0

    .line 18
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 21
    goto :goto_0

    .line 18
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    .line 19
    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;

    invoke-direct {v3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/k;-><init>(Landroid/content/pm/UserInfo;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
