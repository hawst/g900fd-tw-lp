.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;
.source "MdmAgentInstaller.java"

# interfaces
.implements Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static rv:Ljava/lang/String;


# instance fields
.field private qC:Ljava/lang/String;

.field private rm:Ljava/lang/String;

.field private rt:Ljava/lang/String;

.field private ru:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    .line 68
    const-string v0, "UMC:MdmAgentInstaller"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    .line 71
    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;-><init>(Landroid/content/Context;)V

    .line 58
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rm:Ljava/lang/String;

    .line 60
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->qC:Ljava/lang/String;

    .line 64
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->ru:Ljava/lang/String;

    .line 93
    sput-object p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    .line 94
    return-void
.end method

.method private aK(I)V
    .locals 3

    .prologue
    .line 312
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 314
    const-string v1, "showWindow"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 316
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;)V

    .line 318
    return-void
.end method

.method private gq()V
    .locals 6

    .prologue
    .line 279
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iT()V

    .line 280
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rm:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->qC:Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->a(Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 281
    return-void
.end method

.method private gr()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 285
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    const-string v1, "download_on_mobilenetwork_mdmagent"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 287
    const-string v1, "download_on_mobilenetwork_mdmagent"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 288
    return v0
.end method

.method private gs()V
    .locals 3

    .prologue
    .line 321
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    const v2, 0x7f08009e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 322
    const/4 v2, 0x0

    .line 321
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 324
    return-void
.end method

.method private t(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->qC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 305
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleted:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 309
    :cond_0
    return-void
.end method

.method private v(Z)V
    .locals 3

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.INSTALL_APP_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 268
    const-string v1, "url"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rm:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 270
    const-string v1, "error_description"

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const-string v1, "packageName"

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/a/c;->a(Landroid/content/Context;)Landroid/support/v4/a/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/a/c;->a(Landroid/content/Intent;)Z

    .line 273
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InstallMdmAgent status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " broadcast sent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->close()V

    .line 276
    return-void
.end method

.method private w(Z)Z
    .locals 3

    .prologue
    .line 292
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DOWNLOAD_ON_MOBILENETWORK_MDMAGENT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    const-string v1, "download_on_mobilenetwork_mdmagent"

    .line 296
    const/4 v2, 0x0

    .line 295
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 297
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 298
    const-string v1, "download_on_mobilenetwork_mdmagent"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 299
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 2

    .prologue
    .line 128
    if-nez p5, :cond_0

    .line 129
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "showing mdm agent download and install status on notification bar"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/h;)V

    .line 133
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->O(Landroid/content/Context;)V

    .line 136
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->a(Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 137
    return-void
.end method

.method public aH(I)V
    .locals 2

    .prologue
    .line 186
    const v0, 0x7f08007b

    .line 187
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 188
    const-string v1, "Download error"

    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    .line 195
    :goto_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->c(ZI)V

    .line 197
    invoke-super {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->aH(I)V

    .line 198
    return-void

    .line 189
    :cond_0
    const/4 v1, -0x2

    if-ne p1, v1, :cond_1

    .line 190
    const-string v0, "Install error"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    .line 191
    const v0, 0x7f08007c

    .line 192
    goto :goto_0

    .line 193
    :cond_1
    const-string v1, "Internal error"

    sput-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    goto :goto_0
.end method

.method public aI(I)V
    .locals 2

    .prologue
    .line 243
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 245
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->iV()Z

    move-result v0

    .line 244
    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->w(Z)Z

    .line 246
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->gq()V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 248
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "Should retry the download"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->t(Landroid/content/Context;)V

    .line 250
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->gq()V

    goto :goto_0
.end method

.method public aJ(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 257
    const-string v0, "User cancel"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    .line 258
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->v(Z)V

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 260
    const-string v0, "User cancel"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    .line 261
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->v(Z)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 97
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 98
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 101
    :cond_1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rm:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->qC:Ljava/lang/String;

    .line 103
    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->ru:Ljava/lang/String;

    .line 107
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->J(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->gr()Z

    move-result v0

    if-nez v0, :cond_2

    .line 109
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->aK(I)V

    .line 111
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "device not connected on Wifi. Inform the user"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_2
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->aK(I)V

    .line 117
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->gq()V

    goto :goto_0
.end method

.method public fZ()V
    .locals 3

    .prologue
    .line 142
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mMdmUrl :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->ru:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->qC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 145
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 146
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_1

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 149
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    .line 150
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mMdmAgentPkgNamee:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->ru:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 156
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getInstance()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->ru:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/AdminManager;->getAdmin(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_2

    .line 159
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->getAppAdminInfo()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin$AdminInfo;->mAdminUId:I

    .line 161
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/Admin;->clearProxyAdminInstallBlock(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPreInstall : Remove Install Block for : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    :goto_2
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->fZ()V

    .line 172
    return-void

    .line 152
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "unable to get MDM agent package name "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 167
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Admin is null for :  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->ru:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public gm()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 177
    const-string v0, "success"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rv:Ljava/lang/String;

    .line 178
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->c(ZI)V

    .line 179
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->v(Z)V

    .line 180
    invoke-super {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->gm()V

    .line 181
    return-void
.end method

.method public onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 2

    .prologue
    .line 233
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "mdmAgent download fail"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-direct {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->gs()V

    .line 235
    invoke-super {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 239
    return-void
.end method

.method public onProgress(JII)V
    .locals 3

    .prologue
    .line 209
    div-int/lit16 v0, p3, 0x400

    div-int/lit16 v1, p4, 0x400

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/MdmAgentInstallActivity;->e(II)V

    .line 210
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onProgress(JII)V

    .line 211
    return-void
.end method

.method public onStart(J)V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onStart(J)V

    .line 205
    return-void
.end method

.method public onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 3

    .prologue
    .line 215
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "mdmAgent download success "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 218
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->qC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 219
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 223
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    .line 224
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mMdmAgentPkgNamee:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->rt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a;->onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 229
    return-void

    .line 226
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/install/a/a;->TAG:Ljava/lang/String;

    const-string v1, "unable to get MDM agent package name "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
