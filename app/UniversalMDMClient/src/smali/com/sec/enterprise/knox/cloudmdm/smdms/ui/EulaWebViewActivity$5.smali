.class Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;
.super Ljava/lang/Object;
.source "EulaWebViewActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation


# instance fields
.field final synthetic yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

.field private final synthetic yL:F


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;F)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yL:F

    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    .line 445
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getTop()I

    move-result v1

    int-to-float v1, v1

    .line 444
    sub-float/2addr v0, v1

    .line 446
    iget v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yL:F

    mul-float/2addr v0, v1

    .line 447
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getTop()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 448
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity$5;->yK:Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/EulaWebViewActivity;)Landroid/webkit/WebView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebView;->scrollTo(II)V

    .line 449
    return-void
.end method
