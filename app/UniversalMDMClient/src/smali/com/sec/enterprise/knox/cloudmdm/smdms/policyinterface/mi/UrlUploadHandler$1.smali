.class Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$Uploader;
.source "UrlUploadHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->Upload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

.field private final synthetic val$argName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    iput-object p3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->val$argName:Ljava/lang/String;

    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$Uploader;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$Uploader;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 4

    .prologue
    .line 170
    const-string v0, "UrlUploadHandler"

    const-string v1, "upload()onFailure "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x0

    .line 172
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->vG:Landroid/os/Bundle;

    const-string v2, "errorMsg"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 174
    if-eqz v1, :cond_0

    .line 175
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 179
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 180
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->val$argName:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v0, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;->onUrlUploadError(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 182
    return-void
.end method

.method public onProgress(JII)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public onStart(J)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    .locals 4

    .prologue
    .line 153
    const-string v0, "UrlUploadHandler"

    const-string v1, "Upload()onSuccess "

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->val$argName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadFile:Ljava/io/File;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$1(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 159
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mUploadListener:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$2(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$UploadListener;->onUrlUploadSuccess(Z)V

    .line 166
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v0, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    .line 163
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;
    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$0(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse;->stateData:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;

    iget-object v1, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandResponse$StateData;->mPendingFilesToUpload:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler$1;->this$0:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;

    # getter for: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->mCommandRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;
    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$3(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/CommandRequest;->params:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    # invokes: Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->Upload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;->access$4(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/mi/UrlUploadHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
