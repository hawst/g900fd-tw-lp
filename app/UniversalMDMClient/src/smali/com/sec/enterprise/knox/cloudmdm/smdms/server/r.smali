.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;
.super Lcom/a/a/a/g;
.source "HttpService.java"


# instance fields
.field final synthetic wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    .line 265
    invoke-direct {p0, p2}, Lcom/a/a/a/g;-><init>(Ljava/io/File;)V

    .line 266
    return-void
.end method

.method private b([Lorg/apache/http/Header;)V
    .locals 4

    .prologue
    .line 316
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 325
    :cond_0
    return-void

    .line 320
    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 322
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v1

    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p1, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 322
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(I[Lorg/apache/http/Header;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 289
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess:statusCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 289
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    .line 292
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;I)V

    .line 293
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Z)V

    .line 295
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    .line 296
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSuccess:caller not intersted any tags!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    return-void

    .line 300
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->b([Lorg/apache/http/Header;)V

    .line 301
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->c(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/util/List;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 304
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v1

    .line 306
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p2, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p2, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 305
    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 330
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFailure: statusCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->b([Lorg/apache/http/Header;)V

    .line 333
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    .line 334
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;I)V

    .line 335
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Z)V

    .line 336
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFailure: file deleted:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 339
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    .line 342
    :cond_0
    return-void
.end method

.method public onFinish()V
    .locals 4

    .prologue
    .line 346
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onFinish ResponseHandler"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 350
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    .line 351
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->f(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)I

    move-result v1

    .line 350
    invoke-direct {v0, v2, v3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    .line 353
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;->setResponseBundle(Landroid/os/Bundle;)V

    .line 355
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->g(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "destFile"

    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 362
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/a/a/a/g;->onFinish()V

    .line 364
    return-void

    .line 359
    :cond_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onProgress(JII)V

    .line 284
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/a/a/a/g;->onProgress(II)V

    .line 285
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 270
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hS()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStart in ResponseHandler"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;->wk:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onStart(J)V

    .line 273
    :cond_0
    invoke-super {p0}, Lcom/a/a/a/g;->onStart()V

    .line 274
    return-void
.end method
