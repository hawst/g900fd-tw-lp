.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;
.super Ljava/lang/Object;
.source "Management.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private pem:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;->pem:Ljava/lang/String;

    return-object v0
.end method

.method public setPem(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gateway/Management$Certificate;->pem:Ljava/lang/String;

    .line 65
    return-void
.end method
