.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;
.super Ljava/lang/Object;
.source "HttpService.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mStatus:I

.field private final ro:J

.field private vE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

.field private wc:Lcom/a/a/a/a;

.field private wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

.field private we:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

.field private wf:Ljava/lang/String;

.field private wg:Ljava/io/File;

.field private wh:Ljava/io/File;

.field private wi:Z

.field private wj:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLcom/sec/enterprise/knox/cloudmdm/smdms/server/g;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    .line 38
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mContext:Landroid/content/Context;

    .line 40
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    .line 42
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    .line 44
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->we:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

    .line 48
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wg:Ljava/io/File;

    .line 52
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wh:Ljava/io/File;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mStatus:I

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wi:Z

    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wj:Landroid/os/Bundle;

    .line 60
    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vE:Ljava/util/List;

    .line 68
    if-eqz p1, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    if-nez p4, :cond_1

    .line 69
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "ContentTransferManager: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 73
    :cond_1
    iput-wide p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->ro:J

    .line 74
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mContext:Landroid/content/Context;

    .line 75
    iput-object p4, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    .line 76
    new-instance v0, Lcom/a/a/a/a;

    invoke-direct {v0}, Lcom/a/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    .line 77
    return-void
.end method

.method private H(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 474
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v2, "Start mutual auth"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v1, "javax.net.debug"

    const-string v2, "ssl"

    invoke-static {v1, v2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 477
    const-string v1, "javax.net.ssl.debug"

    const-string v2, "all"

    invoke-static {v1, v2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 482
    invoke-static {p1, v0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->l(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 483
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Alias : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    if-eqz v1, :cond_1

    .line 485
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v3, "CSR"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->j(Landroid/content/Context;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 487
    if-eqz v1, :cond_0

    .line 488
    new-instance v0, Lorg/apache/a/a/a/a;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/a/a/a/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 505
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const/16 v2, 0x7530

    invoke-virtual {v1, v2}, Lcom/a/a/a/a;->setTimeout(I)V

    .line 508
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    invoke-virtual {v1, v0}, Lcom/a/a/a/a;->a(Lorg/apache/a/a/a/a;)V

    .line 510
    return-void

    .line 489
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/SecurityUtils;->hP()Z

    move-result v2

    if-nez v2, :cond_2

    .line 490
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v3, "Non-TIMA-CCM"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/b;->j(Landroid/content/Context;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 492
    if-eqz v1, :cond_0

    .line 493
    new-instance v0, Lorg/apache/a/a/a/a;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/a/a/a/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    goto :goto_0

    .line 498
    :cond_2
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "TIMA-CCM"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->hK()V

    .line 501
    new-instance v0, Lorg/apache/a/a/a/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/a/a/a/a;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;I)V
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mStatus:I

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wg:Ljava/io/File;

    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Z)V
    .locals 0

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wi:Z

    return-void
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "Upload: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_1
    :goto_0
    return-void

    .line 173
    :cond_2
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wh:Ljava/io/File;

    .line 175
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->we:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

    if-nez v0, :cond_3

    .line 176
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->we:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->we:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

    if-nez v0, :cond_4

    .line 179
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "mResponseHandlerUpload is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->ro:J

    const/16 v4, -0x190

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    goto :goto_0

    .line 185
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    invoke-virtual {v0}, Lcom/a/a/a/a;->eO()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 186
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpParms: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    new-instance v0, Lcom/a/a/a/j;

    invoke-direct {v0}, Lcom/a/a/a/j;-><init>()V

    .line 198
    :try_start_0
    const-string v1, "uploadedfile"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/j;->a(Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :goto_1
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->aT(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->we:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/s;

    invoke-virtual {v1, p2, v0, v2}, Lcom/a/a/a/a;->c(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private aS(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "GET: Invalid Input"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_1
    :goto_0
    return-void

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 105
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->aU(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->fileList()[Ljava/lang/String;

    .line 115
    :cond_3
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hY()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    .line 117
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    if-nez v0, :cond_4

    .line 118
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "mResponseHandler is null"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    iget-wide v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->ro:J

    const/16 v4, -0x190

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    goto :goto_0

    .line 124
    :cond_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    invoke-virtual {v0}, Lcom/a/a/a/a;->eO()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 125
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpParms: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->aT(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    invoke-virtual {v0, p1, v1, v2}, Lcom/a/a/a/a;->b(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    goto :goto_0
.end method

.method private aT(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 147
    if-eqz p1, :cond_0

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Its https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->H(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SSL exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL"

    const-string v2, "X-SSL"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL-Client-Verify"

    const-string v2, "X-SSL-Client-Verify"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL-Client-DN"

    const-string v2, "X-SSL-Client-DN"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL-Client-CN"

    const-string v2, "X-SSL-Client-CN"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL-Issuer"

    const-string v2, "X-SSL-Issuer"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL-Client-NotBefore"

    const-string v2, "X-SSL-Client-NotBefore"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const-string v1, "X-SSL-Client-NotAfter"

    const-string v2, "X-SSL-Client-NotAfter"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aU(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    .line 246
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 250
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Deleting early downloaded file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 255
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic b(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->ro:J

    return-wide v0
.end method

.method static synthetic c(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/util/List;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vE:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wj:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Ljava/io/File;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wg:Ljava/io/File;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->mStatus:I

    return v0
.end method

.method static synthetic g(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wi:Z

    return v0
.end method

.method static synthetic hS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 514
    const-string v0, "USER_AGENT"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 515
    if-eqz v0, :cond_0

    .line 516
    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    invoke-virtual {v1, v0}, Lcom/a/a/a/a;->setUserAgent(Ljava/lang/String;)V

    .line 520
    :goto_0
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 523
    return-void

    .line 518
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->TAG:Ljava/lang/String;

    const-string v1, "No user agent to set!"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 520
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 521
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Lcom/a/a/a/a;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public aN(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 208
    if-nez p1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vE:Ljava/util/List;

    if-nez v0, :cond_2

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vE:Ljava/util/List;

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vE:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->vE:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    .line 132
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->aS(Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public hY()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    if-nez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    .line 235
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wg:Ljava/io/File;

    .line 236
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    iget-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wg:Ljava/io/File;

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;Ljava/io/File;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wd:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    return-object v0
.end method

.method public m(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-direct {p0, v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public n(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 224
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wf:Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->wc:Lcom/a/a/a/a;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/q;->hY()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/r;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/a/a/a/a;->a(Ljava/lang/String;Lcom/a/a/a/j;Lcom/a/a/a/m;)Lcom/a/a/a/i;

    .line 227
    return-void
.end method
