.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyCommandReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TestPolicyCommandReceiver.java"


# static fields
.field private static final DEMO_ACTIVATE_KLM:I = 0x3e8

.field private static final DEMO_APPLY_POLICIES:I = 0x3ea

.field private static final DEMO_GETALLADMINS:I = 0x3ec

.field private static final DEMO_REGISTER_ADMIN:I = 0x3e9

.field private static final DEMO_UNREGISTER_ADMIN:I = 0x3eb

.field private static final TAG:Ljava/lang/String; = "UMC:TestPolicyCommandReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private sendIntentToUmcDemo(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 83
    const-string v0, "UMC:TestPolicyCommandReceiver"

    const-string v1, "sendIntentToUmcDemo action = com.sec.enterprise.knox.demo.intent.action.ACK"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 87
    const-string v1, "com.sec.enterprise.knox.demo.intent.action.ACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 89
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 90
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 92
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 20
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;

    move-result-object v0

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 22
    const-string v2, "action"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 23
    packed-switch v2, :pswitch_data_0

    .line 73
    const-string v0, "UMC:TestPolicyCommandReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized action:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :goto_0
    return-void

    .line 25
    :pswitch_0
    const-string v1, "UMC:TestPolicyCommandReceiver"

    const-string v2, "DEMO_GETALLADMINS received from Demo App"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 27
    const-string v2, "adminList"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getAdminIds()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 28
    const-string v2, "insideContainer"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->isInsideContainer()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 29
    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyCommandReceiver;->sendIntentToUmcDemo(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 33
    :pswitch_1
    const-string v2, "UMC:TestPolicyCommandReceiver"

    const-string v3, "DEMO_ACTIVATE_KLM received from Demo App"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v2, "key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->activateKlm(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :pswitch_2
    const-string v2, "UMC:TestPolicyCommandReceiver"

    const-string v3, "DEMO_REGISTER_ADMIN received from Demo App"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v2, "mdmUrl"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    const-string v3, "mgdUserId"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->enroll(Ljava/lang/String;Ljava/lang/String;)Z

    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 45
    const-string v2, "adminList"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getAdminIds()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 46
    const-string v2, "insideContainer"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->isInsideContainer()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 47
    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyCommandReceiver;->sendIntentToUmcDemo(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 52
    :pswitch_3
    const-string v2, "UMC:TestPolicyCommandReceiver"

    const-string v3, "DEMO_APPLY_POLICIES received from Demo App"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v2, "mdmUrl"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    const-string v3, "user"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 55
    const-string v3, "policyfiles"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->applyPolicyforTestEnterprise(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :pswitch_4
    const-string v2, "UMC:TestPolicyCommandReceiver"

    const-string v3, "DEMO_UNREGISTER_ADMIN received from Demo App"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v2, "mdmUrl"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 62
    const-string v3, "mgdUserId"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->unenroll(Ljava/lang/String;Ljava/lang/String;)Z

    .line 65
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 66
    const-string v2, "adminList"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->getAdminIds()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 67
    const-string v2, "insideContainer"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyHandler;->isInsideContainer()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 68
    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/TestPolicyCommandReceiver;->sendIntentToUmcDemo(Landroid/content/Context;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 23
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method
