.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "PackageManagerAdapter.java"


# instance fields
.field public oT:Z

.field public oU:Ljava/lang/String;

.field public result:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->oU:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 60
    monitor-enter p0

    .line 61
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->oT:Z

    .line 62
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->oU:Ljava/lang/String;

    .line 63
    iput p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/g;->result:I

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 60
    monitor-exit p0

    .line 66
    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
