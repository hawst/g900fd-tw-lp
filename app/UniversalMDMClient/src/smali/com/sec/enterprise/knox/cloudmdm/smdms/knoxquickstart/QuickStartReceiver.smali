.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;
.super Landroid/content/BroadcastReceiver;
.source "QuickStartReceiver.java"


# static fields
.field private static mHandler:Landroid/os/Handler;

.field public static tr:Ljava/lang/String;

.field public static ts:Ljava/lang/String;

.field private static tt:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "com.samsung.knoxpb.mdm"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    .line 61
    const-string v0, "com.sec.enterprise.knox.express"

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->ts:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    .line 70
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 71
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G9006V"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G900F"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G900FQ"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G900I"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G900M"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G900FD"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G900MD"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G901F"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G906K"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G906L"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    const-string v1, "SM-G906S"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static A(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 195
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->ts:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->r(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-nez v0, :cond_1

    .line 196
    const-string v0, "QuickStartReceiver"

    const-string v1, "Need to do My KNOX Clean Up"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    .line 198
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const-string v0, "QuickStartReceiver"

    const-string v1, "No Need for My KNOX Clean Up"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->r(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_0

    .line 203
    const-string v1, "QuickStartReceiver"

    const-string v2, "Apply Uninstall Policy for UMC using My KNOX Manager App"

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableAppUninstall(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static B(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 210
    invoke-static {p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->C(Landroid/content/Context;)V

    .line 211
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 212
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 213
    const-string v2, "FP_DISABLE_PING"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 215
    const/high16 v2, 0x8000000

    .line 214
    invoke-static {p0, v6, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    .line 217
    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 218
    const-string v0, "QuickStartReceiver"

    const-string v1, "addAlarm()"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public static C(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 222
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    .line 223
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 224
    return-void
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 142
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;-><init>()V

    .line 144
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/d;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;I)V

    .line 145
    monitor-enter v1

    .line 146
    :goto_0
    :try_start_0
    iget-boolean v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;->oT:Z

    if-eqz v0, :cond_0

    .line 145
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    iget-boolean v0, v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/f;->result:Z

    return v0

    .line 148
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method static synthetic hg()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method getPackageName(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 159
    :goto_0
    return-object v0

    .line 158
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 164
    const-string v0, "QuickStartReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent Action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.USER_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 166
    const-string v0, "android.intent.extra.user_handle"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 167
    const-string v1, "QuickStartReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 169
    const-string v2, "QuickStartReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "model : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tt:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->jM()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->tr:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->r(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 177
    if-eqz v1, :cond_2

    .line 178
    const-string v2, "QuickStartReceiver"

    const-string v3, "Apply Container FP Policy using My KNOX Manager App"

    invoke-static {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {p1, v1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/UMCAdmin;->disableFingerPrint(Landroid/content/Context;II)V

    .line 182
    :cond_2
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->B(Landroid/content/Context;)V

    goto :goto_0

    .line 183
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "QuickStartReceiver"

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent Replacing : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 184
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->getPackageName(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->ts:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    .line 189
    sget-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/QuickStartReceiver;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
