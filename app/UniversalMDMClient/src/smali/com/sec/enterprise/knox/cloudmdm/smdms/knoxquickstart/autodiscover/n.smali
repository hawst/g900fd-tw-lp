.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;
.super Ljava/lang/Object;
.source "SslValidator.java"

# interfaces
.implements Ljavax/net/ssl/HostnameVerifier;


# instance fields
.field final synthetic vk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;


# direct methods
.method private constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;->vk:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/n;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/autodiscover/m;)V

    return-void
.end method


# virtual methods
.method public verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z
    .locals 2

    .prologue
    .line 39
    .line 41
    :try_start_0
    invoke-interface {p2}, Ljavax/net/ssl/SSLSession;->getPeerHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 42
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 46
    :goto_0
    return v0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const/4 v0, 0x0

    goto :goto_0
.end method
