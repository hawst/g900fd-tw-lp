.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;
.super Landroid/content/BroadcastReceiver;
.source "ByodSetupFinalizer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V
    .locals 5

    .prologue
    .line 208
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->D(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->init()Z

    .line 210
    if-eqz p3, :cond_0

    .line 211
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->l(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 212
    const-string v2, "UMC:ByodSetupFinalizer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Store MDM Certs : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    invoke-virtual {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 215
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->q(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-virtual {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v2

    invoke-virtual {p4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getCertificate()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Certificate;->getPem()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/security/CloudMDMSecurity;->a(Ljava/lang/String;Ljava/security/KeyPair;Ljava/lang/String;)V

    .line 217
    const-string v0, "UMC:ByodSetupFinalizer"

    const-string v1, "Store Cert Key Pair"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 101
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-static {p1, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)I

    .line 103
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 104
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->permissions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0

    .line 105
    :goto_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->n(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;

    move-result-object v0

    .line 107
    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fU()Ljava/lang/String;

    move-result-object v6

    .line 109
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v1

    invoke-static {v6, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/d;->j(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object v5, v6

    .line 113
    :goto_1
    if-nez v5, :cond_0

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 116
    :cond_0
    if-nez v6, :cond_1

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 119
    :cond_1
    const-string v1, "UMC:ByodSetupFinalizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "packageName "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v1, "UMC:ByodSetupFinalizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "adminPackageToShareOwnership "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    iget-object v2, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getEmail()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getIcon()[B

    move-result-object v3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/h;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;Ljava/lang/String;[B[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getPollingInterval()J

    move-result-wide v0

    .line 124
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 125
    iget-object v2, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v2

    .line 126
    sget-object v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->ra:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;

    invoke-virtual {v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/push/PushStorage$PushMessage$PushMessageType;->toString()Ljava/lang/String;

    move-result-object v3

    .line 125
    invoke-static {p1, v0, v1, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/AlarmHandler;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getPayload()Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 132
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getManagement()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device$Management;->getPayload()Lcom/fasterxml/jackson/databind/JsonNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/jackson/databind/JsonNode;->toString()Ljava/lang/String;

    move-result-object v7

    .line 134
    :cond_3
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fU()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 135
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fU()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fV()Ljava/lang/String;

    move-result-object v3

    .line 136
    iget v4, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qN:I

    iget v5, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qO:I

    iget-object v6, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qP:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    .line 135
    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_4
    if-eqz p3, :cond_5

    .line 140
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->keyPair:Ljava/security/KeyPair;

    invoke-virtual {p3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->setKeyPair(Ljava/security/KeyPair;)V

    .line 141
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    iget-object v1, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qQ:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V

    .line 142
    iget-object v0, p2, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->qM:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;

    invoke-direct {p0, p1, p3, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V

    .line 144
    :cond_5
    return-void

    :cond_6
    move-object v5, v7

    goto/16 :goto_1

    :cond_7
    move-object v4, v7

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V
    .locals 4

    .prologue
    .line 149
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/v;)V

    .line 171
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;->getProfile()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/DeviceInfo;->setDeviceUserId(Ljava/lang/String;)V

    .line 172
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;

    move-result-object v0

    .line 173
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;->wI:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v3

    .line 172
    invoke-virtual {v0, v1, p2, v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/PendingNetworkOperationProcessor;->addPendingNetworkOperation(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->fT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->setUrl(Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->hZ()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;

    move-result-object v0

    invoke-virtual {p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/ProfileStorage$Profile;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/t;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V

    .line 177
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 182
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.enterprise.knox.intent.action.LAUNCH_APP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 183
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    const-string v1, "com.sec.enterprise.knox.intent.extra.USER_CREDENTIALS"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    const-string v1, "com.sec.enterprise.knox.intent.extra.APP_PAYLOAD"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const-string v1, "com.sec.enterprise.knox.intent.extra.ELM_ERROR_CODE"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 187
    const-string v1, "com.sec.enterprise.knox.intent.extra.KLM_ERROR_CODE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 188
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 191
    const-string v0, "UMC:ByodSetupFinalizer"

    const-string v1, " Launch Intent : com.sec.enterprise.knox.intent.action.LAUNCH_APP"

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.USER_CREDENTIALS): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 194
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.APP_PAYLOAD): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/e;->zU:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.ELM_ERROR_CODE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Launch Intent Extra(com.sec.enterprise.knox.intent.extra.KLM_ERROR_CODE): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->s(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method public static p(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 202
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 203
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/ui/LaunchActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 204
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 202
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 205
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 76
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User Id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/a/j;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Process uid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v0, "UMC:ByodSetupFinalizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UserHandle "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "UMC:ByodSetupFinalizer"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.enterprise.knox.cloudmdm.smdms.core.intent.ENROLLMENT_INFO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "com.sec.enterprise.knox.cloudmdm.smdms.core.extra.ENROLLMENT_INFO"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;

    .line 85
    const-string v1, "UMC:ByodSetupFinalizer"

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const/4 v2, 0x0

    .line 88
    :try_start_0
    iget-object v1, v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;->device:Ljava/lang/String;

    const-class v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;

    invoke-static {v1, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/x;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->a(Landroid/content/Context;Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/EnrollmentInfo;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/mdm/Device;)V

    .line 94
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->p(Landroid/content/Context;)V

    .line 95
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/byod/ByodSetupFinalizer;->setResultCode(I)V

    .line 97
    :cond_0
    return-void

    .line 89
    :catch_0
    move-exception v1

    .line 90
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    goto :goto_0
.end method
