.class Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;
.source "FtpService.java"


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V
    .locals 1

    .prologue
    .line 87
    invoke-direct/range {p0 .. p8}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;J)V

    .line 88
    return-void
.end method

.method private a(Lorg/apache/commons/net/ftp/c;Ljava/lang/String;)J
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 91
    const-wide/16 v0, 0x0

    .line 92
    invoke-virtual {p1, p2}, Lorg/apache/commons/net/ftp/c;->bF(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v2

    .line 93
    array-length v3, v2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    aget-object v3, v2, v5

    invoke-virtual {v3}, Lorg/apache/commons/net/ftp/FTPFile;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    aget-object v0, v2, v5

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->getSize()J

    move-result-wide v0

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "File size = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-wide v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 102
    new-instance v4, Lorg/apache/commons/net/ftp/c;

    invoke-direct {v4}, Lorg/apache/commons/net/ftp/c;-><init>()V

    .line 103
    new-instance v2, Lorg/apache/commons/net/ftp/f;

    invoke-direct {v2}, Lorg/apache/commons/net/ftp/f;-><init>()V

    .line 110
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->server:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lorg/apache/commons/net/ftp/c;->connect(Ljava/lang/String;)V

    .line 111
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jZ()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jW()I

    move-result v2

    .line 118
    invoke-static {v2}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 119
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V

    .line 120
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    const-string v5, "FTP server refused connection."

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 181
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 124
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->user:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vI:Ljava/lang/String;

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->user:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vI:Ljava/lang/String;

    invoke-virtual {v4, v2, v5}, Lorg/apache/commons/net/ftp/c;->E(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 125
    :cond_3
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jZ()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Lorg/apache/commons/net/ftp/c;->bq(I)Z

    .line 127
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->kj()V

    .line 128
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lorg/apache/commons/net/ftp/c;->G(Z)V

    .line 134
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vK:Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->a(Lorg/apache/commons/net/ftp/c;Ljava/lang/String;)J

    move-result-wide v6

    long-to-int v2, v6

    iput v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vL:I

    .line 135
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Filesize:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vL:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vK:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lorg/apache/commons/net/ftp/c;->bD(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 137
    iget-object v5, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->jZ()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vJ:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 141
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 143
    new-instance v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;

    invoke-direct {v5, p0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;Ljava/io/OutputStream;)V

    .line 157
    invoke-static {v2, v5}, Lorg/apache/commons/io/a;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 158
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 159
    invoke-static {v6}, Lorg/apache/commons/io/a;->a(Ljava/io/OutputStream;)V

    .line 160
    invoke-static {v2}, Lorg/apache/commons/io/a;->c(Ljava/io/InputStream;)V

    .line 170
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->ki()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    :goto_1
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 183
    :try_start_3
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move v2, v1

    .line 189
    :goto_2
    if-nez v2, :cond_4

    .line 190
    iget-object v3, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    new-instance v4, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    iget-wide v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->ro:J

    const/16 v5, 0xc8

    invoke-direct {v4, v6, v7, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onSuccess(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V

    .line 191
    :cond_4
    if-eqz v2, :cond_7

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 172
    :cond_5
    :try_start_4
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->TAG:Ljava/lang/String;

    const-string v5, "Wrong user/pass to login"

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 174
    :catch_0
    move-exception v2

    .line 176
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 181
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 183
    :try_start_6
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    move v2, v3

    .line 184
    goto :goto_2

    :catch_1
    move-exception v2

    move v2, v3

    goto :goto_2

    .line 177
    :catch_2
    move-exception v2

    .line 178
    :try_start_7
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 179
    iget-object v2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->vH:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;

    new-instance v3, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;

    iget-wide v6, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->ro:J

    const/16 v5, 0x1f4

    invoke-direct {v3, v6, v7, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;-><init>(JI)V

    invoke-interface {v2, v3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/g;->onFailure(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/h;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 181
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 183
    :try_start_8
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    move v2, v1

    .line 184
    goto :goto_2

    :catch_3
    move-exception v2

    move v2, v1

    goto :goto_2

    .line 180
    :catchall_0
    move-exception v0

    .line 181
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 183
    :try_start_9
    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/c;->disconnect()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 188
    :cond_6
    :goto_4
    throw v0

    .line 184
    :catch_4
    move-exception v2

    move v2, v1

    goto :goto_2

    :cond_7
    move v0, v1

    .line 191
    goto :goto_3

    .line 184
    :catch_5
    move-exception v1

    goto/16 :goto_0

    :catch_6
    move-exception v1

    goto :goto_4

    :cond_8
    move v2, v1

    goto :goto_2

    :cond_9
    move v2, v3

    goto :goto_2
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/l;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
