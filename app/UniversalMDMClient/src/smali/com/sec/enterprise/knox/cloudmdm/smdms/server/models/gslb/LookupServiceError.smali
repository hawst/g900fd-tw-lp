.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;
.super Ljava/lang/Object;
.source "LookupServiceError.java"


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# instance fields
.field private error:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getError()Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->error:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->status:Ljava/lang/String;

    return-object v0
.end method

.method public setError(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->error:Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError$Error;

    .line 44
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/models/gslb/LookupServiceError;->status:Ljava/lang/String;

    .line 40
    return-void
.end method
