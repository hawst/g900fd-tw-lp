.class Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d$1;
.super Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;
.source "DeviceVersionChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->m(Landroid/content/Context;)V
.end annotation


# instance fields
.field final synthetic qa:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d$1;->qa:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    .line 112
    invoke-direct {p0, p2, p3}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/server/NetworkOperation;Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 125
    const-string v0, "DeviceVersionChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "DeviceVersionChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d$1;->qa:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;Ljava/lang/String;)V

    .line 128
    const/16 v0, 0xcc

    invoke-super {p0, v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->c(ILjava/lang/String;)V

    .line 129
    return-void
.end method

.method public c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 118
    const-string v0, "DeviceVersionChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess Device Version Checker "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d$1;->qa:Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;

    invoke-static {v0, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;->a(Lcom/sec/enterprise/knox/cloudmdm/smdms/core/d;Ljava/lang/String;)V

    .line 120
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/e;->c(ILjava/lang/String;)V

    .line 121
    return-void
.end method
