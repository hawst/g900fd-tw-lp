.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;
.super Ljava/lang/Object;
.source "SecureDataGenerator.java"


# static fields
.field private static ry:Ljava/lang/String;

.field private static final xA:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    const-class v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->ry:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 46
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "AES/ECB/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "DESede/CBC/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "DESede/ECB/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->xA:Ljava/util/Map;

    .line 51
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Long;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 343
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide v2, -0x410efd670d2ddc6L    # -9.459999887958783E288

    xor-long/2addr v0, v2

    .line 344
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;BJ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 335
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x18

    if-ge v0, v1, :cond_1

    .line 336
    :cond_0
    const/4 v0, 0x0

    .line 339
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s%02d%s%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a([BLjava/lang/String;[B)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x0

    const/4 v5, 0x0

    const/16 v4, 0x10

    .line 298
    sget-object v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->xA:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331
    :goto_0
    return-object v0

    .line 302
    :cond_0
    if-eqz p1, :cond_1

    if-nez p3, :cond_2

    .line 303
    :cond_1
    const-string v1, "getEnc1Data : data or F2Value is null"

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_2
    const-string v1, "AES"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 311
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    .line 312
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v3, 0x5

    invoke-virtual {v1, p3, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const-string v3, "AES"

    .line 311
    invoke-direct {v2, v1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 313
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, p3, v5, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 320
    :goto_1
    :try_start_0
    invoke-static {p2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 321
    const-string v4, "CBC"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 322
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 326
    :goto_2
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 315
    :cond_3
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    .line 316
    const/16 v1, 0x18

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/16 v3, 0x18

    invoke-virtual {v1, p3, v5, v3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const-string v3, "DESede"

    .line 315
    invoke-direct {v2, v1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 317
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, p3, v5, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    goto :goto_1

    .line 324
    :cond_4
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {v3, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 328
    :catch_0
    move-exception v1

    .line 329
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getEnc1Data exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(BLjava/lang/String;Ljava/lang/String;J)[B
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 238
    if-eqz p3, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    if-eqz p2, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    move-object v1, p0

    move v2, p1

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(B[B[B[BJ)[B

    move-result-object v5

    goto :goto_0
.end method

.method private a(BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)[B
    .locals 9

    .prologue
    .line 230
    if-eqz p3, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-gez v0, :cond_1

    .line 231
    :cond_0
    const/4 v0, 0x0

    .line 234
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move-object v1, p0

    move v2, p1

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(B[B[B[BJ)[B

    move-result-object v0

    goto :goto_0
.end method

.method private a(B[B[B[BJ)[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 246
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move-object v0, v1

    .line 265
    :goto_0
    return-object v0

    .line 250
    :cond_1
    array-length v0, p2

    array-length v2, p3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x9

    .line 251
    if-eqz p4, :cond_2

    array-length v2, p4

    add-int/2addr v0, v2

    .line 252
    :cond_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 253
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 254
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 255
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 256
    if-eqz p4, :cond_3

    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 257
    :cond_3
    invoke-virtual {v0, p5, p6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 259
    :try_start_0
    const-string v2, "SHA-256"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 260
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 261
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 262
    :catch_0
    move-exception v0

    .line 263
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "md5bytes exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 265
    goto :goto_0
.end method

.method private b(B)Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    .line 144
    new-instance v0, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;-><init>(Landroid/content/Context;)V

    .line 146
    packed-switch p1, :pswitch_data_0

    .line 158
    const/4 v0, 0x0

    .line 162
    :goto_0
    return-object v0

    .line 149
    :pswitch_0
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->in()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 154
    :pswitch_1
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->ip()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bd(Ljava/lang/String;)B
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 196
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;

    invoke-static {}, Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;->fj()Lcom/sec/enterprise/knox/cloudmdm/smdms/core/Core;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;-><init>(Landroid/content/Context;)V

    .line 201
    if-nez p1, :cond_1

    .line 202
    const-string v1, "getDataFormatID : serviceName is null"

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 226
    :cond_0
    :goto_0
    return v0

    .line 206
    :cond_1
    invoke-virtual {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/a;->in()Ljava/lang/String;

    move-result-object v1

    .line 209
    const-string v2, "ELM"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 211
    if-eqz v1, :cond_2

    .line 212
    const/4 v0, 0x0

    .line 213
    goto :goto_0

    .line 214
    :cond_2
    const/4 v0, 0x1

    .line 217
    goto :goto_0

    :cond_3
    const-string v2, "KNOX"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "GSLB"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "SEGD-API"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    :cond_4
    if-eqz v1, :cond_5

    .line 219
    const/4 v0, 0x2

    .line 220
    goto :goto_0

    .line 221
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 168
    .line 170
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 171
    :cond_0
    const-string v1, "getDeviceID : serviceName or baseData is null"

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 192
    :cond_1
    :goto_0
    return-object v0

    .line 176
    :cond_2
    :try_start_0
    const-string v1, "GSLB"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 177
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 178
    const-string v2, "deviceid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180
    :cond_3
    const-string v1, "KNOX"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 181
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 182
    const-string v2, "deviceid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184
    :cond_4
    const-string v1, "SEGD-API"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 186
    const-string v2, "deviceid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v1

    .line 189
    sget-object v2, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->ry:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDeviceID exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private t(Ljava/lang/String;Ljava/lang/String;)B
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 270
    .line 272
    if-nez p1, :cond_1

    .line 273
    const-string v1, "getCiphersuite : serviceName is null"

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 293
    :cond_0
    :goto_0
    return v0

    .line 277
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->bd(Ljava/lang/String;)B

    move-result v1

    .line 278
    if-ne v1, v0, :cond_2

    .line 279
    const-string v1, "getCiphersuite : dataFormatId is null"

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :cond_2
    if-nez p2, :cond_3

    .line 285
    const-string v1, "getCiphersuite : deviceId is null"

    invoke-static {v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 289
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    .line 291
    if-gez v0, :cond_0

    mul-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 18

    .prologue
    .line 55
    const/4 v7, 0x0

    .line 66
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 67
    :cond_0
    const/4 v4, 0x0

    .line 136
    :goto_0
    return-object v4

    .line 70
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->bd(Ljava/lang/String;)B

    move-result v6

    .line 71
    const-string v4, "SecureDataGenerator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "dataformatid: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v4, -0x1

    if-ne v6, v4, :cond_2

    .line 73
    const-string v4, "getSignatureData : dataFormatId is null"

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 74
    const/4 v4, 0x0

    goto :goto_0

    .line 77
    :cond_2
    const-string v4, "ELM"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 78
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->b(B)Ljava/lang/String;

    move-result-object v7

    .line 83
    :cond_3
    :goto_1
    const-string v4, "SecureDataGenerator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "deviceId:"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    if-nez v7, :cond_6

    .line 85
    const-string v4, "getSignatureData : deviceId is null"

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 86
    const/4 v4, 0x0

    goto :goto_0

    .line 79
    :cond_4
    const-string v4, "GSLB"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "KNOX"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "SEGD-API"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 80
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 89
    :cond_6
    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-nez v4, :cond_7

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 96
    :goto_2
    if-eqz p3, :cond_8

    move-object/from16 v5, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    .line 97
    invoke-direct/range {v5 .. v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)[B

    move-result-object v4

    move-object v5, v4

    .line 102
    :goto_3
    if-nez v5, :cond_9

    .line 103
    const-string v4, "getSignatureData : F2Value is null"

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 104
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_7
    move-wide/from16 v10, p4

    .line 92
    goto :goto_2

    :cond_8
    move-object/from16 v12, p0

    move v13, v6

    move-object v14, v7

    move-object/from16 v15, p2

    move-wide/from16 v16, v10

    .line 99
    invoke-direct/range {v12 .. v17}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(BLjava/lang/String;Ljava/lang/String;J)[B

    move-result-object v4

    move-object v5, v4

    goto :goto_3

    .line 107
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->t(Ljava/lang/String;Ljava/lang/String;)B

    move-result v4

    .line 108
    if-ltz v4, :cond_a

    const/4 v7, 0x4

    if-lt v4, v7, :cond_b

    .line 109
    :cond_a
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getSignatureData : ciphersuite("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is invalid."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 110
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 113
    :cond_b
    sget-object v7, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->xA:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 118
    :try_start_0
    const-string v7, "SHA-256"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    .line 119
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->update([B)V

    .line 120
    invoke-virtual {v7}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 127
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4, v5}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a([BLjava/lang/String;[B)Ljava/lang/String;

    move-result-object v4

    .line 129
    if-nez v4, :cond_c

    .line 130
    const-string v4, "getSignatureData : enc1Data is null"

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/b;->d(Ljava/lang/String;)V

    .line 131
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 122
    :catch_0
    move-exception v4

    .line 123
    sget-object v5, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->ry:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getSignatureData exception: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/enterprise/knox/cloudmdm/smdms/b/b;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 134
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v10, v11}, Lcom/sec/enterprise/knox/cloudmdm/smdms/server/a/c;->a(Ljava/lang/String;BJ)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method
