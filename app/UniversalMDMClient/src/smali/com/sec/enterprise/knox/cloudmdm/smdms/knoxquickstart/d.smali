.class Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;
.super Ljava/lang/Object;
.source "EmailCertificatePickerFragment.java"


# instance fields
.field final synthetic rX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

.field sd:Landroid/os/FileObserver;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;)V
    .locals 2

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->rX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 220
    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d$1;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d$1;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->sd:Landroid/os/FileObserver;

    .line 226
    return-void
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;)Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->rX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->d(ILjava/lang/String;)V

    return-void
.end method

.method private d(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 229
    sparse-switch p1, :sswitch_data_0

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 232
    :sswitch_0
    const-string v0, ".p12"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->rX:Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d$2;

    invoke-direct {v1, p0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d$2;-><init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 229
    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method startWatching()V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->sd:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->startWatching()V

    .line 248
    return-void
.end method

.method stopWatching()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/knoxquickstart/d;->sd:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    .line 253
    return-void
.end method
