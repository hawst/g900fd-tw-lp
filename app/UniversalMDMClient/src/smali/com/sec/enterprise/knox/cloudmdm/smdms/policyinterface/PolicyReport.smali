.class public Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;
.super Ljava/lang/Object;
.source "PolicyReport.java"


# instance fields
.field public policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

.field public policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 48
    iput-object p2, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    .line 50
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->fromJsonExternal(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    .line 54
    invoke-static {p2}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->fromJsonExternal(Ljava/lang/String;)Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    .line 55
    return-void
.end method


# virtual methods
.method public getPolicyRequest()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    return-object v0
.end method

.method public getPolicyRequestString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyRequest:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyRequest;->toJsonExternal()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPolicyResponse()Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    return-object v0
.end method

.method public getPolicyResponseString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/PolicyReport;->policyResponse:Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/cloudmdm/smdms/policyinterface/json/PolicyResponse;->toJsonExternal()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
