.class public interface abstract Lcom/google/android/gms/games/multiplayer/Participant;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/a/a",
        "<",
        "Lcom/google/android/gms/games/multiplayer/Participant;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract W()Landroid/net/Uri;
.end method

.method public abstract X()Landroid/net/Uri;
.end method

.method public abstract aq()Ljava/lang/String;
.end method

.method public abstract ar()Z
.end method

.method public abstract at()Ljava/lang/String;
.end method

.method public abstract au()Lcom/google/android/gms/games/Player;
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getStatus()I
.end method
