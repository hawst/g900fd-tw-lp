.class public interface abstract Lcom/google/android/gms/games/Game;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/a/a",
        "<",
        "Lcom/google/android/gms/games/Game;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract T()Ljava/lang/String;
.end method

.method public abstract U()Ljava/lang/String;
.end method

.method public abstract V()Ljava/lang/String;
.end method

.method public abstract W()Landroid/net/Uri;
.end method

.method public abstract X()Landroid/net/Uri;
.end method

.method public abstract Y()Landroid/net/Uri;
.end method

.method public abstract Z()Z
.end method

.method public abstract aa()Z
.end method

.method public abstract ab()Ljava/lang/String;
.end method

.method public abstract ac()I
.end method

.method public abstract ad()I
.end method

.method public abstract ae()I
.end method

.method public abstract getApplicationId()Ljava/lang/String;
.end method

.method public abstract getDescription()Ljava/lang/String;
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method
