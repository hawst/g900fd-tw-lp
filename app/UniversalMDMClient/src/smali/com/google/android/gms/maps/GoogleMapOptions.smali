.class public final Lcom/google/android/gms/maps/GoogleMapOptions;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/ae;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/maps/a;


# instance fields
.field private final fO:I

.field private jl:Ljava/lang/Boolean;

.field private jm:Ljava/lang/Boolean;

.field private jn:I

.field private jo:Lcom/google/android/gms/maps/model/CameraPosition;

.field private jp:Ljava/lang/Boolean;

.field private jq:Ljava/lang/Boolean;

.field private jr:Ljava/lang/Boolean;

.field private js:Ljava/lang/Boolean;

.field private jt:Ljava/lang/Boolean;

.field private ju:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/a;

    invoke-direct {v0}, Lcom/google/android/gms/maps/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/maps/GoogleMapOptions;->CREATOR:Lcom/google/android/gms/maps/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jn:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->fO:I

    return-void
.end method

.method constructor <init>(IBBILcom/google/android/gms/maps/model/CameraPosition;BBBBBB)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jn:I

    iput p1, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->fO:I

    invoke-static {p2}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jl:Ljava/lang/Boolean;

    invoke-static {p3}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jm:Ljava/lang/Boolean;

    iput p4, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jn:I

    iput-object p5, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jo:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-static {p6}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jp:Ljava/lang/Boolean;

    invoke-static {p7}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jq:Ljava/lang/Boolean;

    invoke-static {p8}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jr:Ljava/lang/Boolean;

    invoke-static {p9}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->js:Ljava/lang/Boolean;

    invoke-static {p10}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jt:Ljava/lang/Boolean;

    invoke-static {p11}, Lcom/google/android/gms/internal/w;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->ju:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public aC()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->fO:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dg()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jl:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public dh()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jm:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public di()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jp:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public dj()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jq:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public dk()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jr:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public dl()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->js:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public dm()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jt:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public dn()B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->ju:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/Boolean;)B

    move-result v0

    return v0
.end method

.method public do()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jn:I

    return v0
.end method

.method public dp()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/GoogleMapOptions;->jo:Lcom/google/android/gms/maps/model/CameraPosition;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/internal/x;->bl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/v;->a(Lcom/google/android/gms/maps/GoogleMapOptions;Landroid/os/Parcel;I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/maps/a;->a(Lcom/google/android/gms/maps/GoogleMapOptions;Landroid/os/Parcel;I)V

    goto :goto_0
.end method
