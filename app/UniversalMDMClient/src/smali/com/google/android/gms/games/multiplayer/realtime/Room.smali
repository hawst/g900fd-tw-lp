.class public interface abstract Lcom/google/android/gms/games/multiplayer/realtime/Room;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/a/a;
.implements Lcom/google/android/gms/games/multiplayer/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/a/a",
        "<",
        "Lcom/google/android/gms/games/multiplayer/realtime/Room;",
        ">;",
        "Lcom/google/android/gms/games/multiplayer/a;"
    }
.end annotation


# virtual methods
.method public abstract am()J
.end method

.method public abstract aw()Ljava/lang/String;
.end method

.method public abstract ax()Ljava/lang/String;
.end method

.method public abstract ay()I
.end method

.method public abstract az()Landroid/os/Bundle;
.end method

.method public abstract getDescription()Ljava/lang/String;
.end method

.method public abstract getStatus()I
.end method
