.class public interface abstract Lcom/google/android/gms/games/Player;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/a/a",
        "<",
        "Lcom/google/android/gms/games/Player;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract W()Landroid/net/Uri;
.end method

.method public abstract X()Landroid/net/Uri;
.end method

.method public abstract ag()Ljava/lang/String;
.end method

.method public abstract ah()J
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method
