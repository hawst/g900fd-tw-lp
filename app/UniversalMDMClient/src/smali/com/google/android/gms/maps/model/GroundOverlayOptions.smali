.class public final Lcom/google/android/gms/maps/model/GroundOverlayOptions;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/ae;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/maps/model/d;


# instance fields
.field private final fO:I

.field private jF:F

.field private jG:Z

.field private jH:Lcom/google/android/gms/maps/model/a;

.field private jI:Lcom/google/android/gms/maps/model/LatLng;

.field private jJ:F

.field private jK:F

.field private jL:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private jM:F

.field private jN:F

.field private jO:F

.field private jP:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/d;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->CREATOR:Lcom/google/android/gms/maps/model/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jG:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jN:F

    iput v1, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jO:F

    iput v1, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jP:F

    iput v2, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->fO:I

    return-void
.end method

.method constructor <init>(ILandroid/os/IBinder;Lcom/google/android/gms/maps/model/LatLng;FFLcom/google/android/gms/maps/model/LatLngBounds;FFZFFF)V
    .locals 2

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jG:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jN:F

    iput v1, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jO:F

    iput v1, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jP:F

    iput p1, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->fO:I

    new-instance v0, Lcom/google/android/gms/maps/model/a;

    invoke-static {p2}, Lcom/google/android/gms/internal/s;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/model/a;-><init>(Lcom/google/android/gms/internal/r;)V

    iput-object v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jH:Lcom/google/android/gms/maps/model/a;

    iput-object p3, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jI:Lcom/google/android/gms/maps/model/LatLng;

    iput p4, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jJ:F

    iput p5, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jK:F

    iput-object p6, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jL:Lcom/google/android/gms/maps/model/LatLngBounds;

    iput p7, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jM:F

    iput p8, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jF:F

    iput-boolean p9, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jG:Z

    iput p10, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jN:F

    iput p11, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jO:F

    iput p12, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jP:F

    return-void
.end method


# virtual methods
.method public aC()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->fO:I

    return v0
.end method

.method public dA()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jP:F

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public du()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jF:F

    return v0
.end method

.method public dv()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jH:Lcom/google/android/gms/maps/model/a;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/a;->dq()Lcom/google/android/gms/internal/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/r;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public dw()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jI:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public dx()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jL:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v0
.end method

.method public dy()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jN:F

    return v0
.end method

.method public dz()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jO:F

    return v0
.end method

.method public getBearing()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jM:F

    return v0
.end method

.method public getHeight()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jK:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jJ:F

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->jG:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/internal/x;->bl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/aa;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;Landroid/os/Parcel;I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/maps/model/d;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;Landroid/os/Parcel;I)V

    goto :goto_0
.end method
