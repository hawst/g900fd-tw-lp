.class public interface abstract Lcom/google/android/gms/games/multiplayer/Invitation;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/a/a;
.implements Lcom/google/android/gms/games/multiplayer/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/a/a",
        "<",
        "Lcom/google/android/gms/games/multiplayer/Invitation;",
        ">;",
        "Lcom/google/android/gms/games/multiplayer/a;"
    }
.end annotation


# virtual methods
.method public abstract aj()Lcom/google/android/gms/games/Game;
.end method

.method public abstract ak()Ljava/lang/String;
.end method

.method public abstract al()Lcom/google/android/gms/games/multiplayer/Participant;
.end method

.method public abstract am()J
.end method

.method public abstract an()I
.end method
