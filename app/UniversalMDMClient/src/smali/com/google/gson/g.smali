.class public final Lcom/google/gson/g;
.super Ljava/lang/Object;
.source "GsonBuilder.java"


# instance fields
.field private kA:Z

.field private kB:Z

.field private kG:Lcom/google/gson/internal/c;

.field private kH:Lcom/google/gson/LongSerializationPolicy;

.field private kI:Lcom/google/gson/d;

.field private final kJ:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gson/h",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final kK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/u;",
            ">;"
        }
    .end annotation
.end field

.field private kL:Ljava/lang/String;

.field private kM:I

.field private kN:I

.field private kO:Z

.field private kP:Z

.field private kQ:Z

.field private final kw:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/u;",
            ">;"
        }
    .end annotation
.end field

.field private ky:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    sget-object v0, Lcom/google/gson/internal/c;->ln:Lcom/google/gson/internal/c;

    iput-object v0, p0, Lcom/google/gson/g;->kG:Lcom/google/gson/internal/c;

    .line 70
    sget-object v0, Lcom/google/gson/LongSerializationPolicy;->kU:Lcom/google/gson/LongSerializationPolicy;

    iput-object v0, p0, Lcom/google/gson/g;->kH:Lcom/google/gson/LongSerializationPolicy;

    .line 71
    sget-object v0, Lcom/google/gson/FieldNamingPolicy;->ko:Lcom/google/gson/FieldNamingPolicy;

    iput-object v0, p0, Lcom/google/gson/g;->kI:Lcom/google/gson/d;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/gson/g;->kJ:Ljava/util/Map;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/gson/g;->kw:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/gson/g;->kK:Ljava/util/List;

    .line 79
    iput v1, p0, Lcom/google/gson/g;->kM:I

    .line 80
    iput v1, p0, Lcom/google/gson/g;->kN:I

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gson/g;->kQ:Z

    .line 94
    return-void
.end method

.method private a(Ljava/lang/String;IILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 554
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 555
    new-instance v0, Lcom/google/gson/a;

    invoke-direct {v0, p1}, Lcom/google/gson/a;-><init>(Ljava/lang/String;)V

    .line 562
    :goto_0
    const-class v1, Ljava/util/Date;

    invoke-static {v1}, Lcom/google/gson/b/a;->get(Ljava/lang/Class;)Lcom/google/gson/b/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/gson/r;->a(Lcom/google/gson/b/a;Ljava/lang/Object;)Lcom/google/gson/u;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 563
    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1}, Lcom/google/gson/b/a;->get(Ljava/lang/Class;)Lcom/google/gson/b/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/gson/r;->a(Lcom/google/gson/b/a;Ljava/lang/Object;)Lcom/google/gson/u;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    const-class v1, Ljava/sql/Date;

    invoke-static {v1}, Lcom/google/gson/b/a;->get(Ljava/lang/Class;)Lcom/google/gson/b/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/gson/r;->a(Lcom/google/gson/b/a;Ljava/lang/Object;)Lcom/google/gson/u;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    :cond_0
    return-void

    .line 556
    :cond_1
    if-eq p2, v2, :cond_0

    if-eq p3, v2, :cond_0

    .line 557
    new-instance v0, Lcom/google/gson/a;

    invoke-direct {v0, p2, p3}, Lcom/google/gson/a;-><init>(II)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/gson/b;)Lcom/google/gson/g;
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/gson/g;->kG:Lcom/google/gson/internal/c;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/gson/internal/c;->a(Lcom/google/gson/b;ZZ)Lcom/google/gson/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/g;->kG:Lcom/google/gson/internal/c;

    .line 323
    return-object p0
.end method

.method public a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;
    .locals 2

    .prologue
    .line 448
    instance-of v0, p2, Lcom/google/gson/q;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/k;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/h;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/t;

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/gson/internal/a;->l(Z)V

    .line 452
    instance-of v0, p2, Lcom/google/gson/h;

    if-eqz v0, :cond_1

    .line 453
    iget-object v1, p0, Lcom/google/gson/g;->kJ:Ljava/util/Map;

    move-object v0, p2

    check-cast v0, Lcom/google/gson/h;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    :cond_1
    instance-of v0, p2, Lcom/google/gson/q;

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/google/gson/k;

    if-eqz v0, :cond_3

    .line 456
    :cond_2
    invoke-static {p1}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v0

    .line 457
    iget-object v1, p0, Lcom/google/gson/g;->kw:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/google/gson/r;->b(Lcom/google/gson/b/a;Ljava/lang/Object;)Lcom/google/gson/u;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    :cond_3
    instance-of v0, p2, Lcom/google/gson/t;

    if-eqz v0, :cond_4

    .line 460
    iget-object v0, p0, Lcom/google/gson/g;->kw:Ljava/util/List;

    invoke-static {p1}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object v1

    check-cast p2, Lcom/google/gson/t;

    invoke-static {v1, p2}, Lcom/google/gson/internal/a/p;->a(Lcom/google/gson/b/a;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    :cond_4
    return-object p0

    .line 448
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dJ()Lcom/google/gson/g;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gson/g;->ky:Z

    .line 158
    return-object p0
.end method

.method public dK()Lcom/google/gson/g;
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gson/g;->kB:Z

    .line 351
    return-object p0
.end method

.method public dL()Lcom/google/gson/g;
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/gson/g;->kQ:Z

    .line 363
    return-object p0
.end method

.method public dM()Lcom/google/gson/e;
    .locals 12

    .prologue
    .line 539
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 540
    iget-object v0, p0, Lcom/google/gson/g;->kw:Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 541
    invoke-static {v11}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 542
    iget-object v0, p0, Lcom/google/gson/g;->kK:Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 543
    iget-object v0, p0, Lcom/google/gson/g;->kL:Ljava/lang/String;

    iget v1, p0, Lcom/google/gson/g;->kM:I

    iget v2, p0, Lcom/google/gson/g;->kN:I

    invoke-direct {p0, v0, v1, v2, v11}, Lcom/google/gson/g;->a(Ljava/lang/String;IILjava/util/List;)V

    .line 545
    new-instance v0, Lcom/google/gson/e;

    iget-object v1, p0, Lcom/google/gson/g;->kG:Lcom/google/gson/internal/c;

    iget-object v2, p0, Lcom/google/gson/g;->kI:Lcom/google/gson/d;

    iget-object v3, p0, Lcom/google/gson/g;->kJ:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/google/gson/g;->ky:Z

    iget-boolean v5, p0, Lcom/google/gson/g;->kO:Z

    iget-boolean v6, p0, Lcom/google/gson/g;->kA:Z

    iget-boolean v7, p0, Lcom/google/gson/g;->kQ:Z

    iget-boolean v8, p0, Lcom/google/gson/g;->kB:Z

    iget-boolean v9, p0, Lcom/google/gson/g;->kP:Z

    iget-object v10, p0, Lcom/google/gson/g;->kH:Lcom/google/gson/LongSerializationPolicy;

    invoke-direct/range {v0 .. v11}, Lcom/google/gson/e;-><init>(Lcom/google/gson/internal/c;Lcom/google/gson/d;Ljava/util/Map;ZZZZZZLcom/google/gson/LongSerializationPolicy;Ljava/util/List;)V

    return-object v0
.end method
