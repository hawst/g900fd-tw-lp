.class final Lcom/google/gson/r;
.super Lcom/google/gson/t;
.source "TreeTypeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/gson/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private kF:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final kX:Lcom/google/gson/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/q",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final kY:Lcom/google/gson/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/k",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final kZ:Lcom/google/gson/e;

.field private final la:Lcom/google/gson/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/b/a",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final lb:Lcom/google/gson/u;


# direct methods
.method private constructor <init>(Lcom/google/gson/q;Lcom/google/gson/k;Lcom/google/gson/e;Lcom/google/gson/b/a;Lcom/google/gson/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/q",
            "<TT;>;",
            "Lcom/google/gson/k",
            "<TT;>;",
            "Lcom/google/gson/e;",
            "Lcom/google/gson/b/a",
            "<TT;>;",
            "Lcom/google/gson/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/gson/t;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/gson/r;->kX:Lcom/google/gson/q;

    .line 44
    iput-object p2, p0, Lcom/google/gson/r;->kY:Lcom/google/gson/k;

    .line 45
    iput-object p3, p0, Lcom/google/gson/r;->kZ:Lcom/google/gson/e;

    .line 46
    iput-object p4, p0, Lcom/google/gson/r;->la:Lcom/google/gson/b/a;

    .line 47
    iput-object p5, p0, Lcom/google/gson/r;->lb:Lcom/google/gson/u;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/gson/q;Lcom/google/gson/k;Lcom/google/gson/e;Lcom/google/gson/b/a;Lcom/google/gson/u;Lcom/google/gson/r$1;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/google/gson/r;-><init>(Lcom/google/gson/q;Lcom/google/gson/k;Lcom/google/gson/e;Lcom/google/gson/b/a;Lcom/google/gson/u;)V

    return-void
.end method

.method public static a(Lcom/google/gson/b/a;Ljava/lang/Object;)Lcom/google/gson/u;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/b/a",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 85
    new-instance v0, Lcom/google/gson/s;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/gson/s;-><init>(Ljava/lang/Object;Lcom/google/gson/b/a;ZLjava/lang/Class;Lcom/google/gson/r$1;)V

    return-object v0
.end method

.method public static b(Lcom/google/gson/b/a;Ljava/lang/Object;)Lcom/google/gson/u;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/b/a",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/gson/b/a;->getRawType()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v3, 0x1

    .line 96
    :goto_0
    new-instance v0, Lcom/google/gson/s;

    move-object v1, p1

    move-object v2, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/gson/s;-><init>(Ljava/lang/Object;Lcom/google/gson/b/a;ZLjava/lang/Class;Lcom/google/gson/r$1;)V

    return-object v0

    .line 95
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private ed()Lcom/google/gson/t;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gson/t",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/gson/r;->kF:Lcom/google/gson/t;

    .line 76
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/gson/r;->kZ:Lcom/google/gson/e;

    iget-object v1, p0, Lcom/google/gson/r;->lb:Lcom/google/gson/u;

    iget-object v2, p0, Lcom/google/gson/r;->la:Lcom/google/gson/b/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/e;->a(Lcom/google/gson/u;Lcom/google/gson/b/a;)Lcom/google/gson/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/r;->kF:Lcom/google/gson/t;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/gson/stream/b;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/b;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/gson/r;->kX:Lcom/google/gson/q;

    if-nez v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/google/gson/r;->ed()Lcom/google/gson/t;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/t;->a(Lcom/google/gson/stream/b;Ljava/lang/Object;)V

    .line 72
    :goto_0
    return-void

    .line 66
    :cond_0
    if-nez p2, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/google/gson/stream/b;->ey()Lcom/google/gson/stream/b;

    goto :goto_0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/gson/r;->kX:Lcom/google/gson/q;

    iget-object v1, p0, Lcom/google/gson/r;->la:Lcom/google/gson/b/a;

    invoke-virtual {v1}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lcom/google/gson/r;->kZ:Lcom/google/gson/e;

    iget-object v2, v2, Lcom/google/gson/e;->kD:Lcom/google/gson/p;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/gson/q;->serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/p;)Lcom/google/gson/l;

    move-result-object v0

    .line 71
    invoke-static {v0, p1}, Lcom/google/gson/internal/k;->b(Lcom/google/gson/l;Lcom/google/gson/stream/b;)V

    goto :goto_0
.end method

.method public b(Lcom/google/gson/stream/a;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/a;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/gson/r;->kY:Lcom/google/gson/k;

    if-nez v0, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/google/gson/r;->ed()Lcom/google/gson/t;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/gson/t;->b(Lcom/google/gson/stream/a;)Ljava/lang/Object;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    .line 54
    :cond_0
    invoke-static {p1}, Lcom/google/gson/internal/k;->f(Lcom/google/gson/stream/a;)Lcom/google/gson/l;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/google/gson/l;->dW()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :cond_1
    iget-object v1, p0, Lcom/google/gson/r;->kY:Lcom/google/gson/k;

    iget-object v2, p0, Lcom/google/gson/r;->la:Lcom/google/gson/b/a;

    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    iget-object v3, p0, Lcom/google/gson/r;->kZ:Lcom/google/gson/e;

    iget-object v3, v3, Lcom/google/gson/e;->kC:Lcom/google/gson/j;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/gson/k;->b(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
