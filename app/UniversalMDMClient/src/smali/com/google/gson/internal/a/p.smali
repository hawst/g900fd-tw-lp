.class public final Lcom/google/gson/internal/a/p;
.super Ljava/lang/Object;
.source "TypeAdapters.java"


# static fields
.field public static final mA:Lcom/google/gson/u;

.field public static final mB:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mC:Lcom/google/gson/u;

.field public static final mD:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mE:Lcom/google/gson/u;

.field public static final mF:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mG:Lcom/google/gson/u;

.field public static final mH:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mI:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mJ:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mK:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final mL:Lcom/google/gson/u;

.field public static final mM:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final mN:Lcom/google/gson/u;

.field public static final mO:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final mP:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final mQ:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final mR:Lcom/google/gson/u;

.field public static final mS:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final mT:Lcom/google/gson/u;

.field public static final mU:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final mV:Lcom/google/gson/u;

.field public static final mW:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final mX:Lcom/google/gson/u;

.field public static final mY:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final mZ:Lcom/google/gson/u;

.field public static final mu:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final mv:Lcom/google/gson/u;

.field public static final mw:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final mx:Lcom/google/gson/u;

.field public static final my:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final mz:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final na:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final nb:Lcom/google/gson/u;

.field public static final nc:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final nd:Lcom/google/gson/u;

.field public static final ne:Lcom/google/gson/u;

.field public static final nf:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final ng:Lcom/google/gson/u;

.field public static final nh:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final ni:Lcom/google/gson/u;

.field public static final nj:Lcom/google/gson/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/t",
            "<",
            "Lcom/google/gson/l;",
            ">;"
        }
    .end annotation
.end field

.field public static final nk:Lcom/google/gson/u;

.field public static final nl:Lcom/google/gson/u;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lcom/google/gson/internal/a/p$1;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$1;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mu:Lcom/google/gson/t;

    .line 82
    const-class v0, Ljava/lang/Class;

    sget-object v1, Lcom/google/gson/internal/a/p;->mu:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mv:Lcom/google/gson/u;

    .line 84
    new-instance v0, Lcom/google/gson/internal/a/p$12;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$12;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mw:Lcom/google/gson/t;

    .line 141
    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lcom/google/gson/internal/a/p;->mw:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mx:Lcom/google/gson/u;

    .line 143
    new-instance v0, Lcom/google/gson/internal/a/p$23;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$23;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->my:Lcom/google/gson/t;

    .line 169
    new-instance v0, Lcom/google/gson/internal/a/p$27;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$27;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mz:Lcom/google/gson/t;

    .line 183
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gson/internal/a/p;->my:Lcom/google/gson/t;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mA:Lcom/google/gson/u;

    .line 186
    new-instance v0, Lcom/google/gson/internal/a/p$28;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$28;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mB:Lcom/google/gson/t;

    .line 206
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gson/internal/a/p;->mB:Lcom/google/gson/t;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mC:Lcom/google/gson/u;

    .line 209
    new-instance v0, Lcom/google/gson/internal/a/p$29;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$29;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mD:Lcom/google/gson/t;

    .line 228
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gson/internal/a/p;->mD:Lcom/google/gson/t;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mE:Lcom/google/gson/u;

    .line 231
    new-instance v0, Lcom/google/gson/internal/a/p$30;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$30;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mF:Lcom/google/gson/t;

    .line 250
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gson/internal/a/p;->mF:Lcom/google/gson/t;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mG:Lcom/google/gson/u;

    .line 253
    new-instance v0, Lcom/google/gson/internal/a/p$31;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$31;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mH:Lcom/google/gson/t;

    .line 272
    new-instance v0, Lcom/google/gson/internal/a/p$32;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$32;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mI:Lcom/google/gson/t;

    .line 287
    new-instance v0, Lcom/google/gson/internal/a/p$2;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$2;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mJ:Lcom/google/gson/t;

    .line 302
    new-instance v0, Lcom/google/gson/internal/a/p$3;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$3;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mK:Lcom/google/gson/t;

    .line 322
    const-class v0, Ljava/lang/Number;

    sget-object v1, Lcom/google/gson/internal/a/p;->mK:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mL:Lcom/google/gson/u;

    .line 324
    new-instance v0, Lcom/google/gson/internal/a/p$4;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$4;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mM:Lcom/google/gson/t;

    .line 343
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gson/internal/a/p;->mM:Lcom/google/gson/t;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mN:Lcom/google/gson/u;

    .line 346
    new-instance v0, Lcom/google/gson/internal/a/p$5;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$5;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mO:Lcom/google/gson/t;

    .line 366
    new-instance v0, Lcom/google/gson/internal/a/p$6;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$6;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mP:Lcom/google/gson/t;

    .line 384
    new-instance v0, Lcom/google/gson/internal/a/p$7;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$7;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mQ:Lcom/google/gson/t;

    .line 402
    const-class v0, Ljava/lang/String;

    sget-object v1, Lcom/google/gson/internal/a/p;->mO:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mR:Lcom/google/gson/u;

    .line 404
    new-instance v0, Lcom/google/gson/internal/a/p$8;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$8;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mS:Lcom/google/gson/t;

    .line 419
    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/gson/internal/a/p;->mS:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mT:Lcom/google/gson/u;

    .line 422
    new-instance v0, Lcom/google/gson/internal/a/p$9;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$9;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mU:Lcom/google/gson/t;

    .line 437
    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/google/gson/internal/a/p;->mU:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mV:Lcom/google/gson/u;

    .line 440
    new-instance v0, Lcom/google/gson/internal/a/p$10;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$10;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mW:Lcom/google/gson/t;

    .line 456
    const-class v0, Ljava/net/URL;

    sget-object v1, Lcom/google/gson/internal/a/p;->mW:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mX:Lcom/google/gson/u;

    .line 458
    new-instance v0, Lcom/google/gson/internal/a/p$11;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$11;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->mY:Lcom/google/gson/t;

    .line 478
    const-class v0, Ljava/net/URI;

    sget-object v1, Lcom/google/gson/internal/a/p;->mY:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->mZ:Lcom/google/gson/u;

    .line 480
    new-instance v0, Lcom/google/gson/internal/a/p$13;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$13;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->na:Lcom/google/gson/t;

    .line 496
    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lcom/google/gson/internal/a/p;->na:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->b(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->nb:Lcom/google/gson/u;

    .line 499
    new-instance v0, Lcom/google/gson/internal/a/p$14;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$14;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->nc:Lcom/google/gson/t;

    .line 514
    const-class v0, Ljava/util/UUID;

    sget-object v1, Lcom/google/gson/internal/a/p;->nc:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->nd:Lcom/google/gson/u;

    .line 516
    new-instance v0, Lcom/google/gson/internal/a/p$15;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$15;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->ne:Lcom/google/gson/u;

    .line 537
    new-instance v0, Lcom/google/gson/internal/a/p$16;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$16;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->nf:Lcom/google/gson/t;

    .line 602
    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gson/internal/a/p;->nf:Lcom/google/gson/t;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/p;->b(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->ng:Lcom/google/gson/u;

    .line 605
    new-instance v0, Lcom/google/gson/internal/a/p$17;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$17;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->nh:Lcom/google/gson/t;

    .line 640
    const-class v0, Ljava/util/Locale;

    sget-object v1, Lcom/google/gson/internal/a/p;->nh:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->ni:Lcom/google/gson/u;

    .line 642
    new-instance v0, Lcom/google/gson/internal/a/p$18;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$18;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/p;->nj:Lcom/google/gson/t;

    .line 714
    const-class v0, Lcom/google/gson/l;

    sget-object v1, Lcom/google/gson/internal/a/p;->nj:Lcom/google/gson/t;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/p;->b(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->nk:Lcom/google/gson/u;

    .line 749
    invoke-static {}, Lcom/google/gson/internal/a/p;->ez()Lcom/google/gson/u;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/p;->nl:Lcom/google/gson/u;

    return-void
.end method

.method public static a(Lcom/google/gson/b/a;Lcom/google/gson/t;)Lcom/google/gson/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/b/a",
            "<TTT;>;",
            "Lcom/google/gson/t",
            "<TTT;>;)",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    .line 769
    new-instance v0, Lcom/google/gson/internal/a/p$20;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/p$20;-><init>(Lcom/google/gson/b/a;Lcom/google/gson/t;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/gson/t",
            "<TTT;>;)",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    .line 779
    new-instance v0, Lcom/google/gson/internal/a/p$21;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/p$21;-><init>(Ljava/lang/Class;Lcom/google/gson/t;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/gson/t",
            "<-TTT;>;)",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    .line 792
    new-instance v0, Lcom/google/gson/internal/a/p$22;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/a/p$22;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/gson/t",
            "<TTT;>;)",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    .line 822
    new-instance v0, Lcom/google/gson/internal/a/p$25;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/p$25;-><init>(Ljava/lang/Class;Lcom/google/gson/t;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)Lcom/google/gson/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<+TTT;>;",
            "Lcom/google/gson/t",
            "<-TTT;>;)",
            "Lcom/google/gson/u;"
        }
    .end annotation

    .prologue
    .line 807
    new-instance v0, Lcom/google/gson/internal/a/p$24;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/a/p$24;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/t;)V

    return-object v0
.end method

.method public static ez()Lcom/google/gson/u;
    .locals 1

    .prologue
    .line 752
    new-instance v0, Lcom/google/gson/internal/a/p$19;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p$19;-><init>()V

    return-object v0
.end method
