.class final Lcom/google/gson/internal/a/p$5;
.super Lcom/google/gson/t;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gson/internal/a/p;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/t",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/gson/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/gson/stream/b;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 346
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/a/p$5;->a(Lcom/google/gson/stream/b;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/gson/stream/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 362
    invoke-virtual {p1, p2}, Lcom/google/gson/stream/b;->x(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 363
    return-void
.end method

.method public synthetic b(Lcom/google/gson/stream/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 346
    invoke-virtual {p0, p1}, Lcom/google/gson/internal/a/p$5;->n(Lcom/google/gson/stream/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n(Lcom/google/gson/stream/a;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 349
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->eo()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    .line 350
    sget-object v1, Lcom/google/gson/stream/JsonToken;->nO:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_0

    .line 351
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->nextNull()V

    .line 352
    const/4 v0, 0x0

    .line 358
    :goto_0
    return-object v0

    .line 355
    :cond_0
    sget-object v1, Lcom/google/gson/stream/JsonToken;->nN:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_1

    .line 356
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->nextBoolean()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358
    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->nextString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
