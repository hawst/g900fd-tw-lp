.class final Lcom/google/gson/internal/a/p$14;
.super Lcom/google/gson/t;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gson/internal/a/p;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/t",
        "<",
        "Ljava/util/UUID;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 499
    invoke-direct {p0}, Lcom/google/gson/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/gson/stream/b;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 499
    check-cast p2, Ljava/util/UUID;

    invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/a/p$14;->a(Lcom/google/gson/stream/b;Ljava/util/UUID;)V

    return-void
.end method

.method public a(Lcom/google/gson/stream/b;Ljava/util/UUID;)V
    .locals 1

    .prologue
    .line 510
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/b;->x(Ljava/lang/String;)Lcom/google/gson/stream/b;

    .line 511
    return-void

    .line 510
    :cond_0
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic b(Lcom/google/gson/stream/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lcom/google/gson/internal/a/p$14;->w(Lcom/google/gson/stream/a;)Ljava/util/UUID;

    move-result-object v0

    return-object v0
.end method

.method public w(Lcom/google/gson/stream/a;)Ljava/util/UUID;
    .locals 2

    .prologue
    .line 502
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->eo()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->nO:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_0

    .line 503
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->nextNull()V

    .line 504
    const/4 v0, 0x0

    .line 506
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    goto :goto_0
.end method
