.class public Landroid/support/v4/view/ViewPager;
.super Landroid/view/ViewGroup;
.source "ViewPager.java"


# static fields
.field private static final aZ:[I

.field private static final bW:Landroid/support/v4/view/aj;

.field private static final ba:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/support/v4/view/ad;",
            ">;"
        }
    .end annotation
.end field

.field private static final bb:Landroid/view/animation/Interpolator;


# instance fields
.field private bA:F

.field private bB:I

.field private bC:I

.field private bD:I

.field private bE:I

.field private bF:I

.field private bG:I

.field private bH:I

.field private bI:Z

.field private bJ:Landroid/support/v4/d/a;

.field private bK:Landroid/support/v4/d/a;

.field private bL:Z

.field private bM:Z

.field private bN:Z

.field private bO:I

.field private bP:Landroid/support/v4/view/ag;

.field private bQ:Landroid/support/v4/view/ag;

.field private bR:Landroid/support/v4/view/af;

.field private bS:Landroid/support/v4/view/ah;

.field private bT:Ljava/lang/reflect/Method;

.field private bU:I

.field private bV:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final bX:Ljava/lang/Runnable;

.field private bY:I

.field private final bc:Landroid/support/v4/view/ad;

.field private bd:Landroid/support/v4/view/k;

.field private be:I

.field private bf:I

.field private bg:Landroid/os/Parcelable;

.field private bh:Ljava/lang/ClassLoader;

.field private bi:Landroid/support/v4/view/ai;

.field private bj:I

.field private bk:Landroid/graphics/drawable/Drawable;

.field private bl:I

.field private bm:I

.field private bn:F

.field private bo:F

.field private bp:I

.field private bq:I

.field private br:Z

.field private bs:I

.field private bt:Z

.field private bu:Z

.field private bv:I

.field private bw:I

.field private bx:I

.field private by:F

.field private bz:F

.field private mInLayout:Z

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/view/ad;",
            ">;"
        }
    .end annotation
.end field

.field private mScroller:Landroid/widget/Scroller;

.field private mScrollingCacheEnabled:Z

.field private final mTempRect:Landroid/graphics/Rect;

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 96
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/view/ViewPager;->aZ:[I

    .line 108
    new-instance v0, Landroid/support/v4/view/ViewPager$1;

    invoke-direct {v0}, Landroid/support/v4/view/ViewPager$1;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->ba:Ljava/util/Comparator;

    .line 115
    new-instance v0, Landroid/support/v4/view/ViewPager$2;

    invoke-direct {v0}, Landroid/support/v4/view/ViewPager$2;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->bb:Landroid/view/animation/Interpolator;

    .line 216
    new-instance v0, Landroid/support/v4/view/aj;

    invoke-direct {v0}, Landroid/support/v4/view/aj;-><init>()V

    sput-object v0, Landroid/support/v4/view/ViewPager;->bW:Landroid/support/v4/view/aj;

    return-void
.end method

.method private a(IFII)I
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 2070
    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bE:I

    if-le v0, v1, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bC:I

    if-le v0, v1, :cond_2

    .line 2071
    if-lez p3, :cond_1

    .line 2081
    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2082
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 2083
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ad;

    .line 2086
    iget v0, v0, Landroid/support/v4/view/ad;->position:I

    iget v1, v1, Landroid/support/v4/view/ad;->position:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2089
    :cond_0
    return p1

    .line 2071
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2072
    :cond_2
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bG:I

    if-ltz v0, :cond_3

    iget v0, p0, Landroid/support/v4/view/ViewPager;->bG:I

    if-ge v0, p1, :cond_3

    cmpg-float v0, p2, v2

    if-gez v0, :cond_3

    .line 2073
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2074
    :cond_3
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bH:I

    if-ltz v0, :cond_4

    iget v0, p0, Landroid/support/v4/view/ViewPager;->bH:I

    add-int/lit8 v1, p1, 0x1

    if-le v0, v1, :cond_4

    cmpl-float v0, p2, v2

    if-ltz v0, :cond_4

    .line 2076
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 2078
    :cond_4
    int-to-float v0, p1

    add-float/2addr v0, p2

    add-float/2addr v0, v2

    float-to-int p1, v0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2464
    if-nez p1, :cond_2

    .line 2465
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2467
    :goto_0
    if-nez p2, :cond_0

    .line 2468
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    move-object v0, v1

    .line 2486
    :goto_1
    return-object v0

    .line 2471
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 2472
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 2473
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 2474
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 2476
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2477
    :goto_2
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    if-eq v0, p0, :cond_1

    .line 2478
    check-cast v0, Landroid/view/ViewGroup;

    .line 2479
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2480
    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2481
    iget v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 2482
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2484
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_2

    :cond_1
    move-object v0, v1

    .line 2486
    goto :goto_1

    :cond_2
    move-object v1, p1

    goto :goto_0
.end method

.method private a(IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1398
    if-lez p2, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1399
    add-int v0, p1, p3

    .line 1400
    add-int v1, p2, p4

    .line 1401
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v3

    .line 1402
    int-to-float v3, v3

    int-to-float v1, v1

    div-float v1, v3, v1

    .line 1403
    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 1405
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1406
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1408
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->timePassed()I

    move-result v3

    sub-int v5, v0, v3

    .line 1409
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->j(I)Landroid/support/v4/view/ad;

    move-result-object v3

    .line 1410
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    iget v3, v3, Landroid/support/v4/view/ad;->offset:F

    int-to-float v4, p1

    mul-float/2addr v3, v4

    float-to-int v3, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1422
    :cond_0
    :goto_0
    return-void

    .line 1414
    :cond_1
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->j(I)Landroid/support/v4/view/ad;

    move-result-object v0

    .line 1415
    if-eqz v0, :cond_2

    iget v0, v0, Landroid/support/v4/view/ad;->offset:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bo:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1416
    :goto_1
    int-to-float v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1417
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1418
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->e(Z)V

    .line 1419
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    goto :goto_0

    .line 1415
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IZIZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 529
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->j(I)Landroid/support/v4/view/ad;

    move-result-object v0

    .line 531
    if-eqz v0, :cond_5

    .line 532
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 533
    int-to-float v2, v2

    iget v3, p0, Landroid/support/v4/view/ViewPager;->bn:F

    iget v0, v0, Landroid/support/v4/view/ad;->offset:F

    iget v4, p0, Landroid/support/v4/view/ViewPager;->bo:F

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 536
    :goto_0
    if-eqz p2, :cond_2

    .line 537
    invoke-virtual {p0, v0, v1, p3}, Landroid/support/v4/view/ViewPager;->a(III)V

    .line 538
    if-eqz p4, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ag;->l(I)V

    .line 541
    :cond_0
    if-eqz p4, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bQ:Landroid/support/v4/view/ag;

    if-eqz v0, :cond_1

    .line 542
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bQ:Landroid/support/v4/view/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ag;->l(I)V

    .line 554
    :cond_1
    :goto_1
    return-void

    .line 545
    :cond_2
    if-eqz p4, :cond_3

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    if-eqz v2, :cond_3

    .line 546
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    invoke-interface {v2, p1}, Landroid/support/v4/view/ag;->l(I)V

    .line 548
    :cond_3
    if-eqz p4, :cond_4

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bQ:Landroid/support/v4/view/ag;

    if-eqz v2, :cond_4

    .line 549
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bQ:Landroid/support/v4/view/ag;

    invoke-interface {v2, p1}, Landroid/support/v4/view/ag;->l(I)V

    .line 551
    :cond_4
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->e(Z)V

    .line 552
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/support/v4/view/ad;ILandroid/support/v4/view/ad;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 1061
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0}, Landroid/support/v4/view/k;->getCount()I

    move-result v7

    .line 1062
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v0

    .line 1063
    if-lez v0, :cond_0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    move v6, v0

    .line 1065
    :goto_0
    if-eqz p3, :cond_6

    .line 1066
    iget v0, p3, Landroid/support/v4/view/ad;->position:I

    .line 1068
    iget v1, p1, Landroid/support/v4/view/ad;->position:I

    if-ge v0, v1, :cond_3

    .line 1071
    iget v1, p3, Landroid/support/v4/view/ad;->offset:F

    iget v2, p3, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v1, v2

    add-float v3, v1, v6

    .line 1072
    add-int/lit8 v2, v0, 0x1

    move v1, v4

    .line 1073
    :goto_1
    iget v0, p1, Landroid/support/v4/view/ad;->position:I

    if-gt v2, v0, :cond_6

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1074
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 1075
    :goto_2
    iget v5, v0, Landroid/support/v4/view/ad;->position:I

    if-le v2, v5, :cond_1

    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1

    .line 1076
    add-int/lit8 v1, v1, 0x1

    .line 1077
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    goto :goto_2

    .line 1063
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0

    .line 1079
    :cond_1
    :goto_3
    iget v5, v0, Landroid/support/v4/view/ad;->position:I

    if-ge v2, v5, :cond_2

    .line 1082
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v5, v2}, Landroid/support/v4/view/k;->h(I)F

    move-result v5

    add-float/2addr v5, v6

    add-float/2addr v3, v5

    .line 1083
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1085
    :cond_2
    iput v3, v0, Landroid/support/v4/view/ad;->offset:F

    .line 1086
    iget v0, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v0, v6

    add-float/2addr v3, v0

    .line 1073
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1088
    :cond_3
    iget v1, p1, Landroid/support/v4/view/ad;->position:I

    if-le v0, v1, :cond_6

    .line 1089
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 1091
    iget v3, p3, Landroid/support/v4/view/ad;->offset:F

    .line 1092
    add-int/lit8 v2, v0, -0x1

    .line 1093
    :goto_4
    iget v0, p1, Landroid/support/v4/view/ad;->position:I

    if-lt v2, v0, :cond_6

    if-ltz v1, :cond_6

    .line 1094
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 1095
    :goto_5
    iget v5, v0, Landroid/support/v4/view/ad;->position:I

    if-ge v2, v5, :cond_4

    if-lez v1, :cond_4

    .line 1096
    add-int/lit8 v1, v1, -0x1

    .line 1097
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    goto :goto_5

    .line 1099
    :cond_4
    :goto_6
    iget v5, v0, Landroid/support/v4/view/ad;->position:I

    if-le v2, v5, :cond_5

    .line 1102
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v5, v2}, Landroid/support/v4/view/k;->h(I)F

    move-result v5

    add-float/2addr v5, v6

    sub-float/2addr v3, v5

    .line 1103
    add-int/lit8 v2, v2, -0x1

    goto :goto_6

    .line 1105
    :cond_5
    iget v5, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v5, v6

    sub-float/2addr v3, v5

    .line 1106
    iput v3, v0, Landroid/support/v4/view/ad;->offset:F

    .line 1093
    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    .line 1112
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1113
    iget v2, p1, Landroid/support/v4/view/ad;->offset:F

    .line 1114
    iget v0, p1, Landroid/support/v4/view/ad;->position:I

    add-int/lit8 v1, v0, -0x1

    .line 1115
    iget v0, p1, Landroid/support/v4/view/ad;->position:I

    if-nez v0, :cond_7

    iget v0, p1, Landroid/support/v4/view/ad;->offset:F

    :goto_7
    iput v0, p0, Landroid/support/v4/view/ViewPager;->bn:F

    .line 1116
    iget v0, p1, Landroid/support/v4/view/ad;->position:I

    add-int/lit8 v3, v7, -0x1

    if-ne v0, v3, :cond_8

    iget v0, p1, Landroid/support/v4/view/ad;->offset:F

    iget v3, p1, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v0, v3

    sub-float/2addr v0, v10

    :goto_8
    iput v0, p0, Landroid/support/v4/view/ViewPager;->bo:F

    .line 1119
    add-int/lit8 v0, p2, -0x1

    move v5, v0

    :goto_9
    if-ltz v5, :cond_b

    .line 1120
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    move v3, v2

    .line 1121
    :goto_a
    iget v2, v0, Landroid/support/v4/view/ad;->position:I

    if-le v1, v2, :cond_9

    .line 1122
    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v9, v1}, Landroid/support/v4/view/k;->h(I)F

    move-result v1

    add-float/2addr v1, v6

    sub-float v1, v3, v1

    move v3, v1

    move v1, v2

    goto :goto_a

    .line 1115
    :cond_7
    const v0, -0x800001

    goto :goto_7

    .line 1116
    :cond_8
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_8

    .line 1124
    :cond_9
    iget v2, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v2, v6

    sub-float v2, v3, v2

    .line 1125
    iput v2, v0, Landroid/support/v4/view/ad;->offset:F

    .line 1126
    iget v0, v0, Landroid/support/v4/view/ad;->position:I

    if-nez v0, :cond_a

    iput v2, p0, Landroid/support/v4/view/ViewPager;->bn:F

    .line 1119
    :cond_a
    add-int/lit8 v0, v5, -0x1

    add-int/lit8 v1, v1, -0x1

    move v5, v0

    goto :goto_9

    .line 1128
    :cond_b
    iget v0, p1, Landroid/support/v4/view/ad;->offset:F

    iget v1, p1, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v0, v1

    add-float v2, v0, v6

    .line 1129
    iget v0, p1, Landroid/support/v4/view/ad;->position:I

    add-int/lit8 v1, v0, 0x1

    .line 1131
    add-int/lit8 v0, p2, 0x1

    move v5, v0

    :goto_b
    if-ge v5, v8, :cond_e

    .line 1132
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    move v3, v2

    .line 1133
    :goto_c
    iget v2, v0, Landroid/support/v4/view/ad;->position:I

    if-ge v1, v2, :cond_c

    .line 1134
    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v9, v1}, Landroid/support/v4/view/k;->h(I)F

    move-result v1

    add-float/2addr v1, v6

    add-float/2addr v1, v3

    move v3, v1

    move v1, v2

    goto :goto_c

    .line 1136
    :cond_c
    iget v2, v0, Landroid/support/v4/view/ad;->position:I

    add-int/lit8 v9, v7, -0x1

    if-ne v2, v9, :cond_d

    .line 1137
    iget v2, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v2, v3

    sub-float/2addr v2, v10

    iput v2, p0, Landroid/support/v4/view/ViewPager;->bo:F

    .line 1139
    :cond_d
    iput v3, v0, Landroid/support/v4/view/ad;->offset:F

    .line 1140
    iget v0, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v0, v6

    add-float v2, v3, v0

    .line 1131
    add-int/lit8 v0, v5, 0x1

    add-int/lit8 v1, v1, 0x1

    move v5, v0

    goto :goto_b

    .line 1143
    :cond_e
    iput-boolean v4, p0, Landroid/support/v4/view/ViewPager;->bM:Z

    .line 1144
    return-void
.end method

.method private a(FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1701
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bw:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bw:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    cmpg-float v0, p2, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$400()[I
    .locals 1

    .prologue
    .line 84
    sget-object v0, Landroid/support/v4/view/ViewPager;->aZ:[I

    return-object v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 2307
    invoke-static {p1}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2308
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 2309
    iget v2, p0, Landroid/support/v4/view/ViewPager;->bB:I

    if-ne v1, v2, :cond_0

    .line 2312
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2313
    :goto_0
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 2314
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    .line 2315
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2316
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2319
    :cond_0
    return-void

    .line 2312
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(F)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1976
    .line 1978
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    sub-float/2addr v0, p1

    .line 1979
    iput p1, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 1981
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    .line 1982
    add-float v5, v1, v0

    .line 1983
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v7

    .line 1985
    int-to-float v0, v7

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bn:F

    mul-float v4, v0, v1

    .line 1986
    int-to-float v0, v7

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bo:F

    mul-float v6, v0, v1

    .line 1990
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 1991
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    iget-object v8, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ad;

    .line 1992
    iget v8, v0, Landroid/support/v4/view/ad;->position:I

    if-eqz v8, :cond_5

    .line 1994
    iget v0, v0, Landroid/support/v4/view/ad;->offset:F

    int-to-float v4, v7

    mul-float/2addr v0, v4

    move v4, v0

    move v0, v2

    .line 1996
    :goto_0
    iget v8, v1, Landroid/support/v4/view/ad;->position:I

    iget-object v9, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v9}, Landroid/support/v4/view/k;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v8, v9, :cond_4

    .line 1998
    iget v1, v1, Landroid/support/v4/view/ad;->offset:F

    int-to-float v3, v7

    mul-float/2addr v1, v3

    move v3, v2

    .line 2001
    :goto_1
    cmpg-float v6, v5, v4

    if-gez v6, :cond_1

    .line 2002
    if-eqz v0, :cond_0

    .line 2003
    sub-float v0, v4, v5

    .line 2004
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v2, v7

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v4/d/a;->c(F)Z

    move-result v2

    .line 2015
    :cond_0
    :goto_2
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    float-to-int v1, v4

    int-to-float v1, v1

    sub-float v1, v4, v1

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 2016
    float-to-int v0, v4

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 2017
    float-to-int v0, v4

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->k(I)Z

    .line 2019
    return v2

    .line 2007
    :cond_1
    cmpl-float v0, v5, v1

    if-lez v0, :cond_3

    .line 2008
    if-eqz v3, :cond_2

    .line 2009
    sub-float v0, v5, v1

    .line 2010
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v3, v7

    div-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/support/v4/d/a;->c(F)Z

    move-result v2

    :cond_2
    move v4, v1

    .line 2012
    goto :goto_2

    :cond_3
    move v4, v5

    goto :goto_2

    :cond_4
    move v1, v6

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_0
.end method

.method private e(Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1670
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bY:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    move v0, v4

    .line 1671
    :goto_0
    if-eqz v0, :cond_1

    .line 1673
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 1674
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1675
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    .line 1676
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v3

    .line 1677
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    .line 1678
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    .line 1679
    if-ne v1, v5, :cond_0

    if-eq v3, v6, :cond_1

    .line 1680
    :cond_0
    invoke-virtual {p0, v5, v6}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1683
    :cond_1
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->br:Z

    move v1, v2

    move v3, v0

    .line 1684
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1685
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 1686
    iget-boolean v5, v0, Landroid/support/v4/view/ad;->bZ:Z

    if-eqz v5, :cond_2

    .line 1688
    iput-boolean v2, v0, Landroid/support/v4/view/ad;->bZ:Z

    move v3, v4

    .line 1684
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 1670
    goto :goto_0

    .line 1691
    :cond_4
    if-eqz v3, :cond_5

    .line 1692
    if-eqz p1, :cond_6

    .line 1693
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bX:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/q;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1698
    :cond_5
    :goto_2
    return-void

    .line 1695
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bX:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2
.end method

.method private f(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1705
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    move v2, v1

    .line 1706
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1707
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    .line 1709
    :goto_1
    invoke-virtual {p0, v2}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Landroid/support/v4/view/q;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1706
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1707
    goto :goto_1

    .line 1711
    :cond_1
    return-void
.end method

.method private k(I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1558
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 1559
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bN:Z

    .line 1560
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IFI)V

    .line 1561
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->bN:Z

    if-nez v1, :cond_2

    .line 1562
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1567
    :cond_0
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->q()Landroid/support/v4/view/ad;

    move-result-object v1

    .line 1568
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 1569
    iget v3, p0, Landroid/support/v4/view/ViewPager;->bj:I

    add-int/2addr v3, v2

    .line 1570
    iget v4, p0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    .line 1571
    iget v5, v1, Landroid/support/v4/view/ad;->position:I

    .line 1572
    int-to-float v6, p1

    int-to-float v2, v2

    div-float v2, v6, v2

    iget v6, v1, Landroid/support/v4/view/ad;->offset:F

    sub-float/2addr v2, v6

    iget v1, v1, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v1, v4

    div-float v1, v2, v1

    .line 1574
    int-to-float v2, v3

    mul-float/2addr v2, v1

    float-to-int v2, v2

    .line 1576
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bN:Z

    .line 1577
    invoke-virtual {p0, v5, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IFI)V

    .line 1578
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bN:Z

    if-nez v0, :cond_1

    .line 1579
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1582
    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 444
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 445
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 446
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 447
    iget-boolean v0, v0, Landroid/support/v4/view/ae;->cb:Z

    if-nez v0, :cond_0

    .line 448
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->removeViewAt(I)V

    .line 449
    add-int/lit8 v1, v1, -0x1

    .line 444
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 452
    :cond_1
    return-void
.end method

.method private q()Landroid/support/v4/view/ad;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2027
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v1

    .line 2028
    if-lez v1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    int-to-float v4, v1

    div-float/2addr v0, v4

    move v9, v0

    .line 2029
    :goto_0
    if-lez v1, :cond_4

    iget v0, p0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    .line 2030
    :goto_1
    const/4 v5, -0x1

    .line 2033
    const/4 v4, 0x1

    .line 2035
    const/4 v0, 0x0

    move v6, v2

    move v7, v2

    move v8, v5

    move v2, v3

    move v5, v4

    move-object v4, v0

    .line 2036
    :goto_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2037
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 2039
    if-nez v5, :cond_6

    iget v10, v0, Landroid/support/v4/view/ad;->position:I

    add-int/lit8 v11, v8, 0x1

    if-eq v10, v11, :cond_6

    .line 2041
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bc:Landroid/support/v4/view/ad;

    .line 2042
    add-float/2addr v6, v7

    add-float/2addr v6, v1

    iput v6, v0, Landroid/support/v4/view/ad;->offset:F

    .line 2043
    add-int/lit8 v6, v8, 0x1

    iput v6, v0, Landroid/support/v4/view/ad;->position:I

    .line 2044
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget v7, v0, Landroid/support/v4/view/ad;->position:I

    invoke-virtual {v6, v7}, Landroid/support/v4/view/k;->h(I)F

    move-result v6

    iput v6, v0, Landroid/support/v4/view/ad;->ca:F

    .line 2045
    add-int/lit8 v2, v2, -0x1

    move-object v12, v0

    move v0, v2

    move-object v2, v12

    .line 2047
    :goto_3
    iget v6, v2, Landroid/support/v4/view/ad;->offset:F

    .line 2050
    iget v7, v2, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v7, v6

    add-float/2addr v7, v1

    .line 2051
    if-nez v5, :cond_0

    cmpl-float v5, v9, v6

    if-ltz v5, :cond_2

    .line 2052
    :cond_0
    cmpg-float v4, v9, v7

    if-ltz v4, :cond_1

    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_5

    :cond_1
    move-object v4, v2

    .line 2065
    :cond_2
    return-object v4

    :cond_3
    move v9, v2

    .line 2028
    goto :goto_0

    :cond_4
    move v1, v2

    .line 2029
    goto :goto_1

    .line 2059
    :cond_5
    iget v5, v2, Landroid/support/v4/view/ad;->position:I

    .line 2061
    iget v4, v2, Landroid/support/v4/view/ad;->ca:F

    .line 2036
    add-int/lit8 v0, v0, 0x1

    move v7, v6

    move v8, v5

    move v5, v3

    move v6, v4

    move-object v4, v2

    move v2, v0

    goto :goto_2

    :cond_6
    move-object v12, v0

    move v0, v2

    move-object v2, v12

    goto :goto_3
.end method

.method private r()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2322
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    .line 2323
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bu:Z

    .line 2325
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2326
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2327
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2329
    :cond_0
    return-void
.end method

.method private setScrollState(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 380
    iget v1, p0, Landroid/support/v4/view/ViewPager;->bY:I

    if-ne v1, p1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    iput p1, p0, Landroid/support/v4/view/ViewPager;->bY:I

    .line 385
    if-ne p1, v0, :cond_2

    .line 386
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->bH:I

    iput v1, p0, Landroid/support/v4/view/ViewPager;->bG:I

    .line 388
    :cond_2
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bS:Landroid/support/v4/view/ah;

    if-eqz v1, :cond_3

    .line 390
    if-eqz p1, :cond_4

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->f(Z)V

    .line 392
    :cond_3
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ag;->m(I)V

    goto :goto_0

    .line 390
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setScrollingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 2332
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->mScrollingCacheEnabled:Z

    if-eq v0, p1, :cond_0

    .line 2333
    iput-boolean p1, p0, Landroid/support/v4/view/ViewPager;->mScrollingCacheEnabled:Z

    .line 2344
    :cond_0
    return-void
.end method


# virtual methods
.method a(F)F
    .locals 4

    .prologue
    .line 735
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p1, v0

    .line 736
    float-to-double v0, v0

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 737
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method a(II)Landroid/support/v4/view/ad;
    .locals 2

    .prologue
    .line 799
    new-instance v0, Landroid/support/v4/view/ad;

    invoke-direct {v0}, Landroid/support/v4/view/ad;-><init>()V

    .line 800
    iput p1, v0, Landroid/support/v4/view/ad;->position:I

    .line 801
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v1, p0, p1}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    .line 802
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/k;->h(I)F

    move-result v1

    iput v1, v0, Landroid/support/v4/view/ad;->ca:F

    .line 803
    if-ltz p2, :cond_0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_1

    .line 804
    :cond_0
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 808
    :goto_0
    return-object v0

    .line 806
    :cond_1
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(IFI)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1599
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bO:I

    if-lez v0, :cond_2

    .line 1600
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v5

    .line 1601
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    .line 1602
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v2

    .line 1603
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v6

    .line 1604
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v7

    move v4, v3

    .line 1605
    :goto_0
    if-ge v4, v7, :cond_2

    .line 1606
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1607
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 1608
    iget-boolean v9, v0, Landroid/support/v4/view/ae;->cb:Z

    if-nez v9, :cond_1

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1605
    :cond_0
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v10, v1

    move v1, v2

    move v2, v10

    goto :goto_0

    .line 1610
    :cond_1
    iget v0, v0, Landroid/support/v4/view/ae;->gravity:I

    and-int/lit8 v0, v0, 0x7

    .line 1612
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1629
    :goto_2
    add-int/2addr v0, v5

    .line 1631
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v9

    sub-int/2addr v0, v9

    .line 1632
    if-eqz v0, :cond_0

    .line 1633
    invoke-virtual {v8, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_1

    .line 1618
    :pswitch_1
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    move v10, v1

    move v1, v2

    move v2, v0

    move v0, v10

    .line 1619
    goto :goto_2

    .line 1621
    :pswitch_2
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v6, v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1623
    goto :goto_2

    .line 1625
    :pswitch_3
    sub-int v0, v6, v2

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    sub-int/2addr v0, v9

    .line 1626
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v2, v9

    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_2

    .line 1638
    :cond_2
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bG:I

    if-ltz v0, :cond_3

    iget v0, p0, Landroid/support/v4/view/ViewPager;->bG:I

    if-ge p1, v0, :cond_4

    .line 1639
    :cond_3
    iput p1, p0, Landroid/support/v4/view/ViewPager;->bG:I

    .line 1641
    :cond_4
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bH:I

    if-ltz v0, :cond_5

    int-to-float v0, p1

    add-float/2addr v0, p2

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bH:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 1642
    :cond_5
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bH:I

    .line 1645
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    if-eqz v0, :cond_7

    .line 1646
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ag;->a(IFI)V

    .line 1648
    :cond_7
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bQ:Landroid/support/v4/view/ag;

    if-eqz v0, :cond_8

    .line 1649
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bQ:Landroid/support/v4/view/ag;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ag;->a(IFI)V

    .line 1652
    :cond_8
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bS:Landroid/support/v4/view/ah;

    if-eqz v0, :cond_a

    .line 1653
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v2

    .line 1654
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    move v1, v3

    .line 1655
    :goto_3
    if-ge v1, v4, :cond_a

    .line 1656
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1657
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 1659
    iget-boolean v0, v0, Landroid/support/v4/view/ae;->cb:Z

    if-eqz v0, :cond_9

    .line 1655
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1661
    :cond_9
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v0, v5

    .line 1662
    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->bS:Landroid/support/v4/view/ah;

    invoke-interface {v5, v3, v0}, Landroid/support/v4/view/ah;->a(Landroid/view/View;F)V

    goto :goto_4

    .line 1666
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bN:Z

    .line 1667
    return-void

    .line 1612
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method a(III)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 758
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 760
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 796
    :goto_0
    return-void

    .line 763
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v1

    .line 764
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v2

    .line 765
    sub-int v3, p1, v1

    .line 766
    sub-int v4, p2, v2

    .line 767
    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 768
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->e(Z)V

    .line 769
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->p()V

    .line 770
    invoke-direct {p0, v5}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    goto :goto_0

    .line 774
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 775
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 777
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v0

    .line 778
    div-int/lit8 v5, v0, 0x2

    .line 779
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    int-to-float v7, v0

    div-float/2addr v6, v7

    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 780
    int-to-float v7, v5

    int-to-float v5, v5

    invoke-virtual {p0, v6}, Landroid/support/v4/view/ViewPager;->a(F)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    .line 784
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 785
    if-lez v6, :cond_2

    .line 786
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 792
    :goto_1
    const/16 v5, 0x258

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 794
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 795
    invoke-static {p0}, Landroid/support/v4/view/q;->f(Landroid/view/View;)V

    goto :goto_0

    .line 788
    :cond_2
    int-to-float v0, v0

    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget v6, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-virtual {v5, v6}, Landroid/support/v4/view/k;->h(I)F

    move-result v5

    mul-float/2addr v0, v5

    .line 789
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    div-float v0, v5, v0

    .line 790
    add-float/2addr v0, v8

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v0, v5

    float-to-int v0, v0

    goto :goto_1
.end method

.method a(IZZ)V
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 496
    return-void
.end method

.method a(IZZI)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 499
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0}, Landroid/support/v4/view/k;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 500
    :cond_0
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 525
    :goto_0
    return-void

    .line 503
    :cond_1
    if-nez p3, :cond_2

    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 504
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_0

    .line 508
    :cond_2
    if-gez p1, :cond_5

    move p1, v1

    .line 513
    :cond_3
    :goto_1
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bs:I

    .line 514
    iget v2, p0, Landroid/support/v4/view/ViewPager;->be:I

    add-int/2addr v2, v0

    if-gt p1, v2, :cond_4

    iget v2, p0, Landroid/support/v4/view/ViewPager;->be:I

    sub-int v0, v2, v0

    if-ge p1, v0, :cond_6

    :cond_4
    move v2, v1

    .line 518
    :goto_2
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 519
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    iput-boolean v3, v0, Landroid/support/v4/view/ad;->bZ:Z

    .line 518
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 510
    :cond_5
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0}, Landroid/support/v4/view/k;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_3

    .line 511
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0}, Landroid/support/v4/view/k;->getCount()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_1

    .line 522
    :cond_6
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-eq v0, p1, :cond_7

    move v1, v3

    .line 523
    :cond_7
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->i(I)V

    .line 524
    invoke-direct {p0, p1, p2, p4, v1}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;ZIII)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 2358
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v6, p1

    .line 2359
    check-cast v6, Landroid/view/ViewGroup;

    .line 2360
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v8

    .line 2361
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v9

    .line 2362
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 2364
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_2

    .line 2367
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2368
    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2377
    :cond_0
    :goto_1
    return v2

    .line 2364
    :cond_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    .line 2377
    :cond_2
    if-eqz p2, :cond_3

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/q;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 2510
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2512
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getDescendantFocusability()I

    move-result v2

    .line 2514
    const/high16 v0, 0x60000

    if-eq v2, v0, :cond_1

    .line 2515
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2516
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2517
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 2518
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v4

    .line 2519
    if-eqz v4, :cond_0

    iget v4, v4, Landroid/support/v4/view/ad;->position:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v4, v5, :cond_0

    .line 2520
    invoke-virtual {v3, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2515
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2530
    :cond_1
    const/high16 v0, 0x40000

    if-ne v2, v0, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 2536
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isFocusable()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2547
    :cond_3
    :goto_1
    return-void

    .line 2539
    :cond_4
    and-int/lit8 v0, p3, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->isFocusableInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2543
    :cond_5
    if-eqz p1, :cond_3

    .line 2544
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2557
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2558
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2559
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 2560
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v2

    .line 2561
    if-eqz v2, :cond_0

    iget v2, v2, Landroid/support/v4/view/ad;->position:I

    iget v3, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v2, v3, :cond_0

    .line 2562
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 2557
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2566
    :cond_1
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    .line 1231
    invoke-virtual {p0, p3}, Landroid/support/v4/view/ViewPager;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1232
    invoke-virtual {p0, p3}, Landroid/support/v4/view/ViewPager;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1234
    check-cast v0, Landroid/support/v4/view/ae;

    .line 1235
    iget-boolean v2, v0, Landroid/support/v4/view/ae;->cb:Z

    instance-of v3, p1, Landroid/support/v4/view/ac;

    or-int/2addr v2, v3

    iput-boolean v2, v0, Landroid/support/v4/view/ae;->cb:Z

    .line 1236
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->mInLayout:Z

    if-eqz v2, :cond_1

    .line 1237
    if-eqz v0, :cond_0

    iget-boolean v2, v0, Landroid/support/v4/view/ae;->cb:Z

    if-eqz v2, :cond_0

    .line 1238
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add pager decor view during layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1240
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/view/ae;->cc:Z

    .line 1241
    invoke-virtual {p0, p1, p2, v1}, Landroid/support/v4/view/ViewPager;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1253
    :goto_1
    return-void

    .line 1243
    :cond_1
    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_2
    move-object v1, p3

    goto :goto_0
.end method

.method public arrowScroll(I)Z
    .locals 5

    .prologue
    const/16 v4, 0x42

    const/16 v3, 0x11

    .line 2421
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 2422
    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 2424
    :cond_0
    const/4 v1, 0x0

    .line 2426
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v2

    invoke-virtual {v2, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 2428
    if-eqz v2, :cond_5

    if-eq v2, v0, :cond_5

    .line 2429
    if-ne p1, v3, :cond_3

    .line 2432
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 2433
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 2434
    if-eqz v0, :cond_2

    if-lt v1, v3, :cond_2

    .line 2435
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->s()Z

    move-result v0

    .line 2457
    :goto_0
    if-eqz v0, :cond_1

    .line 2458
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->playSoundEffect(I)V

    .line 2460
    :cond_1
    return v0

    .line 2437
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 2439
    :cond_3
    if-ne p1, v4, :cond_9

    .line 2442
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 2443
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 2444
    if-eqz v0, :cond_4

    if-gt v1, v3, :cond_4

    .line 2445
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->t()Z

    move-result v0

    goto :goto_0

    .line 2447
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 2450
    :cond_5
    if-eq p1, v3, :cond_6

    const/4 v0, 0x1

    if-ne p1, v0, :cond_7

    .line 2452
    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->s()Z

    move-result v0

    goto :goto_0

    .line 2453
    :cond_7
    if-eq p1, v4, :cond_8

    const/4 v0, 0x2

    if-ne p1, v0, :cond_9

    .line 2455
    :cond_8
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->t()Z

    move-result v0

    goto :goto_0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public b(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 486
    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->br:Z

    .line 487
    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 488
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2635
    instance-of v0, p1, Landroid/support/v4/view/ae;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    .line 1534
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1535
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v0

    .line 1536
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollY()I

    move-result v1

    .line 1537
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 1538
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v3

    .line 1540
    if-ne v0, v2, :cond_0

    if-eq v1, v3, :cond_1

    .line 1541
    :cond_0
    invoke-virtual {p0, v2, v3}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1542
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->k(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1543
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1544
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 1549
    :cond_1
    invoke-static {p0}, Landroid/support/v4/view/q;->f(Landroid/view/View;)V

    .line 1555
    :goto_0
    return-void

    .line 1554
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->e(Z)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2383
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2608
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    move v1, v0

    .line 2609
    :goto_0
    if-ge v1, v2, :cond_0

    .line 2610
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2611
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 2612
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v4

    .line 2613
    if-eqz v4, :cond_1

    iget v4, v4, Landroid/support/v4/view/ad;->position:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v4, v5, :cond_1

    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2615
    const/4 v0, 0x1

    .line 2620
    :cond_0
    return v0

    .line 2609
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 2094
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2095
    const/4 v0, 0x0

    .line 2097
    invoke-static {p0}, Landroid/support/v4/view/q;->e(Landroid/view/View;)I

    move-result v1

    .line 2098
    if-eqz v1, :cond_0

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v1}, Landroid/support/v4/view/k;->getCount()I

    move-result v1

    if-le v1, v2, :cond_4

    .line 2101
    :cond_0
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-virtual {v1}, Landroid/support/v4/d/a;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 2103
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2104
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v3

    .line 2106
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2107
    neg-int v4, v2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Landroid/support/v4/view/ViewPager;->bn:F

    int-to-float v6, v3

    mul-float/2addr v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2108
    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-virtual {v4, v2, v3}, Landroid/support/v4/d/a;->setSize(II)V

    .line 2109
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-virtual {v2, p1}, Landroid/support/v4/d/a;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 2110
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2112
    :cond_1
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-virtual {v1}, Landroid/support/v4/d/a;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2113
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 2114
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 2115
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2117
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2118
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    iget v5, p0, Landroid/support/v4/view/ViewPager;->bo:F

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    neg-float v5, v5

    int-to-float v6, v2

    mul-float/2addr v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2119
    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-virtual {v4, v3, v2}, Landroid/support/v4/d/a;->setSize(II)V

    .line 2120
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-virtual {v2, p1}, Landroid/support/v4/d/a;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 2121
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2128
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 2130
    invoke-static {p0}, Landroid/support/v4/view/q;->f(Landroid/view/View;)V

    .line 2132
    :cond_3
    return-void

    .line 2124
    :cond_4
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-virtual {v1}, Landroid/support/v4/d/a;->finish()V

    .line 2125
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-virtual {v1}, Landroid/support/v4/d/a;->finish()V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 723
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 724
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bk:Landroid/graphics/drawable/Drawable;

    .line 725
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 726
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 728
    :cond_0
    return-void
.end method

.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2395
    const/4 v0, 0x0

    .line 2396
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 2397
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2417
    :cond_0
    :goto_0
    return v0

    .line 2399
    :sswitch_0
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 2402
    :sswitch_1
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 2405
    :sswitch_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 2408
    invoke-static {p1}, Landroid/support/v4/view/a;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2409
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 2410
    :cond_1
    invoke-static {p1, v3}, Landroid/support/v4/view/a;->a(Landroid/view/KeyEvent;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2411
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 2397
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x3d -> :sswitch_2
    .end sparse-switch
.end method

.method g(Landroid/view/View;)Landroid/support/v4/view/ad;
    .locals 4

    .prologue
    .line 1256
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1257
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 1258
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v3, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Landroid/support/v4/view/k;->a(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1262
    :goto_1
    return-object v0

    .line 1256
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1262
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2625
    new-instance v0, Landroid/support/v4/view/ae;

    invoke-direct {v0}, Landroid/support/v4/view/ae;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2640
    new-instance v0, Landroid/support/v4/view/ae;

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/view/ae;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2630
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/support/v4/view/k;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 611
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bU:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p1, -0x1

    sub-int p2, v0, p2

    .line 612
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bV:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    iget v0, v0, Landroid/support/v4/view/ae;->cd:I

    .line 613
    return v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 491
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    return v0
.end method

.method public getOffscreenPageLimit()I
    .locals 1

    .prologue
    .line 636
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bs:I

    return v0
.end method

.method public getPageMargin()I
    .locals 1

    .prologue
    .line 692
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bj:I

    return v0
.end method

.method h(Landroid/view/View;)Landroid/support/v4/view/ad;
    .locals 2

    .prologue
    .line 1267
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_2

    .line 1268
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 1269
    :cond_0
    const/4 v0, 0x0

    .line 1273
    :goto_1
    return-object v0

    .line 1271
    :cond_1
    check-cast v0, Landroid/view/View;

    move-object p1, v0

    goto :goto_0

    .line 1273
    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v0

    goto :goto_1
.end method

.method i(I)V
    .locals 14

    .prologue
    .line 885
    const/4 v0, 0x0

    .line 886
    iget v1, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-eq v1, p1, :cond_23

    .line 887
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->j(I)Landroid/support/v4/view/ad;

    move-result-object v0

    .line 888
    iput p1, p0, Landroid/support/v4/view/ViewPager;->be:I

    move-object v1, v0

    .line 891
    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-nez v0, :cond_1

    .line 1058
    :cond_0
    return-void

    .line 899
    :cond_1
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->br:Z

    if-nez v0, :cond_0

    .line 907
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;)V

    .line 913
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bs:I

    .line 914
    const/4 v2, 0x0

    iget v3, p0, Landroid/support/v4/view/ViewPager;->be:I

    sub-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 915
    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v2}, Landroid/support/v4/view/k;->getCount()I

    move-result v8

    .line 916
    add-int/lit8 v2, v8, -0x1

    iget v3, p0, Landroid/support/v4/view/ViewPager;->be:I

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 920
    const/4 v3, 0x0

    .line 921
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_22

    .line 922
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 923
    iget v4, v0, Landroid/support/v4/view/ad;->position:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-lt v4, v5, :cond_8

    .line 924
    iget v4, v0, Landroid/support/v4/view/ad;->position:I

    iget v5, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v4, v5, :cond_22

    .line 929
    :goto_2
    if-nez v0, :cond_21

    if-lez v8, :cond_21

    .line 930
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-virtual {p0, v0, v2}, Landroid/support/v4/view/ViewPager;->a(II)Landroid/support/v4/view/ad;

    move-result-object v0

    move-object v6, v0

    .line 936
    :goto_3
    if-eqz v6, :cond_4

    .line 937
    const/4 v5, 0x0

    .line 938
    add-int/lit8 v4, v2, -0x1

    .line 939
    if-ltz v4, :cond_9

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 940
    :goto_4
    const/high16 v3, 0x40000000    # 2.0f

    iget v10, v6, Landroid/support/v4/view/ad;->ca:F

    sub-float v10, v3, v10

    .line 941
    iget v3, p0, Landroid/support/v4/view/ViewPager;->be:I

    add-int/lit8 v3, v3, -0x1

    move v12, v3

    move v3, v5

    move v5, v12

    move v13, v4

    move v4, v2

    move v2, v13

    :goto_5
    if-ltz v5, :cond_2

    .line 942
    cmpl-float v11, v3, v10

    if-ltz v11, :cond_d

    if-ge v5, v7, :cond_d

    .line 943
    if-nez v0, :cond_a

    .line 969
    :cond_2
    iget v3, v6, Landroid/support/v4/view/ad;->ca:F

    .line 970
    add-int/lit8 v5, v4, 0x1

    .line 971
    const/high16 v0, 0x40000000    # 2.0f

    cmpg-float v0, v3, v0

    if-gez v0, :cond_3

    .line 972
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_11

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 973
    :goto_6
    iget v2, p0, Landroid/support/v4/view/ViewPager;->be:I

    add-int/lit8 v2, v2, 0x1

    move v12, v2

    move v2, v3

    move v3, v5

    move v5, v12

    :goto_7
    if-ge v5, v8, :cond_3

    .line 974
    const/high16 v7, 0x40000000    # 2.0f

    cmpl-float v7, v2, v7

    if-ltz v7, :cond_14

    if-le v5, v9, :cond_14

    .line 975
    if-nez v0, :cond_12

    .line 1000
    :cond_3
    invoke-direct {p0, v6, v4, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ad;ILandroid/support/v4/view/ad;)V

    .line 1010
    :cond_4
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget v2, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-eqz v6, :cond_18

    iget-object v0, v6, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    :goto_8
    invoke-virtual {v1, p0, v2, v0}, Landroid/support/v4/view/k;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1012
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/k;->b(Landroid/view/ViewGroup;)V

    .line 1016
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bU:I

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    move v2, v0

    .line 1017
    :goto_9
    if-eqz v2, :cond_5

    .line 1018
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bV:Ljava/util/ArrayList;

    if-nez v0, :cond_1a

    .line 1019
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->bV:Ljava/util/ArrayList;

    .line 1024
    :cond_5
    :goto_a
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    .line 1025
    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v3, :cond_1b

    .line 1026
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1027
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 1028
    iput v1, v0, Landroid/support/v4/view/ae;->cd:I

    .line 1029
    iget-boolean v5, v0, Landroid/support/v4/view/ae;->cb:Z

    if-nez v5, :cond_6

    iget v5, v0, Landroid/support/v4/view/ae;->ca:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_6

    .line 1031
    invoke-virtual {p0, v4}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v5

    .line 1032
    if-eqz v5, :cond_6

    .line 1033
    iget v6, v5, Landroid/support/v4/view/ad;->ca:F

    iput v6, v0, Landroid/support/v4/view/ae;->ca:F

    .line 1034
    iget v5, v5, Landroid/support/v4/view/ad;->position:I

    iput v5, v0, Landroid/support/v4/view/ae;->position:I

    .line 1037
    :cond_6
    if-eqz v2, :cond_7

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bV:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1025
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 921
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 939
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 946
    :cond_a
    iget v11, v0, Landroid/support/v4/view/ad;->position:I

    if-ne v5, v11, :cond_b

    iget-boolean v11, v0, Landroid/support/v4/view/ad;->bZ:Z

    if-nez v11, :cond_b

    .line 947
    iget-object v11, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 948
    iget-object v11, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v0, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    invoke-virtual {v11, p0, v5, v0}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 953
    add-int/lit8 v2, v2, -0x1

    .line 954
    add-int/lit8 v4, v4, -0x1

    .line 955
    if-ltz v2, :cond_c

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 941
    :cond_b
    :goto_c
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_5

    .line 955
    :cond_c
    const/4 v0, 0x0

    goto :goto_c

    .line 957
    :cond_d
    if-eqz v0, :cond_f

    iget v11, v0, Landroid/support/v4/view/ad;->position:I

    if-ne v5, v11, :cond_f

    .line 958
    iget v0, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v3, v0

    .line 959
    add-int/lit8 v2, v2, -0x1

    .line 960
    if-ltz v2, :cond_e

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    goto :goto_c

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    .line 962
    :cond_f
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p0, v5, v0}, Landroid/support/v4/view/ViewPager;->a(II)Landroid/support/v4/view/ad;

    move-result-object v0

    .line 963
    iget v0, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v3, v0

    .line 964
    add-int/lit8 v4, v4, 0x1

    .line 965
    if-ltz v2, :cond_10

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    goto :goto_c

    :cond_10
    const/4 v0, 0x0

    goto :goto_c

    .line 972
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 978
    :cond_12
    iget v7, v0, Landroid/support/v4/view/ad;->position:I

    if-ne v5, v7, :cond_20

    iget-boolean v7, v0, Landroid/support/v4/view/ad;->bZ:Z

    if-nez v7, :cond_20

    .line 979
    iget-object v7, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 980
    iget-object v7, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v0, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    invoke-virtual {v7, p0, v5, v0}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 985
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_13

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    :goto_d
    move v12, v2

    move-object v2, v0

    move v0, v12

    .line 973
    :goto_e
    add-int/lit8 v5, v5, 0x1

    move v12, v0

    move-object v0, v2

    move v2, v12

    goto/16 :goto_7

    .line 985
    :cond_13
    const/4 v0, 0x0

    goto :goto_d

    .line 987
    :cond_14
    if-eqz v0, :cond_16

    iget v7, v0, Landroid/support/v4/view/ad;->position:I

    if-ne v5, v7, :cond_16

    .line 988
    iget v0, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v2, v0

    .line 989
    add-int/lit8 v3, v3, 0x1

    .line 990
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_15

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    :goto_f
    move v12, v2

    move-object v2, v0

    move v0, v12

    goto :goto_e

    :cond_15
    const/4 v0, 0x0

    goto :goto_f

    .line 992
    :cond_16
    invoke-virtual {p0, v5, v3}, Landroid/support/v4/view/ViewPager;->a(II)Landroid/support/v4/view/ad;

    move-result-object v0

    .line 993
    add-int/lit8 v3, v3, 0x1

    .line 994
    iget v0, v0, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v2, v0

    .line 995
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_17

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    :goto_10
    move v12, v2

    move-object v2, v0

    move v0, v12

    goto :goto_e

    :cond_17
    const/4 v0, 0x0

    goto :goto_10

    .line 1010
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 1016
    :cond_19
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_9

    .line 1021
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_a

    .line 1039
    :cond_1b
    if-eqz v2, :cond_1c

    .line 1040
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bV:Ljava/util/ArrayList;

    sget-object v1, Landroid/support/v4/view/ViewPager;->bW:Landroid/support/v4/view/aj;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1043
    :cond_1c
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1044
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1045
    if-eqz v0, :cond_1f

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->h(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v0

    .line 1046
    :goto_11
    if-eqz v0, :cond_1d

    iget v0, v0, Landroid/support/v4/view/ad;->position:I

    iget v1, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-eq v0, v1, :cond_0

    .line 1047
    :cond_1d
    const/4 v0, 0x0

    :goto_12
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1048
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1049
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v2

    .line 1050
    if-eqz v2, :cond_1e

    iget v2, v2, Landroid/support/v4/view/ad;->position:I

    iget v3, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v2, v3, :cond_1e

    .line 1051
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/View;->requestFocus(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1047
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 1045
    :cond_1f
    const/4 v0, 0x0

    goto :goto_11

    :cond_20
    move v12, v2

    move-object v2, v0

    move v0, v12

    goto/16 :goto_e

    :cond_21
    move-object v6, v0

    goto/16 :goto_3

    :cond_22
    move-object v0, v3

    goto/16 :goto_2

    :cond_23
    move-object v1, v0

    goto/16 :goto_0
.end method

.method j(I)Landroid/support/v4/view/ad;
    .locals 3

    .prologue
    .line 1277
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1278
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 1279
    iget v2, v0, Landroid/support/v4/view/ad;->position:I

    if-ne v2, p1, :cond_0

    .line 1283
    :goto_1
    return-object v0

    .line 1277
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1283
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method o()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 814
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Landroid/support/v4/view/ViewPager;->bs:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    if-ge v0, v3, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v3}, Landroid/support/v4/view/k;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    move v0, v1

    .line 816
    :goto_0
    iget v3, p0, Landroid/support/v4/view/ViewPager;->be:I

    move v4, v2

    move v5, v3

    move v6, v0

    move v3, v2

    .line 819
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 820
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 821
    iget-object v7, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v8, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    invoke-virtual {v7, v8}, Landroid/support/v4/view/k;->a(Ljava/lang/Object;)I

    move-result v7

    .line 823
    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v6

    .line 819
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 814
    goto :goto_0

    .line 827
    :cond_1
    const/4 v8, -0x2

    if-ne v7, v8, :cond_3

    .line 828
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 829
    add-int/lit8 v3, v3, -0x1

    .line 831
    if-nez v4, :cond_2

    .line 832
    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v4, p0}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;)V

    move v4, v1

    .line 836
    :cond_2
    iget-object v6, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget v7, v0, Landroid/support/v4/view/ad;->position:I

    iget-object v8, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    invoke-virtual {v6, p0, v7, v8}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 839
    iget v6, p0, Landroid/support/v4/view/ViewPager;->be:I

    iget v0, v0, Landroid/support/v4/view/ad;->position:I

    if-ne v6, v0, :cond_b

    .line 841
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    iget-object v5, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v5}, Landroid/support/v4/view/k;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    .line 842
    goto :goto_2

    .line 847
    :cond_3
    iget v8, v0, Landroid/support/v4/view/ad;->position:I

    if-eq v8, v7, :cond_a

    .line 848
    iget v6, v0, Landroid/support/v4/view/ad;->position:I

    iget v8, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v6, v8, :cond_4

    move v5, v7

    .line 853
    :cond_4
    iput v7, v0, Landroid/support/v4/view/ad;->position:I

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    .line 854
    goto :goto_2

    .line 858
    :cond_5
    if-eqz v4, :cond_6

    .line 859
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/k;->b(Landroid/view/ViewGroup;)V

    .line 862
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    sget-object v3, Landroid/support/v4/view/ViewPager;->ba:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 864
    if-eqz v6, :cond_9

    .line 866
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v4

    move v3, v2

    .line 867
    :goto_3
    if-ge v3, v4, :cond_8

    .line 868
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 869
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 870
    iget-boolean v6, v0, Landroid/support/v4/view/ae;->cb:Z

    if-nez v6, :cond_7

    .line 871
    const/4 v6, 0x0

    iput v6, v0, Landroid/support/v4/view/ae;->ca:F

    .line 867
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 875
    :cond_8
    invoke-virtual {p0, v5, v2, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 876
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 878
    :cond_9
    return-void

    :cond_a
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v6

    goto/16 :goto_2

    :cond_b
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    goto/16 :goto_2
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 1288
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1289
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bL:Z

    .line 1290
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bX:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 376
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 377
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    .prologue
    .line 2136
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2139
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->bj:I

    if-lez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->bk:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v1, :cond_2

    .line 2140
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v6

    .line 2141
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v7

    .line 2143
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v1, v1

    int-to-float v2, v7

    div-float v8, v1, v2

    .line 2144
    const/4 v5, 0x0

    .line 2145
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ad;

    .line 2146
    iget v4, v1, Landroid/support/v4/view/ad;->offset:F

    .line 2147
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 2148
    iget v3, v1, Landroid/support/v4/view/ad;->position:I

    .line 2149
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v10, v9, -0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ad;

    iget v10, v2, Landroid/support/v4/view/ad;->position:I

    move v2, v5

    move v5, v3

    .line 2150
    :goto_0
    if-ge v5, v10, :cond_2

    .line 2151
    :goto_1
    iget v3, v1, Landroid/support/v4/view/ad;->position:I

    if-le v5, v3, :cond_0

    if-ge v2, v9, :cond_0

    .line 2152
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ad;

    goto :goto_1

    .line 2156
    :cond_0
    iget v3, v1, Landroid/support/v4/view/ad;->position:I

    if-ne v5, v3, :cond_3

    .line 2157
    iget v3, v1, Landroid/support/v4/view/ad;->offset:F

    iget v4, v1, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v3, v4

    int-to-float v4, v7

    mul-float/2addr v3, v4

    .line 2158
    iget v4, v1, Landroid/support/v4/view/ad;->offset:F

    iget v11, v1, Landroid/support/v4/view/ad;->ca:F

    add-float/2addr v4, v11

    add-float/2addr v4, v8

    .line 2165
    :goto_2
    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v11, v11

    add-float/2addr v11, v3

    int-to-float v12, v6

    cmpl-float v11, v11, v12

    if-lez v11, :cond_1

    .line 2166
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->bk:Landroid/graphics/drawable/Drawable;

    float-to-int v12, v3

    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v4/view/ViewPager;->bl:I

    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v4/view/ViewPager;->bj:I

    int-to-float v14, v14

    add-float/2addr v14, v3

    const/high16 v15, 0x3f000000    # 0.5f

    add-float/2addr v14, v15

    float-to-int v14, v14

    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v4/view/ViewPager;->bm:I

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2168
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/view/ViewPager;->bk:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2171
    :cond_1
    add-int v11, v6, v7

    int-to-float v11, v11

    cmpl-float v3, v3, v11

    if-lez v3, :cond_4

    .line 2176
    :cond_2
    return-void

    .line 2160
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v3, v5}, Landroid/support/v4/view/k;->h(I)F

    move-result v11

    .line 2161
    add-float v3, v4, v11

    int-to-float v12, v7

    mul-float/2addr v3, v12

    .line 2162
    add-float/2addr v11, v8

    add-float/2addr v4, v11

    goto :goto_2

    .line 2150
    :cond_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v3, -0x1

    const/4 v12, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1721
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1724
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-ne v0, v6, :cond_2

    .line 1727
    :cond_0
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    .line 1728
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->bu:Z

    .line 1729
    iput v3, p0, Landroid/support/v4/view/ViewPager;->bB:I

    .line 1730
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 1731
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1732
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1853
    :cond_1
    :goto_0
    return v2

    .line 1739
    :cond_2
    if-eqz v0, :cond_4

    .line 1740
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    if-eqz v1, :cond_3

    move v2, v6

    .line 1742
    goto :goto_0

    .line 1744
    :cond_3
    iget-boolean v1, p0, Landroid/support/v4/view/ViewPager;->bu:Z

    if-nez v1, :cond_1

    .line 1750
    :cond_4
    sparse-switch v0, :sswitch_data_0

    .line 1844
    :cond_5
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_6

    .line 1845
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1847
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1853
    iget-boolean v2, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    goto :goto_0

    .line 1761
    :sswitch_0
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    .line 1762
    if-eq v0, v3, :cond_5

    .line 1767
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1768
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 1769
    iget v1, p0, Landroid/support/v4/view/ViewPager;->bz:F

    sub-float v8, v7, v1

    .line 1770
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 1771
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->d(Landroid/view/MotionEvent;I)F

    move-result v10

    .line 1772
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bA:F

    sub-float v0, v10, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1775
    cmpl-float v0, v8, v12

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    invoke-direct {p0, v0, v8}, Landroid/support/v4/view/ViewPager;->a(FF)Z

    move-result v0

    if-nez v0, :cond_7

    float-to-int v3, v8

    float-to-int v4, v7

    float-to-int v5, v10

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1778
    iput v7, p0, Landroid/support/v4/view/ViewPager;->bz:F

    iput v7, p0, Landroid/support/v4/view/ViewPager;->by:F

    .line 1779
    iput v10, p0, Landroid/support/v4/view/ViewPager;->bA:F

    .line 1780
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->bu:Z

    goto :goto_0

    .line 1783
    :cond_7
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_a

    cmpl-float v0, v9, v11

    if-lez v0, :cond_a

    .line 1785
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    .line 1786
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 1787
    cmpl-float v0, v8, v12

    if-lez v0, :cond_9

    iget v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_2
    iput v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 1789
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 1800
    :cond_8
    :goto_3
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    if-eqz v0, :cond_5

    .line 1802
    invoke-direct {p0, v7}, Landroid/support/v4/view/ViewPager;->b(F)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1803
    invoke-static {p0}, Landroid/support/v4/view/q;->f(Landroid/view/View;)V

    goto :goto_1

    .line 1787
    :cond_9
    iget v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_2

    .line 1791
    :cond_a
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_8

    .line 1797
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->bu:Z

    goto :goto_3

    .line 1814
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 1815
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bA:F

    .line 1816
    invoke-static {p1, v2}, Landroid/support/v4/view/f;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    .line 1817
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->bu:Z

    .line 1819
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 1820
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bY:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bF:I

    if-le v0, v1, :cond_b

    .line 1823
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1824
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->br:Z

    .line 1825
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->p()V

    .line 1826
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    .line 1827
    invoke-direct {p0, v6}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    goto/16 :goto_1

    .line 1829
    :cond_b
    invoke-direct {p0, v2}, Landroid/support/v4/view/ViewPager;->e(Z)V

    .line 1830
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    goto/16 :goto_1

    .line 1840
    :sswitch_2
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 1750
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 17

    .prologue
    .line 1426
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/view/ViewPager;->mInLayout:Z

    .line 1427
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->p()V

    .line 1428
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/view/ViewPager;->mInLayout:Z

    .line 1430
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v9

    .line 1431
    sub-int v10, p4, p2

    .line 1432
    sub-int v11, p5, p3

    .line 1433
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v6

    .line 1434
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v2

    .line 1435
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v5

    .line 1436
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v3

    .line 1437
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v12

    .line 1439
    const/4 v4, 0x0

    .line 1443
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v9, :cond_0

    .line 1444
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 1445
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v7, 0x8

    if-eq v1, v7, :cond_4

    .line 1446
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ae;

    .line 1449
    iget-boolean v7, v1, Landroid/support/v4/view/ae;->cb:Z

    if-eqz v7, :cond_4

    .line 1450
    iget v7, v1, Landroid/support/v4/view/ae;->gravity:I

    and-int/lit8 v7, v7, 0x7

    .line 1451
    iget v1, v1, Landroid/support/v4/view/ae;->gravity:I

    and-int/lit8 v14, v1, 0x70

    .line 1452
    packed-switch v7, :pswitch_data_0

    :pswitch_0
    move v7, v6

    .line 1469
    :goto_1
    sparse-switch v14, :sswitch_data_0

    move v1, v2

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 1486
    :goto_2
    add-int/2addr v7, v12

    .line 1487
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v14, v7

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v1

    invoke-virtual {v13, v7, v1, v14, v15}, Landroid/view/View;->layout(IIII)V

    .line 1490
    add-int/lit8 v1, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v5

    move v5, v6

    .line 1443
    :goto_3
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v6, v5

    move v5, v2

    move v2, v4

    move v4, v1

    goto :goto_0

    .line 1458
    :pswitch_1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    move v7, v6

    move v6, v1

    .line 1459
    goto :goto_1

    .line 1461
    :pswitch_2
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v10, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 1463
    goto :goto_1

    .line 1465
    :pswitch_3
    sub-int v1, v10, v5

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v1, v7

    .line 1466
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    move v7, v1

    goto :goto_1

    .line 1475
    :sswitch_0
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v2

    move/from16 v16, v2

    move v2, v3

    move v3, v1

    move/from16 v1, v16

    .line 1476
    goto :goto_2

    .line 1478
    :sswitch_1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v11, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 1480
    goto :goto_2

    .line 1482
    :sswitch_2
    sub-int v1, v11, v3

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    sub-int/2addr v1, v14

    .line 1483
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    add-int/2addr v3, v14

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    goto :goto_2

    .line 1496
    :cond_0
    const/4 v1, 0x0

    move v7, v1

    :goto_4
    if-ge v7, v9, :cond_3

    .line 1497
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1498
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v12, 0x8

    if-eq v1, v12, :cond_2

    .line 1499
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ae;

    .line 1501
    iget-boolean v12, v1, Landroid/support/v4/view/ae;->cb:Z

    if-nez v12, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 1502
    int-to-float v13, v10

    iget v12, v12, Landroid/support/v4/view/ad;->offset:F

    mul-float/2addr v12, v13

    float-to-int v12, v12

    .line 1503
    add-int/2addr v12, v6

    .line 1505
    iget-boolean v13, v1, Landroid/support/v4/view/ae;->cc:Z

    if-eqz v13, :cond_1

    .line 1508
    const/4 v13, 0x0

    iput-boolean v13, v1, Landroid/support/v4/view/ae;->cc:Z

    .line 1509
    sub-int v13, v10, v6

    sub-int/2addr v13, v5

    int-to-float v13, v13

    iget v1, v1, Landroid/support/v4/view/ae;->ca:F

    mul-float/2addr v1, v13

    float-to-int v1, v1

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v1, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1512
    sub-int v13, v11, v2

    sub-int/2addr v13, v3

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 1515
    invoke-virtual {v8, v1, v13}, Landroid/view/View;->measure(II)V

    .line 1520
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v12

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v2

    invoke-virtual {v8, v12, v2, v1, v13}, Landroid/view/View;->layout(IIII)V

    .line 1496
    :cond_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_4

    .line 1526
    :cond_3
    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v4/view/ViewPager;->bl:I

    .line 1527
    sub-int v1, v11, v3

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/view/ViewPager;->bm:I

    .line 1528
    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v4/view/ViewPager;->bO:I

    .line 1529
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/view/ViewPager;->bL:Z

    .line 1530
    return-void

    :cond_4
    move v1, v4

    move v4, v2

    move v2, v5

    move v5, v6

    goto/16 :goto_3

    .line 1452
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 1469
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    .line 1299
    const/4 v0, 0x0

    invoke-static {v0, p1}, Landroid/support/v4/view/ViewPager;->getDefaultSize(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, p2}, Landroid/support/v4/view/ViewPager;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/ViewPager;->setMeasuredDimension(II)V

    .line 1302
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredWidth()I

    move-result v0

    .line 1303
    div-int/lit8 v1, v0, 0xa

    .line 1304
    iget v2, p0, Landroid/support/v4/view/ViewPager;->bv:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->bw:I

    .line 1307
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 1308
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getPaddingBottom()I

    move-result v1

    sub-int v5, v0, v1

    .line 1315
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v9

    .line 1316
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_8

    .line 1317
    invoke-virtual {p0, v8}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1318
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    .line 1319
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 1320
    if-eqz v0, :cond_3

    iget-boolean v1, v0, Landroid/support/v4/view/ae;->cb:Z

    if-eqz v1, :cond_3

    .line 1321
    iget v1, v0, Landroid/support/v4/view/ae;->gravity:I

    and-int/lit8 v6, v1, 0x7

    .line 1322
    iget v1, v0, Landroid/support/v4/view/ae;->gravity:I

    and-int/lit8 v4, v1, 0x70

    .line 1323
    const/high16 v2, -0x80000000

    .line 1324
    const/high16 v1, -0x80000000

    .line 1325
    const/16 v7, 0x30

    if-eq v4, v7, :cond_0

    const/16 v7, 0x50

    if-ne v4, v7, :cond_4

    :cond_0
    const/4 v4, 0x1

    move v7, v4

    .line 1326
    :goto_1
    const/4 v4, 0x3

    if-eq v6, v4, :cond_1

    const/4 v4, 0x5

    if-ne v6, v4, :cond_5

    :cond_1
    const/4 v4, 0x1

    move v6, v4

    .line 1328
    :goto_2
    if-eqz v7, :cond_6

    .line 1329
    const/high16 v2, 0x40000000    # 2.0f

    .line 1336
    :cond_2
    :goto_3
    iget v4, v0, Landroid/support/v4/view/ae;->width:I

    const/4 v11, -0x2

    if-eq v4, v11, :cond_e

    .line 1337
    const/high16 v4, 0x40000000    # 2.0f

    .line 1338
    iget v2, v0, Landroid/support/v4/view/ae;->width:I

    const/4 v11, -0x1

    if-eq v2, v11, :cond_d

    .line 1339
    iget v2, v0, Landroid/support/v4/view/ae;->width:I

    .line 1342
    :goto_4
    iget v11, v0, Landroid/support/v4/view/ae;->height:I

    const/4 v12, -0x2

    if-eq v11, v12, :cond_c

    .line 1343
    const/high16 v1, 0x40000000    # 2.0f

    .line 1344
    iget v11, v0, Landroid/support/v4/view/ae;->height:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_c

    .line 1345
    iget v0, v0, Landroid/support/v4/view/ae;->height:I

    .line 1348
    :goto_5
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1349
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1350
    invoke-virtual {v10, v2, v0}, Landroid/view/View;->measure(II)V

    .line 1352
    if-eqz v7, :cond_7

    .line 1353
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v5, v0

    .line 1316
    :cond_3
    :goto_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 1325
    :cond_4
    const/4 v4, 0x0

    move v7, v4

    goto :goto_1

    .line 1326
    :cond_5
    const/4 v4, 0x0

    move v6, v4

    goto :goto_2

    .line 1330
    :cond_6
    if-eqz v6, :cond_2

    .line 1331
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_3

    .line 1354
    :cond_7
    if-eqz v6, :cond_3

    .line 1355
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    goto :goto_6

    .line 1361
    :cond_8
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bp:I

    .line 1362
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bq:I

    .line 1365
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->mInLayout:Z

    .line 1366
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->p()V

    .line 1367
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/ViewPager;->mInLayout:Z

    .line 1370
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    .line 1371
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v2, :cond_b

    .line 1372
    invoke-virtual {p0, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1373
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_a

    .line 1377
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ae;

    .line 1378
    if-eqz v0, :cond_9

    iget-boolean v5, v0, Landroid/support/v4/view/ae;->cb:Z

    if-nez v5, :cond_a

    .line 1379
    :cond_9
    int-to-float v5, v3

    iget v0, v0, Landroid/support/v4/view/ae;->ca:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1381
    iget v5, p0, Landroid/support/v4/view/ViewPager;->bq:I

    invoke-virtual {v4, v0, v5}, Landroid/view/View;->measure(II)V

    .line 1371
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1385
    :cond_b
    return-void

    :cond_c
    move v0, v5

    goto :goto_5

    :cond_d
    move v2, v3

    goto/16 :goto_4

    :cond_e
    move v4, v2

    move v2, v3

    goto/16 :goto_4
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 2577
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v0

    .line 2578
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_0

    move v1, v2

    move v3, v4

    .line 2587
    :goto_0
    if-eq v3, v0, :cond_2

    .line 2588
    invoke-virtual {p0, v3}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2589
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 2590
    invoke-virtual {p0, v5}, Landroid/support/v4/view/ViewPager;->g(Landroid/view/View;)Landroid/support/v4/view/ad;

    move-result-object v6

    .line 2591
    if-eqz v6, :cond_1

    iget v6, v6, Landroid/support/v4/view/ad;->position:I

    iget v7, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-ne v6, v7, :cond_1

    .line 2592
    invoke-virtual {v5, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2598
    :goto_1
    return v2

    .line 2583
    :cond_0
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v1

    .line 2585
    goto :goto_0

    .line 2587
    :cond_1
    add-int/2addr v3, v1

    goto :goto_0

    :cond_2
    move v2, v4

    .line 2598
    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 1211
    instance-of v0, p1, Landroid/support/v4/view/ViewPager$SavedState;

    if-nez v0, :cond_0

    .line 1212
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1227
    :goto_0
    return-void

    .line 1216
    :cond_0
    check-cast p1, Landroid/support/v4/view/ViewPager$SavedState;

    .line 1217
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1219
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v0, :cond_1

    .line 1220
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v1, p1, Landroid/support/v4/view/ViewPager$SavedState;->cf:Landroid/os/Parcelable;

    iget-object v2, p1, Landroid/support/v4/view/ViewPager$SavedState;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/k;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 1221
    iget v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->position:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    goto :goto_0

    .line 1223
    :cond_1
    iget v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->position:I

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bf:I

    .line 1224
    iget-object v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->cf:Landroid/os/Parcelable;

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->bg:Landroid/os/Parcelable;

    .line 1225
    iget-object v0, p1, Landroid/support/v4/view/ViewPager$SavedState;->loader:Ljava/lang/ClassLoader;

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->bh:Ljava/lang/ClassLoader;

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1200
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1201
    new-instance v1, Landroid/support/v4/view/ViewPager$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/view/ViewPager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1202
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    iput v0, v1, Landroid/support/v4/view/ViewPager$SavedState;->position:I

    .line 1203
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v0, :cond_0

    .line 1204
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0}, Landroid/support/v4/view/k;->m()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/view/ViewPager$SavedState;->cf:Landroid/os/Parcelable;

    .line 1206
    :cond_0
    return-object v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 1389
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1392
    if-eq p1, p3, :cond_0

    .line 1393
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bj:I

    iget v1, p0, Landroid/support/v4/view/ViewPager;->bj:I

    invoke-direct {p0, p1, p3, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    .line 1395
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1858
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bI:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 1972
    :goto_0
    return v0

    .line 1865
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 1868
    goto :goto_0

    .line 1871
    :cond_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0}, Landroid/support/v4/view/k;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    .line 1873
    goto :goto_0

    .line 1876
    :cond_3
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_4

    .line 1877
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1879
    :cond_4
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1881
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1884
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 1969
    :cond_5
    :goto_1
    :pswitch_0
    if-eqz v2, :cond_6

    .line 1970
    invoke-static {p0}, Landroid/support/v4/view/q;->f(Landroid/view/View;)V

    :cond_6
    move v0, v1

    .line 1972
    goto :goto_0

    .line 1886
    :pswitch_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1887
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->br:Z

    .line 1888
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->p()V

    .line 1889
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    .line 1890
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 1893
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 1894
    invoke-static {p1, v2}, Landroid/support/v4/view/f;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    goto :goto_1

    .line 1898
    :pswitch_2
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    if-nez v0, :cond_7

    .line 1899
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    invoke-static {p1, v0}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1900
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1901
    iget v4, p0, Landroid/support/v4/view/ViewPager;->bz:F

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 1902
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1903
    iget v5, p0, Landroid/support/v4/view/ViewPager;->bA:F

    sub-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1905
    iget v5, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_7

    cmpl-float v0, v4, v0

    if-lez v0, :cond_7

    .line 1907
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    .line 1908
    iget v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    sub-float v0, v3, v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    iget v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    iget v3, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    :goto_2
    iput v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 1910
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollState(I)V

    .line 1911
    invoke-direct {p0, v1}, Landroid/support/v4/view/ViewPager;->setScrollingCacheEnabled(Z)V

    .line 1915
    :cond_7
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    if-eqz v0, :cond_5

    .line 1917
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    invoke-static {p1, v0}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1919
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1920
    invoke-direct {p0, v0}, Landroid/support/v4/view/ViewPager;->b(F)Z

    move-result v0

    or-int/2addr v2, v0

    .line 1921
    goto :goto_1

    .line 1908
    :cond_8
    iget v0, p0, Landroid/support/v4/view/ViewPager;->by:F

    iget v3, p0, Landroid/support/v4/view/ViewPager;->bx:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    goto :goto_2

    .line 1924
    :pswitch_3
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    if-eqz v0, :cond_5

    .line 1925
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1926
    const/16 v2, 0x3e8

    iget v3, p0, Landroid/support/v4/view/ViewPager;->bD:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1927
    iget v2, p0, Landroid/support/v4/view/ViewPager;->bB:I

    invoke-static {v0, v2}, Landroid/support/v4/view/l;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    .line 1929
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->br:Z

    .line 1930
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v2

    .line 1931
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v3

    .line 1932
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->q()Landroid/support/v4/view/ad;

    move-result-object v4

    .line 1933
    iget v5, v4, Landroid/support/v4/view/ad;->position:I

    .line 1934
    int-to-float v3, v3

    int-to-float v2, v2

    div-float v2, v3, v2

    iget v3, v4, Landroid/support/v4/view/ad;->offset:F

    sub-float/2addr v2, v3

    iget v3, v4, Landroid/support/v4/view/ad;->ca:F

    div-float/2addr v2, v3

    .line 1935
    iget v3, p0, Landroid/support/v4/view/ViewPager;->bB:I

    invoke-static {p1, v3}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 1937
    invoke-static {p1, v3}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1938
    iget v4, p0, Landroid/support/v4/view/ViewPager;->by:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 1939
    invoke-direct {p0, v5, v2, v0, v3}, Landroid/support/v4/view/ViewPager;->a(IFII)I

    move-result v2

    .line 1941
    invoke-virtual {p0, v2, v1, v1, v0}, Landroid/support/v4/view/ViewPager;->a(IZZI)V

    .line 1943
    iput v6, p0, Landroid/support/v4/view/ViewPager;->bB:I

    .line 1944
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->r()V

    .line 1945
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-virtual {v0}, Landroid/support/v4/d/a;->u()Z

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-virtual {v2}, Landroid/support/v4/d/a;->u()Z

    move-result v2

    or-int/2addr v2, v0

    .line 1946
    goto/16 :goto_1

    .line 1949
    :pswitch_4
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bt:Z

    if-eqz v0, :cond_5

    .line 1950
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-direct {p0, v0, v1, v2, v2}, Landroid/support/v4/view/ViewPager;->a(IZIZ)V

    .line 1951
    iput v6, p0, Landroid/support/v4/view/ViewPager;->bB:I

    .line 1952
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->r()V

    .line 1953
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bJ:Landroid/support/v4/d/a;

    invoke-virtual {v0}, Landroid/support/v4/d/a;->u()Z

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bK:Landroid/support/v4/d/a;

    invoke-virtual {v2}, Landroid/support/v4/d/a;->u()Z

    move-result v2

    or-int/2addr v2, v0

    goto/16 :goto_1

    .line 1957
    :pswitch_5
    invoke-static {p1}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1958
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1959
    iput v3, p0, Landroid/support/v4/view/ViewPager;->bz:F

    .line 1960
    invoke-static {p1, v0}, Landroid/support/v4/view/f;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    goto/16 :goto_1

    .line 1964
    :pswitch_6
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;->b(Landroid/view/MotionEvent;)V

    .line 1965
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bB:I

    invoke-static {p1, v0}, Landroid/support/v4/view/f;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/f;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/ViewPager;->bz:F

    goto/16 :goto_1

    .line 1884
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method p()V
    .locals 1

    .prologue
    .line 881
    iget v0, p0, Landroid/support/v4/view/ViewPager;->be:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->i(I)V

    .line 882
    return-void
.end method

.method s()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2490
    iget v1, p0, Landroid/support/v4/view/ViewPager;->be:I

    if-lez v1, :cond_0

    .line 2491
    iget v1, p0, Landroid/support/v4/view/ViewPager;->be:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->b(IZ)V

    .line 2494
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapter(Landroid/support/v4/view/k;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 403
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bi:Landroid/support/v4/view/ai;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/k;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 405
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;)V

    move v1, v2

    .line 406
    :goto_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 407
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ad;

    .line 408
    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget v4, v0, Landroid/support/v4/view/ad;->position:I

    iget-object v0, v0, Landroid/support/v4/view/ad;->object:Ljava/lang/Object;

    invoke-virtual {v3, p0, v4, v0}, Landroid/support/v4/view/k;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 406
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 410
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/k;->b(Landroid/view/ViewGroup;)V

    .line 411
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 412
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager;->n()V

    .line 413
    iput v2, p0, Landroid/support/v4/view/ViewPager;->be:I

    .line 414
    invoke-virtual {p0, v2, v2}, Landroid/support/v4/view/ViewPager;->scrollTo(II)V

    .line 417
    :cond_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    .line 418
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    .line 420
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v1, :cond_3

    .line 421
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bi:Landroid/support/v4/view/ai;

    if-nez v1, :cond_2

    .line 422
    new-instance v1, Landroid/support/v4/view/ai;

    invoke-direct {v1, p0, v5}, Landroid/support/v4/view/ai;-><init>(Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/ViewPager$1;)V

    iput-object v1, p0, Landroid/support/v4/view/ViewPager;->bi:Landroid/support/v4/view/ai;

    .line 424
    :cond_2
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->bi:Landroid/support/v4/view/ai;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/k;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 425
    iput-boolean v2, p0, Landroid/support/v4/view/ViewPager;->br:Z

    .line 426
    iput-boolean v6, p0, Landroid/support/v4/view/ViewPager;->bL:Z

    .line 427
    iget v1, p0, Landroid/support/v4/view/ViewPager;->bf:I

    if-ltz v1, :cond_5

    .line 428
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    iget-object v3, p0, Landroid/support/v4/view/ViewPager;->bg:Landroid/os/Parcelable;

    iget-object v4, p0, Landroid/support/v4/view/ViewPager;->bh:Ljava/lang/ClassLoader;

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/view/k;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 429
    iget v1, p0, Landroid/support/v4/view/ViewPager;->bf:I

    invoke-virtual {p0, v1, v2, v6}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 430
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v4/view/ViewPager;->bf:I

    .line 431
    iput-object v5, p0, Landroid/support/v4/view/ViewPager;->bg:Landroid/os/Parcelable;

    .line 432
    iput-object v5, p0, Landroid/support/v4/view/ViewPager;->bh:Ljava/lang/ClassLoader;

    .line 438
    :cond_3
    :goto_1
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bR:Landroid/support/v4/view/af;

    if-eqz v1, :cond_4

    if-eq v0, p1, :cond_4

    .line 439
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bR:Landroid/support/v4/view/af;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/af;->a(Landroid/support/v4/view/k;Landroid/support/v4/view/k;)V

    .line 441
    :cond_4
    return-void

    .line 434
    :cond_5
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->p()V

    goto :goto_1
.end method

.method setChildrenDrawingOrderEnabledCompat(Z)V
    .locals 5

    .prologue
    .line 594
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bT:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 596
    :try_start_0
    const-class v0, Landroid/view/ViewGroup;

    const-string v1, "setChildrenDrawingOrderEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/ViewPager;->bT:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bT:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 607
    :goto_1
    return-void

    .line 598
    :catch_0
    move-exception v0

    .line 599
    const-string v1, "ViewPager"

    const-string v2, "Can\'t find setChildrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 604
    :catch_1
    move-exception v0

    .line 605
    const-string v1, "ViewPager"

    const-string v2, "Error changing children drawing order"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setCurrentItem(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 475
    iput-boolean v1, p0, Landroid/support/v4/view/ViewPager;->br:Z

    .line 476
    iget-boolean v0, p0, Landroid/support/v4/view/ViewPager;->bL:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0, v1}, Landroid/support/v4/view/ViewPager;->a(IZZ)V

    .line 477
    return-void

    :cond_0
    move v0, v1

    .line 476
    goto :goto_0
.end method

.method public setOffscreenPageLimit(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 657
    if-ge p1, v0, :cond_0

    .line 658
    const-string v1, "ViewPager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requested offscreen page limit "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " too small; defaulting to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v0

    .line 662
    :cond_0
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bs:I

    if-eq p1, v0, :cond_1

    .line 663
    iput p1, p0, Landroid/support/v4/view/ViewPager;->bs:I

    .line 664
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->p()V

    .line 666
    :cond_1
    return-void
.end method

.method setOnAdapterChangeListener(Landroid/support/v4/view/af;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->bR:Landroid/support/v4/view/af;

    .line 465
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ag;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->bP:Landroid/support/v4/view/ag;

    .line 564
    return-void
.end method

.method public setPageMargin(I)V
    .locals 2

    .prologue
    .line 677
    iget v0, p0, Landroid/support/v4/view/ViewPager;->bj:I

    .line 678
    iput p1, p0, Landroid/support/v4/view/ViewPager;->bj:I

    .line 680
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v1

    .line 681
    invoke-direct {p0, v1, v1, p1, v0}, Landroid/support/v4/view/ViewPager;->a(IIII)V

    .line 683
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->requestLayout()V

    .line 684
    return-void
.end method

.method public setPageMarginDrawable(I)V
    .locals 1

    .prologue
    .line 713
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 714
    return-void
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 701
    iput-object p1, p0, Landroid/support/v4/view/ViewPager;->bk:Landroid/graphics/drawable/Drawable;

    .line 702
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->refreshDrawableState()V

    .line 703
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setWillNotDraw(Z)V

    .line 704
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->invalidate()V

    .line 705
    return-void

    .line 703
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method t()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2498
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v4/view/ViewPager;->be:I

    iget-object v2, p0, Landroid/support/v4/view/ViewPager;->bd:Landroid/support/v4/view/k;

    invoke-virtual {v2}, Landroid/support/v4/view/k;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 2499
    iget v1, p0, Landroid/support/v4/view/ViewPager;->be:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/ViewPager;->b(IZ)V

    .line 2502
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 718
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->bk:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
