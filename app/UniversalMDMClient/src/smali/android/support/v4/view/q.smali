.class public Landroid/support/v4/view/q;
.super Ljava/lang/Object;
.source "ViewCompat.java"


# static fields
.field static final aY:Landroid/support/v4/view/x;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 323
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 324
    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 325
    new-instance v0, Landroid/support/v4/view/w;

    invoke-direct {v0}, Landroid/support/v4/view/w;-><init>()V

    sput-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    .line 337
    :goto_0
    return-void

    .line 326
    :cond_0
    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 327
    new-instance v0, Landroid/support/v4/view/v;

    invoke-direct {v0}, Landroid/support/v4/view/v;-><init>()V

    sput-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    goto :goto_0

    .line 328
    :cond_1
    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    .line 329
    new-instance v0, Landroid/support/v4/view/u;

    invoke-direct {v0}, Landroid/support/v4/view/u;-><init>()V

    sput-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    goto :goto_0

    .line 330
    :cond_2
    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 331
    new-instance v0, Landroid/support/v4/view/t;

    invoke-direct {v0}, Landroid/support/v4/view/t;-><init>()V

    sput-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    goto :goto_0

    .line 332
    :cond_3
    const/16 v1, 0x9

    if-lt v0, v1, :cond_4

    .line 333
    new-instance v0, Landroid/support/v4/view/s;

    invoke-direct {v0}, Landroid/support/v4/view/s;-><init>()V

    sput-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    goto :goto_0

    .line 335
    :cond_4
    new-instance v0, Landroid/support/v4/view/r;

    invoke-direct {v0}, Landroid/support/v4/view/r;-><init>()V

    sput-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 713
    sget-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/x;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 714
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 576
    sget-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/x;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 577
    return-void
.end method

.method public static b(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 347
    sget-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/x;->b(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 371
    sget-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    invoke-interface {v0, p0}, Landroid/support/v4/view/x;->e(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static f(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 544
    sget-object v0, Landroid/support/v4/view/q;->aY:Landroid/support/v4/view/x;

    invoke-interface {v0, p0}, Landroid/support/v4/view/x;->f(Landroid/view/View;)V

    .line 545
    return-void
.end method
