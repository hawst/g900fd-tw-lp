.class public Landroid/support/v4/app/v;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# instance fields
.field aA:Z

.field aB:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/s;",
            ">;"
        }
    .end annotation
.end field

.field aC:Landroid/app/Notification;

.field ao:Ljava/lang/CharSequence;

.field ap:Landroid/app/PendingIntent;

.field aq:Landroid/app/PendingIntent;

.field ar:Landroid/widget/RemoteViews;

.field as:Landroid/graphics/Bitmap;

.field at:Ljava/lang/CharSequence;

.field au:I

.field av:Z

.field aw:Landroid/support/v4/app/ac;

.field ax:Ljava/lang/CharSequence;

.field ay:I

.field az:I

.field mContentText:Ljava/lang/CharSequence;

.field mContext:Landroid/content/Context;

.field mPriority:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/v;->aB:Ljava/util/ArrayList;

    .line 186
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    .line 200
    iput-object p1, p0, Landroid/support/v4/app/v;->mContext:Landroid/content/Context;

    .line 203
    iget-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 204
    iget-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/v;->mPriority:I

    .line 206
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 498
    if-eqz p2, :cond_0

    .line 499
    iget-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 503
    :goto_0
    return-void

    .line 501
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method


# virtual methods
.method public a(IIZ)Landroid/support/v4/app/v;
    .locals 0

    .prologue
    .line 310
    iput p1, p0, Landroid/support/v4/app/v;->ay:I

    .line 311
    iput p2, p0, Landroid/support/v4/app/v;->az:I

    .line 312
    iput-boolean p3, p0, Landroid/support/v4/app/v;->aA:Z

    .line 313
    return-object p0
.end method

.method public a(Landroid/app/PendingIntent;)Landroid/support/v4/app/v;
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Landroid/support/v4/app/v;->ap:Landroid/app/PendingIntent;

    .line 334
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Landroid/support/v4/app/v;->ao:Ljava/lang/CharSequence;

    .line 266
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Landroid/support/v4/app/v;->mContentText:Ljava/lang/CharSequence;

    .line 274
    return-object p0
.end method

.method public build()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 562
    invoke-static {}, Landroid/support/v4/app/r;->j()Landroid/support/v4/app/x;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/support/v4/app/x;->a(Landroid/support/v4/app/v;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/CharSequence;)Landroid/support/v4/app/v;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 374
    return-object p0
.end method

.method public c(Z)Landroid/support/v4/app/v;
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/v;->a(IZ)V

    .line 456
    return-object p0
.end method

.method public d(Z)Landroid/support/v4/app/v;
    .locals 1

    .prologue
    .line 475
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/v;->a(IZ)V

    .line 476
    return-object p0
.end method

.method public g(I)Landroid/support/v4/app/v;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 242
    return-object p0
.end method
