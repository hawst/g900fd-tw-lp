.class Landroid/support/v4/app/ab;
.super Ljava/lang/Object;
.source "NotificationCompat.java"

# interfaces
.implements Landroid/support/v4/app/x;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/v;)Landroid/app/Notification;
    .locals 18

    .prologue
    .line 117
    new-instance v1, Landroid/support/v4/app/af;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/v;->aC:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/v;->ao:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/v;->mContentText:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/v;->at:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/v;->ar:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v8, v0, Landroid/support/v4/app/v;->au:I

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/support/v4/app/v;->ap:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/v;->aq:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/app/v;->as:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, Landroid/support/v4/app/v;->ay:I

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/v;->az:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Landroid/support/v4/app/v;->aA:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/v;->av:Z

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/v;->mPriority:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/v;->ax:Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    invoke-direct/range {v1 .. v17}, Landroid/support/v4/app/af;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;)V

    .line 122
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aB:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/s;

    .line 123
    iget v4, v2, Landroid/support/v4/app/s;->icon:I

    iget-object v5, v2, Landroid/support/v4/app/s;->title:Ljava/lang/CharSequence;

    iget-object v2, v2, Landroid/support/v4/app/s;->actionIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v4/app/af;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 125
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    if-eqz v2, :cond_1

    .line 126
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    instance-of v2, v2, Landroid/support/v4/app/u;

    if-eqz v2, :cond_2

    .line 127
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    check-cast v2, Landroid/support/v4/app/u;

    .line 128
    iget-object v3, v2, Landroid/support/v4/app/u;->aE:Ljava/lang/CharSequence;

    iget-boolean v4, v2, Landroid/support/v4/app/u;->aG:Z

    iget-object v5, v2, Landroid/support/v4/app/u;->aF:Ljava/lang/CharSequence;

    iget-object v2, v2, Landroid/support/v4/app/u;->an:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 146
    :cond_1
    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/app/af;->build()Landroid/app/Notification;

    move-result-object v1

    return-object v1

    .line 132
    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    instance-of v2, v2, Landroid/support/v4/app/w;

    if-eqz v2, :cond_3

    .line 133
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    check-cast v2, Landroid/support/v4/app/w;

    .line 134
    iget-object v3, v2, Landroid/support/v4/app/w;->aE:Ljava/lang/CharSequence;

    iget-boolean v4, v2, Landroid/support/v4/app/w;->aG:Z

    iget-object v5, v2, Landroid/support/v4/app/w;->aF:Ljava/lang/CharSequence;

    iget-object v2, v2, Landroid/support/v4/app/w;->aD:Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 138
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    instance-of v2, v2, Landroid/support/v4/app/t;

    if-eqz v2, :cond_1

    .line 139
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/v;->aw:Landroid/support/v4/app/ac;

    check-cast v2, Landroid/support/v4/app/t;

    .line 140
    iget-object v3, v2, Landroid/support/v4/app/t;->aE:Ljava/lang/CharSequence;

    iget-boolean v4, v2, Landroid/support/v4/app/t;->aG:Z

    iget-object v5, v2, Landroid/support/v4/app/t;->aF:Ljava/lang/CharSequence;

    iget-object v2, v2, Landroid/support/v4/app/t;->am:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
