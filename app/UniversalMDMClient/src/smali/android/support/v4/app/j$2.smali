.class Landroid/support/v4/app/j$2;
.super Ljava/lang/Object;
.source "FragmentManager.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v4/app/j;->a(Landroid/support/v4/app/Fragment;IIIZ)V
.end annotation


# instance fields
.field final synthetic S:Landroid/support/v4/app/j;

.field final synthetic T:Landroid/support/v4/app/Fragment;


# direct methods
.method constructor <init>(Landroid/support/v4/app/j;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 991
    iput-object p1, p0, Landroid/support/v4/app/j$2;->S:Landroid/support/v4/app/j;

    iput-object p2, p0, Landroid/support/v4/app/j$2;->T:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 994
    iget-object v0, p0, Landroid/support/v4/app/j$2;->T:Landroid/support/v4/app/Fragment;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Landroid/support/v4/app/j$2;->T:Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/app/Fragment;->o:Landroid/view/View;

    .line 996
    iget-object v0, p0, Landroid/support/v4/app/j$2;->S:Landroid/support/v4/app/j;

    iget-object v1, p0, Landroid/support/v4/app/j$2;->T:Landroid/support/v4/app/Fragment;

    iget-object v2, p0, Landroid/support/v4/app/j$2;->T:Landroid/support/v4/app/Fragment;

    iget v2, v2, Landroid/support/v4/app/Fragment;->mStateAfterAnimating:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/j;->a(Landroid/support/v4/app/Fragment;IIIZ)V

    .line 999
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1002
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1005
    return-void
.end method
