.class public Landroid/support/v4/d/a;
.super Ljava/lang/Object;
.source "EdgeEffectCompat.java"


# static fields
.field private static final ch:Landroid/support/v4/d/d;


# instance fields
.field private cg:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 38
    new-instance v0, Landroid/support/v4/d/c;

    invoke-direct {v0}, Landroid/support/v4/d/c;-><init>()V

    sput-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    new-instance v0, Landroid/support/v4/d/b;

    invoke-direct {v0}, Landroid/support/v4/d/b;-><init>()V

    sput-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    goto :goto_0
.end method


# virtual methods
.method public c(F)Z
    .locals 2

    .prologue
    .line 177
    sget-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    iget-object v1, p0, Landroid/support/v4/d/a;->cg:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/d/d;->a(Ljava/lang/Object;F)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)Z
    .locals 2

    .prologue
    .line 218
    sget-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    iget-object v1, p0, Landroid/support/v4/d/a;->cg:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/d/d;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v0

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 162
    sget-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    iget-object v1, p0, Landroid/support/v4/d/a;->cg:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/d/d;->c(Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public isFinished()Z
    .locals 2

    .prologue
    .line 154
    sget-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    iget-object v1, p0, Landroid/support/v4/d/a;->cg:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/d/d;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setSize(II)V
    .locals 2

    .prologue
    .line 143
    sget-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    iget-object v1, p0, Landroid/support/v4/d/a;->cg:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/d/d;->a(Ljava/lang/Object;II)V

    .line 144
    return-void
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 189
    sget-object v0, Landroid/support/v4/d/a;->ch:Landroid/support/v4/d/d;

    iget-object v1, p0, Landroid/support/v4/d/a;->cg:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/d/d;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
