.class public abstract Lorg/htmlcleaner/c;
.super Ljava/lang/Object;
.source "BaseTokenImpl.java"

# interfaces
.implements Lorg/htmlcleaner/b;


# instance fields
.field private Cv:I

.field private row:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public bx(I)V
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lorg/htmlcleaner/c;->row:I

    .line 27
    return-void
.end method

.method public by(I)V
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lorg/htmlcleaner/c;->Cv:I

    .line 33
    return-void
.end method

.method public getRow()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lorg/htmlcleaner/c;->row:I

    return v0
.end method

.method public kM()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lorg/htmlcleaner/c;->Cv:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(line="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlcleaner/c;->getRow()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", col="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/htmlcleaner/c;->kM()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
