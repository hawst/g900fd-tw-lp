.class public Lorg/htmlcleaner/m;
.super Ljava/lang/Object;
.source "HtmlCleaner.java"


# instance fields
.field private DT:Lorg/htmlcleaner/f;

.field private DU:Lorg/htmlcleaner/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 303
    invoke-direct {p0, v0, v0}, Lorg/htmlcleaner/m;-><init>(Lorg/htmlcleaner/t;Lorg/htmlcleaner/f;)V

    .line 304
    return-void
.end method

.method public constructor <init>(Lorg/htmlcleaner/t;Lorg/htmlcleaner/f;)V
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    if-nez p2, :cond_0

    new-instance p2, Lorg/htmlcleaner/f;

    invoke-direct {p2}, Lorg/htmlcleaner/f;-><init>()V

    :cond_0
    iput-object p2, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    .line 329
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    if-nez p1, :cond_1

    sget-object p1, Lorg/htmlcleaner/j;->DE:Lorg/htmlcleaner/j;

    :cond_1
    invoke-virtual {v0, p1}, Lorg/htmlcleaner/f;->a(Lorg/htmlcleaner/t;)V

    .line 330
    return-void
.end method

.method private C(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 633
    instance-of v0, p1, Lorg/htmlcleaner/y;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/htmlcleaner/y;

    invoke-virtual {p1}, Lorg/htmlcleaner/y;->mp()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Lorg/htmlcleaner/q;Ljava/lang/Object;Lorg/htmlcleaner/e;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List;",
            "Lorg/htmlcleaner/q;",
            "Ljava/lang/Object;",
            "Lorg/htmlcleaner/e;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1033
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1034
    invoke-static {p2}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v7

    .line 1037
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    move v3, v4

    move-object v2, v5

    .line 1040
    :goto_0
    if-nez p3, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    if-eqz p3, :cond_8

    if-eq v1, p3, :cond_8

    .line 1041
    :cond_1
    invoke-direct {p0, v1}, Lorg/htmlcleaner/m;->C(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 1042
    check-cast v0, Lorg/htmlcleaner/y;

    .line 1043
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1044
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->mo()Ljava/util/List;

    move-result-object v8

    .line 1045
    if-eqz v8, :cond_2

    .line 1046
    invoke-direct {p0, p4}, Lorg/htmlcleaner/m;->c(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/o;

    .line 1047
    invoke-interface {v8, v4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v9

    invoke-virtual {p0, v8, v9, p4}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Ljava/util/ListIterator;Lorg/htmlcleaner/e;)V

    .line 1048
    invoke-direct {p0, v8, p4}, Lorg/htmlcleaner/m;->c(Ljava/util/List;Lorg/htmlcleaner/e;)V

    .line 1049
    invoke-virtual {v0, v5}, Lorg/htmlcleaner/y;->d(Ljava/util/List;)V

    .line 1050
    invoke-direct {p0, p4}, Lorg/htmlcleaner/m;->d(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/o;

    .line 1053
    :cond_2
    invoke-direct {p0, v0}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;)Lorg/htmlcleaner/y;

    move-result-object v0

    .line 1054
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, p4}, Lorg/htmlcleaner/m;->b(Ljava/lang/String;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/x;

    move-result-object v9

    .line 1055
    invoke-direct {p0, v9, v0, p4}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/x;Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V

    .line 1056
    if-eqz v2, :cond_3

    .line 1057
    invoke-virtual {v2, v8}, Lorg/htmlcleaner/y;->c(Ljava/util/List;)V

    .line 1058
    invoke-virtual {v2, v0}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    .line 1059
    invoke-interface {v7, v5}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 1069
    :goto_1
    invoke-direct {p0, p4}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v2

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lorg/htmlcleaner/p;->d(Lorg/htmlcleaner/p;Ljava/lang/String;)V

    .line 1080
    :goto_2
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1081
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v0

    goto :goto_0

    .line 1061
    :cond_3
    if-eqz v8, :cond_4

    .line 1062
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1063
    invoke-interface {v7, v8}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 1065
    :cond_4
    invoke-interface {v7, v0}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 1072
    :cond_5
    if-eqz v2, :cond_6

    .line 1073
    invoke-interface {v7, v5}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 1074
    if-eqz v1, :cond_6

    .line 1075
    invoke-virtual {v2, v1}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    :cond_6
    move-object v0, v2

    goto :goto_2

    .line 1083
    :cond_7
    const/4 v2, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_0

    .line 1086
    :cond_8
    return-object v6
.end method

.method private a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;
    .locals 1

    .prologue
    .line 1242
    iget-object v0, p1, Lorg/htmlcleaner/e;->CF:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/o;

    invoke-virtual {v0}, Lorg/htmlcleaner/o;->lq()Lorg/htmlcleaner/p;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/htmlcleaner/y;)Lorg/htmlcleaner/y;
    .locals 0

    .prologue
    .line 604
    invoke-virtual {p1}, Lorg/htmlcleaner/y;->mq()V

    .line 605
    return-object p1
.end method

.method private a(Ljava/util/List;Ljava/lang/Object;Lorg/htmlcleaner/e;)V
    .locals 2

    .prologue
    .line 620
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/p;->b(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;

    move-result-object v0

    .line 621
    if-eqz v0, :cond_1

    invoke-static {v0}, Lorg/htmlcleaner/q;->c(Lorg/htmlcleaner/q;)Lorg/htmlcleaner/x;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lorg/htmlcleaner/q;->c(Lorg/htmlcleaner/q;)Lorg/htmlcleaner/x;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/x;->me()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 625
    :cond_1
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/p;->c(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_0

    .line 627
    invoke-static {v0}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 628
    invoke-virtual {v0, p2}, Lorg/htmlcleaner/y;->F(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Ljava/util/ListIterator;Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ListIterator",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;",
            "Lorg/htmlcleaner/y;",
            "Lorg/htmlcleaner/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 958
    .line 959
    invoke-virtual {p2}, Lorg/htmlcleaner/y;->mt()Lorg/htmlcleaner/y;

    move-result-object v0

    .line 960
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->O(Z)V

    .line 961
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->removeAttribute(Ljava/lang/String;)V

    .line 962
    invoke-interface {p1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 963
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-virtual {p2}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/ListIterator;->previousIndex()I

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;Ljava/lang/String;I)V

    .line 964
    return-void
.end method

.method private a(Lorg/htmlcleaner/e;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/htmlcleaner/e;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 480
    iget-object v0, p1, Lorg/htmlcleaner/e;->CG:Lorg/htmlcleaner/y;

    iput-object v0, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    .line 496
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->kV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p1, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->mm()Ljava/util/List;

    move-result-object v0

    .line 498
    new-instance v1, Lorg/htmlcleaner/y;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/htmlcleaner/y;-><init>(Ljava/lang/String;)V

    iput-object v1, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    .line 499
    if-eqz v0, :cond_0

    .line 500
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 502
    iget-object v2, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    invoke-virtual {v2, v1}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    goto :goto_0

    .line 506
    :cond_0
    iget-object v0, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v1

    .line 512
    iget-object v0, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    const-string v2, "xmlns"

    invoke-virtual {v0, v2}, Lorg/htmlcleaner/y;->hasAttribute(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 513
    iget-object v0, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    const-string v2, ""

    iget-object v3, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    const-string v4, "xmlns"

    invoke-virtual {v3, v4}, Lorg/htmlcleaner/y;->cv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/htmlcleaner/y;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_1
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->kZ()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    .line 517
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 518
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 519
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 520
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xmlns:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 524
    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "xml"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 525
    iget-object v4, p1, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    invoke-virtual {v4, v3, v0}, Lorg/htmlcleaner/y;->H(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 529
    :cond_3
    return-void
.end method

.method private a(Lorg/htmlcleaner/x;Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V
    .locals 1

    .prologue
    .line 1108
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 1109
    invoke-virtual {p1}, Lorg/htmlcleaner/x;->mi()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/htmlcleaner/x;->mj()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p3, Lorg/htmlcleaner/e;->CA:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p3, Lorg/htmlcleaner/e;->CB:Z

    if-nez v0, :cond_1

    .line 1110
    :cond_0
    iget-object v0, p3, Lorg/htmlcleaner/e;->CC:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1113
    :cond_1
    return-void
.end method

.method private a(Lorg/htmlcleaner/y;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/htmlcleaner/y;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 538
    if-eqz p2, :cond_1

    .line 539
    invoke-virtual {p1}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v2

    .line 540
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 541
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 542
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 543
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 544
    invoke-virtual {p1, v1, v0}, Lorg/htmlcleaner/y;->H(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 548
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Lorg/htmlcleaner/e;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 645
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->kZ()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 652
    :goto_0
    return v0

    .line 646
    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    goto :goto_0

    .line 647
    :cond_1
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    .line 648
    :cond_2
    iget-object v0, p2, Lorg/htmlcleaner/e;->CO:Ljava/util/Stack;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lorg/htmlcleaner/e;->CO:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_0

    .line 649
    :cond_4
    iget-object v0, p2, Lorg/htmlcleaner/e;->CO:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 650
    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    .line 651
    :cond_5
    const-string v3, "http://www.w3.org/1999/xhtml"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "http://w3.org/1999/xhtml"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v2

    .line 652
    goto :goto_0
.end method

.method private a(Ljava/util/List;Lorg/htmlcleaner/e;)Z
    .locals 4

    .prologue
    .line 457
    const/4 v0, 0x0

    .line 458
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 459
    instance-of v3, v0, Lorg/htmlcleaner/y;

    if-eqz v3, :cond_2

    iget-object v3, p2, Lorg/htmlcleaner/e;->CM:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 460
    check-cast v0, Lorg/htmlcleaner/y;

    .line 461
    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    const/4 v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 466
    goto :goto_0

    .line 463
    :cond_0
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 464
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->mm()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Lorg/htmlcleaner/e;)Z

    move-result v0

    or-int/2addr v1, v0

    move v0, v1

    goto :goto_1

    .line 468
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lorg/htmlcleaner/b;Lorg/htmlcleaner/e;)Z
    .locals 2

    .prologue
    .line 609
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/p;->b(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;

    move-result-object v0

    .line 610
    if-eqz v0, :cond_0

    .line 611
    invoke-static {v0}, Lorg/htmlcleaner/q;->c(Lorg/htmlcleaner/q;)Lorg/htmlcleaner/x;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 612
    invoke-static {v0}, Lorg/htmlcleaner/q;->c(Lorg/htmlcleaner/q;)Lorg/htmlcleaner/x;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/htmlcleaner/x;->b(Lorg/htmlcleaner/b;)Z

    move-result v0

    .line 616
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lorg/htmlcleaner/x;Lorg/htmlcleaner/e;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 556
    if-eqz p1, :cond_0

    .line 557
    invoke-virtual {p1}, Lorg/htmlcleaner/x;->mb()Ljava/lang/String;

    move-result-object v1

    .line 558
    if-nez v1, :cond_1

    .line 561
    :cond_0
    :goto_0
    return v0

    .line 558
    :cond_1
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0, v1}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Lorg/htmlcleaner/y;Ljava/util/ListIterator;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/htmlcleaner/y;",
            "Ljava/util/ListIterator",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v3, 0x0

    .line 932
    move v2, v3

    move v1, v3

    .line 934
    :goto_0
    invoke-interface {p1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    if-ge v1, v5, :cond_0

    .line 935
    invoke-interface {p1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 936
    add-int/lit8 v4, v1, 0x1

    .line 937
    instance-of v1, v0, Lorg/htmlcleaner/y;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lorg/htmlcleaner/y;

    invoke-virtual {v1}, Lorg/htmlcleaner/y;->mu()Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast v0, Lorg/htmlcleaner/y;

    invoke-static {v0, p0}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Lorg/htmlcleaner/y;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 938
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v4

    .line 942
    goto :goto_0

    :cond_0
    move v4, v1

    :cond_1
    move v0, v3

    .line 943
    :goto_1
    if-ge v0, v4, :cond_2

    .line 944
    invoke-interface {p1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 943
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 946
    :cond_2
    if-ne v2, v5, :cond_3

    const/4 v3, 0x1

    :cond_3
    return v3
.end method

.method private static a(Lorg/htmlcleaner/y;Lorg/htmlcleaner/y;)Z
    .locals 2

    .prologue
    .line 953
    iget-object v0, p0, Lorg/htmlcleaner/y;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/htmlcleaner/y;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;
    .locals 1

    .prologue
    .line 1246
    iget-object v0, p1, Lorg/htmlcleaner/e;->CF:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/o;

    invoke-virtual {v0}, Lorg/htmlcleaner/o;->lr()Lorg/htmlcleaner/n;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/x;
    .locals 2

    .prologue
    .line 1142
    const/4 v0, 0x0

    .line 1143
    invoke-direct {p0, p1, p2}, Lorg/htmlcleaner/m;->a(Ljava/lang/String;Lorg/htmlcleaner/e;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/htmlcleaner/m;->kP()Lorg/htmlcleaner/t;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/htmlcleaner/t;->cb(Ljava/lang/String;)Lorg/htmlcleaner/x;

    move-result-object v0

    .line 1144
    :cond_0
    return-object v0
.end method

.method private b(Ljava/util/List;Lorg/htmlcleaner/e;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 977
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 978
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 979
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 981
    if-eqz v1, :cond_0

    .line 987
    instance-of v0, v1, Lorg/htmlcleaner/y;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 988
    check-cast v0, Lorg/htmlcleaner/y;

    .line 989
    invoke-virtual {p0}, Lorg/htmlcleaner/m;->kP()Lorg/htmlcleaner/t;

    move-result-object v5

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/htmlcleaner/t;->cb(Ljava/lang/String;)Lorg/htmlcleaner/x;

    move-result-object v5

    .line 990
    invoke-direct {p0, v5, v0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/x;Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V

    move v0, v2

    .line 997
    :goto_1
    if-eqz v0, :cond_0

    .line 998
    iget-object v0, p2, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    goto :goto_0

    .line 992
    :cond_1
    instance-of v0, v1, Lorg/htmlcleaner/i;

    if-eqz v0, :cond_8

    .line 993
    const-string v0, ""

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_1

    .line 1003
    :cond_3
    iget-object v0, p2, Lorg/htmlcleaner/e;->CC:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1004
    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1005
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 1008
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->lQ()Lorg/htmlcleaner/y;

    move-result-object v1

    .line 1010
    :goto_3
    if-eqz v1, :cond_7

    .line 1011
    iget-object v5, p2, Lorg/htmlcleaner/e;->CC:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    move v1, v3

    .line 1018
    :goto_4
    if-eqz v1, :cond_4

    .line 1019
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->lR()Z

    .line 1020
    iget-object v1, p2, Lorg/htmlcleaner/e;->CJ:Lorg/htmlcleaner/y;

    invoke-virtual {v1, v0}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    goto :goto_2

    .line 1015
    :cond_5
    invoke-virtual {v1}, Lorg/htmlcleaner/y;->lQ()Lorg/htmlcleaner/y;

    move-result-object v1

    goto :goto_3

    .line 1023
    :cond_6
    return-void

    :cond_7
    move v1, v2

    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_1
.end method

.method private b(Lorg/htmlcleaner/x;Lorg/htmlcleaner/e;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 570
    if-eqz p1, :cond_3

    .line 571
    invoke-virtual {p1}, Lorg/htmlcleaner/x;->ma()Ljava/lang/String;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_3

    .line 573
    invoke-virtual {p1}, Lorg/htmlcleaner/x;->mb()Ljava/lang/String;

    move-result-object v1

    .line 574
    const/4 v0, -0x1

    .line 575
    if-eqz v1, :cond_4

    .line 576
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v4

    invoke-static {v4, v1}, Lorg/htmlcleaner/p;->b(Lorg/htmlcleaner/p;Ljava/lang/String;)Lorg/htmlcleaner/q;

    move-result-object v1

    .line 577
    if-eqz v1, :cond_4

    .line 578
    invoke-static {v1}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    move v1, v0

    .line 583
    :goto_0
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v4

    invoke-static {v4}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0, v4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    .line 584
    :cond_0
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585
    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    .line 586
    invoke-static {v0}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/htmlcleaner/x;->cs(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 587
    invoke-static {v0}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    if-gt v0, v1, :cond_1

    move v0, v2

    .line 595
    :goto_1
    return v0

    :cond_1
    move v0, v3

    .line 587
    goto :goto_1

    :cond_2
    move v0, v2

    .line 591
    goto :goto_1

    :cond_3
    move v0, v3

    .line 595
    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private b(Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1148
    iget-object v0, p2, Lorg/htmlcleaner/e;->CL:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 1149
    iget-object v0, p2, Lorg/htmlcleaner/e;->CL:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/a/a;

    .line 1150
    invoke-interface {v0, p1}, Lorg/htmlcleaner/a/a;->c(Lorg/htmlcleaner/y;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1151
    invoke-virtual {p0, p1, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V

    .line 1152
    iget-object v2, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v2, v0, p1}, Lorg/htmlcleaner/f;->a(Lorg/htmlcleaner/a/a;Lorg/htmlcleaner/y;)V

    move v0, v1

    .line 1170
    :goto_0
    return v0

    .line 1158
    :cond_1
    iget-object v0, p2, Lorg/htmlcleaner/e;->CN:Ljava/util/Set;

    if-eqz v0, :cond_5

    iget-object v0, p2, Lorg/htmlcleaner/e;->CN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1159
    iget-object v0, p2, Lorg/htmlcleaner/e;->CN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/a/a;

    .line 1160
    invoke-interface {v0, p1}, Lorg/htmlcleaner/a/a;->c(Lorg/htmlcleaner/y;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 1161
    goto :goto_0

    .line 1164
    :cond_3
    invoke-virtual {p1}, Lorg/htmlcleaner/y;->mr()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1165
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    sget-object v2, Lorg/htmlcleaner/audit/ErrorType;->Fz:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v0, v1, p1, v2}, Lorg/htmlcleaner/f;->c(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    .line 1167
    :cond_4
    invoke-virtual {p0, p1, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V

    move v0, v1

    .line 1168
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1170
    goto :goto_0
.end method

.method private c(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/o;
    .locals 2

    .prologue
    .line 1251
    iget-object v0, p1, Lorg/htmlcleaner/e;->CF:Ljava/util/Stack;

    new-instance v1, Lorg/htmlcleaner/o;

    invoke-direct {v1, p0}, Lorg/htmlcleaner/o;-><init>(Lorg/htmlcleaner/m;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/o;

    return-object v0
.end method

.method private c(Ljava/util/List;Lorg/htmlcleaner/e;)V
    .locals 6

    .prologue
    .line 1093
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/p;->e(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;

    move-result-object v1

    .line 1094
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    .line 1095
    iget-object v3, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    const/4 v4, 0x1

    invoke-static {v0}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    sget-object v5, Lorg/htmlcleaner/audit/ErrorType;->FB:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v3, v4, v0, v5}, Lorg/htmlcleaner/f;->a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto :goto_0

    .line 1097
    :cond_0
    if-eqz v1, :cond_1

    .line 1098
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0, p2}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Lorg/htmlcleaner/q;Ljava/lang/Object;Lorg/htmlcleaner/e;)Ljava/util/List;

    .line 1100
    :cond_1
    return-void
.end method

.method private ce(Ljava/lang/String;)Lorg/htmlcleaner/y;
    .locals 1

    .prologue
    .line 599
    new-instance v0, Lorg/htmlcleaner/y;

    invoke-direct {v0, p1}, Lorg/htmlcleaner/y;-><init>(Ljava/lang/String;)V

    .line 600
    return-object v0
.end method

.method private d(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/o;
    .locals 1

    .prologue
    .line 1254
    iget-object v0, p1, Lorg/htmlcleaner/e;->CF:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/o;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/io/Reader;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/y;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 405
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->c(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/o;

    .line 406
    iput-boolean v0, p2, Lorg/htmlcleaner/e;->CA:Z

    .line 407
    iput-boolean v0, p2, Lorg/htmlcleaner/e;->CB:Z

    .line 408
    iget-object v0, p2, Lorg/htmlcleaner/e;->CC:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 409
    iget-object v0, p2, Lorg/htmlcleaner/e;->CE:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 410
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->le()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p2, Lorg/htmlcleaner/e;->CL:Ljava/util/Set;

    .line 411
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->lf()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p2, Lorg/htmlcleaner/e;->CN:Ljava/util/Set;

    .line 412
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->li()Lorg/htmlcleaner/g;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlcleaner/m;->DU:Lorg/htmlcleaner/g;

    .line 413
    iget-object v0, p2, Lorg/htmlcleaner/e;->CM:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 415
    const-string v0, "html"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/m;->ce(Ljava/lang/String;)Lorg/htmlcleaner/y;

    move-result-object v0

    iput-object v0, p2, Lorg/htmlcleaner/e;->CG:Lorg/htmlcleaner/y;

    .line 416
    const-string v0, "body"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/m;->ce(Ljava/lang/String;)Lorg/htmlcleaner/y;

    move-result-object v0

    iput-object v0, p2, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    .line 417
    const-string v0, "head"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/m;->ce(Ljava/lang/String;)Lorg/htmlcleaner/y;

    move-result-object v0

    iput-object v0, p2, Lorg/htmlcleaner/e;->CJ:Lorg/htmlcleaner/y;

    .line 418
    const/4 v0, 0x0

    iput-object v0, p2, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    .line 419
    iget-object v0, p2, Lorg/htmlcleaner/e;->CG:Lorg/htmlcleaner/y;

    iget-object v1, p2, Lorg/htmlcleaner/e;->CJ:Lorg/htmlcleaner/y;

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    .line 420
    iget-object v0, p2, Lorg/htmlcleaner/e;->CG:Lorg/htmlcleaner/y;

    iget-object v1, p2, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    .line 422
    new-instance v1, Lorg/htmlcleaner/s;

    invoke-direct {v1, p0, p1, p2}, Lorg/htmlcleaner/s;-><init>(Lorg/htmlcleaner/m;Ljava/io/Reader;Lorg/htmlcleaner/e;)V

    .line 424
    invoke-virtual {v1}, Lorg/htmlcleaner/s;->start()V

    .line 426
    invoke-virtual {v1}, Lorg/htmlcleaner/s;->lv()Ljava/util/List;

    move-result-object v0

    .line 427
    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/m;->c(Ljava/util/List;Lorg/htmlcleaner/e;)V

    .line 429
    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/m;->b(Ljava/util/List;Lorg/htmlcleaner/e;)V

    .line 430
    invoke-virtual {v1}, Lorg/htmlcleaner/s;->lw()Ljava/util/Set;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;Ljava/util/Set;)V

    .line 435
    :cond_0
    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Lorg/htmlcleaner/e;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 440
    iget-object v0, p2, Lorg/htmlcleaner/e;->CM:Ljava/util/Set;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lorg/htmlcleaner/e;->CM:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 441
    iget-object v0, p2, Lorg/htmlcleaner/e;->CM:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 442
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 443
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 444
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->lQ()Lorg/htmlcleaner/y;

    move-result-object v3

    .line 445
    if-eqz v3, :cond_1

    .line 446
    invoke-virtual {v3, v0}, Lorg/htmlcleaner/y;->E(Ljava/lang/Object;)Z

    goto :goto_0

    .line 451
    :cond_2
    iget-object v0, p2, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    invoke-virtual {v1}, Lorg/htmlcleaner/s;->lP()Lorg/htmlcleaner/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->a(Lorg/htmlcleaner/k;)V

    .line 452
    invoke-direct {p0, p2}, Lorg/htmlcleaner/m;->d(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/o;

    .line 453
    iget-object v0, p2, Lorg/htmlcleaner/e;->CK:Lorg/htmlcleaner/y;

    return-object v0
.end method

.method a(Ljava/util/List;Ljava/util/ListIterator;Lorg/htmlcleaner/e;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List;",
            "Ljava/util/ListIterator",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;",
            "Lorg/htmlcleaner/e;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 664
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 665
    invoke-interface {p2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 667
    instance-of v1, v0, Lorg/htmlcleaner/l;

    if-eqz v1, :cond_c

    .line 668
    check-cast v0, Lorg/htmlcleaner/l;

    .line 669
    invoke-virtual {v0}, Lorg/htmlcleaner/l;->getName()Ljava/lang/String;

    move-result-object v2

    .line 670
    invoke-direct {p0, v2, p3}, Lorg/htmlcleaner/m;->b(Ljava/lang/String;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/x;

    move-result-object v6

    .line 672
    if-nez v6, :cond_1

    iget-object v1, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->kQ()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v2, p3}, Lorg/htmlcleaner/m;->a(Ljava/lang/String;Lorg/htmlcleaner/e;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lorg/htmlcleaner/x;->mc()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->kS()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 674
    :cond_2
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 675
    :cond_3
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lorg/htmlcleaner/x;->mf()Z

    move-result v1

    if-nez v1, :cond_4

    .line 677
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 680
    :cond_4
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v1

    invoke-static {v1, v2}, Lorg/htmlcleaner/p;->b(Lorg/htmlcleaner/p;Ljava/lang/String;)Lorg/htmlcleaner/q;

    move-result-object v7

    .line 682
    if-eqz v7, :cond_0

    .line 685
    invoke-direct {p0, p1, v7, v0, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Lorg/htmlcleaner/q;Ljava/lang/Object;Lorg/htmlcleaner/e;)Ljava/util/List;

    move-result-object v8

    .line 690
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 691
    const-string v1, "xmlns"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/y;->hasAttribute(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 692
    iget-object v0, p3, Lorg/htmlcleaner/e;->CO:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 695
    :cond_5
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 696
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_7

    .line 697
    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 699
    if-lez v1, :cond_6

    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lorg/htmlcleaner/x;->cu(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 703
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->mt()Lorg/htmlcleaner/y;

    move-result-object v0

    .line 704
    invoke-virtual {v0, v5}, Lorg/htmlcleaner/y;->O(Z)V

    .line 705
    invoke-interface {p2, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 706
    invoke-interface {p2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 696
    :cond_6
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 709
    :cond_7
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/n;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 710
    :goto_2
    invoke-static {v7}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/htmlcleaner/n;->lp()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 713
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/n;->lo()Lorg/htmlcleaner/q;

    goto :goto_2

    .line 716
    :cond_8
    :goto_3
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/n;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/n;->ln()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v7}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v1

    invoke-virtual {v1}, Lorg/htmlcleaner/n;->lp()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 719
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/n;->a(Lorg/htmlcleaner/n;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    invoke-static {v0}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 721
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/n;->lo()Lorg/htmlcleaner/q;

    move-result-object v0

    invoke-static {v0}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v1

    .line 722
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 724
    instance-of v6, v0, Lorg/htmlcleaner/y;

    if-eqz v6, :cond_9

    .line 726
    check-cast v0, Lorg/htmlcleaner/y;

    invoke-direct {p0, p2, v0, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/ListIterator;Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V

    goto :goto_3

    .line 727
    :cond_9
    instance-of v6, v0, Ljava/util/List;

    if-eqz v6, :cond_8

    .line 734
    check-cast v0, Ljava/util/List;

    .line 736
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 737
    invoke-interface {p2, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 738
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Ljava/util/ListIterator;Lorg/htmlcleaner/e;)V

    goto :goto_4

    .line 741
    :cond_a
    invoke-interface {p1, v1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 753
    :cond_b
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/n;->lo()Lorg/htmlcleaner/q;

    goto/16 :goto_3

    .line 759
    :cond_c
    invoke-direct {p0, v0}, Lorg/htmlcleaner/m;->C(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    move-object v1, v0

    .line 760
    check-cast v1, Lorg/htmlcleaner/y;

    .line 761
    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v7

    .line 762
    invoke-direct {p0, v7, p3}, Lorg/htmlcleaner/m;->b(Ljava/lang/String;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/x;

    move-result-object v8

    .line 764
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v2

    invoke-static {v2}, Lorg/htmlcleaner/p;->d(Lorg/htmlcleaner/p;)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object v6, v3

    .line 765
    :goto_5
    if-nez v6, :cond_f

    move-object v2, v3

    .line 769
    :goto_6
    iget-object v9, p3, Lorg/htmlcleaner/e;->CE:Ljava/util/Set;

    invoke-interface {v9, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 776
    const-string v9, "xmlns"

    invoke-virtual {v1, v9}, Lorg/htmlcleaner/y;->hasAttribute(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 778
    const-string v9, "xmlns"

    invoke-virtual {v1, v9}, Lorg/htmlcleaner/y;->cv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 784
    const-string v10, "html"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    const-string v10, "http://www.w3.org/TR/REC-html40"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 785
    const-string v9, "xmlns"

    invoke-virtual {v1, v9}, Lorg/htmlcleaner/y;->removeAttribute(Ljava/lang/String;)V

    .line 795
    :cond_d
    :goto_7
    invoke-direct {p0, v7, p3}, Lorg/htmlcleaner/m;->a(Ljava/lang/String;Lorg/htmlcleaner/e;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 796
    invoke-virtual {v1, v5}, Lorg/htmlcleaner/y;->Q(Z)V

    .line 802
    :goto_8
    const-string v9, "html"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 803
    iget-object v0, p3, Lorg/htmlcleaner/e;->CG:Lorg/htmlcleaner/y;

    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Ljava/util/Map;)V

    .line 804
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 764
    :cond_e
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v2

    invoke-static {v2}, Lorg/htmlcleaner/p;->b(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;

    move-result-object v2

    move-object v6, v2

    goto :goto_5

    .line 765
    :cond_f
    invoke-static {v6}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p3}, Lorg/htmlcleaner/m;->b(Ljava/lang/String;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/x;

    move-result-object v2

    goto :goto_6

    .line 787
    :cond_10
    iget-object v10, p3, Lorg/htmlcleaner/e;->CO:Ljava/util/Stack;

    invoke-virtual {v10, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    const-string v10, ""

    invoke-virtual {v1, v10, v9}, Lorg/htmlcleaner/y;->I(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 798
    :cond_11
    invoke-virtual {v1, v4}, Lorg/htmlcleaner/y;->Q(Z)V

    goto :goto_8

    .line 806
    :cond_12
    const-string v9, "body"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 807
    iput-boolean v5, p3, Lorg/htmlcleaner/e;->CB:Z

    .line 808
    iget-object v0, p3, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Ljava/util/Map;)V

    .line 809
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 811
    :cond_13
    const-string v9, "head"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 812
    iput-boolean v5, p3, Lorg/htmlcleaner/e;->CA:Z

    .line 813
    iget-object v0, p3, Lorg/htmlcleaner/e;->CJ:Lorg/htmlcleaner/y;

    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Ljava/util/Map;)V

    .line 814
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 817
    :cond_14
    if-nez v8, :cond_15

    iget-object v9, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v9}, Lorg/htmlcleaner/f;->kQ()Z

    move-result v9

    if-eqz v9, :cond_15

    invoke-direct {p0, v7, p3}, Lorg/htmlcleaner/m;->a(Ljava/lang/String;Lorg/htmlcleaner/e;)Z

    move-result v9

    if-nez v9, :cond_15

    .line 818
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 819
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    sget-object v2, Lorg/htmlcleaner/audit/ErrorType;->FF:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v0, v5, v1, v2}, Lorg/htmlcleaner/f;->b(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto/16 :goto_0

    .line 820
    :cond_15
    if-eqz v8, :cond_16

    invoke-virtual {v8}, Lorg/htmlcleaner/x;->mc()Z

    move-result v9

    if-eqz v9, :cond_16

    iget-object v9, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v9}, Lorg/htmlcleaner/f;->kS()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 821
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 822
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    sget-object v2, Lorg/htmlcleaner/audit/ErrorType;->FD:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v0, v5, v1, v2}, Lorg/htmlcleaner/f;->b(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto/16 :goto_0

    .line 824
    :cond_16
    if-nez v8, :cond_17

    if-eqz v2, :cond_17

    invoke-virtual {v2}, Lorg/htmlcleaner/x;->mk()Z

    move-result v9

    if-nez v9, :cond_17

    .line 825
    invoke-direct {p0, p1, v6, v1, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Lorg/htmlcleaner/q;Ljava/lang/Object;Lorg/htmlcleaner/e;)Ljava/util/List;

    .line 826
    invoke-interface {p2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    goto/16 :goto_0

    .line 827
    :cond_17
    if-eqz v8, :cond_18

    invoke-virtual {v8}, Lorg/htmlcleaner/x;->mh()Z

    move-result v9

    if-eqz v9, :cond_18

    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v9

    invoke-virtual {v8}, Lorg/htmlcleaner/x;->lZ()Ljava/util/Set;

    move-result-object v10

    invoke-static {v9, v10}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;Ljava/util/Set;)Z

    move-result v9

    if-eqz v9, :cond_18

    .line 828
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 830
    :cond_18
    if-eqz v8, :cond_19

    invoke-virtual {v8}, Lorg/htmlcleaner/x;->md()Z

    move-result v9

    if-eqz v9, :cond_19

    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v9

    invoke-static {v9, v7}, Lorg/htmlcleaner/p;->c(Lorg/htmlcleaner/p;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_19

    .line 831
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 832
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    sget-object v2, Lorg/htmlcleaner/audit/ErrorType;->FC:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v0, v5, v1, v2}, Lorg/htmlcleaner/f;->a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto/16 :goto_0

    .line 834
    :cond_19
    invoke-direct {p0, v8, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/x;Lorg/htmlcleaner/e;)Z

    move-result v9

    if-nez v9, :cond_1a

    .line 835
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 836
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    sget-object v2, Lorg/htmlcleaner/audit/ErrorType;->Fy:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v0, v5, v1, v2}, Lorg/htmlcleaner/f;->a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto/16 :goto_0

    .line 838
    :cond_1a
    invoke-direct {p0, v8, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/x;Lorg/htmlcleaner/e;)Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 839
    invoke-virtual {v8}, Lorg/htmlcleaner/x;->ma()Ljava/lang/String;

    move-result-object v0

    .line 840
    invoke-direct {p0, v0}, Lorg/htmlcleaner/m;->ce(Ljava/lang/String;)Lorg/htmlcleaner/y;

    move-result-object v0

    .line 841
    invoke-virtual {v0, v5}, Lorg/htmlcleaner/y;->O(Z)V

    .line 842
    invoke-interface {p2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 843
    invoke-interface {p2, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 844
    invoke-interface {p2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 845
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    sget-object v2, Lorg/htmlcleaner/audit/ErrorType;->FA:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v0, v5, v1, v2}, Lorg/htmlcleaner/f;->a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto/16 :goto_0

    .line 847
    :cond_1b
    if-eqz v8, :cond_21

    if-eqz v6, :cond_21

    invoke-virtual {v8, v2}, Lorg/htmlcleaner/x;->a(Lorg/htmlcleaner/x;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 850
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->b(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/n;

    move-result-object v0

    new-instance v2, Lorg/htmlcleaner/q;

    invoke-interface {p2}, Ljava/util/ListIterator;->previousIndex()I

    move-result v7

    invoke-virtual {v8}, Lorg/htmlcleaner/x;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, p0, v7, v9}, Lorg/htmlcleaner/q;-><init>(Lorg/htmlcleaner/m;ILjava/lang/String;)V

    invoke-virtual {v0, v6, v2}, Lorg/htmlcleaner/n;->a(Lorg/htmlcleaner/q;Lorg/htmlcleaner/q;)V

    .line 851
    const-string v0, "id"

    invoke-virtual {v1, v0}, Lorg/htmlcleaner/y;->hasAttribute(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    move v2, v4

    .line 852
    :goto_9
    iget-object v7, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-static {v6}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    sget-object v9, Lorg/htmlcleaner/audit/ErrorType;->FE:Lorg/htmlcleaner/audit/ErrorType;

    invoke-virtual {v7, v2, v0, v9}, Lorg/htmlcleaner/f;->a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    .line 853
    invoke-direct {p0, p1, v6, v1, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Lorg/htmlcleaner/q;Ljava/lang/Object;Lorg/htmlcleaner/e;)Ljava/util/List;

    move-result-object v0

    .line 854
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 857
    invoke-virtual {v8}, Lorg/htmlcleaner/x;->mg()Z

    move-result v2

    if-eqz v2, :cond_20

    if-lez v1, :cond_20

    .line 860
    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    .line 861
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 862
    :goto_a
    invoke-interface {v1}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 863
    invoke-interface {v1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 864
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lorg/htmlcleaner/x;->ct(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 865
    invoke-interface {v2, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_a

    :cond_1c
    move v2, v5

    .line 851
    goto :goto_9

    .line 871
    :cond_1d
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_20

    .line 872
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 873
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 874
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/y;

    .line 875
    invoke-static {v0, p2}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;Ljava/util/ListIterator;)Z

    move-result v6

    if-nez v6, :cond_1e

    .line 876
    invoke-virtual {v0}, Lorg/htmlcleaner/y;->mt()Lorg/htmlcleaner/y;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    goto :goto_b

    .line 878
    :cond_1e
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_b

    :cond_1f
    move v0, v4

    .line 883
    :goto_c
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_20

    .line 884
    invoke-interface {p2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 883
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 889
    :cond_20
    invoke-interface {p2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    goto/16 :goto_0

    .line 890
    :cond_21
    invoke-direct {p0, v0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/b;Lorg/htmlcleaner/e;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 892
    invoke-direct {p0, p1, v0, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Ljava/lang/Object;Lorg/htmlcleaner/e;)V

    .line 893
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 894
    :cond_22
    if-eqz v8, :cond_23

    invoke-virtual {v8}, Lorg/htmlcleaner/x;->mf()Z

    move-result v0

    if-nez v0, :cond_23

    .line 896
    invoke-direct {p0, v1}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/y;)Lorg/htmlcleaner/y;

    move-result-object v0

    .line 897
    invoke-direct {p0, v8, v0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/x;Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V

    .line 898
    invoke-interface {p2, v0}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 901
    :cond_23
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/ListIterator;->previousIndex()I

    move-result v1

    invoke-static {v0, v7, v1}, Lorg/htmlcleaner/p;->a(Lorg/htmlcleaner/p;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 904
    :cond_24
    iget-boolean v1, p3, Lorg/htmlcleaner/e;->CA:Z

    if-eqz v1, :cond_25

    iget-boolean v1, p3, Lorg/htmlcleaner/e;->CB:Z

    if-nez v1, :cond_25

    iget-object v1, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->la()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 905
    instance-of v1, v0, Lorg/htmlcleaner/h;

    if-eqz v1, :cond_26

    .line 906
    invoke-direct {p0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/e;)Lorg/htmlcleaner/p;

    move-result-object v1

    invoke-static {v1}, Lorg/htmlcleaner/p;->b(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;

    move-result-object v1

    if-nez v1, :cond_25

    .line 907
    iget-object v2, p3, Lorg/htmlcleaner/e;->CC:Ljava/util/Set;

    new-instance v6, Lorg/htmlcleaner/u;

    move-object v1, v0

    check-cast v1, Lorg/htmlcleaner/h;

    iget-object v7, p3, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    invoke-direct {v6, v1, v7}, Lorg/htmlcleaner/u;-><init>(Lorg/htmlcleaner/h;Lorg/htmlcleaner/y;)V

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 920
    :cond_25
    :goto_d
    invoke-direct {p0, v0, p3}, Lorg/htmlcleaner/m;->a(Lorg/htmlcleaner/b;Lorg/htmlcleaner/e;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 921
    invoke-direct {p0, p1, v0, p3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Ljava/lang/Object;Lorg/htmlcleaner/e;)V

    .line 922
    invoke-interface {p2, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 909
    :cond_26
    instance-of v1, v0, Lorg/htmlcleaner/i;

    if-eqz v1, :cond_25

    move-object v1, v0

    .line 910
    check-cast v1, Lorg/htmlcleaner/i;

    .line 911
    invoke-virtual {v1}, Lorg/htmlcleaner/i;->lk()Z

    move-result v2

    if-eqz v2, :cond_25

    .line 912
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/htmlcleaner/b;

    .line 913
    if-ne v2, v0, :cond_25

    .line 914
    iget-object v2, p3, Lorg/htmlcleaner/e;->CC:Ljava/util/Set;

    new-instance v6, Lorg/htmlcleaner/u;

    iget-object v7, p3, Lorg/htmlcleaner/e;->CI:Lorg/htmlcleaner/y;

    invoke-direct {v6, v1, v7}, Lorg/htmlcleaner/u;-><init>(Lorg/htmlcleaner/i;Lorg/htmlcleaner/y;)V

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 926
    :cond_27
    return-void
.end method

.method protected a(Lorg/htmlcleaner/y;Lorg/htmlcleaner/e;)V
    .locals 1

    .prologue
    .line 1128
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/htmlcleaner/y;->P(Z)V

    .line 1129
    iget-object v0, p2, Lorg/htmlcleaner/e;->CM:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1130
    return-void
.end method

.method public cd(Ljava/lang/String;)Lorg/htmlcleaner/y;
    .locals 2

    .prologue
    .line 334
    :try_start_0
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    new-instance v1, Lorg/htmlcleaner/e;

    invoke-direct {v1}, Lorg/htmlcleaner/e;-><init>()V

    invoke-virtual {p0, v0, v1}, Lorg/htmlcleaner/m;->a(Ljava/io/Reader;Lorg/htmlcleaner/e;)Lorg/htmlcleaner/y;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 335
    :catch_0
    move-exception v0

    .line 337
    new-instance v1, Lorg/htmlcleaner/HtmlCleanerException;

    invoke-direct {v1, v0}, Lorg/htmlcleaner/HtmlCleanerException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public kP()Lorg/htmlcleaner/t;
    .locals 1

    .prologue
    .line 1181
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->kP()Lorg/htmlcleaner/t;

    move-result-object v0

    return-object v0
.end method

.method public ll()Lorg/htmlcleaner/f;
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lorg/htmlcleaner/m;->DT:Lorg/htmlcleaner/f;

    return-object v0
.end method

.method public lm()Lorg/htmlcleaner/g;
    .locals 1

    .prologue
    .line 1188
    iget-object v0, p0, Lorg/htmlcleaner/m;->DU:Lorg/htmlcleaner/g;

    return-object v0
.end method
