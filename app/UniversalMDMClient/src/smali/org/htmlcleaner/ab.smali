.class public Lorg/htmlcleaner/ab;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static Fs:Ljava/lang/String;

.field private static final Ft:Ljava/util/regex/Pattern;

.field public static Fu:Ljava/util/regex/Pattern;

.field public static Fv:Ljava/util/regex/Pattern;

.field public static Fw:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209
    const-string v0, "\\p{Print}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/htmlcleaner/ab;->Ft:Ljava/util/regex/Pattern;

    .line 262
    const-string v0, "^([x|X][\\p{XDigit}]+)(;?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/htmlcleaner/ab;->Fu:Ljava/util/regex/Pattern;

    .line 263
    const-string v0, "^0*([x|X][\\p{XDigit}]+)(;?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/htmlcleaner/ab;->Fv:Ljava/util/regex/Pattern;

    .line 264
    const-string v0, "^([\\p{Digit}]+)(;?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/htmlcleaner/ab;->Fw:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static G(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 337
    if-nez p0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return v1

    .line 340
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    .line 341
    invoke-static/range {v0 .. v7}, Lorg/htmlcleaner/ab;->a(Ljava/lang/String;ZZZZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 343
    const/16 v3, 0xa0

    const/16 v4, 0x20

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;IZLjava/lang/StringBuilder;)I
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 281
    add-int/lit8 v1, p1, 0xf

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 283
    if-eqz p2, :cond_2

    .line 284
    sget-object v0, Lorg/htmlcleaner/ab;->Fv:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 289
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v0, Lorg/htmlcleaner/ab;->Fw:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/2addr p1, v1

    .line 292
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_1
    return p1

    .line 286
    :cond_2
    sget-object v0, Lorg/htmlcleaner/ab;->Fu:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;ZZZLjava/lang/StringBuilder;I)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    invoke-static {p0, p5, v0, v1}, Lorg/htmlcleaner/ab;->a(Ljava/lang/String;IZLjava/lang/StringBuilder;)I

    move-result v2

    .line 222
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 224
    const/4 v0, 0x0

    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v1, v0, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 225
    if-eqz v3, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x10

    invoke-static {v0, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-char v0, v0

    .line 228
    :goto_0
    sget-object v4, Lorg/htmlcleaner/v;->EB:Lorg/htmlcleaner/v;

    invoke-virtual {v4, v0}, Lorg/htmlcleaner/v;->bD(I)Lorg/htmlcleaner/w;

    move-result-object v4

    .line 229
    if-nez v0, :cond_1

    .line 232
    const-string v0, "&amp;"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :goto_1
    return v2

    .line 225
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-char v0, v0

    goto :goto_0

    .line 233
    :cond_1
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lorg/htmlcleaner/w;->lW()Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez p2, :cond_6

    .line 238
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {v4}, Lorg/htmlcleaner/w;->lU()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 251
    :catch_0
    move-exception v0

    .line 253
    const-string v0, "&amp;#"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 238
    :cond_3
    if-eqz p3, :cond_5

    if-eqz v3, :cond_4

    :try_start_1
    invoke-virtual {v4}, Lorg/htmlcleaner/w;->lY()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Lorg/htmlcleaner/w;->lX()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Lorg/htmlcleaner/w;->lV()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 241
    :cond_6
    if-eqz p2, :cond_7

    .line 243
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 244
    :cond_7
    sget-object v3, Lorg/htmlcleaner/ab;->Ft:Ljava/util/regex/Pattern;

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [C

    const/4 v6, 0x0

    aput-char v0, v5, v6

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 247
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 249
    :cond_8
    const-string v0, "&#"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 256
    :cond_9
    const-string v0, "&amp;"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/String;ZZZZZZZ)Ljava/lang/String;
    .locals 7

    .prologue
    .line 146
    if-eqz p0, :cond_11

    .line 147
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 148
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 150
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_10

    .line 151
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 154
    const/16 v2, 0x26

    if-ne v0, v2, :cond_d

    .line 155
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    add-int/lit8 v0, v6, -0x1

    if-ge v1, v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x23

    if-ne v0, v2, :cond_1

    .line 156
    add-int/lit8 v5, v1, 0x2

    move-object v0, p0

    move v1, p4

    move v2, p2

    move v3, p6

    invoke-static/range {v0 .. v5}, Lorg/htmlcleaner/ab;->a(Ljava/lang/String;ZZZLjava/lang/StringBuilder;I)I

    move-result v0

    .line 150
    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 157
    :cond_1
    if-nez p3, :cond_2

    if-eqz p1, :cond_a

    :cond_2
    sget-object v0, Lorg/htmlcleaner/v;->EB:Lorg/htmlcleaner/v;

    const/16 v2, 0xa

    sub-int v3, v6, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/htmlcleaner/v;->cj(Ljava/lang/String;)Lorg/htmlcleaner/w;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 159
    if-eqz p3, :cond_4

    invoke-virtual {v2}, Lorg/htmlcleaner/w;->lW()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    if-eqz p2, :cond_3

    .line 161
    invoke-virtual {v2}, Lorg/htmlcleaner/w;->intValue()I

    move-result v0

    int-to-char v0, v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 165
    :goto_2
    invoke-virtual {v2}, Lorg/htmlcleaner/w;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    goto :goto_1

    .line 163
    :cond_3
    invoke-virtual {v2}, Lorg/htmlcleaner/w;->lX()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 166
    :cond_4
    if-eqz p1, :cond_8

    .line 171
    if-eqz p5, :cond_5

    invoke-virtual {v2}, Lorg/htmlcleaner/w;->lX()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {v2}, Lorg/htmlcleaner/w;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    goto :goto_1

    .line 171
    :cond_5
    if-nez p7, :cond_6

    if-eqz p4, :cond_7

    :cond_6
    const/4 v0, 0x1

    :goto_4
    invoke-virtual {v2, v0}, Lorg/htmlcleaner/w;->L(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    .line 174
    :cond_8
    if-eqz p5, :cond_9

    invoke-static {}, Lorg/htmlcleaner/ab;->mA()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    :cond_9
    const-string v0, "&amp;"

    goto :goto_5

    .line 180
    :cond_a
    if-eqz p7, :cond_b

    sget-object v0, Lorg/htmlcleaner/v;->EB:Lorg/htmlcleaner/v;

    const/16 v2, 0xa

    sub-int v3, v6, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/htmlcleaner/v;->cj(Ljava/lang/String;)Lorg/htmlcleaner/w;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 182
    const-string v0, "&"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto/16 :goto_1

    .line 184
    :cond_b
    if-eqz p5, :cond_c

    invoke-static {}, Lorg/htmlcleaner/ab;->mA()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto/16 :goto_1

    :cond_c
    const-string v0, "&amp;"

    goto :goto_6

    .line 186
    :cond_d
    sget-object v2, Lorg/htmlcleaner/v;->EB:Lorg/htmlcleaner/v;

    invoke-virtual {v2, v0}, Lorg/htmlcleaner/v;->bD(I)Lorg/htmlcleaner/w;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 187
    if-eqz p5, :cond_e

    invoke-virtual {v2}, Lorg/htmlcleaner/w;->lX()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto/16 :goto_1

    :cond_e
    invoke-virtual {v2, p4}, Lorg/htmlcleaner/w;->L(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 189
    :cond_f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    goto/16 :goto_1

    .line 193
    :cond_10
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_8
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public static i(C)Z
    .locals 1

    .prologue
    .line 305
    const/16 v0, 0x3a

    if-eq v0, p0, :cond_0

    const/16 v0, 0x2e

    if-eq v0, p0, :cond_0

    const/16 v0, 0x2d

    if-eq v0, p0, :cond_0

    const/16 v0, 0x5f

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static mA()Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    sget-object v0, Lorg/htmlcleaner/ab;->Fs:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 203
    sget-object v0, Lorg/htmlcleaner/v;->EB:Lorg/htmlcleaner/v;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/v;->bD(I)Lorg/htmlcleaner/w;

    move-result-object v0

    invoke-virtual {v0}, Lorg/htmlcleaner/w;->lX()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/htmlcleaner/ab;->Fs:Ljava/lang/String;

    .line 206
    :cond_0
    sget-object v0, Lorg/htmlcleaner/ab;->Fs:Ljava/lang/String;

    return-object v0
.end method
