.class public Lorg/htmlcleaner/ac;
.super Ljava/lang/Object;
.source "XPather.java"


# instance fields
.field private Fx:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v0, "/()[]\"\'=<>"

    const/4 v1, 0x1

    invoke-direct {v2, p1, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 83
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    .line 84
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    .line 86
    const/4 v0, 0x0

    .line 90
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    iget-object v3, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    move v0, v1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method private H(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 451
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 452
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    return-object v0
.end method

.method private I(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 603
    if-nez p1, :cond_0

    .line 604
    const-string v0, ""

    .line 608
    :goto_0
    return-object v0

    .line 605
    :cond_0
    instance-of v0, p1, Lorg/htmlcleaner/y;

    if-eqz v0, :cond_1

    .line 606
    check-cast p1, Lorg/htmlcleaner/y;

    invoke-virtual {p1}, Lorg/htmlcleaner/y;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 608
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;II)Ljava/util/Collection;
    .locals 13

    .prologue
    .line 350
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 351
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 352
    const/4 v5, 0x0

    .line 353
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v6

    .line 354
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    .line 356
    add-int/lit8 v5, v5, 0x1

    .line 358
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {p0, v11}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v7, 0x1

    invoke-direct {p0, v11}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v8

    move-object v0, p0

    move v2, p2

    move/from16 v3, p3

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 359
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 360
    const/4 v0, 0x0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 361
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 362
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 365
    :cond_1
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 366
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 367
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 370
    :cond_2
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 374
    :cond_3
    return-object v9
.end method

.method private a(Ljava/util/Collection;IIIIZ)Ljava/util/Collection;
    .locals 14

    .prologue
    .line 301
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 302
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 304
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v10

    .line 305
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 306
    const/4 v0, 0x0

    move v1, v0

    .line 307
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 308
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 309
    add-int/lit8 v9, v1, 0x1

    .line 310
    const-string v1, "last"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311
    if-eqz p6, :cond_1

    move/from16 v0, p5

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    move v1, v9

    .line 338
    goto :goto_0

    :cond_1
    move v0, v10

    .line 311
    goto :goto_1

    .line 312
    :cond_2
    const-string v1, "position"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 313
    if-eqz p6, :cond_3

    move/from16 v0, p4

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move v0, v9

    goto :goto_3

    .line 314
    :cond_4
    const-string v1, "text"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 315
    instance-of v1, v0, Lorg/htmlcleaner/y;

    if-eqz v1, :cond_5

    .line 316
    check-cast v0, Lorg/htmlcleaner/y;

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 317
    :cond_5
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 318
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 320
    :cond_6
    const-string v0, "count"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 321
    add-int/lit8 v2, p2, 0x2

    add-int/lit8 v3, p3, -0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move/from16 v5, p4

    move/from16 v7, p6

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 323
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 324
    :cond_7
    const-string v0, "data"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 325
    add-int/lit8 v2, p2, 0x2

    add-int/lit8 v3, p3, -0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move/from16 v5, p4

    move/from16 v7, p6

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 326
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 327
    :cond_8
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 329
    instance-of v2, v0, Lorg/htmlcleaner/y;

    if-eqz v2, :cond_9

    .line 330
    check-cast v0, Lorg/htmlcleaner/y;

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 331
    :cond_9
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 332
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 336
    :cond_a
    new-instance v0, Lorg/htmlcleaner/XPatherException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown function "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/htmlcleaner/XPatherException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_b
    return-object v12
.end method

.method private a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;
    .locals 10

    .prologue
    .line 129
    if-ltz p2, :cond_0

    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    array-length v0, v0

    if-ge p3, v0, :cond_0

    if-gt p2, p3, :cond_0

    .line 130
    const-string v0, ""

    iget-object v1, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    add-int/lit8 v2, p2, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    .line 195
    :cond_0
    :goto_0
    return-object p1

    .line 132
    :cond_1
    const-string v0, "("

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 133
    invoke-direct {p0, p2, p3}, Lorg/htmlcleaner/ac;->j(II)I

    move-result v9

    .line 134
    if-lez v9, :cond_2

    .line 135
    add-int/lit8 v2, p2, 0x1

    add-int/lit8 v3, v9, -0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    .line 136
    add-int/lit8 v2, v9, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto :goto_0

    .line 138
    :cond_2
    invoke-direct {p0}, Lorg/htmlcleaner/ac;->mB()V

    .line 198
    :goto_1
    new-instance v0, Lorg/htmlcleaner/XPatherException;

    invoke-direct {v0}, Lorg/htmlcleaner/XPatherException;-><init>()V

    throw v0

    .line 140
    :cond_3
    const-string v0, "["

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 141
    invoke-direct {p0, p2, p3}, Lorg/htmlcleaner/ac;->j(II)I

    move-result v0

    .line 142
    if-lez v0, :cond_4

    if-eqz p1, :cond_4

    .line 143
    add-int/lit8 v1, p2, 0x1

    add-int/lit8 v2, v0, -0x1

    invoke-direct {p0, p1, v1, v2}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;II)Ljava/util/Collection;

    move-result-object v1

    .line 144
    add-int/lit8 v2, v0, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto :goto_0

    .line 146
    :cond_4
    invoke-direct {p0}, Lorg/htmlcleaner/ac;->mB()V

    goto :goto_1

    .line 148
    :cond_5
    const-string v0, "\""

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "\'"

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 149
    :cond_6
    invoke-direct {p0, p2, p3}, Lorg/htmlcleaner/ac;->j(II)I

    move-result v0

    .line 150
    if-le v0, p2, :cond_7

    .line 151
    add-int/lit8 v1, p2, 0x1

    add-int/lit8 v2, v0, -0x1

    invoke-direct {p0, v1, v2}, Lorg/htmlcleaner/ac;->h(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 152
    add-int/lit8 v2, v0, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0

    .line 154
    :cond_7
    invoke-direct {p0}, Lorg/htmlcleaner/ac;->mB()V

    goto :goto_1

    .line 156
    :cond_8
    const-string v0, "="

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "<"

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, ">"

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_9
    if-eqz p7, :cond_c

    .line 158
    const-string v0, "="

    add-int/lit8 v1, p2, 0x1

    invoke-direct {p0, v0, v1}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "<"

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, ">"

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 159
    :cond_a
    add-int/lit8 v2, p2, 0x2

    const/4 v4, 0x0

    move-object v0, p0

    move-object/from16 v1, p8

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 160
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    add-int/lit8 v3, p2, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/String;)Z

    move-result v0

    .line 165
    :goto_2
    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {v1, v0}, Ljava/lang/Boolean;-><init>(Z)V

    invoke-direct {p0, v1}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0

    .line 162
    :cond_b
    add-int/lit8 v2, p2, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object/from16 v1, p8

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-direct {p0, p1, v0, v1}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/String;)Z

    move-result v0

    goto :goto_2

    .line 166
    :cond_c
    const-string v0, "/"

    invoke-direct {p0, v0, p2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 167
    const-string v0, "/"

    add-int/lit8 v1, p2, 0x1

    invoke-direct {p0, v0, v1}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v4

    .line 168
    if-eqz v4, :cond_d

    .line 169
    add-int/lit8 p2, p2, 0x1

    .line 171
    :cond_d
    if-ge p2, p3, :cond_f

    .line 172
    invoke-direct {p0, p2, p3}, Lorg/htmlcleaner/ac;->j(II)I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .line 173
    if-gt v3, p2, :cond_e

    move v3, p3

    .line 176
    :cond_e
    add-int/lit8 v2, p2, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    .line 177
    add-int/lit8 v2, v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p3

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0

    .line 179
    :cond_f
    invoke-direct {p0}, Lorg/htmlcleaner/ac;->mB()V

    goto/16 :goto_1

    .line 181
    :cond_10
    invoke-direct {p0, p2, p3}, Lorg/htmlcleaner/ac;->i(II)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 182
    add-int/lit8 v0, p2, 0x1

    invoke-direct {p0, v0, p3}, Lorg/htmlcleaner/ac;->j(II)I

    move-result v7

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    .line 183
    invoke-direct/range {v0 .. v6}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIIIZ)Ljava/util/Collection;

    move-result-object v1

    .line 184
    add-int/lit8 v2, v7, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p3

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0

    .line 185
    :cond_11
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-static {v0}, Lorg/htmlcleaner/ac;->cx(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 186
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 187
    add-int/lit8 v2, p2, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0

    .line 188
    :cond_12
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-direct {p0, v0}, Lorg/htmlcleaner/ac;->cy(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 189
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 190
    add-int/lit8 v2, p2, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0

    :cond_13
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p7

    .line 192
    invoke-direct/range {v0 .. v5}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZZ)Ljava/util/Collection;

    move-result-object p1

    goto/16 :goto_0
.end method

.method private a(Ljava/util/Collection;IIZZ)Ljava/util/Collection;
    .locals 19

    .prologue
    .line 466
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    .line 468
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/htmlcleaner/ac;->cA(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 469
    const/4 v1, 0x1

    invoke-virtual {v14, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 470
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 472
    if-eqz p4, :cond_2

    .line 473
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 474
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 475
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 476
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 477
    instance-of v4, v1, Lorg/htmlcleaner/y;

    if-eqz v4, :cond_0

    .line 478
    check-cast v1, Lorg/htmlcleaner/y;

    .line 479
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/htmlcleaner/y;->M(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    move-object/from16 p1, v2

    .line 486
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 487
    :cond_3
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 488
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 489
    instance-of v2, v1, Lorg/htmlcleaner/y;

    if-eqz v2, :cond_5

    .line 490
    check-cast v1, Lorg/htmlcleaner/y;

    .line 491
    const-string v2, "*"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 492
    invoke-virtual {v1}, Lorg/htmlcleaner/y;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v9, 0x0

    move-object/from16 v1, p0

    move/from16 v4, p3

    move/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 494
    :cond_4
    invoke-virtual {v1, v11}, Lorg/htmlcleaner/y;->cv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 495
    if-eqz v1, :cond_3

    .line 496
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v9, 0x0

    move-object/from16 v1, p0

    move/from16 v4, p3

    move/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 500
    :cond_5
    invoke-direct/range {p0 .. p0}, Lorg/htmlcleaner/ac;->mB()V

    goto :goto_1

    :cond_6
    move-object v1, v10

    .line 551
    :goto_2
    return-object v1

    .line 505
    :cond_7
    new-instance v13, Ljava/util/LinkedHashSet;

    invoke-direct {v13}, Ljava/util/LinkedHashSet;-><init>()V

    .line 506
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 507
    const/4 v1, 0x0

    move v2, v1

    .line 508
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 509
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 510
    instance-of v3, v1, Lorg/htmlcleaner/y;

    if-eqz v3, :cond_11

    move-object v10, v1

    .line 511
    check-cast v10, Lorg/htmlcleaner/y;

    .line 512
    add-int/lit8 v6, v2, 0x1

    .line 513
    const-string v1, "."

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    .line 514
    const-string v1, ".."

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    .line 515
    const-string v1, "*"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    .line 518
    if-eqz v16, :cond_b

    .line 519
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 527
    :goto_4
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 528
    add-int/lit8 v3, p2, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->size()I

    move-result v7

    const/4 v9, 0x0

    move-object/from16 v1, p0

    move/from16 v4, p3

    move/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v2

    .line 530
    if-eqz p4, :cond_f

    .line 531
    invoke-virtual {v10}, Lorg/htmlcleaner/y;->mn()Ljava/util/List;

    move-result-object v1

    .line 532
    if-nez v16, :cond_8

    if-nez v17, :cond_8

    if-eqz v18, :cond_9

    .line 533
    :cond_8
    invoke-interface {v13, v2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 535
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 536
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 537
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/htmlcleaner/y;

    .line 538
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v8

    move-object/from16 v7, p0

    move/from16 v9, p2

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p5

    invoke-direct/range {v7 .. v12}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZZ)Ljava/util/Collection;

    move-result-object v4

    .line 539
    if-nez v16, :cond_a

    if-nez v17, :cond_a

    if-nez v18, :cond_a

    invoke-interface {v2, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 540
    invoke-interface {v13, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 542
    :cond_a
    invoke-interface {v13, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 520
    :cond_b
    if-eqz v17, :cond_d

    .line 521
    invoke-virtual {v10}, Lorg/htmlcleaner/y;->lQ()Lorg/htmlcleaner/y;

    move-result-object v1

    .line 522
    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    goto :goto_4

    :cond_c
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_4

    .line 524
    :cond_d
    if-eqz v18, :cond_e

    invoke-virtual {v10}, Lorg/htmlcleaner/y;->mn()Ljava/util/List;

    move-result-object v1

    goto :goto_4

    :cond_e
    const/4 v1, 0x0

    invoke-virtual {v10, v14, v1}, Lorg/htmlcleaner/y;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v1

    goto :goto_4

    .line 545
    :cond_f
    invoke-interface {v13, v2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    :cond_10
    move v1, v6

    :goto_6
    move v2, v1

    .line 550
    goto/16 :goto_3

    .line 548
    :cond_11
    invoke-direct/range {p0 .. p0}, Lorg/htmlcleaner/ac;->mB()V

    move v1, v2

    goto :goto_6

    :cond_12
    move-object v1, v13

    .line 551
    goto/16 :goto_2
.end method

.method private a(Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 563
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 599
    :cond_1
    :goto_0
    return v2

    .line 566
    :cond_2
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 567
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 568
    instance-of v4, v0, Ljava/lang/Number;

    if-eqz v4, :cond_8

    instance-of v4, v1, Ljava/lang/Number;

    if-eqz v4, :cond_8

    .line 569
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    move-object v0, v1

    .line 570
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 571
    const-string v6, "="

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 572
    cmpl-double v0, v4, v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 573
    :cond_4
    const-string v6, "<"

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 574
    cmpg-double v0, v4, v0

    if-ltz v0, :cond_1

    move v2, v3

    goto :goto_0

    .line 575
    :cond_5
    const-string v6, ">"

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 576
    cmpl-double v0, v4, v0

    if-gtz v0, :cond_1

    move v2, v3

    goto :goto_0

    .line 577
    :cond_6
    const-string v6, "<="

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 578
    cmpg-double v0, v4, v0

    if-lez v0, :cond_1

    move v2, v3

    goto :goto_0

    .line 579
    :cond_7
    const-string v6, ">="

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 580
    cmpl-double v0, v4, v0

    if-gez v0, :cond_1

    move v2, v3

    goto :goto_0

    .line 583
    :cond_8
    invoke-direct {p0, v0}, Lorg/htmlcleaner/ac;->I(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 584
    invoke-direct {p0, v1}, Lorg/htmlcleaner/ac;->I(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 585
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 586
    const-string v1, "="

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 587
    if-eqz v0, :cond_1

    move v2, v3

    goto/16 :goto_0

    .line 588
    :cond_9
    const-string v1, "<"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 589
    if-ltz v0, :cond_1

    move v2, v3

    goto/16 :goto_0

    .line 590
    :cond_a
    const-string v1, ">"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 591
    if-gtz v0, :cond_1

    move v2, v3

    goto/16 :goto_0

    .line 592
    :cond_b
    const-string v1, "<="

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 593
    if-lez v0, :cond_1

    move v2, v3

    goto/16 :goto_0

    .line 594
    :cond_c
    const-string v1, ">="

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 595
    if-gez v0, :cond_1

    move v2, v3

    goto/16 :goto_0

    :cond_d
    move v2, v3

    .line 599
    goto/16 :goto_0
.end method

.method private cA(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 443
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v0, :cond_0

    const-string v1, "@"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static cx(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/16 v6, 0x39

    const/16 v5, 0x30

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 215
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 216
    if-lez v3, :cond_1

    .line 217
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 218
    const/16 v4, 0x2b

    if-eq v2, v4, :cond_0

    const/16 v4, 0x2d

    if-eq v2, v4, :cond_0

    if-lt v2, v5, :cond_1

    if-gt v2, v6, :cond_1

    :cond_0
    move v2, v1

    .line 219
    :goto_0
    if-ge v2, v3, :cond_3

    .line 220
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 221
    if-lt v4, v5, :cond_1

    if-le v4, v6, :cond_2

    .line 227
    :cond_1
    :goto_1
    return v0

    .line 219
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 224
    goto :goto_1
.end method

.method private cy(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/16 v7, 0x39

    const/16 v6, 0x30

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 231
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 232
    if-lez v3, :cond_1

    .line 233
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 234
    const/16 v4, 0x2b

    if-eq v2, v4, :cond_0

    const/16 v4, 0x2d

    if-eq v2, v4, :cond_0

    const/16 v4, 0x20

    if-eq v2, v4, :cond_0

    if-lt v2, v6, :cond_1

    if-gt v2, v7, :cond_1

    :cond_0
    move v2, v1

    .line 235
    :goto_0
    if-ge v2, v3, :cond_3

    .line 236
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 237
    const/16 v5, 0x2e

    if-eq v4, v5, :cond_2

    if-lt v4, v6, :cond_1

    if-le v4, v7, :cond_2

    .line 243
    :cond_1
    :goto_1
    return v0

    .line 235
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 240
    goto :goto_1
.end method

.method private cz(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 251
    if-nez p1, :cond_1

    .line 268
    :cond_0
    return v4

    .line 255
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 257
    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 261
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 262
    const/16 v3, 0x5f

    if-eq v2, v3, :cond_2

    const/16 v3, 0x2d

    if-eq v2, v3, :cond_2

    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private h(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    if-gt p1, p2, :cond_1

    .line 203
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 204
    :goto_0
    if-gt p1, p2, :cond_0

    .line 205
    iget-object v1, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 204
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 208
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    :goto_1
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private i(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 278
    iget-object v1, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {p0, v1}, Lorg/htmlcleaner/ac;->cz(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "("

    add-int/lit8 v2, p1, 0x1

    invoke-direct {p0, v1, v2}, Lorg/htmlcleaner/ac;->m(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    :cond_0
    :goto_0
    return v0

    :cond_1
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v1, p2}, Lorg/htmlcleaner/ac;->j(II)I

    move-result v1

    add-int/lit8 v2, p1, 0x1

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private j(II)I
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 389
    if-ge p1, p2, :cond_11

    .line 390
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v4, v0, p1

    .line 392
    const-string v0, "\""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 393
    add-int/lit8 v0, p1, 0x1

    :goto_0
    if-gt v0, p2, :cond_11

    .line 394
    const-string v1, "\""

    iget-object v2, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 435
    :cond_0
    :goto_1
    return v0

    .line 393
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_2
    const-string v0, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 399
    add-int/lit8 v0, p1, 0x1

    :goto_2
    if-gt v0, p2, :cond_11

    .line 400
    const-string v1, "\'"

    iget-object v2, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 404
    :cond_3
    const-string v0, "("

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "["

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 407
    :cond_4
    const-string v0, "("

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 408
    :goto_3
    const-string v3, "["

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    .line 409
    :goto_4
    const-string v5, "/"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v1

    .line 410
    :goto_5
    add-int/lit8 v5, p1, 0x1

    move v6, v0

    move v7, v1

    move v8, v1

    move v10, v3

    move v3, v5

    move v5, v10

    :goto_6
    if-gt v3, p2, :cond_11

    .line 411
    const-string v0, "\""

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 412
    if-nez v8, :cond_8

    move v0, v1

    :goto_7
    move v10, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v0

    move v0, v10

    .line 427
    :goto_8
    if-eqz v7, :cond_10

    if-eqz v6, :cond_10

    if-nez v5, :cond_10

    if-nez v4, :cond_10

    if-nez v0, :cond_10

    move v0, v3

    .line 428
    goto :goto_1

    :cond_5
    move v0, v2

    .line 407
    goto :goto_3

    :cond_6
    move v3, v2

    .line 408
    goto :goto_4

    :cond_7
    move v4, v2

    .line 409
    goto :goto_5

    :cond_8
    move v0, v2

    .line 412
    goto :goto_7

    .line 413
    :cond_9
    const-string v0, "\'"

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 414
    if-nez v7, :cond_a

    move v0, v1

    :goto_9
    move v7, v8

    move v10, v5

    move v5, v6

    move v6, v0

    move v0, v4

    move v4, v10

    goto :goto_8

    :cond_a
    move v0, v2

    goto :goto_9

    .line 415
    :cond_b
    const-string v0, "("

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    if-eqz v8, :cond_c

    if-eqz v7, :cond_c

    .line 416
    add-int/lit8 v0, v6, 0x1

    move v6, v7

    move v7, v8

    move v10, v0

    move v0, v4

    move v4, v5

    move v5, v10

    goto :goto_8

    .line 417
    :cond_c
    const-string v0, ")"

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    if-eqz v8, :cond_d

    if-eqz v7, :cond_d

    .line 418
    add-int/lit8 v0, v6, -0x1

    move v6, v7

    move v7, v8

    move v10, v0

    move v0, v4

    move v4, v5

    move v5, v10

    goto :goto_8

    .line 419
    :cond_d
    const-string v0, "["

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    if-eqz v8, :cond_e

    if-eqz v7, :cond_e

    .line 420
    add-int/lit8 v0, v5, 0x1

    move v5, v6

    move v6, v7

    move v7, v8

    move v10, v0

    move v0, v4

    move v4, v10

    goto :goto_8

    .line 421
    :cond_e
    const-string v0, "]"

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    if-eqz v8, :cond_f

    if-eqz v7, :cond_f

    .line 422
    add-int/lit8 v0, v5, -0x1

    move v5, v6

    move v6, v7

    move v7, v8

    move v10, v0

    move v0, v4

    move v4, v10

    goto/16 :goto_8

    .line 423
    :cond_f
    const-string v0, "/"

    iget-object v9, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v9, v9, v3

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    if-eqz v8, :cond_12

    if-eqz v7, :cond_12

    if-nez v6, :cond_12

    if-nez v5, :cond_12

    .line 424
    add-int/lit8 v0, v4, -0x1

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    goto/16 :goto_8

    .line 410
    :cond_10
    add-int/lit8 v3, v3, 0x1

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v0

    goto/16 :goto_6

    .line 435
    :cond_11
    const/4 v0, -0x1

    goto/16 :goto_1

    :cond_12
    move v0, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    goto/16 :goto_8
.end method

.method private m(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    array-length v0, v0

    .line 379
    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mB()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lorg/htmlcleaner/XPatherException;

    invoke-direct {v0}, Lorg/htmlcleaner/XPatherException;-><init>()V

    throw v0
.end method


# virtual methods
.method public b(Lorg/htmlcleaner/y;)[Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 101
    if-nez p1, :cond_0

    .line 102
    new-instance v0, Lorg/htmlcleaner/XPatherException;

    const-string v1, "Cannot evaluate XPath expression against null value!"

    invoke-direct {v0, v1}, Lorg/htmlcleaner/XPatherException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    invoke-direct {p0, p1}, Lorg/htmlcleaner/ac;->H(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    iget-object v0, p0, Lorg/htmlcleaner/ac;->Fx:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v3, v0, -0x1

    const/4 v5, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move v4, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/ac;->a(Ljava/util/Collection;IIZIIZLjava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 106
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Object;

    .line 108
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 110
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    add-int/lit8 v0, v2, 0x1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v1, v2

    move v2, v0

    goto :goto_0

    .line 114
    :cond_1
    return-object v1
.end method
