.class Lorg/htmlcleaner/p;
.super Ljava/lang/Object;
.source "HtmlCleaner.java"


# instance fields
.field final synthetic DX:Lorg/htmlcleaner/m;

.field private Ea:Lorg/htmlcleaner/q;

.field private Eb:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/htmlcleaner/m;)V
    .locals 1

    .prologue
    .line 194
    iput-object p1, p0, Lorg/htmlcleaner/p;->DX:Lorg/htmlcleaner/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    .line 197
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/p;->Eb:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lorg/htmlcleaner/p;)Ljava/util/List;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lorg/htmlcleaner/p;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Lorg/htmlcleaner/p;->l(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 286
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    .line 287
    invoke-static {v0}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    const/4 v0, 0x1

    .line 291
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lorg/htmlcleaner/p;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lorg/htmlcleaner/p;->cg(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lorg/htmlcleaner/p;Ljava/util/Set;)Z
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lorg/htmlcleaner/p;->a(Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/htmlcleaner/p;->lt()Lorg/htmlcleaner/q;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lorg/htmlcleaner/p;Ljava/lang/String;)Lorg/htmlcleaner/q;
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lorg/htmlcleaner/p;->cf(Ljava/lang/String;)Lorg/htmlcleaner/q;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/htmlcleaner/p;->lu()Lorg/htmlcleaner/q;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lorg/htmlcleaner/p;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lorg/htmlcleaner/p;->ch(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private cf(Ljava/lang/String;)Lorg/htmlcleaner/q;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 231
    if-eqz p1, :cond_1

    .line 232
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    iget-object v1, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 234
    iget-object v0, p0, Lorg/htmlcleaner/p;->DX:Lorg/htmlcleaner/m;

    invoke-virtual {v0}, Lorg/htmlcleaner/m;->kP()Lorg/htmlcleaner/t;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/htmlcleaner/t;->cb(Ljava/lang/String;)Lorg/htmlcleaner/x;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_3

    .line 236
    invoke-virtual {v0}, Lorg/htmlcleaner/x;->mb()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 239
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    .line 241
    invoke-static {v0}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v2, v0

    .line 250
    :cond_1
    :goto_1
    return-object v2

    .line 243
    :cond_2
    if-eqz v1, :cond_0

    invoke-static {v0}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_3
    move-object v1, v2

    goto :goto_0
.end method

.method private cg(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lorg/htmlcleaner/p;->cf(Ljava/lang/String;)Lorg/htmlcleaner/q;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ch(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lorg/htmlcleaner/p;->Eb:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lorg/htmlcleaner/p;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lorg/htmlcleaner/p;->removeTag(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lorg/htmlcleaner/p;)Z
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/htmlcleaner/p;->isEmpty()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lorg/htmlcleaner/p;)Lorg/htmlcleaner/q;
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/htmlcleaner/p;->ls()Lorg/htmlcleaner/q;

    move-result-object v0

    return-object v0
.end method

.method private isEmpty()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method private l(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 204
    new-instance v0, Lorg/htmlcleaner/q;

    iget-object v1, p0, Lorg/htmlcleaner/p;->DX:Lorg/htmlcleaner/m;

    invoke-direct {v0, v1, p2, p1}, Lorg/htmlcleaner/q;-><init>(Lorg/htmlcleaner/m;ILjava/lang/String;)V

    iput-object v0, p0, Lorg/htmlcleaner/p;->Ea:Lorg/htmlcleaner/q;

    .line 205
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    iget-object v1, p0, Lorg/htmlcleaner/p;->Ea:Lorg/htmlcleaner/q;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object v0, p0, Lorg/htmlcleaner/p;->Eb:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    return-void
.end method

.method private ls()Lorg/htmlcleaner/q;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    goto :goto_0
.end method

.method private lt()Lorg/htmlcleaner/q;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/htmlcleaner/p;->Ea:Lorg/htmlcleaner/q;

    return-object v0
.end method

.method private lu()Lorg/htmlcleaner/q;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 259
    .line 261
    invoke-direct {p0}, Lorg/htmlcleaner/p;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 262
    iget-object v1, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    iget-object v2, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v2

    move-object v1, v0

    .line 263
    :goto_0
    invoke-interface {v2}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    invoke-interface {v2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    .line 265
    invoke-static {v0}, Lorg/htmlcleaner/q;->c(Lorg/htmlcleaner/q;)Lorg/htmlcleaner/x;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lorg/htmlcleaner/q;->c(Lorg/htmlcleaner/q;)Lorg/htmlcleaner/x;

    move-result-object v3

    invoke-virtual {v3}, Lorg/htmlcleaner/x;->mk()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 266
    :cond_0
    if-eqz v1, :cond_1

    .line 274
    :goto_1
    return-object v1

    :cond_1
    move-object v1, v0

    .line 270
    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 274
    goto :goto_1
.end method

.method private removeTag(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    iget-object v1, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    .line 211
    :cond_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    invoke-interface {v1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    .line 213
    invoke-static {v0}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    .line 219
    :cond_1
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/htmlcleaner/p;->Ea:Lorg/htmlcleaner/q;

    .line 220
    return-void

    .line 219
    :cond_2
    iget-object v0, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    iget-object v1, p0, Lorg/htmlcleaner/p;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    goto :goto_0
.end method
