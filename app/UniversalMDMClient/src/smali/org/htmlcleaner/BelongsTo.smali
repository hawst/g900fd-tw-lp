.class public final enum Lorg/htmlcleaner/BelongsTo;
.super Ljava/lang/Enum;
.source "BelongsTo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/htmlcleaner/BelongsTo;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Cw:Lorg/htmlcleaner/BelongsTo;

.field public static final enum Cx:Lorg/htmlcleaner/BelongsTo;

.field public static final enum Cy:Lorg/htmlcleaner/BelongsTo;

.field private static final synthetic Cz:[Lorg/htmlcleaner/BelongsTo;


# instance fields
.field private final dbCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-instance v0, Lorg/htmlcleaner/BelongsTo;

    const-string v1, "HEAD_AND_BODY"

    const-string v2, "all"

    invoke-direct {v0, v1, v3, v2}, Lorg/htmlcleaner/BelongsTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/htmlcleaner/BelongsTo;->Cw:Lorg/htmlcleaner/BelongsTo;

    .line 44
    new-instance v0, Lorg/htmlcleaner/BelongsTo;

    const-string v1, "HEAD"

    const-string v2, "head"

    invoke-direct {v0, v1, v4, v2}, Lorg/htmlcleaner/BelongsTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    .line 45
    new-instance v0, Lorg/htmlcleaner/BelongsTo;

    const-string v1, "BODY"

    const-string v2, "body"

    invoke-direct {v0, v1, v5, v2}, Lorg/htmlcleaner/BelongsTo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/htmlcleaner/BelongsTo;

    sget-object v1, Lorg/htmlcleaner/BelongsTo;->Cw:Lorg/htmlcleaner/BelongsTo;

    aput-object v1, v0, v3

    sget-object v1, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    aput-object v1, v0, v4

    sget-object v1, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    aput-object v1, v0, v5

    sput-object v0, Lorg/htmlcleaner/BelongsTo;->Cz:[Lorg/htmlcleaner/BelongsTo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, Lorg/htmlcleaner/BelongsTo;->dbCode:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/htmlcleaner/BelongsTo;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/htmlcleaner/BelongsTo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/BelongsTo;

    return-object v0
.end method

.method public static values()[Lorg/htmlcleaner/BelongsTo;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lorg/htmlcleaner/BelongsTo;->Cz:[Lorg/htmlcleaner/BelongsTo;

    invoke-virtual {v0}, [Lorg/htmlcleaner/BelongsTo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/htmlcleaner/BelongsTo;

    return-object v0
.end method
