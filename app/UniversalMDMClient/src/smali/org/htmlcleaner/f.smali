.class public Lorg/htmlcleaner/f;
.super Ljava/lang/Object;
.source "CleanerProperties.java"

# interfaces
.implements Lorg/htmlcleaner/audit/a;


# instance fields
.field private CL:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/htmlcleaner/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private CN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/htmlcleaner/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private CP:Lorg/htmlcleaner/t;

.field private CQ:Z

.field private CR:Z

.field private CS:Z

.field private CT:Z

.field private CU:Z

.field private CV:Z

.field private CW:Z

.field private CX:Z

.field private CY:Z

.field private CZ:Lorg/htmlcleaner/OptionalOutput;

.field private Da:Lorg/htmlcleaner/OptionalOutput;

.field private Db:Lorg/htmlcleaner/OptionalOutput;

.field private Dc:Z

.field private Dd:Z

.field private De:Ljava/lang/String;

.field private Df:Z

.field private Dg:Z

.field private Dh:Z

.field private Di:Z

.field private Dj:Z

.field private Dk:Z

.field private Dl:Z

.field private Dm:Ljava/lang/String;

.field private Dn:Ljava/lang/String;

.field private Do:Ljava/lang/String;

.field private Dp:Lorg/htmlcleaner/g;

.field private Dq:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/audit/a;",
            ">;"
        }
    .end annotation
.end field

.field private Dr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, Lorg/htmlcleaner/g;

    invoke-direct {v0}, Lorg/htmlcleaner/g;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/f;->Dp:Lorg/htmlcleaner/g;

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/f;->CL:Ljava/util/Set;

    .line 117
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/f;->CN:Ljava/util/Set;

    .line 118
    const-string v0, "UTF-8"

    iput-object v0, p0, Lorg/htmlcleaner/f;->Dr:Ljava/lang/String;

    .line 122
    invoke-virtual {p0}, Lorg/htmlcleaner/f;->reset()V

    .line 123
    return-void
.end method

.method private a(Ljava/util/Set;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/htmlcleaner/a/a;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 392
    if-eqz p2, :cond_0

    .line 393
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v0, p2, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 395
    new-instance v1, Lorg/htmlcleaner/a/d;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/htmlcleaner/a/d;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 398
    :cond_0
    return-void
.end method

.method private bX(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lorg/htmlcleaner/f;->CN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 375
    iget-object v0, p0, Lorg/htmlcleaner/f;->CN:Ljava/util/Set;

    invoke-direct {p0, v0, p1}, Lorg/htmlcleaner/f;->a(Ljava/util/Set;Ljava/lang/String;)V

    .line 376
    return-void
.end method

.method private lh()V
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lorg/htmlcleaner/f;->CL:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 493
    iget-object v0, p0, Lorg/htmlcleaner/f;->CL:Ljava/util/Set;

    sget-object v1, Lorg/htmlcleaner/a/c;->FH:Lorg/htmlcleaner/a/c;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 494
    return-void
.end method


# virtual methods
.method public H(Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lorg/htmlcleaner/f;->CT:Z

    .line 186
    return-void
.end method

.method public I(Z)V
    .locals 0

    .prologue
    .line 225
    iput-boolean p1, p0, Lorg/htmlcleaner/f;->CX:Z

    .line 226
    return-void
.end method

.method public J(Z)V
    .locals 0

    .prologue
    .line 269
    iput-boolean p1, p0, Lorg/htmlcleaner/f;->Dd:Z

    .line 270
    return-void
.end method

.method public K(Z)V
    .locals 0

    .prologue
    .line 277
    iput-boolean p1, p0, Lorg/htmlcleaner/f;->Dg:Z

    .line 278
    return-void
.end method

.method public a(Lorg/htmlcleaner/a/a;Lorg/htmlcleaner/y;)V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/audit/a;

    .line 523
    invoke-interface {v0, p1, p2}, Lorg/htmlcleaner/audit/a;->a(Lorg/htmlcleaner/a/a;Lorg/htmlcleaner/y;)V

    goto :goto_0

    .line 525
    :cond_0
    return-void
.end method

.method a(Lorg/htmlcleaner/t;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lorg/htmlcleaner/f;->CP:Lorg/htmlcleaner/t;

    .line 138
    return-void
.end method

.method public a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/audit/a;

    .line 529
    invoke-interface {v0, p1, p2, p3}, Lorg/htmlcleaner/audit/a;->a(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto :goto_0

    .line 532
    :cond_0
    return-void
.end method

.method public b(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V
    .locals 2

    .prologue
    .line 535
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/audit/a;

    .line 536
    invoke-interface {v0, p1, p2, p3}, Lorg/htmlcleaner/audit/a;->b(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto :goto_0

    .line 538
    :cond_0
    return-void
.end method

.method public bV(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 346
    iput-object p1, p0, Lorg/htmlcleaner/f;->Dn:Ljava/lang/String;

    .line 347
    invoke-direct {p0}, Lorg/htmlcleaner/f;->lh()V

    .line 348
    iget-object v0, p0, Lorg/htmlcleaner/f;->CL:Ljava/util/Set;

    invoke-direct {p0, v0, p1}, Lorg/htmlcleaner/f;->a(Ljava/util/Set;Ljava/lang/String;)V

    .line 349
    return-void
.end method

.method public bW(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lorg/htmlcleaner/f;->Do:Ljava/lang/String;

    .line 370
    invoke-direct {p0, p1}, Lorg/htmlcleaner/f;->bX(Ljava/lang/String;)V

    .line 371
    return-void
.end method

.method public c(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V
    .locals 2

    .prologue
    .line 541
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dq:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/audit/a;

    .line 542
    invoke-interface {v0, p1, p2, p3}, Lorg/htmlcleaner/audit/a;->c(ZLorg/htmlcleaner/y;Lorg/htmlcleaner/audit/ErrorType;)V

    goto :goto_0

    .line 544
    :cond_0
    return-void
.end method

.method public kP()Lorg/htmlcleaner/t;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/htmlcleaner/f;->CP:Lorg/htmlcleaner/t;

    return-object v0
.end method

.method public kQ()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->CU:Z

    return v0
.end method

.method public kR()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->CV:Z

    return v0
.end method

.method public kS()Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->CW:Z

    return v0
.end method

.method public kT()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->CY:Z

    return v0
.end method

.method public kU()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->CX:Z

    return v0
.end method

.method public kV()Z
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lorg/htmlcleaner/f;->Db:Lorg/htmlcleaner/OptionalOutput;

    sget-object v1, Lorg/htmlcleaner/OptionalOutput;->Ev:Lorg/htmlcleaner/OptionalOutput;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public kW()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Dd:Z

    return v0
.end method

.method public kX()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Dg:Z

    return v0
.end method

.method public kY()Z
    .locals 1

    .prologue
    .line 281
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Df:Z

    return v0
.end method

.method public kZ()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Dh:Z

    return v0
.end method

.method public la()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Dl:Z

    return v0
.end method

.method public lb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dm:Ljava/lang/String;

    return-object v0
.end method

.method public lc()Z
    .locals 1

    .prologue
    .line 325
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Di:Z

    return v0
.end method

.method public ld()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lorg/htmlcleaner/f;->Dj:Z

    return v0
.end method

.method public le()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lorg/htmlcleaner/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    iget-object v0, p0, Lorg/htmlcleaner/f;->CL:Ljava/util/Set;

    return-object v0
.end method

.method public lf()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lorg/htmlcleaner/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401
    iget-object v0, p0, Lorg/htmlcleaner/f;->CN:Ljava/util/Set;

    return-object v0
.end method

.method public lg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lorg/htmlcleaner/f;->De:Ljava/lang/String;

    return-object v0
.end method

.method public li()Lorg/htmlcleaner/g;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dp:Lorg/htmlcleaner/g;

    return-object v0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 460
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->CQ:Z

    .line 461
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->CR:Z

    .line 462
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->CS:Z

    .line 463
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->CT:Z

    .line 464
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->CU:Z

    .line 465
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->CV:Z

    .line 466
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->CW:Z

    .line 467
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->CY:Z

    .line 468
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->CX:Z

    .line 469
    sget-object v0, Lorg/htmlcleaner/OptionalOutput;->Ex:Lorg/htmlcleaner/OptionalOutput;

    iput-object v0, p0, Lorg/htmlcleaner/f;->CZ:Lorg/htmlcleaner/OptionalOutput;

    .line 470
    sget-object v0, Lorg/htmlcleaner/OptionalOutput;->Ex:Lorg/htmlcleaner/OptionalOutput;

    iput-object v0, p0, Lorg/htmlcleaner/f;->Da:Lorg/htmlcleaner/OptionalOutput;

    .line 471
    sget-object v0, Lorg/htmlcleaner/OptionalOutput;->Ex:Lorg/htmlcleaner/OptionalOutput;

    iput-object v0, p0, Lorg/htmlcleaner/f;->Db:Lorg/htmlcleaner/OptionalOutput;

    .line 472
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->Dc:Z

    .line 473
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->Dd:Z

    .line 474
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->Dg:Z

    .line 475
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->Df:Z

    .line 476
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->Dh:Z

    .line 477
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->Dk:Z

    .line 478
    iput-boolean v1, p0, Lorg/htmlcleaner/f;->Dl:Z

    .line 479
    const-string v0, "="

    iput-object v0, p0, Lorg/htmlcleaner/f;->Dm:Ljava/lang/String;

    .line 480
    invoke-virtual {p0, v3}, Lorg/htmlcleaner/f;->bV(Ljava/lang/String;)V

    .line 481
    invoke-virtual {p0, v3}, Lorg/htmlcleaner/f;->bW(Ljava/lang/String;)V

    .line 482
    const-string v0, "self"

    iput-object v0, p0, Lorg/htmlcleaner/f;->De:Ljava/lang/String;

    .line 483
    const-string v0, "UTF-8"

    iput-object v0, p0, Lorg/htmlcleaner/f;->Dr:Ljava/lang/String;

    .line 484
    iget-object v0, p0, Lorg/htmlcleaner/f;->Dp:Lorg/htmlcleaner/g;

    invoke-virtual {v0}, Lorg/htmlcleaner/g;->clear()V

    .line 485
    invoke-direct {p0}, Lorg/htmlcleaner/f;->lh()V

    .line 486
    sget-object v0, Lorg/htmlcleaner/j;->DE:Lorg/htmlcleaner/j;

    iput-object v0, p0, Lorg/htmlcleaner/f;->CP:Lorg/htmlcleaner/t;

    .line 487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/f;->Dq:Ljava/util/List;

    .line 488
    iput-boolean v2, p0, Lorg/htmlcleaner/f;->Di:Z

    .line 489
    return-void
.end method
