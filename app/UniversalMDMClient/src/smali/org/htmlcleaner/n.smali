.class Lorg/htmlcleaner/n;
.super Ljava/lang/Object;
.source "HtmlCleaner.java"


# instance fields
.field private DV:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lorg/htmlcleaner/q;",
            ">;"
        }
    .end annotation
.end field

.field private DW:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lorg/htmlcleaner/q;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic DX:Lorg/htmlcleaner/m;


# direct methods
.method private constructor <init>(Lorg/htmlcleaner/m;)V
    .locals 1

    .prologue
    .line 131
    iput-object p1, p0, Lorg/htmlcleaner/n;->DX:Lorg/htmlcleaner/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/n;->DV:Ljava/util/Stack;

    .line 133
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/n;->DW:Ljava/util/Stack;

    return-void
.end method

.method synthetic constructor <init>(Lorg/htmlcleaner/m;Lorg/htmlcleaner/m$1;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lorg/htmlcleaner/n;-><init>(Lorg/htmlcleaner/m;)V

    return-void
.end method

.method static synthetic a(Lorg/htmlcleaner/n;)Ljava/util/Stack;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/htmlcleaner/n;->DV:Ljava/util/Stack;

    return-object v0
.end method


# virtual methods
.method public a(Lorg/htmlcleaner/q;Lorg/htmlcleaner/q;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/htmlcleaner/n;->DV:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v0, p0, Lorg/htmlcleaner/n;->DW:Ljava/util/Stack;

    invoke-virtual {v0, p2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 144
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/htmlcleaner/n;->DV:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public ln()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lorg/htmlcleaner/n;->DW:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    invoke-static {v0}, Lorg/htmlcleaner/q;->a(Lorg/htmlcleaner/q;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public lo()Lorg/htmlcleaner/q;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lorg/htmlcleaner/n;->DW:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 164
    iget-object v0, p0, Lorg/htmlcleaner/n;->DV:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    return-object v0
.end method

.method public lp()I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lorg/htmlcleaner/n;->DW:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/n;->DW:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/q;

    invoke-static {v0}, Lorg/htmlcleaner/q;->b(Lorg/htmlcleaner/q;)I

    move-result v0

    goto :goto_0
.end method
