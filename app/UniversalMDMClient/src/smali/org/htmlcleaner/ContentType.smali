.class public final enum Lorg/htmlcleaner/ContentType;
.super Ljava/lang/Enum;
.source "ContentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/htmlcleaner/ContentType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DA:Lorg/htmlcleaner/ContentType;

.field public static final enum DB:Lorg/htmlcleaner/ContentType;

.field private static final synthetic DC:[Lorg/htmlcleaner/ContentType;

.field public static final enum Dz:Lorg/htmlcleaner/ContentType;


# instance fields
.field private final dbCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lorg/htmlcleaner/ContentType;

    const-string v1, "all"

    const-string v2, "all"

    invoke-direct {v0, v1, v3, v2}, Lorg/htmlcleaner/ContentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    .line 46
    new-instance v0, Lorg/htmlcleaner/ContentType;

    const-string v1, "none"

    const-string v2, "none"

    invoke-direct {v0, v1, v4, v2}, Lorg/htmlcleaner/ContentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    .line 47
    new-instance v0, Lorg/htmlcleaner/ContentType;

    const-string v1, "text"

    const-string v2, "text"

    invoke-direct {v0, v1, v5, v2}, Lorg/htmlcleaner/ContentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/htmlcleaner/ContentType;

    sget-object v1, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    aput-object v1, v0, v5

    sput-object v0, Lorg/htmlcleaner/ContentType;->DC:[Lorg/htmlcleaner/ContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lorg/htmlcleaner/ContentType;->dbCode:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/htmlcleaner/ContentType;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/htmlcleaner/ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/ContentType;

    return-object v0
.end method

.method public static values()[Lorg/htmlcleaner/ContentType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lorg/htmlcleaner/ContentType;->DC:[Lorg/htmlcleaner/ContentType;

    invoke-virtual {v0}, [Lorg/htmlcleaner/ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/htmlcleaner/ContentType;

    return-object v0
.end method
