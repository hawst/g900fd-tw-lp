.class public Lorg/htmlcleaner/w;
.super Ljava/lang/Object;
.source "SpecialEntity.java"


# instance fields
.field private final EH:I

.field private final EI:Ljava/lang/String;

.field private EJ:Z

.field private final EK:Ljava/lang/String;

.field private final key:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/htmlcleaner/w;->key:Ljava/lang/String;

    .line 58
    iput p2, p0, Lorg/htmlcleaner/w;->EH:I

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    if-eqz p3, :cond_0

    .line 61
    iput-object p3, p0, Lorg/htmlcleaner/w;->EI:Ljava/lang/String;

    .line 65
    :goto_0
    if-eqz p4, :cond_1

    .line 66
    iget v0, p0, Lorg/htmlcleaner/w;->EH:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlcleaner/w;->EK:Ljava/lang/String;

    .line 70
    :goto_1
    iput-boolean p4, p0, Lorg/htmlcleaner/w;->EJ:Z

    .line 71
    return-void

    .line 63
    :cond_0
    iput-object v0, p0, Lorg/htmlcleaner/w;->EI:Ljava/lang/String;

    goto :goto_0

    .line 68
    :cond_1
    iput-object v0, p0, Lorg/htmlcleaner/w;->EK:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public L(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/htmlcleaner/w;->lU()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/htmlcleaner/w;->lV()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/htmlcleaner/w;->key:Ljava/lang/String;

    return-object v0
.end method

.method public intValue()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lorg/htmlcleaner/w;->EH:I

    return v0
.end method

.method public lU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/htmlcleaner/w;->EI:Ljava/lang/String;

    return-object v0
.end method

.method public lV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/htmlcleaner/w;->EK:Ljava/lang/String;

    return-object v0
.end method

.method public lW()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lorg/htmlcleaner/w;->EJ:Z

    return v0
.end method

.method public lX()Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/htmlcleaner/w;->EH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public lY()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&#x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/htmlcleaner/w;->EH:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
