.class public Lorg/htmlcleaner/y;
.super Lorg/htmlcleaner/z;
.source "TagNode.java"

# interfaces
.implements Lorg/htmlcleaner/r;


# instance fields
.field private Fb:Lorg/htmlcleaner/y;

.field private final Fc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Fd:Lorg/htmlcleaner/k;

.field private Fe:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;"
        }
    .end annotation
.end field

.field private Ff:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private transient Fg:Z

.field private Fh:Z

.field private Fi:Z

.field private Fj:Z

.field private Fk:Z

.field private final Fl:Z

.field private final children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/htmlcleaner/y;-><init>(Ljava/lang/String;Z)V

    .line 105
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lorg/htmlcleaner/z;-><init>(Ljava/lang/String;)V

    .line 58
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/htmlcleaner/y;->Fj:Z

    .line 109
    iput-boolean p2, p0, Lorg/htmlcleaner/y;->Fl:Z

    .line 110
    return-void
.end method

.method private a(Lorg/htmlcleaner/a/a;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/htmlcleaner/a/a;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 480
    if-nez p1, :cond_0

    move-object v0, v1

    .line 499
    :goto_0
    return-object v0

    .line 484
    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 485
    instance-of v3, v0, Lorg/htmlcleaner/y;

    if-eqz v3, :cond_1

    .line 486
    check-cast v0, Lorg/htmlcleaner/y;

    .line 487
    invoke-interface {p1, v0}, Lorg/htmlcleaner/a/a;->c(Lorg/htmlcleaner/y;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 488
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 490
    :cond_2
    if-eqz p2, :cond_1

    .line 491
    invoke-direct {v0, p1, p2}, Lorg/htmlcleaner/y;->a(Lorg/htmlcleaner/a/a;Z)Ljava/util/List;

    move-result-object v0

    .line 492
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 493
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 499
    goto :goto_0
.end method

.method private b(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 225
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 226
    return-void
.end method

.method private mv()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 839
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 840
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 841
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 843
    :cond_0
    return-object v1
.end method


# virtual methods
.method public D(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 415
    if-nez p1, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 419
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lorg/htmlcleaner/y;->c(Ljava/util/List;)V

    goto :goto_0

    .line 420
    :cond_2
    instance-of v0, p1, Lorg/htmlcleaner/u;

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    check-cast p1, Lorg/htmlcleaner/u;

    invoke-virtual {p1}, Lorg/htmlcleaner/u;->lS()Lorg/htmlcleaner/b;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 422
    :cond_3
    instance-of v0, p1, Lorg/htmlcleaner/b;

    if-eqz v0, :cond_4

    .line 423
    iget-object v1, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    move-object v0, p1

    check-cast v0, Lorg/htmlcleaner/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    instance-of v0, p1, Lorg/htmlcleaner/y;

    if-eqz v0, :cond_0

    .line 425
    check-cast p1, Lorg/htmlcleaner/y;

    .line 426
    iput-object p0, p1, Lorg/htmlcleaner/y;->Fb:Lorg/htmlcleaner/y;

    goto :goto_0

    .line 429
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempted to add invalid child object to TagNode; class="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public E(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method F(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 628
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fe:Ljava/util/List;

    if-nez v0, :cond_0

    .line 629
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/y;->Fe:Ljava/util/List;

    .line 631
    :cond_0
    instance-of v0, p1, Lorg/htmlcleaner/b;

    if-eqz v0, :cond_1

    .line 632
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fe:Ljava/util/List;

    check-cast p1, Lorg/htmlcleaner/b;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    return-void

    .line 634
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempt to add invalid item for moving; class="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public H(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 255
    if-eqz p1, :cond_0

    .line 256
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 257
    iget-boolean v1, p0, Lorg/htmlcleaner/y;->Fi:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lorg/htmlcleaner/y;->Fj:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 258
    :goto_0
    if-nez p2, :cond_1

    const-string v0, ""

    .line 259
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    iget-object v2, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    :cond_0
    return-void

    .line 258
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\p{Cntrl}"

    const-string v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public I(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lorg/htmlcleaner/y;->Ff:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 714
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/y;->Ff:Ljava/util/Map;

    .line 716
    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/y;->Ff:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 717
    return-void
.end method

.method public M(Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<+",
            "Lorg/htmlcleaner/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 530
    new-instance v0, Lorg/htmlcleaner/a/b;

    invoke-direct {v0}, Lorg/htmlcleaner/a/b;-><init>()V

    invoke-virtual {p0, v0, p1}, Lorg/htmlcleaner/y;->b(Lorg/htmlcleaner/a/a;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method N(Z)V
    .locals 0

    .prologue
    .line 652
    iput-boolean p1, p0, Lorg/htmlcleaner/y;->Fg:Z

    .line 653
    return-void
.end method

.method public O(Z)V
    .locals 0

    .prologue
    .line 663
    iput-boolean p1, p0, Lorg/htmlcleaner/y;->Fh:Z

    .line 664
    return-void
.end method

.method public P(Z)V
    .locals 0

    .prologue
    .line 681
    iput-boolean p1, p0, Lorg/htmlcleaner/y;->Fk:Z

    .line 682
    return-void
.end method

.method public Q(Z)V
    .locals 1

    .prologue
    .line 821
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/htmlcleaner/y;->Fj:Z

    .line 822
    iput-boolean p1, p0, Lorg/htmlcleaner/y;->Fi:Z

    .line 828
    if-nez p1, :cond_0

    .line 829
    invoke-virtual {p0}, Lorg/htmlcleaner/y;->ml()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/htmlcleaner/y;->b(Ljava/util/Map;)V

    .line 831
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<+",
            "Lorg/htmlcleaner/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542
    new-instance v0, Lorg/htmlcleaner/a/d;

    invoke-direct {v0, p1}, Lorg/htmlcleaner/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lorg/htmlcleaner/y;->b(Lorg/htmlcleaner/a/a;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    iget-boolean v0, p0, Lorg/htmlcleaner/y;->Fj:Z

    if-eqz v0, :cond_0

    .line 177
    invoke-direct {p0, p1}, Lorg/htmlcleaner/y;->b(Ljava/util/Map;)V

    .line 215
    :goto_0
    return-void

    .line 189
    :cond_0
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 195
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 198
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 204
    iget-boolean v2, p0, Lorg/htmlcleaner/y;->Fj:Z

    if-nez v2, :cond_1

    .line 205
    iget-object v2, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v3, v0

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 206
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    :goto_3
    move-object v3, v2

    .line 207
    goto :goto_2

    :cond_1
    move-object v3, v0

    .line 211
    :cond_2
    invoke-virtual {v4, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 213
    :cond_3
    invoke-direct {p0, v4}, Lorg/htmlcleaner/y;->b(Ljava/util/Map;)V

    goto :goto_0

    :cond_4
    move-object v2, v3

    goto :goto_3
.end method

.method public a(Lorg/htmlcleaner/k;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lorg/htmlcleaner/y;->Fd:Lorg/htmlcleaner/k;

    .line 412
    return-void
.end method

.method public b(Lorg/htmlcleaner/a/a;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/htmlcleaner/a/a;",
            "Z)",
            "Ljava/util/List",
            "<+",
            "Lorg/htmlcleaner/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 510
    invoke-direct {p0, p1, p2}, Lorg/htmlcleaner/y;->a(Lorg/htmlcleaner/a/a;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/htmlcleaner/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 439
    if-eqz p1, :cond_0

    .line 440
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 441
    invoke-virtual {p0, v0}, Lorg/htmlcleaner/y;->D(Ljava/lang/Object;)V

    goto :goto_0

    .line 444
    :cond_0
    return-void
.end method

.method public cv(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 134
    if-nez p1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public cw(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 598
    new-instance v0, Lorg/htmlcleaner/ac;

    invoke-direct {v0, p1}, Lorg/htmlcleaner/ac;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lorg/htmlcleaner/ac;->b(Lorg/htmlcleaner/y;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 644
    iput-object p1, p0, Lorg/htmlcleaner/y;->Fe:Ljava/util/List;

    .line 645
    return-void
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lorg/htmlcleaner/y;->Fi:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lorg/htmlcleaner/y;->name:Ljava/lang/String;

    .line 124
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/y;->name:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/htmlcleaner/y;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 334
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 335
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 336
    instance-of v3, v0, Lorg/htmlcleaner/i;

    if-eqz v3, :cond_1

    .line 337
    check-cast v0, Lorg/htmlcleaner/i;

    invoke-virtual {v0}, Lorg/htmlcleaner/i;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 338
    :cond_1
    instance-of v3, v0, Lorg/htmlcleaner/y;

    if-eqz v3, :cond_0

    .line 339
    check-cast v0, Lorg/htmlcleaner/y;

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 340
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 344
    :cond_2
    return-object v1
.end method

.method public hasAttribute(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 235
    if-nez p1, :cond_0

    move v0, v1

    .line 244
    :goto_0
    return v0

    .line 240
    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 241
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 244
    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 685
    invoke-virtual {p0}, Lorg/htmlcleaner/y;->ms()Z

    move-result v0

    if-nez v0, :cond_4

    .line 686
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 687
    instance-of v3, v0, Lorg/htmlcleaner/y;

    if-eqz v3, :cond_1

    .line 688
    check-cast v0, Lorg/htmlcleaner/y;

    invoke-virtual {v0}, Lorg/htmlcleaner/y;->ms()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 703
    :goto_0
    return v0

    .line 691
    :cond_1
    instance-of v3, v0, Lorg/htmlcleaner/i;

    if-eqz v3, :cond_2

    .line 692
    check-cast v0, Lorg/htmlcleaner/i;

    invoke-virtual {v0}, Lorg/htmlcleaner/i;->lk()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 693
    goto :goto_0

    .line 695
    :cond_2
    instance-of v0, v0, Lorg/htmlcleaner/h;

    if-eqz v0, :cond_3

    move v0, v1

    .line 697
    goto :goto_0

    :cond_3
    move v0, v1

    .line 699
    goto :goto_0

    .line 703
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public lQ()Lorg/htmlcleaner/y;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fb:Lorg/htmlcleaner/y;

    return-object v0
.end method

.method public lR()Z
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fb:Lorg/htmlcleaner/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/htmlcleaner/y;->Fb:Lorg/htmlcleaner/y;

    invoke-virtual {v0, p0}, Lorg/htmlcleaner/y;->E(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ml()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0}, Lorg/htmlcleaner/y;->mv()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public mm()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lorg/htmlcleaner/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    return-object v0
.end method

.method public mn()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 300
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 301
    iget-object v0, p0, Lorg/htmlcleaner/y;->children:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 302
    instance-of v3, v0, Lorg/htmlcleaner/y;

    if-eqz v3, :cond_0

    .line 303
    check-cast v0, Lorg/htmlcleaner/y;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 307
    :cond_1
    return-object v1
.end method

.method mo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lorg/htmlcleaner/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 640
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fe:Ljava/util/List;

    return-object v0
.end method

.method mp()Z
    .locals 1

    .prologue
    .line 648
    iget-boolean v0, p0, Lorg/htmlcleaner/y;->Fg:Z

    return v0
.end method

.method mq()V
    .locals 1

    .prologue
    .line 656
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/htmlcleaner/y;->N(Z)V

    .line 657
    return-void
.end method

.method public mr()Z
    .locals 1

    .prologue
    .line 670
    iget-boolean v0, p0, Lorg/htmlcleaner/y;->Fh:Z

    return v0
.end method

.method public ms()Z
    .locals 1

    .prologue
    .line 677
    iget-boolean v0, p0, Lorg/htmlcleaner/y;->Fk:Z

    return v0
.end method

.method public mt()Lorg/htmlcleaner/y;
    .locals 3

    .prologue
    .line 765
    new-instance v0, Lorg/htmlcleaner/y;

    iget-object v1, p0, Lorg/htmlcleaner/y;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lorg/htmlcleaner/y;-><init>(Ljava/lang/String;Z)V

    .line 766
    iget-object v1, v0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    iget-object v2, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 767
    return-object v0
.end method

.method public mu()Z
    .locals 1

    .prologue
    .line 771
    iget-boolean v0, p0, Lorg/htmlcleaner/y;->Fl:Z

    return v0
.end method

.method public removeAttribute(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 271
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/htmlcleaner/y;->Fc:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    :cond_0
    return-void
.end method
