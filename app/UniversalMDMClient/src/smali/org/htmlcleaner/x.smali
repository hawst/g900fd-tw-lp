.class public Lorg/htmlcleaner/x;
.super Ljava/lang/Object;
.source "TagInfo.java"


# instance fields
.field private EL:Lorg/htmlcleaner/ContentType;

.field private EM:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private EN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private EO:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private EP:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private EQ:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ER:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ES:Lorg/htmlcleaner/BelongsTo;

.field private ET:Ljava/lang/String;

.field private EU:Ljava/lang/String;

.field private EV:Z

.field private EW:Z

.field private EX:Z

.field private EY:Lorg/htmlcleaner/CloseTag;

.field private EZ:Lorg/htmlcleaner/Display;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/x;->EM:Ljava/util/Set;

    .line 109
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/x;->EN:Ljava/util/Set;

    .line 110
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/x;->EO:Ljava/util/Set;

    .line 111
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/x;->EP:Ljava/util/Set;

    .line 112
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/x;->EQ:Ljava/util/Set;

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/x;->ER:Ljava/util/Set;

    .line 114
    sget-object v0, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    iput-object v0, p0, Lorg/htmlcleaner/x;->ES:Lorg/htmlcleaner/BelongsTo;

    .line 124
    iput-object p1, p0, Lorg/htmlcleaner/x;->name:Ljava/lang/String;

    .line 125
    iput-object p2, p0, Lorg/htmlcleaner/x;->EL:Lorg/htmlcleaner/ContentType;

    .line 126
    iput-object p3, p0, Lorg/htmlcleaner/x;->ES:Lorg/htmlcleaner/BelongsTo;

    .line 127
    iput-boolean p4, p0, Lorg/htmlcleaner/x;->EV:Z

    .line 128
    iput-boolean p5, p0, Lorg/htmlcleaner/x;->EW:Z

    .line 129
    iput-boolean p6, p0, Lorg/htmlcleaner/x;->EX:Z

    .line 130
    iput-object p7, p0, Lorg/htmlcleaner/x;->EY:Lorg/htmlcleaner/CloseTag;

    .line 131
    iput-object p8, p0, Lorg/htmlcleaner/x;->EZ:Lorg/htmlcleaner/Display;

    .line 132
    return-void
.end method


# virtual methods
.method a(Lorg/htmlcleaner/x;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 359
    if-eqz p1, :cond_1

    .line 360
    iget-object v1, p0, Lorg/htmlcleaner/x;->EM:Ljava/util/Set;

    invoke-virtual {p1}, Lorg/htmlcleaner/x;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lorg/htmlcleaner/x;->EL:Lorg/htmlcleaner/ContentType;

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 363
    :cond_1
    return v0
.end method

.method b(Lorg/htmlcleaner/b;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 372
    iget-object v0, p0, Lorg/htmlcleaner/x;->EL:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    if-eq v0, v3, :cond_1

    instance-of v0, p1, Lorg/htmlcleaner/z;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 373
    check-cast v0, Lorg/htmlcleaner/z;

    .line 374
    invoke-virtual {v0}, Lorg/htmlcleaner/z;->getName()Ljava/lang/String;

    move-result-object v0

    .line 375
    const-string v3, "script"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    :cond_0
    :goto_0
    return v1

    .line 380
    :cond_1
    sget-object v0, Lorg/htmlcleaner/x$1;->Fa:[I

    iget-object v3, p0, Lorg/htmlcleaner/x;->EL:Lorg/htmlcleaner/ContentType;

    invoke-virtual {v3}, Lorg/htmlcleaner/ContentType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_2
    move v1, v2

    .line 403
    goto :goto_0

    .line 382
    :pswitch_0
    iget-object v0, p0, Lorg/htmlcleaner/x;->EO:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 383
    instance-of v0, p1, Lorg/htmlcleaner/z;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lorg/htmlcleaner/x;->EO:Ljava/util/Set;

    check-cast p1, Lorg/htmlcleaner/z;

    invoke-virtual {p1}, Lorg/htmlcleaner/z;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 386
    :cond_3
    iget-object v0, p0, Lorg/htmlcleaner/x;->EP:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    instance-of v0, p1, Lorg/htmlcleaner/z;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lorg/htmlcleaner/x;->EP:Ljava/util/Set;

    check-cast p1, Lorg/htmlcleaner/z;

    invoke-virtual {p1}, Lorg/htmlcleaner/z;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    .line 393
    :pswitch_1
    instance-of v0, p1, Lorg/htmlcleaner/z;

    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0

    .line 395
    :pswitch_2
    instance-of v0, p1, Lorg/htmlcleaner/i;

    if-eqz v0, :cond_5

    .line 397
    check-cast p1, Lorg/htmlcleaner/i;

    invoke-virtual {p1}, Lorg/htmlcleaner/i;->lk()Z

    move-result v1

    goto :goto_0

    .line 398
    :cond_5
    instance-of v0, p1, Lorg/htmlcleaner/z;

    if-nez v0, :cond_2

    goto :goto_0

    .line 380
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public ck(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 135
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 138
    iput-object v1, p0, Lorg/htmlcleaner/x;->EU:Ljava/lang/String;

    .line 139
    iget-object v2, p0, Lorg/htmlcleaner/x;->EN:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    :cond_0
    return-void
.end method

.method public cl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 144
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 147
    iput-object v1, p0, Lorg/htmlcleaner/x;->ET:Ljava/lang/String;

    .line 148
    iget-object v2, p0, Lorg/htmlcleaner/x;->EN:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    :cond_0
    return-void
.end method

.method public cm(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 153
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 156
    iget-object v2, p0, Lorg/htmlcleaner/x;->EP:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 158
    :cond_0
    return-void
.end method

.method public cn(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 161
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 164
    iget-object v2, p0, Lorg/htmlcleaner/x;->EO:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 166
    :cond_0
    return-void
.end method

.method public co(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 169
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 172
    iget-object v2, p0, Lorg/htmlcleaner/x;->EN:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_0
    return-void
.end method

.method public cp(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 177
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 180
    iget-object v2, p0, Lorg/htmlcleaner/x;->EQ:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v2, p0, Lorg/htmlcleaner/x;->EM:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_0
    return-void
.end method

.method public cq(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 186
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 189
    iget-object v2, p0, Lorg/htmlcleaner/x;->ER:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method public cr(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 194
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 197
    iget-object v2, p0, Lorg/htmlcleaner/x;->EM:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :cond_0
    return-void
.end method

.method cs(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lorg/htmlcleaner/x;->EN:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method ct(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lorg/htmlcleaner/x;->EQ:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method cu(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lorg/htmlcleaner/x;->ER:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lorg/htmlcleaner/x;->name:Ljava/lang/String;

    return-object v0
.end method

.method public lZ()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lorg/htmlcleaner/x;->EP:Ljava/util/Set;

    return-object v0
.end method

.method public ma()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lorg/htmlcleaner/x;->ET:Ljava/lang/String;

    return-object v0
.end method

.method public mb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lorg/htmlcleaner/x;->EU:Ljava/lang/String;

    return-object v0
.end method

.method public mc()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lorg/htmlcleaner/x;->EV:Z

    return v0
.end method

.method public md()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lorg/htmlcleaner/x;->EW:Z

    return v0
.end method

.method public me()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lorg/htmlcleaner/x;->EX:Z

    return v0
.end method

.method mf()Z
    .locals 2

    .prologue
    .line 327
    sget-object v0, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    iget-object v1, p0, Lorg/htmlcleaner/x;->EL:Lorg/htmlcleaner/ContentType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mg()Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lorg/htmlcleaner/x;->EQ:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mh()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lorg/htmlcleaner/x;->EP:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mi()Z
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lorg/htmlcleaner/x;->ES:Lorg/htmlcleaner/BelongsTo;

    sget-object v1, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mj()Z
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lorg/htmlcleaner/x;->ES:Lorg/htmlcleaner/BelongsTo;

    sget-object v1, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/htmlcleaner/x;->ES:Lorg/htmlcleaner/BelongsTo;

    sget-object v1, Lorg/htmlcleaner/BelongsTo;->Cw:Lorg/htmlcleaner/BelongsTo;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mk()Z
    .locals 2

    .prologue
    .line 408
    sget-object v0, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    iget-object v1, p0, Lorg/htmlcleaner/x;->EL:Lorg/htmlcleaner/ContentType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/htmlcleaner/x;->EO:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
