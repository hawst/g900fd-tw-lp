.class public Lorg/htmlcleaner/j;
.super Ljava/lang/Object;
.source "DefaultTagProvider.java"

# interfaces
.implements Lorg/htmlcleaner/t;


# static fields
.field public static final DE:Lorg/htmlcleaner/j;


# instance fields
.field private DD:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/htmlcleaner/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lorg/htmlcleaner/j;

    invoke-direct {v0}, Lorg/htmlcleaner/j;-><init>()V

    sput-object v0, Lorg/htmlcleaner/j;->DE:Lorg/htmlcleaner/j;

    return-void
.end method

.method public constructor <init>()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/j;->DD:Ljava/util/concurrent/ConcurrentMap;

    .line 82
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "div"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 83
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 84
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 85
    const-string v1, "div"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 92
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "aside"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 93
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 94
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 95
    const-string v1, "aside"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 97
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "section"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 98
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 99
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 100
    const-string v1, "section"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 102
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "article"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 103
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 104
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 105
    const-string v1, "article"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 107
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "main"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 108
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 109
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 110
    const-string v1, "main"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 112
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "nav"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 113
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 114
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 115
    const-string v1, "nav"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 117
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "details"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 118
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 119
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 120
    const-string v1, "details"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 121
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "summary"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 122
    const-string v1, "details"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 123
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 124
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 125
    const-string v1, "summary"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 127
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "figure"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 128
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 129
    const-string v1, "p"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 130
    const-string v1, "figure"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 131
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "figcaption"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 132
    const-string v1, "figure"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 133
    const-string v1, "figcaption"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 136
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "header"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 137
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 138
    const-string v1, "p,header,footer,main"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 139
    const-string v1, "header"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 141
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "footer"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 142
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 143
    const-string v1, "p,header,footer,main"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 144
    const-string v1, "footer"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 149
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "mark"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 150
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 151
    const-string v1, "mark"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 153
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "bdi"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 154
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 155
    const-string v1, "bdi"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 157
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "time"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 158
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 159
    const-string v1, "time"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 161
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "meter"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 162
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 163
    const-string v1, "meter"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 164
    const-string v1, "meter"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 170
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "ruby"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 171
    const-string v1, "rt,rp"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 172
    const-string v1, "ruby"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 174
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "rt"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 175
    const-string v1, "ruby"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 176
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 177
    const-string v1, "rt"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 179
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "rp"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 180
    const-string v1, "ruby"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 181
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 182
    const-string v1, "rp"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 187
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "audio"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 188
    const-string v1, "audio,video"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 189
    const-string v1, "audio"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 191
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "video"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 192
    const-string v1, "audio,video"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 193
    const-string v1, "video"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 195
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "source"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 196
    const-string v1, "audio,video"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 197
    const-string v1, "source"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 199
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "track"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 200
    const-string v1, "audio,video"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 201
    const-string v1, "track"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 203
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "canvas"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 204
    const-string v1, "canvas"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 209
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "dialog"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 210
    const-string v1, "dialog"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 212
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "progress"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 213
    const-string v1, "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 214
    const-string v1, "progress"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 215
    const-string v1, "progress"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 221
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "span"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 222
    const-string v1, "span"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 224
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "meta"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 225
    const-string v1, "meta"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 227
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "link"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 228
    const-string v1, "link"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 230
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "title"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v9

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 231
    const-string v1, "title"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 233
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "style"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 234
    const-string v1, "style"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 236
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "bgsound"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 237
    const-string v1, "bgsound"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 239
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "h1"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 240
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 241
    const-string v1, "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 242
    const-string v1, "h1"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 244
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "h2"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 245
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 246
    const-string v1, "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 247
    const-string v1, "h2"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 249
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "h3"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 250
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 251
    const-string v1, "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 252
    const-string v1, "h3"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 254
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "h4"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 255
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 256
    const-string v1, "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 257
    const-string v1, "h4"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 259
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "h5"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 260
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 261
    const-string v1, "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 262
    const-string v1, "h5"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 264
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "h6"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 265
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 266
    const-string v1, "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 267
    const-string v1, "h6"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 270
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "p"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 271
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 272
    const-string v1, "p,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 273
    const-string v1, "p"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 275
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "strong"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 276
    const-string v1, "strong"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 278
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "em"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 279
    const-string v1, "em"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 281
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "abbr"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 282
    const-string v1, "abbr"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 284
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "acronym"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 285
    const-string v1, "acronym"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 287
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "address"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 288
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 289
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 290
    const-string v1, "address"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 292
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "bdo"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 293
    const-string v1, "bdo"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 295
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "blockquote"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 296
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 297
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 298
    const-string v1, "blockquote"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 300
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "cite"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 301
    const-string v1, "cite"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 303
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "q"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 304
    const-string v1, "q"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 306
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "code"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 307
    const-string v1, "code"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 309
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "ins"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 310
    const-string v1, "ins"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 312
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "del"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 313
    const-string v1, "del"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 315
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "dfn"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 316
    const-string v1, "dfn"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 318
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "kbd"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 319
    const-string v1, "kbd"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 321
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "pre"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 322
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 323
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 324
    const-string v1, "pre"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 326
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "samp"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 327
    const-string v1, "samp"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 329
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "listing"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 330
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 331
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 332
    const-string v1, "listing"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 334
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "var"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 335
    const-string v1, "var"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 337
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "br"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 338
    const-string v1, "br"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 340
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "wbr"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 341
    const-string v1, "wbr"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 343
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "nobr"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 344
    const-string v1, "nobr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 345
    const-string v1, "nobr"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 347
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "xmp"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 348
    const-string v1, "xmp"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 350
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "a"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 351
    const-string v1, "a"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 352
    const-string v1, "a"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 354
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "base"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cx:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 355
    const-string v1, "base"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 357
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "img"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 358
    const-string v1, "img"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 360
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "area"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 361
    const-string v1, "map"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 362
    const-string v1, "area"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 363
    const-string v1, "area"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 365
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "map"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 366
    const-string v1, "map"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 367
    const-string v1, "map"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 369
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "object"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 370
    const-string v1, "object"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 372
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "param"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 373
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 374
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 375
    const-string v1, "param"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 377
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "applet"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 378
    const-string v0, "applet"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 380
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "xml"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 381
    const-string v1, "xml"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 383
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "ul"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 384
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 385
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 386
    const-string v1, "ul"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 388
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "ol"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 389
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 390
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 391
    const-string v1, "ol"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 393
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "li"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 394
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 395
    const-string v1, "li,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 396
    const-string v1, "li"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 398
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "dl"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 399
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 400
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 401
    const-string v1, "dl"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 403
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "dt"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 404
    const-string v1, "dt,dd"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 405
    const-string v1, "dt"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 407
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "dd"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 408
    const-string v1, "dt,dd"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 409
    const-string v1, "dd"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 411
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "menu"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 412
    const-string v0, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 413
    const-string v0, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 414
    const-string v0, "menu"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 416
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "dir"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 417
    const-string v0, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 418
    const-string v0, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 419
    const-string v0, "dir"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 421
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "table"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 422
    const-string v1, "tr,tbody,thead,tfoot,colgroup,caption"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 423
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 424
    const-string v1, "tr,thead,tbody,tfoot,caption,colgroup,table,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 425
    const-string v1, "table"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 427
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "tr"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 428
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 429
    const-string v1, "tbody"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 430
    const-string v1, "td,th"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 431
    const-string v1, "thead,tfoot"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->co(Ljava/lang/String;)V

    .line 432
    const-string v1, "tr,td,th,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 433
    const-string v1, "tr"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 436
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "td"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 437
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 438
    const-string v1, "tr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 439
    const-string v1, "td,th,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 440
    const-string v1, "td"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 442
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "th"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 443
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 444
    const-string v1, "tr"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cl(Ljava/lang/String;)V

    .line 445
    const-string v1, "td,th,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 446
    const-string v1, "th"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 448
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "tbody"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 449
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 450
    const-string v1, "tr,form"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 451
    const-string v1, "td,th,tr,tbody,thead,tfoot,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 452
    const-string v1, "tbody"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 454
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "thead"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 455
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 456
    const-string v1, "tr,form"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 457
    const-string v1, "td,th,tr,tbody,thead,tfoot,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 458
    const-string v1, "thead"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 460
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "tfoot"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 461
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 462
    const-string v1, "tr,form"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 463
    const-string v1, "td,th,tr,tbody,thead,tfoot,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 464
    const-string v1, "tfoot"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 466
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "col"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 467
    const-string v1, "colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 468
    const-string v1, "col"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 470
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "colgroup"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 471
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 472
    const-string v1, "col"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 473
    const-string v1, "td,th,tr,tbody,thead,tfoot,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 474
    const-string v1, "colgroup"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 476
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "caption"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 477
    const-string v1, "table"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 478
    const-string v1, "td,th,tr,tbody,thead,tfoot,caption,colgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 479
    const-string v1, "caption"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 481
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "form"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v9

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 482
    const-string v1, "form"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cm(Ljava/lang/String;)V

    .line 483
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 484
    const-string v1, "option,optgroup,textarea,select,fieldset,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 485
    const-string v1, "form"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 487
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "input"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 488
    const-string v1, "select,optgroup,option"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 489
    const-string v1, "input"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 491
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "textarea"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 492
    const-string v1, "select,optgroup,option"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 493
    const-string v1, "textarea"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 495
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "select"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v9

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 496
    const-string v1, "option,optgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 497
    const-string v1, "option,optgroup,select"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 498
    const-string v1, "select"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 500
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "option"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DB:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dv:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v9

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 501
    const-string v1, "select"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 502
    const-string v1, "option"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 503
    const-string v1, "option"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 505
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "optgroup"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v9

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 506
    const-string v1, "select"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->ck(Ljava/lang/String;)V

    .line 507
    const-string v1, "option"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cn(Ljava/lang/String;)V

    .line 508
    const-string v1, "optgroup"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 509
    const-string v1, "optgroup"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 511
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "button"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 512
    const-string v1, "select,optgroup,option"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 513
    const-string v1, "button"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 515
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "label"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 516
    const-string v1, "label"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 518
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "fieldset"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 519
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 520
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 521
    const-string v1, "fieldset"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 523
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "isindex"

    sget-object v7, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 524
    const-string v0, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 525
    const-string v0, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 526
    const-string v0, "isindex"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 528
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "script"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cw:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 529
    const-string v1, "script"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 531
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "noscript"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cw:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 532
    const-string v1, "noscript"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 534
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "b"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 535
    const-string v1, "u,i,tt,sub,sup,big,small,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 536
    const-string v1, "b"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 538
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "i"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 539
    const-string v1, "b,u,tt,sub,sup,big,small,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 540
    const-string v1, "i"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 542
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "u"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 543
    const-string v0, "b,i,tt,sub,sup,big,small,strike,blink,s"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 544
    const-string v0, "u"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 546
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "tt"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 547
    const-string v1, "b,u,i,sub,sup,big,small,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 548
    const-string v1, "tt"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 550
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "sub"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 551
    const-string v1, "b,u,i,tt,sup,big,small,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 552
    const-string v1, "sub"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 554
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "sup"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 555
    const-string v1, "b,u,i,tt,sub,big,small,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 556
    const-string v1, "sup"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 558
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "big"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 559
    const-string v1, "b,u,i,tt,sub,sup,small,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 560
    const-string v1, "big"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 562
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "small"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 563
    const-string v1, "b,u,i,tt,sub,sup,big,strike,blink,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 564
    const-string v1, "small"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 566
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "strike"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 567
    const-string v0, "b,u,i,tt,sub,sup,big,small,blink,s"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 568
    const-string v0, "strike"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 570
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "blink"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 571
    const-string v1, "b,u,i,tt,sub,sup,big,small,strike,s"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 572
    const-string v1, "blink"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 574
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "marquee"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 575
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 576
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 577
    const-string v1, "marquee"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 579
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "s"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 580
    const-string v0, "b,u,i,tt,sub,sup,big,small,strike,blink"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cq(Ljava/lang/String;)V

    .line 581
    const-string v0, "s"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 583
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "hr"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 584
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 585
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 586
    const-string v1, "hr"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 588
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "font"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DG:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 589
    const-string v0, "font"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 591
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "basefont"

    sget-object v7, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 592
    const-string v0, "basefont"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 594
    new-instance v5, Lorg/htmlcleaner/x;

    const-string v6, "center"

    sget-object v7, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v8, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v12, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v13, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v10, v4

    move v11, v4

    invoke-direct/range {v5 .. v13}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 595
    const-string v0, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 596
    const-string v0, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v5, v0}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 597
    const-string v0, "center"

    invoke-direct {p0, v0, v5}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 599
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "comment"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 600
    const-string v1, "comment"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 602
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "server"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DJ:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 603
    const-string v1, "server"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 605
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "iframe"

    sget-object v2, Lorg/htmlcleaner/ContentType;->Dz:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Du:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DI:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 606
    const-string v1, "iframe"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 608
    new-instance v0, Lorg/htmlcleaner/x;

    const-string v1, "embed"

    sget-object v2, Lorg/htmlcleaner/ContentType;->DA:Lorg/htmlcleaner/ContentType;

    sget-object v3, Lorg/htmlcleaner/BelongsTo;->Cy:Lorg/htmlcleaner/BelongsTo;

    sget-object v7, Lorg/htmlcleaner/CloseTag;->Dw:Lorg/htmlcleaner/CloseTag;

    sget-object v8, Lorg/htmlcleaner/Display;->DF:Lorg/htmlcleaner/Display;

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v8}, Lorg/htmlcleaner/x;-><init>(Ljava/lang/String;Lorg/htmlcleaner/ContentType;Lorg/htmlcleaner/BelongsTo;ZZZLorg/htmlcleaner/CloseTag;Lorg/htmlcleaner/Display;)V

    .line 609
    const-string v1, "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cp(Ljava/lang/String;)V

    .line 610
    const-string v1, "p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml"

    invoke-virtual {v0, v1}, Lorg/htmlcleaner/x;->cr(Ljava/lang/String;)V

    .line 611
    const-string v1, "embed"

    invoke-direct {p0, v1, v0}, Lorg/htmlcleaner/j;->a(Ljava/lang/String;Lorg/htmlcleaner/x;)V

    .line 612
    return-void
.end method

.method private a(Ljava/lang/String;Lorg/htmlcleaner/x;)V
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lorg/htmlcleaner/j;->DD:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    return-void
.end method


# virtual methods
.method public cb(Ljava/lang/String;)Lorg/htmlcleaner/x;
    .locals 1

    .prologue
    .line 623
    if-nez p1, :cond_0

    .line 625
    const/4 v0, 0x0

    .line 627
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/j;->DD:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/x;

    goto :goto_0
.end method
