.class public Lorg/htmlcleaner/s;
.super Ljava/lang/Object;
.source "HtmlTokenizer.java"


# instance fields
.field private DU:Lorg/htmlcleaner/g;

.field private Ed:Ljava/io/BufferedReader;

.field private Ee:[C

.field private transient Ef:I

.field private transient Eg:I

.field private transient Eh:I

.field private transient Ei:I

.field private transient Ej:Ljava/lang/StringBuffer;

.field private transient Ek:Z

.field private transient El:Lorg/htmlcleaner/k;

.field private transient Em:Lorg/htmlcleaner/z;

.field private transient En:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;"
        }
    .end annotation
.end field

.field private transient Eo:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Ep:Z

.field private Eq:Z

.field private Er:Z

.field private Es:Lorg/htmlcleaner/m;

.field private Et:Lorg/htmlcleaner/f;

.field private Eu:Lorg/htmlcleaner/e;


# direct methods
.method public constructor <init>(Lorg/htmlcleaner/m;Ljava/io/Reader;Lorg/htmlcleaner/e;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lorg/htmlcleaner/s;->Eg:I

    .line 63
    iput v2, p0, Lorg/htmlcleaner/s;->Eh:I

    .line 64
    iput v2, p0, Lorg/htmlcleaner/s;->Ei:I

    .line 67
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/htmlcleaner/s;->Eo:Ljava/util/Set;

    .line 75
    iput-boolean v2, p0, Lorg/htmlcleaner/s;->Ep:Z

    .line 92
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/htmlcleaner/s;->Ed:Ljava/io/BufferedReader;

    .line 93
    iput-object p1, p0, Lorg/htmlcleaner/s;->Es:Lorg/htmlcleaner/m;

    .line 94
    invoke-virtual {p1}, Lorg/htmlcleaner/m;->ll()Lorg/htmlcleaner/f;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    .line 95
    invoke-virtual {p1}, Lorg/htmlcleaner/m;->lm()Lorg/htmlcleaner/g;

    move-result-object v0

    iput-object v0, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    .line 96
    iput-object p3, p0, Lorg/htmlcleaner/s;->Eu:Lorg/htmlcleaner/e;

    .line 97
    return-void
.end method

.method private a(Lorg/htmlcleaner/b;)V
    .locals 4

    .prologue
    .line 100
    iget v0, p0, Lorg/htmlcleaner/s;->Eh:I

    invoke-interface {p1, v0}, Lorg/htmlcleaner/b;->bx(I)V

    .line 101
    iget v0, p0, Lorg/htmlcleaner/s;->Ei:I

    invoke-interface {p1, v0}, Lorg/htmlcleaner/b;->by(I)V

    .line 102
    iget-object v0, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lorg/htmlcleaner/s;->Es:Lorg/htmlcleaner/m;

    iget-object v1, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    iget-object v2, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    iget-object v3, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v2

    iget-object v3, p0, Lorg/htmlcleaner/s;->Eu:Lorg/htmlcleaner/e;

    invoke-virtual {v0, v1, v2, v3}, Lorg/htmlcleaner/m;->a(Ljava/util/List;Ljava/util/ListIterator;Lorg/htmlcleaner/e;)V

    .line 104
    return-void
.end method

.method private a(IC)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 209
    iget v1, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v1, :cond_1

    iget v1, p0, Lorg/htmlcleaner/s;->Eg:I

    if-lt p1, v1, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v1

    iget-object v2, p0, Lorg/htmlcleaner/s;->Ee:[C

    aget-char v2, v2, p1

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bA(I)V
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/htmlcleaner/s;->Ef:I

    .line 154
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bz(I)V

    .line 155
    return-void
.end method

.method private bB(I)Z
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    if-lt p1, v0, :cond_0

    .line 232
    const/4 v0, 0x0

    .line 236
    :goto_0
    return v0

    .line 235
    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    aget-char v0, v0, p1

    .line 236
    invoke-static {v0}, Ljava/lang/Character;->isUnicodeIdentifierStart(C)Z

    move-result v0

    goto :goto_0
.end method

.method private bC(I)V
    .locals 2

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lorg/htmlcleaner/s;->bz(I)V

    .line 306
    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    .line 307
    :goto_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v1

    if-nez v1, :cond_0

    if-lez p1, :cond_0

    .line 308
    iget-object v1, p0, Lorg/htmlcleaner/s;->Ee:[C

    aget-char v1, v1, v0

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->f(C)V

    .line 309
    add-int/lit8 v0, v0, 0x1

    .line 310
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 312
    :cond_0
    return-void
.end method

.method private bz(I)V
    .locals 9

    .prologue
    const/16 v5, 0x400

    const/16 v8, 0x20

    const/4 v3, 0x0

    .line 107
    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/2addr v0, p1

    if-lt v0, v5, :cond_6

    .line 108
    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    rsub-int v1, v0, 0x400

    .line 109
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v2, p0, Lorg/htmlcleaner/s;->Ef:I

    iget-object v4, p0, Lorg/htmlcleaner/s;->Ee:[C

    invoke-static {v0, v2, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 110
    iput v3, p0, Lorg/htmlcleaner/s;->Ef:I

    .line 112
    rsub-int v0, v1, 0x400

    move v2, v3

    move v4, v0

    move v0, v1

    .line 117
    :cond_0
    iget-object v6, p0, Lorg/htmlcleaner/s;->Ed:Ljava/io/BufferedReader;

    iget-object v7, p0, Lorg/htmlcleaner/s;->Ee:[C

    invoke-virtual {v6, v7, v0, v4}, Ljava/io/BufferedReader;->read([CII)I

    move-result v6

    .line 118
    if-ltz v6, :cond_1

    .line 119
    add-int/2addr v2, v6

    .line 120
    add-int/2addr v0, v6

    .line 121
    sub-int/2addr v4, v6

    .line 123
    :cond_1
    if-ltz v6, :cond_2

    if-gtz v4, :cond_0

    .line 125
    :cond_2
    if-lez v4, :cond_3

    .line 126
    add-int v0, v2, v1

    iput v0, p0, Lorg/htmlcleaner/s;->Eg:I

    .line 131
    :cond_3
    :goto_0
    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v0, :cond_5

    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    :goto_1
    if-ge v3, v0, :cond_6

    .line 132
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    aget-char v0, v0, v3

    .line 133
    const/4 v1, 0x1

    if-lt v0, v1, :cond_4

    if-gt v0, v8, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    .line 134
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    aput-char v8, v0, v3

    .line 131
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    move v0, v5

    goto :goto_1

    .line 138
    :cond_6
    return-void
.end method

.method private ci(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 510
    const-string v0, "html"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "head"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(C)Z
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    invoke-direct {p0, v0, p1}, Lorg/htmlcleaner/s;->a(IC)Z

    move-result v0

    return v0
.end method

.method private f(C)V
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lorg/htmlcleaner/s;->g(C)V

    .line 273
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    return-void
.end method

.method private g(C)V
    .locals 1

    .prologue
    .line 283
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 284
    iget v0, p0, Lorg/htmlcleaner/s;->Eh:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/htmlcleaner/s;->Eh:I

    .line 285
    const/4 v0, 0x1

    iput v0, p0, Lorg/htmlcleaner/s;->Ei:I

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    iget v0, p0, Lorg/htmlcleaner/s;->Ei:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/htmlcleaner/s;->Ei:I

    goto :goto_0
.end method

.method private h(C)V
    .locals 2

    .prologue
    .line 840
    :cond_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_1

    .line 841
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 842
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v1, p0, Lorg/htmlcleaner/s;->Ef:I

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->g(C)V

    .line 843
    invoke-direct {p0, p1}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 847
    :cond_1
    return-void
.end method

.method private isWhitespace()Z
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->isWhitespace(I)Z

    move-result v0

    return v0
.end method

.method private isWhitespace(I)Z
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    if-lt p1, v0, :cond_0

    .line 188
    const/4 v0, 0x0

    .line 191
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    aget-char v0, v0, p1

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    goto :goto_0
.end method

.method private lA()Z
    .locals 2

    .prologue
    .line 264
    iget v0, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v0, :cond_0

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    iget v1, p0, Lorg/htmlcleaner/s;->Eg:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lB()V
    .locals 2

    .prologue
    .line 295
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v1, p0, Lorg/htmlcleaner/s;->Ef:I

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->f(C)V

    .line 298
    :cond_0
    return-void
.end method

.method private lC()V
    .locals 1

    .prologue
    .line 320
    :goto_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/htmlcleaner/s;->isWhitespace()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 322
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto :goto_0

    .line 324
    :cond_0
    return-void
.end method

.method private lD()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 327
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 328
    new-instance v2, Lorg/htmlcleaner/i;

    iget-object v0, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->ld()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/htmlcleaner/s;->lE()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Lorg/htmlcleaner/i;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    .line 329
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 330
    const/4 v0, 0x1

    .line 333
    :goto_1
    return v0

    .line 328
    :cond_0
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 333
    goto :goto_1
.end method

.method private lE()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v5, -0x1

    const/4 v8, 0x0

    .line 337
    sget-object v9, Lorg/htmlcleaner/v;->EB:Lorg/htmlcleaner/v;

    .line 341
    invoke-virtual {v9}, Lorg/htmlcleaner/v;->lT()I

    move-result v10

    .line 343
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    move v7, v8

    move v2, v8

    move v4, v8

    move v1, v5

    .line 344
    :goto_0
    if-ge v7, v0, :cond_9

    .line 345
    iget-object v6, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    const/16 v11, 0x26

    if-ne v6, v11, :cond_0

    .line 349
    add-int/lit8 v1, v7, 0x1

    move v2, v8

    move v4, v8

    move v12, v1

    move v1, v7

    move v7, v12

    goto :goto_0

    .line 350
    :cond_0
    if-eq v1, v5, :cond_8

    .line 351
    iget-object v6, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    const/16 v11, 0x3b

    if-ne v6, v11, :cond_5

    .line 353
    if-eqz v4, :cond_3

    .line 355
    :try_start_0
    iget-object v11, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    if-eqz v2, :cond_1

    const/4 v6, 0x3

    :goto_1
    add-int/2addr v6, v1

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v11

    if-eqz v2, :cond_2

    const/16 v6, 0x10

    :goto_2
    invoke-static {v11, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v9, v6}, Lorg/htmlcleaner/v;->bD(I)Lorg/htmlcleaner/w;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 368
    :goto_3
    if-eqz v6, :cond_4

    .line 369
    invoke-virtual {v6}, Lorg/htmlcleaner/w;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v6

    .line 370
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    add-int/lit8 v7, v7, 0x1

    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v6}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v1, v7, v11}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 371
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 372
    array-length v6, v6

    add-int/2addr v1, v6

    :goto_4
    move v7, v1

    move v1, v5

    .line 377
    goto :goto_0

    .line 355
    :cond_1
    const/4 v6, 0x2

    goto :goto_1

    :cond_2
    const/16 v6, 0xa

    goto :goto_2

    .line 362
    :catch_0
    move-exception v6

    .line 363
    const/4 v6, 0x0

    .line 364
    goto :goto_3

    .line 366
    :cond_3
    iget-object v6, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    add-int/lit8 v11, v1, 0x1

    invoke-virtual {v6, v11, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Lorg/htmlcleaner/v;->cj(Ljava/lang/String;)Lorg/htmlcleaner/w;

    move-result-object v6

    goto :goto_3

    .line 374
    :cond_4
    add-int/lit8 v1, v7, 0x1

    goto :goto_4

    .line 378
    :cond_5
    add-int/lit8 v6, v1, 0x1

    if-ne v7, v6, :cond_6

    iget-object v6, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    const/16 v11, 0x23

    if-ne v6, v11, :cond_6

    move v4, v1

    move v1, v2

    move v2, v3

    .line 385
    :goto_5
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v12, v1

    move v1, v4

    move v4, v2

    move v2, v12

    goto/16 :goto_0

    .line 380
    :cond_6
    add-int/lit8 v6, v1, 0x2

    if-ne v7, v6, :cond_7

    if-eqz v4, :cond_7

    iget-object v6, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    const/16 v11, 0x78

    if-ne v6, v11, :cond_7

    move v2, v4

    move v4, v1

    move v1, v3

    .line 381
    goto :goto_5

    .line 382
    :cond_7
    sub-int v6, v7, v1

    if-le v6, v10, :cond_a

    move v1, v2

    move v2, v4

    move v4, v5

    .line 383
    goto :goto_5

    .line 388
    :cond_8
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto/16 :goto_0

    .line 391
    :cond_9
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_a
    move v12, v2

    move v2, v4

    move v4, v1

    move v1, v12

    goto :goto_5
.end method

.method private lF()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 520
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 521
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 523
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    :goto_0
    return-void

    .line 527
    :cond_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lH()Ljava/lang/String;

    move-result-object v0

    .line 528
    iget-object v1, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    invoke-virtual {v1, v0}, Lorg/htmlcleaner/g;->ca(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 530
    if-eqz v1, :cond_3

    .line 531
    iget-object v2, p0, Lorg/htmlcleaner/s;->Es:Lorg/htmlcleaner/m;

    invoke-virtual {v2}, Lorg/htmlcleaner/m;->kP()Lorg/htmlcleaner/t;

    move-result-object v2

    .line 532
    invoke-interface {v2, v1}, Lorg/htmlcleaner/t;->cb(Ljava/lang/String;)Lorg/htmlcleaner/x;

    move-result-object v2

    .line 533
    if-nez v2, :cond_1

    iget-object v3, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v3}, Lorg/htmlcleaner/f;->kQ()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v3}, Lorg/htmlcleaner/f;->kR()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->ci(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v3}, Lorg/htmlcleaner/f;->kZ()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lorg/htmlcleaner/x;->mc()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->kS()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->kT()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 535
    :cond_2
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lK()Z

    goto :goto_0

    .line 540
    :cond_3
    new-instance v2, Lorg/htmlcleaner/y;

    invoke-direct {v2, v1}, Lorg/htmlcleaner/y;-><init>(Ljava/lang/String;)V

    .line 541
    iput-object v2, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    .line 543
    iget-boolean v3, p0, Lorg/htmlcleaner/s;->Ep:Z

    if-eqz v3, :cond_9

    .line 544
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 545
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lI()V

    .line 547
    if-eqz v1, :cond_5

    .line 548
    iget-object v3, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    if-eqz v3, :cond_4

    .line 549
    iget-object v3, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    invoke-virtual {v2}, Lorg/htmlcleaner/y;->ml()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lorg/htmlcleaner/g;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/htmlcleaner/y;->a(Ljava/util/Map;)V

    .line 551
    :cond_4
    iget-object v0, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    .line 554
    :cond_5
    const/16 v0, 0x3e

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 555
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 556
    const-string v0, "script"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 557
    iput-boolean v5, p0, Lorg/htmlcleaner/s;->Eq:Z

    .line 559
    :cond_6
    const-string v0, "style"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 560
    iput-boolean v5, p0, Lorg/htmlcleaner/s;->Er:Z

    .line 571
    :cond_7
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    goto/16 :goto_0

    .line 562
    :cond_8
    const-string v0, "/>"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 563
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bA(I)V

    .line 568
    new-instance v0, Lorg/htmlcleaner/l;

    invoke-direct {v0, v1}, Lorg/htmlcleaner/l;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    goto :goto_1

    .line 573
    :cond_9
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lD()Z

    goto/16 :goto_0
.end method

.method private lG()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v3, 0x0

    .line 585
    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bC(I)V

    .line 586
    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bA(I)V

    .line 587
    iget v0, p0, Lorg/htmlcleaner/s;->Ei:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/htmlcleaner/s;->Ei:I

    .line 589
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650
    :goto_0
    return-void

    .line 593
    :cond_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lH()Ljava/lang/String;

    move-result-object v0

    .line 594
    iget-object v1, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    invoke-virtual {v1, v0}, Lorg/htmlcleaner/g;->bY(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 595
    iget-object v1, p0, Lorg/htmlcleaner/s;->DU:Lorg/htmlcleaner/g;

    invoke-virtual {v1, v0}, Lorg/htmlcleaner/g;->bZ(Ljava/lang/String;)Lorg/htmlcleaner/aa;

    move-result-object v1

    .line 596
    if-eqz v1, :cond_1

    .line 597
    invoke-virtual {v1}, Lorg/htmlcleaner/aa;->mx()Ljava/lang/String;

    move-result-object v0

    .line 601
    :cond_1
    if-eqz v0, :cond_4

    .line 602
    iget-object v1, p0, Lorg/htmlcleaner/s;->Es:Lorg/htmlcleaner/m;

    invoke-virtual {v1}, Lorg/htmlcleaner/m;->kP()Lorg/htmlcleaner/t;

    move-result-object v1

    .line 603
    invoke-interface {v1, v0}, Lorg/htmlcleaner/t;->cb(Ljava/lang/String;)Lorg/htmlcleaner/x;

    move-result-object v1

    .line 604
    if-nez v1, :cond_2

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->kQ()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->kR()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->ci(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->kZ()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lorg/htmlcleaner/x;->mc()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->kS()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->kT()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 606
    :cond_3
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lK()Z

    goto :goto_0

    .line 611
    :cond_4
    new-instance v1, Lorg/htmlcleaner/l;

    invoke-direct {v1, v0}, Lorg/htmlcleaner/l;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    .line 613
    iget-boolean v1, p0, Lorg/htmlcleaner/s;->Ep:Z

    if-eqz v1, :cond_a

    .line 614
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 615
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lI()V

    .line 617
    if-eqz v0, :cond_5

    .line 618
    iget-object v1, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    .line 621
    :cond_5
    const/16 v1, 0x3e

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 622
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 625
    :cond_6
    const-string v1, "script"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 626
    iput-boolean v3, p0, Lorg/htmlcleaner/s;->Eq:Z

    .line 629
    :cond_7
    const-string v1, "style"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 630
    iput-boolean v3, p0, Lorg/htmlcleaner/s;->Er:Z

    .line 642
    :cond_8
    if-eqz v0, :cond_9

    const-string v1, "html"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 643
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 646
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    goto/16 :goto_0

    .line 648
    :cond_a
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lD()Z

    goto/16 :goto_0
.end method

.method private lH()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x3a

    const/4 v4, 0x0

    .line 657
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/htmlcleaner/s;->Ep:Z

    .line 659
    invoke-direct {p0}, Lorg/htmlcleaner/s;->ly()Z

    move-result v1

    if-nez v1, :cond_1

    .line 660
    iput-boolean v4, p0, Lorg/htmlcleaner/s;->Ep:Z

    .line 701
    :cond_0
    :goto_0
    return-object v0

    .line 664
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 666
    :goto_1
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lorg/htmlcleaner/s;->lz()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 667
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 668
    iget-object v2, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v3, p0, Lorg/htmlcleaner/s;->Ef:I

    aget-char v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 669
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto :goto_1

    .line 673
    :cond_2
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lorg/htmlcleaner/ab;->i(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 674
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 677
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 681
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 683
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 684
    if-ltz v1, :cond_0

    .line 685
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 686
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 687
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 688
    if-ltz v1, :cond_4

    .line 689
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 691
    :cond_4
    iget-object v1, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->kZ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 693
    const-string v1, "xmlns"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 694
    iget-object v1, p0, Lorg/htmlcleaner/s;->Eo:Ljava/util/Set;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private lI()V
    .locals 5

    .prologue
    const/16 v4, 0x3e

    const/16 v3, 0x3c

    .line 709
    :cond_0
    :goto_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lorg/htmlcleaner/s;->Ep:Z

    if-eqz v0, :cond_6

    invoke-direct {p0, v4}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "/>"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 710
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 711
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lH()Ljava/lang/String;

    move-result-object v1

    .line 713
    iget-boolean v0, p0, Lorg/htmlcleaner/s;->Ep:Z

    if-nez v0, :cond_2

    .line 714
    invoke-direct {p0, v3}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v4}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "/>"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 715
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 716
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 719
    :cond_1
    invoke-direct {p0, v3}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-nez v0, :cond_0

    .line 720
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/htmlcleaner/s;->Ep:Z

    goto :goto_0

    .line 728
    :cond_2
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 729
    const/16 v0, 0x3d

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 731
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 732
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lJ()Ljava/lang/String;

    move-result-object v0

    .line 741
    :goto_1
    iget-boolean v2, p0, Lorg/htmlcleaner/s;->Ep:Z

    if-eqz v2, :cond_0

    .line 742
    iget-object v2, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    invoke-virtual {v2, v1, v0}, Lorg/htmlcleaner/z;->H(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 733
    :cond_3
    const-string v0, "empty"

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->lg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 734
    const-string v0, ""

    goto :goto_1

    .line 735
    :cond_4
    const-string v0, "true"

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->lg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 736
    const-string v0, "true"

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 738
    goto :goto_1

    .line 745
    :cond_6
    return-void
.end method

.method private lJ()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x27

    const/16 v9, 0x22

    const/16 v8, 0x3e

    const/16 v7, 0x3c

    .line 756
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 758
    invoke-direct {p0, v7}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v8}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/>"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 759
    :cond_0
    const-string v0, ""

    .line 801
    :goto_0
    return-object v0

    .line 765
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 767
    invoke-direct {p0, v10}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 768
    const/4 v0, 0x1

    .line 769
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 770
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 777
    :goto_1
    iget-object v3, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v3}, Lorg/htmlcleaner/f;->kW()Z

    move-result v3

    .line 779
    iget-object v4, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v4}, Lorg/htmlcleaner/f;->kX()Z

    move-result v4

    .line 781
    :goto_2
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v5

    if-nez v5, :cond_8

    if-eqz v0, :cond_3

    invoke-direct {p0, v10}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_3

    if-nez v4, :cond_2

    invoke-direct {p0, v8}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-direct {p0, v7}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    if-nez v3, :cond_6

    invoke-direct {p0}, Lorg/htmlcleaner/s;->isWhitespace()Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_3
    if-eqz v1, :cond_5

    invoke-direct {p0, v9}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_5

    if-nez v4, :cond_4

    invoke-direct {p0, v8}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-direct {p0, v7}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    if-nez v3, :cond_6

    invoke-direct {p0}, Lorg/htmlcleaner/s;->isWhitespace()Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    if-nez v0, :cond_8

    if-nez v1, :cond_8

    invoke-direct {p0}, Lorg/htmlcleaner/s;->isWhitespace()Z

    move-result v5

    if-nez v5, :cond_8

    invoke-direct {p0, v8}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-direct {p0, v7}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v5

    if-nez v5, :cond_8

    .line 787
    :cond_6
    iget-object v5, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v6, p0, Lorg/htmlcleaner/s;->Ef:I

    aget-char v5, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 788
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 789
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto :goto_2

    .line 771
    :cond_7
    invoke-direct {p0, v9}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 772
    const/4 v0, 0x1

    .line 773
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 774
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    move v11, v1

    move v1, v0

    move v0, v11

    goto/16 :goto_1

    .line 792
    :cond_8
    invoke-direct {p0, v10}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v3

    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    .line 793
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 794
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 801
    :cond_9
    :goto_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 795
    :cond_a
    invoke-direct {p0, v9}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_9

    if-eqz v1, :cond_9

    .line 796
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 797
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method private lK()Z
    .locals 1

    .prologue
    .line 805
    :cond_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_1

    .line 808
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 809
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    .line 811
    const-string v0, "/*<![CDATA[*/"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "//<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 823
    :cond_1
    :goto_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lD()Z

    move-result v0

    return v0

    .line 815
    :cond_2
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lL()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private lL()Z
    .locals 1

    .prologue
    .line 836
    const-string v0, "</"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<!"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<?"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bB(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lM()V
    .locals 6

    .prologue
    const/16 v5, 0x2d

    const/4 v4, 0x0

    .line 850
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bA(I)V

    .line 851
    :goto_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-->"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 852
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 853
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto :goto_0

    .line 856
    :cond_0
    const-string v0, "-->"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 857
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bA(I)V

    .line 860
    :cond_1
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 861
    iget-object v0, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->kU()Z

    move-result v0

    if-nez v0, :cond_4

    .line 862
    iget-object v0, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->lb()Ljava/lang/String;

    move-result-object v1

    .line 863
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "--"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 865
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_2

    .line 866
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 868
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 869
    if-lez v2, :cond_3

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_3

    .line 870
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 873
    :cond_3
    new-instance v1, Lorg/htmlcleaner/h;

    invoke-direct {v1, v0}, Lorg/htmlcleaner/h;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    .line 875
    :cond_4
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 877
    :cond_5
    return-void
.end method

.method private lN()V
    .locals 3

    .prologue
    .line 881
    const/4 v0, 0x0

    .line 883
    iget-boolean v1, p0, Lorg/htmlcleaner/s;->Eq:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/htmlcleaner/s;->Er:Z

    if-nez v1, :cond_0

    .line 887
    iget-object v1, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v1}, Lorg/htmlcleaner/f;->lc()Z

    move-result v1

    if-nez v1, :cond_0

    .line 888
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lK()Z

    .line 939
    :goto_0
    return-void

    .line 893
    :cond_0
    const-string v1, "/*<![CDATA[*/"

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 894
    const-string v1, "/*<![CDATA[*/"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->bA(I)V

    .line 902
    :goto_1
    iget-object v1, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 904
    :goto_2
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "/*]]>*/"

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "]]>"

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "//]]>"

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 905
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lB()V

    .line 906
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto :goto_2

    .line 895
    :cond_1
    const-string v1, "//<![CDATA["

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 896
    const/4 v0, 0x1

    .line 897
    const-string v1, "//<![CDATA["

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->bA(I)V

    goto :goto_1

    .line 899
    :cond_2
    const-string v1, "<![CDATA["

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/htmlcleaner/s;->bA(I)V

    goto :goto_1

    .line 910
    :cond_3
    const-string v2, "/*]]>*/"

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 911
    const-string v2, "/*]]>*/"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->bA(I)V

    .line 920
    :cond_4
    :goto_3
    iget-object v2, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_7

    .line 925
    iget-boolean v2, p0, Lorg/htmlcleaner/s;->Eq:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lorg/htmlcleaner/s;->Er:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v2}, Lorg/htmlcleaner/f;->lc()Z

    move-result v2

    if-nez v2, :cond_7

    .line 930
    :cond_5
    if-eqz v0, :cond_6

    new-instance v0, Lorg/htmlcleaner/i;

    const-string v2, "//"

    invoke-direct {v0, v2}, Lorg/htmlcleaner/i;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    .line 932
    :cond_6
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 933
    new-instance v2, Lorg/htmlcleaner/d;

    invoke-direct {v2, v0}, Lorg/htmlcleaner/d;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->a(Lorg/htmlcleaner/b;)V

    .line 937
    :cond_7
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 913
    :cond_8
    const-string v2, "//]]>"

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 914
    const-string v2, "//]]>"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->bA(I)V

    goto :goto_3

    .line 916
    :cond_9
    const-string v2, "]]>"

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 917
    const-string v2, "]]>"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->bA(I)V

    goto :goto_3
.end method

.method private lO()V
    .locals 6

    .prologue
    .line 942
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bA(I)V

    .line 944
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 945
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lH()Ljava/lang/String;

    move-result-object v1

    .line 946
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 947
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lH()Ljava/lang/String;

    move-result-object v2

    .line 948
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 949
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lJ()Ljava/lang/String;

    move-result-object v3

    .line 950
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 951
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lJ()Ljava/lang/String;

    move-result-object v4

    .line 952
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lC()V

    .line 953
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lJ()Ljava/lang/String;

    move-result-object v5

    .line 955
    const/16 v0, 0x3c

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->h(C)V

    .line 957
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 958
    :cond_0
    new-instance v0, Lorg/htmlcleaner/k;

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/htmlcleaner/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/htmlcleaner/s;->El:Lorg/htmlcleaner/k;

    .line 962
    :goto_0
    return-void

    .line 960
    :cond_1
    new-instance v0, Lorg/htmlcleaner/k;

    invoke-direct/range {v0 .. v5}, Lorg/htmlcleaner/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/htmlcleaner/s;->El:Lorg/htmlcleaner/k;

    goto :goto_0
.end method

.method private lx()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bA(I)V

    .line 150
    return-void
.end method

.method private ly()Z
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bB(I)Z

    move-result v0

    return v0
.end method

.method private lz()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 252
    iget v1, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v1, :cond_1

    iget v1, p0, Lorg/htmlcleaner/s;->Ef:I

    iget v2, p0, Lorg/htmlcleaner/s;->Eg:I

    if-lt v1, v2, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v0

    .line 256
    :cond_1
    iget-object v1, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v2, p0, Lorg/htmlcleaner/s;->Ef:I

    aget-char v1, v1, v2

    .line 257
    invoke-static {v1}, Ljava/lang/Character;->isUnicodeIdentifierStart(C)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Lorg/htmlcleaner/ab;->i(C)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startsWith(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 164
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 165
    invoke-direct {p0, v2}, Lorg/htmlcleaner/s;->bz(I)V

    .line 166
    iget v1, p0, Lorg/htmlcleaner/s;->Eg:I

    if-ltz v1, :cond_1

    iget v1, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/2addr v1, v2

    iget v3, p0, Lorg/htmlcleaner/s;->Eg:I

    if-le v1, v3, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 170
    :goto_1
    if-ge v1, v2, :cond_2

    .line 171
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v3

    .line 172
    iget-object v4, p0, Lorg/htmlcleaner/s;->Ee:[C

    iget v5, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/2addr v5, v1

    aget-char v4, v4, v5

    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    .line 173
    if-ne v3, v4, :cond_0

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 178
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public lP()Lorg/htmlcleaner/k;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lorg/htmlcleaner/s;->El:Lorg/htmlcleaner/k;

    return-object v0
.end method

.method lv()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/htmlcleaner/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    return-object v0
.end method

.method lw()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lorg/htmlcleaner/s;->Eo:Ljava/util/Set;

    return-object v0
.end method

.method start()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x3e

    const/16 v6, 0x3c

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 400
    iput-object v8, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    .line 401
    iget-object v0, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 402
    iput-boolean v2, p0, Lorg/htmlcleaner/s;->Ep:Z

    .line 403
    iput-boolean v4, p0, Lorg/htmlcleaner/s;->Eq:Z

    .line 404
    iput-boolean v4, p0, Lorg/htmlcleaner/s;->Er:Z

    .line 405
    iput-boolean v4, p0, Lorg/htmlcleaner/s;->Ek:Z

    .line 406
    iget-object v0, p0, Lorg/htmlcleaner/s;->Eo:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 408
    const/16 v0, 0x400

    iput v0, p0, Lorg/htmlcleaner/s;->Ef:I

    .line 409
    invoke-direct {p0, v4}, Lorg/htmlcleaner/s;->bz(I)V

    move v1, v2

    move v3, v2

    .line 414
    :cond_0
    :goto_0
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lA()Z

    move-result v0

    if-nez v0, :cond_17

    .line 416
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    iget-object v5, p0, Lorg/htmlcleaner/s;->Ej:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 417
    iput-object v8, p0, Lorg/htmlcleaner/s;->Em:Lorg/htmlcleaner/z;

    .line 418
    iput-boolean v2, p0, Lorg/htmlcleaner/s;->Ep:Z

    .line 421
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bz(I)V

    .line 423
    iget-boolean v0, p0, Lorg/htmlcleaner/s;->Eq:Z

    if-eqz v0, :cond_6

    .line 424
    const-string v0, "</script"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x8

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->isWhitespace(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x8

    invoke-direct {p0, v0, v7}, Lorg/htmlcleaner/s;->a(IC)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 425
    :cond_1
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lG()V

    move v0, v3

    .line 442
    :goto_1
    iget-boolean v3, p0, Lorg/htmlcleaner/s;->Eq:Z

    if-nez v3, :cond_1a

    move v3, v2

    .line 443
    goto :goto_0

    .line 426
    :cond_2
    if-eqz v3, :cond_3

    const-string v0, "<!--"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 427
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lM()V

    move v0, v3

    goto :goto_1

    .line 428
    :cond_3
    const-string v0, "/*<![CDATA[*/"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "//<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 429
    :cond_4
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lN()V

    move v0, v3

    goto :goto_1

    .line 431
    :cond_5
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lK()Z

    move-result v0

    .line 432
    if-eqz v3, :cond_1b

    if-eqz v0, :cond_1b

    .line 433
    iget-object v0, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    iget-object v5, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 434
    if-eqz v0, :cond_1b

    .line 435
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 436
    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1b

    move v0, v4

    .line 437
    goto :goto_1

    .line 446
    :cond_6
    iget-boolean v0, p0, Lorg/htmlcleaner/s;->Er:Z

    if-eqz v0, :cond_c

    .line 447
    const-string v0, "</style"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x7

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->isWhitespace(I)Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x7

    invoke-direct {p0, v0, v7}, Lorg/htmlcleaner/s;->a(IC)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 448
    :cond_7
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lG()V

    move v0, v1

    .line 465
    :goto_2
    iget-boolean v1, p0, Lorg/htmlcleaner/s;->Er:Z

    if-nez v1, :cond_18

    move v1, v2

    .line 466
    goto/16 :goto_0

    .line 449
    :cond_8
    if-eqz v1, :cond_9

    const-string v0, "<!--"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 450
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lM()V

    move v0, v1

    goto :goto_2

    .line 451
    :cond_9
    const-string v0, "/*<![CDATA[*/"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "//<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 452
    :cond_a
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lN()V

    move v0, v1

    goto :goto_2

    .line 454
    :cond_b
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lK()Z

    move-result v0

    .line 455
    if-eqz v1, :cond_19

    if-eqz v0, :cond_19

    .line 456
    iget-object v0, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    iget-object v5, p0, Lorg/htmlcleaner/s;->En:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/htmlcleaner/b;

    .line 457
    if-eqz v0, :cond_19

    .line 458
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 459
    if-eqz v0, :cond_19

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_19

    move v0, v4

    .line 460
    goto :goto_2

    .line 470
    :cond_c
    const-string v0, "<!doctype"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 471
    iget-boolean v0, p0, Lorg/htmlcleaner/s;->Ek:Z

    if-nez v0, :cond_d

    .line 472
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lO()V

    .line 473
    iput-boolean v2, p0, Lorg/htmlcleaner/s;->Ek:Z

    goto/16 :goto_0

    .line 475
    :cond_d
    invoke-direct {p0, v6}, Lorg/htmlcleaner/s;->h(C)V

    goto/16 :goto_0

    .line 477
    :cond_e
    const-string v0, "</"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bB(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 478
    iput-boolean v2, p0, Lorg/htmlcleaner/s;->Ek:Z

    .line 479
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lG()V

    goto/16 :goto_0

    .line 480
    :cond_f
    const-string v0, "/*<![CDATA[*/"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "//<![CDATA["

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 481
    :cond_10
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lN()V

    goto/16 :goto_0

    .line 482
    :cond_11
    const-string v0, "<!--"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 483
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lM()V

    goto/16 :goto_0

    .line 484
    :cond_12
    const-string v0, "<"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget v0, p0, Lorg/htmlcleaner/s;->Ef:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->bB(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 485
    iput-boolean v2, p0, Lorg/htmlcleaner/s;->Ek:Z

    .line 486
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lF()V

    goto/16 :goto_0

    .line 487
    :cond_13
    iget-object v0, p0, Lorg/htmlcleaner/s;->Et:Lorg/htmlcleaner/f;

    invoke-virtual {v0}, Lorg/htmlcleaner/f;->kY()Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "<!"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "<?"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 488
    :cond_14
    invoke-direct {p0, v6}, Lorg/htmlcleaner/s;->h(C)V

    .line 489
    invoke-direct {p0, v7}, Lorg/htmlcleaner/s;->e(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lx()V

    goto/16 :goto_0

    .line 492
    :cond_15
    const-string v0, "<?xml"

    invoke-direct {p0, v0}, Lorg/htmlcleaner/s;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 493
    invoke-direct {p0, v6}, Lorg/htmlcleaner/s;->h(C)V

    goto/16 :goto_0

    .line 495
    :cond_16
    invoke-direct {p0}, Lorg/htmlcleaner/s;->lK()Z

    goto/16 :goto_0

    .line 501
    :cond_17
    iget-object v0, p0, Lorg/htmlcleaner/s;->Ed:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 502
    return-void

    :cond_18
    move v1, v0

    goto/16 :goto_0

    :cond_19
    move v0, v1

    goto/16 :goto_2

    :cond_1a
    move v3, v0

    goto/16 :goto_0

    :cond_1b
    move v0, v3

    goto/16 :goto_1
.end method
