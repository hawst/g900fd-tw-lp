.class public Lorg/apache/commons/net/ftp/l;
.super Ljava/lang/Object;
.source "FTPListParseEngine.java"


# instance fields
.field private BM:Ljava/util/ListIterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ListIterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final BN:Lorg/apache/commons/net/ftp/h;

.field private entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/net/ftp/h;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    .line 78
    iget-object v0, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/l;->BM:Ljava/util/ListIterator;

    .line 83
    iput-object p1, p0, Lorg/apache/commons/net/ftp/l;->BN:Lorg/apache/commons/net/ftp/h;

    .line 84
    return-void
.end method

.method private b(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 123
    if-nez p2, :cond_0

    .line 125
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 132
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/net/ftp/l;->BN:Lorg/apache/commons/net/ftp/h;

    invoke-interface {v1, v0}, Lorg/apache/commons/net/ftp/h;->a(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v1

    .line 134
    :goto_1
    if-eqz v1, :cond_1

    .line 136
    iget-object v2, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v1, p0, Lorg/apache/commons/net/ftp/l;->BN:Lorg/apache/commons/net/ftp/h;

    invoke-interface {v1, v0}, Lorg/apache/commons/net/ftp/h;->a(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 129
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    goto :goto_0

    .line 139
    :cond_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 140
    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    .line 101
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/net/ftp/l;->b(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lorg/apache/commons/net/ftp/l;->BN:Lorg/apache/commons/net/ftp/h;

    iget-object v1, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    invoke-interface {v0, v1}, Lorg/apache/commons/net/ftp/h;->b(Ljava/util/List;)Ljava/util/List;

    .line 103
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/l;->kB()V

    .line 104
    return-void
.end method

.method public a(Lorg/apache/commons/net/ftp/j;)[Lorg/apache/commons/net/ftp/FTPFile;
    .locals 4

    .prologue
    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 253
    iget-object v0, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 254
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256
    iget-object v3, p0, Lorg/apache/commons/net/ftp/l;->BN:Lorg/apache/commons/net/ftp/h;

    invoke-interface {v3, v0}, Lorg/apache/commons/net/ftp/h;->bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    .line 257
    invoke-interface {p1, v0}, Lorg/apache/commons/net/ftp/j;->a(Lorg/apache/commons/net/ftp/FTPFile;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 258
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/commons/net/ftp/FTPFile;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/commons/net/ftp/FTPFile;

    return-object v0
.end method

.method public kA()[Lorg/apache/commons/net/ftp/FTPFile;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lorg/apache/commons/net/ftp/k;->BK:Lorg/apache/commons/net/ftp/j;

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/l;->a(Lorg/apache/commons/net/ftp/j;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    return-object v0
.end method

.method public kB()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/commons/net/ftp/l;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/l;->BM:Ljava/util/ListIterator;

    .line 292
    return-void
.end method
