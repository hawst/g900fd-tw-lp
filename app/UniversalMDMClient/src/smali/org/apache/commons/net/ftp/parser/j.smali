.class public Lorg/apache/commons/net/ftp/parser/j;
.super Lorg/apache/commons/net/ftp/parser/b;
.source "OS2FTPEntryParser.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/j;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/net/ftp/f;)V
    .locals 1

    .prologue
    .line 75
    const-string v0, "\\s*([0-9]+)\\s*(\\s+|[A-Z]+)\\s*(DIR|\\s+)\\s*(\\S+)\\s+(\\S+)\\s+(\\S.*)"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;-><init>(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/j;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 77
    return-void
.end method


# virtual methods
.method public bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 92
    new-instance v0, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 93
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/j;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    invoke-virtual {p0, v6}, Lorg/apache/commons/net/ftp/parser/j;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 96
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/j;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 97
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lorg/apache/commons/net/ftp/parser/j;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 98
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lorg/apache/commons/net/ftp/parser/j;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {p0, v5}, Lorg/apache/commons/net/ftp/parser/j;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 99
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, Lorg/apache/commons/net/ftp/parser/j;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 102
    :try_start_0
    invoke-super {p0, v4}, Lorg/apache/commons/net/ftp/parser/b;->bP(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/commons/net/ftp/FTPFile;->a(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DIR"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DIR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    :cond_0
    invoke-virtual {v0, v6}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 122
    :goto_1
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V

    .line 129
    :goto_2
    return-object v0

    .line 117
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    goto :goto_1

    .line 129
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 104
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method protected kC()Lorg/apache/commons/net/ftp/f;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 141
    new-instance v0, Lorg/apache/commons/net/ftp/f;

    const-string v1, "OS/2"

    const-string v2, "MM-dd-yy HH:mm"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
