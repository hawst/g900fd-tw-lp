.class Lorg/apache/commons/net/ftp/e;
.super Ljava/lang/Object;
.source "FTPClient.java"


# static fields
.field static final By:Ljava/util/Properties;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 388
    const-class v0, Lorg/apache/commons/net/ftp/c;

    const-string v1, "/systemType.properties"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 389
    const/4 v0, 0x0

    .line 390
    if-eqz v1, :cond_0

    .line 391
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 393
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 403
    :cond_0
    :goto_0
    sput-object v0, Lorg/apache/commons/net/ftp/e;->By:Ljava/util/Properties;

    .line 404
    return-void

    .line 394
    :catch_0
    move-exception v2

    .line 397
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 398
    :catch_1
    move-exception v1

    goto :goto_0

    .line 396
    :catchall_0
    move-exception v0

    .line 397
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 400
    :goto_1
    throw v0

    .line 398
    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_1
.end method
