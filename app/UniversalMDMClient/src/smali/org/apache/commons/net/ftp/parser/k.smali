.class public Lorg/apache/commons/net/ftp/parser/k;
.super Lorg/apache/commons/net/ftp/parser/b;
.source "OS400FTPEntryParser.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/k;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/net/ftp/f;)V
    .locals 1

    .prologue
    .line 71
    const-string v0, "(\\S+)\\s+(\\d+)\\s+(\\S+)\\s+(\\S+)\\s+(\\*\\S+)\\s+(\\S+/?)\\s*"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;-><init>(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/k;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 73
    return-void
.end method


# virtual methods
.method public bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 9

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 79
    new-instance v4, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v4}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 80
    invoke-virtual {v4, p1}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/k;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 85
    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/k;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 86
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lorg/apache/commons/net/ftp/parser/k;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 87
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/k;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v7, 0x4

    invoke-virtual {p0, v7}, Lorg/apache/commons/net/ftp/parser/k;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 88
    const/4 v3, 0x5

    invoke-virtual {p0, v3}, Lorg/apache/commons/net/ftp/parser/k;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 89
    const/4 v3, 0x6

    invoke-virtual {p0, v3}, Lorg/apache/commons/net/ftp/parser/k;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 93
    :try_start_0
    invoke-super {p0, v7}, Lorg/apache/commons/net/ftp/parser/b;->bP(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v4, v7}, Lorg/apache/commons/net/ftp/FTPFile;->a(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :goto_0
    const-string v7, "*STMF"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v0, v1

    .line 114
    :cond_0
    :goto_1
    invoke-virtual {v4, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 116
    invoke-virtual {v4, v5}, Lorg/apache/commons/net/ftp/FTPFile;->setUser(Ljava/lang/String;)V

    .line 120
    :try_start_1
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 127
    :goto_2
    const-string v0, "/"

    invoke-virtual {v3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 129
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 131
    :goto_3
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 132
    const/4 v2, -0x1

    if-le v1, v2, :cond_1

    .line 134
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    :cond_1
    invoke-virtual {v4, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    move-object v0, v4

    .line 141
    :goto_4
    return-object v0

    .line 105
    :cond_2
    const-string v7, "*DIR"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    move v0, v2

    .line 111
    goto :goto_1

    .line 141
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 122
    :catch_0
    move-exception v0

    goto :goto_2

    .line 95
    :catch_1
    move-exception v7

    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_3
.end method

.method protected kC()Lorg/apache/commons/net/ftp/f;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 152
    new-instance v0, Lorg/apache/commons/net/ftp/f;

    const-string v1, "OS/400"

    const-string v2, "yy/MM/dd HH:mm:ss"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
