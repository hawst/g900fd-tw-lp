.class public Lorg/apache/commons/net/ftp/parser/n;
.super Lorg/apache/commons/net/ftp/parser/b;
.source "VMSFTPEntryParser.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/n;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/net/ftp/f;)V
    .locals 1

    .prologue
    .line 95
    const-string v0, "(.*;[0-9]+)\\s*(\\d+)/\\d+\\s*(\\S+)\\s+(\\S+)\\s+\\[(([0-9$A-Za-z_]+)|([0-9$A-Za-z_]+),([0-9$a-zA-Z_]+))\\]?\\s*\\([a-zA-Z]*,([a-zA-Z]*),([a-zA-Z]*),([a-zA-Z]*)\\)"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;-><init>(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/n;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 97
    return-void
.end method


# virtual methods
.method public a(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 216
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 218
    :goto_0
    if-eqz v0, :cond_2

    .line 220
    const-string v2, "Directory"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Total"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 221
    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 225
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 230
    :cond_3
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 112
    const-wide/16 v6, 0x200

    .line 114
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/n;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 116
    new-instance v4, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v4}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 117
    invoke-virtual {v4, p1}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 119
    invoke-virtual {p0, v12}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v13}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v9, 0x4

    invoke-virtual {p0, v9}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    const/4 v9, 0x5

    invoke-virtual {p0, v9}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 122
    new-array v10, v13, [Ljava/lang/String;

    .line 123
    const/16 v11, 0x9

    invoke-virtual {p0, v11}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v3

    .line 124
    const/16 v11, 0xa

    invoke-virtual {p0, v11}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v2

    .line 125
    const/16 v11, 0xb

    invoke-virtual {p0, v11}, Lorg/apache/commons/net/ftp/parser/n;->group(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v12

    .line 128
    :try_start_0
    invoke-super {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;->bP(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/apache/commons/net/ftp/FTPFile;->a(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v11, ","

    invoke-direct {v0, v9, v11}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    move-object v0, v1

    .line 153
    :goto_1
    const-string v9, ".DIR"

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    const/4 v11, -0x1

    if-eq v9, v11, :cond_0

    .line 155
    invoke-virtual {v4, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 163
    :goto_2
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/n;->kE()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 165
    invoke-virtual {v4, v5}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 174
    :goto_3
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    mul-long/2addr v6, v8

    .line 175
    invoke-virtual {v4, v6, v7}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V

    .line 177
    invoke-virtual {v4, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setGroup(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v4, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setUser(Ljava/lang/String;)V

    move v1, v3

    .line 186
    :goto_4
    if-ge v1, v13, :cond_5

    .line 188
    aget-object v5, v10, v1

    .line 190
    const/16 v0, 0x52

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_2

    move v0, v2

    :goto_5
    invoke-virtual {v4, v1, v3, v0}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    .line 191
    const/16 v0, 0x57

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v2

    :goto_6
    invoke-virtual {v4, v1, v2, v0}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    .line 192
    const/16 v0, 0x45

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_4

    move v0, v2

    :goto_7
    invoke-virtual {v4, v1, v12, v0}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    .line 186
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 142
    :pswitch_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 145
    :pswitch_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 159
    :cond_0
    invoke-virtual {v4, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    goto :goto_2

    .line 169
    :cond_1
    const-string v9, ";"

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v5, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 170
    invoke-virtual {v4, v5}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    goto :goto_3

    :cond_2
    move v0, v3

    .line 190
    goto :goto_5

    :cond_3
    move v0, v3

    .line 191
    goto :goto_6

    :cond_4
    move v0, v3

    .line 192
    goto :goto_7

    :cond_5
    move-object v1, v4

    .line 197
    :cond_6
    return-object v1

    .line 130
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected kC()Lorg/apache/commons/net/ftp/f;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 247
    new-instance v0, Lorg/apache/commons/net/ftp/f;

    const-string v1, "VMS"

    const-string v2, "d-MMM-yyyy HH:mm:ss"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected kE()Z
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    return v0
.end method
