.class public final Lorg/apache/commons/net/io/d;
.super Ljava/io/PushbackInputStream;
.source "FromNetASCIIInputStream.java"


# static fields
.field static final Cc:Z

.field static final Cd:Ljava/lang/String;

.field static final Ce:[B


# instance fields
.field private Cf:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/io/d;->Cd:Ljava/lang/String;

    .line 42
    sget-object v0, Lorg/apache/commons/net/io/d;->Cd:Ljava/lang/String;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/commons/net/io/d;->Cc:Z

    .line 43
    sget-object v0, Lorg/apache/commons/net/io/d;->Cd:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/io/d;->Ce:[B

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lorg/apache/commons/net/io/d;->Ce:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, v0}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/net/io/d;->Cf:I

    .line 69
    return-void
.end method

.method private kF()I
    .locals 3

    .prologue
    const/16 v1, 0xd

    .line 76
    invoke-super {p0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 78
    if-ne v0, v1, :cond_0

    .line 80
    invoke-super {p0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 81
    const/16 v2, 0xa

    if-ne v0, v2, :cond_1

    .line 83
    sget-object v0, Lorg/apache/commons/net/io/d;->Ce:[B

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/io/d;->unread([B)V

    .line 84
    invoke-super {p0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 86
    iget v1, p0, Lorg/apache/commons/net/io/d;->Cf:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/net/io/d;->Cf:I

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 91
    invoke-virtual {p0, v0}, Lorg/apache/commons/net/io/d;->unread(I)V

    :cond_2
    move v0, v1

    .line 92
    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/commons/net/io/d;->in:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/io/d;->buf:[B

    array-length v0, v0

    iget v1, p0, Lorg/apache/commons/net/io/d;->pos:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/commons/net/io/d;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public read()I
    .locals 1

    .prologue
    .line 116
    sget-boolean v0, Lorg/apache/commons/net/io/d;->Cc:Z

    if-eqz v0, :cond_0

    .line 117
    invoke-super {p0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 119
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/commons/net/io/d;->kF()I

    move-result v0

    goto :goto_0
.end method

.method public read([B)I
    .locals 2

    .prologue
    .line 137
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/commons/net/io/d;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 158
    sget-boolean v0, Lorg/apache/commons/net/io/d;->Cc:Z

    if-eqz v0, :cond_0

    .line 159
    invoke-super {p0, p1, p2, p3}, Ljava/io/PushbackInputStream;->read([BII)I

    move-result v0

    .line 187
    :goto_0
    return v0

    .line 161
    :cond_0
    if-ge p3, v1, :cond_1

    .line 162
    const/4 v0, 0x0

    goto :goto_0

    .line 166
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/net/io/d;->available()I

    move-result v0

    .line 168
    if-le p3, v0, :cond_2

    move p3, v0

    :cond_2
    iput p3, p0, Lorg/apache/commons/net/io/d;->Cf:I

    .line 171
    iget v0, p0, Lorg/apache/commons/net/io/d;->Cf:I

    if-ge v0, v1, :cond_3

    .line 172
    iput v1, p0, Lorg/apache/commons/net/io/d;->Cf:I

    .line 175
    :cond_3
    invoke-direct {p0}, Lorg/apache/commons/net/io/d;->kF()I

    move-result v0

    if-ne v0, v3, :cond_4

    move v0, v3

    .line 176
    goto :goto_0

    :cond_4
    move v1, p2

    .line 182
    :goto_1
    add-int/lit8 v2, v1, 0x1

    int-to-byte v0, v0

    aput-byte v0, p1, v1

    .line 184
    iget v0, p0, Lorg/apache/commons/net/io/d;->Cf:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/net/io/d;->Cf:I

    if-lez v0, :cond_5

    invoke-direct {p0}, Lorg/apache/commons/net/io/d;->kF()I

    move-result v0

    if-ne v0, v3, :cond_6

    .line 187
    :cond_5
    sub-int v0, v2, p2

    goto :goto_0

    :cond_6
    move v1, v2

    goto :goto_1
.end method
