.class Lorg/apache/commons/net/ftp/d;
.super Ljava/lang/Object;
.source "FTPClient.java"

# interfaces
.implements Lorg/apache/commons/net/io/c;


# instance fields
.field private final Bu:Lorg/apache/commons/net/ftp/c;

.field private final Bv:J

.field private final Bw:I

.field private Bx:I

.field private time:J


# direct methods
.method constructor <init>(Lorg/apache/commons/net/ftp/c;JI)V
    .locals 2

    .prologue
    .line 3288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3285
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/commons/net/ftp/d;->time:J

    .line 3289
    iput-wide p2, p0, Lorg/apache/commons/net/ftp/d;->Bv:J

    .line 3290
    iput-object p1, p0, Lorg/apache/commons/net/ftp/d;->Bu:Lorg/apache/commons/net/ftp/c;

    .line 3291
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/c;->getSoTimeout()I

    move-result v0

    iput v0, p0, Lorg/apache/commons/net/ftp/d;->Bw:I

    .line 3292
    invoke-virtual {p1, p4}, Lorg/apache/commons/net/ftp/c;->setSoTimeout(I)V

    .line 3293
    return-void
.end method


# virtual methods
.method public a(JIJ)V
    .locals 6

    .prologue
    .line 3300
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3301
    iget-wide v2, p0, Lorg/apache/commons/net/ftp/d;->time:J

    sub-long v2, v0, v2

    iget-wide v4, p0, Lorg/apache/commons/net/ftp/d;->Bv:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 3303
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/net/ftp/d;->Bu:Lorg/apache/commons/net/ftp/c;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/c;->jV()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3308
    :goto_0
    iput-wide v0, p0, Lorg/apache/commons/net/ftp/d;->time:J

    .line 3310
    :cond_0
    return-void

    .line 3304
    :catch_0
    move-exception v2

    .line 3305
    iget v2, p0, Lorg/apache/commons/net/ftp/d;->Bx:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/commons/net/ftp/d;->Bx:I

    goto :goto_0

    .line 3306
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method iv()V
    .locals 2

    .prologue
    .line 3313
    :goto_0
    iget v0, p0, Lorg/apache/commons/net/ftp/d;->Bx:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/commons/net/ftp/d;->Bx:I

    if-lez v0, :cond_0

    .line 3314
    iget-object v0, p0, Lorg/apache/commons/net/ftp/d;->Bu:Lorg/apache/commons/net/ftp/c;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/c;->jT()V

    goto :goto_0

    .line 3316
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/d;->Bu:Lorg/apache/commons/net/ftp/c;

    iget v1, p0, Lorg/apache/commons/net/ftp/d;->Bw:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/c;->setSoTimeout(I)V

    .line 3317
    return-void
.end method
