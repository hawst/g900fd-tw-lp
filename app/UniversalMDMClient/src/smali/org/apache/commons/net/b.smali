.class public abstract Lorg/apache/commons/net/b;
.super Ljava/lang/Object;
.source "SocketClient.java"


# static fields
.field private static final Au:Ljavax/net/SocketFactory;

.field private static final Av:Ljavax/net/ServerSocketFactory;


# instance fields
.field protected AB:Ljava/io/InputStream;

.field protected AC:Ljava/io/OutputStream;

.field protected AE:Ljavax/net/SocketFactory;

.field protected AF:Ljavax/net/ServerSocketFactory;

.field protected AG:I

.field private AH:I

.field private AI:I

.field private Aw:Lorg/apache/commons/net/ProtocolCommandSupport;

.field protected Ax:I

.field protected Ay:Ljava/net/Socket;

.field protected Az:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/b;->Au:Ljavax/net/SocketFactory;

    .line 67
    invoke-static {}, Ljavax/net/ServerSocketFactory;->getDefault()Ljavax/net/ServerSocketFactory;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/b;->Av:Ljavax/net/ServerSocketFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput v0, p0, Lorg/apache/commons/net/b;->AG:I

    .line 102
    iput v2, p0, Lorg/apache/commons/net/b;->AH:I

    .line 105
    iput v2, p0, Lorg/apache/commons/net/b;->AI:I

    .line 115
    iput-object v1, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    .line 116
    iput-object v1, p0, Lorg/apache/commons/net/b;->AB:Ljava/io/InputStream;

    .line 117
    iput-object v1, p0, Lorg/apache/commons/net/b;->AC:Ljava/io/OutputStream;

    .line 118
    iput v0, p0, Lorg/apache/commons/net/b;->Ax:I

    .line 119
    iput v0, p0, Lorg/apache/commons/net/b;->Az:I

    .line 120
    sget-object v0, Lorg/apache/commons/net/b;->Au:Ljavax/net/SocketFactory;

    iput-object v0, p0, Lorg/apache/commons/net/b;->AE:Ljavax/net/SocketFactory;

    .line 121
    sget-object v0, Lorg/apache/commons/net/b;->Av:Ljavax/net/ServerSocketFactory;

    iput-object v0, p0, Lorg/apache/commons/net/b;->AF:Ljavax/net/ServerSocketFactory;

    .line 122
    return-void
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 312
    if-eqz p1, :cond_0

    .line 314
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 315
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private closeQuietly(Ljava/net/Socket;)V
    .locals 1

    .prologue
    .line 303
    if-eqz p1, :cond_0

    .line 305
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected A(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 739
    invoke-virtual {p0}, Lorg/apache/commons/net/b;->jR()Lorg/apache/commons/net/ProtocolCommandSupport;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/net/ProtocolCommandSupport;->jP()I

    move-result v0

    if-lez v0, :cond_0

    .line 740
    invoke-virtual {p0}, Lorg/apache/commons/net/b;->jR()Lorg/apache/commons/net/ProtocolCommandSupport;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/net/ProtocolCommandSupport;->A(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    :cond_0
    return-void
.end method

.method public a(Ljava/net/Socket;)Z
    .locals 2

    .prologue
    .line 630
    invoke-virtual {p1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 631
    invoke-virtual {p0}, Lorg/apache/commons/net/b;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 633
    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bn(I)V
    .locals 0

    .prologue
    .line 379
    iput p1, p0, Lorg/apache/commons/net/b;->Az:I

    .line 380
    return-void
.end method

.method public connect(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lorg/apache/commons/net/b;->Az:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/net/b;->connect(Ljava/lang/String;I)V

    .line 279
    return-void
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 189
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/net/b;->connect(Ljava/net/InetAddress;I)V

    .line 190
    return-void
.end method

.method public connect(Ljava/net/InetAddress;I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 165
    iget-object v0, p0, Lorg/apache/commons/net/b;->AE:Ljavax/net/SocketFactory;

    invoke-virtual {v0}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    .line 166
    iget v0, p0, Lorg/apache/commons/net/b;->AH:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    iget v1, p0, Lorg/apache/commons/net/b;->AH:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setReceiveBufferSize(I)V

    .line 167
    :cond_0
    iget v0, p0, Lorg/apache/commons/net/b;->AI:I

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    iget v1, p0, Lorg/apache/commons/net/b;->AI:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSendBufferSize(I)V

    .line 168
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iget v2, p0, Lorg/apache/commons/net/b;->AG:I

    invoke-virtual {v0, v1, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 169
    invoke-virtual {p0}, Lorg/apache/commons/net/b;->jQ()V

    .line 170
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 294
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-direct {p0, v0}, Lorg/apache/commons/net/b;->closeQuietly(Ljava/net/Socket;)V

    .line 295
    iget-object v0, p0, Lorg/apache/commons/net/b;->AB:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/apache/commons/net/b;->a(Ljava/io/Closeable;)V

    .line 296
    iget-object v0, p0, Lorg/apache/commons/net/b;->AC:Ljava/io/OutputStream;

    invoke-direct {p0, v0}, Lorg/apache/commons/net/b;->a(Ljava/io/Closeable;)V

    .line 297
    iput-object v1, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    .line 298
    iput-object v1, p0, Lorg/apache/commons/net/b;->AB:Ljava/io/InputStream;

    .line 299
    iput-object v1, p0, Lorg/apache/commons/net/b;->AC:Ljava/io/OutputStream;

    .line 300
    return-void
.end method

.method protected f(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 726
    invoke-virtual {p0}, Lorg/apache/commons/net/b;->jR()Lorg/apache/commons/net/ProtocolCommandSupport;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/net/ProtocolCommandSupport;->jP()I

    move-result v0

    if-lez v0, :cond_0

    .line 727
    invoke-virtual {p0}, Lorg/apache/commons/net/b;->jR()Lorg/apache/commons/net/ProtocolCommandSupport;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/commons/net/ProtocolCommandSupport;->f(ILjava/lang/String;)V

    .line 729
    :cond_0
    return-void
.end method

.method public getLocalAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public getSoTimeout()I
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getSoTimeout()I

    move-result v0

    return v0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    if-nez v0, :cond_0

    .line 329
    const/4 v0, 0x0

    .line 331
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    goto :goto_0
.end method

.method protected jQ()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    iget v1, p0, Lorg/apache/commons/net/b;->Ax:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 144
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/b;->AB:Ljava/io/InputStream;

    .line 145
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/b;->AC:Ljava/io/OutputStream;

    .line 146
    return-void
.end method

.method protected jR()Lorg/apache/commons/net/ProtocolCommandSupport;
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lorg/apache/commons/net/b;->Aw:Lorg/apache/commons/net/ProtocolCommandSupport;

    return-object v0
.end method

.method public setSoTimeout(I)V
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lorg/apache/commons/net/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 436
    return-void
.end method
