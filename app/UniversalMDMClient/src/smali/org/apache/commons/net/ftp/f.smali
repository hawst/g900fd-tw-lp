.class public Lorg/apache/commons/net/ftp/f;
.super Ljava/lang/Object;
.source "FTPClientConfig.java"


# static fields
.field private static final BH:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private BA:Ljava/lang/String;

.field private BB:Ljava/lang/String;

.field private BD:Z

.field private BE:Ljava/lang/String;

.field private BF:Ljava/lang/String;

.field private BG:Ljava/lang/String;

.field private final Bz:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 265
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    .line 275
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "en"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "de"

    sget-object v2, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "it"

    sget-object v2, Ljava/util/Locale;->ITALIAN:Ljava/util/Locale;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "es"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "es"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "pt"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "pt"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "da"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "da"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "sv"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "sv"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "no"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "no"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "nl"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "nl"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "ro"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "ro"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "sq"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "sq"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "sh"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "sh"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "sk"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "sk"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "sl"

    new-instance v2, Ljava/util/Locale;

    const-string v3, "sl"

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    const-string v1, "fr"

    const-string v2, "jan|f\u00e9v|mar|avr|mai|jun|jui|ao\u00fb|sep|oct|nov|d\u00e9c"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 231
    const-string v0, "UNIX"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-object v1, p0, Lorg/apache/commons/net/ftp/f;->BA:Ljava/lang/String;

    .line 210
    iput-object v1, p0, Lorg/apache/commons/net/ftp/f;->BB:Ljava/lang/String;

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/net/ftp/f;->BD:Z

    .line 212
    iput-object v1, p0, Lorg/apache/commons/net/ftp/f;->BE:Ljava/lang/String;

    .line 213
    iput-object v1, p0, Lorg/apache/commons/net/ftp/f;->BF:Ljava/lang/String;

    .line 214
    iput-object v1, p0, Lorg/apache/commons/net/ftp/f;->BG:Ljava/lang/String;

    .line 223
    iput-object p1, p0, Lorg/apache/commons/net/ftp/f;->Bz:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;)V

    .line 258
    iput-object p2, p0, Lorg/apache/commons/net/ftp/f;->BA:Ljava/lang/String;

    .line 259
    iput-object p3, p0, Lorg/apache/commons/net/ftp/f;->BB:Ljava/lang/String;

    .line 260
    iput-object p4, p0, Lorg/apache/commons/net/ftp/f;->BE:Ljava/lang/String;

    .line 261
    iput-object p5, p0, Lorg/apache/commons/net/ftp/f;->BF:Ljava/lang/String;

    .line 262
    iput-object p6, p0, Lorg/apache/commons/net/ftp/f;->BG:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public static bJ(Ljava/lang/String;)Ljava/text/DateFormatSymbols;
    .locals 2

    .prologue
    .line 524
    sget-object v0, Lorg/apache/commons/net/ftp/f;->BH:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_1

    .line 526
    instance-of v1, v0, Ljava/util/Locale;

    if-eqz v1, :cond_0

    .line 527
    new-instance v1, Ljava/text/DateFormatSymbols;

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v0}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    move-object v0, v1

    .line 532
    :goto_0
    return-object v0

    .line 528
    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 529
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/net/ftp/f;->bK(Ljava/lang/String;)Ljava/text/DateFormatSymbols;

    move-result-object v0

    goto :goto_0

    .line 532
    :cond_1
    new-instance v0, Ljava/text/DateFormatSymbols;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public static bK(Ljava/lang/String;)Ljava/text/DateFormatSymbols;
    .locals 3

    .prologue
    .line 545
    invoke-static {p0}, Lorg/apache/commons/net/ftp/f;->bL(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 546
    new-instance v1, Ljava/text/DateFormatSymbols;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 547
    invoke-virtual {v1, v0}, Ljava/text/DateFormatSymbols;->setShortMonths([Ljava/lang/String;)V

    .line 548
    return-object v1
.end method

.method private static bL(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    .prologue
    .line 552
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v0, "|"

    invoke-direct {v2, p0, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    .line 554
    const/16 v1, 0xc

    if-eq v1, v0, :cond_0

    .line 555
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "expecting a pipe-delimited string containing 12 tokens"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 558
    :cond_0
    const/16 v0, 0xd

    new-array v3, v0, [Ljava/lang/String;

    .line 559
    const/4 v0, 0x0

    .line 560
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 561
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    move v0, v1

    goto :goto_0

    .line 563
    :cond_1
    const-string v1, ""

    aput-object v1, v3, v0

    .line 564
    return-object v3
.end method


# virtual methods
.method public bH(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lorg/apache/commons/net/ftp/f;->BA:Ljava/lang/String;

    .line 383
    return-void
.end method

.method public bI(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lorg/apache/commons/net/ftp/f;->BB:Ljava/lang/String;

    .line 402
    return-void
.end method

.method public ks()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lorg/apache/commons/net/ftp/f;->Bz:Ljava/lang/String;

    return-object v0
.end method

.method public kt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lorg/apache/commons/net/ftp/f;->BA:Ljava/lang/String;

    return-object v0
.end method

.method public ku()Ljava/lang/String;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lorg/apache/commons/net/ftp/f;->BB:Ljava/lang/String;

    return-object v0
.end method

.method public kv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lorg/apache/commons/net/ftp/f;->BG:Ljava/lang/String;

    return-object v0
.end method

.method public kw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lorg/apache/commons/net/ftp/f;->BF:Ljava/lang/String;

    return-object v0
.end method

.method public kx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/commons/net/ftp/f;->BE:Ljava/lang/String;

    return-object v0
.end method

.method public ky()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lorg/apache/commons/net/ftp/f;->BD:Z

    return v0
.end method
