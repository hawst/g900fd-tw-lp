.class public Lorg/apache/commons/net/ftp/parser/a;
.super Lorg/apache/commons/net/ftp/i;
.source "CompositeFileEntryParser.java"


# instance fields
.field private final BO:[Lorg/apache/commons/net/ftp/h;

.field private BP:Lorg/apache/commons/net/ftp/h;


# direct methods
.method public constructor <init>([Lorg/apache/commons/net/ftp/h;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/i;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/a;->BP:Lorg/apache/commons/net/ftp/h;

    .line 43
    iput-object p1, p0, Lorg/apache/commons/net/ftp/parser/a;->BO:[Lorg/apache/commons/net/ftp/h;

    .line 44
    return-void
.end method


# virtual methods
.method public bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/a;->BP:Lorg/apache/commons/net/ftp/h;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/a;->BP:Lorg/apache/commons/net/ftp/h;

    invoke-interface {v0, p1}, Lorg/apache/commons/net/ftp/h;->bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_2

    .line 70
    :goto_0
    return-object v0

    .line 58
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/commons/net/ftp/parser/a;->BO:[Lorg/apache/commons/net/ftp/h;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 60
    iget-object v1, p0, Lorg/apache/commons/net/ftp/parser/a;->BO:[Lorg/apache/commons/net/ftp/h;

    aget-object v2, v1, v0

    .line 62
    invoke-interface {v2, p1}, Lorg/apache/commons/net/ftp/h;->bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_1

    .line 65
    iput-object v2, p0, Lorg/apache/commons/net/ftp/parser/a;->BP:Lorg/apache/commons/net/ftp/h;

    move-object v0, v1

    .line 66
    goto :goto_0

    .line 58
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
