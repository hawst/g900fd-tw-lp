.class public final Lorg/apache/commons/net/io/g;
.super Ljava/lang/Object;
.source "Util.java"


# direct methods
.method public static final a(Ljava/io/InputStream;Ljava/io/OutputStream;IJLorg/apache/commons/net/io/c;Z)J
    .locals 9

    .prologue
    .line 96
    new-array v7, p2, [B

    .line 97
    const-wide/16 v2, 0x0

    .line 101
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0, v7}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v1, -0x1

    if-eq v4, v1, :cond_1

    .line 106
    if-nez v4, :cond_4

    .line 108
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 109
    if-gez v1, :cond_2

    .line 134
    :cond_1
    return-wide v2

    .line 111
    :cond_2
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 112
    if-eqz p6, :cond_3

    .line 113
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 114
    :cond_3
    const-wide/16 v5, 0x1

    add-long/2addr v2, v5

    .line 115
    if-eqz p5, :cond_0

    .line 116
    const/4 v4, 0x1

    move-object v1, p5

    move-wide v5, p3

    invoke-interface/range {v1 .. v6}, Lorg/apache/commons/net/io/c;->a(JIJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 128
    :catch_0
    move-exception v1

    .line 130
    new-instance v4, Lorg/apache/commons/net/io/CopyStreamException;

    const-string v5, "IOException caught while copying."

    invoke-direct {v4, v5, v2, v3, v1}, Lorg/apache/commons/net/io/CopyStreamException;-><init>(Ljava/lang/String;JLjava/io/IOException;)V

    throw v4

    .line 120
    :cond_4
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p1, v7, v1, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 121
    if-eqz p6, :cond_5

    .line 122
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 123
    :cond_5
    int-to-long v5, v4

    add-long/2addr v2, v5

    .line 124
    if-eqz p5, :cond_0

    move-object v1, p5

    move-wide v5, p3

    .line 125
    invoke-interface/range {v1 .. v6}, Lorg/apache/commons/net/io/c;->a(JIJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static closeQuietly(Ljava/net/Socket;)V
    .locals 1

    .prologue
    .line 359
    if-eqz p0, :cond_0

    .line 361
    :try_start_0
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 362
    :catch_0
    move-exception v0

    goto :goto_0
.end method
