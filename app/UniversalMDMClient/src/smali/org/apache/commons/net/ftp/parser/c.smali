.class public Lorg/apache/commons/net/ftp/parser/c;
.super Ljava/lang/Object;
.source "DefaultFTPFileEntryParserFactory.java"

# interfaces
.implements Lorg/apache/commons/net/ftp/parser/d;


# static fields
.field private static final BR:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "(\\p{javaJavaIdentifierStart}(\\p{javaJavaIdentifierPart})*\\.)+\\p{javaJavaIdentifierStart}(\\p{javaJavaIdentifierPart})*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/ftp/parser/c;->BR:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;
    .locals 5

    .prologue
    .line 98
    const/4 v1, 0x0

    .line 101
    sget-object v0, Lorg/apache/commons/net/ftp/parser/c;->BR:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 106
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/net/ftp/h;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v0

    .line 121
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 122
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 123
    const-string v1, "UNIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_3

    .line 125
    new-instance v1, Lorg/apache/commons/net/ftp/parser/m;

    invoke-direct {v1, p2}, Lorg/apache/commons/net/ftp/parser/m;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 164
    :cond_1
    :goto_1
    instance-of v0, v1, Lorg/apache/commons/net/ftp/a;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 165
    check-cast v0, Lorg/apache/commons/net/ftp/a;

    invoke-interface {v0, p2}, Lorg/apache/commons/net/ftp/a;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 167
    :cond_2
    return-object v1

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :try_start_2
    new-instance v3, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " does not implement the interface "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "org.apache.commons.net.ftp.FTPFileEntryParser."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2, v0}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 116
    :catch_1
    move-exception v0

    goto :goto_0

    .line 111
    :catch_2
    move-exception v0

    .line 112
    new-instance v2, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v3, "Error initializing parser"

    invoke-direct {v2, v3, v0}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 113
    :catch_3
    move-exception v0

    .line 114
    new-instance v2, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v3, "Error initializing parser"

    invoke-direct {v2, v3, v0}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 127
    :cond_3
    const-string v1, "VMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_4

    .line 129
    new-instance v1, Lorg/apache/commons/net/ftp/parser/o;

    invoke-direct {v1, p2}, Lorg/apache/commons/net/ftp/parser/o;-><init>(Lorg/apache/commons/net/ftp/f;)V

    goto :goto_1

    .line 131
    :cond_4
    const-string v1, "WINDOWS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_5

    .line 133
    invoke-direct {p0, p2}, Lorg/apache/commons/net/ftp/parser/c;->c(Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;

    move-result-object v1

    goto :goto_1

    .line 135
    :cond_5
    const-string v1, "OS/2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_6

    .line 137
    new-instance v1, Lorg/apache/commons/net/ftp/parser/j;

    invoke-direct {v1, p2}, Lorg/apache/commons/net/ftp/parser/j;-><init>(Lorg/apache/commons/net/ftp/f;)V

    goto :goto_1

    .line 139
    :cond_6
    const-string v1, "OS/400"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_7

    const-string v1, "AS/400"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_8

    .line 142
    :cond_7
    invoke-direct {p0, p2}, Lorg/apache/commons/net/ftp/parser/c;->d(Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;

    move-result-object v1

    goto :goto_1

    .line 144
    :cond_8
    const-string v1, "MVS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_9

    .line 146
    new-instance v1, Lorg/apache/commons/net/ftp/parser/g;

    invoke-direct {v1}, Lorg/apache/commons/net/ftp/parser/g;-><init>()V

    goto/16 :goto_1

    .line 148
    :cond_9
    const-string v1, "NETWARE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_a

    .line 150
    new-instance v1, Lorg/apache/commons/net/ftp/parser/i;

    invoke-direct {v1, p2}, Lorg/apache/commons/net/ftp/parser/i;-><init>(Lorg/apache/commons/net/ftp/f;)V

    goto/16 :goto_1

    .line 152
    :cond_a
    const-string v1, "TYPE: L8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_b

    .line 156
    new-instance v1, Lorg/apache/commons/net/ftp/parser/m;

    invoke-direct {v1, p2}, Lorg/apache/commons/net/ftp/parser/m;-><init>(Lorg/apache/commons/net/ftp/f;)V

    goto/16 :goto_1

    .line 160
    :cond_b
    new-instance v0, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown parser type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;
    .locals 4

    .prologue
    .line 227
    if-eqz p1, :cond_0

    const-string v0, "WINDOWS"

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/f;->ks()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    new-instance v0, Lorg/apache/commons/net/ftp/parser/h;

    invoke-direct {v0, p1}, Lorg/apache/commons/net/ftp/parser/h;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 232
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/commons/net/ftp/parser/a;

    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/commons/net/ftp/h;

    const/4 v2, 0x0

    new-instance v3, Lorg/apache/commons/net/ftp/parser/h;

    invoke-direct {v3, p1}, Lorg/apache/commons/net/ftp/parser/h;-><init>(Lorg/apache/commons/net/ftp/f;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/commons/net/ftp/parser/m;

    invoke-direct {v3, p1}, Lorg/apache/commons/net/ftp/parser/m;-><init>(Lorg/apache/commons/net/ftp/f;)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/parser/a;-><init>([Lorg/apache/commons/net/ftp/h;)V

    goto :goto_0
.end method

.method private d(Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;
    .locals 4

    .prologue
    .line 259
    if-eqz p1, :cond_0

    const-string v0, "OS/400"

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/f;->ks()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    new-instance v0, Lorg/apache/commons/net/ftp/parser/k;

    invoke-direct {v0, p1}, Lorg/apache/commons/net/ftp/parser/k;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 264
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/commons/net/ftp/parser/a;

    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/commons/net/ftp/h;

    const/4 v2, 0x0

    new-instance v3, Lorg/apache/commons/net/ftp/parser/k;

    invoke-direct {v3, p1}, Lorg/apache/commons/net/ftp/parser/k;-><init>(Lorg/apache/commons/net/ftp/f;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/commons/net/ftp/parser/m;

    invoke-direct {v3, p1}, Lorg/apache/commons/net/ftp/parser/m;-><init>(Lorg/apache/commons/net/ftp/f;)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/parser/a;-><init>([Lorg/apache/commons/net/ftp/h;)V

    goto :goto_0
.end method


# virtual methods
.method public b(Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/f;->ks()Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/c;->a(Ljava/lang/String;Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;

    move-result-object v0

    return-object v0
.end method

.method public bQ(Ljava/lang/String;)Lorg/apache/commons/net/ftp/h;
    .locals 2

    .prologue
    .line 91
    if-nez p1, :cond_0

    .line 92
    new-instance v0, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;

    const-string v1, "Parser key cannot be null"

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/parser/ParserInitializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/net/ftp/parser/c;->a(Ljava/lang/String;Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;

    move-result-object v0

    return-object v0
.end method
