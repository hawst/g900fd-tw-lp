.class public abstract Lorg/apache/commons/net/ftp/parser/l;
.super Lorg/apache/commons/net/ftp/i;
.source "RegexFTPFileEntryParserImpl.java"


# instance fields
.field private BW:Ljava/util/regex/Pattern;

.field private BX:Ljava/util/regex/MatchResult;

.field protected BY:Ljava/util/regex/Matcher;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/i;-><init>()V

    .line 43
    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BW:Ljava/util/regex/Pattern;

    .line 48
    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BX:Ljava/util/regex/MatchResult;

    .line 54
    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BY:Ljava/util/regex/Matcher;

    .line 72
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/l;->bU(Ljava/lang/String;)Z

    .line 73
    return-void
.end method


# virtual methods
.method public bU(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 147
    :try_start_0
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BW:Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BW:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unparseable regex supplied: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public group(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BX:Ljava/util/regex/MatchResult;

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BX:Ljava/util/regex/MatchResult;

    invoke-interface {v0, p1}, Ljava/util/regex/MatchResult;->group(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public matches(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BX:Ljava/util/regex/MatchResult;

    .line 85
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BW:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BY:Ljava/util/regex/Matcher;

    .line 86
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BY:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BY:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->toMatchResult()Ljava/util/regex/MatchResult;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BX:Ljava/util/regex/MatchResult;

    .line 89
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/l;->BX:Ljava/util/regex/MatchResult;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
