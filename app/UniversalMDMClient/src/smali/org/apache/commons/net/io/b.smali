.class public Lorg/apache/commons/net/io/b;
.super Ljava/lang/Object;
.source "CopyStreamAdapter.java"

# interfaces
.implements Lorg/apache/commons/net/io/c;


# instance fields
.field private final Cb:Lorg/apache/commons/net/util/ListenerList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lorg/apache/commons/net/util/ListenerList;

    invoke-direct {v0}, Lorg/apache/commons/net/util/ListenerList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/io/b;->Cb:Lorg/apache/commons/net/util/ListenerList;

    .line 51
    return-void
.end method


# virtual methods
.method public a(JIJ)V
    .locals 7

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/commons/net/io/b;->Cb:Lorg/apache/commons/net/util/ListenerList;

    invoke-virtual {v0}, Lorg/apache/commons/net/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EventListener;

    .line 91
    check-cast v0, Lorg/apache/commons/net/io/c;

    check-cast v0, Lorg/apache/commons/net/io/c;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lorg/apache/commons/net/io/c;->a(JIJ)V

    goto :goto_0

    .line 94
    :cond_0
    return-void
.end method

.method public b(Lorg/apache/commons/net/io/c;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/commons/net/io/b;->Cb:Lorg/apache/commons/net/util/ListenerList;

    invoke-virtual {v0, p1}, Lorg/apache/commons/net/util/ListenerList;->a(Ljava/util/EventListener;)V

    .line 105
    return-void
.end method
