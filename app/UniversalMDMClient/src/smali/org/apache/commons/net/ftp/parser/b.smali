.class public abstract Lorg/apache/commons/net/ftp/parser/b;
.super Lorg/apache/commons/net/ftp/parser/l;
.source "ConfigurableFTPFileEntryParserImpl.java"

# interfaces
.implements Lorg/apache/commons/net/ftp/a;


# instance fields
.field private final BQ:Lorg/apache/commons/net/ftp/parser/e;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lorg/apache/commons/net/ftp/parser/l;-><init>(Ljava/lang/String;)V

    .line 60
    new-instance v0, Lorg/apache/commons/net/ftp/parser/f;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/f;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/b;->BQ:Lorg/apache/commons/net/ftp/parser/e;

    .line 61
    return-void
.end method


# virtual methods
.method public a(Lorg/apache/commons/net/ftp/f;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/b;->BQ:Lorg/apache/commons/net/ftp/parser/e;

    instance-of v0, v0, Lorg/apache/commons/net/ftp/a;

    if-eqz v0, :cond_2

    .line 93
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/parser/b;->kC()Lorg/apache/commons/net/ftp/f;

    move-result-object v1

    .line 94
    if-eqz p1, :cond_3

    .line 95
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/f;->kt()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 96
    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/f;->kt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/commons/net/ftp/f;->bH(Ljava/lang/String;)V

    .line 98
    :cond_0
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/f;->ku()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 99
    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/f;->ku()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/commons/net/ftp/f;->bI(Ljava/lang/String;)V

    .line 101
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/b;->BQ:Lorg/apache/commons/net/ftp/parser/e;

    check-cast v0, Lorg/apache/commons/net/ftp/a;

    invoke-interface {v0, p1}, Lorg/apache/commons/net/ftp/a;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 106
    :cond_2
    :goto_0
    return-void

    .line 103
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/b;->BQ:Lorg/apache/commons/net/ftp/parser/e;

    check-cast v0, Lorg/apache/commons/net/ftp/a;

    invoke-interface {v0, v1}, Lorg/apache/commons/net/ftp/a;->a(Lorg/apache/commons/net/ftp/f;)V

    goto :goto_0
.end method

.method public bP(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/b;->BQ:Lorg/apache/commons/net/ftp/parser/e;

    invoke-interface {v0, p1}, Lorg/apache/commons/net/ftp/parser/e;->bP(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method protected abstract kC()Lorg/apache/commons/net/ftp/f;
.end method
