.class public Lorg/apache/commons/net/ftp/b;
.super Lorg/apache/commons/net/b;
.source "FTP.java"


# instance fields
.field protected AJ:I

.field protected AK:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected AL:Z

.field protected AM:Ljava/lang/String;

.field protected AN:Ljava/lang/String;

.field protected AO:Lorg/apache/commons/net/ProtocolCommandSupport;

.field protected AP:Z

.field protected AQ:Ljava/io/BufferedReader;

.field protected AR:Ljava/io/BufferedWriter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 261
    invoke-direct {p0}, Lorg/apache/commons/net/b;-><init>()V

    .line 234
    iput-boolean v1, p0, Lorg/apache/commons/net/ftp/b;->AP:Z

    .line 262
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/b;->bn(I)V

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    .line 264
    iput-boolean v1, p0, Lorg/apache/commons/net/ftp/b;->AL:Z

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AM:Ljava/lang/String;

    .line 266
    const-string v0, "ISO-8859-1"

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AN:Ljava/lang/String;

    .line 267
    new-instance v0, Lorg/apache/commons/net/ProtocolCommandSupport;

    invoke-direct {v0, p0}, Lorg/apache/commons/net/ProtocolCommandSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AO:Lorg/apache/commons/net/ProtocolCommandSupport;

    .line 268
    return-void
.end method

.method private B(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 272
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 481
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    if-eqz p2, :cond_0

    .line 485
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 486
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    :cond_0
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private F(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/net/ftp/b;->AL:Z

    .line 308
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 310
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AQ:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 312
    if-nez v0, :cond_0

    .line 313
    new-instance v0, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;

    const-string v1, "Connection closed without indication."

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 319
    if-ge v1, v4, :cond_1

    .line 320
    new-instance v1, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Truncated server reply: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 326
    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x3

    :try_start_0
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 327
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lorg/apache/commons/net/ftp/b;->AJ:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    iget-object v3, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    if-le v1, v4, :cond_4

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_4

    .line 342
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AQ:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 344
    if-nez v0, :cond_3

    .line 345
    new-instance v0, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;

    const-string v1, "Connection closed without indication."

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :catch_0
    move-exception v1

    .line 331
    new-instance v1, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not parse response code.\nServer Reply: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 348
    :cond_3
    iget-object v1, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/b;->kf()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v0, v2}, Lorg/apache/commons/net/ftp/b;->B(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 357
    :cond_4
    :goto_0
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/b;->jZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/net/ftp/b;->f(ILjava/lang/String;)V

    .line 359
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    const/16 v1, 0x1a5

    if-ne v0, v1, :cond_6

    .line 360
    new-instance v0, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;

    const-string v1, "FTP response 421 received.  Server closed connection."

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_5
    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/b;->bv(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 362
    :cond_6
    return-void
.end method

.method private bv(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 281
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2d

    if-eq v1, v2, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private bx(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 495
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AR:Ljava/io/BufferedWriter;

    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AR:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    return-void

    .line 498
    :catch_0
    move-exception v0

    .line 500
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/b;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 502
    new-instance v0, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;

    const-string v1, "Connection unexpectedly closed."

    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/FTPConnectionClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :cond_0
    throw v0
.end method

.method private jS()V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/b;->F(Z)V

    .line 291
    return-void
.end method


# virtual methods
.method public C(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AR:Ljava/io/BufferedWriter;

    if-nez v0, :cond_0

    .line 465
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Connection is not open"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/net/ftp/b;->D(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 470
    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/b;->bx(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/net/ftp/b;->A(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;->jS()V

    .line 475
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    return v0
.end method

.method public a(Ljava/net/InetAddress;I)I
    .locals 4

    .prologue
    const/16 v3, 0x2c

    .line 862
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 864
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    ushr-int/lit8 v1, p2, 0x8

    .line 866
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 867
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 868
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 869
    and-int/lit16 v1, p2, 0xff

    .line 870
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 872
    const/16 v1, 0x8

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public b(Ljava/net/InetAddress;I)I
    .locals 4

    .prologue
    .line 904
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 908
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 909
    const-string v2, "%"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 910
    if-lez v2, :cond_0

    .line 911
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 913
    :cond_0
    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 915
    instance-of v2, p1, Ljava/net/Inet4Address;

    if-eqz v2, :cond_2

    .line 916
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    :cond_1
    :goto_0
    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    const-string v0, "|"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 922
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 923
    const-string v0, "|"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 925
    const/16 v0, 0x25

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0

    .line 917
    :cond_2
    instance-of v2, p1, Ljava/net/Inet6Address;

    if-eqz v2, :cond_1

    .line 918
    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public bA(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1245
    const/16 v0, 0x12

    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bo(I)I
    .locals 1

    .prologue
    .line 596
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bp(I)I
    .locals 3

    .prologue
    .line 1018
    const/16 v0, 0xa

    const-string v1, "AEILNTCFRPSBC"

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bw(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lorg/apache/commons/net/ftp/b;->AN:Ljava/lang/String;

    .line 410
    return-void
.end method

.method public by(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 693
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bz(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public disconnect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 434
    invoke-super {p0}, Lorg/apache/commons/net/b;->disconnect()V

    .line 435
    iput-object v1, p0, Lorg/apache/commons/net/ftp/b;->AQ:Ljava/io/BufferedReader;

    .line 436
    iput-object v1, p0, Lorg/apache/commons/net/ftp/b;->AR:Ljava/io/BufferedWriter;

    .line 437
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/net/ftp/b;->AL:Z

    .line 438
    iput-object v1, p0, Lorg/apache/commons/net/ftp/b;->AM:Ljava/lang/String;

    .line 439
    return-void
.end method

.method public g(ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 547
    invoke-static {p1}, Lorg/apache/commons/net/ftp/g;->bs(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/commons/net/ftp/b;->C(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected jQ()V
    .locals 4

    .prologue
    .line 371
    invoke-super {p0}, Lorg/apache/commons/net/b;->jQ()V

    .line 372
    new-instance v0, Lorg/apache/commons/net/io/a;

    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v2, p0, Lorg/apache/commons/net/ftp/b;->AB:Ljava/io/InputStream;

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/b;->jU()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/commons/net/io/a;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AQ:Ljava/io/BufferedReader;

    .line 374
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    iget-object v2, p0, Lorg/apache/commons/net/ftp/b;->AC:Ljava/io/OutputStream;

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/b;->jU()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AR:Ljava/io/BufferedWriter;

    .line 376
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AG:I

    if-lez v0, :cond_2

    .line 377
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getSoTimeout()I

    move-result v1

    .line 378
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->Ay:Ljava/net/Socket;

    iget v2, p0, Lorg/apache/commons/net/ftp/b;->AG:I

    invoke-virtual {v0, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 380
    :try_start_0
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;->jS()V

    .line 382
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bu(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;->jS()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 397
    :cond_1
    :goto_0
    return-void

    .line 384
    :catch_0
    move-exception v0

    .line 385
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Timed out waiting for initial connect reply"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 386
    invoke-virtual {v2, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 387
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lorg/apache/commons/net/ftp/b;->Ay:Ljava/net/Socket;

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0

    .line 392
    :cond_2
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;->jS()V

    .line 394
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bu(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;->jS()V

    goto :goto_0
.end method

.method protected jR()Lorg/apache/commons/net/ProtocolCommandSupport;
    .locals 1

    .prologue
    .line 1726
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AO:Lorg/apache/commons/net/ProtocolCommandSupport;

    return-object v0
.end method

.method protected jT()V
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/b;->F(Z)V

    .line 301
    return-void
.end method

.method public jU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AN:Ljava/lang/String;

    return-object v0
.end method

.method protected jV()V
    .locals 2

    .prologue
    .line 519
    const/16 v0, 0x20

    invoke-static {v0}, Lorg/apache/commons/net/ftp/g;->bs(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/net/ftp/b;->D(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 520
    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/b;->bx(Ljava/lang/String;)V

    .line 521
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/b;->jT()V

    .line 522
    return-void
.end method

.method public jW()I
    .locals 1

    .prologue
    .line 610
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    return v0
.end method

.method public jX()I
    .locals 1

    .prologue
    .line 632
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;->jS()V

    .line 633
    iget v0, p0, Lorg/apache/commons/net/ftp/b;->AJ:I

    return v0
.end method

.method public jY()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public jZ()Ljava/lang/String;
    .locals 3

    .prologue
    .line 660
    iget-boolean v0, p0, Lorg/apache/commons/net/ftp/b;->AL:Z

    if-nez v0, :cond_0

    .line 661
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AM:Ljava/lang/String;

    .line 673
    :goto_0
    return-object v0

    .line 664
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x100

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 666
    iget-object v0, p0, Lorg/apache/commons/net/ftp/b;->AK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 667
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 668
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 671
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/net/ftp/b;->AL:Z

    .line 673
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/b;->AM:Ljava/lang/String;

    goto :goto_0
.end method

.method public ka()I
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/b;->bo(I)I

    move-result v0

    return v0
.end method

.method public kb()I
    .locals 1

    .prologue
    .line 945
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/b;->bo(I)I

    move-result v0

    return v0
.end method

.method public kc()I
    .locals 1

    .prologue
    .line 966
    const/16 v0, 0x24

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/b;->bo(I)I

    move-result v0

    return v0
.end method

.method public kd()I
    .locals 1

    .prologue
    .line 1205
    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/b;->bo(I)I

    move-result v0

    return v0
.end method

.method public ke()I
    .locals 1

    .prologue
    .line 1608
    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/b;->bo(I)I

    move-result v0

    return v0
.end method

.method public kf()Z
    .locals 1

    .prologue
    .line 1709
    iget-boolean v0, p0, Lorg/apache/commons/net/ftp/b;->AP:Z

    return v0
.end method

.method public mode(I)I
    .locals 3

    .prologue
    .line 1060
    const/16 v0, 0xc

    const-string v1, "AEILNTCFRPSBC"

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/net/ftp/b;->g(ILjava/lang/String;)I

    move-result v0

    return v0
.end method
