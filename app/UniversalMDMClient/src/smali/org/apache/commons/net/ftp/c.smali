.class public Lorg/apache/commons/net/ftp/c;
.super Lorg/apache/commons/net/ftp/b;
.source "FTPClient.java"

# interfaces
.implements Lorg/apache/commons/net/ftp/a;


# static fields
.field private static final Br:Ljava/util/regex/Pattern;


# instance fields
.field private AS:I

.field private AT:I

.field private AU:I

.field private AV:Ljava/lang/String;

.field private final AW:Ljava/util/Random;

.field private AX:I

.field private AY:I

.field private AZ:Ljava/net/InetAddress;

.field private Ba:I

.field private Bb:I

.field private Bc:I

.field private Bd:I

.field private Be:Z

.field private Bf:J

.field private Bg:Lorg/apache/commons/net/ftp/parser/d;

.field private Bh:I

.field private Bi:Z

.field private Bj:Z

.field private Bk:Ljava/lang/String;

.field private Bl:Lorg/apache/commons/net/ftp/h;

.field private Bm:Ljava/lang/String;

.field private Bn:Lorg/apache/commons/net/ftp/f;

.field private Bo:Lorg/apache/commons/net/io/c;

.field private Bp:J

.field private Bq:I

.field private Bs:Z

.field private Bt:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 373
    const-string v0, "(\\d{1,3},\\d{1,3},\\d{1,3},\\d{1,3}),(\\d{1,3}),(\\d{1,3})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/net/ftp/c;->Br:Ljava/util/regex/Pattern;

    .line 375
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 430
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/b;-><init>()V

    .line 368
    const/16 v0, 0x3e8

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->Bq:I

    .line 378
    iput-boolean v1, p0, Lorg/apache/commons/net/ftp/c;->Bs:Z

    .line 431
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kh()V

    .line 432
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->AT:I

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/net/ftp/c;->Be:Z

    .line 434
    new-instance v0, Lorg/apache/commons/net/ftp/parser/c;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/c;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bg:Lorg/apache/commons/net/ftp/parser/d;

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bn:Lorg/apache/commons/net/ftp/f;

    .line 436
    iput-boolean v1, p0, Lorg/apache/commons/net/ftp/c;->Bi:Z

    .line 437
    iput-boolean v1, p0, Lorg/apache/commons/net/ftp/c;->Bj:Z

    .line 438
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->AW:Ljava/util/Random;

    .line 439
    return-void
.end method

.method private a(Lorg/apache/commons/net/ftp/h;Ljava/lang/String;)Lorg/apache/commons/net/ftp/l;
    .locals 4

    .prologue
    .line 2989
    new-instance v0, Lorg/apache/commons/net/ftp/l;

    invoke-direct {v0, p1}, Lorg/apache/commons/net/ftp/l;-><init>(Lorg/apache/commons/net/ftp/h;)V

    .line 2990
    const/16 v1, 0x1a

    invoke-virtual {p0, p2}, Lorg/apache/commons/net/ftp/c;->bG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/commons/net/ftp/c;->h(ILjava/lang/String;)Ljava/net/Socket;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3003
    :goto_0
    return-object v0

    .line 2996
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->jU()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/net/ftp/l;->a(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2999
    invoke-static {v1}, Lorg/apache/commons/net/io/g;->closeQuietly(Ljava/net/Socket;)V

    .line 3002
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->km()Z

    goto :goto_0

    .line 2999
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lorg/apache/commons/net/io/g;->closeQuietly(Ljava/net/Socket;)V

    throw v0
.end method

.method private a(Lorg/apache/commons/net/io/c;)Lorg/apache/commons/net/io/c;
    .locals 2

    .prologue
    .line 3329
    if-nez p1, :cond_1

    .line 3330
    iget-object p1, p0, Lorg/apache/commons/net/ftp/c;->Bo:Lorg/apache/commons/net/io/c;

    .line 3339
    :cond_0
    :goto_0
    return-object p1

    .line 3332
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bo:Lorg/apache/commons/net/io/c;

    if-eqz v0, :cond_0

    .line 3336
    new-instance v0, Lorg/apache/commons/net/io/b;

    invoke-direct {v0}, Lorg/apache/commons/net/io/b;-><init>()V

    .line 3337
    invoke-virtual {v0, p1}, Lorg/apache/commons/net/io/b;->b(Lorg/apache/commons/net/io/c;)V

    .line 3338
    iget-object v1, p0, Lorg/apache/commons/net/ftp/c;->Bo:Lorg/apache/commons/net/io/c;

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/io/b;->b(Lorg/apache/commons/net/io/c;)V

    move-object p1, v0

    .line 3339
    goto :goto_0
.end method

.method private a(ILjava/lang/String;Ljava/io/InputStream;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 551
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/net/ftp/c;->h(ILjava/lang/String;)Ljava/net/Socket;

    move-result-object v9

    if-nez v9, :cond_1

    .line 585
    :cond_0
    :goto_0
    return v0

    .line 554
    :cond_1
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-virtual {v9}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kp()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 557
    iget v1, p0, Lorg/apache/commons/net/ftp/c;->Ba:I

    if-nez v1, :cond_3

    .line 558
    new-instance v2, Lorg/apache/commons/net/io/f;

    invoke-direct {v2, v0}, Lorg/apache/commons/net/io/f;-><init>(Ljava/io/OutputStream;)V

    .line 560
    :goto_1
    const/4 v0, 0x0

    .line 561
    iget-wide v4, p0, Lorg/apache/commons/net/ftp/c;->Bp:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 562
    new-instance v0, Lorg/apache/commons/net/ftp/d;

    iget-wide v4, p0, Lorg/apache/commons/net/ftp/c;->Bp:J

    iget v1, p0, Lorg/apache/commons/net/ftp/c;->Bq:I

    invoke-direct {v0, p0, v4, v5, v1}, Lorg/apache/commons/net/ftp/d;-><init>(Lorg/apache/commons/net/ftp/c;JI)V

    move-object v8, v0

    .line 568
    :goto_2
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kp()I

    move-result v3

    const-wide/16 v4, -0x1

    invoke-direct {p0, v8}, Lorg/apache/commons/net/ftp/c;->a(Lorg/apache/commons/net/io/c;)Lorg/apache/commons/net/io/c;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v7}, Lorg/apache/commons/net/io/g;->a(Ljava/io/InputStream;Ljava/io/OutputStream;IJLorg/apache/commons/net/io/c;Z)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 579
    invoke-virtual {v9}, Ljava/net/Socket;->close()V

    .line 581
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->km()Z

    move-result v0

    .line 582
    if-eqz v8, :cond_0

    .line 583
    invoke-virtual {v8}, Lorg/apache/commons/net/ftp/d;->iv()V

    goto :goto_0

    .line 572
    :catch_0
    move-exception v0

    .line 574
    invoke-static {v9}, Lorg/apache/commons/net/io/g;->closeQuietly(Ljava/net/Socket;)V

    .line 575
    throw v0

    :cond_2
    move-object v8, v0

    goto :goto_2

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method private bB(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 476
    sget-object v0, Lorg/apache/commons/net/ftp/c;->Br:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 477
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_0

    .line 478
    new-instance v0, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse passive host information.\nServer Reply: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2c

    const/16 v3, 0x2e

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    .line 486
    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 487
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 488
    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->AU:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 499
    invoke-virtual {v0}, Ljava/net/InetAddress;->isSiteLocalAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->isSiteLocalAddress()Z

    move-result v0

    if-nez v0, :cond_1

    .line 500
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 501
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Replacing site local address "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/commons/net/ftp/c;->f(ILjava/lang/String;)V

    .line 503
    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    .line 509
    :cond_1
    return-void

    .line 490
    :catch_0
    move-exception v0

    .line 492
    new-instance v0, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse passive port information.\nServer Reply: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 505
    :catch_1
    move-exception v0

    .line 506
    new-instance v0, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse passive host information.\nServer Reply: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private bC(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 516
    const/16 v0, 0x28

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x29

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 520
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 521
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 522
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 523
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 525
    if-ne v1, v2, :cond_0

    if-ne v2, v3, :cond_0

    if-eq v3, v4, :cond_1

    .line 527
    :cond_0
    new-instance v1, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not parse extended passive host information.\nServer Reply: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 531
    :cond_1
    const/4 v1, 0x3

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 541
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    .line 542
    iput v0, p0, Lorg/apache/commons/net/ftp/c;->AU:I

    .line 543
    return-void

    .line 533
    :catch_0
    move-exception v1

    .line 535
    new-instance v1, Lorg/apache/commons/net/MalformedServerReplyException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not parse extended passive host information.\nServer Reply: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/commons/net/MalformedServerReplyException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private c(J)Z
    .locals 3

    .prologue
    .line 2227
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/commons/net/ftp/c;->Bf:J

    .line 2228
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/c;->bA(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bw(I)Z

    move-result v0

    return v0
.end method

.method private static kg()Ljava/util/Properties;
    .locals 1

    .prologue
    .line 408
    sget-object v0, Lorg/apache/commons/net/ftp/e;->By:Ljava/util/Properties;

    return-object v0
.end method

.method private kh()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 444
    iput v1, p0, Lorg/apache/commons/net/ftp/c;->AS:I

    .line 445
    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    .line 446
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->AU:I

    .line 447
    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->AZ:Ljava/net/InetAddress;

    .line 448
    iput v1, p0, Lorg/apache/commons/net/ftp/c;->AX:I

    .line 449
    iput v1, p0, Lorg/apache/commons/net/ftp/c;->AY:I

    .line 450
    iput v1, p0, Lorg/apache/commons/net/ftp/c;->Ba:I

    .line 451
    const/4 v0, 0x7

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->Bc:I

    .line 452
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->Bb:I

    .line 453
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->Bd:I

    .line 454
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/commons/net/ftp/c;->Bf:J

    .line 455
    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->Bk:Ljava/lang/String;

    .line 456
    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->Bl:Lorg/apache/commons/net/ftp/h;

    .line 457
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bm:Ljava/lang/String;

    .line 458
    const/16 v0, 0x400

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->Bh:I

    .line 459
    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->Bt:Ljava/util/HashMap;

    .line 460
    return-void
.end method

.method private kk()I
    .locals 3

    .prologue
    .line 1197
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AX:I

    if-lez v0, :cond_1

    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AY:I

    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AX:I

    if-lt v0, v1, :cond_1

    .line 1199
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AY:I

    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AX:I

    if-ne v0, v1, :cond_0

    .line 1200
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AY:I

    .line 1207
    :goto_0
    return v0

    .line 1202
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AW:Ljava/util/Random;

    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AY:I

    iget v2, p0, Lorg/apache/commons/net/ftp/c;->AX:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AX:I

    add-int/2addr v0, v1

    goto :goto_0

    .line 1207
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private kl()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AZ:Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AZ:Ljava/net/InetAddress;

    .line 1225
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_0
.end method

.method private kn()Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2048
    iget-object v1, p0, Lorg/apache/commons/net/ftp/c;->Bt:Ljava/util/HashMap;

    if-nez v1, :cond_4

    .line 2050
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kd()I

    move-result v1

    invoke-static {v1}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v1

    .line 2052
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/commons/net/ftp/c;->Bt:Ljava/util/HashMap;

    .line 2053
    if-nez v1, :cond_0

    .line 2077
    :goto_0
    return v0

    .line 2056
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->jY()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    .line 2057
    const-string v0, " "

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2059
    const-string v0, ""

    .line 2060
    const/16 v1, 0x20

    invoke-virtual {v6, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    .line 2061
    if-lez v7, :cond_3

    .line 2062
    invoke-virtual {v6, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2063
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 2067
    :goto_2
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 2068
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bt:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 2069
    if-nez v0, :cond_1

    .line 2070
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2071
    iget-object v7, p0, Lorg/apache/commons/net/ftp/c;->Bt:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2056
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2065
    :cond_3
    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_2

    :cond_4
    move v0, v2

    .line 2077
    goto :goto_0
.end method


# virtual methods
.method public E(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 872
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/c;->by(Ljava/lang/String;)I

    .line 874
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AJ:I

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 875
    const/4 v0, 0x1

    .line 882
    :goto_0
    return v0

    .line 879
    :cond_0
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AJ:I

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bw(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 880
    const/4 v0, 0x0

    goto :goto_0

    .line 882
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/commons/net/ftp/c;->bz(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    goto :goto_0
.end method

.method public F(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/commons/net/ftp/l;
    .locals 2

    .prologue
    .line 2928
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bl:Lorg/apache/commons/net/ftp/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bm:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2929
    :cond_0
    if-eqz p1, :cond_2

    .line 2932
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bg:Lorg/apache/commons/net/ftp/parser/d;

    invoke-interface {v0, p1}, Lorg/apache/commons/net/ftp/parser/d;->bQ(Ljava/lang/String;)Lorg/apache/commons/net/ftp/h;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bl:Lorg/apache/commons/net/ftp/h;

    .line 2934
    iput-object p1, p0, Lorg/apache/commons/net/ftp/c;->Bm:Ljava/lang/String;

    .line 2965
    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bl:Lorg/apache/commons/net/ftp/h;

    invoke-direct {p0, v0, p2}, Lorg/apache/commons/net/ftp/c;->a(Lorg/apache/commons/net/ftp/h;Ljava/lang/String;)Lorg/apache/commons/net/ftp/l;

    move-result-object v0

    return-object v0

    .line 2939
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bn:Lorg/apache/commons/net/ftp/f;

    if-eqz v0, :cond_3

    .line 2940
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bg:Lorg/apache/commons/net/ftp/parser/d;

    iget-object v1, p0, Lorg/apache/commons/net/ftp/c;->Bn:Lorg/apache/commons/net/ftp/f;

    invoke-interface {v0, v1}, Lorg/apache/commons/net/ftp/parser/d;->b(Lorg/apache/commons/net/ftp/f;)Lorg/apache/commons/net/ftp/h;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bl:Lorg/apache/commons/net/ftp/h;

    .line 2942
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bn:Lorg/apache/commons/net/ftp/f;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/f;->ks()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bm:Ljava/lang/String;

    goto :goto_0

    .line 2948
    :cond_3
    const-string v0, "org.apache.commons.net.ftp.systemType"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2949
    if-nez v0, :cond_4

    .line 2950
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->ko()Ljava/lang/String;

    move-result-object v1

    .line 2951
    invoke-static {}, Lorg/apache/commons/net/ftp/c;->kg()Ljava/util/Properties;

    move-result-object v0

    .line 2952
    if-eqz v0, :cond_5

    .line 2953
    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2954
    if-eqz v0, :cond_5

    .line 2959
    :cond_4
    :goto_1
    iget-object v1, p0, Lorg/apache/commons/net/ftp/c;->Bg:Lorg/apache/commons/net/ftp/parser/d;

    invoke-interface {v1, v0}, Lorg/apache/commons/net/ftp/parser/d;->bQ(Ljava/lang/String;)Lorg/apache/commons/net/ftp/h;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/commons/net/ftp/c;->Bl:Lorg/apache/commons/net/ftp/h;

    .line 2960
    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bm:Ljava/lang/String;

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public G(Z)V
    .locals 0

    .prologue
    .line 3348
    iput-boolean p1, p0, Lorg/apache/commons/net/ftp/c;->Bs:Z

    .line 3349
    return-void
.end method

.method public a(Lorg/apache/commons/net/ftp/f;)V
    .locals 0

    .prologue
    .line 3163
    iput-object p1, p0, Lorg/apache/commons/net/ftp/c;->Bn:Lorg/apache/commons/net/ftp/f;

    .line 3164
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/InputStream;)Z
    .locals 1

    .prologue
    .line 1704
    const/16 v0, 0xe

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/commons/net/ftp/c;->a(ILjava/lang/String;Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method public bD(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 1658
    const/16 v0, 0xd

    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/net/ftp/c;->h(ILjava/lang/String;)Ljava/net/Socket;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1659
    const/4 v0, 0x0

    .line 1674
    :goto_0
    return-object v0

    .line 1661
    :cond_0
    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 1662
    iget v1, p0, Lorg/apache/commons/net/ftp/c;->Ba:I

    if-nez v1, :cond_1

    .line 1670
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kp()I

    move-result v3

    invoke-direct {v1, v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 1672
    new-instance v0, Lorg/apache/commons/net/io/d;

    invoke-direct {v0, v1}, Lorg/apache/commons/net/io/d;-><init>(Ljava/io/InputStream;)V

    .line 1674
    :cond_1
    new-instance v1, Lorg/apache/commons/net/io/e;

    invoke-direct {v1, v2, v0}, Lorg/apache/commons/net/io/e;-><init>(Ljava/net/Socket;Ljava/io/InputStream;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public bE(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2012
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2013
    const/4 v0, 0x0

    .line 2015
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bt:Ljava/util/HashMap;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public bF(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;
    .locals 1

    .prologue
    .line 2623
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lorg/apache/commons/net/ftp/c;->F(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/commons/net/ftp/l;

    move-result-object v0

    .line 2624
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/l;->kA()[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    return-object v0
.end method

.method protected bG(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3036
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3038
    if-eqz p1, :cond_1

    .line 3040
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 3041
    const-string v1, "-a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3042
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3043
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 3051
    :cond_0
    :goto_0
    return-object p1

    .line 3047
    :cond_1
    const-string p1, "-a"

    goto :goto_0
.end method

.method public bq(I)Z
    .locals 1

    .prologue
    .line 1279
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/c;->bp(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281
    iput p1, p0, Lorg/apache/commons/net/ftp/c;->Ba:I

    .line 1282
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->Bb:I

    .line 1283
    const/4 v0, 0x1

    .line 1285
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public br(I)Z
    .locals 1

    .prologue
    .line 1376
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/c;->mode(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1378
    iput p1, p0, Lorg/apache/commons/net/ftp/c;->Bd:I

    .line 1379
    const/4 v0, 0x1

    .line 1381
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    .prologue
    .line 824
    invoke-super {p0}, Lorg/apache/commons/net/ftp/b;->disconnect()V

    .line 825
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kh()V

    .line 826
    return-void
.end method

.method protected h(ILjava/lang/String;)Ljava/net/Socket;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 638
    iget v3, p0, Lorg/apache/commons/net/ftp/c;->AS:I

    if-eqz v3, :cond_1

    iget v3, p0, Lorg/apache/commons/net/ftp/c;->AS:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    move-object v0, v1

    .line 757
    :cond_0
    :goto_0
    return-object v0

    .line 642
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v3

    instance-of v3, v3, Ljava/net/Inet6Address;

    .line 644
    iget v4, p0, Lorg/apache/commons/net/ftp/c;->AS:I

    if-nez v4, :cond_8

    .line 648
    iget-object v2, p0, Lorg/apache/commons/net/ftp/c;->AF:Ljavax/net/ServerSocketFactory;

    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kk()I

    move-result v4

    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kl()Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v2, v4, v0, v5}, Ljavax/net/ServerSocketFactory;->createServerSocket(IILjava/net/InetAddress;)Ljava/net/ServerSocket;

    move-result-object v2

    .line 657
    if-eqz v3, :cond_2

    .line 659
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kl()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v3

    invoke-virtual {p0, v0, v3}, Lorg/apache/commons/net/ftp/c;->b(Ljava/net/InetAddress;I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 661
    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    move-object v0, v1

    .line 662
    goto :goto_0

    .line 667
    :cond_2
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kl()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v3

    invoke-virtual {p0, v0, v3}, Lorg/apache/commons/net/ftp/c;->a(Ljava/net/InetAddress;I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 669
    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    move-object v0, v1

    .line 670
    goto :goto_0

    .line 674
    :cond_3
    iget-wide v4, p0, Lorg/apache/commons/net/ftp/c;->Bf:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    iget-wide v4, p0, Lorg/apache/commons/net/ftp/c;->Bf:J

    invoke-direct {p0, v4, v5}, Lorg/apache/commons/net/ftp/c;->c(J)Z

    move-result v0

    if-nez v0, :cond_4

    .line 676
    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    move-object v0, v1

    .line 677
    goto :goto_0

    .line 680
    :cond_4
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/net/ftp/c;->g(ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bu(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 682
    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    move-object v0, v1

    .line 683
    goto :goto_0

    .line 690
    :cond_5
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AT:I

    if-ltz v0, :cond_6

    .line 691
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->AT:I

    invoke-virtual {v2, v0}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    .line 693
    :cond_6
    :try_start_0
    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 695
    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    .line 740
    :cond_7
    iget-boolean v1, p0, Lorg/apache/commons/net/ftp/c;->Be:Z

    if-eqz v1, :cond_f

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/c;->a(Ljava/net/Socket;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 744
    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 745
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 747
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 749
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Host attempting data connection "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not same as server "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 695
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V

    throw v0

    .line 708
    :cond_8
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kr()Z

    move-result v4

    if-nez v4, :cond_9

    if-eqz v3, :cond_a

    .line 709
    :cond_9
    :goto_1
    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kc()I

    move-result v0

    const/16 v4, 0xe5

    if-ne v0, v4, :cond_b

    .line 711
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/c;->bC(Ljava/lang/String;)V

    .line 725
    :goto_2
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AE:Ljavax/net/SocketFactory;

    invoke-virtual {v0}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    .line 726
    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    iget v4, p0, Lorg/apache/commons/net/ftp/c;->AU:I

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iget v3, p0, Lorg/apache/commons/net/ftp/c;->AG:I

    invoke-virtual {v0, v2, v3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 727
    iget-wide v2, p0, Lorg/apache/commons/net/ftp/c;->Bf:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_e

    iget-wide v2, p0, Lorg/apache/commons/net/ftp/c;->Bf:J

    invoke-direct {p0, v2, v3}, Lorg/apache/commons/net/ftp/c;->c(J)Z

    move-result v2

    if-nez v2, :cond_e

    .line 729
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    move-object v0, v1

    .line 730
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 708
    goto :goto_1

    .line 715
    :cond_b
    if-eqz v3, :cond_c

    move-object v0, v1

    .line 716
    goto/16 :goto_0

    .line 719
    :cond_c
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->kb()I

    move-result v0

    const/16 v3, 0xe3

    if-eq v0, v3, :cond_d

    move-object v0, v1

    .line 720
    goto/16 :goto_0

    .line 722
    :cond_d
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/c;->bB(Ljava/lang/String;)V

    goto :goto_2

    .line 733
    :cond_e
    invoke-virtual {p0, p1, p2}, Lorg/apache/commons/net/ftp/c;->g(ILjava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lorg/apache/commons/net/ftp/m;->bu(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 735
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    move-object v0, v1

    .line 736
    goto/16 :goto_0

    .line 754
    :cond_f
    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AT:I

    if-ltz v1, :cond_0

    .line 755
    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AT:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    goto/16 :goto_0
.end method

.method protected jQ()V
    .locals 6

    .prologue
    .line 764
    invoke-super {p0}, Lorg/apache/commons/net/ftp/b;->jQ()V

    .line 765
    invoke-direct {p0}, Lorg/apache/commons/net/ftp/c;->kh()V

    .line 768
    iget-boolean v0, p0, Lorg/apache/commons/net/ftp/c;->Bs:Z

    if-eqz v0, :cond_2

    .line 770
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 771
    iget v1, p0, Lorg/apache/commons/net/ftp/c;->AJ:I

    .line 772
    const-string v2, "UTF8"

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/c;->bE(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/c;->bE(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 774
    :cond_0
    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/c;->bw(Ljava/lang/String;)V

    .line 775
    new-instance v2, Lorg/apache/commons/net/io/a;

    new-instance v3, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lorg/apache/commons/net/ftp/c;->AB:Ljava/io/InputStream;

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->jU()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lorg/apache/commons/net/io/a;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->AQ:Ljava/io/BufferedReader;

    .line 777
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    iget-object v4, p0, Lorg/apache/commons/net/ftp/c;->AC:Ljava/io/OutputStream;

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->jU()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v2, p0, Lorg/apache/commons/net/ftp/c;->AR:Ljava/io/BufferedWriter;

    .line 781
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 782
    iget-object v2, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 783
    iput v1, p0, Lorg/apache/commons/net/ftp/c;->AJ:I

    .line 785
    :cond_2
    return-void
.end method

.method public ki()Z
    .locals 1

    .prologue
    .line 941
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->ka()I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    return v0
.end method

.method public kj()V
    .locals 1

    .prologue
    .line 1065
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->AS:I

    .line 1068
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->AV:Ljava/lang/String;

    .line 1069
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/net/ftp/c;->AU:I

    .line 1070
    return-void
.end method

.method public km()Z
    .locals 1

    .prologue
    .line 1561
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->jX()I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    return v0
.end method

.method public ko()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2422
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bk:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2423
    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->ke()I

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/net/ftp/m;->bv(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2425
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/commons/net/ftp/c;->AK:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bk:Ljava/lang/String;

    .line 2430
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/net/ftp/c;->Bk:Ljava/lang/String;

    return-object v0

    .line 2427
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to determine system type - response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/commons/net/ftp/c;->jZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public kp()I
    .locals 1

    .prologue
    .line 3150
    iget v0, p0, Lorg/apache/commons/net/ftp/c;->Bh:I

    return v0
.end method

.method public kq()Z
    .locals 1

    .prologue
    .line 3185
    iget-boolean v0, p0, Lorg/apache/commons/net/ftp/c;->Bi:Z

    return v0
.end method

.method public kr()Z
    .locals 1

    .prologue
    .line 3195
    iget-boolean v0, p0, Lorg/apache/commons/net/ftp/c;->Bj:Z

    return v0
.end method
