.class public final Lorg/apache/commons/net/ftp/g;
.super Ljava/lang/Object;
.source "FTPCommand.java"


# static fields
.field private static final BI:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 141
    const/16 v0, 0x28

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "USER"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "PASS"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ACCT"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CWD"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CDUP"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SMNT"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "REIN"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "QUIT"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "PORT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "PASV"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TYPE"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "STRU"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "MODE"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "RETR"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "STOR"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "STOU"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "APPE"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "ALLO"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "REST"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "RNFR"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "RNTO"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ABOR"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "DELE"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "RMD"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "MKD"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "PWD"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "LIST"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "NLST"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "SITE"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "SYST"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "STAT"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "HELP"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "NOOP"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "MDTM"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "FEAT"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "MFMT"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "EPSV"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "EPRT"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "MLSD"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "MLST"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/commons/net/ftp/g;->BI:[Ljava/lang/String;

    return-void
.end method

.method public static final bs(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lorg/apache/commons/net/ftp/g;->BI:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method
