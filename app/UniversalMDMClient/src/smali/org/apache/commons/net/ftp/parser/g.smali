.class public Lorg/apache/commons/net/ftp/parser/g;
.super Lorg/apache/commons/net/ftp/parser/b;
.source "MVSFTPEntryParser.java"


# instance fields
.field private BU:I

.field private BV:Lorg/apache/commons/net/ftp/parser/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 217
    const-string v0, ""

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;-><init>(Ljava/lang/String;)V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    .line 218
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 219
    return-void
.end method

.method private a(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 282
    invoke-virtual {p0, p2}, Lorg/apache/commons/net/ftp/parser/g;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    invoke-virtual {p1, p2}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 284
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 285
    invoke-virtual {p0, v1}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 286
    invoke-virtual {p1, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 289
    const-string v2, "PS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 290
    invoke-virtual {p1, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    :goto_0
    move v0, v1

    .line 303
    :cond_0
    return v0

    .line 292
    :cond_1
    const-string v2, "PO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "PO-E"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    :cond_2
    invoke-virtual {p1, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    goto :goto_0
.end method

.method private b(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    invoke-virtual {p0, p2}, Lorg/apache/commons/net/ftp/parser/g;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    invoke-virtual {p1, p2}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 329
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 330
    invoke-virtual {p1, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 331
    invoke-virtual {p1, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 333
    :try_start_0
    invoke-super {p0, v3}, Lorg/apache/commons/net/ftp/parser/b;->bP(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/commons/net/ftp/FTPFile;->a(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :goto_0
    return v0

    .line 334
    :catch_0
    move-exception v0

    .line 335
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    move v0, v1

    .line 338
    goto :goto_0

    :cond_0
    move v0, v1

    .line 343
    goto :goto_0
.end method

.method private c(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 356
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 357
    invoke-virtual {p1, p2}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 358
    const-string v1, " "

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v0

    .line 359
    invoke-virtual {p1, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 360
    invoke-virtual {p1, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 361
    const/4 v0, 0x1

    .line 363
    :cond_0
    return v0
.end method

.method private d(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lorg/apache/commons/net/ftp/parser/g;->BV:Lorg/apache/commons/net/ftp/parser/m;

    invoke-virtual {v0, p2}, Lorg/apache/commons/net/ftp/parser/m;->bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    .line 375
    if-nez v0, :cond_0

    .line 376
    const/4 v0, 0x0

    .line 377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 395
    invoke-virtual {p0, p2}, Lorg/apache/commons/net/ftp/parser/g;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OUTPUT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 397
    invoke-virtual {p1, p2}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 398
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 399
    invoke-virtual {p1, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 400
    invoke-virtual {p1, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 401
    const/4 v0, 0x1

    .line 405
    :cond_0
    return v0
.end method

.method private f(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 424
    invoke-virtual {p0, p2}, Lorg/apache/commons/net/ftp/parser/g;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OUTPUT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 426
    invoke-virtual {p1, p2}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 427
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lorg/apache/commons/net/ftp/parser/g;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 428
    invoke-virtual {p1, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p1, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 430
    const/4 v0, 0x1

    .line 434
    :cond_0
    return v0
.end method


# virtual methods
.method public b(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 448
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 449
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 450
    const-string v1, "Volume"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_1

    const-string v1, "Dsname"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 451
    invoke-virtual {p0, v3}, Lorg/apache/commons/net/ftp/parser/g;->setType(I)V

    .line 452
    const-string v0, "\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s+[FV]\\S*\\s+\\S+\\s+\\S+\\s+(PS|PO|PO-E)\\s+(\\S+)\\s*"

    invoke-super {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;->bU(Ljava/lang/String;)Z

    .line 470
    :goto_0
    iget v0, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    if-eq v0, v4, :cond_0

    .line 471
    invoke-interface {p1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 475
    :cond_0
    return-object p1

    .line 453
    :cond_1
    const-string v1, "Name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_2

    const-string v1, "Id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_2

    .line 454
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/g;->setType(I)V

    .line 455
    const-string v0, "(\\S+)\\s+\\S+\\s+\\S+\\s+(\\S+)\\s+(\\S+)\\s+\\S+\\s+\\S+\\s+\\S+\\s+\\S+\\s*"

    invoke-super {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;->bU(Ljava/lang/String;)Z

    goto :goto_0

    .line 456
    :cond_2
    const-string v1, "total"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 457
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/g;->setType(I)V

    .line 458
    new-instance v0, Lorg/apache/commons/net/ftp/parser/m;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/parser/m;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/net/ftp/parser/g;->BV:Lorg/apache/commons/net/ftp/parser/m;

    goto :goto_0

    .line 459
    :cond_3
    const-string v1, "Spool Files"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x1e

    if-lt v1, v2, :cond_4

    .line 460
    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/g;->setType(I)V

    .line 461
    const-string v0, "(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s*"

    invoke-super {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;->bU(Ljava/lang/String;)Z

    goto :goto_0

    .line 462
    :cond_4
    const-string v1, "JOBNAME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "JOBID"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x8

    if-le v0, v1, :cond_5

    .line 464
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/g;->setType(I)V

    .line 465
    const-string v0, "(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+).*"

    invoke-super {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;->bU(Ljava/lang/String;)Z

    goto :goto_0

    .line 467
    :cond_5
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lorg/apache/commons/net/ftp/parser/g;->setType(I)V

    goto :goto_0
.end method

.method public bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 4

    .prologue
    .line 233
    const/4 v1, 0x0

    .line 234
    new-instance v0, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 236
    iget v2, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    if-nez v2, :cond_2

    .line 237
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/g;->a(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z

    move-result v1

    .line 250
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 251
    const/4 v0, 0x0

    .line 253
    :cond_1
    return-object v0

    .line 238
    :cond_2
    iget v2, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 239
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/g;->b(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z

    move-result v1

    .line 240
    if-nez v1, :cond_0

    .line 241
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/g;->c(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 242
    :cond_3
    iget v2, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 243
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/g;->d(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 244
    :cond_4
    iget v2, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    .line 245
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/g;->e(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 246
    :cond_5
    iget v2, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 247
    invoke-direct {p0, v0, p1}, Lorg/apache/commons/net/ftp/parser/g;->f(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method protected kC()Lorg/apache/commons/net/ftp/f;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 491
    new-instance v0, Lorg/apache/commons/net/ftp/f;

    const-string v1, "MVS"

    const-string v2, "yyyy/MM/dd HH:mm"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method setType(I)V
    .locals 0

    .prologue
    .line 483
    iput p1, p0, Lorg/apache/commons/net/ftp/parser/g;->BU:I

    .line 484
    return-void
.end method
