.class public Lorg/apache/commons/net/ftp/parser/m;
.super Lorg/apache/commons/net/ftp/parser/b;
.source "UnixFTPEntryParser.java"


# static fields
.field public static final BZ:Lorg/apache/commons/net/ftp/f;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 60
    new-instance v0, Lorg/apache/commons/net/ftp/f;

    const-string v1, "UNIX"

    const-string v2, "yyyy-MM-dd HH:mm"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/apache/commons/net/ftp/parser/m;->BZ:Lorg/apache/commons/net/ftp/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/m;-><init>(Lorg/apache/commons/net/ftp/f;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/net/ftp/f;)V
    .locals 1

    .prologue
    .line 138
    const-string v0, "([bcdelfmpSs-])(((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-]))((r|-)(w|-)([xsStTL-])))\\+?\\s*(\\d+)\\s+(?:(\\S+(?:\\s\\S+)*?)\\s+)?(?:(\\S+(?:\\s\\S+)*)\\s+)?(\\d+(?:,\\s*\\d+)?)\\s+((?:\\d+[-/]\\d+[-/]\\d+)|(?:\\S{3}\\s+\\d{1,2})|(?:\\d{1,2}\\s+\\S{3}))\\s+(\\d+(?::\\d+)?)\\s+(\\S*)(\\s*.*)"

    invoke-direct {p0, v0}, Lorg/apache/commons/net/ftp/parser/b;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/m;->a(Lorg/apache/commons/net/ftp/f;)V

    .line 140
    return-void
.end method


# virtual methods
.method public b(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 148
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 150
    const-string v2, "^total \\d+$"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 154
    :cond_1
    return-object p1
.end method

.method public bO(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPFile;
    .locals 14

    .prologue
    .line 168
    new-instance v3, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v3}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 169
    invoke-virtual {v3, p1}, Lorg/apache/commons/net/ftp/FTPFile;->bM(Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x0

    .line 173
    invoke-virtual {p0, p1}, Lorg/apache/commons/net/ftp/parser/m;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 175
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 176
    const/16 v2, 0xf

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 177
    const/16 v2, 0x10

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 178
    const/16 v2, 0x11

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 179
    const/16 v2, 0x12

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 180
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x13

    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x14

    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 181
    const/16 v4, 0x15

    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v10

    .line 182
    const/16 v4, 0x16

    invoke-virtual {p0, v4}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v11

    .line 186
    :try_start_0
    invoke-super {p0, v2}, Lorg/apache/commons/net/ftp/parser/b;->bP(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/apache/commons/net/ftp/FTPFile;->a(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_2

    .line 197
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 218
    const/4 v1, 0x3

    .line 221
    :goto_1
    invoke-virtual {v3, v1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 223
    const/4 v4, 0x4

    .line 224
    const/4 v2, 0x0

    move v5, v4

    move v4, v2

    :goto_2
    const/4 v2, 0x3

    if-ge v4, v2, :cond_3

    .line 227
    const/4 v12, 0x0

    invoke-virtual {p0, v5}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v2

    const-string v13, "-"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v3, v4, v12, v2}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    .line 229
    const/4 v12, 0x1

    add-int/lit8 v2, v5, 0x1

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v2

    const-string v13, "-"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_4
    invoke-virtual {v3, v4, v12, v2}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    .line 232
    add-int/lit8 v2, v5, 0x2

    invoke-virtual {p0, v2}, Lorg/apache/commons/net/ftp/parser/m;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 233
    const-string v12, "-"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const/4 v12, 0x0

    invoke-virtual {v2, v12}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v2

    if-nez v2, :cond_2

    .line 235
    const/4 v2, 0x2

    const/4 v12, 0x1

    invoke-virtual {v3, v4, v2, v12}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    .line 224
    :goto_5
    add-int/lit8 v2, v4, 0x1

    add-int/lit8 v4, v5, 0x4

    move v5, v4

    move v4, v2

    goto :goto_2

    .line 200
    :sswitch_0
    const/4 v1, 0x1

    .line 201
    goto :goto_1

    .line 203
    :sswitch_1
    const/4 v1, 0x2

    .line 204
    goto :goto_1

    .line 206
    :sswitch_2
    const/4 v1, 0x2

    .line 207
    goto :goto_1

    .line 210
    :sswitch_3
    const/4 v0, 0x1

    .line 215
    :sswitch_4
    const/4 v1, 0x0

    .line 216
    goto :goto_1

    .line 227
    :cond_0
    const/4 v2, 0x0

    goto :goto_3

    .line 229
    :cond_1
    const/4 v2, 0x0

    goto :goto_4

    .line 239
    :cond_2
    const/4 v2, 0x2

    const/4 v12, 0x0

    invoke-virtual {v3, v4, v2, v12}, Lorg/apache/commons/net/ftp/FTPFile;->b(IIZ)V

    goto :goto_5

    .line 243
    :cond_3
    if-nez v0, :cond_4

    .line 247
    :try_start_1
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lorg/apache/commons/net/ftp/FTPFile;->bt(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 255
    :cond_4
    :goto_6
    invoke-virtual {v3, v7}, Lorg/apache/commons/net/ftp/FTPFile;->setUser(Ljava/lang/String;)V

    .line 256
    invoke-virtual {v3, v8}, Lorg/apache/commons/net/ftp/FTPFile;->setGroup(Ljava/lang/String;)V

    .line 260
    :try_start_2
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    .line 267
    :goto_7
    if-nez v11, :cond_5

    .line 269
    invoke-virtual {v3, v10}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    :goto_8
    move-object v0, v3

    .line 299
    :goto_9
    return-object v0

    .line 275
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 276
    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 279
    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 281
    const/4 v2, -0x1

    if-ne v1, v2, :cond_6

    .line 283
    invoke-virtual {v3, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    goto :goto_8

    .line 287
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 288
    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/apache/commons/net/ftp/FTPFile;->bN(Ljava/lang/String;)V

    goto :goto_8

    .line 294
    :cond_7
    invoke-virtual {v3, v0}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    goto :goto_8

    .line 299
    :cond_8
    const/4 v0, 0x0

    goto :goto_9

    .line 262
    :catch_0
    move-exception v0

    goto :goto_7

    .line 249
    :catch_1
    move-exception v0

    goto :goto_6

    .line 188
    :catch_2
    move-exception v2

    goto/16 :goto_0

    .line 197
    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_4
        0x62 -> :sswitch_3
        0x63 -> :sswitch_3
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_4
        0x6c -> :sswitch_2
    .end sparse-switch
.end method

.method protected kC()Lorg/apache/commons/net/ftp/f;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 310
    new-instance v0, Lorg/apache/commons/net/ftp/f;

    const-string v1, "UNIX"

    const-string v2, "MMM d yyyy"

    const-string v3, "MMM d HH:mm"

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lorg/apache/commons/net/ftp/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
