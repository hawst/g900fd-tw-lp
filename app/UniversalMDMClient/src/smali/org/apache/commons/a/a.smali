.class public Lorg/apache/commons/a/a;
.super Ljava/lang/Object;
.source "ArrayUtils.java"


# static fields
.field public static final Ac:[Ljava/lang/Object;

.field public static final Ad:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final Ae:[Ljava/lang/String;

.field public static final Af:[J

.field public static final Ag:[Ljava/lang/Long;

.field public static final Ah:[I

.field public static final Ai:[Ljava/lang/Integer;

.field public static final Aj:[Ljava/lang/Short;

.field public static final Ak:[Ljava/lang/Byte;

.field public static final Al:[D

.field public static final Am:[Ljava/lang/Double;

.field public static final An:[F

.field public static final Ao:[Ljava/lang/Float;

.field public static final Ap:[Z

.field public static final Aq:[Ljava/lang/Boolean;

.field public static final Ar:[C

.field public static final As:[Ljava/lang/Character;

.field public static final EMPTY_BYTE_ARRAY:[B

.field public static final EMPTY_SHORT_ARRAY:[S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lorg/apache/commons/a/a;->Ac:[Ljava/lang/Object;

    .line 53
    new-array v0, v1, [Ljava/lang/Class;

    sput-object v0, Lorg/apache/commons/a/a;->Ad:[Ljava/lang/Class;

    .line 57
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lorg/apache/commons/a/a;->Ae:[Ljava/lang/String;

    .line 61
    new-array v0, v1, [J

    sput-object v0, Lorg/apache/commons/a/a;->Af:[J

    .line 65
    new-array v0, v1, [Ljava/lang/Long;

    sput-object v0, Lorg/apache/commons/a/a;->Ag:[Ljava/lang/Long;

    .line 69
    new-array v0, v1, [I

    sput-object v0, Lorg/apache/commons/a/a;->Ah:[I

    .line 73
    new-array v0, v1, [Ljava/lang/Integer;

    sput-object v0, Lorg/apache/commons/a/a;->Ai:[Ljava/lang/Integer;

    .line 77
    new-array v0, v1, [S

    sput-object v0, Lorg/apache/commons/a/a;->EMPTY_SHORT_ARRAY:[S

    .line 81
    new-array v0, v1, [Ljava/lang/Short;

    sput-object v0, Lorg/apache/commons/a/a;->Aj:[Ljava/lang/Short;

    .line 85
    new-array v0, v1, [B

    sput-object v0, Lorg/apache/commons/a/a;->EMPTY_BYTE_ARRAY:[B

    .line 89
    new-array v0, v1, [Ljava/lang/Byte;

    sput-object v0, Lorg/apache/commons/a/a;->Ak:[Ljava/lang/Byte;

    .line 93
    new-array v0, v1, [D

    sput-object v0, Lorg/apache/commons/a/a;->Al:[D

    .line 97
    new-array v0, v1, [Ljava/lang/Double;

    sput-object v0, Lorg/apache/commons/a/a;->Am:[Ljava/lang/Double;

    .line 101
    new-array v0, v1, [F

    sput-object v0, Lorg/apache/commons/a/a;->An:[F

    .line 105
    new-array v0, v1, [Ljava/lang/Float;

    sput-object v0, Lorg/apache/commons/a/a;->Ao:[Ljava/lang/Float;

    .line 109
    new-array v0, v1, [Z

    sput-object v0, Lorg/apache/commons/a/a;->Ap:[Z

    .line 113
    new-array v0, v1, [Ljava/lang/Boolean;

    sput-object v0, Lorg/apache/commons/a/a;->Aq:[Ljava/lang/Boolean;

    .line 117
    new-array v0, v1, [C

    sput-object v0, Lorg/apache/commons/a/a;->Ar:[C

    .line 121
    new-array v0, v1, [Ljava/lang/Character;

    sput-object v0, Lorg/apache/commons/a/a;->As:[Ljava/lang/Character;

    return-void
.end method

.method public static a([Ljava/lang/Byte;)[B
    .locals 3

    .prologue
    .line 2963
    if-nez p0, :cond_0

    .line 2964
    const/4 v0, 0x0

    .line 2972
    :goto_0
    return-object v0

    .line 2965
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2966
    sget-object v0, Lorg/apache/commons/a/a;->EMPTY_BYTE_ARRAY:[B

    goto :goto_0

    .line 2968
    :cond_1
    array-length v0, p0

    new-array v1, v0, [B

    .line 2969
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2970
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v1, v0

    .line 2969
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2972
    goto :goto_0
.end method

.method public static a([Ljava/lang/Character;)[C
    .locals 3

    .prologue
    .line 2691
    if-nez p0, :cond_0

    .line 2692
    const/4 v0, 0x0

    .line 2700
    :goto_0
    return-object v0

    .line 2693
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2694
    sget-object v0, Lorg/apache/commons/a/a;->Ar:[C

    goto :goto_0

    .line 2696
    :cond_1
    array-length v0, p0

    new-array v1, v0, [C

    .line 2697
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2698
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    aput-char v2, v1, v0

    .line 2697
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2700
    goto :goto_0
.end method

.method public static a([Ljava/lang/Double;)[D
    .locals 4

    .prologue
    .line 3031
    if-nez p0, :cond_0

    .line 3032
    const/4 v0, 0x0

    .line 3040
    :goto_0
    return-object v0

    .line 3033
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3034
    sget-object v0, Lorg/apache/commons/a/a;->Al:[D

    goto :goto_0

    .line 3036
    :cond_1
    array-length v0, p0

    new-array v1, v0, [D

    .line 3037
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 3038
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 3037
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3040
    goto :goto_0
.end method

.method public static a([Ljava/lang/Float;)[F
    .locals 3

    .prologue
    .line 3099
    if-nez p0, :cond_0

    .line 3100
    const/4 v0, 0x0

    .line 3108
    :goto_0
    return-object v0

    .line 3101
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3102
    sget-object v0, Lorg/apache/commons/a/a;->An:[F

    goto :goto_0

    .line 3104
    :cond_1
    array-length v0, p0

    new-array v1, v0, [F

    .line 3105
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 3106
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    aput v2, v1, v0

    .line 3105
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3108
    goto :goto_0
.end method

.method public static a([Ljava/lang/Integer;)[I
    .locals 3

    .prologue
    .line 2827
    if-nez p0, :cond_0

    .line 2828
    const/4 v0, 0x0

    .line 2836
    :goto_0
    return-object v0

    .line 2829
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2830
    sget-object v0, Lorg/apache/commons/a/a;->Ah:[I

    goto :goto_0

    .line 2832
    :cond_1
    array-length v0, p0

    new-array v1, v0, [I

    .line 2833
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2834
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 2833
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2836
    goto :goto_0
.end method

.method public static a([Ljava/lang/Long;)[J
    .locals 4

    .prologue
    .line 2759
    if-nez p0, :cond_0

    .line 2760
    const/4 v0, 0x0

    .line 2768
    :goto_0
    return-object v0

    .line 2761
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2762
    sget-object v0, Lorg/apache/commons/a/a;->Af:[J

    goto :goto_0

    .line 2764
    :cond_1
    array-length v0, p0

    new-array v1, v0, [J

    .line 2765
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2766
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 2765
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2768
    goto :goto_0
.end method

.method public static a([Z)[Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3211
    if-nez p0, :cond_0

    .line 3212
    const/4 v0, 0x0

    .line 3220
    :goto_0
    return-object v0

    .line 3213
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3214
    sget-object v0, Lorg/apache/commons/a/a;->Aq:[Ljava/lang/Boolean;

    goto :goto_0

    .line 3216
    :cond_1
    array-length v0, p0

    new-array v2, v0, [Ljava/lang/Boolean;

    .line 3217
    const/4 v0, 0x0

    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_3

    .line 3218
    aget-boolean v1, p0, v0

    if-eqz v1, :cond_2

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_2
    aput-object v1, v2, v0

    .line 3217
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3218
    :cond_2
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 3220
    goto :goto_0
.end method

.method public static a([D)[Ljava/lang/Double;
    .locals 4

    .prologue
    .line 3075
    if-nez p0, :cond_0

    .line 3076
    const/4 v0, 0x0

    .line 3084
    :goto_0
    return-object v0

    .line 3077
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3078
    sget-object v0, Lorg/apache/commons/a/a;->Am:[Ljava/lang/Double;

    goto :goto_0

    .line 3080
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Double;

    .line 3081
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 3082
    aget-wide v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3081
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3084
    goto :goto_0
.end method

.method public static a([F)[Ljava/lang/Float;
    .locals 3

    .prologue
    .line 3143
    if-nez p0, :cond_0

    .line 3144
    const/4 v0, 0x0

    .line 3152
    :goto_0
    return-object v0

    .line 3145
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3146
    sget-object v0, Lorg/apache/commons/a/a;->Ao:[Ljava/lang/Float;

    goto :goto_0

    .line 3148
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Float;

    .line 3149
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 3150
    aget v2, p0, v0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3149
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3152
    goto :goto_0
.end method

.method public static a([I)[Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 2871
    if-nez p0, :cond_0

    .line 2872
    const/4 v0, 0x0

    .line 2880
    :goto_0
    return-object v0

    .line 2873
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2874
    sget-object v0, Lorg/apache/commons/a/a;->Ai:[Ljava/lang/Integer;

    goto :goto_0

    .line 2876
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Integer;

    .line 2877
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2878
    aget v2, p0, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2877
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2880
    goto :goto_0
.end method

.method public static a([J)[Ljava/lang/Long;
    .locals 4

    .prologue
    .line 2803
    if-nez p0, :cond_0

    .line 2804
    const/4 v0, 0x0

    .line 2812
    :goto_0
    return-object v0

    .line 2805
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2806
    sget-object v0, Lorg/apache/commons/a/a;->Ag:[Ljava/lang/Long;

    goto :goto_0

    .line 2808
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Long;

    .line 2809
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2810
    aget-wide v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2809
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2812
    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 320
    if-nez p0, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 323
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;[TT;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 3449
    if-nez p0, :cond_0

    .line 3450
    invoke-static {p1}, Lorg/apache/commons/a/a;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 3474
    :goto_0
    return-object v0

    .line 3451
    :cond_0
    if-nez p1, :cond_1

    .line 3452
    invoke-static {p0}, Lorg/apache/commons/a/a;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 3454
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    .line 3456
    array-length v0, p0

    array-length v2, p1

    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 3457
    array-length v2, p0

    invoke-static {p0, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3459
    const/4 v2, 0x0

    :try_start_0
    array-length v3, p0

    array-length v4, p1

    invoke-static {p1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/ArrayStoreException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3460
    :catch_0
    move-exception v0

    .line 3467
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    .line 3468
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 3469
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot store "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " in an array of "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 3472
    :cond_2
    throw v0
.end method

.method public static a([S)[Ljava/lang/Short;
    .locals 3

    .prologue
    .line 2939
    if-nez p0, :cond_0

    .line 2940
    const/4 v0, 0x0

    .line 2948
    :goto_0
    return-object v0

    .line 2941
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2942
    sget-object v0, Lorg/apache/commons/a/a;->Aj:[Ljava/lang/Short;

    goto :goto_0

    .line 2944
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Short;

    .line 2945
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2946
    aget-short v2, p0, v0

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2945
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2948
    goto :goto_0
.end method

.method public static a([Ljava/lang/Short;)[S
    .locals 3

    .prologue
    .line 2895
    if-nez p0, :cond_0

    .line 2896
    const/4 v0, 0x0

    .line 2904
    :goto_0
    return-object v0

    .line 2897
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2898
    sget-object v0, Lorg/apache/commons/a/a;->EMPTY_SHORT_ARRAY:[S

    goto :goto_0

    .line 2900
    :cond_1
    array-length v0, p0

    new-array v1, v0, [S

    .line 2901
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2902
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Short;->shortValue()S

    move-result v2

    aput-short v2, v1, v0

    .line 2901
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2904
    goto :goto_0
.end method

.method public static a([Ljava/lang/Boolean;)[Z
    .locals 3

    .prologue
    .line 3167
    if-nez p0, :cond_0

    .line 3168
    const/4 v0, 0x0

    .line 3176
    :goto_0
    return-object v0

    .line 3169
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3170
    sget-object v0, Lorg/apache/commons/a/a;->Ap:[Z

    goto :goto_0

    .line 3172
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Z

    .line 3173
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 3174
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 3173
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3176
    goto :goto_0
.end method

.method public static b([C)[Ljava/lang/Character;
    .locals 3

    .prologue
    .line 2735
    if-nez p0, :cond_0

    .line 2736
    const/4 v0, 0x0

    .line 2744
    :goto_0
    return-object v0

    .line 2737
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 2738
    sget-object v0, Lorg/apache/commons/a/a;->As:[Ljava/lang/Character;

    goto :goto_0

    .line 2740
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Character;

    .line 2741
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2742
    aget-char v2, p0, v0

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2741
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2744
    goto :goto_0
.end method

.method public static f([B)[Ljava/lang/Byte;
    .locals 3

    .prologue
    .line 3007
    if-nez p0, :cond_0

    .line 3008
    const/4 v0, 0x0

    .line 3016
    :goto_0
    return-object v0

    .line 3009
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 3010
    sget-object v0, Lorg/apache/commons/a/a;->Ak:[Ljava/lang/Byte;

    goto :goto_0

    .line 3012
    :cond_1
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/Byte;

    .line 3013
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 3014
    aget-byte v2, p0, v0

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3013
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3016
    goto :goto_0
.end method
