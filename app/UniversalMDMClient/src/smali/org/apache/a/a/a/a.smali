.class public Lorg/apache/a/a/a/a;
.super Ljava/lang/Object;
.source "SSLSocketFactory.java"

# interfaces
.implements Lorg/apache/http/conn/scheme/LayeredSocketFactory;


# static fields
.field public static final dp:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

.field public static final dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

.field public static final dr:Lorg/apache/http/conn/ssl/X509HostnameVerifier;


# instance fields
.field private final dt:Ljavax/net/ssl/SSLContext;

.field private final du:Ljavax/net/ssl/SSLSocketFactory;

.field private final dv:Lorg/apache/http/conn/scheme/HostNameResolver;

.field private dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;-><init>()V

    .line 145
    sput-object v0, Lorg/apache/a/a/a/a;->dp:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 149
    new-instance v0, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    .line 148
    sput-object v0, Lorg/apache/a/a/a/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 152
    new-instance v0, Lorg/apache/http/conn/ssl/StrictHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/StrictHostnameVerifier;-><init>()V

    .line 151
    sput-object v0, Lorg/apache/a/a/a/a;->dr:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 152
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    sget-object v0, Lorg/apache/a/a/a/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    iput-object v0, p0, Lorg/apache/a/a/a/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 246
    iput-object v1, p0, Lorg/apache/a/a/a/a;->dt:Ljavax/net/ssl/SSLContext;

    .line 247
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/a/a/a/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    .line 248
    iput-object v1, p0, Lorg/apache/a/a/a/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    .line 249
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;Ljava/security/SecureRandom;Lorg/apache/http/conn/scheme/HostNameResolver;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    sget-object v1, Lorg/apache/a/a/a/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    iput-object v1, p0, Lorg/apache/a/a/a/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 188
    if-nez p1, :cond_0

    .line 189
    const-string p1, "TLS"

    .line 192
    :cond_0
    if-eqz p2, :cond_2

    .line 193
    invoke-static {p2, p3}, Lorg/apache/a/a/a/a;->a(Ljava/security/KeyStore;Ljava/lang/String;)[Ljavax/net/ssl/KeyManager;

    move-result-object v1

    .line 196
    :goto_0
    if-eqz p4, :cond_1

    .line 197
    invoke-static {p4}, Lorg/apache/a/a/a/a;->a(Ljava/security/KeyStore;)[Ljavax/net/ssl/TrustManager;

    move-result-object v0

    .line 199
    :cond_1
    invoke-static {p1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/a/a/a/a;->dt:Ljavax/net/ssl/SSLContext;

    .line 200
    iget-object v2, p0, Lorg/apache/a/a/a/a;->dt:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v2, v1, v0, p5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 201
    iget-object v0, p0, Lorg/apache/a/a/a/a;->dt:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/a/a/a/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    .line 202
    iput-object p6, p0, Lorg/apache/a/a/a/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    .line 203
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/security/KeyStore;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 223
    const-string v1, "TLS"

    move-object v0, p0

    move-object v3, v2

    move-object v4, p1

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lorg/apache/a/a/a/a;-><init>(Ljava/lang/String;Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;Ljava/security/SecureRandom;Lorg/apache/http/conn/scheme/HostNameResolver;)V

    .line 224
    return-void
.end method

.method public constructor <init>(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    sget-object v0, Lorg/apache/a/a/a/a;->dq:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    iput-object v0, p0, Lorg/apache/a/a/a/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 234
    iput-object v1, p0, Lorg/apache/a/a/a/a;->dt:Ljavax/net/ssl/SSLContext;

    .line 235
    iput-object p1, p0, Lorg/apache/a/a/a/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    .line 236
    iput-object v1, p0, Lorg/apache/a/a/a/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    .line 237
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/a/a/a/a;)V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0}, Lorg/apache/a/a/a/a;-><init>()V

    return-void
.end method

.method private static a(Ljava/security/KeyStore;Ljava/lang/String;)[Ljavax/net/ssl/KeyManager;
    .locals 2

    .prologue
    .line 253
    if-nez p0, :cond_0

    .line 254
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Keystore may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 256
    invoke-static {v0}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v1

    .line 258
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    :goto_0
    invoke-virtual {v1, p0, v0}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    .line 259
    invoke-virtual {v1}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object v0

    return-object v0

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/security/KeyStore;)[Ljavax/net/ssl/TrustManager;
    .locals 2

    .prologue
    .line 264
    if-nez p0, :cond_0

    .line 265
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Keystore may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 267
    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 269
    invoke-virtual {v0, p0}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 270
    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v0

    return-object v0
.end method

.method public static kJ()Lorg/apache/a/a/a/a;
    .locals 1

    .prologue
    .line 170
    invoke-static {}, Lorg/apache/a/a/a/b;->kK()Lorg/apache/a/a/a/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V
    .locals 2

    .prologue
    .line 394
    if-nez p1, :cond_0

    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Hostname verifier may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_0
    iput-object p1, p0, Lorg/apache/a/a/a/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .line 398
    return-void
.end method

.method public connectSocket(Ljava/net/Socket;Ljava/lang/String;ILjava/net/InetAddress;ILorg/apache/http/params/HttpParams;)Ljava/net/Socket;
    .locals 5

    .prologue
    .line 293
    if-nez p2, :cond_0

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target host may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_0
    if-nez p6, :cond_1

    .line 297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameters may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_1
    if-eqz p1, :cond_5

    move-object v0, p1

    .line 300
    :goto_0
    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 303
    if-nez p4, :cond_2

    if-lez p5, :cond_4

    .line 306
    :cond_2
    if-gez p5, :cond_3

    .line 307
    const/4 p5, 0x0

    .line 310
    :cond_3
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p4, p5}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 311
    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->bind(Ljava/net/SocketAddress;)V

    .line 314
    :cond_4
    invoke-static {p6}, Lorg/apache/http/params/HttpConnectionParams;->getConnectionTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v2

    .line 315
    invoke-static {p6}, Lorg/apache/http/params/HttpConnectionParams;->getSoTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v3

    .line 318
    iget-object v1, p0, Lorg/apache/a/a/a/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    if-eqz v1, :cond_6

    .line 319
    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lorg/apache/a/a/a/a;->dv:Lorg/apache/http/conn/scheme/HostNameResolver;

    invoke-interface {v4, p2}, Lorg/apache/http/conn/scheme/HostNameResolver;->resolve(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    invoke-direct {v1, v4, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 324
    :goto_1
    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    .line 326
    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 328
    :try_start_0
    iget-object v1, p0, Lorg/apache/a/a/a/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-interface {v1, p2, v0}, Lorg/apache/http/conn/ssl/X509HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSocket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    return-object v0

    .line 301
    :cond_5
    invoke-virtual {p0}, Lorg/apache/a/a/a/a;->createSocket()Ljava/net/Socket;

    move-result-object v0

    goto :goto_0

    .line 321
    :cond_6
    new-instance v1, Ljava/net/InetSocketAddress;

    invoke-direct {v1, p2, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    goto :goto_1

    .line 330
    :catch_0
    move-exception v1

    .line 332
    :try_start_1
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 333
    :goto_2
    throw v1

    .line 332
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public createSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lorg/apache/a/a/a/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lorg/apache/a/a/a/a;->du:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 388
    iget-object v1, p0, Lorg/apache/a/a/a/a;->dw:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-interface {v1, p2, v0}, Lorg/apache/http/conn/ssl/X509HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSocket;)V

    .line 390
    return-object v0
.end method

.method public isSecure(Ljava/net/Socket;)Z
    .locals 2

    .prologue
    .line 357
    if-nez p1, :cond_0

    .line 358
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_0
    instance-of v0, p1, Ljavax/net/ssl/SSLSocket;

    if-nez v0, :cond_1

    .line 362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 363
    const-string v1, "Socket not created by this factory."

    .line 362
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_1
    invoke-virtual {p1}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
