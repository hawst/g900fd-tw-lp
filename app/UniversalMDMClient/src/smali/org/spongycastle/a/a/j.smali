.class public abstract Lorg/spongycastle/a/a/j;
.super Ljava/lang/Object;
.source "ECPoint.java"


# static fields
.field private static converter:Lorg/spongycastle/asn1/r/k;


# instance fields
.field UV:Lorg/spongycastle/a/a/c;

.field aeO:Lorg/spongycastle/a/a/f;

.field aeP:Lorg/spongycastle/a/a/f;

.field protected aeQ:Lorg/spongycastle/a/a/i;

.field protected aeR:Lorg/spongycastle/a/a/o;

.field protected withCompression:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lorg/spongycastle/asn1/r/k;

    invoke-direct {v0}, Lorg/spongycastle/asn1/r/k;-><init>()V

    sput-object v0, Lorg/spongycastle/a/a/j;->converter:Lorg/spongycastle/asn1/r/k;

    return-void
.end method

.method protected constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lorg/spongycastle/a/a/j;->aeQ:Lorg/spongycastle/a/a/i;

    .line 20
    iput-object v0, p0, Lorg/spongycastle/a/a/j;->aeR:Lorg/spongycastle/a/a/o;

    .line 26
    iput-object p1, p0, Lorg/spongycastle/a/a/j;->UV:Lorg/spongycastle/a/a/c;

    .line 27
    iput-object p2, p0, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    .line 28
    iput-object p3, p0, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    .line 29
    return-void
.end method

.method static synthetic sA()Lorg/spongycastle/asn1/r/k;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lorg/spongycastle/a/a/j;->converter:Lorg/spongycastle/asn1/r/k;

    return-object v0
.end method


# virtual methods
.method a(Lorg/spongycastle/a/a/o;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lorg/spongycastle/a/a/j;->aeR:Lorg/spongycastle/a/a/o;

    .line 109
    return-void
.end method

.method public abstract b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;
.end method

.method public abstract c(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;
.end method

.method public e(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p1}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-gez v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The multiplicator cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/a/a/j;->sv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    :goto_0
    return-object p0

    .line 146
    :cond_1
    invoke-virtual {p1}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-nez v0, :cond_2

    .line 148
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object p0

    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {p0}, Lorg/spongycastle/a/a/j;->sz()V

    .line 152
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeQ:Lorg/spongycastle/a/a/i;

    iget-object v1, p0, Lorg/spongycastle/a/a/j;->aeR:Lorg/spongycastle/a/a/o;

    invoke-interface {v0, p0, p1, v1}, Lorg/spongycastle/a/a/i;->a(Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Lorg/spongycastle/a/a/o;)Lorg/spongycastle/a/a/j;

    move-result-object p0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    instance-of v2, p1, Lorg/spongycastle/a/a/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 66
    goto :goto_0

    .line 69
    :cond_2
    check-cast p1, Lorg/spongycastle/a/a/j;

    .line 71
    invoke-virtual {p0}, Lorg/spongycastle/a/a/j;->sv()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 73
    invoke-virtual {p1}, Lorg/spongycastle/a/a/j;->sv()Z

    move-result v0

    goto :goto_0

    .line 76
    :cond_3
    iget-object v2, p0, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v3, p1, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    iget-object v3, p1, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public abstract getEncoded()[B
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lorg/spongycastle/a/a/j;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    goto :goto_0
.end method

.method public pO()Lorg/spongycastle/a/a/c;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->UV:Lorg/spongycastle/a/a/c;

    return-object v0
.end method

.method public st()Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    return-object v0
.end method

.method public su()Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    return-object v0
.end method

.method public sv()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sw()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lorg/spongycastle/a/a/j;->withCompression:Z

    return v0
.end method

.method public abstract sx()Lorg/spongycastle/a/a/j;
.end method

.method public abstract sy()Lorg/spongycastle/a/a/j;
.end method

.method declared-synchronized sz()V
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->aeQ:Lorg/spongycastle/a/a/i;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lorg/spongycastle/a/a/m;

    invoke-direct {v0}, Lorg/spongycastle/a/a/m;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/a/a/j;->aeQ:Lorg/spongycastle/a/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
