.class public Lorg/spongycastle/a/a/a;
.super Ljava/lang/Object;
.source "ECAlgorithms.java"


# direct methods
.method public static a(Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;
    .locals 2

    .prologue
    .line 10
    invoke-virtual {p0}, Lorg/spongycastle/a/a/j;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    .line 11
    invoke-virtual {p2}, Lorg/spongycastle/a/a/j;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "P and Q must be on same curve"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    instance-of v1, v0, Lorg/spongycastle/a/a/d;

    if-eqz v1, :cond_1

    .line 19
    check-cast v0, Lorg/spongycastle/a/a/d;

    .line 20
    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->si()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    invoke-virtual {p0, p1}, Lorg/spongycastle/a/a/j;->e(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    invoke-virtual {p2, p3}, Lorg/spongycastle/a/a/j;->e(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/j;->b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lorg/spongycastle/a/a/a;->b(Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;)Lorg/spongycastle/a/a/j;
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    invoke-virtual {p3}, Ljava/math/BigInteger;->bitLength()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 63
    invoke-virtual {p0, p2}, Lorg/spongycastle/a/a/j;->b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;

    move-result-object v2

    .line 64
    invoke-virtual {p0}, Lorg/spongycastle/a/a/j;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object v1

    .line 66
    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    if-ltz v1, :cond_3

    .line 68
    invoke-virtual {v0}, Lorg/spongycastle/a/a/j;->sy()Lorg/spongycastle/a/a/j;

    move-result-object v0

    .line 70
    invoke-virtual {p1, v1}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    invoke-virtual {p3, v1}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/j;->b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    .line 66
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v0, p0}, Lorg/spongycastle/a/a/j;->b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    goto :goto_1

    .line 83
    :cond_2
    invoke-virtual {p3, v1}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 85
    invoke-virtual {v0, p2}, Lorg/spongycastle/a/a/j;->b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;

    move-result-object v0

    goto :goto_1

    .line 90
    :cond_3
    return-object v0
.end method
