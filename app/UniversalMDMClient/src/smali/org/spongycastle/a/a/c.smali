.class public abstract Lorg/spongycastle/a/a/c;
.super Ljava/lang/Object;
.source "ECCurve.java"


# instance fields
.field aeC:Lorg/spongycastle/a/a/f;

.field aeD:Lorg/spongycastle/a/a/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    return-void
.end method


# virtual methods
.method public abstract R([B)Lorg/spongycastle/a/a/j;
.end method

.method public abstract a(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lorg/spongycastle/a/a/j;
.end method

.method public abstract d(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/f;
.end method

.method public abstract getFieldSize()I
.end method

.method public abstract sf()Lorg/spongycastle/a/a/j;
.end method

.method public sg()Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lorg/spongycastle/a/a/c;->aeC:Lorg/spongycastle/a/a/f;

    return-object v0
.end method

.method public sh()Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lorg/spongycastle/a/a/c;->aeD:Lorg/spongycastle/a/a/f;

    return-object v0
.end method
