.class public Lorg/spongycastle/a/a/g;
.super Lorg/spongycastle/a/a/f;
.source "ECFieldElement.java"


# instance fields
.field private acJ:I

.field private aeE:I

.field private aeF:I

.field private aeG:I

.field private aeL:I

.field private aeM:Lorg/spongycastle/a/a/n;

.field private aeN:I


# direct methods
.method public constructor <init>(IIIILjava/math/BigInteger;)V
    .locals 2

    .prologue
    .line 861
    invoke-direct {p0}, Lorg/spongycastle/a/a/f;-><init>()V

    .line 863
    add-int/lit8 v0, p1, 0x1f

    shr-int/lit8 v0, v0, 0x5

    iput v0, p0, Lorg/spongycastle/a/a/g;->aeN:I

    .line 864
    new-instance v0, Lorg/spongycastle/a/a/n;

    iget v1, p0, Lorg/spongycastle/a/a/g;->aeN:I

    invoke-direct {v0, p5, v1}, Lorg/spongycastle/a/a/n;-><init>(Ljava/math/BigInteger;I)V

    iput-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    .line 866
    if-nez p3, :cond_0

    if-nez p4, :cond_0

    .line 868
    const/4 v0, 0x2

    iput v0, p0, Lorg/spongycastle/a/a/g;->aeL:I

    .line 885
    :goto_0
    invoke-virtual {p5}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-gez v0, :cond_3

    .line 887
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "x value cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 872
    :cond_0
    if-lt p3, p4, :cond_1

    .line 874
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "k2 must be smaller than k3"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 877
    :cond_1
    if-gtz p3, :cond_2

    .line 879
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "k2 must be larger than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 882
    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lorg/spongycastle/a/a/g;->aeL:I

    goto :goto_0

    .line 890
    :cond_3
    iput p1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    .line 891
    iput p2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    .line 892
    iput p3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    .line 893
    iput p4, p0, Lorg/spongycastle/a/a/g;->aeG:I

    .line 894
    return-void
.end method

.method private constructor <init>(IIIILorg/spongycastle/a/a/n;)V
    .locals 1

    .prologue
    .line 912
    invoke-direct {p0}, Lorg/spongycastle/a/a/f;-><init>()V

    .line 913
    add-int/lit8 v0, p1, 0x1f

    shr-int/lit8 v0, v0, 0x5

    iput v0, p0, Lorg/spongycastle/a/a/g;->aeN:I

    .line 914
    iput-object p5, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    .line 915
    iput p1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    .line 916
    iput p2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    .line 917
    iput p3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    .line 918
    iput p4, p0, Lorg/spongycastle/a/a/g;->aeG:I

    .line 920
    if-nez p3, :cond_0

    if-nez p4, :cond_0

    .line 922
    const/4 v0, 0x2

    iput v0, p0, Lorg/spongycastle/a/a/g;->aeL:I

    .line 929
    :goto_0
    return-void

    .line 926
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lorg/spongycastle/a/a/g;->aeL:I

    goto :goto_0
.end method

.method public static a(Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V
    .locals 2

    .prologue
    .line 961
    instance-of v0, p0, Lorg/spongycastle/a/a/g;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lorg/spongycastle/a/a/g;

    if-nez v0, :cond_1

    .line 963
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Field elements are not both instances of ECFieldElement.F2m"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 967
    :cond_1
    check-cast p0, Lorg/spongycastle/a/a/g;

    .line 968
    check-cast p1, Lorg/spongycastle/a/a/g;

    .line 970
    iget v0, p0, Lorg/spongycastle/a/a/g;->acJ:I

    iget v1, p1, Lorg/spongycastle/a/a/g;->acJ:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lorg/spongycastle/a/a/g;->aeE:I

    iget v1, p1, Lorg/spongycastle/a/a/g;->aeE:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lorg/spongycastle/a/a/g;->aeF:I

    iget v1, p1, Lorg/spongycastle/a/a/g;->aeF:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lorg/spongycastle/a/a/g;->aeG:I

    iget v1, p1, Lorg/spongycastle/a/a/g;->aeG:I

    if-eq v0, v1, :cond_3

    .line 973
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Field elements are not elements of the same field F2m"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 977
    :cond_3
    iget v0, p0, Lorg/spongycastle/a/a/g;->aeL:I

    iget v1, p1, Lorg/spongycastle/a/a/g;->aeL:I

    if-eq v0, v1, :cond_4

    .line 980
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "One of the field elements are not elements has incorrect representation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 984
    :cond_4
    return-void
.end method


# virtual methods
.method public c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
    .locals 6

    .prologue
    .line 991
    iget-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/n;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/spongycastle/a/a/n;

    .line 992
    check-cast p1, Lorg/spongycastle/a/a/g;

    .line 993
    iget-object v0, p1, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Lorg/spongycastle/a/a/n;->a(Lorg/spongycastle/a/a/n;I)V

    .line 994
    new-instance v0, Lorg/spongycastle/a/a/g;

    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    iget v4, p0, Lorg/spongycastle/a/a/g;->aeG:I

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/a/a/g;-><init>(IIIILorg/spongycastle/a/a/n;)V

    return-object v0
.end method

.method public d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 1000
    invoke-virtual {p0, p1}, Lorg/spongycastle/a/a/g;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
    .locals 6

    .prologue
    .line 1012
    check-cast p1, Lorg/spongycastle/a/a/g;

    .line 1013
    iget-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    iget-object v1, p1, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    iget v2, p0, Lorg/spongycastle/a/a/g;->acJ:I

    invoke-virtual {v0, v1, v2}, Lorg/spongycastle/a/a/n;->b(Lorg/spongycastle/a/a/n;I)Lorg/spongycastle/a/a/n;

    move-result-object v5

    .line 1014
    iget v0, p0, Lorg/spongycastle/a/a/g;->acJ:I

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeE:I

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    aput v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeG:I

    aput v3, v1, v2

    invoke-virtual {v5, v0, v1}, Lorg/spongycastle/a/a/n;->b(I[I)V

    .line 1015
    new-instance v0, Lorg/spongycastle/a/a/g;

    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    iget v4, p0, Lorg/spongycastle/a/a/g;->aeG:I

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/a/a/g;-><init>(IIIILorg/spongycastle/a/a/n;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1173
    if-ne p1, p0, :cond_1

    .line 1185
    :cond_0
    :goto_0
    return v0

    .line 1178
    :cond_1
    instance-of v2, p1, Lorg/spongycastle/a/a/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 1180
    goto :goto_0

    .line 1183
    :cond_2
    check-cast p1, Lorg/spongycastle/a/a/g;

    .line 1185
    iget v2, p0, Lorg/spongycastle/a/a/g;->acJ:I

    iget v3, p1, Lorg/spongycastle/a/a/g;->acJ:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    iget v3, p1, Lorg/spongycastle/a/a/g;->aeE:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeF:I

    iget v3, p1, Lorg/spongycastle/a/a/g;->aeF:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeG:I

    iget v3, p1, Lorg/spongycastle/a/a/g;->aeG:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeL:I

    iget v3, p1, Lorg/spongycastle/a/a/g;->aeL:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    iget-object v3, p1, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    invoke-virtual {v2, v3}, Lorg/spongycastle/a/a/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
    .locals 1

    .prologue
    .line 1021
    invoke-virtual {p1}, Lorg/spongycastle/a/a/f;->sr()Lorg/spongycastle/a/a/f;

    move-result-object v0

    .line 1022
    invoke-virtual {p0, v0}, Lorg/spongycastle/a/a/g;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method public getFieldSize()I
    .locals 1

    .prologue
    .line 943
    iget v0, p0, Lorg/spongycastle/a/a/g;->acJ:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1193
    iget-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/n;->hashCode()I

    move-result v0

    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    xor-int/2addr v0, v1

    iget v1, p0, Lorg/spongycastle/a/a/g;->aeE:I

    xor-int/2addr v0, v1

    iget v1, p0, Lorg/spongycastle/a/a/g;->aeF:I

    xor-int/2addr v0, v1

    iget v1, p0, Lorg/spongycastle/a/a/g;->aeG:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public sp()Lorg/spongycastle/a/a/f;
    .locals 0

    .prologue
    .line 1028
    return-object p0
.end method

.method public sq()Lorg/spongycastle/a/a/f;
    .locals 6

    .prologue
    .line 1033
    iget-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/n;->cH(I)Lorg/spongycastle/a/a/n;

    move-result-object v5

    .line 1034
    iget v0, p0, Lorg/spongycastle/a/a/g;->acJ:I

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeE:I

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    aput v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeG:I

    aput v3, v1, v2

    invoke-virtual {v5, v0, v1}, Lorg/spongycastle/a/a/n;->b(I[I)V

    .line 1035
    new-instance v0, Lorg/spongycastle/a/a/g;

    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    iget v4, p0, Lorg/spongycastle/a/a/g;->aeG:I

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/a/a/g;-><init>(IIIILorg/spongycastle/a/a/n;)V

    return-object v0
.end method

.method public sr()Lorg/spongycastle/a/a/f;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1046
    iget-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/n;

    .line 1049
    new-instance v2, Lorg/spongycastle/a/a/n;

    iget v1, p0, Lorg/spongycastle/a/a/g;->aeN:I

    invoke-direct {v2, v1}, Lorg/spongycastle/a/a/n;-><init>(I)V

    .line 1050
    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/n;->cG(I)V

    .line 1051
    invoke-virtual {v2, v4}, Lorg/spongycastle/a/a/n;->cG(I)V

    .line 1052
    iget v1, p0, Lorg/spongycastle/a/a/g;->aeE:I

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/n;->cG(I)V

    .line 1053
    iget v1, p0, Lorg/spongycastle/a/a/g;->aeL:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 1055
    iget v1, p0, Lorg/spongycastle/a/a/g;->aeF:I

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/n;->cG(I)V

    .line 1056
    iget v1, p0, Lorg/spongycastle/a/a/g;->aeG:I

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/n;->cG(I)V

    .line 1060
    :cond_0
    new-instance v1, Lorg/spongycastle/a/a/n;

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeN:I

    invoke-direct {v1, v3}, Lorg/spongycastle/a/a/n;-><init>(I)V

    .line 1061
    invoke-virtual {v1, v4}, Lorg/spongycastle/a/a/n;->cG(I)V

    .line 1062
    new-instance v5, Lorg/spongycastle/a/a/n;

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeN:I

    invoke-direct {v5, v3}, Lorg/spongycastle/a/a/n;-><init>(I)V

    move-object v7, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v7

    .line 1065
    :goto_0
    invoke-virtual {v2}, Lorg/spongycastle/a/a/n;->sB()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1070
    invoke-virtual {v2}, Lorg/spongycastle/a/a/n;->bitLength()I

    move-result v3

    invoke-virtual {v1}, Lorg/spongycastle/a/a/n;->bitLength()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1073
    if-gez v3, :cond_1

    .line 1083
    neg-int v3, v3

    move-object v7, v0

    move-object v0, v5

    move-object v5, v7

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    .line 1093
    :cond_1
    shr-int/lit8 v4, v3, 0x5

    .line 1095
    and-int/lit8 v3, v3, 0x1f

    .line 1096
    invoke-virtual {v1, v3}, Lorg/spongycastle/a/a/n;->cE(I)Lorg/spongycastle/a/a/n;

    move-result-object v6

    .line 1097
    invoke-virtual {v2, v6, v4}, Lorg/spongycastle/a/a/n;->a(Lorg/spongycastle/a/a/n;I)V

    .line 1101
    invoke-virtual {v5, v3}, Lorg/spongycastle/a/a/n;->cE(I)Lorg/spongycastle/a/a/n;

    move-result-object v3

    .line 1102
    invoke-virtual {v0, v3, v4}, Lorg/spongycastle/a/a/n;->a(Lorg/spongycastle/a/a/n;I)V

    goto :goto_0

    .line 1105
    :cond_2
    new-instance v0, Lorg/spongycastle/a/a/g;

    iget v1, p0, Lorg/spongycastle/a/a/g;->acJ:I

    iget v2, p0, Lorg/spongycastle/a/a/g;->aeE:I

    iget v3, p0, Lorg/spongycastle/a/a/g;->aeF:I

    iget v4, p0, Lorg/spongycastle/a/a/g;->aeG:I

    invoke-direct/range {v0 .. v5}, Lorg/spongycastle/a/a/g;-><init>(IIIILorg/spongycastle/a/a/n;)V

    return-object v0
.end method

.method public ss()Lorg/spongycastle/a/a/f;
    .locals 2

    .prologue
    .line 1111
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toBigInteger()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lorg/spongycastle/a/a/g;->aeM:Lorg/spongycastle/a/a/n;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/n;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
