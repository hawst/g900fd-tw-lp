.class public Lorg/spongycastle/a/a/k;
.super Lorg/spongycastle/a/a/j;
.source "ECPoint.java"


# direct methods
.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/spongycastle/a/a/k;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    .line 342
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V
    .locals 2

    .prologue
    .line 352
    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/a/a/j;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    .line 354
    if-eqz p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    .line 356
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Exactly one of the field elements is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_2
    if-eqz p2, :cond_3

    .line 362
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v1, p0, Lorg/spongycastle/a/a/k;->aeP:Lorg/spongycastle/a/a/f;

    invoke-static {v0, v1}, Lorg/spongycastle/a/a/g;->a(Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    .line 365
    if-eqz p1, :cond_3

    .line 367
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v1, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v1}, Lorg/spongycastle/a/a/c;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/spongycastle/a/a/g;->a(Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    .line 371
    :cond_3
    iput-boolean p4, p0, Lorg/spongycastle/a/a/k;->withCompression:Z

    .line 372
    return-void
.end method

.method private static a(Lorg/spongycastle/a/a/j;Lorg/spongycastle/a/a/j;)V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lorg/spongycastle/a/a/j;->UV:Lorg/spongycastle/a/a/c;

    iget-object v1, p1, Lorg/spongycastle/a/a/j;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only points on the same curve can be added or subtracted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 442
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;
    .locals 4

    .prologue
    .line 464
    .line 465
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 500
    :goto_0
    return-object v0

    .line 470
    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/a/a/k;->sv()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 472
    goto :goto_0

    .line 475
    :cond_1
    invoke-virtual {p1}, Lorg/spongycastle/a/a/k;->st()Lorg/spongycastle/a/a/f;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/g;

    .line 476
    invoke-virtual {p1}, Lorg/spongycastle/a/a/k;->su()Lorg/spongycastle/a/a/f;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/a/a/g;

    .line 479
    iget-object v2, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 481
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 484
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->sy()Lorg/spongycastle/a/a/j;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/k;

    goto :goto_0

    .line 488
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/k;

    goto :goto_0

    .line 491
    :cond_3
    iget-object v2, p0, Lorg/spongycastle/a/a/k;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v0}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->f(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/a/a/g;

    .line 494
    invoke-virtual {v1}, Lorg/spongycastle/a/a/g;->sq()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    iget-object v3, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v3}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/c;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/g;

    .line 497
    iget-object v2, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v0}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/g;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/a/a/k;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/a/a/g;

    .line 500
    new-instance p1, Lorg/spongycastle/a/a/k;

    iget-object v2, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    iget-boolean v3, p0, Lorg/spongycastle/a/a/k;->withCompression:Z

    invoke-direct {p1, v2, v0, v1, v3}, Lorg/spongycastle/a/a/k;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    move-object v0, p1

    goto/16 :goto_0
.end method

.method public b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 449
    invoke-static {p0, p1}, Lorg/spongycastle/a/a/k;->a(Lorg/spongycastle/a/a/j;Lorg/spongycastle/a/a/j;)V

    .line 450
    check-cast p1, Lorg/spongycastle/a/a/k;

    invoke-virtual {p0, p1}, Lorg/spongycastle/a/a/k;->a(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;

    move-result-object v0

    return-object v0
.end method

.method public b(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;
    .locals 1

    .prologue
    .line 523
    invoke-virtual {p1}, Lorg/spongycastle/a/a/k;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/a/a/k;->sx()Lorg/spongycastle/a/a/j;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/k;

    invoke-virtual {p0, v0}, Lorg/spongycastle/a/a/k;->a(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;

    move-result-object p0

    goto :goto_0
.end method

.method public c(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 508
    invoke-static {p0, p1}, Lorg/spongycastle/a/a/k;->a(Lorg/spongycastle/a/a/j;Lorg/spongycastle/a/a/j;)V

    .line 509
    check-cast p1, Lorg/spongycastle/a/a/k;

    invoke-virtual {p0, p1}, Lorg/spongycastle/a/a/k;->b(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;

    move-result-object v0

    return-object v0
.end method

.method public getEncoded()[B
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 379
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    new-array v0, v6, [B

    .line 422
    :goto_0
    return-object v0

    .line 384
    :cond_0
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r/k;->a(Lorg/spongycastle/a/a/f;)I

    move-result v1

    .line 385
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v0

    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->st()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lorg/spongycastle/asn1/r/k;->a(Ljava/math/BigInteger;I)[B

    move-result-object v2

    .line 388
    iget-boolean v0, p0, Lorg/spongycastle/a/a/k;->withCompression:Z

    if-eqz v0, :cond_2

    .line 391
    add-int/lit8 v0, v1, 0x1

    new-array v0, v0, [B

    .line 393
    const/4 v3, 0x2

    aput-byte v3, v0, v5

    .line 399
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->st()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    sget-object v4, Lorg/spongycastle/a/a/b;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 401
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->su()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->st()Lorg/spongycastle/a/a/f;

    move-result-object v4

    invoke-virtual {v4}, Lorg/spongycastle/a/a/f;->sr()Lorg/spongycastle/a/a/f;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/spongycastle/a/a/f;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 405
    const/4 v3, 0x3

    aput-byte v3, v0, v5

    .line 409
    :cond_1
    invoke-static {v2, v5, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 413
    :cond_2
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v0

    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->su()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lorg/spongycastle/asn1/r/k;->a(Ljava/math/BigInteger;I)[B

    move-result-object v3

    .line 415
    add-int v0, v1, v1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [B

    .line 417
    const/4 v4, 0x4

    aput-byte v4, v0, v5

    .line 418
    invoke-static {v2, v5, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 419
    add-int/lit8 v2, v1, 0x1

    invoke-static {v3, v5, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public sx()Lorg/spongycastle/a/a/j;
    .locals 5

    .prologue
    .line 567
    new-instance v0, Lorg/spongycastle/a/a/k;

    iget-object v1, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->st()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->su()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->st()Lorg/spongycastle/a/a/f;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v3

    iget-boolean v4, p0, Lorg/spongycastle/a/a/k;->withCompression:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/spongycastle/a/a/k;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    return-object v0
.end method

.method public sy()Lorg/spongycastle/a/a/j;
    .locals 5

    .prologue
    .line 537
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    :goto_0
    return-object p0

    .line 543
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-nez v0, :cond_1

    .line 547
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object p0

    goto :goto_0

    .line 550
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v1, p0, Lorg/spongycastle/a/a/k;->aeP:Lorg/spongycastle/a/a/f;

    iget-object v2, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->f(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/g;

    .line 553
    invoke-virtual {v0}, Lorg/spongycastle/a/a/g;->sq()Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/c;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    check-cast v1, Lorg/spongycastle/a/a/g;

    .line 557
    iget-object v2, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    sget-object v3, Lorg/spongycastle/a/a/b;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Lorg/spongycastle/a/a/c;->d(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    .line 558
    iget-object v3, p0, Lorg/spongycastle/a/a/k;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->sq()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/g;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/a/a/g;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/g;

    .line 562
    new-instance v2, Lorg/spongycastle/a/a/k;

    iget-object v3, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    iget-boolean v4, p0, Lorg/spongycastle/a/a/k;->withCompression:Z

    invoke-direct {v2, v3, v1, v0, v4}, Lorg/spongycastle/a/a/k;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    move-object p0, v2

    goto :goto_0
.end method

.method declared-synchronized sz()V
    .locals 1

    .prologue
    .line 575
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->aeQ:Lorg/spongycastle/a/a/i;

    if-nez v0, :cond_0

    .line 577
    iget-object v0, p0, Lorg/spongycastle/a/a/k;->UV:Lorg/spongycastle/a/a/c;

    check-cast v0, Lorg/spongycastle/a/a/d;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->si()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    new-instance v0, Lorg/spongycastle/a/a/t;

    invoke-direct {v0}, Lorg/spongycastle/a/a/t;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/a/a/k;->aeQ:Lorg/spongycastle/a/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 586
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 583
    :cond_1
    :try_start_1
    new-instance v0, Lorg/spongycastle/a/a/r;

    invoke-direct {v0}, Lorg/spongycastle/a/a/r;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/a/a/k;->aeQ:Lorg/spongycastle/a/a/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 575
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
