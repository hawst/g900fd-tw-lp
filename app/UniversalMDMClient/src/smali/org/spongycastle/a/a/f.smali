.class public abstract Lorg/spongycastle/a/a/f;
.super Ljava/lang/Object;
.source "ECFieldElement.java"

# interfaces
.implements Lorg/spongycastle/a/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 774
    return-void
.end method


# virtual methods
.method public abstract c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
.end method

.method public abstract d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
.end method

.method public abstract e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
.end method

.method public abstract f(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;
.end method

.method public abstract getFieldSize()I
.end method

.method public abstract sp()Lorg/spongycastle/a/a/f;
.end method

.method public abstract sq()Lorg/spongycastle/a/a/f;
.end method

.method public abstract sr()Lorg/spongycastle/a/a/f;
.end method

.method public abstract ss()Lorg/spongycastle/a/a/f;
.end method

.method public abstract toBigInteger()Ljava/math/BigInteger;
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
