.class Lorg/spongycastle/a/a/t;
.super Ljava/lang/Object;
.source "WTauNafMultiplier.java"

# interfaces
.implements Lorg/spongycastle/a/a/i;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lorg/spongycastle/a/a/k;Lorg/spongycastle/a/a/v;Lorg/spongycastle/a/a/o;BB)Lorg/spongycastle/a/a/k;
    .locals 6

    .prologue
    const/4 v2, 0x4

    .line 54
    if-nez p4, :cond_0

    .line 56
    sget-object v5, Lorg/spongycastle/a/a/q;->aeX:[Lorg/spongycastle/a/a/v;

    .line 64
    :goto_0
    invoke-static {p5, v2}, Lorg/spongycastle/a/a/q;->b(BI)Ljava/math/BigInteger;

    move-result-object v4

    .line 66
    const-wide/16 v0, 0x10

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v3

    move v0, p5

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lorg/spongycastle/a/a/q;->a(BLorg/spongycastle/a/a/v;BLjava/math/BigInteger;Ljava/math/BigInteger;[Lorg/spongycastle/a/a/v;)[B

    move-result-object v0

    .line 69
    invoke-static {p1, v0, p3}, Lorg/spongycastle/a/a/t;->a(Lorg/spongycastle/a/a/k;[BLorg/spongycastle/a/a/o;)Lorg/spongycastle/a/a/k;

    move-result-object v0

    return-object v0

    .line 61
    :cond_0
    sget-object v5, Lorg/spongycastle/a/a/q;->aeZ:[Lorg/spongycastle/a/a/v;

    goto :goto_0
.end method

.method private static a(Lorg/spongycastle/a/a/k;[BLorg/spongycastle/a/a/o;)Lorg/spongycastle/a/a/k;
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/d;

    .line 85
    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->byteValue()B

    move-result v0

    .line 88
    if-eqz p2, :cond_0

    instance-of v1, p2, Lorg/spongycastle/a/a/u;

    if-nez v1, :cond_2

    .line 90
    :cond_0
    invoke-static {p0, v0}, Lorg/spongycastle/a/a/q;->a(Lorg/spongycastle/a/a/k;B)[Lorg/spongycastle/a/a/k;

    move-result-object v0

    .line 91
    new-instance v1, Lorg/spongycastle/a/a/u;

    invoke-direct {v1, v0}, Lorg/spongycastle/a/a/u;-><init>([Lorg/spongycastle/a/a/k;)V

    invoke-virtual {p0, v1}, Lorg/spongycastle/a/a/k;->a(Lorg/spongycastle/a/a/o;)V

    move-object v1, v0

    .line 99
    :goto_0
    invoke-virtual {p0}, Lorg/spongycastle/a/a/k;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/k;

    .line 100
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_4

    .line 102
    invoke-static {v0}, Lorg/spongycastle/a/a/q;->c(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;

    move-result-object v0

    .line 103
    aget-byte v3, p1, v2

    if-eqz v3, :cond_1

    .line 105
    aget-byte v3, p1, v2

    if-lez v3, :cond_3

    .line 107
    aget-byte v3, p1, v2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Lorg/spongycastle/a/a/k;->a(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;

    move-result-object v0

    .line 100
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 95
    :cond_2
    check-cast p2, Lorg/spongycastle/a/a/u;

    invoke-virtual {p2}, Lorg/spongycastle/a/a/u;->sK()[Lorg/spongycastle/a/a/k;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 112
    :cond_3
    aget-byte v3, p1, v2

    neg-int v3, v3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Lorg/spongycastle/a/a/k;->b(Lorg/spongycastle/a/a/k;)Lorg/spongycastle/a/a/k;

    move-result-object v0

    goto :goto_2

    .line 117
    :cond_4
    return-object v0
.end method


# virtual methods
.method public a(Lorg/spongycastle/a/a/j;Ljava/math/BigInteger;Lorg/spongycastle/a/a/o;)Lorg/spongycastle/a/a/j;
    .locals 11

    .prologue
    .line 21
    instance-of v0, p1, Lorg/spongycastle/a/a/k;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only ECPoint.F2m can be used in WTauNafMultiplier"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v6, p1

    .line 27
    check-cast v6, Lorg/spongycastle/a/a/k;

    .line 29
    invoke-virtual {v6}, Lorg/spongycastle/a/a/k;->pO()Lorg/spongycastle/a/a/c;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/a/a/d;

    .line 30
    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->getM()I

    move-result v1

    .line 31
    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->sg()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->byteValue()B

    move-result v2

    .line 32
    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->sj()B

    move-result v4

    .line 33
    invoke-virtual {v0}, Lorg/spongycastle/a/a/d;->sk()[Ljava/math/BigInteger;

    move-result-object v3

    .line 35
    const/16 v5, 0xa

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lorg/spongycastle/a/a/q;->a(Ljava/math/BigInteger;IB[Ljava/math/BigInteger;BB)Lorg/spongycastle/a/a/v;

    move-result-object v7

    move-object v5, p0

    move-object v8, p3

    move v9, v2

    move v10, v4

    .line 37
    invoke-direct/range {v5 .. v10}, Lorg/spongycastle/a/a/t;->a(Lorg/spongycastle/a/a/k;Lorg/spongycastle/a/a/v;Lorg/spongycastle/a/a/o;BB)Lorg/spongycastle/a/a/k;

    move-result-object v0

    return-object v0
.end method
