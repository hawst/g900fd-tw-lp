.class public Lorg/spongycastle/a/a/l;
.super Lorg/spongycastle/a/a/j;
.source "ECPoint.java"


# direct methods
.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/spongycastle/a/a/l;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    .line 171
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/a/a/j;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    .line 185
    if-eqz p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    .line 187
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Exactly one of the field elements is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_2
    iput-boolean p4, p0, Lorg/spongycastle/a/a/l;->withCompression:Z

    .line 191
    return-void
.end method


# virtual methods
.method public b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;
    .locals 3

    .prologue
    .line 243
    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    :goto_0
    return-object p1

    .line 248
    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/a/a/j;->sv()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p0

    .line 250
    goto :goto_0

    .line 254
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v1, p1, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    iget-object v1, p1, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 259
    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->sy()Lorg/spongycastle/a/a/j;

    move-result-object p1

    goto :goto_0

    .line 263
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object p1

    goto :goto_0

    .line 266
    :cond_3
    iget-object v0, p1, Lorg/spongycastle/a/a/j;->aeP:Lorg/spongycastle/a/a/f;

    iget-object v1, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    iget-object v1, p1, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/f;->f(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->sq()Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p1, Lorg/spongycastle/a/a/j;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    .line 269
    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/f;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    .line 271
    new-instance p1, Lorg/spongycastle/a/a/l;

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    invoke-direct {p1, v2, v1, v0}, Lorg/spongycastle/a/a/l;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;)V

    goto :goto_0
.end method

.method public c(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p1}, Lorg/spongycastle/a/a/j;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/a/a/j;->sx()Lorg/spongycastle/a/a/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/spongycastle/a/a/l;->b(Lorg/spongycastle/a/a/j;)Lorg/spongycastle/a/a/j;

    move-result-object p0

    goto :goto_0
.end method

.method public getEncoded()[B
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 198
    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    new-array v0, v5, [B

    .line 236
    :goto_0
    return-object v0

    .line 203
    :cond_0
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/r/k;->a(Lorg/spongycastle/a/a/f;)I

    move-result v1

    .line 205
    iget-boolean v0, p0, Lorg/spongycastle/a/a/l;->withCompression:Z

    if-eqz v0, :cond_2

    .line 209
    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->su()Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    const/4 v0, 0x3

    .line 218
    :goto_1
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v2

    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->st()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lorg/spongycastle/asn1/r/k;->a(Ljava/math/BigInteger;I)[B

    move-result-object v2

    .line 219
    array-length v1, v2

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [B

    .line 221
    aput-byte v0, v1, v4

    .line 222
    array-length v0, v2

    invoke-static {v2, v4, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 224
    goto :goto_0

    .line 215
    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    .line 228
    :cond_2
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v0

    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->st()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lorg/spongycastle/asn1/r/k;->a(Ljava/math/BigInteger;I)[B

    move-result-object v2

    .line 229
    invoke-static {}, Lorg/spongycastle/a/a/j;->sA()Lorg/spongycastle/asn1/r/k;

    move-result-object v0

    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->su()Lorg/spongycastle/a/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Lorg/spongycastle/asn1/r/k;->a(Ljava/math/BigInteger;I)[B

    move-result-object v1

    .line 230
    array-length v0, v2

    array-length v3, v1

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [B

    .line 232
    const/4 v3, 0x4

    aput-byte v3, v0, v4

    .line 233
    array-length v3, v2

    invoke-static {v2, v4, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    array-length v3, v1

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public sx()Lorg/spongycastle/a/a/j;
    .locals 5

    .prologue
    .line 314
    new-instance v0, Lorg/spongycastle/a/a/l;

    iget-object v1, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    iget-object v3, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v3}, Lorg/spongycastle/a/a/f;->sp()Lorg/spongycastle/a/a/f;

    move-result-object v3

    iget-boolean v4, p0, Lorg/spongycastle/a/a/l;->withCompression:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/spongycastle/a/a/l;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    return-object v0
.end method

.method public sy()Lorg/spongycastle/a/a/j;
    .locals 5

    .prologue
    .line 277
    invoke-virtual {p0}, Lorg/spongycastle/a/a/l;->sv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    :goto_0
    return-object p0

    .line 283
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/f;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-nez v0, :cond_1

    .line 287
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    invoke-virtual {v0}, Lorg/spongycastle/a/a/c;->sf()Lorg/spongycastle/a/a/j;

    move-result-object p0

    goto :goto_0

    .line 290
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/c;->d(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    const-wide/16 v2, 0x3

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/c;->d(Ljava/math/BigInteger;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    .line 292
    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2}, Lorg/spongycastle/a/a/f;->sq()Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/spongycastle/a/a/f;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    iget-object v2, v2, Lorg/spongycastle/a/a/c;->aeC:Lorg/spongycastle/a/a/f;

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->c(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v2, v0}, Lorg/spongycastle/a/a/f;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/a/a/f;->f(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    .line 294
    invoke-virtual {v1}, Lorg/spongycastle/a/a/f;->sq()Lorg/spongycastle/a/a/f;

    move-result-object v2

    iget-object v3, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v3, v0}, Lorg/spongycastle/a/a/f;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v2

    .line 295
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->aeO:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v2}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/a/a/f;->e(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/a/a/l;->aeP:Lorg/spongycastle/a/a/f;

    invoke-virtual {v0, v1}, Lorg/spongycastle/a/a/f;->d(Lorg/spongycastle/a/a/f;)Lorg/spongycastle/a/a/f;

    move-result-object v1

    .line 297
    new-instance v0, Lorg/spongycastle/a/a/l;

    iget-object v3, p0, Lorg/spongycastle/a/a/l;->UV:Lorg/spongycastle/a/a/c;

    iget-boolean v4, p0, Lorg/spongycastle/a/a/l;->withCompression:Z

    invoke-direct {v0, v3, v2, v1, v4}, Lorg/spongycastle/a/a/l;-><init>(Lorg/spongycastle/a/a/c;Lorg/spongycastle/a/a/f;Lorg/spongycastle/a/a/f;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method declared-synchronized sz()V
    .locals 1

    .prologue
    .line 322
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/a/a/l;->aeQ:Lorg/spongycastle/a/a/i;

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Lorg/spongycastle/a/a/r;

    invoke-direct {v0}, Lorg/spongycastle/a/a/r;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/a/a/l;->aeQ:Lorg/spongycastle/a/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    :cond_0
    monitor-exit p0

    return-void

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
