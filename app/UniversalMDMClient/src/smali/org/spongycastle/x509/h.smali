.class public interface abstract Lorg/spongycastle/x509/h;
.super Ljava/lang/Object;
.source "X509AttributeCertificate.java"

# interfaces
.implements Ljava/security/cert/X509Extension;


# virtual methods
.method public abstract checkValidity(Ljava/util/Date;)V
.end method

.method public abstract dh(Ljava/lang/String;)[Lorg/spongycastle/x509/f;
.end method

.method public abstract getEncoded()[B
.end method

.method public abstract getNotAfter()Ljava/util/Date;
.end method

.method public abstract getSerialNumber()Ljava/math/BigInteger;
.end method

.method public abstract ti()Lorg/spongycastle/x509/a;
.end method

.method public abstract tl()Lorg/spongycastle/x509/b;
.end method
