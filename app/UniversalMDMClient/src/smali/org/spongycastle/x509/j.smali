.class public Lorg/spongycastle/x509/j;
.super Ljava/lang/Object;
.source "X509CertPairStoreSelector.java"

# interfaces
.implements Lorg/spongycastle/util/e;


# instance fields
.field private agc:Lorg/spongycastle/x509/k;

.field private agd:Lorg/spongycastle/x509/k;

.field private age:Lorg/spongycastle/x509/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/x509/k;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lorg/spongycastle/x509/j;->agc:Lorg/spongycastle/x509/k;

    .line 55
    return-void
.end method

.method public b(Lorg/spongycastle/x509/k;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lorg/spongycastle/x509/j;->agd:Lorg/spongycastle/x509/k;

    .line 64
    return-void
.end method

.method public bp(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 105
    :try_start_0
    instance-of v1, p1, Lorg/spongycastle/x509/l;

    if-nez v1, :cond_0

    move v1, v2

    .line 132
    :goto_0
    return v1

    .line 109
    :cond_0
    move-object v0, p1

    check-cast v0, Lorg/spongycastle/x509/l;

    move-object v1, v0

    .line 111
    iget-object v3, p0, Lorg/spongycastle/x509/j;->agc:Lorg/spongycastle/x509/k;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/spongycastle/x509/j;->agc:Lorg/spongycastle/x509/k;

    invoke-virtual {v1}, Lorg/spongycastle/x509/l;->tr()Ljava/security/cert/X509Certificate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/spongycastle/x509/k;->bp(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    move v1, v2

    .line 114
    goto :goto_0

    .line 117
    :cond_1
    iget-object v3, p0, Lorg/spongycastle/x509/j;->agd:Lorg/spongycastle/x509/k;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/spongycastle/x509/j;->agd:Lorg/spongycastle/x509/k;

    invoke-virtual {v1}, Lorg/spongycastle/x509/l;->ts()Ljava/security/cert/X509Certificate;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/spongycastle/x509/k;->bp(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 120
    goto :goto_0

    .line 123
    :cond_2
    iget-object v1, p0, Lorg/spongycastle/x509/j;->age:Lorg/spongycastle/x509/l;

    if-eqz v1, :cond_3

    .line 125
    iget-object v1, p0, Lorg/spongycastle/x509/j;->age:Lorg/spongycastle/x509/l;

    invoke-virtual {v1, p1}, Lorg/spongycastle/x509/l;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 128
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 130
    :catch_0
    move-exception v1

    move v1, v2

    .line 132
    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 74
    new-instance v1, Lorg/spongycastle/x509/j;

    invoke-direct {v1}, Lorg/spongycastle/x509/j;-><init>()V

    .line 76
    iget-object v0, p0, Lorg/spongycastle/x509/j;->age:Lorg/spongycastle/x509/l;

    iput-object v0, v1, Lorg/spongycastle/x509/j;->age:Lorg/spongycastle/x509/l;

    .line 78
    iget-object v0, p0, Lorg/spongycastle/x509/j;->agc:Lorg/spongycastle/x509/k;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lorg/spongycastle/x509/j;->agc:Lorg/spongycastle/x509/k;

    invoke-virtual {v0}, Lorg/spongycastle/x509/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/x509/k;

    invoke-virtual {v1, v0}, Lorg/spongycastle/x509/j;->a(Lorg/spongycastle/x509/k;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/x509/j;->agd:Lorg/spongycastle/x509/k;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lorg/spongycastle/x509/j;->agd:Lorg/spongycastle/x509/k;

    invoke-virtual {v0}, Lorg/spongycastle/x509/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/x509/k;

    invoke-virtual {v1, v0}, Lorg/spongycastle/x509/j;->b(Lorg/spongycastle/x509/k;)V

    .line 90
    :cond_1
    return-object v1
.end method

.method public tp()Lorg/spongycastle/x509/l;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lorg/spongycastle/x509/j;->age:Lorg/spongycastle/x509/l;

    return-object v0
.end method

.method public tq()Lorg/spongycastle/x509/k;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/spongycastle/x509/j;->agc:Lorg/spongycastle/x509/k;

    return-object v0
.end method
