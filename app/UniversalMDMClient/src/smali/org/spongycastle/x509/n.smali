.class public Lorg/spongycastle/x509/n;
.super Ljava/lang/Object;
.source "X509Store.java"

# interfaces
.implements Lorg/spongycastle/util/f;


# instance fields
.field private agi:Ljava/security/Provider;

.field private agj:Lorg/spongycastle/x509/p;


# direct methods
.method private constructor <init>(Ljava/security/Provider;Lorg/spongycastle/x509/p;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lorg/spongycastle/x509/n;->agi:Ljava/security/Provider;

    .line 67
    iput-object p2, p0, Lorg/spongycastle/x509/n;->agj:Lorg/spongycastle/x509/p;

    .line 68
    return-void
.end method

.method public static a(Ljava/lang/String;Lorg/spongycastle/x509/o;Ljava/lang/String;)Lorg/spongycastle/x509/n;
    .locals 1

    .prologue
    .line 32
    invoke-static {p2}, Lorg/spongycastle/x509/r;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lorg/spongycastle/x509/n;->a(Ljava/lang/String;Lorg/spongycastle/x509/o;Ljava/security/Provider;)Lorg/spongycastle/x509/n;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lorg/spongycastle/x509/o;Ljava/security/Provider;)Lorg/spongycastle/x509/n;
    .locals 2

    .prologue
    .line 40
    :try_start_0
    const-string v0, "X509Store"

    invoke-static {v0, p0, p2}, Lorg/spongycastle/x509/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Lorg/spongycastle/x509/s;

    move-result-object v0

    .line 42
    invoke-static {v0, p1}, Lorg/spongycastle/x509/n;->a(Lorg/spongycastle/x509/s;Lorg/spongycastle/x509/o;)Lorg/spongycastle/x509/n;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 44
    :catch_0
    move-exception v0

    .line 46
    new-instance v1, Lorg/spongycastle/x509/NoSuchStoreException;

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/x509/NoSuchStoreException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(Lorg/spongycastle/x509/s;Lorg/spongycastle/x509/o;)Lorg/spongycastle/x509/n;
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lorg/spongycastle/x509/s;->tt()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/x509/p;

    .line 54
    invoke-virtual {v0, p1}, Lorg/spongycastle/x509/p;->engineInit(Lorg/spongycastle/x509/o;)V

    .line 56
    new-instance v1, Lorg/spongycastle/x509/n;

    invoke-virtual {p0}, Lorg/spongycastle/x509/s;->getProvider()Ljava/security/Provider;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/x509/n;-><init>(Ljava/security/Provider;Lorg/spongycastle/x509/p;)V

    return-object v1
.end method


# virtual methods
.method public a(Lorg/spongycastle/util/e;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/spongycastle/x509/n;->agj:Lorg/spongycastle/x509/p;

    invoke-virtual {v0, p1}, Lorg/spongycastle/x509/p;->engineGetMatches(Lorg/spongycastle/util/e;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
