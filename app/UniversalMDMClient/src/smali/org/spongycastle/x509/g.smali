.class public Lorg/spongycastle/x509/g;
.super Ljava/lang/Object;
.source "X509AttributeCertStoreSelector.java"

# interfaces
.implements Lorg/spongycastle/util/e;


# instance fields
.field private afP:Lorg/spongycastle/x509/a;

.field private afQ:Lorg/spongycastle/x509/b;

.field private afR:Ljava/math/BigInteger;

.field private afS:Ljava/util/Date;

.field private afT:Lorg/spongycastle/x509/h;

.field private afU:Ljava/util/Collection;

.field private afV:Ljava/util/Collection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/x509/g;->afU:Ljava/util/Collection;

    .line 50
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/x509/g;->afV:Ljava/util/Collection;

    .line 55
    return-void
.end method


# virtual methods
.method public bp(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 66
    instance-of v0, p1, Lorg/spongycastle/x509/h;

    if-nez v0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return v1

    .line 71
    :cond_1
    check-cast p1, Lorg/spongycastle/x509/h;

    .line 73
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afT:Lorg/spongycastle/x509/h;

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afT:Lorg/spongycastle/x509/h;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afR:Ljava/math/BigInteger;

    if-eqz v0, :cond_3

    .line 82
    invoke-interface {p1}, Lorg/spongycastle/x509/h;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/x509/g;->afR:Ljava/math/BigInteger;

    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    :cond_3
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afP:Lorg/spongycastle/x509/a;

    if-eqz v0, :cond_4

    .line 89
    invoke-interface {p1}, Lorg/spongycastle/x509/h;->ti()Lorg/spongycastle/x509/a;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/x509/g;->afP:Lorg/spongycastle/x509/a;

    invoke-virtual {v0, v2}, Lorg/spongycastle/x509/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    :cond_4
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afQ:Lorg/spongycastle/x509/b;

    if-eqz v0, :cond_5

    .line 96
    invoke-interface {p1}, Lorg/spongycastle/x509/h;->tl()Lorg/spongycastle/x509/b;

    move-result-object v0

    iget-object v2, p0, Lorg/spongycastle/x509/g;->afQ:Lorg/spongycastle/x509/b;

    invoke-virtual {v0, v2}, Lorg/spongycastle/x509/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    :cond_5
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afS:Ljava/util/Date;

    if-eqz v0, :cond_6

    .line 106
    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afS:Ljava/util/Date;

    invoke-interface {p1, v0}, Lorg/spongycastle/x509/h;->checkValidity(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_0 .. :try_end_0} :catch_2

    .line 117
    :cond_6
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afU:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lorg/spongycastle/x509/g;->afV:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 120
    :cond_7
    sget-object v0, Lorg/spongycastle/asn1/q/av;->TR:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/spongycastle/x509/h;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v0

    .line 122
    if-eqz v0, :cond_f

    .line 127
    :try_start_1
    new-instance v2, Lorg/spongycastle/asn1/h;

    invoke-static {v0}, Lorg/spongycastle/asn1/bd;->j([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/bd;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bd;->getOctets()[B

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/h;-><init>([B)V

    invoke-virtual {v2}, Lorg/spongycastle/asn1/h;->mF()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q/ap;->bd(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/ap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lorg/spongycastle/asn1/q/ap;->pE()[Lorg/spongycastle/asn1/q/aq;

    move-result-object v5

    .line 142
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afU:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    move v2, v1

    .line 146
    :goto_1
    array-length v3, v5

    if-ge v0, v3, :cond_a

    .line 148
    aget-object v3, v5, v0

    .line 149
    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/aq;->pF()[Lorg/spongycastle/asn1/q/ao;

    move-result-object v6

    move v3, v1

    .line 150
    :goto_2
    array-length v7, v6

    if-ge v3, v7, :cond_8

    .line 152
    iget-object v7, p0, Lorg/spongycastle/x509/g;->afU:Ljava/util/Collection;

    aget-object v8, v6, v3

    invoke-virtual {v8}, Lorg/spongycastle/asn1/q/ao;->pD()Lorg/spongycastle/asn1/q/u;

    move-result-object v8

    invoke-static {v8}, Lorg/spongycastle/asn1/q/u;->aO(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/u;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    move v2, v4

    .line 146
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 160
    :cond_a
    if-eqz v2, :cond_0

    .line 165
    :cond_b
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afV:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    move v2, v1

    .line 169
    :goto_3
    array-length v3, v5

    if-ge v0, v3, :cond_e

    .line 171
    aget-object v3, v5, v0

    .line 172
    invoke-virtual {v3}, Lorg/spongycastle/asn1/q/aq;->pF()[Lorg/spongycastle/asn1/q/ao;

    move-result-object v6

    move v3, v1

    .line 173
    :goto_4
    array-length v7, v6

    if-ge v3, v7, :cond_c

    .line 175
    iget-object v7, p0, Lorg/spongycastle/x509/g;->afV:Ljava/util/Collection;

    aget-object v8, v6, v3

    invoke-virtual {v8}, Lorg/spongycastle/asn1/q/ao;->pC()Lorg/spongycastle/asn1/q/u;

    move-result-object v8

    invoke-static {v8}, Lorg/spongycastle/asn1/q/u;->aO(Ljava/lang/Object;)Lorg/spongycastle/asn1/q/u;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    move v2, v4

    .line 169
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 173
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 183
    :cond_e
    if-eqz v2, :cond_0

    :cond_f
    move v1, v4

    .line 190
    goto/16 :goto_0

    .line 137
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 133
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 112
    :catch_2
    move-exception v0

    goto/16 :goto_0

    .line 108
    :catch_3
    move-exception v0

    goto/16 :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 200
    new-instance v0, Lorg/spongycastle/x509/g;

    invoke-direct {v0}, Lorg/spongycastle/x509/g;-><init>()V

    .line 201
    iget-object v1, p0, Lorg/spongycastle/x509/g;->afT:Lorg/spongycastle/x509/h;

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afT:Lorg/spongycastle/x509/h;

    .line 202
    invoke-virtual {p0}, Lorg/spongycastle/x509/g;->th()Ljava/util/Date;

    move-result-object v1

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afS:Ljava/util/Date;

    .line 203
    iget-object v1, p0, Lorg/spongycastle/x509/g;->afP:Lorg/spongycastle/x509/a;

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afP:Lorg/spongycastle/x509/a;

    .line 204
    iget-object v1, p0, Lorg/spongycastle/x509/g;->afQ:Lorg/spongycastle/x509/b;

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afQ:Lorg/spongycastle/x509/b;

    .line 205
    iget-object v1, p0, Lorg/spongycastle/x509/g;->afR:Ljava/math/BigInteger;

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afR:Ljava/math/BigInteger;

    .line 206
    invoke-virtual {p0}, Lorg/spongycastle/x509/g;->tk()Ljava/util/Collection;

    move-result-object v1

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afV:Ljava/util/Collection;

    .line 207
    invoke-virtual {p0}, Lorg/spongycastle/x509/g;->tj()Ljava/util/Collection;

    move-result-object v1

    iput-object v1, v0, Lorg/spongycastle/x509/g;->afU:Ljava/util/Collection;

    .line 208
    return-object v0
.end method

.method public getSerialNumber()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afR:Ljava/math/BigInteger;

    return-object v0
.end method

.method public tg()Lorg/spongycastle/x509/h;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afT:Lorg/spongycastle/x509/h;

    return-object v0
.end method

.method public th()Ljava/util/Date;
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afS:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 241
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lorg/spongycastle/x509/g;->afS:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 244
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ti()Lorg/spongycastle/x509/a;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afP:Lorg/spongycastle/x509/a;

    return-object v0
.end method

.method public tj()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afU:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public tk()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lorg/spongycastle/x509/g;->afV:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
