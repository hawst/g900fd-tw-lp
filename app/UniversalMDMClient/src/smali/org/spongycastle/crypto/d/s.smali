.class public Lorg/spongycastle/crypto/d/s;
.super Ljava/lang/Object;
.source "IESEngine.java"


# instance fields
.field Wt:Z

.field ZZ:Lorg/spongycastle/crypto/c;

.field aaa:Lorg/spongycastle/crypto/o;

.field aab:[B

.field aac:Lorg/spongycastle/crypto/h;

.field aad:Lorg/spongycastle/crypto/h;

.field aae:Lorg/spongycastle/crypto/j/ag;

.field cipher:Lorg/spongycastle/crypto/f;

.field kdf:Lorg/spongycastle/crypto/j;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/c;Lorg/spongycastle/crypto/j;Lorg/spongycastle/crypto/o;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/spongycastle/crypto/d/s;->ZZ:Lorg/spongycastle/crypto/c;

    .line 47
    iput-object p2, p0, Lorg/spongycastle/crypto/d/s;->kdf:Lorg/spongycastle/crypto/j;

    .line 48
    iput-object p3, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    .line 49
    invoke-interface {p3}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/d/s;->aab:[B

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    .line 51
    return-void
.end method

.method private a(Lorg/spongycastle/crypto/j/aj;I)[B
    .locals 4

    .prologue
    .line 230
    new-array v0, p2, [B

    .line 232
    iget-object v1, p0, Lorg/spongycastle/crypto/d/s;->kdf:Lorg/spongycastle/crypto/j;

    invoke-interface {v1, p1}, Lorg/spongycastle/crypto/j;->init(Lorg/spongycastle/crypto/k;)V

    .line 234
    iget-object v1, p0, Lorg/spongycastle/crypto/d/s;->kdf:Lorg/spongycastle/crypto/j;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-interface {v1, v0, v2, v3}, Lorg/spongycastle/crypto/j;->generateBytes([BII)I

    .line 236
    return-object v0
.end method

.method private a([BII[B)[B
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 102
    .line 104
    new-instance v1, Lorg/spongycastle/crypto/j/aj;

    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ag;->qP()[B

    move-result-object v0

    invoke-direct {v1, p4, v0}, Lorg/spongycastle/crypto/j/aj;-><init>([B[B)V

    .line 105
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ag;->qR()I

    move-result v6

    .line 107
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->kdf:Lorg/spongycastle/crypto/j;

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/j;->init(Lorg/spongycastle/crypto/k;)V

    .line 109
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    sub-int v3, p3, v0

    .line 111
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    if-nez v0, :cond_1

    .line 113
    div-int/lit8 v0, v6, 0x8

    add-int/2addr v0, v3

    invoke-direct {p0, v1, v0}, Lorg/spongycastle/crypto/d/s;->a(Lorg/spongycastle/crypto/j/aj;I)[B

    move-result-object v2

    .line 115
    new-array v1, v3, [B

    move v0, v5

    .line 117
    :goto_0
    if-eq v0, v3, :cond_0

    .line 119
    add-int v4, p2, v0

    aget-byte v4, p1, v4

    aget-byte v7, v2, v0

    xor-int/2addr v4, v7

    int-to-byte v4, v4

    aput-byte v4, v1, v0

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    new-instance v0, Lorg/spongycastle/crypto/j/ak;

    div-int/lit8 v4, v6, 0x8

    invoke-direct {v0, v2, v3, v4}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    .line 144
    :goto_1
    iget-object v2, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    invoke-virtual {v2}, Lorg/spongycastle/crypto/j/ag;->qQ()[B

    move-result-object v2

    .line 146
    iget-object v4, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v4, v0}, Lorg/spongycastle/crypto/o;->a(Lorg/spongycastle/crypto/h;)V

    .line 147
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0, p1, p2, v3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 148
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    array-length v4, v2

    invoke-interface {v0, v2, v5, v4}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 149
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    iget-object v2, p0, Lorg/spongycastle/crypto/d/s;->aab:[B

    invoke-interface {v0, v2, v5}, Lorg/spongycastle/crypto/o;->doFinal([BI)I

    .line 151
    add-int v0, p2, v3

    .line 153
    :goto_2
    iget-object v2, p0, Lorg/spongycastle/crypto/d/s;->aab:[B

    array-length v2, v2

    if-ge v5, v2, :cond_3

    .line 155
    iget-object v2, p0, Lorg/spongycastle/crypto/d/s;->aab:[B

    aget-byte v2, v2, v5

    add-int v3, v0, v5

    aget-byte v3, p1, v3

    if-eq v2, v3, :cond_2

    .line 157
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "Mac codes failed to equal."

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    check-cast v0, Lorg/spongycastle/crypto/j/ah;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ah;->qS()I

    move-result v7

    .line 127
    div-int/lit8 v0, v7, 0x8

    div-int/lit8 v2, v6, 0x8

    add-int/2addr v0, v2

    invoke-direct {p0, v1, v0}, Lorg/spongycastle/crypto/d/s;->a(Lorg/spongycastle/crypto/j/aj;I)[B

    move-result-object v8

    .line 129
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    new-instance v1, Lorg/spongycastle/crypto/j/ak;

    div-int/lit8 v2, v7, 0x8

    invoke-direct {v1, v8, v5, v2}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    invoke-virtual {v0, v5, v1}, Lorg/spongycastle/crypto/f;->init(ZLorg/spongycastle/crypto/h;)V

    .line 131
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, v3}, Lorg/spongycastle/crypto/f;->getOutputSize(I)I

    move-result v0

    new-array v4, v0, [B

    .line 133
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/crypto/f;->processBytes([BII[BI)I

    move-result v0

    .line 135
    iget-object v1, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v1, v4, v0}, Lorg/spongycastle/crypto/f;->doFinal([BI)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    new-array v1, v0, [B

    .line 139
    invoke-static {v4, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 141
    new-instance v0, Lorg/spongycastle/crypto/j/ak;

    div-int/lit8 v2, v7, 0x8

    div-int/lit8 v4, v6, 0x8

    invoke-direct {v0, v8, v2, v4}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    goto :goto_1

    .line 153
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 161
    :cond_3
    return-object v1
.end method

.method private b([BII[B)[B
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 171
    .line 173
    new-instance v1, Lorg/spongycastle/crypto/j/aj;

    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ag;->qP()[B

    move-result-object v0

    invoke-direct {v1, p4, v0}, Lorg/spongycastle/crypto/j/aj;-><init>([B[B)V

    .line 175
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ag;->qR()I

    move-result v6

    .line 177
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    if-nez v0, :cond_1

    .line 179
    div-int/lit8 v0, v6, 0x8

    add-int/2addr v0, p3

    invoke-direct {p0, v1, v0}, Lorg/spongycastle/crypto/d/s;->a(Lorg/spongycastle/crypto/j/aj;I)[B

    move-result-object v2

    .line 181
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    add-int/2addr v0, p3

    new-array v1, v0, [B

    move v0, v5

    .line 184
    :goto_0
    if-eq v0, p3, :cond_0

    .line 186
    add-int v3, p2, v0

    aget-byte v3, p1, v3

    aget-byte v4, v2, v0

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_0
    new-instance v0, Lorg/spongycastle/crypto/j/ak;

    div-int/lit8 v3, v6, 0x8

    invoke-direct {v0, v2, p3, v3}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    .line 214
    :goto_1
    iget-object v2, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    invoke-virtual {v2}, Lorg/spongycastle/crypto/j/ag;->qQ()[B

    move-result-object v2

    .line 216
    iget-object v3, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->a(Lorg/spongycastle/crypto/h;)V

    .line 217
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0, v1, v5, p3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 218
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    array-length v3, v2

    invoke-interface {v0, v2, v5, v3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 222
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0, v1, p3}, Lorg/spongycastle/crypto/o;->doFinal([BI)I

    .line 223
    return-object v1

    .line 193
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    check-cast v0, Lorg/spongycastle/crypto/j/ah;

    invoke-virtual {v0}, Lorg/spongycastle/crypto/j/ah;->qS()I

    move-result v7

    .line 194
    div-int/lit8 v0, v7, 0x8

    div-int/lit8 v2, v6, 0x8

    add-int/2addr v0, v2

    invoke-direct {p0, v1, v0}, Lorg/spongycastle/crypto/d/s;->a(Lorg/spongycastle/crypto/j/aj;I)[B

    move-result-object v8

    .line 196
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    const/4 v1, 0x1

    new-instance v2, Lorg/spongycastle/crypto/j/ak;

    div-int/lit8 v3, v7, 0x8

    invoke-direct {v2, v8, v5, v3}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    invoke-virtual {v0, v1, v2}, Lorg/spongycastle/crypto/f;->init(ZLorg/spongycastle/crypto/h;)V

    .line 198
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v0, p3}, Lorg/spongycastle/crypto/f;->getOutputSize(I)I

    move-result v0

    .line 200
    new-array v4, v0, [B

    .line 202
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/spongycastle/crypto/f;->processBytes([BII[BI)I

    move-result v0

    .line 204
    iget-object v1, p0, Lorg/spongycastle/crypto/d/s;->cipher:Lorg/spongycastle/crypto/f;

    invoke-virtual {v1, v4, v0}, Lorg/spongycastle/crypto/f;->doFinal([BI)I

    move-result v1

    add-int p3, v0, v1

    .line 206
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->aaa:Lorg/spongycastle/crypto/o;

    invoke-interface {v0}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v0

    add-int/2addr v0, p3

    new-array v1, v0, [B

    .line 209
    invoke-static {v4, v5, v1, v5, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 211
    new-instance v0, Lorg/spongycastle/crypto/j/ak;

    div-int/lit8 v2, v7, 0x8

    div-int/lit8 v3, v6, 0x8

    invoke-direct {v0, v8, v2, v3}, Lorg/spongycastle/crypto/j/ak;-><init>([BII)V

    goto :goto_1
.end method


# virtual methods
.method public a(ZLorg/spongycastle/crypto/h;Lorg/spongycastle/crypto/h;Lorg/spongycastle/crypto/h;)V
    .locals 0

    .prologue
    .line 89
    iput-boolean p1, p0, Lorg/spongycastle/crypto/d/s;->Wt:Z

    .line 90
    iput-object p2, p0, Lorg/spongycastle/crypto/d/s;->aac:Lorg/spongycastle/crypto/h;

    .line 91
    iput-object p3, p0, Lorg/spongycastle/crypto/d/s;->aad:Lorg/spongycastle/crypto/h;

    .line 92
    check-cast p4, Lorg/spongycastle/crypto/j/ag;

    iput-object p4, p0, Lorg/spongycastle/crypto/d/s;->aae:Lorg/spongycastle/crypto/j/ag;

    .line 93
    return-void
.end method

.method public f([BII)[B
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->ZZ:Lorg/spongycastle/crypto/c;

    iget-object v1, p0, Lorg/spongycastle/crypto/d/s;->aac:Lorg/spongycastle/crypto/h;

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/c;->a(Lorg/spongycastle/crypto/h;)V

    .line 247
    iget-object v0, p0, Lorg/spongycastle/crypto/d/s;->ZZ:Lorg/spongycastle/crypto/c;

    iget-object v1, p0, Lorg/spongycastle/crypto/d/s;->aad:Lorg/spongycastle/crypto/h;

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/c;->b(Lorg/spongycastle/crypto/h;)Ljava/math/BigInteger;

    move-result-object v0

    .line 250
    invoke-static {v0}, Lorg/spongycastle/util/b;->g(Ljava/math/BigInteger;)[B

    move-result-object v0

    .line 252
    iget-boolean v1, p0, Lorg/spongycastle/crypto/d/s;->Wt:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/spongycastle/crypto/d/s;->b([BII[B)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3, v0}, Lorg/spongycastle/crypto/d/s;->a([BII[B)[B

    move-result-object v0

    goto :goto_0
.end method
