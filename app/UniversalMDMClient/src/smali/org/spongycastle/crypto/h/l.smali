.class public Lorg/spongycastle/crypto/h/l;
.super Ljava/lang/Object;
.source "SICBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/crypto/d;


# instance fields
.field private final Wu:Lorg/spongycastle/crypto/d;

.field private final abL:I

.field private abT:[B

.field private acB:[B

.field private acr:[B


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    .line 30
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/crypto/h/l;->abL:I

    .line 31
    iget v0, p0, Lorg/spongycastle/crypto/h/l;->abL:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/l;->abT:[B

    .line 32
    iget v0, p0, Lorg/spongycastle/crypto/h/l;->abL:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    .line 33
    iget v0, p0, Lorg/spongycastle/crypto/h/l;->abL:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/l;->acB:[B

    .line 34
    return-void
.end method


# virtual methods
.method public a([BI[BI)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    iget-object v4, p0, Lorg/spongycastle/crypto/h/l;->acB:[B

    invoke-interface {v0, v3, v1, v4, v1}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move v0, v1

    .line 92
    :goto_0
    iget-object v3, p0, Lorg/spongycastle/crypto/h/l;->acB:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 94
    add-int v3, p4, v0

    iget-object v4, p0, Lorg/spongycastle/crypto/h/l;->acB:[B

    aget-byte v4, v4, v0

    add-int v5, p2, v0

    aget-byte v5, p1, v5

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p3, v3

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v2

    :goto_1
    if-ltz v3, :cond_2

    .line 101
    iget-object v4, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    aget-byte v4, v4, v3

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v4, v0

    .line 103
    const/16 v0, 0xff

    if-le v4, v0, :cond_1

    move v0, v2

    .line 112
    :goto_2
    iget-object v5, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    int-to-byte v4, v4

    aput-byte v4, v5, v3

    .line 99
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 109
    goto :goto_2

    .line 115
    :cond_2
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    array-length v0, v0

    return v0
.end method

.method public getAlgorithmName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v1}, Lorg/spongycastle/crypto/d;->getAlgorithmName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/SIC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBlockSize()I
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    return v0
.end method

.method public getUnderlyingCipher()Lorg/spongycastle/crypto/d;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    return-object v0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    instance-of v0, p2, Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_1

    .line 55
    check-cast p2, Lorg/spongycastle/crypto/j/an;

    .line 56
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->getIV()[B

    move-result-object v0

    .line 57
    iget-object v1, p0, Lorg/spongycastle/crypto/h/l;->abT:[B

    iget-object v2, p0, Lorg/spongycastle/crypto/h/l;->abT:[B

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    invoke-virtual {p0}, Lorg/spongycastle/crypto/h/l;->reset()V

    .line 62
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    const/4 v1, 0x1

    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/spongycastle/crypto/d;->init(ZLorg/spongycastle/crypto/h;)V

    .line 71
    :cond_0
    return-void

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SIC mode requires ParametersWithIV"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->abT:[B

    iget-object v1, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    iget-object v2, p0, Lorg/spongycastle/crypto/h/l;->acr:[B

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    iget-object v0, p0, Lorg/spongycastle/crypto/h/l;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->reset()V

    .line 123
    return-void
.end method
