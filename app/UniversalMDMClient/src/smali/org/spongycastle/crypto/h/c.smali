.class public Lorg/spongycastle/crypto/h/c;
.super Ljava/lang/Object;
.source "CCMBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/crypto/h/a;


# instance fields
.field private Wt:Z

.field private Wu:Lorg/spongycastle/crypto/d;

.field private abG:I

.field private abL:I

.field private ace:[B

.field private acf:[B

.field private acg:Lorg/spongycastle/crypto/h;

.field private ach:[B

.field private aci:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    .line 41
    iput-object p1, p0, Lorg/spongycastle/crypto/h/c;->Wu:Lorg/spongycastle/crypto/d;

    .line 42
    invoke-interface {p1}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    iput v0, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    .line 43
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    .line 45
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cipher required with a block size of 16."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    return-void
.end method

.method private c([BII[B)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 257
    new-instance v3, Lorg/spongycastle/crypto/g/a;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->Wu:Lorg/spongycastle/crypto/d;

    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    mul-int/lit8 v4, v4, 0x8

    invoke-direct {v3, v2, v4}, Lorg/spongycastle/crypto/g/a;-><init>(Lorg/spongycastle/crypto/d;I)V

    .line 259
    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->acg:Lorg/spongycastle/crypto/h;

    invoke-interface {v3, v2}, Lorg/spongycastle/crypto/o;->a(Lorg/spongycastle/crypto/h;)V

    .line 264
    const/16 v2, 0x10

    new-array v4, v2, [B

    .line 266
    invoke-direct {p0}, Lorg/spongycastle/crypto/h/c;->qE()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    aget-byte v2, v4, v1

    or-int/lit8 v2, v2, 0x40

    int-to-byte v2, v2

    aput-byte v2, v4, v1

    .line 271
    :cond_0
    aget-byte v2, v4, v1

    invoke-interface {v3}, Lorg/spongycastle/crypto/o;->qc()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    div-int/lit8 v5, v5, 0x2

    and-int/lit8 v5, v5, 0x7

    shl-int/lit8 v5, v5, 0x3

    or-int/2addr v2, v5

    int-to-byte v2, v2

    aput-byte v2, v4, v1

    .line 273
    aget-byte v2, v4, v1

    iget-object v5, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    array-length v5, v5

    rsub-int/lit8 v5, v5, 0xf

    add-int/lit8 v5, v5, -0x1

    and-int/lit8 v5, v5, 0x7

    or-int/2addr v2, v5

    int-to-byte v2, v2

    aput-byte v2, v4, v1

    .line 275
    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    iget-object v5, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    array-length v5, v5

    invoke-static {v2, v1, v4, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, p3

    .line 279
    :goto_0
    if-lez v2, :cond_1

    .line 281
    array-length v5, v4

    sub-int/2addr v5, v0

    and-int/lit16 v6, v2, 0xff

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 282
    ushr-int/lit8 v2, v2, 0x8

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 286
    :cond_1
    array-length v0, v4

    invoke-interface {v3, v4, v1, v0}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 291
    invoke-direct {p0}, Lorg/spongycastle/crypto/h/c;->qE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 295
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    const v2, 0xff00

    if-ge v0, v2, :cond_2

    .line 297
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    shr-int/lit8 v0, v0, 0x8

    int-to-byte v0, v0

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 298
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    int-to-byte v0, v0

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 300
    const/4 v0, 0x2

    .line 314
    :goto_1
    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    iget-object v4, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v4, v4

    invoke-interface {v3, v2, v1, v4}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 316
    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v2, v2

    add-int/2addr v0, v2

    rem-int/lit8 v2, v0, 0x10

    .line 317
    if-eqz v2, :cond_3

    move v0, v1

    .line 319
    :goto_2
    rsub-int/lit8 v4, v2, 0x10

    if-eq v0, v4, :cond_3

    .line 321
    invoke-interface {v3, v1}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 319
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 304
    :cond_2
    const/4 v0, -0x1

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 305
    const/4 v0, -0x2

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 306
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    shr-int/lit8 v0, v0, 0x18

    int-to-byte v0, v0

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 307
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    shr-int/lit8 v0, v0, 0x10

    int-to-byte v0, v0

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 308
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    shr-int/lit8 v0, v0, 0x8

    int-to-byte v0, v0

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 309
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    int-to-byte v0, v0

    invoke-interface {v3, v0}, Lorg/spongycastle/crypto/o;->update(B)V

    .line 311
    const/4 v0, 0x6

    goto :goto_1

    .line 329
    :cond_3
    invoke-interface {v3, p1, p2, p3}, Lorg/spongycastle/crypto/o;->update([BII)V

    .line 331
    invoke-interface {v3, p4, v1}, Lorg/spongycastle/crypto/o;->doFinal([BI)I

    move-result v0

    return v0
.end method

.method private qE()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 116
    array-length v1, v0

    invoke-virtual {p0, v0, v2, v1}, Lorg/spongycastle/crypto/h/c;->m([BII)[B

    move-result-object v0

    .line 118
    array-length v1, v0

    invoke-static {v0, v2, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    invoke-virtual {p0}, Lorg/spongycastle/crypto/h/c;->reset()V

    .line 122
    array-length v0, v0

    return v0
.end method

.method public getOutputSize(I)I
    .locals 2

    .prologue
    .line 153
    iget-boolean v0, p0, Lorg/spongycastle/crypto/h/c;->Wt:Z

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    add-int/2addr v0, v1

    .line 159
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getUnderlyingCipher()Lorg/spongycastle/crypto/d;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->Wu:Lorg/spongycastle/crypto/d;

    return-object v0
.end method

.method public getUpdateOutputSize(I)I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 2

    .prologue
    .line 65
    iput-boolean p1, p0, Lorg/spongycastle/crypto/h/c;->Wt:Z

    .line 67
    instance-of v0, p2, Lorg/spongycastle/crypto/j/a;

    if-eqz v0, :cond_0

    .line 69
    check-cast p2, Lorg/spongycastle/crypto/j/a;

    .line 71
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qI()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    .line 72
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qH()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    .line 73
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qc()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    .line 74
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qG()Lorg/spongycastle/crypto/j/ak;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->acg:Lorg/spongycastle/crypto/h;

    .line 89
    :goto_0
    return-void

    .line 76
    :cond_0
    instance-of v0, p2, Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_1

    .line 78
    check-cast p2, Lorg/spongycastle/crypto/j/an;

    .line 80
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->getIV()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->acf:[B

    .line 82
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    .line 83
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/c;->acg:Lorg/spongycastle/crypto/h;

    goto :goto_0

    .line 87
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid parameters passed to CCM"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public m([BII)[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 166
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->acg:Lorg/spongycastle/crypto/h;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CCM cipher unitialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    new-instance v3, Lorg/spongycastle/crypto/h/l;

    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->Wu:Lorg/spongycastle/crypto/d;

    invoke-direct {v3, v0}, Lorg/spongycastle/crypto/h/l;-><init>(Lorg/spongycastle/crypto/d;)V

    .line 172
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    new-array v0, v0, [B

    .line 175
    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    array-length v2, v2

    rsub-int/lit8 v2, v2, 0xf

    add-int/lit8 v2, v2, -0x1

    and-int/lit8 v2, v2, 0x7

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 177
    iget-object v2, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    const/4 v4, 0x1

    iget-object v5, p0, Lorg/spongycastle/crypto/h/c;->ace:[B

    array-length v5, v5

    invoke-static {v2, v1, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    iget-boolean v2, p0, Lorg/spongycastle/crypto/h/c;->Wt:Z

    new-instance v4, Lorg/spongycastle/crypto/j/an;

    iget-object v5, p0, Lorg/spongycastle/crypto/h/c;->acg:Lorg/spongycastle/crypto/h;

    invoke-direct {v4, v5, v0}, Lorg/spongycastle/crypto/j/an;-><init>(Lorg/spongycastle/crypto/h;[B)V

    invoke-interface {v3, v2, v4}, Lorg/spongycastle/crypto/d;->init(ZLorg/spongycastle/crypto/h;)V

    .line 181
    iget-boolean v0, p0, Lorg/spongycastle/crypto/h/c;->Wt:Z

    if-eqz v0, :cond_2

    .line 186
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    add-int/2addr v0, p3

    new-array v2, v0, [B

    .line 188
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/spongycastle/crypto/h/c;->c([BII[B)I

    .line 190
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    iget-object v4, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    invoke-interface {v3, v0, v1, v4, v1}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    move v0, v1

    .line 192
    :goto_0
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    sub-int v4, p3, v4

    if-ge p2, v4, :cond_1

    .line 194
    invoke-interface {v3, p1, p2, v2, v0}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 195
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    add-int/2addr v0, v4

    .line 196
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    add-int/2addr p2, v4

    goto :goto_0

    .line 199
    :cond_1
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    new-array v4, v4, [B

    .line 201
    sub-int v5, p3, p2

    invoke-static {p1, p2, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    invoke-interface {v3, v4, v1, v4, v1}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 205
    sub-int v3, p3, p2

    invoke-static {v4, v1, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    sub-int v3, p3, p2

    add-int/2addr v0, v3

    .line 209
    iget-object v3, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    array-length v4, v2

    sub-int/2addr v4, v0

    invoke-static {v3, v1, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v2

    .line 252
    :goto_1
    return-object v0

    .line 216
    :cond_2
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    sub-int v0, p3, v0

    new-array v2, v0, [B

    .line 218
    add-int v0, p2, p3

    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    sub-int/2addr v0, v4

    iget-object v4, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    iget v5, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    invoke-static {p1, v0, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 220
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    iget-object v4, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    invoke-interface {v3, v0, v1, v4, v1}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 222
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abG:I

    :goto_2
    iget-object v4, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    array-length v4, v4

    if-eq v0, v4, :cond_5

    .line 224
    iget-object v4, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    aput-byte v1, v4, v0

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 227
    :goto_3
    array-length v4, v2

    iget v5, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    sub-int/2addr v4, v5

    if-ge v0, v4, :cond_3

    .line 229
    invoke-interface {v3, p1, p2, v2, v0}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 230
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    add-int/2addr v0, v4

    .line 231
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    add-int/2addr p2, v4

    goto :goto_3

    .line 234
    :cond_3
    iget v4, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    new-array v4, v4, [B

    .line 236
    array-length v5, v2

    sub-int/2addr v5, v0

    invoke-static {p1, p2, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 238
    invoke-interface {v3, v4, v1, v4, v1}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 240
    array-length v3, v2

    sub-int/2addr v3, v0

    invoke-static {v4, v1, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    iget v0, p0, Lorg/spongycastle/crypto/h/c;->abL:I

    new-array v0, v0, [B

    .line 244
    array-length v3, v2

    invoke-direct {p0, v2, v1, v3, v0}, Lorg/spongycastle/crypto/h/c;->c([BII[B)I

    .line 246
    iget-object v1, p0, Lorg/spongycastle/crypto/h/c;->ach:[B

    invoke-static {v1, v0}, Lorg/spongycastle/util/a;->i([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 248
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "mac check in CCM failed"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v0, v2

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public processByte(B[BI)I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public processBytes([BII[BI)I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->reset()V

    .line 128
    iget-object v0, p0, Lorg/spongycastle/crypto/h/c;->aci:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 129
    return-void
.end method
