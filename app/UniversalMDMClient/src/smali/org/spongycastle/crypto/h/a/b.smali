.class abstract Lorg/spongycastle/crypto/h/a/b;
.super Ljava/lang/Object;
.source "GCMUtil.java"


# direct methods
.method static M([B)[I
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 24
    new-array v0, v3, [I

    .line 25
    invoke-static {p0, v2}, Lorg/spongycastle/crypto/l/a;->u([BI)I

    move-result v1

    aput v1, v0, v2

    .line 26
    const/4 v1, 0x1

    invoke-static {p0, v3}, Lorg/spongycastle/crypto/l/a;->u([BI)I

    move-result v2

    aput v2, v0, v1

    .line 27
    const/4 v1, 0x2

    const/16 v2, 0x8

    invoke-static {p0, v2}, Lorg/spongycastle/crypto/l/a;->u([BI)I

    move-result v2

    aput v2, v0, v1

    .line 28
    const/4 v1, 0x3

    const/16 v2, 0xc

    invoke-static {p0, v2}, Lorg/spongycastle/crypto/l/a;->u([BI)I

    move-result v2

    aput v2, v0, v1

    .line 29
    return-object v0
.end method

.method static b([I[I)V
    .locals 3

    .prologue
    .line 150
    const/4 v0, 0x3

    :goto_0
    if-ltz v0, :cond_0

    .line 152
    aget v1, p0, v0

    aget v2, p1, v0

    xor-int/2addr v1, v2

    aput v1, p0, v0

    .line 150
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method

.method static c([II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 126
    move v1, v0

    .line 130
    :goto_0
    aget v2, p0, v1

    .line 131
    ushr-int v3, v2, p1

    or-int/2addr v0, v3

    aput v0, p0, v1

    .line 132
    add-int/lit8 v1, v1, 0x1

    const/4 v0, 0x4

    if-ne v1, v0, :cond_0

    .line 138
    return-void

    .line 136
    :cond_0
    rsub-int/lit8 v0, p1, 0x20

    shl-int v0, v2, v0

    .line 137
    goto :goto_0
.end method

.method static f([I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 64
    const/4 v0, 0x3

    aget v0, p0, v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 65
    :goto_0
    invoke-static {p0}, Lorg/spongycastle/crypto/h/a/b;->h([I)V

    .line 66
    if-eqz v0, :cond_0

    .line 70
    aget v0, p0, v1

    const/high16 v2, -0x1f000000

    xor-int/2addr v0, v2

    aput v0, p0, v1

    .line 72
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 64
    goto :goto_0
.end method

.method static g([I)V
    .locals 6

    .prologue
    .line 81
    const/4 v0, 0x3

    aget v1, p0, v0

    .line 82
    const/16 v0, 0x8

    invoke-static {p0, v0}, Lorg/spongycastle/crypto/h/a/b;->c([II)V

    .line 83
    const/4 v0, 0x7

    :goto_0
    if-ltz v0, :cond_1

    .line 85
    const/4 v2, 0x1

    shl-int/2addr v2, v0

    and-int/2addr v2, v1

    if-eqz v2, :cond_0

    .line 87
    const/4 v2, 0x0

    aget v3, p0, v2

    const/high16 v4, -0x1f000000

    rsub-int/lit8 v5, v0, 0x7

    ushr-int/2addr v4, v5

    xor-int/2addr v3, v4

    aput v3, p0, v2

    .line 83
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 90
    :cond_1
    return-void
.end method

.method static h([I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 110
    move v1, v0

    .line 114
    :goto_0
    aget v2, p0, v1

    .line 115
    ushr-int/lit8 v3, v2, 0x1

    or-int/2addr v0, v3

    aput v0, p0, v1

    .line 116
    add-int/lit8 v1, v1, 0x1

    const/4 v0, 0x4

    if-ne v1, v0, :cond_0

    .line 122
    return-void

    .line 120
    :cond_0
    shl-int/lit8 v0, v2, 0x1f

    .line 121
    goto :goto_0
.end method
