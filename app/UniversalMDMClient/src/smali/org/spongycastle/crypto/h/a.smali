.class public interface abstract Lorg/spongycastle/crypto/h/a;
.super Ljava/lang/Object;
.source "AEADBlockCipher.java"


# virtual methods
.method public abstract doFinal([BI)I
.end method

.method public abstract getOutputSize(I)I
.end method

.method public abstract getUnderlyingCipher()Lorg/spongycastle/crypto/d;
.end method

.method public abstract getUpdateOutputSize(I)I
.end method

.method public abstract init(ZLorg/spongycastle/crypto/h;)V
.end method

.method public abstract processByte(B[BI)I
.end method

.method public abstract processBytes([BII[BI)I
.end method
