.class public Lorg/spongycastle/crypto/h/g;
.super Ljava/lang/Object;
.source "GCMBlockCipher.java"

# interfaces
.implements Lorg/spongycastle/crypto/h/a;


# static fields
.field private static final abI:[B


# instance fields
.field private WF:[B

.field private WS:[B

.field private Ws:I

.field private Wt:Z

.field private Wu:Lorg/spongycastle/crypto/d;

.field private abG:I

.field private ace:[B

.field private ach:[B

.field private acm:[B

.field private acn:Lorg/spongycastle/crypto/h/a/a;

.field private aco:[B

.field private acp:[B

.field private acq:[B

.field private acr:[B

.field private acs:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0x10

    new-array v0, v0, [B

    sput-object v0, Lorg/spongycastle/crypto/h/g;->abI:[B

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/crypto/d;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/crypto/h/g;-><init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/h/a/a;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/crypto/d;Lorg/spongycastle/crypto/h/a/a;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-interface {p1}, Lorg/spongycastle/crypto/d;->getBlockSize()I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cipher required with a block size of 16."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    if-nez p2, :cond_1

    .line 62
    new-instance p2, Lorg/spongycastle/crypto/h/a/c;

    invoke-direct {p2}, Lorg/spongycastle/crypto/h/a/c;-><init>()V

    .line 65
    :cond_1
    iput-object p1, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    .line 66
    iput-object p2, p0, Lorg/spongycastle/crypto/h/g;->acn:Lorg/spongycastle/crypto/h/a/a;

    .line 67
    return-void
.end method

.method private K([B)[B
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x10

    .line 374
    new-array v2, v5, [B

    move v0, v1

    .line 376
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 378
    new-array v3, v5, [B

    .line 379
    array-length v4, p1

    sub-int/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 380
    invoke-static {p1, v0, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 381
    invoke-static {v2, v3}, Lorg/spongycastle/crypto/h/g;->f([B[B)V

    .line 382
    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acn:Lorg/spongycastle/crypto/h/a/a;

    invoke-interface {v3, v2}, Lorg/spongycastle/crypto/h/a/a;->L([B)V

    .line 376
    add-int/lit8 v0, v0, 0x10

    goto :goto_0

    .line 385
    :cond_0
    return-object v2
.end method

.method private a(B[BI)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v0, 0x10

    .line 225
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget v3, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    aput-byte p1, v2, v3

    .line 227
    iget v2, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    array-length v3, v3

    if-ne v2, v3, :cond_1

    .line 229
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    invoke-direct {p0, v2, v0, p2, p3}, Lorg/spongycastle/crypto/h/g;->h([BI[BI)V

    .line 230
    iget-boolean v2, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    if-nez v2, :cond_0

    .line 232
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget v4, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    invoke-static {v2, v0, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 235
    :cond_0
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x10

    iput v1, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    .line 240
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static d(J[BI)V
    .locals 2

    .prologue
    .line 418
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    long-to-int v0, v0

    invoke-static {v0, p2, p3}, Lorg/spongycastle/crypto/l/a;->h(I[BI)V

    .line 419
    long-to-int v0, p0

    add-int/lit8 v1, p3, 0x4

    invoke-static {v0, p2, v1}, Lorg/spongycastle/crypto/l/a;->h(I[BI)V

    .line 420
    return-void
.end method

.method private static f([B[B)V
    .locals 3

    .prologue
    .line 410
    const/16 v0, 0xf

    :goto_0
    if-ltz v0, :cond_0

    .line 412
    aget-byte v1, p0, v0

    aget-byte v2, p1, v0

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 410
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 414
    :cond_0
    return-void
.end method

.method private h([BI[BI)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 334
    const/16 v0, 0xf

    :goto_0
    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    .line 336
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->acr:[B

    aget-byte v1, v1, v0

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 337
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acr:[B

    aput-byte v1, v2, v0

    .line 339
    if-eqz v1, :cond_1

    .line 345
    :cond_0
    const/16 v0, 0x10

    new-array v1, v0, [B

    .line 346
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acr:[B

    invoke-interface {v0, v2, v3, v1, v3}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 349
    iget-boolean v0, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    if-eqz v0, :cond_2

    .line 351
    sget-object v0, Lorg/spongycastle/crypto/h/g;->abI:[B

    rsub-int/lit8 v2, p2, 0x10

    invoke-static {v0, p2, v1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 359
    :goto_1
    add-int/lit8 v2, p2, -0x1

    :goto_2
    if-ltz v2, :cond_3

    .line 361
    aget-byte v3, v1, v2

    aget-byte v4, p1, v2

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 362
    add-int v3, p4, v2

    aget-byte v4, v1, v2

    aput-byte v4, p3, v3

    .line 359
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 334
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 356
    goto :goto_1

    .line 366
    :cond_3
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    invoke-static {v1, v0}, Lorg/spongycastle/crypto/h/g;->f([B[B)V

    .line 367
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acn:Lorg/spongycastle/crypto/h/a/a;

    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/h/a/a;->L([B)V

    .line 369
    iget-wide v0, p0, Lorg/spongycastle/crypto/h/g;->acs:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/spongycastle/crypto/h/g;->acs:J

    .line 370
    return-void
.end method

.method private reset(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 313
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acp:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->T([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    .line 314
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->T([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->acr:[B

    .line 315
    iput v2, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    .line 316
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/spongycastle/crypto/h/g;->acs:J

    .line 318
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    invoke-static {v0, v2}, Lorg/spongycastle/util/a;->fill([BB)V

    .line 323
    :cond_0
    if-eqz p1, :cond_1

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->ach:[B

    .line 328
    :cond_1
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0}, Lorg/spongycastle/crypto/d;->reset()V

    .line 329
    return-void
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 10

    .prologue
    const-wide/16 v8, 0x8

    const/16 v6, 0x10

    const/4 v5, 0x0

    .line 246
    iget v0, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    .line 247
    iget-boolean v1, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    if-nez v1, :cond_1

    .line 249
    iget v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    if-ge v0, v1, :cond_0

    .line 251
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "data too short"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    iget v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    sub-int/2addr v0, v1

    .line 256
    :cond_1
    if-lez v0, :cond_2

    .line 258
    new-array v1, v6, [B

    .line 259
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    invoke-static {v2, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 260
    invoke-direct {p0, v1, v0, p1, p2}, Lorg/spongycastle/crypto/h/g;->h([BI[BI)V

    .line 264
    :cond_2
    new-array v1, v6, [B

    .line 265
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->aco:[B

    array-length v2, v2

    int-to-long v2, v2

    mul-long/2addr v2, v8

    invoke-static {v2, v3, v1, v5}, Lorg/spongycastle/crypto/h/g;->d(J[BI)V

    .line 266
    iget-wide v2, p0, Lorg/spongycastle/crypto/h/g;->acs:J

    mul-long/2addr v2, v8

    const/16 v4, 0x8

    invoke-static {v2, v3, v1, v4}, Lorg/spongycastle/crypto/h/g;->d(J[BI)V

    .line 268
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    invoke-static {v2, v1}, Lorg/spongycastle/crypto/h/g;->f([B[B)V

    .line 269
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->acn:Lorg/spongycastle/crypto/h/a/a;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    invoke-interface {v1, v2}, Lorg/spongycastle/crypto/h/a/a;->L([B)V

    .line 273
    new-array v1, v6, [B

    .line 274
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    invoke-interface {v2, v3, v5, v1, v5}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 275
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    invoke-static {v1, v2}, Lorg/spongycastle/crypto/h/g;->f([B[B)V

    .line 280
    iget v2, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/spongycastle/crypto/h/g;->ach:[B

    .line 281
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->ach:[B

    iget v3, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    invoke-static {v1, v5, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 283
    iget-boolean v1, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    if-eqz v1, :cond_4

    .line 286
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->ach:[B

    iget v2, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int/2addr v2, p2

    iget v3, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    invoke-static {v1, v5, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    iget v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    add-int/2addr v0, v1

    .line 300
    :cond_3
    invoke-direct {p0, v5}, Lorg/spongycastle/crypto/h/g;->reset(Z)V

    .line 302
    return v0

    .line 292
    :cond_4
    iget v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    new-array v1, v1, [B

    .line 293
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget v3, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    invoke-static {v2, v0, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 294
    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->ach:[B

    invoke-static {v2, v1}, Lorg/spongycastle/util/a;->i([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 296
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string v1, "mac check in GCM failed"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOutputSize(I)I
    .locals 2

    .prologue
    .line 176
    iget-boolean v0, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    if-eqz v0, :cond_0

    .line 178
    iget v0, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    add-int/2addr v0, v1

    .line 181
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int/2addr v0, p1

    iget v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getUnderlyingCipher()Lorg/spongycastle/crypto/d;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    return-object v0
.end method

.method public getUpdateOutputSize(I)I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int/2addr v0, p1

    div-int/lit8 v0, v0, 0x10

    mul-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public init(ZLorg/spongycastle/crypto/h;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/16 v1, 0x10

    const/4 v6, 0x0

    .line 82
    iput-boolean p1, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    .line 83
    iput-object v2, p0, Lorg/spongycastle/crypto/h/g;->ach:[B

    .line 87
    instance-of v0, p2, Lorg/spongycastle/crypto/j/a;

    if-eqz v0, :cond_3

    .line 89
    check-cast p2, Lorg/spongycastle/crypto/j/a;

    .line 91
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qI()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    .line 92
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qH()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->aco:[B

    .line 94
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qc()I

    move-result v0

    .line 95
    const/16 v2, 0x60

    if-lt v0, v2, :cond_0

    const/16 v2, 0x80

    if-gt v0, v2, :cond_0

    rem-int/lit8 v2, v0, 0x8

    if-eqz v2, :cond_1

    .line 97
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid value for MAC size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 100
    :cond_1
    div-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    .line 101
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/a;->qG()Lorg/spongycastle/crypto/j/ak;

    move-result-object v0

    move-object v2, v0

    .line 117
    :goto_0
    if-eqz p1, :cond_5

    move v0, v1

    .line 118
    :goto_1
    new-array v0, v0, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    .line 120
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    array-length v0, v0

    if-ge v0, v4, :cond_6

    .line 122
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IV must be at least 1 byte"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_3
    instance-of v0, p2, Lorg/spongycastle/crypto/j/an;

    if-eqz v0, :cond_4

    .line 105
    check-cast p2, Lorg/spongycastle/crypto/j/an;

    .line 107
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->getIV()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    .line 108
    iput-object v2, p0, Lorg/spongycastle/crypto/h/g;->aco:[B

    .line 109
    iput v1, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    .line 110
    invoke-virtual {p2}, Lorg/spongycastle/crypto/j/an;->qY()Lorg/spongycastle/crypto/h;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/crypto/j/ak;

    move-object v2, v0

    .line 111
    goto :goto_0

    .line 114
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid parameters passed to GCM"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_5
    iget v0, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    add-int/lit8 v0, v0, 0x10

    goto :goto_1

    .line 125
    :cond_6
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->aco:[B

    if-nez v0, :cond_7

    .line 128
    new-array v0, v6, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->aco:[B

    .line 133
    :cond_7
    if-eqz v2, :cond_8

    .line 135
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    invoke-interface {v0, v4, v2}, Lorg/spongycastle/crypto/d;->init(ZLorg/spongycastle/crypto/h;)V

    .line 142
    :cond_8
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->WF:[B

    .line 143
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->Wu:Lorg/spongycastle/crypto/d;

    sget-object v2, Lorg/spongycastle/crypto/h/g;->abI:[B

    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->WF:[B

    invoke-interface {v0, v2, v6, v3, v6}, Lorg/spongycastle/crypto/d;->a([BI[BI)I

    .line 144
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acn:Lorg/spongycastle/crypto/h/a/a;

    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->WF:[B

    invoke-interface {v0, v2}, Lorg/spongycastle/crypto/h/a/a;->init([B)V

    .line 146
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->aco:[B

    invoke-direct {p0, v0}, Lorg/spongycastle/crypto/h/g;->K([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->acp:[B

    .line 148
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    array-length v0, v0

    const/16 v2, 0xc

    if-ne v0, v2, :cond_9

    .line 150
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    .line 151
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    iget-object v2, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    array-length v2, v2

    invoke-static {v0, v6, v1, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    const/16 v1, 0xf

    aput-byte v4, v0, v1

    .line 163
    :goto_2
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acp:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->T([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->WS:[B

    .line 164
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->T([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->acr:[B

    .line 165
    iput v6, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    .line 166
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/spongycastle/crypto/h/g;->acs:J

    .line 167
    return-void

    .line 156
    :cond_9
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    invoke-direct {p0, v0}, Lorg/spongycastle/crypto/h/g;->K([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    .line 157
    new-array v0, v1, [B

    .line 158
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->ace:[B

    array-length v1, v1

    int-to-long v2, v1

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    const/16 v1, 0x8

    invoke-static {v2, v3, v0, v1}, Lorg/spongycastle/crypto/h/g;->d(J[BI)V

    .line 159
    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    invoke-static {v1, v0}, Lorg/spongycastle/crypto/h/g;->f([B[B)V

    .line 160
    iget-object v0, p0, Lorg/spongycastle/crypto/h/g;->acn:Lorg/spongycastle/crypto/h/a/a;

    iget-object v1, p0, Lorg/spongycastle/crypto/h/g;->acq:[B

    invoke-interface {v0, v1}, Lorg/spongycastle/crypto/h/a/a;->L([B)V

    goto :goto_2
.end method

.method public processByte(B[BI)I
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/crypto/h/g;->a(B[BI)I

    move-result v0

    return v0
.end method

.method public processBytes([BII[BI)I
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v2, 0x0

    .line 198
    move v1, v2

    move v0, v2

    .line 200
    :goto_0
    if-eq v1, p3, :cond_2

    .line 203
    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget v4, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    add-int v5, p2, v1

    aget-byte v5, p1, v5

    aput-byte v5, v3, v4

    .line 205
    iget v3, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    iget-object v4, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    array-length v4, v4

    if-ne v3, v4, :cond_1

    .line 207
    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    add-int v4, p5, v0

    invoke-direct {p0, v3, v6, p4, v4}, Lorg/spongycastle/crypto/h/g;->h([BI[BI)V

    .line 208
    iget-boolean v3, p0, Lorg/spongycastle/crypto/h/g;->Wt:Z

    if-nez v3, :cond_0

    .line 210
    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget-object v4, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    iget v5, p0, Lorg/spongycastle/crypto/h/g;->abG:I

    invoke-static {v3, v6, v4, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 213
    :cond_0
    iget-object v3, p0, Lorg/spongycastle/crypto/h/g;->acm:[B

    array-length v3, v3

    add-int/lit8 v3, v3, -0x10

    iput v3, p0, Lorg/spongycastle/crypto/h/g;->Ws:I

    .line 215
    add-int/lit8 v0, v0, 0x10

    .line 200
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 219
    :cond_2
    return v0
.end method
